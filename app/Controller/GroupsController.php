 <?php

/**
 * @name GroupssController.php
 * @abstract GroupsController contain main methods that related to Groups Module
 * @version Gridle Revamp August 2015
 */
 
App::uses('AppController', 'Controller');
App::uses('CakeEvent', 'Event');

class GroupsController extends AppController {

    public $uses = array('Group', 'User', 'Network', 'UserGroup','UserNetwork');

    /* Functions accoring to REST */

    /**
     * Create new group and addd member to it.
     * @param type $name    Name of the group
     * @return type $returnObject     
     */
    public function add() {
        $this->viewClass = 'Json';
        $this->request->allowMethod('post');
        if (empty($this->request->data) || empty($this->request->data['Group']['name'])) {
            throw new InvalidArgumentException(__('Missing Parameters.'), 400);
        }
        //Get User's username and network_id
        $user_id = $this->Auth->user('id');    
        $network_id = $this->Session->read('CurrentNetwork');
        $this->request->data['Group']['admin'] = $user_id;
        $this->request->data['Group']['network_id'] = $network_id;

        if ($this->Group->add($this->request->data)) {
//                $returnObject['data'] = $this->Group->data;
            $this->request->data['group_id'] = $this->Group->data['Group']['id'];
//            debug($this->Group->data);
//                $returnObject['message'] = "Group has been created.";
            if(isset($this->request->data['users'])){
                $returnObject = $this->requestAction(array('action' => 'addmember'), 
                    $extra = array(
                        'data' => array('users' => $this->request->data['users'],
                                        'group_id' => $this->request->data['group_id'])
                        ));
//$this->addmember();
            }else{
            #get group and its metadata    
            $group_data = $this->Group->getGroups($this->request->data['group_id'], $network_id);
            $group_data = $this->Group->getGroupUserInfo($group_data);
            $returnObject['data'] = $group_data;
            $returnObject['message'] = "Group has been created.";   
            }
            $this->set(array(
                'data' => $returnObject,
                '_serialize' => 'data'
            ));
            return;
        } else {
            $message = array_values($this->Group->validationErrors);
            if (isset($message)) {
                throw new Exception($message[0][0], 403);
            } else {
                throw new Exception('Some error occured. Please try again later.', 403);
            }
        }
    }

    public function edit() {
        $user_id = $this->Auth->user('id');
        $network_id = $this->Session->read('CurrentNetwork');
        $this->viewClass = 'Json';
        //Check if parameters are set
        if (isset($this->request->data['name'], $this->request->data['group_id'])) {
            //Check if user is admin
            $data['name'] = $this->request->data['name'];
            $data['id'] = $this->request->data['group_id'];
            $oldname = $this->Group->getGroupField($this->request->data['group_id'], $network_id, 'name');
            if ($this->Group->UsersGroup->getUsersGroupField($user_id, $this->request->data['group_id'], $network_id, 'role') == ROLE_ADMIN) {
                if ($this->Group->save($data)) {
                    $Event = new CakeEvent(
                            'Controller.Group.afterEdit', $this, array('user_id' => $user_id, 'group_id' => $this->request->data['group_id'], 'old_value' => $oldname, 'network_id' => $network_id, 'new_value' => $data['name'], 'changed' => Log_activity_edit_name)
                    );
                    $this->getEventManager()->dispatch($Event);
                    $returnObject['message'] = "Group details have been updated.";
                    $group_object['Group']['name'] = $this->request->data['name'];
                    
                    #set groupdata or groupinfo metadata
//                    $group_data1 = $this->Group->findById($this->request->data['group_id']);
                    $group_data = $this->Group->getGroups($this->request->data['group_id'], $network_id);
                    $group_data = $this->Group->getGroupUserInfo($group_data);
                    $returnObject['data'] = $group_data;
                    $this->set(array(
                        'data' => $returnObject,
                        '_serialize' => 'data'
                    ));
                    return;
                } else {
                    throw new Exception('Failed to update. Please try again.', 403);
                }
            } else {
                throw new Exception('You are not authorized to update.', 403);
            }
        } else {
            throw new Exception('Some error occured. Please try again later.', 403);
        }
    }

    public function delete() {

        $user_id = $this->Auth->user('id');
        //Check if parameters are set
        if (isset($this->request->data['group_id'])) {
            $group_id = $this->request->data['group_id'];
            $network_id = $this->Session->read('CurrentNetwork');
            $this->viewClass = 'Json';
            if ($this->Group->UsersGroup->getUsersGroupField($user_id, $group_id, $network_id, 'role') == ROLE_ADMIN) {
                if ($this->Group->groupDelete($group_id, $network_id)) {
                    $Event = new CakeEvent(
                            'Controller.Group.afterdelete', $this, array('user_id' => $user_id, 'network_id' => $network_id, 'group_id' => $group_id)
                    );
                    $this->getEventManager()->dispatch($Event);
                    $returnObject['message'] = "Group has been deleted.";
                    $this->set(array(
                        'data' => $returnObject,
                        '_serialize' => 'data'
                    ));
                    return;
                } else {
                    throw new Exception('Failed to delete group.', 403);
                }
            } else {
                throw new Exception('You are not authorized to delete group.', 403);
            }
        } else {
            throw new Exception('Some error occured. Please try again later.', 403);
        }
    }

    public function leave() {
        $user_id = $this->Auth->user('id');
        $network_id = $this->Session->read('CurrentNetwork');
        $this->viewClass = 'Json';
        $group_id = $this->request->data['group_id'];
        if (isset($group_id, $user_id)) {
            //Check if user is admin of group
            if (!($this->Group->UsersGroup->getUsersGroupField($user_id, $group_id, $network_id, 'role') == ROLE_ADMIN)) {
                if ($this->Group->UsersGroup->removemember($group_id, array($user_id),$user_id)) {
                    $Event = new CakeEvent(
                            'Controller.Group.afterleave', $this, array('user_id' => $user_id, 'network_id' => $network_id, 'group_id' => $group_id)
                    );
                    $this->getEventManager()->dispatch($Event);
                    
                    #set groupdata or groupinfo metadata
                    $group_data = $this->Group->getGroups($group_id, $network_id);
                    $groups = $this->Group->getGroupUserInfo($group_data);
//                    $groups = $this->Group->getgroups($group_id, $network_id);
                    $returnObject['message'] = 'Group left successfully.';
                    $returnObject['data'] = $groups;
                    $this->set(array(
                        'data' => $returnObject,
                        '_serialize' => 'data'
                    ));
                    return;
                } else {
                    throw new Exception('Failed to leave group.', 403);
                }
            } else {
                throw new Exception('Admin cannot leave group.', 403);
            }
        } else {
            throw new Exception('Some error occured. Please try again later.', 403);
        }
    }

    /**
     * Function to differentiate between a user that currently belongs to a network and 
     * a user who is not part of the same network.
     * @author Manish M Demblani <manish_demblani@blogtard.com>
     * @since Gridle Revamp August 2015
     */
    private function seperateEmail($network_id, $data = array()) {
        if(empty($network_id)) 
            throw new NotFoundException('Current Network not Found.');

        $result = $this->UserNetwork->getNetworkUser($network_id);
        $result = Hash::combine($result, '{n}.UserNetwork.user_id','{n}.UserInfo.email');
        /**
         * List of emails that already exist in the current network
         * Format: array( [user_id] => [email_id]);
         */
        $partOfNetwork = array_intersect($result,$data);

        /**
         * List of emails that are not part of the current network
         * Format: array( [user_id] => [email_id]);
         */
        $not_partOfNetwork = array_values(array_diff($data,$result));
        if(!empty($not_partOfNetwork)) {
            $this->request->data['email'] = $not_partOfNetwork;
            /**
             * Returns the list of users that were added to the network
             */
            $users_add = $this->requestAction(array('controller' => 'users', 'action' => 'admin_add'), 
                    $extra = array(
                        'data' => array('User' => array('return_to' => 'invite_network'),
                                        'email' => $not_partOfNetwork)
                        ));
            //Reformatting the array
            $users_add = Hash::combine($users_add, 'data.info.{n}.user_id', 'data.info.{n}.email');
            $partOfNetwork = array_replace($partOfNetwork ,$users_add);
        }
        
        /*
         * Return only the array keys since array format is as follows
         * array( [user_id] => [email_id]);
         */
        return array_keys($partOfNetwork);
    }
    
    public function addmember() {
        $this->viewClass = 'Json';
        $this->request->allowMethod('post','put');
        $user_id = $this->Auth->user('id');
        $network_id = $this->Session->read('CurrentNetwork');
        //Checks for all the possibilities of request data
        /**
         * Can be combined to one
         */
        if(empty($this->request->data) || empty($this->request->data['group_id']) || empty($this->request->data['users']))
            throw new InvalidArgumentException(__('Missing Parameters.'), 400);
        $data = $this->request->data['users'];
        if(empty($data) || (empty($data['email']) && empty($data['userlist'])))
            throw new InvalidArgumentException(__('Missing Parameters.'), 400);
        /**
         * Can be combined to one
         */
        //if user-id's are provided then the following condition will execute
        $userlist = isset($data['userlist']) ? $data['userlist'] : array();
        if(!empty($data['email'])) {
            //strtolower used since e-mail is stored as lowercase in database
            $result = $this->seperateEmail($network_id, array_map('strtolower', $data['email']));
            if($result) {
                    $userlist = array_merge($userlist, $result);
            }
        }
        //Create a unique list since multiple entries are possible.
        $userlist = array_unique($userlist);
        $group_id = $this->request->data['group_id'];        
        //Check if user is admin of group
        if ($this->Group->UsersGroup->getUsersGroupField($user_id, $group_id, $network_id, 'role') != ROLE_ADMIN) {
            throw new UnauthorizedException(__('You are not authorized to perform this action.'));
        }
        
        //Check If all user are part of this network - doubtfull
        if (!$this->Network->UserNetwork->isNetworkUsers($network_id, $userlist)) {
            throw new ForbiddenException(__('User(s) are not part of this network.'));
        }
                
        //Check if any user given is part of the current group
        /*Returns the list of users currenntly part of group*/
        $grp_users = Hash::extract(($this->Group->getGroups($group_id,$network_id)),'{n}.UsersGroup.{n}.user_id');

        //Check 1 - Check if admin is trying to add self
        if(in_array($user_id,$userlist)) {
            throw new UnauthorizedException(__('Cannot add self.'));
        } 
                
        //Remove entries of users from userslist array that already belong to the group
        $userlist = array_diff($userlist,$grp_users);
        
        if(empty($userlist)) {
            throw new RuntimeException(__('No new Members to be added'));
        }
        if ($this->Group->UsersGroup->addmember($group_id, $network_id, $user_id, $userlist)) {
            $group_data = $this->Group->getGroups($group_id, $network_id);
            $groups[0] = $this->Group->getGroupUserInfo($group_data);
            $gg = $this->Group->UsersGroup->findByGroupIdAndNetworkId($group_id,$network_id, array('UsersGroup.id','UsersGroup.group_id'));
            $groups[0]['Group']['user_group_id'] = $gg['UsersGroup']['id'];
            $returnObject['message'] = 'group added and Member added successfully.';
            $returnObject['data'] = $groups[0];
            $this->set(array(
                'data' => $returnObject,
                '_serialize' => 'data'
            ));
            return $returnObject;
        } else {
            throw new Exception(__('Failed to add members.'), 403);
        }
    }

    public function removemember() {

        $auth_username = $this->Auth->user('id');
        $network_id = $this->Session->read('CurrentNetwork');
        $this->viewClass = 'Json';
        $user_id = $this->request->data['user_id'];
        $group_id = $this->request->data['group_id'];
        if (isset($group_id, $user_id)) {
            //Check if user is admin of group
            if ($this->Group->UsersGroup->getUsersGroupField($auth_username, $group_id, $network_id, 'role') == ROLE_ADMIN) {
                if ($this->Group->UsersGroup->removemember($group_id, array($user_id),$auth_username)) {
                    $Event = new CakeEvent(
                            'Controller.Group.aftermemberremove', $this, array('user_id' => $user_id, 'network_id' => $network_id, 'group_id' => $group_id, 'admin' => $auth_username)
                    );
                    $this->getEventManager()->dispatch($Event);
//                    $groups = $this->Group->getgroups($group_id, $network_id);
                    
                    #set groupdata or groupinfo metadata
                    $group_data = $this->Group->getGroups($group_id, $network_id);
                    $groups[0] = $this->Group->getGroupUserInfo($group_data);
                    $returnObject['message'] = 'Member/s removed successfully.';
                    $returnObject['data'] = $groups[0];
                    $this->set(array(
                        'data' => $returnObject,
                        '_serialize' => 'data'
                    ));
                    return;
                } else {
                    throw new Exception('Failed to remove members.', 403);
                }
            } else {
                throw new Exception('You are not authorized to remove members.', 403);
            }
        } else {
            throw new Exception('Some error occured. Please try again later.', 403);
        }
    }

    public function view() {
        
    }

    public function getGroup($id = NULL) {
        $this->viewClass = 'Json';
//        if ($this->request->is('Post')) {
            if (empty($id)) {
                throw new Exception('Missing Parameters.', 403);
            } else {
                $user_name = $this->Auth->User('username');
                $user_id = $this->User->findByUsername($user_name);
                $user_id = $user_id['User']['id'];
                $network_id = $this->Session->read('CurrentNetwork');
                if ($this->Group->UsersGroup->getUsersGroupField($user_id, $id, $network_id, 'status') === STATUS_ACTIVE) {
                    $group_data = $this->Group->getGroups($id, $network_id);
                    $group_data = $this->Group->getGroupUserInfo($group_data);
//                        $this->log($group_data);                    
                    if (!empty($group_data)) {
                        
                        $returnObject = array(
                            'message' => 'Group Data Fetched Successfully',
                            'data' => $group_data,
                            'count_data' => count($group_data['UsersGroup'])
                        );
                        
//                        $returnObject['message'] = 'Group Data Fetched Successfully';
//                        $returnObject['data'] = $group_data;
//                        $this->log('in getgroups');

                        $this->set(array(
                            'data' => $returnObject,
                            '_serialize' => 'data'
                        ));
                        return;
                    } else {
                        throw new Exception('Unable to Fetch Group Data.', 403);
                    }
                } else {
                    throw new Exception('You are not the member of group.', 403);
                }
            }
//        }
    }


}

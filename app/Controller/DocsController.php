<?php

App::uses('CakeEvent', 'Event');
App::uses('HttpSocket', 'Network/Http');

class DocsController extends AppController {

    public $uses = array('Doc', 'DocAssignment', 'Network', 'UserNetwork', 'Group','UserInfo');
    public $components = array('Comments.Comments' =>
        array(
            'responseActions' => array('isUserPart' => 'isPartOfDoc')
        ),
        'Paginator'
    );

    function index() {
        
    }
    function index_demo() {
        
    }

    public function view($doc_id = null) {
        $data['user'] = $this->Auth->user('id');
        $data['id'] = $doc_id;
        if (!$this->Doc->isPartOfDoc($data)) {
            $this->Session->setFlash("Page could have been removed or not found");
            $this->redirect($this->referer());
        }
    }
/**
 * it returns full info about given doc for notification.
 * @param type $doc_id -> doc id 
 * @return type
 * @throws Exception
 */
    public function getDoc($doc_id) {
        $this->viewClass = 'Json';
        if (true) {
//        if ($this->request->is('Post')) {
            $user_id = $this->Auth->user('id');
            $network_id = $this->Session->read('CurrentNetwork');
            if (!empty($doc_id)) {
                if ($this->DocAssignment->getDocAssignmentField($doc_id, $network_id, $user_id, 'status') === STATUS_ACTIVE) {
//                    $doc = $this->Doc->getDocs($doc_id);
                    $doc_ids= array($doc_id);
                    $doc = $this->getDocData($doc_ids);
                    $returnObject = array(
                        'message' => "File fetched",
                        'data' => $doc[0]
                    );
                    $this->set('returnObject', $returnObject);
                    return;
                } else {
                    throw new Exception('You dont have any such file.', 403);
                }
            } else {
                throw new Exception('Missing Parameters.', 403);
            }
        }
    }

    public function download($doc_id = NULL) {
        $user_id = $this->Auth->user('id');
        $network_id = $this->Session->read('CurrentNetwork');
        $this->viewClass = 'Json';
        if (!empty($doc_id)) {
            //If user or his active group is a part of this group
            $groups = $this->DocAssignment->findByGroupId($this->Group->UsersGroup->getActiveGroups($user_id, $network_id));
            if ($this->DocAssignment->getDocAssignmentField($doc_id, $network_id, $user_id, 'status') === STATUS_ACTIVE || (!empty($groups))) {
                $docs_details = $this->Doc->findById($doc_id);

                if (in_array($docs_details['Doc']['provider'], array('dropbox'))) {
                    $temp_path = $docs_details['Doc']['bucket_path'];
                } else {
                    $temp_path = $this->Doc->getURL($docs_details['Doc']['bucket_path']);
                }
                if ($temp_path) {
                    /*
                     * Force download file from URL
                     */
                    header('Content-disposition: attachment;filename=' . $docs_details['Doc']['name'] . "." . $docs_details['Doc']['file_extension']);
                    header('Content-Type: application/octet-stream');
                    readfile($temp_path);
                    exit;
                } else {
                    throw new Exception('Some error occured. Please try again later.', 403);
                }
            } else {
                throw new Exception('Some error occured. Please try again later.', 403);
            }
        } else {
            throw new Exception('Some error occured. Please try again later.', 403);
        }
    }
    /**
     * method for get preview and thumbnail url from frambench
     * @param type $doc_id
     */
    public function getPreview($doc_id = null){
        $this->viewClass = 'Json';
 
        $docs_details = $this->Doc->findById($doc_id);
        $network_id = $this->Session->read('CurrentNetwork');

        $temp_path = $this->Doc->getURL($docs_details['Doc']['bucket_path']);
        
        $data = array('apiKey' => _framebench_api_key, 'email' => $this->Auth->user('UserInfo.email'), 'firstName' => $this->Auth->user('UserInfo.first_name'), 'lastName' => $this->Auth->user('UserInfo.last_name'), 'workspaceId' => _framebench_prefix . $network_id, 'workspaceName' => _framebench_prefix . $network_id, 'fileLink' => $temp_path, 'fileId' => $doc_id);
        CakeResque::enqueue('default', 'DocShell', array('DocThumbnail', $data));
         $returnObject['data'] = 'success';
                    $this->set('returnObject', $returnObject);
    }

    public function edit() {
        $this->viewClass = 'Json';
        $user_id = $this->Auth->user('id');
        $network_id = $this->Session->read('CurrentNetwork');
        //Check if parameters are set
        if (isset($this->request->data['name'], $this->request->data['doc_id'])) {
            //Check if user is admin
            $data['name'] = $this->request->data['name'];
            $data['id'] = $this->request->data['doc_id'];
            if ($this->Doc->getDocField('user_id', $this->request->data['doc_id'], $network_id) == $user_id) {
                if ($this->Doc->save($data)) {
                    $Event = new CakeEvent(
                            'Controller.Doc.afterEdit', $this, array('user_id' => $user_id, 'network_id' => $network_id, 'doc_id' => $this->request->data['doc_id'])
                    );
                    $this->getEventManager()->dispatch($Event);
                    $returnObject['message'] = "File details updated successfully.";
                    $group_object['Group']['name'] = $this->request->data['name'];
                    $returnObject['data'] = $this->Doc->getDocObject($this->request->data['doc_id'], $network_id);
                    $this->set('returnObject', $returnObject);
                    return;
                } else {
                    throw new Exception('Failed to update details.', 403);
                }
            } else {
                throw new Exception('You are not authorized to edit details.', 403);
            }
        } else {
            throw new Exception('Some error occured. Please try again later.', 403);
        }
    }

    /**
     * Uploader enpoint to store uploaded files
     */
    function upload() {
        $this->viewClass = 'Json';
        if ($this->request->is('Post') && isset($_FILES['file']) && $_FILES['file']['error'] == UPLOAD_ERR_OK) {
            $userID = $this->Auth->user('id');
            $network_id = $this->Session->read('CurrentNetwork');
            $data['Doc']['hash_name'] = md5($_FILES['file']['name'] . time() . mt_rand());
            $data['Doc']['user_id'] = $userID;
            $data['Doc']['network_id'] = $network_id;

            $data['Doc']['file_extension'] = pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);
            $data['Doc']['name'] = pathinfo($_FILES['file']['name'], PATHINFO_FILENAME);
            $data['Doc']['file_mime'] = $_FILES['file']['type'];
            $data['Doc']['file_size'] = $_FILES['file']['size'];
            $data['Doc']['bucket_path'] = S3_File_Upload_Path . $network_id . "/" . $userID . "/" . $data['Doc']['hash_name'];
            $sharelist_final = [];
            if (isset($this->request->data['userlist'])) {
                $sharelist_final = $this->Doc->splittaglist($this->request->data['userlist']);
                if (!$this->UserNetwork->isNetworkUsers($network_id, $sharelist_final['user']) || !$this->Group->isNetworkGroups($network_id, $sharelist_final['group'])) {
                    throw new Exception('User/s or Group/s not part of this network.', 403);
                }
            }
            $user_file_limit = (int) $this->UserNetwork->getUserNetworkField($userID, $network_id, 'file_limit');
            $user_file_count = (int) $this->UserNetwork->getUserNetworkField($userID, $network_id, 'file_count');
            $network_file_limit = (int) $this->Network->getNetworkField($network_id, 'file_limit');
            $network_file_count = (int) $this->Network->getNetworkField($network_id, 'file_count');
            if (($user_file_limit > ($user_file_count + $_FILES['file']['size'])) && ($network_file_limit > ($network_file_count + $_FILES['file']['size']))) {
                $dataSource = $this->Doc->getdatasource();
                $before_save_timestamp = time();

                $dataSource->begin();
                $doc_id = $this->Doc->addDoc($userID, $network_id, $sharelist_final, $data);
                if ($doc_id) {
                    $network_file_count = $this->Doc->getFileSpaceNetwork($network_id);
                    $user_file_space = $this->Doc->getFileSpaceUser($userID, $network_id);
                    if ($this->Network->setNetworkField($network_id, 'file_count', $network_file_count)) {
                        if ($this->UserNetwork->setUserNetworkField($userID, $network_id, 'file_count', $user_file_space)) {
                         //   if(true){
                            if ($this->Doc->uploadFile($data['Doc']['bucket_path'], $_FILES['file']['tmp_name'], $network_id, $userID)) {
                                $Event = new CakeEvent(
                                        'Controller.Doc.afterCreate', $this, array('user_id' => $userID, 'network_id' => $network_id, 'doc_id' => $doc_id)
                                );
                                $this->getEventManager()->dispatch($Event);
                                if (!empty($sharelist_final['user']) || !empty($sharelist_final['group'])) {

                                    $Event = new CakeEvent('Controller.Doc.aftermemberadd', $this, array(
                                        'user_id' => $userID, 
                                        'network_id' => $network_id, 
                                        'doc_id' => $doc_id, 
                                        'userlist' => $sharelist_final['user'], 
                                        'grouplist' => $sharelist_final['group'],
                                        'before_save_timestamp' => $before_save_timestamp,
                                        's_fname'=> $this->Auth->user('UserInfo.first_name'))
                                    );
                                    $this->getEventManager()->dispatch($Event);
                                }
                                $dataSource->commit();
                                $returnObject['message'] = "File/s uploaded successfully.";
//                                $doc = $this->Doc->getDocs($doc_id);
                                $doc_ids= array($doc_id);
                                $doc = $this->getDocData($doc_ids);
                                $returnObject['data'] = $doc[0];
                            
                            /*to send preview url and thumbnail  request*/
                                
                                $temp_path = $this->Doc->getURL($data['Doc']['bucket_path']);
                                $data = array('apiKey' => _framebench_api_key, 'email' => $this->Auth->user('UserInfo.email'), 'firstName' => $this->Auth->user('UserInfo.first_name'), 'lastName' => $this->Auth->user('UserInfo.last_name'), 'workspaceId' => _framebench_prefix . $network_id, 'workspaceName' => _framebench_prefix . $network_id, 'fileLink' => $temp_path, 'fileId' => $doc_id);
        
                                CakeResque::enqueue('default', 'DocShell', array('DocThumbnail', $data));
                                $this->set('returnObject', $returnObject);
                                return;
                            } else {
                                $dataSource->rollback();
                                throw new Exception('Failed to upload file/s.', 403);
                            }
                        } else {
                            $dataSource->rollback();
                            throw new Exception('Failed to upload file/s.', 403);
                        }
                    } else {
                        $dataSource->rollback();
                        throw new Exception('Failed to upload file/s.', 403);
                    }
                } else {
                    $dataSource->rollback();
                    throw new Exception('Failed to upload file/s.', 403);
                }
            } else {
                throw new Exception('Your network has reached limit. Please upgrade your plan.', 403);
            }
        } else {
            throw new Exception('Some error occured. Please try again later.', 403);
        }
    }

    public function addSharedList() {
        $this->viewClass = 'Json';
        if ($this->request->is('Post')) {
            if (isset($this->request->data['doc_id']) && isset($this->request->data['userlist'])) {
                $sharelist = $this->Doc->splittaglist($this->request->data['userlist']);
                $doc_id = $this->request->data['doc_id'];
                $shared_by = $this->Auth->user('id');
                $network_id = $this->Session->read('CurrentNetwork');
                $user = $this->Doc->getDocField('user_id', $doc_id, $network_id);
                if ($this->DocAssignment->getDocAssignmentField($doc_id, $network_id, $shared_by, 'status') == STATUS_ACTIVE) {
                    $sharelist_final = $this->DocAssignment->checkAssignedDoc($doc_id, $sharelist);
                    if (!$this->UserNetwork->isNetworkUsers($network_id, $sharelist_final['user']) || !$this->Group->isNetworkGroups($network_id, $sharelist_final['group'])) {
                        throw new Exception('User/s or Group/s not part of this network.', 403);
                    }
                    if (!empty($sharelist_final['user']) || !empty($sharelist_final['group'])) {

                        $before_save_timestamp = time();
                        if ($this->DocAssignment->addSharedList($doc_id, $shared_by, $network_id, $sharelist_final)) {
                            $Event = new CakeEvent('Controller.Doc.aftermemberadd', $this, array('user_id' => $shared_by, 'network_id' => $network_id, 'doc_id' => $doc_id, 'userlist' => $sharelist_final['user'], 'grouplist' => $sharelist_final['group'], 'before_save_timestamp' => $before_save_timestamp,'s_fname'=> $this->Auth->user('UserInfo.first_name')));



                            $this->getEventManager()->dispatch($Event);
                            $returnObject['message'] = "File shared successfully.";
//                            $doc = $this->Doc->getDocs($doc_id);
                            $doc_ids= array($doc_id);
                            $doc = $this->getDocData($doc_ids);
                            $returnObject['data'] = $doc[0];
                            $this->set('returnObject', $returnObject);
                            return;
                        } else {
                            throw new Exception('Failed to share file.', 403);
                        }
                    } else {
                        throw new Exception('File has already been shared.', 403);
                    }
                } else {
                    throw new Exception('You are not authorized to share this file.', 403);
                }
            } else {
                throw new Exception('Some error occured. Please try again later.', 403);
            }
        } else {
            throw new Exception('Some error occured. Please try again later.', 403);
        }
    }

    public function removeSharedList() {
        $this->viewClass = 'Json';
        if ($this->request->is('Post')) {
            if (isset($this->request->data['doc_id'], $this->request->data['value'], $this->request->data['type'])) {
                $doc_id = $this->request->data['doc_id'];
                $user_id = $this->Auth->user('id');
                $network_id = $this->Session->read('CurrentNetwork');
                $doc_id = $this->request->data['doc_id'];
                $remove_id = $this->request->data['value'];
                $remove_type = $this->request->data ['type'];
                $user = $this->Doc->getDocField('user_id', $doc_id, $network_id);
                if ($this->DocAssignment->getDocAssignmentField($doc_id, $network_id, $user_id, 'status') === STATUS_ACTIVE) {
//                    if ($this->Doc->getDocField('user_id', $doc_id, $network_id) == $user_id) {
                    if ($this->DocAssignment->removeSharedList($doc_id, $user_id, $network_id, $remove_id, $remove_type)) {
                        $sharelist_final['user'] = [];
                        $sharelist_final['group'] = [];
                        array_push($sharelist_final[$remove_type], $remove_id);
                        $Event = new CakeEvent('Controller.Doc.aftermemberremove', $this, array('user_id' => $user_id, 'network_id' => $network_id, 'doc_id' => $doc_id, 'userlist' => $sharelist_final['user'], 'grouplist' => $sharelist_final['group']));
                        $this->getEventManager()->dispatch($Event);
                        $returnObject['message'] = "File unshared successfully.";
                        $doc = $this->Doc->getDocs($doc_id);
                        $returnObject['data'] = $doc[0];
                        $this->set('returnObject', $returnObject);
                        return;
                    } else {
                        throw new Exception('Failed to unshare file.', 403);
                    }
                } else {
                    throw new Exception('You are not authorized to unshare this file.', 403);
                }
            } else {
                throw new Exception('Some error occured. Please try again later.', 403);
            }
        } else {
            throw new Exception('Some error occured. Please try again later.', 403);
        }
    }

    public function delete($doc_id = null) {
        $this->viewClass = 'Json';
            if($this->request->is('Post')){
        if (!is_null($doc_id)) {
                $removed_by = $this->Auth->user('id');
                $network_id = $this->Session->read('CurrentNetwork');
                if ($this->Doc->getDocField('user_id', $doc_id, $network_id) == $removed_by) {
                    $bucket_path = $this->Doc->getDocField('bucket_path', $doc_id, $network_id);
                    $dataSource = $this->Doc->getdatasource();
                    $dataSource->begin();
                    //First delete the doc and docassignment
                    //Then update the file space count
                    //After all this is successfull, only then delete the physical file
                    //This will allow to rollback in case file cannot be deleted
//                    if ($this->Doc->removeDoc($doc_id, $network_id, $removed_by)) {
                    if ($this->Doc->deleteDoc($doc_id)) {
                        $network_file_count = $this->Doc->getFileSpaceNetwork($network_id);
                        $user_file_count = $this->Doc->getFileSpaceUser($removed_by, $network_id);
                        if ($this->Network->setNetworkField($network_id, 'file_count', $network_file_count) && $this->UserNetwork->setUserNetworkField($removed_by, $network_id, 'file_count', $user_file_count)) {
                            if(true){
//                            if ($this->Doc->deleteFile($bucket_path)) {
                                $Event = new CakeEvent('Controller.Doc.afterdelete', $this, array('user_id' => $removed_by, 'network_id' => $network_id, 'doc_id' => $doc_id));
                                $this->getEventManager()->dispatch($Event);
                                $dataSource->commit();
                                $returnObject['message'] = "File deleted successfully.";
                                $returnObject['data'] = $doc_id;
                                $this->set('returnObject', $returnObject);
                                return;
                            } else {
                                $dataSource->rollback();
                                throw new Exception('Some error occured. Please try again later.', 403);
                                $returnObject['status'] = "error";
                                $returnObject['message'] = "Failed to deleted file.";
                            }
                        } else {
                            $dataSource->rollback();
                            throw new Exception('Some error occured. Please try again later.', 403);
                            $returnObject['status'] = "error";
                            $returnObject['message'] = "Failed to delete file.";
                        }
                    } else {
                        $dataSource->rollback();
                        throw new Exception('Failed to delete file.', 403);
                    }
                } else {
                    throw new Exception('You are not authorized to delete this file.', 403);
                }
            } else {
                throw new Exception('Some error occured. Please try again later.', 403);
            }
            }
    }

    public function preview($doc_id) {
        $user_id = $this->Auth->user('id');
        $network_id = $this->Session->read('CurrentNetwork');
        $this->viewClass = 'Json';
        if (!empty($doc_id)) {
            //If user or his active group is a part of this group
            $groups = $this->DocAssignment->findByGroupId($this->Group->UsersGroup->getActiveGroups($user_id, $network_id));
            if ($this->DocAssignment->getDocAssignmentField($doc_id, $network_id, $user_id, 'status') === STATUS_ACTIVE || (!empty($groups))) {
                $docs_details = $this->Doc->findById($doc_id);
                $temp_path = $this->Doc->getURL($docs_details['Doc']['bucket_path']);
                $httpsocket = new HttpSocket();
                $data = array('apiKey' => _framebench_api_key, 'email' => $this->Auth->user('UserInfo.email'), 'firstName' => $this->Auth->user('UserInfo.first_name'), 'lastName' => $this->Auth->user('UserInfo.last_name'), 'workspaceId' => _framebench_prefix . $network_id, 'workspaceName' => _framebench_prefix . $network_id, 'fileLink' => $temp_path, 'fileId' => $doc_id);
                $results = $httpsocket->post('https://lite.framebench.com/api/file', $data);

                if ($results->isOk()) {
                    $response = json_decode($results->body());
                    $returnObject['data'] = $response->link;                    
                    $this->set(array(
                        'data' => $returnObject,
                        '_serialize' => 'data'
                    ));
                    return;
            } else {
                    throw new Exception('Some error occured. Please try again later.', 403);
                }
            } else {
                throw new Exception('Not allowed to access this file.', 403);
            }
        } else {
            throw new Exception('Missing Parameters. Invalid Request.', 403);
        }
    }

    /**
     * Upload file using Dropbox/GDrive/box
     * @author Hardik Sondagar
     * @param string file_url URL of dropbox/gdrive/box's file
     * @param array assignee users list with file assigned
     */
    public function xUpload() {

        if ($this->request->is('Post')) {
            $this->viewClass = 'Json';
            $userID = $this->Auth->user('id');
            $network_id = $this->Session->read('CurrentNetwork');
            $data['Doc']['hash_name'] = md5($this->request->data['Doc']['name'] . time() . mt_rand());
            $data['Doc']['user_id'] = $userID;
            $data['Doc']['network_id'] = $network_id;
            $data['Doc']['provider'] = $this->request->data['Doc']['provider'];
            $data['Doc']['file_extension'] = pathinfo($this->request->data['Doc']['name'], PATHINFO_EXTENSION);
            $data['Doc']['name'] = pathinfo($this->request->data['Doc']['name'], PATHINFO_FILENAME);
            $data['Doc']['file_mime'] = $this->request->data['Doc']['provider'];
            $data['Doc']['file_size'] = $this->request->data['Doc']['size'];
            $data['Doc']['preview_url'] = $this->request->data['Doc']['preview_url'];
            $data['Doc']['bucket_path'] = $this->request->data['Doc']['url'];
            $data['Doc']['thumbnailLink'] = isset($this->request->data['Doc']['thumbnailLink']) ? $this->request->data['Doc']['thumbnailLink'] : null;

            $sharelist_final = [];
            if (isset($this->request->data['userlist'])) {
                $sharelist_final = $this->Doc->splittaglist($this->request->data['userlist']);
                if (!$this->UserNetwork->isNetworkUsers($network_id, $sharelist_final['user']) || !$this->Group->isNetworkGroups($network_id, $sharelist_final['group'])) {
                    throw new Exception('User/s or Group/s not part of this network.', 403);
                }
            }

            $before_save_timestamp = time();
            if ($this->Doc->addDoc($userID, $network_id, $sharelist_final, $data)) {

                $Event = new CakeEvent(
                        'Controller.Doc.afterCreate', $this, array('user_id' => $userID, 'network_id' => $network_id, 'doc_id' => $this->Doc->id)
                );
                $this->getEventManager()->dispatch($Event);
                if (!empty($sharelist_final['user']) || !empty($sharelist_final['group'])) {

                    $Event = new CakeEvent('Controller.Doc.aftermemberadd', $this, array('user_id' => $userID, 'network_id' => $network_id, 'doc_id' => $this->Doc->id, 'userlist' => $sharelist_final['user'], 'grouplist' => $sharelist_final['group'], 'before_save_timestamp' => $before_save_timestamp,'s_fname'=> $this->Auth->user('UserInfo.first_name')));
                    $this->getEventManager()->dispatch($Event);
                }

                $returnObject['message'] = "File/s uploaded successfully.";
//                $doc = $this->Doc->getDocs($this->Doc->id);
                //                                $doc = $this->Doc->getDocs($doc_id);
                                $doc_ids= array($this->Doc->id);
                                $doc = $this->getDocData($doc_ids);
                                $returnObject['data'] = $doc[0];
                $returnObject['data'] = $doc[0];
                $this->set(array(
                    'data' => $returnObject,
                    '_serialize' => 'data'
                ));
//                return;
            } else {
                throw new Exception('Some error occured. Please try again later.', 403);
            }
        }
    }
/**
 * it is the basic method for search sort and paginate of DOCS.
 * @param type $type => user, group , search String
 * @param type $page
 * @param type $id => user_id, group_id
 * @param type $sort => size0,size1,create0,create1,name0,name1
 */
    public function getFiles($type = null, $id = null ,$page = null,$sort = null ) {
        $this->viewClass = 'Json';
         $user_id = $this->Auth->user('id');
         $moreTag = FALSE;
         $network_id = $this->Session->read('CurrentNetwork');
         if(!isset($id)){
            $id = $this->Auth->user('id');
        }
        
        /*check whether it is group or not.if it will group then group_id is current group_id*/
        if($type == 'group'){
            $group_ids = $id;    
            $id = -1;
        }  else{
            $group_ids = $this->Group->UsersGroup->getActiveGroups($user_id, $network_id, $id);
        }    
        $options = [
            'type' =>$type,
            'id'=>$id,
            'group_id'=>$group_ids,
            'page'=>$page,
            'limit'=> Docs_To_Load,
            'sort' => $sort,
        ];
        /*it returns the doc_id of required info*/
        
        $doc_ids = $this->Doc->DocAssignment->getDocId($user_id, $network_id, $options);
        if(count($doc_ids[0])>= Docs_To_Load){
            $moreTag = TRUE;
        }
        /*return the data and its user_info of doc_ids*/
        $result = $this->getDocData($doc_ids[0]);
        $returnObject = array(
            'data' => array('files' => $result),
            'status' => 'success',
            'count'=> $doc_ids['doc_count'],
            'loadMore' => $moreTag
            
        );
        $this->set(array(
            'docs' => $returnObject,
            '_serialize' => 'docs'
        ));
    }
    
    /**
     * this method returns userinfo for docs.
     * @param type $doc_ids -> array of doc ids 
     * @return array
     */
    public function getDocData($doc_ids){
        $this->viewClass = 'Json';
        $result = array();
        
        
        foreach ($doc_ids as $key=>$value) {
            $my_docs = $this->Doc->getDocs($value);
            $my_docs = $my_docs[0];
          
            $tmp = $this->UserInfo->view($my_docs['Doc']['user_id']);
            $my_docs['Doc']['UserInfo'] = $tmp[0]['UserInfo'];        
            $user_id = array_filter(Hash::extract($my_docs,'DocAssignment.{n}.user_id'));
            $group_id = array_filter(Hash::extract($my_docs,'DocAssignment.{n}.group_id'));
            $comments = Hash::extract($my_docs,'Comment');

            /*add user info to user*/            
            if(isset($user_id)){
            foreach ($user_id as $user_key => $user_value) {
                $user = array();
                $user['user_id'] = $user_value;

                $user['type'] = 'user';

                $temp['info'] = $this->UserInfo->view($user['user_id']);
                if(!empty($temp)){
                $user['name'] = $temp['info'][0]['UserInfo']['name'];
                $user['vga'] = $temp['info'][0]['UserInfo']['vga'];

                $my_docs['DocAssignment'][$user_key]['Info'] = $user;
            }    
            }
            }
            
            /*add user/group info to group*/
            if(isset($group_id)){
            foreach ($group_id as $group_key=>$group_value) {

                $data = array();
                $data = $this->Group->getGroup($group_value);
                if(!empty($data)){
                $group = array(
                    'group_id' => $data[0]['Group']['id'],
                    'type' => 'group',
                    'role' => 'user',
                    'name' => $data[0]['Group']['name'],
                    'user_count'=>$data[0]['Group']['users_group_count'],
                    'admin'=>$data[0]['Group']['admin']
                );
                $my_docs['DocAssignment'][$group_key]['Info'] = $group;
            }    
            }
            }
            
            /*add user data to comment*/
            if (isset($comments)) {
                foreach ($comments as $key => $value) {

                $value['value'] = $value['user_id'];
                $value['type'] = 'user';
                $temp['info'] = $this->UserInfo->view($value['value']);

                if(!empty($temp)){
                $my_docs['Comment'][$key]['user_info'] = $temp['info'][0]['UserInfo'];   
                }
                }
            }
            
            array_push($result, $my_docs);
        }
        return $result;
        
    }


}

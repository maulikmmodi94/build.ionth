<?php

App::uses('AppController', 'Controller');
App::uses('CakeNumber', 'Utility');

class AdminsController extends AppController {

    public $name = 'Admins';
    public $uses = array('Network', 'UserNetwork', 'Users.User', 'UserInfo', 'Plan', 'NetworkPlan');
    public $helpers = array('Time');
    public $components = array('RequestHandler', 'Paginator');

    /**
     * beforeFilter callback
     *
     * @return void
     */
    public function beforeFilter() {
        parent::beforeFilter();
        $this->Auth->authorize = array('Controller');
    }

    public function isAuthorized($user = null) {
        if (in_array($this->Auth->user("role"), array(ROLE_ADMIN, ROLE_OWNER))) {
            return true;
        }
        // Default deny
        return false;
    }

    public function networks($id = null) {

        $id = isset($this->request->data['Network']['id']) ? $this->request->data['Network']['id'] : $id;

        $conditions = array(
            'Network.id !=' => $id
        );

        $network_usage = $this->Network->find('all', array('fields' => array('SUM(file_limit) as total'), 'conditions' => $conditions));

        if ($this->request->is('put')) {


            if (!$this->Network->exists($this->request->data['Network']['id'])) {
                $this->Session->setFlash(__('Invalid Network'));
                return $this->redirect($this->referer());
            }

            $network_file_count = $this->Network->getNetworkField($this->request->data['Network']['id'], 'file_count');
            if ($network_file_count > $this->request->data['Network']['file_limit']) {
                $this->Session->setFlash(__('You can not degrade file limit below your current usage.'));
                return $this->redirect($this->referer());
            } else if ($network_usage[0][0]['total'] + $this->request->data['Network']['file_limit'] > Configure::read('file_limit')) {
                $this->Session->setFlash(__('File size limit exceeded.'));
                return $this->redirect($this->referer());
            }


            $expires = new DateTime($this->request->data['Network']['expires']);
            $this->request->data['Network']['expires'] = $expires->format('Y-m-d H:i:00');

            if ($this->Network->save($this->request->data)) {
                $this->Session->setFlash('Network details updated successfully.', 'default', array('class' => 'success message'));
            } else {
                $this->Session->setFlash('Failed to update network.', 'default', array('class' => 'error message'));
            }
            return $this->redirect($this->referer());
        }
        if ($this->request->is('post')) {

            /* Old Code */
            if ($this->request->is('Post')) {

                $this->request->data['NetworkPlan'][0]['plan_id'] = isset($this->request->data['NetworkPlan'][0]['plan_id']) ? $this->request->data['NetworkPlan'][0]['plan_id'] : 1;

                if (!$this->Plan->exists($this->request->data['NetworkPlan'][0]['plan_id'])) {
                    $this->Session->setFlash('Failed to create network.', 'default', array('class' => 'error message'));
                    return $this->redirect($this->referer());
                }

                $plan = $this->Plan->findById($this->request->data['NetworkPlan'][0]['plan_id']);
                $time_limit = date('Y-m-d H:i:s', strtotime("+" . $plan['Plan']['time_limit'] . " months", time()));

                if ($network_usage[0][0]['total'] + $plan['Plan']['storage_limit'] > Configure::read('file_limit')) {
                    $this->Session->setFlash(__('Invalid Limit'));
                    return $this->redirect($this->referer());
                }

                $defaults = array(
                    'Network' => array(
                        'created_by' => $this->Auth->user('id'),
                        'user_limit' => $plan['Plan']['user_limit'],
                        'file_limit' => $plan['Plan']['storage_limit'],
                        'status' => STATUS_ACTIVE,
                        'expires' => $time_limit
                    ),
                    'UserNetwork' => array(
                        array(
                            'user_id' => $this->Auth->user('id'),
                            'status' => STATUS_ACTIVE,
                            'role' => ROLE_OWNER,
                            'file_limit' => $plan['Plan']['storage_limit']
                        )
                    ),
                    'NetworkPlan' => array(
                        array(
                            'start' => date('Y-m-d H:i:s', time()),
                            'end' => $time_limit
                        )
                    )
                );

                $allowed_role = array(ROLE_ADMIN, ROLE_OWNER);
                if (in_array($this->Auth->user('role'), $allowed_role) && !$this->request->data['Network']['self_add']) {
                    unset($defaults['UserNetwork']);
                }



                $this->request->data['Network'] = array_merge($defaults['Network'], $this->request->data['Network']);
                $this->request->data['NetworkPlan'][0]['start'] = date('Y-m-d H:i:s', time());
                $this->request->data['NetworkPlan'][0]['end'] = $time_limit;
                $this->request->data = array_merge($defaults, $this->request->data);

                if ($this->Network->add($this->request->data)) {
                    $this->Session->setFlash('Network created successfully.', 'default', array('class' => 'success message'));
                    return $this->redirect($this->referer());
                } else {
                    $this->Session->setFlash('Failed to create network.', 'default', array('class' => 'error message'));
                }
            }
        }


        if ($this->request->is('delete')) {
            $this->Network->id = $id;
            if (!$this->Network->exists()) {
                throw new NotFoundException(__('Invalid Network'));
            }
            $this->request->allowMethod('delete');

            /* Soft detele network */
            $conditions = array(
                'Network.' . $this->Network->primaryKey => $id
            );

            if ($this->Network->soft_delete($conditions, array(), 0)) {
                $this->Session->setFlash('Network deleted successfully.');
            } else {
                $this->Session->setFlash('Failed to delete network.');
            }

            return $this->redirect($this->referer());
        }


        $this->Paginator->settings = array(
            'limit' => 20,
            'order' => array(
                'Network.name' => 'asc'
            )
        );
        $data = $this->Paginator->paginate('Network');
        $this->set('networks', $data);
        $this->set('plans', $this->Plan->find('list'));
        $this->set('network_usage', $network_usage[0][0]['total']);
        $this->set('network_limit', Configure::read('file_limit'));
    }

    public function plans($id = null) {
        if ($this->request->is('post')) {
            $this->Plan->create();
            if ($this->Plan->save($this->request->data)) {
                $this->Session->setFlash(__('Plan has been added.'));
                return $this->redirect($this->referer());
            } else {
                $this->Session->setFlash(__('The plan could not be saved. Please, try again.'));
            }
        }

        if ($this->request->is('delete')) {
            $this->Plan->id = $id;
            if (!$this->Plan->exists()) {
                throw new NotFoundException(__('Invalid Plan'));
            }
            $this->request->allowMethod('delete');
            if ($this->Plan->delete()) {
                $this->Session->setFlash(__('The Plan has been deleted.'));
            } else {
                $this->Session->setFlash(__('The Plan could not be deleted. Please, try again.'));
            }
            return $this->redirect($this->referer());
        }


        $this->Paginator->settings = array(
            'limit' => 20,
            'order' => array(
                'Plan.name' => 'asc'
            )
        );
        $data = $this->Paginator->paginate('Plan');
        $this->set('plans', $data);
    }

    public function members($id = null) {
        if ($this->request->is('post')) {

            if (!$this->Network->exists($this->request->data['UserNetwork']['network_id'])) {
                $this->Session->setFlash(__('Invalid Network'));
                return $this->redirect($this->referer());
            } else {
                $network = $this->Network->findById($this->request->data['UserNetwork']['network_id']);
                if ($network['Network']['remaining_users'] < count($this->request->data['UserNetwork']['email'])) {
                    $this->Session->setFlash(__('Network member limit exceeded'));
                    return $this->redirect($this->referer());
                }
            }


            $user = $this->addUser($this->request->data['UserNetwork']['email'], $network['Network']['name']);
            if (!isset($user['User']['id'])) {
                $this->Session->setFlash(__('Member could not be added.'));
                return $this->redirect($this->referer());
            }

            $member = $this->UserNetwork->findByUserIdAndNetworkId($user['User']['id'], $this->request->data['UserNetwork']['network_id']);

            if (!empty($member)) {
                $this->Session->setFlash(__('Already member.'));
                return $this->redirect($this->referer());
            }

            $this->UserNetwork->create();
            $this->request->data['UserNetwork']['user_id'] = $user['User']['id'];
            $this->request->data['UserNetwork']['status'] = 'active';
            $this->request->data['UserNetwork']['added_by'] = $this->Auth->user('id');
            $this->request->data['UserNetwork']['file_limit'] = $network['Network']['file_limit'];


            if ($this->UserNetwork->save($this->request->data)) {
                $this->Session->setFlash(__('member has been added.'));

                if (!isset($user['User']['new'])) {

                    CakeResque::enqueue('default', 'NetworkShell', array('invite', array(
                            'Network' => $network['Network'],
                            'UserNetwork' => $user['User']['id'],
                            'invited_by' => $this->Auth->user('UserInfo.name')
                    )));
                }


                return $this->redirect($this->referer());
            } else {
                $this->Session->setFlash(__('The member could not be saved. Please, try again.'));
            }
        }

        if ($this->request->is('delete')) {
            $this->UserNetwork->id = $id;
            if (!$this->UserNetwork->exists()) {
                throw new NotFoundException(__('Invalid member'));
            }
            $this->request->allowMethod('delete');

            $usernetwork = $this->UserNetwork->findById($id);
            if ($this->UserNetwork->remove($usernetwork['UserNetwork']['user_id'], $usernetwork['UserNetwork']['network_id'], $this->Auth->user('id'), true, true) && $this->UserNetwork->getAffectedRows() > 0) {
                /* On successfully removing dispatch event to perform post operation live remove user from Group,Conversation,Task,Files etc */
                $Event = new CakeEvent(
                        'Controller.Network.afterMemberRemove', $this, array('user_id' => $usernetwork['UserNetwork']['user_id'], 'network_id' => $usernetwork['UserNetwork']['network_id'], 'admin' => $this->Auth->user('id'))
                );
                $this->getEventManager()->dispatch($Event);

                $this->Session->setFlash(__('The member has been deleted.'));
            } else {
                $this->Session->setFlash(__('The member could not be deleted. Please, try again.'));
            }


            return $this->redirect($this->referer());
        }

        $conditions = array('Network.isDeleted' => NOT_DELETED);
        if (isset($this->passedArgs['network_id'])) {
            $conditions = array('UserNetwork.network_id' => $this->passedArgs['network_id']);
        }

        $this->Paginator->settings = array(
            'limit' => 20,
            'order' => array(
                'UserInfo.name' => 'asc'
            ),
            'conditions' => $conditions,
            'contain' => array(
                'Network', 'UserInfo'
            )
        );
        $data = $this->Paginator->paginate('UserNetwork');
        $this->set('members', $data);

        $conditions = array();
        if (isset($this->passedArgs['network_id'])) {
            $network_ids = array_unique(array_merge(Hash::extract($data, '{n}.UserNetwork.network_id'), array($this->passedArgs['network_id'])));
            $conditions = array('Network.id' => $network_ids);
            $this->set('default', $this->passedArgs['network_id']);
        }

        $this->set('networks', $this->Network->find('list', array('conditions' => $conditions)));
        $this->set('role', array(ROLE_USER => ROLE_USER, ROLE_ADMIN => ROLE_ADMIN, ROLE_OWNER => ROLE_OWNER));
    }

    private function addUser($email = null, $invited_to = null) {
        $this->viewClass = 'Json';

        $user = $this->User->findByEmail($email);
        if (empty($user) && !is_array($email)) {
            $email = strtolower($email);
            if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                throw new Exception('Invalid email address');
            }


            $email_explode = explode("@", $email);
            $slug = preg_replace("/[^0-9a-zA-Z ]/m", "", $email_explode[0]);
            $slug = preg_replace("/ /", "-", $slug);
            $username = $this->User->makeUniqueSlug(strtolower($slug));


            $data = array(
                'User' => array(
                    'username' => $username,
                    'email' => $email,
                    'password' => $this->User->generatePassword(),
                    'tos' => true,
                    'added_by' => $this->Auth->user('id'),
                    'active' => false,
                    'invite_token' => $this->User->generateToken()
                ),
                'UserInfo' => array(
                    'first_name' => ucfirst($slug),
                    'last_name' => '',
                    'email' => $email
                ),
                'EmailSetting' => array(
                    'email' => $email
                )
            );


            $this->User->UserInfo->validator()->remove('last_name');
            $this->User->UserInfo->validator()->remove('first_name');

            $this->User->create();
            if ($this->User->saveAll($data)) {
                $user = $this->User->findById($this->User->id);
                $user['User']['new'] = true;
            } else {
                $user = $this->User->validationErrors;
            }


            $email_data = array(
                'name' => $this->Auth->user('UserInfo.name'),
                'email' => $email,
                'network_name' => $invited_to,
                'url' => Router::url(array('admin' => false, 'plugin' => 'users', 'controller' => 'users', 'action' => 'add', $data['User']['invite_token']), true)
            );
            CakeResque::enqueue('default', 'MailShell', array('sendInvitationEmail', $email_data));
        }

        return $user;
//        $this->set(array(
//            'user' => $user,
//            '_serialize' => 'user'
//        ));
    }

}

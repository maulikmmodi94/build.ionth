<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class ConversationsController extends AppController {

    public $uses = array('Conversation', 'UserConversation', 'User', 'Message', 'Group', 'UserNetwork');

    /**
     * @abstract    Loads more conversations and its respective data.
     * @request     Get
     * @params      Page Number to be loaded
     * @return      Data if successful, error otherwise
     */
    public function loadmoreconversations($set = null) {
        $this->viewClass = 'Json';
        $network_id = $this->Session->read('CurrentNetwork');
        //load particular set conversations of a user
        $conversations = $this->UserConversation->loadconversations($this->Auth->user('id'), $network_id, $set);
        
        $returnObject['message'] = "More conversations loaded";
        $returnObject['data'] = $conversations;
        $this->set(array(
            'data' => $returnObject,
            '_serialize' => 'data'
        ));
        return;
    }

    /**
     * @abstract    Add users into a conversation
     * @return \CakeResponse
     */
    public function addparticipants() {
        $this->viewClass = 'Json';
        if (!empty($this->request->data['conversation_id']) && !empty($this->request->data['userlist'])) {
            $conversation_id = $this->request->data['conversation_id'];
            //get the required data
            $creator = $this->Auth->user('id');
            $network_id = $this->Session->read('CurrentNetwork');
            $userlist = $this->Conversation->splittaglist($this->request->data['userlist']);
            //If Conversation in one - many
            if ($this->Conversation->getConversationField($conversation_id, $network_id, 'mapping') === Conversation_mapping_multiple && $this->UserConversation->getUserConversationField($creator, $conversation_id, $network_id, 'role') === ROLE_ADMIN) {
                //Check users belong to my connection
                if (!$this->UserNetwork->isNetworkUsers($network_id, $userlist['user'])) {
                    throw new Exception('User/s not part of this network', 403);
                }
                if ($this->UserConversation->addparticipants($conversation_id, $network_id, array_unique($userlist['user']))) {
                    $conversation = $this->UserConversation->loadconversationpeople($conversation_id);
                    
                    $returnObject['message'] = "Members added successfully.";
                    $returnObject['data'] = $conversation['UserConversation'];
                    $this->set(array(
                        'data' => $returnObject,
                        '_serialize' => 'data'
                    ));
                    return;
                } else {
                    throw new Exception('Failed to add members', 403);
                }
            } else {
                throw new Exception('You are not authorized to add members.', 403);
            }
        } else {
            throw new Exception('Some error occured. Please try again later.', 403);
        }
    }

    /**
     * remove participants from a conversation
     * @return type
     */
    public function removeparticipants() {
        $this->viewClass = 'Json';
        if (!empty($this->request->data['conversation_id']) && !empty($this->request->data['user'])) {
            $conversation_id = $this->request->data['conversation_id'];
            //get the required data
            $creator = $this->Auth->user('id');
            $network_id = $this->Session->read('CurrentNetwork');
            $removeuser = $this->request->data['user'];
            //If Conversation in one - many
            if ($this->Conversation->getConversationField($conversation_id, $network_id, 'mapping') === Conversation_mapping_multiple && $this->UserConversation->getUserConversationField($creator, $conversation_id, $network_id, 'role') === ROLE_ADMIN) {
                if ($this->UserConversation->removeparticipants($conversation_id, $network_id, $removeuser)) {
                    $conversation = $this->UserConversation->loadconversationpeople($conversation_id);
                    
                    $returnObject['message'] = "Member removed successfully.";
                    $returnObject['data'] = $conversation['UserConversation'];
                    $this->set(array(
                        'data' => $returnObject,
                        '_serialize' => 'data'
                    ));
                    return;
                } else {
                    throw new Exception('Failed to remove members.', 403);
                }
            } else {
                throw new Exception('You are not authorized to remove members.', 403);
            }
        } else {
            throw new Exception('Some error occured. Please try again later.', 403);
        }
    }

    public function leaveconversation($conversation_id) {
        $this->viewClass = 'Json';
        //check if all data is present
        if (!empty($conversation_id)) {
            //get the required data
            $creator = $this->Auth->user('id');
            $network_id = $this->Session->read('CurrentNetwork');
            //add users to userconversation
            $user_conversation_object = $this->UserConversation->findByConversationIdAndUsername($conversation_id, $creator, array('UserConversation.role', 'UserConversation.network_id'));
            if ($user_conversation_object['UserConversation']['role'] == UserConversation_Role_Admin && $user_conversation_object['UserConversation']['network_id'] == $network_id) {
//            if ($this->UserConversation->getUserConversationField($creator, $conversation_id, 'code') == UserConversation_Status_Active) {
                if ($this->UserConversation->leaveconversation($conversation_id, $creator)) {
                    
                    $returnObject['message'] = "Conversation left successfully.";
                    $returnObject['data'] = $conversation_id;
                    $this->set(array(
                        'data' => $returnObject,
                        '_serialize' => 'data'
                    ));
                    return;
                } else {
                    throw new Exception('Failed to leave conversation.', 403);
                }
            } else {
                throw new Exception('You are not a part of this conversation.', 403);
            }
        } else {
            throw new Exception('Some error occured. Please try again later.', 403);
        }
    }

    /**
     * @abstract    Creates new or adds into conversation if exists.
     * @param array     userlist    Users receiving message
     * @param string    body        Message body     
     * @return \CakeResponse    conversationid if successful,error otherwise
     */
    public function createconversation() {
        $this->viewClass = 'Json';
        //Check all parameters
        if (!empty($this->request->data['body']) && !empty($this->request->data['userlist'])) {
            //get the message and process it
            $msg = $this->request->data['body'];
            $network_id = $this->Session->read('CurrentNetwork');
            $author = $this->Auth->user('id');
            $users = array();       //list of users
            $userlist = $this->Conversation->splittaglist($this->request->data['userlist']);
            if (!$this->UserNetwork->isNetworkUsers($network_id, $userlist['user']) && $this->Group->isNetworkGroups($network_id, $userlist['group'])) {
                throw new Exception('User/s are not a part of this network.', 403);
            }
            //If no users or group passed,or both passed together
            if ((sizeof($userlist['user']) < 1 && sizeof($userlist['group']) < 1) || (sizeof($userlist['user']) > 0 && sizeof($userlist['group']) > 0)) {
                throw new Exception('Only users or one group is allowed at a time.', 403);
            }
            //If no users and only groups passed
            else if (sizeof($userlist['user']) < 1 && sizeof($userlist['group']) > 0) {
                //If more than one group passed
                if (sizeof($userlist['group']) > 1) {
                    throw new Exception('Multiple groups are not allowed.', 403);
                }
                //Only one group
                else {
                    $conversation = $this->Conversation->find('first', array('conditions' => array('Conversation.group_id' => $userlist[1][0])));
                    $conversation_id = $conversation['Conversation']['id'];
                    if ($this->Conversation->Message->addmessage($conversation_id, $author, $msg)) {
                        
                        $returnObject['message'] = "Message sent successfully.";
                        $returnObject['data'] = $conversation_id;
                        $this->set(array(
                            'data' => $returnObject,
                            '_serialize' => 'data'
                        ));
                        return;
                    } else {
                        throw new Exception('Failed to send message.', 403);
                    }
                }
            } else if (sizeof($userlist['user']) > 0) {
                $userlist['user'] = array_unique($userlist['user']);
                if (count($userlist['user']) == 1) {    //one-one conversation
//check if conversation exist
                    $conversation_id = $this->Conversation->UserConversation->getconversationid($author, $userlist['user'][0], $network_id);
                    if (is_null($conversation_id)) { //if no conversation, create new
                        $mapping = Conversation_mapping_single;
                        $conversation_id = $this->Conversation->createconversation($network_id, $author, $userlist['user'], $mapping, NULL, $msg);
                        
                        $returnObject['message'] = "Message sent successfully.";
                        $returnObject['data'] = $conversation_id;
                        $this->set(array(
                            'data' => $returnObject,
                            '_serialize' => 'data'
                        ));
                        return;
                    } else {
                        if ($this->Conversation->Message->addmessage($conversation_id, $author, $msg)) {
                            $this->UserConversation->setconversationunread($conversation_id, $author);
                            
                            $returnObject['message'] = "Message sent successfully.";
                            $returnObject['data'] = $conversation_id;
                            $this->set(array(
                                'data' => $returnObject,
                                '_serialize' => 'data'
                            ));
                            return;
                        } else {
                            throw new Exception('Failed to send message.', 403);
                        }
                    }
                } else if (count($userlist['user']) > 1) {   //one-many conversation                   
                    $mapping = Conversation_mapping_multiple;
                    $conversation_id = $this->Conversation->createconversation($network_id, $author, $userlist['user'], $mapping, NULL, $msg);
                    
                    $returnObject['message'] = "Message sent successfully.";
                    $returnObject['data'] = $conversation_id;
                    $this->set(array(
                        'data' => $returnObject,
                        '_serialize' => 'data'
                    ));
                    return;
                }
            }
        } else {
            throw new Exception('Some error occured. Please try again later.', 403);
        }
    }

    /* Rest Based */

    public function index($conversation_id = NULL) {
        if (!empty($conversation_id)) {
            $network_id = $this->Session->read('CurrentNetwork');
            $user_id = $this->Auth->user('id');
            if ($this->UserConversation->getUserConversationField($user_id, $conversation_id, $network_id, 'status') == STATUS_ACTIVE) {
                return;
            } else {
                $this->redirect('/conversations');
            }
        }
    }

    public function setread($conversation_id) {
        $this->viewClass = 'Json';
        $network_id = $this->Session->read('CurrentNetwork');
        $user_id = $this->Auth->user('id');
        //set this conversation as read by this user
        if (!$this->UserConversation->getUserConversationField($user_id, $conversation_id, $network_id, 'isUnread')) {
            $this->UserConversation->setread($user_id, $conversation_id);
            $conversation = $this->UserConversation->loadconversationpeople($conversation_id);
            $returnObject['data'] = $conversation['UserConversation'];
            
            $returnObject['message'] = "Conversation status changed";
            $this->set(array(
                'data' => $returnObject,
                '_serialize' => 'data'
            ));
            return;
        } else {
            throw new Exception('Conversation is read.', 403);
        }
    }

    public function getConversationUnreadCount() {
        $this->viewClass = 'Json';
        $network_id = $this->Session->read('CurrentNetwork');
        $author = $this->Auth->user('id');
        
        $returnObject['message'] = "Conversation status changed";
        $returnObject['data'] = $this->UserConversation->find('count', array('conditions' =>
            array(
                'UserConversation.network_id' => $network_id,
                'UserConversation.user_id' => $author,
                'UserConversation.isUnread' => NOT_SEEN
            )
        ));
        $this->set(array(
            'data' => $returnObject,
            '_serialize' => 'data'
        ));
        return;
    }

}

?>

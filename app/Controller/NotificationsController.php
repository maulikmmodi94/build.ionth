<?php

class NotificationsController extends AppController {

    public $uses = array('Notification', 'UserNetwork','Data');

    public function index() {
        
    }

    public function setUnreadAll($latest_notification_id = null, $latest_notification_logid = null) {
        $this->viewClass = 'Json';
        $user_id = $this->Auth->User('id');
        $network_id = $this->Session->read('CurrentNetwork');
//        $this->Notification->setIsSeen($network_id, $user_id, $latest_notification_id);
        $count_unread_notifications = $this->Notification->countAndReadUnseenNotifications($network_id, $user_id, $latest_notification_id , $latest_notification_logid);
        $this->UserNetwork->setUserNetworkField($user_id, $network_id, 'unseen_notification', $count_unread_notifications);
        $returnObject['status'] = 'success';
        $this->set(array(
            'data' => $returnObject,
            '_serialize' => 'data'
        ));
        return;
    }
    
    public function readNotification($notification_id = null){
        $this->viewClass = 'Json';
        $user_id = $this->Auth->User('id');
        $network_id = $this->Session->read('CurrentNetwork');
        
        $this->Notification->setNotificationSeen($network_id, $user_id, $notification_id);
        
        $unseen_notification_count = $this->requestAction('datas/getNotificationCount');
        
        $returnObject = array(
            'status' => 'success',
            'count' => $unseen_notification_count['data']);
        $this->set(array(
            'data' => $returnObject,
            '_serialize' => 'data'
        ));
    }

}

<?php

/**
 * @author Naimish Sakhpara <naimish.sakhpara@blogtard.com>
 */
class MessagesController extends AppController {

    public $uses = array('Message', 'Conversation', 'UserConversation');

    //add a message in a conversation
    public function reply($conversation_id) {
        $this->viewClass = 'Json';
        if (!empty($this->request->data['body']) && !empty($conversation_id)) {
            $network_id = $this->Session->read('CurrentNetwork');
            //if user belongs to conversation
            if ($this->UserConversation->getUserConversationField($this->Auth->user('id'), $conversation_id, $network_id, 'status') === STATUS_ACTIVE) {
                if ($this->Message->addmessage($conversation_id, $this->Auth->user('id'), $this->request->data['body'])) {
                    $this->UserConversation->setread($this->Auth->user('id'), $conversation_id);
                    $this->UserConversation->setconversationunread($conversation_id, $this->Auth->user('id'));
                    
                    $returnObject['message'] = "Message sent successfully.";
                    $returnObject['data'] = $conversation_id;
                    $this->set(array(
                        'data' => $returnObject,
                        '_serialize' => 'data'
                    ));
                    return;
                } else {
                    throw new Exception('Failed to send message.', 403);
                }
            } else {
                throw new Exception('No such conversation.', 403);
            }
        } else {//if any argument is not passed or not feteched properly            
            throw new Exception('Some error occured. Please try again later.', 403);
        }
    }

    public function groupreply($group_id) {
        $this->viewClass = 'Json';
        if (!empty($this->request->data['body']) && !empty($group_id)) {
            $network_id = $this->Session->read('CurrentNetwork');
            //if user belongs to conversation
            $conversation_id = $this->UserConversation->findByUserIdAndNetworkIdAndGroupId($this->Auth->user('id'), $network_id, $group_id, array('UserConversation.conversation_id'));
            if (!empty($conversation_id)) {
                $conversation_id = $conversation_id['UserConversation']['conversation_id'];
                if ($this->Message->addmessage($conversation_id, $this->Auth->user('id'), $this->request->data['body'])) {
                    $this->UserConversation->setread($this->Auth->user('id'), $conversation_id);
                    $this->UserConversation->setconversationunread($conversation_id, $this->Auth->user('id'));
                    
                    $returnObject['message'] = "Message sent successfully.";
                    $returnObject['data'] = $conversation_id;
                    $this->set(array(
                        'data' => $returnObject,
                        '_serialize' => 'data'
                    ));
                    return;
                } else {
                    throw new Exception('Failed to send message.', 403);
                }
            } else {
                throw new Exception('No such Conversation.', 403);
            }
        } else {//if any argument is not passed or not feteched properly
            throw new Exception('Some error occured. Please try again later.', 403);
        }
    }

    //load messages of a conversation
    public function loadmessages($conversation_id, $referece, $direction, $limit) {
        $this->viewClass = 'Json';
        if (!empty($conversation_id) && !empty($referece) && isset($direction) && !empty($limit)) {
            //fetch the required data                
            $user = $this->Auth->user('id');
            $network_id = $this->Session->read('CurrentNetwork');
            //check if user belongs to conversation            
            if ($this->UserConversation->getUserConversationField($user, $conversation_id, $network_id, 'status') === STATUS_ACTIVE) {
                //load the messages
                $messages = $this->Message->loadmessage($conversation_id, $referece, $direction, $limit);                
                $returnObject['message'] = "Messages loaded";
                $returnObject['data'] = $messages;
                $this->set(array(
                    'data' => $returnObject,
                    '_serialize' => 'data'
                ));
                return;
            } else {
                throw new Exception('Some error occured. Please try again later.', 403);
            }
        } else {
            throw new Exception('Missing Parameters.', 403);
        }
    }

//    public function viewconversation($type, $value) {
//        $this->viewClass = 'Json';
//        if (!empty($type) && !empty($value)) {
//            $user_id = $this->Auth->user('id');
//            $network_id = $this->Session->read('CurrentNetwork');
//            if ($type === 'user') {
//                $conversation_id = $this->UserConversation->getconversationid($id, $value, $network_id);
//            } else if ($type === 'group') {
//                $conversation_id = $this->Conversation->find('list', array(
//                    'conditions' => array('Conversation.group_id' => $value),
//                    'fields' => array('Conversation.conversation_id'),
//                    'limit' => 1
//                ));
//            }
//            if (!empty($conversation_id)) {
//                $user_conversation_object = $this->UserConversation->findByConversationIdAndUsername($conversation_id, $user_id, array('UserConversation.role', 'UserConversation.network_id'));
//                if ($user_conversation_object['UserConversation']['role'] == UserConversation_Role_Admin && $user_conversation_object['UserConversation']['network_id'] == $network_id) {
//                    $lastid = $this->Message->getlatestid();
//                    $result = $this->Message->loadmessage($conversation_id, $lastid, Direction_lessthan, 4);
//                    
//                    $returnObject['message'] = "Conversation found";
//                    $returnObject['data'] = $result;
//                    $this->set(array(
//                        'data' => $returnObject,
//                        '_serialize' => 'data'
//                    ));
//                    return;
//                } else {
//                    throw new Exception('No such conversations.', 403);
//                    $returnObject['code'] = 'error';
//                    $returnObject['message'] = "No such conversations.";
//                }
//            } else {
//                
//                $returnObject['message'] = "No Conversation found";
//                $this->set(array(
//                    'data' => $returnObject,
//                    '_serialize' => 'data'
//                ));
//                return;
//            }
//            return;
//        } else {
//            //if any argument is not passed or not feteched properly
//            throw new Exception('Missing Parameters.', 403);
//        }
//    }
    
    
}

?>
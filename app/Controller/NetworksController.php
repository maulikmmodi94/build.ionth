<?php

/**
 * @author Hardik Sondagar <hardik.sondagar@blogtard.com>
 * @name NetworksController.php
 * @abstract UserController contain main methods that related to User Module
 * @uses Userinfo This Controller uses Userinfo Model to store user's personal details
 * 
 */
App::uses('AppController', 'Controller');
App::uses('CakeEvent', 'Event');
App::uses('HttpSocket', 'Network/Http');

class NetworksController extends AppController {

    public $name = 'Networks';
    public $uses = array('Group', 'Data', 'UsersGroup', 'Group', 'Task', 'TaskAssignment', 'Network', 'UserNetwork', 'User', 'UserInfo', 'Plan', 'NetworkPlan', 'NetworkAccess');
    public $components = array('Security', 'Session', 'Cookie');
    public $networkExpirationTime = 2592000;
    public $status;
    public $message;
    public $returnData = array();
    public $accessList = array(
        ROLE_OWNER => array(
            'security', 'security_delete', 'delete', 'edit'
        ),
        ROLE_ADMIN => array(
            'remove'
        )
    );

    public function _getRemainingUsers() {
        return $this->Network->getRemainingUsers($this->Session->read('CurrentNetwork'));
    }
/**
 * method for redirecting user to its network
 * when user login then it directs to the networks/index/1
 * @param type $login
 */
    public function index() {
//        $this->Network->defaultConditions = array('Network.isDeleted' => NOT_DELETED,'Network.expires <= NOW()');
        $this->Session->delete('CurrentNetwork');
//        #it checks whether user has previous network_id saved.
//        if (!empty($login)) {
//            $net_id = Hash::extract($this->User->find('first', array(
//                            'conditions' => array('User.id' => $this->Auth->user('id')),
//                            'fields' => array('User.last_network')
//                        )), 'User.last_network');
//            $url = $this->Network->getNetworkField($net_id[0],'url');
//            if(!empty($url)){
//                $this->redirect('/networks/switchNetwork/' . $url . '/desks');
//            }
//        }
        $this->set('plans', $this->Plan->find('list'));
    }
 
    function index_demo() {
        
    }

    function beforeFilter() {

        $this->Auth->allow('defaultNetwork', 'getUsers', 'getNetworks', 'getGroups', 'getuser_stats', 'createNetworkMobile');



        if ($this->action == 'view') {

            $Networks = $this->_setNetworkList();
            /* Set URL as Index value */
            $Networks = Hash::combine($Networks, '{n}.url', '{n}');

            /* Set Current Network */
            if (!$this->request->params['pass']) {
                $this->redirect('/networks');
            }
            if (!empty($Networks[@$this->request->params['pass'][0]])) {
                $this->Session->write('CurrentNetwork', $Networks[$this->request->params['pass'][0]]['id']);
            } else {
                $this->Session->setFlash('Network could not be found.', 'default', array('class' => 'error message'));

                $this->redirect('/networks');
            }
        } else if ($this->action == 'edit') {
            $this->Security->unlockedActions = 'edit';
        } else if ($this->action == 'group_stats') {
            $this->Security->unlockedActions = 'group_stats';
        } else if ($this->action == 'network_stats') {
            $this->Security->unlockedActions = 'network_stats';
        } else if ($this->action == 'add') {
            $this->Security->unlockedActions = 'add';
        }else if ($this->action == 'delete') {
            $this->Security->unlockedActions = 'delete';
        }else if ($this->action == 'remove') {
            $this->Security->unlockedActions = 'remove';
        }else if ($this->action == 'leave_network') {
            $this->Security->unlockedActions = 'leave_network';
        }else if ($this->action == 'upgrade_plan') {
            $this->Security->unlockedActions = 'upgrade_plan';
        }
        
        $this->accessList[ROLE_OWNER] = array_merge($this->accessList[ROLE_OWNER], $this->accessList[ROLE_ADMIN]);

        if (in_array($this->action, $this->accessList[ROLE_OWNER])) {

            $user_id = $this->Auth->user('id');
            $network_id = $this->Session->read('CurrentNetwork');
            $role = $this->Network->UserNetwork->getUserNetworkField($user_id, $network_id, 'role');
          
            if (!in_array($this->action, $this->accessList[$role])) {
                throw new Exception('Not Authorized to access', 400);
            }
        }

        parent::beforeFilter();
    }

    /**
     * Returns the time the email verification token expires
     *
     * @return string
     */
    private function networkExpirationTime() {
        return date('Y-m-d H:i:s', time() + $this->networkExpirationTime);
    }

    public function add() {
//        $this->log($this->request->data);
            #set default parameter
//            if(isset($this->params['named']['user'])){
//             $this->request->data['Network']['name'] = $this->Session->read('Auth.User.UserInfo.first_name') . '\'s Workspace';
//            $this->request->data['Network']['return_to'] = "/desks";
//            }
            
        if ($this->request->is('Post')/* || isset($this->params['named']['user'])*/) {
            
            $this->request->data['NetworkPlan'][0]['plan_id'] = 1;
            if (!$this->Plan->exists($this->request->data['NetworkPlan'][0]['plan_id'])) {
                $this->Session->setFlash('Failed to create network.', 'default', array('class' => 'error message'));
                return $this->redirect('/networks');
            }
            $plan = $this->Plan->findById($this->request->data['NetworkPlan'][0]['plan_id']);
            $time_limit = date('Y-m-d H:i:s', strtotime("+" . $plan['Plan']['time_limit'] . " months", time()));
            $user_limit = $plan['Plan']['user_limit'];
            $storage_limit = $plan['Plan']['storage_limit'];

            $defaults = array(
                'Network' => array(
                    'created_by' => $this->Auth->user('id'),
                    'user_limit' => $user_limit,
                    'file_limit' => $storage_limit,
                    'status' => STATUS_ACTIVE,
                    'expires' => $time_limit
                //'expires' => $this->networkExpirationTime()
                ),
                'UserNetwork' => array(
                    array(
                        'user_id' => $this->Auth->user('id'),
                        'status' => STATUS_ACTIVE,
                        'role' => ROLE_OWNER,
                        'file_limit' => $storage_limit
                    )
                ),
                'NetworkPlan' => array(
                    array(
                        'start' => date('Y-m-d H:i:s', time()),
                        'end' => $time_limit
                    )
                )
            );


            $this->request->data['Network'] = array_merge($defaults['Network'], $this->request->data['Network']);
            $this->request->data['NetworkPlan'][0]['start'] = date('Y-m-d H:i:s', time());
            $this->request->data['NetworkPlan'][0]['end'] = $time_limit;
            $this->request->data = array_merge($defaults, $this->request->data);

            if ($this->Network->add($this->request->data)) {
                $this->Session->write('CurrentNetwork', $this->Network->id);
                
                #set last network you visited in database
                $this->User->updateAll(array('User.last_network'=> $this->Network->id),
                                        array('User.id'=> $this->Auth->user('id')));
//                $this->Cookie->write('CurrentNetworkName', $this->Network->getNetworkField($this->Network->id, 'url'), TRUE, 18144000);
//                $this->Cookie->write('CurrentNetworkId', $this->Network->id, TRUE, 18144000);
                $Event = new CakeEvent(
                        'Controller.Network.afterCreate', $this, array('user_id' => $this->Auth->user('id'), 'network_id' => $this->Network->getLastInsertId())
                );
                $this->getEventManager()->dispatch($Event);
                $this->Session->setFlash('Network created successfully.', 'default', array('class' => 'success message'));


                if (isset($this->request->data['Network']['return_to'])) {
                    if($this->request->data['Network']['return_to'] == 'model'){
                        $this->viewClass = 'Json';
                         $returnObject = array(
                             'message' => 'success',
                             'data' => '/networks/switchNetwork/' . $this->Network->getNetworkField($this->Network->id, 'url') . '/desks',
                             'info' => $this->Network->getNetworkField($this->Network->id, 'url')
                         );
                         $this->set(array(
                            'users' => $returnObject,
                            '_serialize' => 'users'
                            ));
                        return ;
                    }else{
                        return $this->redirect($this->request->data['Network']['return_to']);
                    }
                } else {
                    return $this->redirect('/networks');
                }
            } else 
                {
                $this->Session->setFlash('Failed to create network.', 'default', array('class' => 'error message'));
                return $this->redirect('/networks');
            }
        }
    }

    public function add_members() {
        
    }

    /**
     * Security Network Group
     * @author Hardik Sondagar
     * @abstract View/Add Securitu Group Network Access List
     * @param string $data[NetworkAccess][name] NetworkAccess name as request parameter
     * @param string $data[NetworkAccess][ip_address] NetworkAccess ip_addressas request parameter
     * @param string $data[NetworkAccess][role] NetworkAccess role as request parameter
     * @param string $data[NetworkAccess][user_id] NetworkAccess user_id if role=='special' as request parameter
     * @param string $data[User][password] user's password
     * @access Only network owner can access this function with password authentication
     * @since 20/09/14
     * @modified 25/09/14
     */
    public function security() {

        $current_network_id = $this->Session->read('CurrentNetwork');
        $auth_user_id = $this->Auth->user('id');

        /* allow only owner to access this function */
        if ($this->Network->UserNetwork->getUserNetworkField($auth_user_id, $current_network_id, 'role') != 'owner') {
            $this->Session->setFlash('You not autorized to access security group.', 'default', array('class' => 'error message'));
            return $this->redirect('/Networks/');
        }


        /* If request is post then add security group */
        if ($this->request->is('post')) {


            /* Pre Checking
             * 
             * If Role set or not
             * if role=='special' then check is user part of network?
             * 
             * 
             */
            $this->request->data['NetworkAccess']['network_id'] = $current_network_id;

            /* if access role is ROLE_SPECIAL then check is entered user part of a network? */

            if (isset($this->request->data['NetworkAccess']['role']) && $this->request->data['NetworkAccess']['role'] == ROLE_SPECIAL) {


                $user_id = (isset($this->request->data['NetworkAccess']['user_id'])) ? $this->request->data['NetworkAccess']['user_id'] : NULL;

                if (is_null($user_id) || !$this->Network->UserNetwork->isNetworkUsers($current_network_id, array($user_id))) {

                    $this->Session->setFlash('Failed to add. User not part of network.', 'default', array('class' => 'error message'));
                    goto error;
                }
            }


            if ($this->Network->NetworkAccess->save($this->request->data)) {

                $this->Session->setFlash('Security group added.', 'default', array('class' => 'success message'));
                return $this->redirect('/networks/security');
            } else {

                $this->Session->setFlash('Failed to add security group.', 'default', array('class' => 'error message'));
            }

            error:
            /* set flat return_from with value 'add' to check on view side that function returned from Security Add Group ( POST request ) */
            $this->User->id = null;
        }

        /* Set network user's list for adding user in security group when role==ROLE_SPECIAL */
        $fields = array('UserInfo.user_id', 'CONCAT(UserInfo.first_name," ",UserInfo.last_name) AS name');
        $conditions = array(
            'UserNetwork.network_id' => $current_network_id
        );

        $user_networks = $this->Network->UserNetwork->find('all', array(
            'conditions' => $conditions,
            'contain' => array('UserInfo'),
            'fields' => $fields
        ));

        $users = Hash::combine($user_networks, '{n}.UserInfo.user_id', '{n}.0.name');
        //Sort the array accoring to alphabetical values and maintain key values
        arsort($users, SORT_FLAG_CASE);
        $users = array_reverse($users, TRUE);

        $this->set('users', $users);
        $this->set('roles', array(ROLE_USER => 'All Users', ROLE_ADMIN => 'All Admins', ROLE_SPECIAL => 'Select User'));
        $this->set('current_ip_address', $this->request->clientIp());


        /* Set all network access groups */
        $this->set('NetworkAccess', $this->Network->NetworkAccess->findAllByNetworkId($this->Session->read('CurrentNetwork')));
    }

    public function security_delete($id) {

        $current_network_id = $this->Session->read('CurrentNetwork');
        $auth_user_id = $this->Auth->user('id');

        if ($this->Network->UserNetwork->getUserNetworkField($auth_user_id, $current_network_id, 'role') != 'owner') {
            $this->Session->setFlash('You not autorized to access security group.', 'default', array('class' => 'error message'));
            return $this->redirect('/Networks/');
        }
        $this->NetworkAccess->id = $id;
        $this->request->allowMethod('post', 'delete');
        if (!$this->NetworkAccess->exists()) {
            throw new NotFoundException(__('Invalid Security Group'));
        }
        if ($this->NetworkAccess->delete()) {
            $this->Session->setFlash(__('Security group has been deleted.'));
        } else {
            $this->Session->setFlash(__('Security group could not be deleted. Please, try again.'));
        }
        return $this->redirect(array('action' => 'security'));
    }

    /**
     * Delete Network
     *
     * @author Hardik Sondagar
     * @abstract Delete Network ( Critical Function )
     * @param integer $id NetworkId as post parameter 
     * @since 11/09/14
     * @modified 12/09/14
     */
    public function delete() {


        $this->request->allowMethod('post', 'put');
        $network_id = $this->request->data['network_id'];
        /* Get AuthUserId, Current Network's id and Role of AuthUser in CurrentNetwork */
        $user_id = $this->Auth->user('id');
        // $network_id = $this->request->data['Network']['id'];
        $role = $this->Network->UserNetwork->getUserNetworkField($user_id, $network_id, 'role');

        /* Allowed roles to remove user */
        $allowed_role = array(ROLE_OWNER);

        /* Checking for null and allowed role */
        if (is_null($network_id) || !in_array($role, $allowed_role)) {
            $this->Session->setFlash('You are not authorized to delete network.');
            return $this->redirect($this->referer());
        }


        /* Soft detele network */
        $conditions = array(
            'Network.' . $this->Network->primaryKey => $network_id
        );

        if ($this->Network->soft_delete($conditions, array(), 0)) {
            $this->Session->delete('CurrentNetwork');
            $this->Session->setFlash('Network deleted successfully.');
            return $this->redirect('/networks/index/1');
        } else {
            $this->Session->setFlash('Failed to delete network.');
            return $this->redirect($this->referer());
        }
        /*
         * Saved for SoftDeleting all network related elements
          $this->Network->bindModel(
          array('hasMany' => array(
          'UserNetwork' => array(
          'className' => 'UserNetwork',
          'conditions' => array('isDeleted' => NOT_DELETED)
          ),
          'Task' => array(
          'className' => 'Task',
          'conditions' => array('isDeleted' => NOT_DELETED)
          ),
          'NetworkPlan' => array(
          'className' => 'NetworkPlan',
          'conditions' => array('isDeleted' => NOT_DELETED)
          ),
          'Group' => array(
          'className' => 'Group',
          'conditions' => array('isDeleted' => NOT_DELETED)
          ),
          'Conversation' => array(
          'className' => 'Conversation',
          'conditions' => array('isDeleted' => NOT_DELETED)
          ),
          'Doc' => array(
          'className' => 'Doc',
          'conditions' => array('isDeleted' => NOT_DELETED)
          ),
          'Log' => array(
          'className' => 'Log',
          'conditions' => array('isDeleted' => NOT_DELETED)
          )
          )
          )
          );

          $Network = $this->Network->find('first', array(
          'conditions' => array(
          'Network.id' => $this->Network->id
          ),
          'contain' => array(
          'Task' => array(
          'fields' => array('id')
          ),
          'Group' => array(
          'fields' => array('id')
          ),
          'Conversation' => array(
          'fields' => array('id')
          ),
          'Doc' => array(
          'fields' => array('id')
          ),
          'Log' => array(
          'fields' => array('id')
          ),
          )
          )
          );


          $dataSource = $this->Network->getDataSource();
          $dataSource->begin();
          $return_status = true;
          try {

          $groups = Hash::extract($Network, 'Group.{n}.id');
          $return_status = $return_status && $this->Network->Group->groupDelete($groups, $network_id);
          debug($return_status);
          $tasks = Hash::extract($Network, 'Task.{n}.id');
          $return_status = $return_status && $this->Network->Task->TaskDelete($tasks, $user_id);

          $conversations = Hash::extract($Network, 'Conversation.{n}.id');
          $return_status = $return_status && $this->Network->Conversation->conversationDelete($conversations);

          $doc = Hash::extract($Network, 'Doc.{n}.id');
          $return_status = $return_status && $this->Network->Doc->deleteDoc($doc);

          $logs = Hash::extract($Network, 'Log.{n}.id');
          $return_status = $return_status && $this->Network->Log->deleteLog($logs);

          $return_status = $return_status && $this->Network->deleteNetwork($network_id);


          if (!$return_status) {
          throw new Exception();
          }
          $dataSource->commit();
          $this->Session->setFlash('Network deleted successfully.');
          return $this->redirect('/networks');
          } catch (Exception $e) {
          $dataSource->rollback();
          $this->Session->setFlash('Failed to delete network.');
          return $this->redirect($this->referer());
          } */
    }

    public function edit() {

        $this->viewClass = 'Json';


        /* Set NetworkList to Session */
        /* Set URL as Index value */
        $Networks = $this->Session->read('Networks');
        $CurrentNetwork = $this->Session->read('CurrentNetwork');

        if (!$this->UserNetwork->isAdmin($this->Auth->user('id'), $Networks[$CurrentNetwork]['id'])) {
            throw new Exception('Not authorized', 444);
        } else {
            $this->Network->id = $Networks[$CurrentNetwork]['id'];
            $this->Network->set('name', @$this->request->data['Network']['name']);

            if ($this->Network->validates() && $this->Network->saveField('name', $this->request->data['Network']['name'])) {
                $Event = new CakeEvent(
                        'Controller.Network.afterEdit', $this, array('user_id' => $this->Auth->user('id'), 'network_id' => $CurrentNetwork)
                );
                $this->getEventManager()->dispatch($Event);

                $this->Session->write('Networks.' . $CurrentNetwork . '.name', $this->request->data['Network']['name']);
//                $returnObject = array(
//                    'data' => array('networks' => $this->Session->read('Networks'), 'currentnetwork' => $CurrentNetwork),
//                    'message' => 'Network details updated successfully.',
//                    'status' => 'success'
//                );
                $returnObject = array(
                    'data' => $this->Network->findById($CurrentNetwork),
                    'currentnetwork' => $CurrentNetwork,
                    'message' => 'Network details updated successfully.',
                    'status' => 'success'
                );

                $this->set(array(
                    'network' => $returnObject,
                    '_serialize' => 'network'
                ));
                return;
            } else {
                throw new Exception('Failed to update network details.', 400);
            }
        }
    }

    public function switchNetwork($value = NULL, $return_to_controller = NULL, $return_to = NULL) {

        $field = 'url';
        /* Set NetworkList to Session */
        $Networks = $this->_setNetworkList();

        /* Set URL as Index value */
        $Networks = Hash::combine($Networks, '{n}.' . $field, '{n}');

        /* Set Current Network and its cookie*/
        if (!empty($Networks[$value])) {
            $this->Session->write('CurrentNetwork', $Networks[$value]['id']);
            $this->User->updateAll(array('User.last_network'=> $Networks[$value]['id']),
                                        array('User.id'=> $this->Auth->user('id')));
//            $this->Cookie->write('CurrentNetworkName', $Networks[$value]['url'], TRUE, 3600);
//            $this->Cookie->write('CurrentNetworkId', $Networks[$value]['id'], TRUE, 3600);
        }
        if (is_null($return_to_controller)) {
            $this->redirect('/networks/view/' . $value);
        } else {
            $this->redirect('/' . $return_to_controller . '/' . $return_to);
        }
    }

    public function view($url = null) {
        
    }

    public function upgrade_plan() {
        $this->viewClass = 'Json';
        $this->request->allowMethod('post');
        $user_id = $this->Auth->user('id');
        $current_network = $this->Session->read('CurrentNetwork');
        $network_owner = $this->Network->getNetworkField($current_network, 'created_by');
        $network_name = $this->Network->getNetworkField($current_network, 'name');
        if($network_owner != $user_id)
            throw new UnauthorizedException("Only Network Owners can are allowed to upgrade plan. "
                    . "Please contact your Workspace owner if you want you plan to be upgraded");
        
        CakeResque::enqueue('default', 'NetworkShell', array('networkPlanUpgradeRequest',array(
                'owner_id' => $user_id,
                'network_id' => $current_network,
                'network_name' => $network_name,
                'owner_email' => $this->Auth->user('email'),
                'owner_name' => $this->Auth->user('UserInfo.name'),
            )));
//        ,true
//        ));
        $returnObject = array(
            'currentnetwork' => $current_network,
            'message' => 'Network Upgrade Request Success.',
            'status' => 'success'
        );

        $this->set(array(
            'network' => $returnObject,
            '_serialize' => 'network'
        ));
        return;
    }    
    
    public function network_stats() {
        $this->viewClass = 'Json';
//        debug('hii');
//        $network_id = 5;
        $type = NULL;
        $user_id = $this->Auth->User('id');
//        debug($user_id);
//        $user_rolzz = $this->User->userRole($user_id);
//        debug('hi');
//        debug($user_rolzz);
        $user_rol = Hash::extract($this->User->find('first', array(
                            'conditions' => array('User.id' => $user_id),
                            'fields' => array('User.role')
                        )), 'User.role');
//        debug($user_rol);
//        if (is_null($network_id)) {
        $network_id = $this->Session->read('CurrentNetwork');
//            debug($network_id);
//          $uzz =   $this->UserNetwork->getUsers($network_id, $user_id);
//          debug($uzz);
//              debug($user_role);
        $user_network_role = Hash::extract($this->UserNetwork->getUserRole($network_id, $user_id), '{n}.UserNetwork.role');
//             debug($user_network_role);
        $returnObject = array(
            'message' => 'You are not authorized to perform this action.'
        );
//             if(in_array('admin', $user_network_role) || in_array('owner', $user_network_role) || in_array('admin', $user_rol) || in_array('owner', $user_rol)){
//        } else {
//            $Networks = $this->Session->read('Networks');
//            if (empty($Networks[$network_id])) {
//                $returnObject['status'] = 'error';
//                $returnObject['message'] = 'Network Not found';
//                $this->set('returnObject', $returnObject);
////                return $this->render('/Elements/ajaxreturn');
//            }
//        }
        $UserNetworks = $this->UserNetwork->find('all', array('conditions' => array('UserNetwork.network_id' => $network_id), 'contain' => array('UserInfo')));
//        debug($UserNetworks);
//        $this->set('UserNetworks', $UserNetworks);
//        $returnObject['data'] = array('user_networks' => $UserNetworks, 'isAdminNetwork' => $this->UserNetwork->isAdmin($user_id, $network_id));
//        $returnObject['status'] = 'success';
//        debug($returnObject);

        /* aa data parametcer thi aavse
         * for that there are 2 thing send from frontned
         * 1) array of getUserNetwork.
         * 2) network_id
         *          */
        /* total task of network */
//        debug($network_id);
        $network_taskid = $this->Task->taskCount($network_id);
//        debug($network_taskid);
        $network_taskid = Hash::extract($network_taskid, '{n}.Task.id');
//        debug($network_taskid);
//        $task = $this->Task->taskCountById($task_id);
//        debug($task);
        $productivity = $this->productivity_percentage($network_taskid);
        $productivity['total_task'] = count($network_taskid);
        $productivity['Network_id'] = $network_id;
//    debug($productivity);
        /* total user in network and their personal productivity */
        $user_ids = Hash::extract($UserNetworks, '{n}.UserNetwork.user_id');
//    debug($user_ids);
        $return = array();
//    $type = 'net_stats';
        foreach ($user_ids as $key => $value) {
//        debug($value);

            $arr = $this->user_stats($value, $network_id);
            $arr['User_id'] = $value;
//        debug($arr);
            array_push($return, $arr);
        }

//    debug($return);
        function cmp($a, $b) {
            if ($a['data']['productivity'] == $b['data']['productivity'])
                return 0;
            return ( $a['data']['productivity'] > $b['data']['productivity'] ) ? -1 : 1;
        }

        uasort($return, 'cmp');
//    debug($return);
        $ret = array();
        foreach ($return as $key => $value) {
            array_push($ret, $value);
        }

//    debug($ret);
//    debug($z);
//    $returnObjectz = array_merge($productivity, $return);
        $returnObject = array(
            'current_Network' => $productivity,
            'Top_User_Network' => $ret
        );
//             }
        $this->set(array(
            'network' => $returnObject,
            '_serialize' => 'network'
        ));

//    debug($returnObject);
    }
    
    /**
     * it return the info. related to $group_id and groups related to current network
     * @param type $group_id
     */
    public function group_stats($group_id = null) {
//        debug('hi');
        $this->viewClass = 'Json';
//        $user_id = $this->Auth->User('id');
        $network_id = $this->Session->read('CurrentNetwork');
        $groups = array_unique($this->Group->UsersGroup->getNetworkGroups($network_id));
//        debug($groups);
//        $groups = $this->Group->getGroups($groups, $network_id);
//        debug($groups);
//        $returnObject['status'] = 'success';
//        $group['data'] = array('groups' => $groups);
//      $group_id = Hash::extract($group, 'data.groups.{n}.Group.id');  
//        debug($group);
        /* aa data parameter thi aavse */
//        debug($network_id);
        $group = array();
        array_push($group, $group_id);
        $group = array_merge($group, $groups);
//        debug($group);
        $return = array();
        foreach ($group as $key => $value) {

//            debug($value);
            $task_assigned = $this->TaskAssignment->taskAssignedToGroup($value);
//            debug($task_assigned);
            $task_id = array_unique(Hash::extract($task_assigned, '{n}.TaskAssignment.task_id'));
//            debug($task_id);
            $productivity = $this->productivity_percentage($task_id);
            $returnObject = array(
                'message' => 'Group stats',
                'Group_id' => $value,
                'total_task' => count($task_id),
                'data' => $productivity
            );

//                    debug($returnObject);       
            array_push($return, $returnObject);
        }
        $current_group = $return[0];
        array_shift($return);

//        debug($return);
        function cmpr($a, $b) {
            if ($a['data']['productivity'] == $b['data']['productivity'])
                return 0;
            return ( $a['data']['productivity'] > $b['data']['productivity'] ) ? -1 : 1;
        }

        uasort($return, 'cmpr');
        $ret = array();
        foreach ($return as $key => $value) {
            array_push($ret, $value);
        }

//    $returnObject = array(
//                        'task_complete' => $task_complete,
//    $return = array_merge($current_group, $return);
//        debug($return);
//           $return = array();
//        $task_assigned = $this->TaskAssignment->taskAssignedToGroup($group_id);
////        debug($task_assigned);
//        $task_id = array_unique(Hash::extract($task_assigned, '{n}.TaskAssignment.task_id'));
//    debug($task_id);
////    debug(count($task_id));
////        $arr = array();
////        foreach ($task_id as $key => $value) {
////            debug($value);
//           $productivity = $this->productivity_percentage($task_id);
////           array_push($arr, $productivity);
////        }
        $returnObject = array(
            'current_Group' => $current_group,
            'Top_groups' => $ret
        );
        $this->set(array(
            'network' => $returnObject,
            '_serialize' => 'network'
        ));
//                    debug($return);
    }

    public function user_stats($user_id = null, $network_id = null) {
//        debug('hi');
        $this->viewClass = 'Json';
        if (!is_numeric($user_id)) {
            $user_id = $this->Auth->User('id');
        }
        if (!isset($network_id)) {
            $network_id = $this->Session->read('CurrentNetwork');
        }
//         $network_id = $this->Session->read('CurrentNetwork');
//         if(!isset($type)){
        $task_created = $this->Task->taskCreatedByUser($user_id, $network_id);
        $task_assigned = $this->TaskAssignment->taskAssigned($user_id, $network_id);
//         }else{
//             $task_created = $this->Task->taskCreatedByUser($user_id);
//             $task_assigned = $this->TaskAssignment->taskAssigned($user_id);
//         }
//        $task_created = $this->Task->taskCreatedByUser($user_id);
//        debug(count($task_created));

        $task_created = Hash::extract($task_created, '{n}.Task.id');
//        debug($task_created);
//        $task_assigned = $this->TaskAssignment->taskAssigned($user_id);
//        debug(count($task_assigned));
//        debug($task_assigned);
        $task_assigned = Hash::extract($task_assigned, '{n}.TaskAssignment.task_id');
//        debug($task_assigned);
        $task_total = array_unique(array_merge($task_created, $task_assigned));
//        debug($task_total);
//        debug(count($task_total));
//        $task = $this->Task->taskCountById($task_total);
//        debug(count($task));
//        debug($task);
        $productivity = $this->productivity_percentage($task_total);
        $returnObject = array(
            'message' => 'User stats',
            'task_created' => count($task_created),
            'task_assigned' => count($task_assigned),
            'task_total' => count($task_total),
            'data' => $productivity
        );
//                    $this->set('returnObject', $returnObject);
//        debug($returnObject);
        $this->set(array(
            'network' => $returnObject,
            '_serialize' => 'network'
        ));
//         $this->set(array(
//            'data' => $returnObject,
//            '_serialize' => 'network'
//        ));
        return $returnObject;
    }

    public function productivity_percentage($task_id = array()) {
//        debug('hii');
        $task_acomplish_imp = NULL;
        $task_acomplish_notimp = NULL;
        $task_missed_notimp = NULL;
        $task_missed_imp = NULL;
        $task_notcomplete = NULL;
        $task_complete = NULL;
        $time = NULL;
        $tm = 0;
        $d = 0;
        $h = 0;
        $m = 0;
        $productivity = 0;
        $delay_message = NULL;
        $task = $this->Task->taskCountById($task_id);
        if ($task) {
//        debug($task);
//    $task = $this->Task->taskCount();
//    debug($task);
//    debug(count($task));
//    $task_notcomplete = $this->Task->taskNotcomplte(0);
//    debug(count($task_notcomplete));
//    $task = Hash::extract($task, '{n}.Task');
//    debug($task);
//    debug($task);
//    $arr = array_count_values($task);
//    debug($arr);
//    $userid = array();
//    foreach ($arr as $key => $value) {
////        if($value > 30){
////            array_push($userid, $key);
////        }
//    }
////    debug($userid);
//    $array = array();
//    foreach ($userid as $key => $value) {
//        
////        debug($value);
//    $user = $this->Task->getUser($value);
////    debug($user);
//    $ab = Hash::extract($user, '{n}.User');
////    debug($ab);
//            array_push($array, $ab);
//    }
//    debug($array);
//    $arr = array();
//    debug($task['end_date']);
//    $endArry = array();

            foreach ($task as $key => $value) {
//                debug($value);
                if ($value['Task']['is_completed'] == 1 && $value['Task']['completed'] != NULL) {
                    $task_complete++;
//        debug(count($task));
////        debug(count($task));
//        debug(count($value));
//        debug($value['Task']['end_date']);
                    $end = strtotime($value['Task']['end_date']);
//        debug($end);
//        debug($value['Task']['completed']);
                    $completed = strtotime($value['Task']['completed']);
//        debug($completed);
//        $time = $completed->diff($end);
//        debug($time);
//        $t = round($completed - $end/3600);
//        debug($time);
//        debug($t);
//        $time = $time + $t;
                    $end_date = new DateTime($value['Task']['end_date']);
                    $completed_date = new DateTime($value['Task']['completed']);
                    $t = $completed_date->diff($end_date);
//    debug($t);
//    if($key == 0){
//    array_push($time, $t->m, $t->d, $t->h, $t->i);
//    $time[0] = 0;$time[1] = 0;$time[2] = 0;$time[3] = 0;
//    debug($time);
//    }
//        debug(date("Y-m-d h:m:s", $completed));
//        debug(date("Y-m-d h:m:s", $end));
//        $complt = $value['Task']['completed'];
//        $ed = $value['Task']['end_date'];
//            $time = $value['Task']['completed']->diff($value['Task']['end_date']);
//            debug($time->format('%Y-%m-%d %H:%i:%s'));
//        debug($value['Task']['priority']);
//        debug($completed);
                    $time = ($t->m) * 30 * 24 * 60 + ($t->d) * 24 * 60 + ($t->h) * 60 + ($t->i);
//        debug($time);
                    if (($end > $completed) && ($value['Task']['priority'] == 1)) {
                        $task_acomplish_imp++;
//                $t->m = -$t->m;$t->d = -$t->d;$t->h = -$t->h;$t->i = -$t->i;
                        $time = -$time;
                    } elseif (($end > $completed) && ($value['Task']['priority'] == 0)) {
                        $task_acomplish_notimp++;
//                $t->m = -$t->m;$t->d = -$t->d;$t->h = -$t->h;$t->i = -$t->i;
//                debug($t);
                        $time = -$time;
                    } elseif (($end < $completed) && ($value['Task']['priority'] == 1)) {
                        $task_missed_imp++;
                    } else {
                        $task_missed_notimp++;
                    }
//        $time[0] = $time[0] + $t->m;//total month
//        $time[1] = $time[1] + $t->d;//total days
//        $time[2] = $time[2] + $t->h;//total hours
//        $time[3] = $time[3] + $t->i;//total min
                    $tm = $tm + $time;
                } else {
                    $task_notcomplete++;
                }
            }

//    debug($task_notcomplete);
//            debug($task_acomplish_imp);
//            debug($task_acomplish_notimp);
//            debug($task_missed_imp);
//            debug($task_missed_notimp);
//        debug($time);
            $time = 'after';

            $tasks = (4 * $task_acomplish_imp) + (2 * $task_acomplish_notimp) + ($task_missed_notimp / 2) + ($task_missed_imp / 4);
//            debug($tasks);
            $productivity = floor(($tasks * 100) / ($tasks + $task_notcomplete));
            if ($tm < 0) {
                $time = 'before';
                $tm = -$tm;
            }
            if ($task_complete) {
                $tm = ($tm / $task_complete);
            }
            $d = floor($tm / 1440);
            $h = floor(($tm - $d * 1440) / 60);
            $m = floor($tm - ($d * 1440) - ($h * 60));

            if ($d == 0) {
                $delay_message = $time . " " . $h . ":" . $m . " hours";
                if ($h == 0) {
                    $delay_message = $time . " " . $m . " minutes";
                }
            } else {
                $delay_message = $time . " " . $d . " days";
            }
//        debug($time); 
//        debug($d);
//        debug($h);
//        debug($m);
//            debug($tm);
//            $time = ($time)/($task_complete);
//            debug($time);
//            $time = date("h:m:s", $time);
//            debug($productivity);
//    debug(count($arr));
//    debug(count($endArry));
//    debug($arr);
//    
//    debug($endArry);
        }
        $returnObject = array(
            'task_complete' => $task_complete,
            'task_notcomplete' => $task_notcomplete,
            'task_acomplish_imp' => $task_acomplish_imp,
            'task_acomplish_notimp' => $task_acomplish_notimp,
            'task_missed_imp' => $task_missed_imp,
            'task_missed_notimp' => $task_missed_notimp,
            'productivity' => floor($productivity),
            'Avg_delay' => $delay_message
        );

//                    debug($returnObject);
        return $returnObject;
    }

    public function assignRole($user_id = null, $role = 'user') {

        $this->viewClass = 'Json';
        $authUser = $this->Auth->user('id');
        $network_id = $this->Session->read('CurrentNetwork');

        $returnObject;

        $isAdmin = $this->Network->UserNetwork->isAdmin($authUser, $network_id);

        if ($isAdmin == 2) {
            $allowed_role = array(ROLE_ADMIN, ROLE_USER);
        } else if ($isAdmin == 1) {
            $allowed_role = array(ROLE_USER);
        } else {
            
        }


        if (!isset($user_id) || !isset($role)) {
            $returnObject['status'] = "error";
            $returnObject['message'] = "Some error occured. Please try again later.";
        } else if ($authUser == $user_id) {
            $returnObject['status'] = "error";
            $returnObject['message'] = "You are not authorized to change role.";
        } else if (!in_array($role, $allowed_role)) {
            $returnObject['status'] = "error";
            $returnObject['message'] = "You are not authorized to change role.";
        } else {
            if ($isAdmin == 2) {
                if ($role == ROLE_ADMIN && $this->Network->UserNetwork->getAdminCount($network_id) >= NETWORK_ADMIN_LIMIT) {
                    $returnObject['status'] = "error";
                    $returnObject['message'] = "Maximum 3 admins are allowed.";
                } else if ($this->Network->UserNetwork->assignRole($user_id, $network_id, $role)) {
                    $Event = new CakeEvent(
                            'Controller.Network.afterAssignRole', $this, array(
                        'user_id' => $user_id,
                        'network_id' => $network_id,
                        'role' => $role,
                        'owner' => $authUser
                            )
                    );
                    $this->getEventManager()->dispatch($Event);

                    $Networks = $this->Session->read('Networks');
                    CakeResque::enqueue('default', 'NetworkShell', array('assignRole', array(
                            'Network' => array(
                                'name' => $Networks[$network_id]['name']
                            ),
                            'user_id' => $user_id,
                            'role' => $role,
                            'assignBy' => $this->Auth->user('UserInfo.name')
                    )));

                    $returnObject['status'] = "success";
                    $returnObject['message'] = "Role assigned successfully.";
                } else {
                    $returnObject['status'] = "error";
                    $returnObject['message'] = "Some error occured. Please try again later.";
                }
            } else {
                $returnObject['status'] = "error";
                $returnObject['message'] = "You not authorized to change role.";
            }
        }

        $this->set(array(
            'network' => $returnObject,
            '_serialize' => 'network'
        ));
    }

    /**
     * Remove User from Network
     *
     * @author Hardik Sondagar
     * @abstract Remove User from Network
     * @param integer $type user's id of user to be remove.
     * @return boolean
     * @since 08/09/14
     */
    public function remove() {

        $this->viewClass = 'Json';
        $user_id = $this->request->data('user_id');
        /* Get AuthUserId, Current Network's id and Role of AuthUser in CurrentNetwork */
        $authUser = $this->Auth->user('id');
        $network_id = $this->Session->read('CurrentNetwork');
        $role = $this->Network->UserNetwork->getUserNetworkField($authUser, $network_id, 'role');
        
        /* Allowed roles to remove user */
        $allowed_role = array(ROLE_OWNER, ROLE_ADMIN);

        /**
         * role 'admin'  -> can remove users only
         * role 'owner'  -> can remove users and admins
         * if AuthUser if owner then give access to remove admins 
         */
        $allow_admin_remove = ($role == ROLE_OWNER ? true : false);

        $returnObject;


        if (is_null($user_id) || !in_array($role, $allowed_role)) { /* Checking for null and allowed role */

            throw new Exception('You are not authorized to remove user.', 400);
        } else if ($authUser == $user_id) { /* prevent user to removing himself from network */

            throw new Exception('You cannnot remove yourself.', 400);
        } else {
            /* Removing User form Network */
            if ($this->Network->UserNetwork->remove($user_id, $network_id, $authUser, $allow_admin_remove) && $this->Network->UserNetwork->getAffectedRows() > 0) {
                /* On successfully removing dispatch event to perform post operation live remove user from Group,Conversation,Task,Files etc */
                $Event = new CakeEvent(
                        'Controller.Network.afterMemberRemove', $this, array('user_id' => $user_id, 'network_id' => $network_id, 'admin' => $authUser)
                );
                $this->getEventManager()->dispatch($Event);


                $returnObject['status'] = "success";
                $returnObject['message'] = "Member removed successfully.";
            } else {

                throw new Exception('Some error occured. Please try again later.', 400);
            }
        }

        /* Send response */
        $this->set(array(
            'network' => $returnObject,
            '_serialize' => 'network'
        ));
    }

    
    
    
    
    /**
     * Allow a user to leave a network
     * @param integer $type user's id of user to be remove.
     * @return boolean
     * @since 13-Aug-2015
     * @author Manish M Demblani
     */
    public function leave_network() {

        $user_id = $this->request->data('user_id');
        
        $authUser = $this->Auth->user('id');
        $network_id = $this->Session->read('CurrentNetwork');
        
        if (is_null($user_id)) {
            throw new Exception(__('Missing Parameter'),400);
        }
        
        $Networks = $this->_setNetworkList();
        //Check if user is a part of that network
        if(empty($Networks[$network_id]))
            throw new Exception(__('You are not authorized to access this Network'),403);
        
        //Following check performed for security measures
        if($authUser != $user_id) 
            throw new Exception(__('You are not authorized to perform this action'),403);
        
        if($Networks[$network_id]['created_by'] == $user_id)
            throw new Exception(__('Network owner cannot leave network. Only delete Network'),403);
        
        /* Removing User form Network */
        if ($this->Network->UserNetwork->leavenetwork($network_id, $user_id)) {
            /* On successfully removing dispatch event to perform post operation 
             * live remove user from Group, Conversation, Task, Files etc */
            $Event = new CakeEvent(
                    'Controller.Network.afterMemberLeave', $this, array('user_id' => $user_id, 'network_id' => $network_id, 'admin' => $user_id)
            );
            $this->getEventManager()->dispatch($Event);
            $this->Session->delete('CurrentNetwork');
            $this->Session->setFlash('You have successfully left the Network.');
            return $this->redirect(array('controller' => 'networks', 'action' => 'index'));
        } else {
                throw new Exception(__('Failed to leave network. Try Again Later'), 400);     
        }       
    }    
    
    
    
    
    protected function hodor() {

        $HttpSocket = new HttpSocket();
        $this->loadModel('User');
        /* to full wWhere database */
        for ($i = 0; $i < 1500; $i++) {
            $results = $HttpSocket->get('http://wwhere.co/');
            preg_match('/\<input type="hidden" name="token" value="(.*)"\>/', $results->body, $title);

            $email = $this->User->generateToken() . '@iam.sherlocked';
            $dati = array('email' => $email, 'select_os' => 'checked', 'token' => $title[1]);
            $results = $HttpSocket->post('http://wwhere.co/mailer_signup.php', $dati);
        }
    }

    /*     * **** MOBILE APP FUNCTIONS   ******************** */

    /**
     * Function returns the default network of a user
     * 
     * anupama@blogtard.com
     * 
     * @return type 
     */
    public function defaultNetwork() {

        $this->viewClass = 'Json';
        $this->request->onlyAllow('get');

        //variables definition

        if (!isset($this->request->query['user'])) {

            $returnObject['status'] = "error";
            $returnObject['err_msg'] = 'Missing parameter';
            $this->set(array(
                'data' => $returnObject,
                '_serialize' => 'data'
            ));
            return;
        }
        $user_id = $this->request->query['user'];

        $network_id = $this->UserNetwork->getDefaultNetwork($user_id);

        $returnObject['status'] = 'success';
        $returnObject['data'] = $network_id;

        $this->set(array(
            'data' => $returnObject,
            '_serialize' => 'data'
        ));
    }

    /**
     * Retreive all the users of a network in json format
     * 
     * anupama@blogtard.com
     * 
     * @return json
     */
    public function getUsers() {

        $this->viewClass = 'Json';
        $this->request->onlyAllow('get');

        //MISSING PARAMETERS ERROR
        if (!isset($this->request->query['user']) && !isset($this->request->query['network'])) {

            $this->errMissingParameter();

            return;
        }

        $user_id = $this->request->query['user'];

        $network_id = $this->request->query['network'];

        //CHECK AUTHENTICATION OF USER
        //
            
        //LIST OF USER ID'S FROM USER NETWORK
        $list_users_ids = $this->UserNetwork->find(
                'list', array(
            'conditions' => array('UserNetwork.network_id' => $network_id),
            'fields' => array('UserNetwork.user_id')
        ));

        $list_users = $this->UserInfo->view($list_users_ids);


        $returnObject['status'] = 'success';
        $returnObject['data'] = $list_users;

        $this->set(array(
            'data' => $returnObject,
            '_serialize' => 'data'
        ));
    }

    /**
     * List of networks of a user in json
     * 
     * 
     */
    public function getNetworks() {

        $this->setJson();

        $this->setTypeGet();

        if (!$this->queryHas('user')) {

            $this->errMissingParameter();

            return;
        }

        $user_id = $this->getQueryParam('user');

        $list_networks = $this->UserNetwork->getUserNetworks($user_id);

        $returnObject['status'] = 'success';

        $returnObject['data'] = $list_networks;

        $returnObject['data']['count'] = sizeof($list_networks);


        $this->set(array(
            'data' => $returnObject,
            '_serialize' => 'data'
        ));
    }

    /**
     * Returns the list of a user's group in a network
     * 
     */
    public function getGroups() {

        $Q_PARAM_USER = 'user';

        $Q_PARAM_NETWORK = 'network';

        $this->setJson();

        $this->setTypeGet();

        if (!$this->queryHas($Q_PARAM_USER) || !($this->queryHas($Q_PARAM_NETWORK))) {

            $this->errMissingParameter();

            return;
        }

        $user_id = $this->getQueryParam($Q_PARAM_USER);

        $network_id = $this->getQueryParam($Q_PARAM_NETWORK);

        //FETCHING RESULT

        $list_groups_ids = $this->Group->UsersGroup->getActiveGroups($user_id, $network_id);

        $groups = $this->Group->getGroups($list_groups_ids, $network_id);
        //RETURN OBJECT

        $returnObject['status'] = 'success';

        $returnObject['data'] = array('groups' => $groups);

        $this->set(array(
            'data' => $returnObject,
            '_serialize' => 'data'
        ));
    }

    /**
     * Returns personal user statistics
     * @param type $user_id
     * @param type $network_id
     */
    public function getuser_stats() {

        $this->viewClass = 'Json';
        $this->setTypeGet();
        $PARAM_USER = 'user';
        $PARAM_NETWORK = 'network';

        //Checking if parameters are provided
        if (!$this->queryHas($PARAM_USER) || !$this->queryHas($PARAM_NETWORK)) {
            $this->errMissingParameter();
            return;
        }

        //Retreiving the query parameters
        $user_id = $this->getQueryParam($PARAM_USER);
        $network_id = $this->getQueryParam($PARAM_NETWORK);


        if (!is_numeric($user_id) || !is_numeric($network_id)) {
            $this->errInvalidParameter();
            return;
        }

        $task_created = $this->Task->taskCreatedByUser($user_id, $network_id);
        $task_assigned = $this->TaskAssignment->taskAssigned($user_id, $network_id);

        $task_created = Hash::extract($task_created, '{n}.Task.id');
        $task_assigned = Hash::extract($task_assigned, '{n}.TaskAssignment.task_id');
        $task_total = array_unique(array_merge($task_created, $task_assigned));
        $productivity = $this->productivity_percentage($task_total);
        $returnObject = array(
            'message' => 'User stats',
            'task_created' => count($task_created),
            'task_assigned' => count($task_assigned),
            'task_total' => count($task_total),
            'data' => $productivity
        );

        $this->set(array(
            'network' => $returnObject,
            '_serialize' => 'network'
        ));
        return $returnObject;
    }

    /**
     * Function for json response of Missing Parameter
     * 
     * anupama@blogtard.com
     * 
     * 
     */
    public function errMissingParameter() {

        $returnObject['status'] = "error";
        $returnObject['err_msg'] = 'Missing parameter';
        $this->set(array(
            'data' => $returnObject,
            '_serialize' => 'data'
        ));
    }

    /**
     * If any of the parameter in the request is not as expected
     * 
     */
    public function errInvalidParameter() {

        $returnObject['status'] = "error";
        $returnObject['err_msg'] = 'Unexpected parameter';
        $this->set(array(
            'data' => $returnObject,
            '_serialize' => 'data'
        ));
    }

    public function queryHas($param) {

        if (isset($this->request->query[$param])) {

            return TRUE;
        } else {

            return FALSE;
        }
    }

    /**
     * Returns the value of the given parameter from the URI
     * @param $param The query parameter name
     * @return mixed
     */
    public function getQueryParam($param) {

        $value = $this->request->query[$param];

        return $value;
    }

    public function setJson() {

        $this->viewClass = 'Json';
    }

    public function setTypeGet() {

        $this->request->onlyAllow('get');
    }

    /**
     * Mobile App Web Services
     */

    /**
     * Copy of the add() to create a new Network
     */
    public function createNetworkMobile($user_id) {

//        $this->log("Network Creation Phase");
//        $this->log($data);
        $data['NetworkPlan'][0]['plan_id'] = 1;
        if (!$this->Plan->exists($data['NetworkPlan'][0]['plan_id'])) {
            $returnObject['status'] = 'error';
            $returnObject['message'] = 'Failed to create network.';
            $this->set(array(
                'data' => $returnObject,
                '_serialize' => 'data'
            ));
            return;
        }
        $plan = $this->Plan->findById($data['NetworkPlan'][0]['plan_id']);
        $time_limit = date('Y-m-d H:i:s', strtotime("+" . $plan['Plan']['time_limit'] . " months", time()));
        $user_limit = $plan['Plan']['user_limit'];
        $storage_limit = $plan['Plan']['storage_limit'];

        $defaults = array(
            'Network' => array(
                'name' => 'My Network',
                'created_by' => $user_id,
                'user_limit' => $user_limit,
                'file_limit' => $storage_limit,
                'status' => STATUS_ACTIVE,
                'expires' => $time_limit
            //'expires' => $this->networkExpirationTime()
            ),
            'UserNetwork' => array(
                array(
                    'user_id' => $user_id,
                    'status' => STATUS_ACTIVE,
                    'role' => ROLE_OWNER,
                    'file_limit' => $storage_limit
                )
            ),
            'NetworkPlan' => array(
                array(
                    'start' => date('Y-m-d H:i:s', time()),
                    'end' => $time_limit
                )
            )
        );

//        $this->log("Default");
//        $this->log($defaults);
        $data['Network'] = $defaults['Network'];
//        $this->log($data);
        $data['NetworkPlan'][0]['start'] = date('Y-m-d H:i:s', time());
        $data['NetworkPlan'][0]['end'] = $time_limit;
        $data = array_merge($defaults, $data);
//        $this->log($data);
//        $this->log("About to add network");
        if ($this->Network->add($data)) {
//                $this->Session->write('CurrentNetwork', $this->Network->id);
//            $this->log("Network Created");
            $Event = new CakeEvent(
                    'Controller.Network.afterCreate', $this, array('user_id' => $user_id, 'network_id' => $this->Network->getLastInsertId())
            );
            return true;
//
//
//                if (isset($data['Network']['return_to'])) {
//                    return $this->redirect($data['Network']['return_to']);
//                } else {
//                    return $this->redirect('/networks');
//                }
        } else {
            return false;
        }
    }

}

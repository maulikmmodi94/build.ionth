<?php

class LogsController extends AppController {

    public $uses = array('Log', 'LogParticipant', 'UserInfo', 'Group', 'Doc', 'Task', 'Notification', 'Network');

    public function index() {
        
    }

//    public function getNotifications_old($notstoload = 20) {
//        $this->viewClass = 'Json';
//        $user_id = $this->Auth->user('id');
//        $network_id = $this->Session->read('CurrentNetwork');
////        if (empty($notstoload)) {
////            $notstoload = 99;
////        }
//        $notifications = $this->Notification->getNotification($user_id, $network_id, $notstoload);
//        if (!empty($notifications)) {
//            $log_ids = Hash::extract($notifications, '{n}.Notification.log_id');
//            $logs = $this->Log->getLogs($log_ids);
//            $result = $this->messageformat($logs, Log_type_user, $user_id);
//            $notifications = Hash::combine($notifications, '{n}.Notification.log_id', '{n}');
//            foreach ($logs as $key => $value) {
//                $result[$key]['isSeen'] = $notifications[$logs[$key]['Log']['id']]['Notification']['isSeen'];
//                $result[$key]['notification_id'] = $notifications[$logs[$key]['Log']['id']]['Notification']['id'];
//                $result[$key]['link'] = NULL;
////                $result[$key]['UserInfo'] =$result[$key]['UserInfo'][0]['UserInfo']; 
//            }
//            $returnObject['data'] = $result;
//        } else {
//            $returnObject['data'] = [];
//        }
//        $returnObject['message'] = "Notifications of current network";
//        $this->set(array(
//            'data' => $returnObject,
//            '_serialize' => 'data'
//        ));
//        return;
//    }
    /**
     * returns latest 20 notification for that network
     * @param type $notstoload
     */
    public function getNotifications($page = null, $notstoload = MAX_LOAD_NOTIFICATION){
        $this->viewClass = 'Json';
        $user_id = $this->Auth->user('id');
        $network_id = $this->Session->read('CurrentNetwork');
        $loadMore = FALSE;
        
        $notifications = $this->Notification->getNotification($user_id, $network_id,$page, $notstoload);
        
        if(isset($notifications[MAX_LOAD_NOTIFICATION-1])){
            $loadMore = TRUE;
        }
        $formated_notification = $this->_getFormatedNotification($user_id, $notifications, $loadMore);
        
       
        $this->set(array(
            'data' => $formated_notification,
            '_serialize' => 'data'
        ));
        return $formated_notification;
    }

    /**
     * it fetch logs and notification data for set formated message for notifications
     * @param type $user_id
     * @param type $notifications
     * @return string
     */
    protected function _getFormatedNotification($user_id, $notifications, $loadMore){
        if (empty($notifications)) {
            $returnObject = array(
                'data' => NULL,
                'loadMore'=> $loadMore,
                'message'=> 'there is no new notifications in this network,'
            );
            return $returnObject;
        }
        $log_ids = Hash::extract($notifications, '{n}.Notification.log_id');
        $logs = $this->Log->getLogs($log_ids);
        $result = $this->messageformat($logs, Log_type_user, $user_id);
        
        $formated_notification = $this->_setMetadataForNotifications($logs, $notifications, $result);
        
        $returnObject = array(
            'data' => $formated_notification,
            'loadMore'=> $loadMore,
            'message'=> 'there is new notifications in this network,'
        );
            return $returnObject;
    }
    
    /**
     * it sets extra information for nitification before returns it
     * @param type $logs
     * @param type $notifications
     * @param type $result
     * @return type
     */
    protected function _setMetadataForNotifications($logs, $notifications, $result){
        $notifications = Hash::combine($notifications, '{n}.Notification.log_id', '{n}');
        foreach ($logs as $key => $value) {
            $result[$key]['isSeen'] = $notifications[$logs[$key]['Log']['id']]['Notification']['isSeen'];
            $result[$key]['notification_id'] = $notifications[$logs[$key]['Log']['id']]['Notification']['id'];
            $result[$key]['link'] = NULL;
            $result[$key]['log_id'] = $logs[$key]['Log']['id'];
//          $result[$key]['UserInfo'] =$result[$key]['UserInfo'][0]['UserInfo']; 
            }
            return $result;
    }

    /**
     * return all latest unseen notification
     * @param type $latest_notification_id
     */
    public function getUnseenNotifications($latest_notification_id = null) {
        $this->viewClass = 'Json';
        $user_id = $this->Auth->user('id');
        $network_id = $this->Session->read('CurrentNetwork');
        
        $notifications = $this->Notification->getUnseenNotifications($user_id, $network_id, $latest_notification_id);
        $formated_notification = $this->_getFormatedNotification($user_id, $notifications);
        
        $this->set(array(
            'data' => $formated_notification,
            '_serialize' => 'data'
        ));
    }
    
    
    public function getNewNotifications($latest_notification_id = null){
        $this->viewClass = 'Json';
        $user_id = $this->Auth->user('id');
        $network_id = $this->Session->read('CurrentNetwork');
        
        if($this->Notification->isNewNotification($user_id, $network_id, $latest_notification_id)){
           $formated_notification = $this->getNotifications(MAX_LOAD_NOTIFICATION);
        }else{
            $formated_notification = array(
                'data' => null,
                'message'=> 'there is no new notifications in this network,'
            );
        }
            
        
        $this->set(array(
            'data' => $formated_notification,
            '_serialize' => 'data'
        ));
    }


//    public function getUnseenNotifications($latest_notification_id = null) {
//        $this->viewClass = 'Json';
//        $user_id = $this->Auth->user('id');
//        $notifications = $this->Notification->getUnseenNotifications($user_id, $latest_notification_id);
//        if (!empty($notifications)) {
//            $log_ids = Hash::extract($notifications, '{n}.Notification.log_id');
//            $logs = $this->Log->getLogs($log_ids);
//            $result = $this->messageformat($logs, Log_type_user, $user_id);
//            $notifications = Hash::combine($notifications, '{n}.Notification.log_id', '{n}');
//            foreach ($logs as $key => $value) {
//                $result[$key]['isSeen'] = $notifications[$logs[$key]['Log']['id']]['Notification']['isSeen'];
//            }
//            $returnObject['data'] = $result;
//        } else {
//            $returnObject['data'] = [];
//        }
//        $returnObject['message'] = "Notifications Unseen";
//        $this->set(array(
//            'data' => $returnObject,
//            '_serialize' => 'data'
//        ));
//        return;
//    }

    public function getLogs($page = 1) {
        $this->viewClass = 'Json';
        $user_id = $this->Auth->user('id');
        $network_id = $this->Session->read('CurrentNetwork');
        $logs = $this->LogParticipant->view($user_id, $network_id, Log_type_user, $page);
        if (sizeof($logs) > 0) {
            $result = $this->messageformat($logs, Log_type_user, $user_id);
            $returnObject['message'] = "Activity Logs";
            $returnObject['data'] = $result;
            if (sizeof($logs) == Logstoload) {
                $returnObject['moreOlder'] = true;
            }
            $this->set(array(
                'data' => $returnObject,
                '_serialize' => 'data'
            ));
            return;
        } else {
            throw new Exception('No Logs.', 403);
        }
    }

    public function getLogGroup($group_id = null, $page = 1) {
        $this->viewClass = 'Json';
        $user_id = $this->Auth->user('id');
        $network_id = $this->Session->read('CurrentNetwork');
        if ($this->Group->UsersGroup->getUsersGroupField($user_id, $group_id, $network_id, 'status') == STATUS_ACTIVE) {
            $logs = $this->LogParticipant->view($group_id, $network_id, Log_type_group, $page);
            $result = $this->messageformat($logs, Log_type_group, $user_id);
            $returnObject['message'] = "Activity Logs";
            $returnObject['data'] = $result;
            $this->set(array(
                'data' => $returnObject,
                '_serialize' => 'data'
            ));
            return;
        }
    }

    public function getLogDoc($doc_id = NULL, $page = 1) {
        $this->viewClass = 'Json';
        $user_id = $this->Auth->user('id');
        $network_id = $this->Session->read('CurrentNetwork');
        if ($this->Doc->DocAssignment->getDocAssignmentField($doc_id, $network_id, $user_id, 'status') == STATUS_ACTIVE) {
            $logs = $this->LogParticipant->view($doc_id, $network_id, Log_type_file, $page);
            $result = $this->messageformat($logs, Log_type_file, $user_id);
            $returnObject['message'] = "Activity Logs";
            $returnObject['data'] = $result;
            $this->set(array(
                'data' => $returnObject,
                '_serialize' => 'data'
            ));
            return;
        }
    }

    public function getLogTask($task_id = NULL, $page = 1) {
        $this->viewClass = 'Json';
        $user_id = $this->Auth->user('id');
        $network_id = $this->Session->read('CurrentNetwork');
        if ($this->Doc->DocAssignment->getDocAssignmentField($doc_id, $network_id, $user_id, 'status') == STATUS_ACTIVE) {
            $logs = $this->LogParticipant->view($task_id, $network_id, Log_type_task, $page);
            $result = $this->messageformat($logs, Log_type_task, $user_id);
            $returnObject['message'] = "Activity Logs";
            $returnObject['data'] = $result;
            $this->set(array(
                'data' => $returnObject,
                '_serialize' => 'data'
            ));
            return;
        }
    }

    private function messageformat($logs, $type, $value) {
//        USERS 
        $users = array_merge(hash::extract($logs, '{n}.LogParticipant.{n}[type=/' . Log_type_user . '/].participant_id'), hash::extract($logs, '{n}.Log.user_id'));
        if (!empty($users)) {
            $defaultConditions = $this->UserInfo->defaultConditions;
            $userinfos = $this->UserInfo->find('list', array(
                'conditions' => array('UserInfo.user_id' => $users),
                'fields' => array('UserInfo.user_id', 'UserInfo.name')
            ));
            $this->UserInfo->defaultConditions = $defaultConditions;
        }
//        GROUPS 
        $groups = hash::extract($logs, '{n}.LogParticipant.{n}[type=/' . Log_type_group . '/].participant_id');
        if (!empty($groups)) {
            $defaultConditions = $this->Group->defaultConditions;
            $this->Group->defaultConditions = null;
            $groupinfos = $this->Group->find('list', array(
                'conditions' => array('Group.id' => $groups),
                'fields' => array('Group.id', 'Group.name')
            ));
            $this->Group->defaultConditions = $defaultConditions;
        }
//        DOCS 
        $docs = hash::extract($logs, '{n}.Log[type=/' . Log_type_file . '/].object_id');
        if (!empty($docs)) {
            $defaultConditions = $this->Doc->defaultConditions;
            $this->Doc->defaultConditions = null;
            $docinfos = $this->Doc->find('list', array(
                'conditions' => array('Doc.id' => $docs),
                'fields' => array('Doc.id', 'Doc.name')
            ));
            $this->Doc->defaultConditions = $defaultConditions;
        }
//        TASK 
        $tasks = hash::extract($logs, '{n}.Log[type=/' . Log_type_task . '/].object_id');
        if (!empty($tasks)) {
            $defaultConditions = $this->Task->defaultConditions;
            $this->Task->defaultConditions = null;
            $taskinfos = $this->Task->find('list', array(
                'conditions' => array('Task.id' => $tasks),
                'fields' => array('Task.id', 'Task.title')
            ));
            $this->Task->defaultConditions = $defaultConditions;
        }
//        NETWORKS 
        $networks = hash::extract($logs, '{n}.Log[type=/' . Log_type_network . '/].object_id');
        if (!empty($networks)) {
            $defaultConditions = $this->Network->defaultConditions;
            $this->Network->defaultConditions = null;
            $networkinfos = $this->Network->find('list', array(
                'conditions' => array('Network.id' => $networks),
                'fields' => array('Network.id', 'Network.name')
            ));
            $this->Network->defaultConditions = $defaultConditions;
        }
        $result = [];
        foreach ($logs as $log) {
            $message = null;
            $linkto = null;
            $time = $log['Log']['created'];
            switch ($log['Log']['type']) {
                case Log_type_group:
//                    GROUP 
                    $type = Log_type_group;
                    switch ($log['Log']['activity']) {
                        case Log_activity_create:
                            $linkto = '/groups/view/' . $log['Log']['object_id'];
                            $message = getname($userinfos, $log['Log']['user_id']) . ' created group ' . getname($groupinfos, $log['Log']['object_id']);
                            break;

                        case Log_activity_edit:
                            $linkto = '/groups/view/' . $log['Log']['object_id'];
                            $message = '<span class="notification-user">'.getname($userinfos, $log['Log']['user_id']).'</span>' . ' edited group '.'<span class="notification-object">' . getname($groupinfos, $log['Log']['object_id']).'</span>';
                            break;

                        case Log_activity_member_left:
                            $linkto = '/groups/view/' . $log['Log']['object_id'];
                            $message = '<span class="notification-user">'.getname($userinfos, $log['Log']['user_id']) . ' left group '.'<span class="notification-object">' .getname($groupinfos, $log['Log']['object_id']).'</span>';
                            break;

                        case Log_activity_member_add:
                            $linkto = '/groups/view/' . $log['Log']['object_id'];
                            $users = ' ';
                            foreach ($log['LogParticipant'] as $key => $participant) {
                                if ($participant['type'] == Log_type_user && $participant['participant_id'] != $log['Log']['user_id']) {
                                    $users = $users . getname($userinfos, $participant['participant_id']) . ' ';
                                }
                            }
                            $message = '<span class="notification-user">'.getname($userinfos, $log['Log']['user_id']).'</span>' . ' added ' . $users . ' to team ' .'<span class="notification-object">' .getname($groupinfos, $log['Log']['object_id']).'</span>';
                            break;

                        case Log_activity_member_remove:
                            $linkto = '/groups/view/' . $log['Log']['object_id'];
                            $users = '';
                            foreach ($log['LogParticipant'] as $key => $participant) {
                                if ($participant['type'] == Log_type_user && $participant['participant_id'] != $log['Log']['user_id']) {
                                    $users = getname($userinfos, $participant['participant_id']) . '';
                                }
                            }
                            $message = getname($userinfos, $log['Log']['user_id']) . ' removed ' . $users . ' from group ' . getname($groupinfos, $log['Log']['object_id']);
                            break;

                        case Log_activity_delete:
                            $linkto = '/groups/view/' . $log['Log']['object_id'];
                            $message = getname($userinfos, $log['Log']['user_id']) . ' deleted group ' . getname($groupinfos, $log['Log']['object_id']);
                        default:
                            break;
                    }
                    break;
//                FILE 
                case Log_type_file:
                    $type = Log_type_file;
                    switch ($log['Log']['activity']) {
                        case Log_activity_create:
                            $linkto = '/docs/view/' . $log['Log']['object_id'];
                            $message = getname($userinfos, $log['Log']['user_id']) . ' uploaded file ' . getname($docinfos, $log['Log']['object_id']);
                            break;
                        case Log_activity_edit:
                            $linkto = '/docs/view/' . $log['Log']['object_id'];
                            $message = '<span class="notification-user">'.getname($userinfos, $log['Log']['user_id']).'</span>'. ' edited file ' . '<span class="notification-object">'.getname($docinfos, $log['Log']['object_id']).'</span>';
                            break;
                        case Log_activity_member_add:
                            $linkto = '/docs/view/' . $log['Log']['object_id'];
                            $users = ' ';
                            $groups = ' ';
                            foreach ($log['LogParticipant'] as $key => $participant) {
                                if ($participant['type'] == Log_type_user && $participant['participant_id'] != $log['Log']['user_id']) {
                                    $users = $users . getname($userinfos, $participant['participant_id']) . ' ';
                                } else if ($participant['type'] == Log_type_group) {
                                    $groups = $groups . getname($groupinfos, $participant['participant_id']) . ' ';
                                }
                            }
                            $message = '<span class="notification-user">'.getname($userinfos, $log['Log']['user_id']).'</span>' . ' added ' . $users . $groups . ' to file ' .'<span class="notification-object">' .getname($docinfos, $log['Log']['object_id']).'</span>';
                            break;
                        case Log_activity_member_remove:
                            $linkto = '/docs/view/' . $log['Log']['object_id'];
                            $users = ' ';
                            $groups = ' ';
                            foreach ($log['LogParticipant'] as $key => $participant) {
                                if ($participant['type'] == Log_type_user && $participant['participant_id'] != $log['Log']['user_id']) {
                                    $users = $users . getname($userinfos, $participant['participant_id']) . ' ';
                                } else if ($participant['type'] == Log_type_group) {
                                    $groups = $groups . getname($groupinfos, $participant['participant_id']) . ' ';
                                }
                            }
                            $message = getname($userinfos, $log['Log']['user_id']) . ' removed ' . $users . $groups . ' from file ' . getname($docinfos, $log['Log']['object_id']);
                            break;
                        case Log_activity_delete:
                            $message = getname($userinfos, $log['Log']['user_id']) . ' deleted file ' . getname($docinfos, $log['Log']['object_id']);
                            break;
                        case Log_activity_comment_add:
                            $linkto = '/docs/view/' . $log['Log']['object_id'];
                            $message = '<span class="notification-user">'.getname($userinfos, $log['Log']['user_id']).'</span>' . ' commented on file ' .'<span class="notification-object">'. getname($docinfos, $log['Log']['object_id']).'</span>';
                            break;
                        default:
                            break;
                    }

                    break;
//                TASK 
                case Log_type_task:
                    $type = Log_type_task;
                    switch ($log['Log']['activity']) {
                        case Log_activity_create:
                            $linkto = '/tasks/view/' . $log['Log']['object_id'];
                            $message = getname($userinfos, $log['Log']['user_id']) . ' created task ' . getname($taskinfos, $log['Log']['object_id']);
                            $message = getname($list, $value);
                            break;
                        case Log_activity_edit:
                            $linkto = '/tasks/view/' . $log['Log']['object_id'];
                            $message = '<span class="notification-user">'.getname($userinfos, $log['Log']['user_id']) .'</span>'. ' edited task '.'<span class="notification-object">' . getname($taskinfos, $log['Log']['object_id']).'</span>';
                            break;
                        case Log_activity_task_complete:
                            $linkto = '/tasks/view/' . $log['Log']['object_id'];

                            $message = '<span class="notification-user">'.getname($userinfos, $log['Log']['user_id']).'</span>' . ' completed ' . '<span class="notification-object">'. getname($taskinfos, $log['Log']['object_id']) . '</span>';
                            break;
                        case Log_activity_task_uncomplete:
                            $linkto = '/tasks/view/' . $log['Log']['object_id'];
                            $message = '<span class="notification-user">'.getname($userinfos, $log['Log']['user_id']).'</span>' . ' marked this as incomplete ' .'<span class="notification-object">'. getname($taskinfos, $log['Log']['object_id']).'</span>';
                            break;
                        case Log_activity_member_add:
                            $linkto = '/tasks/view/' . $log['Log']['object_id'];
                            $users = ' ';
                            $groups = ' ';
                            foreach ($log['LogParticipant'] as $key => $participant) {
                                if ($participant['type'] == Log_type_user && $participant['participant_id'] != $log['Log']['user_id']) {
                                    $users = $users . getname($userinfos, $participant['participant_id']) . ' ';
                                } else if ($participant['type'] == Log_type_group) {
                                    $groups = $groups . getname($groupinfos, $participant['participant_id']) . ' ';
                                }
                            }
                            $message = '<span class="notification-user">'.getname($userinfos, $log['Log']['user_id']).'</span>' . ' added ' . $users . $groups . ' to task ' . '<span class="notification-object">'. getname($taskinfos, $log['Log']['object_id']).'</span>';
                            break;
                        case Log_activity_member_remove:
                            $linkto = '/tasks/view/' . $log['Log']['object_id'];
                            $users = ' ';
                            $groups = ' ';
                            foreach ($log['LogParticipant'] as $key => $participant) {
                                if ($participant['type'] == Log_type_user && $participant['participant_id'] != $log['Log']['user_id']) {
                                    $users = $users . getname($userinfos, $participant['participant_id']) . ' ';
                                } else if ($participant['type'] == Log_type_group) {
                                    $groups = $groups . getname($groupinfos, $participant['participant_id']) . ' ';
                                }
                            }
                            $message = '<span class="notification-user">'.getname($userinfos, $log['Log']['user_id']).'</span>' . ' removed ' . $users . $groups . ' from task ' .'<span class="notification-object">'. getname($taskinfos, $log['Log']['object_id']).'</span>';
                            break;
                        case Log_activity_delete:
                            $message = getname($userinfos, $log['Log']['user_id']) . ' deleted task ' . getname($taskinfos, $log['Log']['object_id']);
                            break;
                        case Log_activity_comment_add:
                            $linkto = '/tasks/view/' . $log['Log']['object_id'];
                            $message = '<span class="notification-user">'.getname($userinfos, $log['Log']['user_id']) .'</span>'. ' commented on task ' . '<span class="notification-object">'.getname($taskinfos, $log['Log']['object_id']).'</span>';
                            break;
                        default:
                            break;
                    }
                    break;
                //networks
                case Log_type_network:
                    $type = Log_type_network;
                    switch ($log['Log']['activity']) {
                        case Log_activity_create:
                            $linkto = '/networks/view/' . getname($networkinfos, $log['Log']['object_id']);
                            $message = getname($userinfos, $log['Log']['user_id']) . ' created network ' . getname($networkinfos, $log['Log']['object_id']);
                            break;
                        case Log_activity_edit:
                            $linkto = '/networks/view/' . getname($networkinfos, $log['Log']['object_id']);
                            $message = getname($userinfos, $log['Log']['user_id']) . ' edited network ' . getname($networkinfos, $log['Log']['object_id']);
                            break;
                        case Log_activity_network_assignrole:
                            $linkto = '/networks/view/' . getname($networkinfos, $log['Log']['object_id']);
                            $users = ' ';
                            foreach ($log['LogParticipant'] as $key => $participant) {
                                if ($participant['type'] == Log_type_user && $participant['participant_id'] != $log['Log']['user_id']) {
                                    $users = $users . getname($userinfos, $participant['participant_id']) . ' ';
                                }
                            }
                            $message = getname($userinfos, $log['Log']['user_id']) . ' assigned role ' . $log['Log']['info'] . ' to' . $users;
                            break;
                        case Log_activity_member_add:
                            $linkto = '/networks/view/' . getname($networkinfos, $log['Log']['object_id']);
                            $users = ' ';
                            foreach ($log['LogParticipant'] as $key => $participant) {
                                if ($participant['type'] == Log_type_user && $participant['participant_id'] != $log['Log']['user_id']) {
                                    $users = $users . getname($userinfos, $participant['participant_id']) . ' ';
                                }
                            }
                            $message = getname($userinfos, $log['Log']['user_id']) . ' added ' . $users . ' to network' . getname($networkinfos, $log['Log']['object_id']);
                            break;
                        case Log_activity_member_remove:
                            $linkto = '/networks/view/' . getname($networkinfos, $log['Log']['object_id']);
                            $users = ' ';
                            foreach ($log['LogParticipant'] as $key => $participant) {
                                if ($participant['type'] == Log_type_user && $participant['participant_id'] != $log['Log']['user_id']) {
                                    $users = $users . getname($userinfos, $participant['participant_id']) . ' ';
                                }
                            }
                            $message = getname($userinfos, $log['Log']['user_id']) . ' removed ' . $users . ' from network' . getname($networkinfos, $log['Log']['object_id']);
                            break;
                        case Log_activity_delete:
                            $message = getname($userinfos, $log['Log']['user_id']) . ' deleted network ' . getname($networkinfos, $log['Log']['object_id']);
                        default:
                            break;
                    }
                    break;
                default:
                    break;
            }
            $message = '<span>' . $message . '</span>';
              array_push($result, array('type' => $type, 
                                        'activity' => $log['Log']['activity'], 
                                        'time' => $time,
                                        'message' => $message, 
                                        'link' => $linkto, 
                                        'network_id' => $log['Log']['network_id'],
                                        'type_id' => $log['Log']['object_id'],
                                        'user_id' => $log['Log']['user_id']
                      ));
        }
        return $result;
    }

}

function getname($list, $value) {
    return '<span class="">' . @$list[$value] . '</span>';
}

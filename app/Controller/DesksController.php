<?php

App::uses('AppController', 'Controller');

class DesksController extends AppController {
    
    public $components = array(
        'Auth',
        'Session',
        'Cookie'
    );
    
    
    
    public function afterFilter() {
        parent::afterFilter();
        $this->Session->delete('Auth.User.FirstSession');
    }
      


    public function index() {
    }
    
}

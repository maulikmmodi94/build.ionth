<?php

class FeedsController extends AppController {

    public $uses = array('Feed', 'UserNetwork','UserInfo');
    public $helpers = array('Tags.TagCloud');
    public $paginate = array();
    public $components = array('Comments.Comments',
        'Paginator', 'session'
    );

    function beforeFilter() {
        parent::beforeFilter();
        if (Configure::read('enterprise_feeds') !== TRUE) {
            throw new NotFoundException();
        }
    }
    /**
     * discription : Display a page consists of 20 feed.
     * @param int $page It display the number of page for feed's display
     * @param type $page
     *  */
    public function view($page = 1) {

        $this->viewClass = 'Json';
        $network_id = $this->Session->read('CurrentNetwork');
        $user_id = $this->Auth->user('id');
        $loadMore = FALSE;
        $feed = $this->Feed->getFeeds($network_id, $page, $user_id);
        $feed = $this->_attatchUserInfoandComment($feed);
        if (isset($feed[Feeds_to_load-1])) {
            $loadMore = TRUE;
        }
        if(!empty($feed)){
            if($page == 1){
            //set the value of last seen feed_id which is seen by specific user. 
            $this->UserNetwork->setFeedField($user_id, $network_id, $feed[0]['Feed']['id']);
            }
        }

        $returnObject = array(
            'message' => 'Loaded Successfully',
            'data' => $feed,
            'loadmoreFeed' => $loadMore
        );
        $this->set(array(
            'data' => $returnObject,
            '_serialize' => 'data'
        ));
    }
    
    public function getFeed($feed_id = 100) {
        
        if ($this->request->is('post')) {
        $this->viewClass = 'Json';
        
        if (!$this->Feed->isPartOfFeed($feed_id)) {
            $returnObject = array(
                'message' => 'Feed does not exist',
                'data' => array()
                );
        }
        else {
            $feed = $this->Feed->gets($feed_id);
            $returnObject = array(
                'message' => 'Feed Fetched',
                'data' => $feed
            );
        }
        $this->set(array(
            'data' => $returnObject,
            '_serialize' => 'data'
        ));
        return;
        }
    }

    public function add() {
        if ($this->request->is('post')) {
            $this->viewClass = 'Json';
            $data['Feed'] = $this->request->data;
            if (empty(trim($data['Feed']['preview_link']))) {
                unset($data['Feed']['preview_link']);
            }
            $data['Feed']['tags'] = $this->request->data['hashtags'];
            $this->log($this->request->data);
            $msg = $this->request->data['message'];
            $data['Feed']['message'] = $this->Feed->detectAndTransformHashTags($msg);
            $data['Feed']['user_id'] = $this->Auth->user('id');
            $data['Feed']['network_id'] = $this->Session->read('CurrentNetwork');

            if ($this->Feed->addFeeds($data)) {
                $feed = $this->Feed->findById($this->Feed->getLastInsertID());
                $this->UserNetwork->setFeedField($data['Feed']['user_id'], $data['Feed']['network_id'], $feed['Feed']['id']);
                $returnObject = array(
                    'message' => 'Added Successfully',
                    'data' => $feed
                );
                $this->set(array(
                    'data' => $returnObject,
                    '_serialize' => 'data'
                ));
            } else {
                throw new Exception('Failed to add post.', 403);
            }
        }
    }

    public function edit($id = null) {
        $this->viewClass = 'Json';
        if ($this->request->is(array('post'))) {
            $user_id = $this->Auth->user('id');
            $this->viewClass = 'Json';
            if ($this->Feed->field('user_id', array('Feed.id' => $id)) == $user_id) {
                if ($this->Feed->editFeeds($id, $this->request->data)) {
                    $returnObject = array(
                        'message' => 'Edited Successfully.'
                    );
                    $this->set(array(
                        'data' => $returnObject,
                        '_serialize' => 'data'
                    ));
                } else {
                    throw new Exception('Failed to edit post.', 403);
                }
            } else {
                throw new Exception('You are not authorized to edit this post.', 403);
            }
        }
    }

    public function delete($id) {
        $this->viewClass = 'Json';
        if ($this->request->is(array('post', 'put'))) {
            $user_id = $this->Auth->user('id');
            if ($this->Feed->field('user_id', array('Feed.id' => $id)) == $user_id) {
                if ($this->Feed->delete($id)) {
                    $returnObject = array(
                        'message' => 'Deleted Successfully',
                    );

                    $this->set(array(
                        'data' => $returnObject,
                        '_serialize' => 'data'
                    ));
                } else {
                    throw new Exception('Failed to delete.', 403);
                }
            } else {
                throw new Exception('You are not authorized to delete this post.', 403);
            }
        }
    }

    public function index() {
        
    }

    /**
     * Description:it return the number of feed's which are not read by the user.
     * for this it will perform two operation.
     * one->check if lastfeed has a value or not
     * two->return the difference of that field
     * 
     */
    public function unreadFeedCount() {
        if ($this->request->is(array('get'))) {
            $this->viewClass = 'Json';
            $network_id = $this->Session->read('CurrentNetwork');
            $user_id = $this->Auth->user('id');

            $lastfeed = $this->Feed->getLastFeeds($network_id);
            if ($lastfeed != NULL) {
                $feedId = $this->UserNetwork->getFeedId($user_id, $network_id);
                $returnObject = array(
                    'message' => 'notification for feed',
                    'loadmoreFeedCount' => $lastfeed - $feedId
                );
                $this->set(array(
                    'data' => $returnObject,
                    '_serialize' => 'data'
                ));
            }
        }
    }
    
    /**
     * Description : return the latest feed which are not read by user
     * 
     */
    public function latestFeed() {
        $this->viewClass = 'Json';
        $network_id = $this->Session->read('CurrentNetwork');
        $user_id = $this->Auth->user('id');
        //last seen feed by user
        $lastfeed = $this->UserNetwork->getFeedId($user_id, $network_id);

        //latest feed in table..
        $feed = $this->Feed->getLatestFeeds($network_id, $lastfeed);



        //now, store the updatedfeed in database.
        $latestfeed = $this->Feed->getLastFeeds($network_id);
        //store data into database..
        $this->UserNetwork->setFeedField($user_id, $network_id, $latestfeed);

        $returnObject = array(
            'message' => 'Latest Feed',
            'data' => $feed
        );
        $this->set(array(
            'data' => $returnObject,
            '_serialize' => 'data'
        ));
    }

    /**
     * discription : set the page number when clicks the button
     */
    public function hashtag($tagName, $page = null) {

        if ($this->request->is('post'))  {
        $this->viewClass = 'Json';

//        $this->layout='default';
        $moreTag = FALSE;
        $network_id = $this->Session->read('CurrentNetwork');

        $feed = $this->Feed->getFeedsSearch($tagName, $network_id, $page);
        if(isset($feed[Feeds_to_load-1])){
            $moreTag = TRUE;
        }
        
        $returnObject = array(
            'message' => 'searched Successfully',
            'data' => $feed,
            'loadMoreTag' => $moreTag
        );

        $this->set(array(
            'data' => $returnObject,
            '_serialize' => 'data'
        ));   
       }
    }
 
    
    /**
     * Function invoked when a Like action is processed. Throws an exception when 
     * a illegal like operation is performed.
     * For example : Like a feed that does not belong to the current network
     * @param int $feed_id The id of the feed
     * @throws NotFoundException 
     */
    
    public function like_feed($feed_id = NULL){
        //Invoke likes controller Like method
        $this->viewClass = 'Json';
        if($this->request->is('Post') && !empty($feed_id)) 
        {   //Retrieve the current network
            $network_id = $this->Session->read('CurrentNetwork');
            //Obtain the Network to which the feed belongs 
            $result = $this->Feed->find('first',array(
                    'conditions' => array('Feed.id' => $feed_id),
                    'fields' => array('Feed.network_id')));
            //Perform Comparisions to process a like
            if($result['Feed']['network_id'] == $network_id){
                //Like an item
                $this->Feed->like($feed_id, $this->Auth->user('id'),$network_id);
                $returnObject['message'] = "Feed Liked";
                $returnObject['data']['id'] = $feed_id;
                $this->set(array(
                    'data' => $returnObject,
                    '_serialize' => 'data'
                ));
                return;
            }
            else {
                //if a illegal like operation is performed
                throw new NotFoundException(__('Failed to Like',403));
            }
        }
        else {
            throw new Exception('Missing Parameters.', 403);;
        }
    }

    /**
     * Function invoked when a Dislike action is processed. Throws an exception when 
     * a illegal like operation is performed.
     * For example : Dislike a feed that does not belong to the current network
     * @param int $feed_id The id of the feed
     * @throws NotFoundException 
     */
    public function dislike_feed($feed_id = NULL)
    {   $this->viewClass = 'Json';
        if($this->request->is('Post') && !empty($feed_id)) 
        {
            //Retrieve the current network
            $network_id = $this->Session->read('CurrentNetwork');
            //Obtain the Network to which the feed belongs 
            $result = $this->Feed->find('first',array(
                        'conditions' => array('Feed.id' => $feed_id),
                        'fields' => array('Feed.network_id')));
            //Perform Comparisions to process a Dislike
            if($result['Feed']['network_id'] == $network_id){
                //Dislike an item
                $this->Feed->dislike($feed_id, $this->Auth->user('id'), $network_id);
                $returnObject['message'] = "Feed Disliked";
                $returnObject['data']['id'] = $feed_id;
                $this->set(array(
                    'data' => $returnObject,
                    '_serialize' => 'data'
                ));
                return;
            }
            else {
                //if a illegal like operation is performed
                throw new NotFoundException(__('Failed to Dislike',403));
            }
        }
        else {
            throw new Exception('Missing Parameters.', 403);
        }
    }
    
    /**
     * This function returns the list of all the users that have liked a feed.
     * It returns the list in groups depending on the value of $page variable.
     * @param int $feed_id The id of the feed whose list of users have to be returned
     * @param int $page The page no. to determine the group of users whose information has to be sent. By default its value is 1.
     * @return array Returns a message along with the list of users
     * @throws Exception if
     */
    public function listLikes($feed_id = NULL,$page = NULL)
    {
        $this->viewClass = 'Json';
        if(empty($feed_id)) {
            throw new Exception('Missing Parameters.', 403);
        }
        $result = $this->Feed->likeLoad($feed_id,$page);
        $result = $this->_attatchUserInfoandComment($result , FALSE);
        $returnObject['likes'] = $result;
        if(!$result) {
            $returnObject['message'] = "No likes to load";
            $returnObject['loadMore'] = FALSE;
        }
        else if($result){
            if(sizeof($result) < Max_Likes_to_load) {
                $returnObject['message'] = "All likes loaded";
                $returnObject['loadMore'] = FALSE;
            }
            else {
                $returnObject['message'] = "More likes loaded";
                $returnObject['loadMore'] = TRUE;
            }
        }         
        $this->set(array(
                    'data' => $returnObject,
                    '_serialize' => 'data'
                ));
        
        return $returnObject;
    }

    /** 
     * Lists all the Feeds that the user likes **in a particular network
     * @param int $id The id of the feed
     */
//    public function item_list($id = null)
//    {
//        
//        debug($this->Session->read('CurrentNetwork'));
//        
//        //Retrieve the current network
//        $network_id = $this->Session->read('CurrentNetwork');
//
//        debug($this->Feed->find('first',array(
//                    'conditions' => array('Feed.id' => $id),
//                    'fields' => array('Feed.network_id')))
//        );
//        
//        //Obtain the Network to which the feed belongs 
//        $result = $this->Feed->find('first',array(
//                    'conditions' => array('Feed.id' => $id),
//                    'fields' => array('Feed.network_id')));
//
//        debug($result['Feed']['network_id']);
//        
//        if($result['Feed']['network_id'] == $network_id){       
//            Find all items liked by an user
//            debug($this->Feed->findLikedBy($this->Auth->user('id')));
//        }
//        
//    }
    
    /**
     * Check if user has liked a feed
     * @param int $id
     */
    public function like_test_user($id = NULL)
    {
        
        //Test if an user like an item
        if($this->Feed->isLikedBy($id, $this->Auth->user('id'))) {
        
        }
        else {
        
        }
     
    }
    
    protected function _attatchUserInfoandComment($feeds , $isLike = TRUE){
        foreach ($feeds as $key => $value) {
            if($isLike){
            $user_id = $feeds[$key]['Feed']['user_id'];
            }else{
                $user_id = $feeds[$key]['user_id'];
            }
            $feeds[$key]['user_info'] = $this->UserInfo->findByUser_id($user_id,array('UserInfo.user_id', 'UserInfo.first_name', 'UserInfo.last_name', 'UserInfo.name', 'UserInfo.vga', 'UserInfo.thumb', 'UserInfo.xvga','UserInfo.email'));
            $feeds[$key]['user_info'] = $feeds[$key]['user_info']['UserInfo'];
            
            if($isLike && isset($feeds[$key]['Comment']) & (count($feeds[$key]['Comment']) > 0 )){
                foreach ($feeds[$key]['Comment'] as $k => $v) {
                    $feeds[$key]['Comment'][$k]['user_info'] = $this->UserInfo->findByUser_id($feeds[$key]['Comment'][$k]['user_id'],array('UserInfo.user_id', 'UserInfo.first_name', 'UserInfo.last_name', 'UserInfo.name', 'UserInfo.vga', 'UserInfo.thumb', 'UserInfo.xvga','UserInfo.email'));
            
            $feeds[$key]['Comment'][$k]['user_info'] = $feeds[$key]['Comment'][$k]['user_info']['UserInfo'];      
                }
            }
        }
        return $feeds;     
    }


    /*
     *  Like an item
     *      $this->Post->like($post_id, $this->Auth->user('id'));
     *  Dislike an item
     *      $this->Post->dislike($post_id, $this->Auth->user('id'));
     *  Find all items liked by an user
     *      $this->Post->findLikedBy($this->Auth->user('id'));
     *  Test if an user like an item
     *      if($this->Post->isLikedBy($post_id, $this->Auth->user('id'))){...}
     *  Find the most liked item
     *      $this->Post->find('most_liked', array('limit'=>5));
     */
}

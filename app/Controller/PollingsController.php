<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class PollingsController extends AppController {

    public $uses = array('Message', 'UserConversation');

    public function notifymessages($id) {

        $this->viewClass = 'Json';
        $new = array();
        for ($i = 0; $i < 20; $i++) {
            flush();
            //all new conversations
            $new = $this->Message->checkfornewmessages($id);
            if ($new != NULL) {
                $conversations = $this->UserConversation->loadconversationsfromid($new[0], $this->Auth->user('id'), $id);
                if ($conversations != NULL) {
                    $groups = array();
                    $result = array();
                    foreach ($conversations as $conversation) {
                        array_push($result, array('type' => 'conversations', 'data' => array($conversation), 'direction' => 'new'));
                    }
                    //Return latest messageid and result array
                    $returnObject['latestmessageid'] = $new[1];
                    $returnObject['message'] = "New data";
                    $returnObject['data'] = $result;
                    $this->set(array(
                        'data' => $returnObject,
                        '_serialize' => 'data'
                    ));
                    return;
                }
                $id = $new[1];
            }
            sleep(2);
            session_write_close();
        }
        //Set HTTP Status code as No-Content
        $this->response->statusCode(204);        
        $returnObject['message'] = "No new Messages.";
        $this->set(array(
            'data' => $returnObject,
            '_serialize' => 'data'
        ));
        return;
    }

}

?>

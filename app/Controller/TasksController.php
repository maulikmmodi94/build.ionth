<?php

App::uses('AppController', 'Controller');
App::uses('CakeEvent', 'Event');
App::uses('HttpSocket', 'Network/Http');

/**
 * Tasks Controller
 *
 * @property Task $Task
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class TasksController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Comments.Comments' =>
        array(
            'responseActions' => array('isUserPart' => 'isPartOfTask')
        ),
        'Paginator',
        'Search.Prg',
        'Session'
    );
    public $uses = array('Task', 'TaskAssignment', 'Group', 'UserInfo', 'UserNetwork','User','Network');
    public $accessList = array(
        ROLE_ADMIN => array(
            'edit', 'delete', 'removeAssignee'
        )
    );

    public function beforeFilter() {

        $this->Auth->allow('pending_tasks', 'getTasks', 'setTaskComplete', 'createTask');


        if (isset($this->params['pass'][0]) && in_array($this->action, $this->accessList[ROLE_ADMIN])) {


            $user_id = $this->Auth->user('id');
            $conditions = array(
                'TaskAssignment.task_id' => $this->params['pass'][0],
                'TaskAssignment.user_id' => $user_id,
                'TaskAssignment.role' => ROLE_ADMIN,
            );
            $isAdmin = $this->TaskAssignment->find('count', array('conditions' => $conditions));
            if (!$isAdmin) {
                #401 Unauthorized, the HTTP status code for authentication errors
                throw new Exception('Not Authorized to access', 401);
            }
        }

        $timezone = abs($this->Auth->user('timezone'));
        $dates = array('start_date', 'end_date');

        foreach ($dates as $value) {
            if (isset($this->request->data['Task'][$value])) {
                $date = new DateTime($this->request->data['Task'][$value]);
                if ($this->Auth->user('timezone') > 0) {
                    $date->add(new DateInterval('PT' . $timezone . 'M'));
                } else {
                    $date->sub(new DateInterval('PT' . $timezone . 'M'));
                }
                $this->request->data['Task'][$value] = $date->format('Y:m:d H:i:s');
            }
        }

        parent::beforeFilter();
    }

    public function index() {
        $this->layout = 'gridle_in';
        $tasks = $this->Task->find('list', array('Task.title', 'Task.id'));
    }
/**
 * set assigny for all task
 * if $id is set then it is setassignee for edit task
 * @param type $id
 * @return type
 */
    protected function _setAssignee($id = NULL) {

        if (!isset($this->request->data['TaskAssignment']['assignee_data'])) {
            $this->request->data['TaskAssignment'] = array();
            return;
        }

        # Initialization Variables
        $current_network_id = $this->Session->read('CurrentNetwork');
        $auth_user_id = $this->Auth->user('id');
        $this->loadModel('UsersGroup');
        $this->loadModel('UserNetwork');

        $assignee = $this->Task->splittaglist($this->request->data['TaskAssignment']['assignee_data']);
        $this->request->data['TaskAssignment'] = array();
        $assignee['user'] = array_values($this->UserNetwork->getUsers($current_network_id, $assignee['user']));
        $assignee['group'] = array_values($this->UsersGroup->getGroups($current_network_id, $auth_user_id, $assignee['group']));

        if (!is_null($id)) {
                $assignee = $this->_getUserAndGroupForTaskEdit($id, $assignee);
        }

        $this->_setUserAndGroupTaskAssignee($current_network_id, $auth_user_id, $assignee);
    }
    
    protected function _assignCreaterToTask($current_network_id, $current_user_id){
        #set task-owner to taskAssignment
            array_push($this->request->data['TaskAssignment'], array(
                'network_id' => $current_network_id,
                'user_id' => $current_user_id,
                'group_id' => 0,
                'role' => ROLE_ADMIN,
                'added_by' => $current_user_id
                    ));
            
    }

        protected function _setUserAndGroupTaskAssignee($current_network_id, $auth_user_id, $assignee){
        
        $user_groups = $this->UsersGroup->find('all', array(
            'conditions' => array(
                'UsersGroup.group_id' => $assignee['group'],
            ),
            'fields' => array(
                'UsersGroup.user_id',
                'UsersGroup.group_id'
            )));

        foreach ($assignee['user'] as $key => $value) {
            array_push($this->request->data['TaskAssignment'], array(
                'user_id' => $value,
                'group_id' => 0,
                'role' => ROLE_USER,
                'added_by' => $auth_user_id,
                'network_id' => $current_network_id,
            ));
        }

        foreach ($user_groups as $key => $value) {
            array_push($this->request->data['TaskAssignment'], array(
                'user_id' => $value['UsersGroup']['user_id'],
                'group_id' => $value['UsersGroup']['group_id'],
                'role' => ROLE_USER,
                'added_by' => $auth_user_id,
                'network_id' => $current_network_id,
            ));
        }
    }

        protected function _getUserAndGroupForTaskEdit($id, $assignee){
        
        $already_assignee = $this->TaskAssignment->find('all', array(
                
                'conditions' => array(
                    'TaskAssignment.task_id' => $id,
                    'TaskAssignment.role' => ROLE_USER,
                ),
                'fields' => array(
                    'TaskAssignment.user_id',
                    'TaskAssignment.group_id',
                )
            ));

            $already_assignee['group'] = Hash::extract($already_assignee, '{n}.TaskAssignment[group_id!=0].group_id');
            $already_assignee['user'] = Hash::extract($already_assignee, '{n}.TaskAssignment[group_id<=0].user_id');

            $assignee['user'] = array_diff($assignee['user'], array_values($already_assignee['user']));
            $assignee['group'] = array_diff($assignee['group'], array_values($already_assignee['group']));
            
            return $assignee;
    }

    public function add_nlp() {
        $this->viewClass = 'Json';
        $this->request->onlyAllow('post', 'delete');
        $user_id = $this->Auth->user('id');
        $network_id = $this->Session->read('CurrentNetwork');
        $returnObject = array();
        if (isset($this->request->data)) {
            $httpsocket = new HttpSocket();
            $data = array('data' => $this->request->data['Task']['title']);
            $results = $httpsocket->post('http://nlp.mobilt.co/meet', $data);
            if ($results->isOk()) {
                $response = json_decode($results->body());

                /* for task array */
                $this->request->data['Task']['title'] = $data['data'];

                if (isset($response->date) && strcmp($response->date, "not specified")) {
                    $this->request->data['Task']['start_date'] = date('Y-m-d H:i:s', strtotime($response->date));
                }
                if (isset($response->date_start) && strcmp($response->date, "not spacified")) {
                    $this->request->data['Task']['start_date'] = date('Y-m-d H:i:s', strtotime($response->date_start));
                }

                /* for task assignment array contact 1 */


                $this->request->data['TaskAssignment']['assignee_data'][0]['name'] = $response->contact;

                $group = $this->Group->findByNameAndNetworkId($response->contact, $network_id, array('Group.id'));
                if (isset($group['Group']['id'])) {

                    $this->request->data['TaskAssignment']['assignee_data'][0]['type'] = 'group';
                    $this->request->data['TaskAssignment']['assignee_data'][0]['value'] = $group['Group']['id'];
                } else {

                    $user = $this->UserInfo->findByFirst_name($response->contact);

                    if (($user)) {
                        $this->request->data['TaskAssignment']['assignee_data'][0]['type'] = 'user';
                        $this->request->data['TaskAssignment']['assignee_data'][0]['value'] = $user['UserInfo']['user_id'];
                        $this->request->data['TaskAssignment']['assignee_data'][0]['img'] = $user['UserInfo']['photo'];
                    }
                }
                if (!isset($this->request->data['TaskAssignment']['assignee_data'][0]['value'])) {
                    $this->request->data['TaskAssignment']['assignee_data'][0] = null;
                }

                /* for task assignment array contact 2 */
                if (isset($response->contact_2)) {
                    $this->request->data['TaskAssignment']['assignee_data'][1]['name'] = $response->contact_2;
                    $group = $this->Group->findByNameAndNetworkId($response->contact_2, $network_id, array('Group.id'));
                    if (isset($group['Group']['id'])) {

                        $this->request->data['TaskAssignment']['assignee_data'][1]['type'] = 'group';
                        $this->request->data['TaskAssignment']['assignee_data'][1]['value'] = $group['Group']['id'];
                    } else {
                        $user = $this->UserInfo->findByFirst_name($response->contact_2, array('UserInfo.user_id,UserInfo.photo'));
                        $this->request->data['TaskAssignment']['assignee_data'][1]['type'] = 'user';
                        $this->request->data['TaskAssignment']['assignee_data'][1]['value'] = $user['UserInfo']['user_id'];
                        $this->request->data['TaskAssignment']['assignee_data'][1]['img'] = $user['UserInfo']['photo'];
                    }
                }

                if (!isset($this->request->data['TaskAssignment']['assignee_data'][1]['value'])) {
                    $this->request->data['TaskAssignment']['assignee_data'][1] = null;
                }
                $this->add();
            } else {
                throw new Exception('Some error occured. Please try again later.', 403);
            }
        }
    }

//    public function add_old() {
//        $this->log('$this->request->data-------------');
//        $this->log($this->request->data);
//        $this->viewClass = 'Json';
//        $this->request->onlyAllow('post', 'delete');
//        $current_network_id = $this->Session->read('CurrentNetwork');
//        $auth_user_id = $this->Auth->user('id');
//        $timezone = abs($this->Auth->user('timezone'));
////        if(isset($this->request->data['email'])){
////            $task = $this->requestAction('/users/admin_add');
////            $this->log("i m back");
////            $this->log($task);
////            
////        }
//        
//        $current_date = new DateTime();
//        /* Playing with Dates */
//        if (!isset($this->request->data['Task']['start_date'])) {
//            $this->request->data['Task']['start_date'] = $current_date->format('Y:m:d H:i:00');
//        }
//
//
//        if (!isset($this->request->data['Task']['end_date'])) {
//            $end_date = new DateTime($this->request->data['Task']['start_date']);
//            $end_date->add(new DateInterval('PT1H'));
//            $this->request->data['Task']['end_date'] = $end_date->format('Y:m:d H:i:00');
//        }
//
//
//        $this->request->data['Task']['network_id'] = $current_network_id;
//        $this->request->data['Task']['created_by'] = $auth_user_id;
//        $this->request->data['Task']['priority'] = (isset($this->request->data['Task']['priority']) && $this->request->data['Task']['priority'] == true) ? 1 : 0;
//
//
//
//        $this->_setAssignee();
//        $this->log('$this->request->data-------------new----');
//        $this->log($this->request->data);
//        array_push($this->request->data['TaskAssignment'], array(
//            'network_id' => $current_network_id,
//            'user_id' => $auth_user_id,
//            'group_id' => 0,
//            'role' => ROLE_ADMIN,
//            'added_by' => $auth_user_id
//                )
//        );
//        $before_save_timestamp = time();
////        $this->log($this->request->data);
//        if ($this->Task->saveAll($this->request->data)) {
//            /* Dispatching events */
//            $TaskEvent = new CakeEvent(
//                    'Controller.Task.afterAdd', $this, array('user_id' => $auth_user_id, 'network_id' => $current_network_id, 'task_id' => $this->Task->id)
//            );
//            $this->getEventManager()->dispatch($TaskEvent);
//
//            if (count($this->request->data['TaskAssignment']) > 1) {
////                $this->log('before mail--------------');
//                CakeResque::enqueue('default', 'TaskShell', array('TaskAdd', array(
//                        'id' => $this->Task->id,
//                        'before_save_timestamp' => $before_save_timestamp,
//                        'assigned_by' => $this->Auth->user('UserInfo.first_name'),
//                        'assigned_by_email' => $this->Auth->user('email'),
//                    'url' => Router::url(array('admin' => false, 'plugin' => 'users', 'controller' => 'users', 'action' => 'login'), true)
//                )));
//            }
//            $schedule_time = new DateTime($this->request->data['Task']['end_date']);
//            $schedule_time->sub(new DateInterval('PT' . TASK_REMINDER_TIME . 'S'));
//
//            if (count($this->request->data['TaskAssignment']) > 1 && $schedule_time->getTimestamp() >= time()) {                
//                $job_id = CakeResque::enqueueAt($schedule_time->getTimestamp(), 'default', 'TaskShell', array('TaskDeadlineReminder', array(
//                                'id' => $this->Task->id,
//                                'end_date' => $this->request->data['Task']['end_date'],
//                                'url' => Router::url(array('admin' => false, 'plugin' => 'users', 'controller' => 'users', 'action' => 'login'), true)
//                            )), true);
//                if ($job_id) {
//
//                    $statusName = array(
//                        Resque_Job_Status::STATUS_WAITING => __d('cake_resque', 'waiting'),
//                        Resque_Job_Status::STATUS_RUNNING => __d('cake_resque', 'running'),
//                        Resque_Job_Status::STATUS_FAILED => __d('cake_resque', 'failed'),
//                        Resque_Job_Status::STATUS_COMPLETE => __d('cake_resque', 'complete')
//                    );
//
//
//                    if (Configure::read('CakeResque.Scheduler.enabled') === true) {
//                        $statusName[ResqueScheduler\Job\Status::STATUS_SCHEDULED] = __d('cake_resque', 'scheduled');
//                    }
//
//                    $job_status = CakeResque::getJobStatus($job_id);
//                    $status = isset($statusName[$job_status]) ? $statusName[$job_status] : 'unknown';
//
//                    $data = array(
//                        'Cronjob' => array(
//                            'job_id' => $job_id,
//                            'name' => 'TaskDeadlineReminder',
//                            'schedule_date' => $schedule_time->format('Y:m:d H:i:s'),
//                            'status' => $status
//                        )
//                    );
//
//
//
//                    $this->loadModel('Cronjob');
//                    $this->Cronjob->create();
//                    $this->Cronjob->save($data);
//                }
//            }
//
//
//            if (count($this->request->data['TaskAssignment']) > 0) {
//
//                $list = array();
//                $list['group'] = array_unique(Hash::extract($this->request->data['TaskAssignment'], '{n}[group_id!=0].group_id'));
//                $list['user'] = Hash::extract($this->request->data['TaskAssignment'], '{n}[group_id<=0].user_id');
//                $TaskAssignmentEvent = new CakeEvent(
//                        'Controller.Task.afteraddmember', $this, array(
//                    'user_id' => $auth_user_id,
//                    'network_id' => $current_network_id,
//                    'task_id' => $this->Task->id,
//                    'listgroup' => $list['group'],
//                    'listuser' => $list['user']
//                        )
//                );
//                $this->getEventManager()->dispatch($TaskAssignmentEvent);
//            }
//            $tasks = $this->Task->gets($this->Task->id);
//            $this->log('$tasks/////////////////////////');
//            $this->log($tasks);
//            $tasks = $this->lists($tasks);
//            $this->log('$tasks------------------------------');
//            $this->log($tasks);
//            $returnObject['status'] = 'success';
//            $returnObject['message'] = 'Task created successfully.';
//            $returnObject['data'] = $tasks;
//            $this->set(array(
//                'tasks' => $returnObject,
//                '_serialize' => 'tasks'
//            ));
//             
//        } else {
//            $messages = array_values($this->Task->validationErrors);
//            if (isset($messages[0])) {
//                throw new Exception($messages[0], 400);
//            } else {
//                throw new Exception('Some error occured.', 400);
//            }
//        }
//    }
    
    
    protected function _sendMailForTask($type_of_task, $task_id = null){
        
        switch ($type_of_task){
            
            case TASKADD:
                $this->_sendMailForTaskAdd();
                break;
            
            case TASKEDIT:
                $this->_sendMailForTaskEdit($task_id);
                break;
            
            default :
                break;
        }

        $schedule_time = new DateTime($this->request->data['Task']['end_date']);
        $schedule_time->sub(new DateInterval('PT' . TASK_REMINDER_TIME . 'S'));
        
        if (count($this->request->data['TaskAssignment']) > 0 && $schedule_time->getTimestamp() >= time()){  
            $job_id = CakeResque::enqueueAt($schedule_time->getTimestamp(), 'default', 'TaskShell', array('TaskDeadlineReminder', array(
                        'id' => $this->Task->id,
                        'end_date' => $this->request->data['Task']['end_date'],
                        'url' => Router::url(array('admin' => false, 'plugin' => 'users', 'controller' => 'users', 'action' => 'login'), true)
                            )), true);
        if ($job_id) {
            $this->_setCronJob($job_id, $schedule_time);
                }
            }
    }
    
    protected function _sendMailForTaskAdd(){
        $before_save_timestamp = time();
        if (count($this->request->data['TaskAssignment']) > 1) {
            CakeResque::enqueue('default', 'TaskShell', array('TaskAdd', array(
                        'id' => $this->Task->id,
                        'before_save_timestamp' => $before_save_timestamp,
                        'assigned_by' => $this->Auth->user('UserInfo.first_name'),
                        'assigned_by_email' => $this->Auth->user('email'),
                        'assigned_by_userid' => $this->Auth->user('id'),
                    'url' => Router::url(array('admin' => false, 'plugin' => 'users', 'controller' => 'users', 'action' => 'login'), true)
                )));
            }
    }
    
    protected function _sendMailForTaskEdit($task_id){
        $before_save_timestamp = time();
        /* Saving old task data to check which field have been edited for mail */
        $task_unedited = $this->Task->findById($task_id);
        
        #get unedited userinfo
        $user_unedited = $this->getUserAssignedByTask($task_id);
        
         #send mail for task edit
        CakeResque::enqueue('default', 'TaskShell', array('TaskEdit', array(
                    'task_edited' => $this->Task->findById($this->Task->id),
                    'task_unedited' => $task_unedited,
                'edited_by' => $this->Session->read('Auth.User.UserInfo.first_name'),
                'old_user_all'=> $user_unedited,
                 'url' => Router::url(array('admin' => false, 'plugin' => 'users', 'controller' => 'users', 'action' => 'login'), true)
                )));
        
         #send mail for new assigny for task    
        if (count($this->request->data['TaskAssignment']) > 0) {

                CakeResque::enqueue('default', 'TaskShell', array('TaskAdd', array(
                        'id' => $this->Task->id,
                        'before_save_timestamp' => $before_save_timestamp,
                        'assigned_by' => $this->Auth->user('UserInfo.name'),
                        'assigned_by_email' => $this->Auth->user('email'),
                    'url' => Router::url(array('admin' => false, 'plugin' => 'users', 'controller' => 'users', 'action' => 'login'), true)
                )));
        }
    }

    
    protected function _setEventForTask($current_network_id, $current_user_id, $type_of_task){
         
         # Dispatching events 
        $TaskEvent = new CakeEvent(
                    'Controller.Task.after'.$type_of_task, $this, array('user_id' => $current_user_id, 'network_id' => $current_network_id, 'task_id' => $this->Task->id));
            
        $this->getEventManager()->dispatch($TaskEvent);
        
        switch ($type_of_task){
            
            case TASKADD:   
                $condition_of_assignee = count($this->request->data['TaskAssignment']) > 1;
                break;
            
            case TASKEDIT:
                $condition_of_assignee = count($this->request->data['TaskAssignment']) > 0; 
                break;
            
            default :
                break;
        }
        
        if($condition_of_assignee){
//        if (($type_of_task == TASKADD && count($this->request->data['TaskAssignment']) > 1) || ($type_of_task == TASKEDIT && count($this->request->data['TaskAssignment']) > 0)) {

                $list = array();
                $list['group'] = array_unique(Hash::extract($this->request->data['TaskAssignment'], '{n}[group_id!=0].group_id'));
                $list['user'] = Hash::extract($this->request->data['TaskAssignment'], '{n}[group_id<=0].user_id');
                $TaskAssignmentEvent = new CakeEvent(
                        'Controller.Task.afteraddmember', $this, array(
                    'user_id' => $current_user_id,
                    'network_id' => $current_network_id,
                    'task_id' => $this->Task->id,
                    'listgroup' => $list['group'],
                    'listuser' => $list['user']
                        )
                );
                $this->getEventManager()->dispatch($TaskAssignmentEvent);
            }
    }

        public function add(){
        
        $this->viewClass = 'Json';
        $this->request->onlyAllow('post', 'delete');
        $current_network_id = $this->Session->read('CurrentNetwork');
        $current_user_id = $this->Auth->user('id');
        $this->request->data['Task']['network_id'] = $current_network_id;
        $this->request->data['Task']['created_by'] = $current_user_id;
        $this->request->data['Task']['priority'] = (isset($this->request->data['Task']['priority']) && $this->request->data['Task']['priority'] == true) ? SET : RESET;
        
        $this->_setStartAndEndDateOfTask();
        $this->_setAssignee();
        $this->_assignCreaterToTask($current_network_id, $current_user_id);
        
        if ($this->Task->saveAll($this->request->data)) {
            $this->_sendMailForTask(TASKADD);
            $this->_setEventForTask($current_network_id, $current_user_id, TASKADD);
            
            $task_list = $this->Task->gets($this->Task->id,$current_user_id);
            if(isset($task_list) && !empty($task_list)){
                $task_list_with_userinfo = $this->lists($task_list);
            }
            
            $returnObject = array(
                'status' => 'success',
                'message' => 'Task created successfully.',
                'data' => $task_list_with_userinfo
            );
            $this->set(array(
                'tasks' => $returnObject,
                '_serialize' => 'tasks'
            ));
        }else {
            $messages = array_values($this->Task->validationErrors);
            if (isset($messages[0])) {
                throw new Exception($messages[0], 400);
            } else {
                throw new Exception('Some error occured.', 400);
            }
        }
    }
    /**
     * it checks the starting and ending time of task 
     * and if it is null then it sets starting and ending time of task 
     */
    protected function _setStartAndEndDateOfTask(){
        
        $current_date = new DateTime();
        /* Playing with Dates */
        if (!isset($this->request->data['Task']['start_date'])) {
            $this->request->data['Task']['start_date'] = $current_date->format('Y:m:d H:i:s');
        }

        if (!isset($this->request->data['Task']['end_date'])) {
            $end_date = new DateTime($this->request->data['Task']['start_date']);
            $end_date->add(new DateInterval('PT1H'));
            $this->request->data['Task']['end_date'] = $end_date->format('Y:m:d H:i:s');
        }
    }

    public function edit($task_id = null){
        
        $this->viewClass = 'Json';
        $this->request->onlyAllow('post', 'delete');
        $current_network_id = $this->Session->read('CurrentNetwork');
        $current_user_id = $this->Auth->user('id');
        $this->request->data['Task']['id'] = $task_id;
        $this->request->data['Task']['priority'] = (isset($this->request->data['Task']['priority']) && $this->request->data['Task']['priority'] == "true") ? SET : RESET;
    
        if (!$this->Task->exists($task_id)) {
            throw new Exception('Task not found', 400);
        }
        
        #  Allow edit on not completed tasks
        if ($this->Task->getField($task_id, 'is_completed')) {
            throw new Exception('Completed task cannot be edited.', 400);
        }
        
        $this->_setStartAndEndDateOfTask();
        $this->_setAssignee($task_id);
        if ($this->Task->saveAll($this->request->data)) {
            $this->_sendMailForTask(TASKEDIT, $task_id);
            $this->_setEventForTask($current_network_id, $current_user_id, TASKEDIT);
            
            $task_list = $this->Task->gets($this->Task->id, $current_user_id);
            if(isset($task_list) && !empty($task_list)){
                $task_list_with_userinfo = $this->lists($task_list);
            }
            
            $returnObject = array(
                'status' => 'success',
                'message' => 'Task edited successfully.',
                'data' => $task_list_with_userinfo
            );
            $this->set(array(
                'tasks' => $returnObject,
                '_serialize' => 'tasks'
            ));
            
        }else {
            $messages = array_values($this->Task->validationErrors);
            throw new Exception($messages[0][0], 400);
        }
    }



//    public function edit_old($id = null) {
//
////        $this->log('$this->request->data---------------------------');
////        $this->log($this->request->data);
//        $this->viewClass = 'Json';
//        $this->request->onlyAllow('post', 'delete');
//        $current_network_id = $this->Session->read('CurrentNetwork');
//        $auth_user_id = $this->Auth->user('id');
//        $timezone = abs($this->Auth->user('timezone'));
//        $this->request->data['Task']['id'] = $id;
//
//        if (!$this->Task->exists($id)) {
//            throw new Exception('Task not found', 400);
//        }
//
//        /*
//         * Allow edit on not completed tasks
//         */
//        if ($this->Task->getField($id, 'is_completed')) {
//            throw new Exception('Completed task cannot be edited.', 400);
//        }
//
//        /* Saving old task data to check which field have been edited for mail */
//        $task_unedited = $this->Task->findById($id);
//        $current_date = new DateTime();
//        
//        #get unedited userinfo
//        $users = $this->getUserAssignedByTask($id);
//        
//        /* Playing with Dates */
//        if (!isset($this->request->data['Task']['start_date'])) {
//            $this->request->data['Task']['start_date'] = $current_date->format('Y:m:d H:i:00');
//        }
//
//
//        if (!isset($this->request->data['Task']['end_date'])) {
//            $end_date = new DateTime($this->request->data['Task']['start_date']);
//            $end_date->add(new DateInterval('PT1H'));
//            $this->request->data['Task']['end_date'] = $end_date->format('Y:m:d H:i:00');
//        }
//
//        $this->request->data['Task']['priority'] = (isset($this->request->data['Task']['priority']) && $this->request->data['Task']['priority'] == "true") ? 1 : 0;
//
//        $this->_setAssignee($id);
//
//
//        $before_save_timestamp = time();
//
//        if ($this->Task->saveAll($this->request->data)) {
//            /* Dispatching events */
//            $TaskEvent = new CakeEvent(
//                    'Controller.Task.afterEdit', $this, array('user_id' => $auth_user_id, 'network_id' => $current_network_id, 'task_id' => $this->Task->id)
//            );
//            $this->getEventManager()->dispatch($TaskEvent);
//            #send mail for task edit
//            CakeResque::enqueue('default', 'TaskShell', array('TaskEdit', array(
//                    'task_edited' => $this->Task->findById($this->Task->id),
//                    'task_unedited' => $task_unedited,
//                'edited_by' => $this->Session->read('Auth.User.UserInfo.first_name'),
//                'old_user_all'=> $users,
//                 'url' => Router::url(array('admin' => false, 'plugin' => 'users', 'controller' => 'users', 'action' => 'login'), true)
//                )));
//
//            $schedule_time = new DateTime($this->request->data['Task']['end_date']);
//            $schedule_time->sub(new DateInterval('PT' . TASK_REMINDER_TIME . 'S'));
//
//
//            if (count($this->request->data['TaskAssignment']) > 1 && $schedule_time->getTimestamp() >= time()) {
//                $job_id = CakeResque::enqueueAt($schedule_time->getTimestamp(), 'default', 'TaskShell', array('TaskDeadlineReminder', array(
//                                'id' => $this->Task->id,
//                                'end_date' => $this->request->data['Task']['end_date'],
//                                 'url' => Router::url(array('admin' => false, 'plugin' => 'users', 'controller' => 'users', 'action' => 'login'), true)
//                            )), true);
//                if ($job_id) {
//
//                    $statusName = array(
//                        Resque_Job_Status::STATUS_WAITING => __d('cake_resque', 'waiting'),
//                        Resque_Job_Status::STATUS_RUNNING => __d('cake_resque', 'running'),
//                        Resque_Job_Status::STATUS_FAILED => __d('cake_resque', 'failed'),
//                        Resque_Job_Status::STATUS_COMPLETE => __d('cake_resque', 'complete')
//                    );
//
//
//                    if (Configure::read('CakeResque.Scheduler.enabled') === true) {
//                        $statusName[ResqueScheduler\Job\Status::STATUS_SCHEDULED] = __d('cake_resque', 'scheduled');
//                    }
//
//                    $job_status = CakeResque::getJobStatus($job_id);
//                    $status = isset($statusName[$job_status]) ? $statusName[$job_status] : 'unknown';
//
//                    $data = array(
//                        'Cronjob' => array(
//                            'job_id' => $job_id,
//                            'name' => 'TaskDeadlineReminder',
//                            'schedule_date' => $schedule_time->format('Y:m:d H:i:s'),
//                            'status' => $status
//                        )
//                    );
//
//
//
//                    $this->loadModel('Cronjob');
//                    $this->Cronjob->create();
//                    $this->Cronjob->save($data);
//                }
//            }
//
//            $this->log('$this->request->data[TaskAssignment]----------------edit-------------');
//            $this->log($this->request->data['TaskAssignment']);
//
//            if (count($this->request->data['TaskAssignment']) > 0) {
//
//                CakeResque::enqueue('default', 'TaskShell', array('TaskAdd', array(
//                        'id' => $this->Task->id,
//                        'before_save_timestamp' => $before_save_timestamp,
//                        'assigned_by' => $this->Auth->user('UserInfo.name'),
//                        'assigned_by_email' => $this->Auth->user('email'),
//                    'url' => Router::url(array('admin' => false, 'plugin' => 'users', 'controller' => 'users', 'action' => 'login'), true)
//                )));
//
//                $list = array();
//                $list['group'] = array_unique(Hash::extract($this->request->data['TaskAssignment'], '{n}[group_id!=0].group_id'));
//                $list['user'] = Hash::extract($this->request->data['TaskAssignment'], '{n}[group_id<=0].user_id');
//                $TaskAssignmentEvent = new CakeEvent(
//                        'Controller.Task.afteraddmember', $this, array(
//                    'user_id' => $auth_user_id,
//                    'network_id' => $current_network_id,
//                    'task_id' => $this->Task->id,
//                    'listgroup' => $list['group'],
//                    'listuser' => $list['user']
//                        )
//                );
//                $this->getEventManager()->dispatch($TaskAssignmentEvent);
//            }
//            $tasks = $this->Task->gets($this->Task->id);
//            $tasks = $this->lists($tasks);
//            $returnObject['message'] = 'Task edited successfully.';
//            $returnObject['data'] = $tasks;
//
//            $this->set(array(
//                'tasks' => $returnObject,
//                '_serialize' => 'tasks'
//            ));
//        } else {
//            $messages = array_values($this->Task->validationErrors);
//            throw new Exception($messages[0][0], 400);
//        }
//    }
    
    public function getUserAssignedByTask($id = null){
        $this->loadModel('UserInfo');
            
        $this->TaskAssignment->bindModel(
                array('hasOne' => array(
                        'UserInfo' => array(
                            'className' => 'UserInfo',
                            'foreignKey' => false,
                            'conditions' => array('UserInfo.user_id = `TaskAssignment`.`user_id`')
                        )
                    )
                )
        );

        $current_time = new DateTime();
        $current_time->sub(new DateInterval('PT' . Edit_User_Time . 'S'));

        $task_assignments = $this->TaskAssignment->find('all', array(
            'conditions' => array(
                'TaskAssignment.task_id' => $id,
                'TaskAssignment.role' => ROLE_USER,
//                'OR'=>array(
//                    'TaskAssignment.deleted >' => $current_time->format('Y-m-d H:i:s'),
//                    'TaskAssignment.deleted' => NULL
//                ),
//                'TaskAssignment.isDeleted' => array(DELETED,NOT_DELETED),
//                'TaskAssignment.status' => array(STATUS_ACTIVE,STATUS_REMOVED),
//                'TaskAssignment.created >=' => date('Y-m-d H:i:s', $data['before_save_timestamp'])
            ),
            'contain' => array(
                'UserInfo'
            ),
            'fields' => array(
                'DISTINCT TaskAssignment.user_id',
                'UserInfo.*',
            )
        ));

        $users = array();
        foreach ($task_assignments as $value) {
            $name = array(
                'user' => $value['UserInfo']['first_name'].' '.$value['UserInfo']['last_name']);
            array_push($users, $name);
            
        }
        return Hash::extract($users,'{n}.user');
    }

    public function setComplete($task_id, $action) {

        $this->viewClass = 'Json';

        /* Allowing only Post and Delete method */
        $this->request->onlyAllow('post', 'delete');
        $returnObject = array();
        $network_id = $this->Session->read('CurrentNetwork');
        switch ($action) {
            case 'complete':
                $status = COMPLETE;
                $Event = new CakeEvent(
                        'Controller.Task.aftersetComplete', $this, array('user_id' => $this->Auth->user('id'), 'task_id' => $task_id, 'network_id' => $network_id)
                );
                break;
            case 'incomplete';
                $status = INCOMPLETE;
                $Event = new CakeEvent(
                        'Controller.Task.aftersetUncomplete', $this, array('user_id' => $this->Auth->user('id'), 'task_id' => $task_id, 'network_id' => $network_id)
                );
                break;
        }

        if (isset($status)) {
            if ($this->TaskAssignment->getField($task_id, $this->Auth->user('id'), 'status') !== STATUS_ACTIVE) {
                throw new Exception('You cannot update this task.', 400);
            } else if ($this->Task->setComplete($task_id, $status, $this->Auth->user('id'))) {
                $this->getEventManager()->dispatch($Event);
                $returnObject['message'] = 'Task submitted successfully.';
                $tasks = $this->Task->gets($task_id, $this->Auth->user('id'));
                $returnObject['data'] = $this->lists($tasks);
                $this->set(array(
                    'tasks' => $returnObject,
                    '_serialize' => 'tasks'
                ));

                if ($status == COMPLETE) {
                    CakeResque::enqueue('default', 'TaskShell', array('TaskComplete', array(
                            'id' => $task_id,
                            'user_id' => $this->Auth->user('id'),
                        'url' => Router::url(array('admin' => false, 'plugin' => 'users', 'controller' => 'users', 'action' => 'login'), true)
                    )));
                }
            } else {
                throw new Exception($this->Task->validationErrors, 400);
            }
        } else {
            throw new Exception('Failed to update task.', 400);
        }
    }

    /**
     * removeAssignee: this function is used to remove assignee
     * @author Hardik Sondagar
     * @param integer task_id
     * @param array remove_list
     */
    public function removeAssignee($task_id = NULL) {


        $this->viewClass = 'Json';
        /* Allowing only Post and Delete method */
        $this->request->onlyAllow('post', 'delete');
        $user_id = $this->Auth->user('id');
        $network_id = $this->Session->read('CurrentNetwork');
        $remove_data = $this->request->data['TaskAssignment']['remove_data'];
        $task_id = $this->request->data['TaskAssignment']['task_id'];
        /* Check : Is User creator of this task? */
        if (count($remove_data) == 2 && $this->TaskAssignment->isCreator($task_id, $user_id) > 0) {
            /* Delete the task */

            if ($this->TaskAssignment->removeAssignee($task_id, $remove_data['value'], $remove_data['type'], $user_id)) {

                $sharelist_final['user'] = [];
                $sharelist_final['group'] = [];
                array_push($sharelist_final[$remove_data['type']], $remove_data['value']);
                $Event = new CakeEvent('Controller.Task.aftermemberremove', $this, array('user_id' => $user_id, 'network_id' => $network_id, 'task_id' => $task_id, 'userlist' => $sharelist_final['user'], 'grouplist' => $sharelist_final['group']));
                $this->getEventManager()->dispatch($Event);

                $tasks = $this->Task->gets($task_id, $user_id);
            $tasks = $this->lists($tasks);
                
                $returnObject['message'] = 'Assignee/s removed successfully.';
                $returnObject['data'] = $tasks;
                $this->set(array(
                    'tasks' => $returnObject,
                    '_serialize' => 'tasks'
                ));
                return;
            } else {
                throw new Exception('Failed to remove assignee/s.', 400);
            }
        } else {
            throw new Exception('You are not authorized to remove assignee/s.', 400);
        }
    }

    public function delete($task_id = NULL) {

        $this->viewClass = 'Json';
        /* Allowing only Post and Delete method */
        $this->request->onlyAllow('post', 'delete');
        $returnObject;

        $user_id = $this->Auth->user('id');
        $network_id = $this->Session->read('CurrentNetwork');

        /* Delete the task */
        if ($this->Task->TaskDelete($task_id, $user_id)) {
            $Event = new CakeEvent('Controller.Task.afterdelete', $this, array('user_id' => $user_id, 'network_id' => $network_id, 'task_id' => $task_id));
            $this->getEventManager()->dispatch($Event);
            $returnObject['message'] = 'Task deleted successfully.';
            $returnObject['data'] = $this->Task->gets($task_id, $user_id);
            $this->set(array(
                'tasks' => $returnObject,
                '_serialize' => 'tasks'
            ));
            return;
        } else {
            throw new Exception('Failed to delete task.', 400);
        }
    }
/**
 * option = pending,today,this_week,upcoming,completed,upto,all,other
 * type = user or group or search string
 * it is main method for displaying task of ->single user
 *                                          ->rendome user
 *                                          ->rendome group
 * @param type $option
 */
    public function getAssignedTask($option = null, $type = null,$page = 1 ,$id = null) {
//        $this->log("in get assigned tasks");
        $this->viewClass = 'Json';
        /* Allowing only Post and Delete method */
        $user_cur = $this->Auth->user('id');
        if(empty($id)){
            $id = $this->Auth->user('id');
        }
        $moreTag = FALSE;
        $network_id = $this->Session->read('CurrentNetwork');
        /*fetch tasks of this perticuler type */
        $tasks = $this->Task->getTasks($user_cur, $network_id, $option, $page, $id, $type);
        $task_count = $this->Task->getTaskCount($user_cur, $network_id, $option, $id, $type);
        /*set metadata of task*/
        $tasks = $this->lists($tasks);
        
        if(isset($tasks[Task_To_Load-1])){
            $moreTag = TRUE;
        }
        $returnObject = array(
            'message' => 'loaded Successfully',
            'data' => $tasks,
            'type'=> $option,
            'count' => $task_count,
            'loadMoreTag' => $moreTag
        );
        $this->set(array(
            'tasks' => $returnObject,
            '_serialize' => 'tasks'
        ));
        
    }
    /**
     * set commment metadata of task
     * @param type $tasks
     * @return type
     */
    public function lists($tasks = null){
        foreach ($tasks as $key => $value) {
            $tasks[$key]['user_info'] = $this->UserInfo->findByUser_id($tasks[$key]['Task']['created_by'],array('UserInfo.user_id', 'UserInfo.first_name', 'UserInfo.last_name', 'UserInfo.name', 'UserInfo.vga', 'UserInfo.thumb', 'UserInfo.xvga','UserInfo.email'));
            $tasks[$key]['user_info'] = $tasks[$key]['user_info']['UserInfo'];
            
            if(isset($tasks[$key]['Comment'])){
                foreach ($tasks[$key]['Comment'] as $k => $v) {
                    $tasks[$key]['Comment'][$k]['user_info'] = $this->UserInfo->findByUser_id($tasks[$key]['Comment'][$k]['user_id'],array('UserInfo.user_id', 'UserInfo.first_name', 'UserInfo.last_name', 'UserInfo.name', 'UserInfo.vga', 'UserInfo.thumb', 'UserInfo.xvga','UserInfo.email'));
            
            $tasks[$key]['Comment'][$k]['user_info'] = $tasks[$key]['Comment'][$k]['user_info']['UserInfo'];      
                }
            }
        }
        return $tasks;
        
        
    }
    
        public function view($task_id = null) {
        $data['user'] = $this->Auth->user('id');
        $data['id'] = $task_id;
        if (!$this->Task->isPartOfTask($data)) {
            $this->Session->setFlash("Page could have been removed or not found");
            $this->redirect($this->referer());
        }
    }
/**
 * get individual task by its id.
 * @param type $task_id
 * @throws Exception
 */
    public function getTask($task_id = NULL) {

        $this->viewClass = 'Json';
        $user_id = $this->Auth->user('id');
        $network_id = $this->Session->read('CurrentNetwork');

        if ($this->TaskAssignment->getField($task_id, $user_id, 'network_id') === $network_id) {
            $doc = $this->Task->gets($task_id, $user_id);
            $tasks = $this->lists($doc);
            $returnObject = array(
                'message' => 'Task Fetched',
                'data' => $tasks
            );
            $this->set(array(
                'tasks'=> $returnObject,
                 '_serialize' => 'tasks'));
            
        } else {
            throw new Exception('You dont have any such task', 400);
        }
    }

    /*     * ********************* MOBILE APP *************************** */

    /**
     * Get tasks of a user depending on the options
     */
    public function getTasks() {

        $PARAM_USER = 'user';
        $PARAM_NETWORK = 'network';
        $PARAM_TYPE = 'option';
        $PARAM_PAGE = 'page';
        $PARAM_TIMEZONE = 'timezone';

        $this->setJson();
        $this->setTypeGet();

        //CHECKING THE QUERY PARAMETERS

        if (!$this->queryHas($PARAM_USER) || !$this->queryHas($PARAM_NETWORK) || !$this->queryHas($PARAM_TYPE) || !$this->queryHas($PARAM_TIMEZONE)) {
            $this->errMissingParameter();
            return;
        }

        //COLLECTING THE QUERY PARAMETERS

        $user_id = $this->getQueryParam($PARAM_USER);
        $network_id = $this->getQueryParam($PARAM_NETWORK);
        $option = $this->getQueryParam($PARAM_TYPE);
        $timezone = $this->getQueryParam($PARAM_TIMEZONE);

        //VALIDATING THE QUERY PARAMETERS
        //Validating the parameters
        if (!is_numeric($user_id) || !is_numeric($network_id) || !is_numeric($timezone)) {
            $this->errInvalidParameter();
            return;
        }

        if (!$this->validateTaskOption($option)) {
            $this->errInvalidParameter();
            return;
        }

        //Fetching Data  
        if ($this->queryHas($PARAM_PAGE)) {
            $page = $this->getQueryParam($PARAM_PAGE);
            $tasks = $this->Task->getTasks($user_id, $network_id, $option, $page);
        } else {
            $tasks = $this->Task->getTasks($user_id, $network_id, $option);
        }

        $dates = array('start_date', 'end_date', 'completed');
//        $this->log("Before Timezone");
//        $this->log($tasks);
        $i = 0;
        foreach ($tasks as $task) {
            foreach ($dates as $value) {
                if (isset($task['Task'][$value])) {
                    $date = new DateTime($task['Task'][$value]);
                    if ($timezone > 0) {
                        $date->sub(new DateInterval('PT' . abs($timezone) . 'M'));
                    } else {
                        $date->add(new DateInterval('PT' . abs($timezone) . 'M'));
                    }
                    $tasks[$i]['Task'][$value] = $date->format('Y:m:d H:i:s');
                }
            }
            $i++;
        }


        //RESULT DATA
        $returnObject['status'] = 'success';
        $returnObject['data'] = $tasks;
        $this->set(array(
            'data' => $returnObject,
            '_serialize' => 'data'
        ));
    }

    /**
     * checks if the option provided for receiving the tasks is valid or not
     * 
     * 1. pending,
     * 2. todays,
     * 3. next week and
     * 4. this week
     * 
     */
    public function validateTaskOption($option) {

        $valid = false;

        switch ($option) {

            case "pending" :

                $valid = true;

                break;

            case "today" :

                $valid = true;

                break;
            //Removed - has to be removed from code
            case "this_week" :

                $valid = true;

                break;
            //Removed - has to be removed from code
            case "next_week" :

                $valid = true;

                break;
            case "upcoming" :

                $valid = true;

                break;

            case "completed":

                $valid = true;

                break;
        }

        return $valid;
    }

    /**
     * Function for mobile app to set task status to complete or incomplete
     */
    public function setTaskComplete() {

        $this->setJson();
        $this->request->allowMethod('post', 'put');

//        $this->log("1");
//        $this->log($this->request->input());

        $data = $this->request->input();
        $data = json_decode($data, true);

//        $this->log("2:");
//        $this->log($data);
        //COLLECTING THE QUERY PARAMETERS
        $user_id = $data['user_id'];
        $network_id = $data['network_id'];
        $task_id = $data['task_id'];
        $action = $data['action'];

        //Validating the parameters
        if (!is_numeric($user_id) || !is_numeric($network_id) || !is_numeric($task_id) || is_numeric($action)) {
            $this->errInvalidParameter();
            return;
        }

        switch ($action) {
            case 'complete':
                $status = COMPLETE;
                $Event = new CakeEvent(
                        'Controller.Task.aftersetComplete', $this, array('user_id' => $user_id, 'task_id' => $task_id, 'network_id' => $network_id)
                );
                break;
            case 'incomplete';
                $status = INCOMPLETE;
                $Event = new CakeEvent(
                        'Controller.Task.aftersetUncomplete', $this, array('user_id' => $this->Auth->user('id'), 'task_id' => $task_id, 'network_id' => $network_id)
                );
                break;
        }

        if (isset($status)) {
            if ($this->TaskAssignment->getField($task_id, $user_id, 'status') !== STATUS_ACTIVE) {
                $returnObject['status'] = "error";
                $returnObject['err_msg'] = 'You cannot update this task.';
                $this->set(array(
                    'data' => $returnObject,
                    '_serialize' => 'data'
                ));
            } else if ($this->Task->setComplete($task_id, $status, $user_id)) {
                $this->getEventManager()->dispatch($Event);
                $returnObject['message'] = 'Task submitted successfully.';
                $returnObject['status'] = 'success';
                $returnObject['data'] = Set::ClassicExtract($this->Task->gets($task_id, $user_id), '{n}.Task.{(id|title|is_completed)}');
                $returnObject['data'] = $returnObject['data'][0];
                $this->set(array(
                    'tasks' => $returnObject,
                    '_serialize' => 'tasks'
                ));

                if ($status == COMPLETE) {
                    CakeResque::enqueue('default', 'TaskShell', array('TaskComplete', array(
                            'id' => $task_id,
                            'user_id' => $user_id,
                        'url' => Router::url(array('admin' => false, 'plugin' => 'users', 'controller' => 'users', 'action' => 'login'), true)
                    )));
                }
            } else {
                $returnObject['status'] = "error";
                $returnObject['err_msg'] = 'The task cannot be validated. Try Again Later!';
                $this->set(array(
                    'data' => $returnObject,
                    '_serialize' => 'data'
                ));
            }
        } else {

            $returnObject['status'] = "error";
            $returnObject['err_msg'] = 'Failed to update task. Try Again Later!';
            $this->set(array(
                'data' => $returnObject,
                '_serialize' => 'data'
            ));
        }
    }

    /**
     * Function for mobile app web service to create new Task
     * timezone offset is in minutes
     * @throws Exception
     */
    public function createTask() {
        $this->viewClass = 'Json';
        $this->request->onlyAllow('post', 'put');
//        $this->log($this->request->input());
        $data = $this->request->input();
        $data = json_decode($data, true);

        $current_network_id = $data['network_id'];
        $auth_user_id = $data['user_id'];
        $timezone = $data['timezone'];
        $title = $data['title'];

        unset($data['user_id']);
        unset($data['network_id']);
        unset($data['timezone']);
        unset($data['title']);

        //Changing the received time to UTC
        $dates = array('start_date', 'end_date');
        foreach ($dates as $value) {
            if (isset($data[$value])) {
                $date = new DateTime($data[$value]);
                if ($timezone > 0) {
                    $date->add(new DateInterval('PT' . abs($timezone) . 'M'));
                } else {
                    $date->sub(new DateInterval('PT' . abs($timezone) . 'M'));
                }
                $data[$value] = $date->format('Y:m:d H:i:s');
            }
        }

        $data['Task']['title'] = $title;
        $current_date = new DateTime();
        /* Playing with Dates */
        if (!isset($data['start_date'])) {
            $data['Task']['start_date'] = $current_date->format('Y:m:d H:i:00');
        } else {
            $data['Task']['start_date'] = $data['start_date'];
        }

        if (!isset($data['end_date'])) {
            $end_date = new DateTime($data['Task']['start_date']);
            $end_date->add(new DateInterval('PT1H'));
            $data['Task']['end_date'] = $end_date->format('Y:m:d H:i:00');
        } else {
            $data['Task']['end_date'] = $data['end_date'];
        }

        unset($data['start_date']);
        unset($data['end_date']);

        $data['Task']['network_id'] = $current_network_id;
        $data['Task']['created_by'] = $auth_user_id;
        $data['Task']['priority'] = (isset($data['Task']['priority']) && $data['Task']['priority'] == true) ? 1 : 0;


        //No assignment for mobile app, only todos i.e. Self-Tasks will return empty array()
        $this->_setAssignee();

        if (empty($data['TaskAssignment'])) {
            $data['TaskAssignment'] = array();
        }
        array_push($data['TaskAssignment'], array(
            'network_id' => $current_network_id,
            'user_id' => $auth_user_id,
            'group_id' => 0,
            'role' => ROLE_ADMIN,
            'added_by' => $auth_user_id
                )
        );
//        $this->log("Data to save");
//        $this->log($data);
        $before_save_timestamp = time();
        if ($this->Task->saveAll($data)) {
//            $this->log("Saved");
            /* Dispatching events */
            $TaskEvent = new CakeEvent(
                    'Controller.Task.afterAdd', $this, array('user_id' => $auth_user_id, 'network_id' => $current_network_id, 'task_id' => $this->Task->id)
            );
            $this->getEventManager()->dispatch($TaskEvent);
            //Since no assignment is done in mobile, execution will not enter if loop 
            if (count($this->request->data['TaskAssignment']) > 1) {
                CakeResque::enqueue('default', 'TaskShell', array('TaskAdd', array(
                        'id' => $this->Task->id,
                        'before_save_timestamp' => $before_save_timestamp,
                        'assigned_by' => $this->Auth->user('UserInfo.name'),
                        'assigned_by_email' => $this->Auth->user('email'),
                        'url' => Router::url(array('admin' => false, 'plugin' => 'users', 'controller' => 'users', 'action' => 'login'), true)
                )));
            }

            $schedule_time = new DateTime($data['Task']['end_date']);
            $schedule_time->sub(new DateInterval('PT' . TASK_REMINDER_TIME . 'S'));

            if (count($data['TaskAssignment']) > 1 && $schedule_time->getTimestamp() >= time()) {
                $job_id = CakeResque::enqueueAt($schedule_time->getTimestamp(), 'default', 'TaskShell', array('TaskDeadlineReminder', array(
                                'id' => $this->Task->id,
                                'end_date' => $data['Task']['end_date'],
                                 'url' => Router::url(array('admin' => false, 'plugin' => 'users', 'controller' => 'users', 'action' => 'login'), true)
                            )), true);
                if ($job_id) {

                    $statusName = array(
                        Resque_Job_Status::STATUS_WAITING => __d('cake_resque', 'waiting'),
                        Resque_Job_Status::STATUS_RUNNING => __d('cake_resque', 'running'),
                        Resque_Job_Status::STATUS_FAILED => __d('cake_resque', 'failed'),
                        Resque_Job_Status::STATUS_COMPLETE => __d('cake_resque', 'complete')
                    );


                    if (Configure::read('CakeResque.Scheduler.enabled') === true) {
                        $statusName[ResqueScheduler\Job\Status::STATUS_SCHEDULED] = __d('cake_resque', 'scheduled');
                    }

                    $job_status = CakeResque::getJobStatus($job_id);
                    $status = isset($statusName[$job_status]) ? $statusName[$job_status] : 'unknown';

                    $data = array(
                        'Cronjob' => array(
                            'job_id' => $job_id,
                            'name' => 'TaskDeadlineReminder',
                            'schedule_date' => $schedule_time->format('Y:m:d H:i:s'),
                            'status' => $status
                        )
                    );



                    $this->loadModel('Cronjob');
                    $this->Cronjob->create();
                    $this->Cronjob->save($data);
                }
            }


            if (count($data['TaskAssignment']) > 0) {

                $list = array();
                $list['group'] = array_unique(Hash::extract($data['TaskAssignment'], '{n}[group_id!=0].group_id'));
                $list['user'] = Hash::extract($data['TaskAssignment'], '{n}[group_id<=0].user_id');
                $TaskAssignmentEvent = new CakeEvent(
                        'Controller.Task.afteraddmember', $this, array(
                    'user_id' => $auth_user_id,
                    'network_id' => $current_network_id,
                    'task_id' => $this->Task->id,
                    'listgroup' => $list['group'],
                    'listuser' => $list['user']
                        )
                );
                $this->getEventManager()->dispatch($TaskAssignmentEvent);
            }
            //Setting the timezone to that of client
            $task = $this->Task->gets($this->Task->id, $auth_user_id);
            foreach ($dates as $value) {
                if (isset($task[0]['Task'][$value])) {
                    $date = new DateTime($task[0]['Task'][$value]);
                    if ($timezone > 0) {
                        $date->sub(new DateInterval('PT' . abs($timezone) . 'M'));
                    } else {
                        $date->add(new DateInterval('PT' . abs($timezone) . 'M'));
                    }
                    $task[0]['Task'][$value] = $date->format('Y:m:d H:i:s');
                }
            }
//            $this->log("After reverting timezone");
//            $this->log($task);
            $returnObject['status'] = 'success';
            $returnObject['message'] = 'Task created successfully.';
            $returnObject['data'] = $task;

            $this->set(array(
                'tasks' => $returnObject,
                '_serialize' => 'tasks'
            ));
        } else {
            $messages = array_values($this->Task->validationErrors);
            if (isset($messages[0])) {
                $returnObject['status'] = 'error';
                $returnObject['err_msg'] = $messages[0];
            } else {
                $returnObject['status'] = 'error';
                $returnObject['err_msg'] = 'Some error occured.';
            }
            $this->set(array(
                'data' => $returnObject,
                '_serialize' => 'data'
            ));
        }
    }

    /**
     * Function for json response of Missing Parameter
     * 
     * anupama@blogtard.com
     * 
     * 
     */
    public function errMissingParameter() {

        $returnObject['status'] = "error";
        $returnObject['err_msg'] = 'Missing parameter';
        $this->set(array(
            'data' => $returnObject,
            '_serialize' => 'data'
        ));
    }

    /**
     * If any of the parameter in the request is not as expected
     * 
     */
    public function errInvalidParameter() {

        $returnObject['status'] = "error";
        $returnObject['err_msg'] = 'Unexpected parameter';
        $this->set(array(
            'data' => $returnObject,
            '_serialize' => 'data'
        ));
    }

    public function queryHas($param) {

        if (isset($this->request->query[$param])) {

            return TRUE;
        } else {

            return FALSE;
        }
    }

    public function getQueryParam($param) {

        $value = $this->request->query[$param];

        return $value;
    }

    public function setJson() {

        $this->viewClass = 'Json';
    }

    public function setTypeGet() {

        $this->request->onlyAllow('get');
    }

    public function info() {
        
    }

    /**
     * method for setting data before sending it to GCM.
     */
    public function prePush() {
        $this->log('in method');
        $user_id = 1;
        $this->loadModel('UserRegId');
        $reg = $this->UserRegId->fetch_regId($user_id);
//        hash::extract($array, $user_id);
        $this->log($reg);
        $token = Hash::extract($reg, '{n}.UserRegId.registration_id');
        $this->log($reg);
        $title = ' new demo1';
        $messageText = 'two,this is for testing.';
        $collapseKey = 'modi';
//        $reg = Set::classicExtract($reg, '{n}.UserRegId.registration_id');
//        $this->log($reg);
        $this->sendPush($token, $collapseKey, $title, $messageText);
    }

//sender_id->343588886952
//Api_key->AIzaSyBBJcCH4mXgwh9pwv2QgqnYk02fO23d8xM
//api_key2->AIzaSyBETuWujLUWtGW7PtzgFaGh0IcsCfWOs-Y
//token = register_id
//collepsekey for message type(send-to-sync, message with payload)
//title and messageText is for data    

    public function sendPush($token = array(), $collapseKey = null, $title = null, $messageText = null) {
        $this->log('in send push');
        if (!is_array($token)) {
            $token = array($token);
        }
//        $this->viewClass = 'Json';
//        
//        $this->log($title);
//        $this->log($messageText);


        $HttpSocket = new HttpSocket(array(
            'ssl_verify_host' => false
        ));
        $data = array(
            'registration_ids' => $token,
            'collapse_key' => $collapseKey,
//            'dry_run' => true,
            'data' => array(
                'title' => $title,
                'message' => $messageText
            )
        );
        $apiKey = 'AIzaSyBETuWujLUWtGW7PtzgFaGh0IcsCfWOs-Y';
        $this->log($data);
        //Authorization key is your api_key
        $result = $HttpSocket->post(
                'https://android.googleapis.com/gcm/send', json_encode($data), array(
            'header' => array(
                'Authorization' => 'key=' . $apiKey,
                'Content-Type' => 'application/json'
            )
                )
        );
        $this->log($result);
        $this->log('get result');
        if ($result->isOk()) {
            $response = json_decode($result->body(), TRUE);
            $this->log($response);
//            $returnObject['data'] = $response->link;
//            $this->log($returnObject);
//                    $this->set(array(
//                        'data' => $returnObject,
//                        '_serialize' => 'data'
//                    ));
            return;
        } else {
            throw new Exception('Some error occured. Please try again later.', 403);
        }

//        $this->log($result->isOk());
//        $this->log($result->code);
//        $this->log(json_decode($result->body(), TRUE));
//        if ($result->code !== "200") {
//            $this->log('in false');
//            return false;
//        } else {
//            $this->log('in json');
//            $this->log(json_decode($result->body, TRUE));
//            json_decode($result->body, TRUE);
//        }
    }

//--------------------------------------------------------------------------------------------------------
    /**
     * for showing notification change into following fields
     *  $this->UserNetwork->setUserNetworkField($user_id, $network_id, 'unseen_notification');
     * and refere tasklisteners method
     */
//-----------------------------------------------------------------------------------------------------    
}

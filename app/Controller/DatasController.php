<?php

App::uses('AppController', 'Controller');

class DatasController extends AppController {

    public $name = 'Data';
    public $uses = array('Group', 'UserInfo', 'UserNetwork', 'Doc', 'Notification', 'Conversation', 'UserConversation','TaskAssignment','Network','User','UserGroup');

    public $helpers = array('Time');
    public $components = array('RequestHandler');
    
    
    public function beforeFilter() {
        parent::beforeFilter();
        $this->viewClass = 'Json';
    }

/**
 * fetch the data of users
 * @param type $page page number
 * @param type $loadMoreCount return current loadmorecount
 * @param type $id id of last user
 */
    public function getUser($page = 1, $id=0) {
        $this->viewClass = 'Json';
        $user_id = $this->Auth->User('id');
        $network_id = $this->Session->read('CurrentNetwork');
        $loadMore = FALSE;
        $limit = User_To_Load;
        $user_ids = null;
        $result = array();
        
        $task_ids = $this->TaskAssignment->getTaskId($network_id, $user_id);
        #five most task-associated Users
        $users_ids = $this->TaskAssignment->getTaskUserId($task_ids, $user_id, $page, $limit);
        #if there is more task-associated users
        if(isset($users_ids[User_To_Load-1])){
                $loadMore = TRUE;
        }#if there is no task assined users
        elseif (empty ($users_ids)) {
            #all task-associated users  
            $user_ids = $this->TaskAssignment->getUserId($task_ids);
            array_push($user_ids, $user_id);
             $user = $this->UserNetwork->getAllUser($network_id, $user_ids, 1, $limit, $id);
             if(isset($user[$limit-1])){
                $loadMore = TRUE; 
             }
            $users_ids = array_merge($users_ids, $user);    
        }#if there is not enough task-associated users
        elseif(!isset($users_ids[User_To_Load-1])){
                $limit=User_To_Load-count($users_ids);
                $user_ids = $this->TaskAssignment->getUserId($task_ids);
                
                $user = $this->UserNetwork->getUser($network_id, $user_ids, 1, $limit);   
                $users_ids = array_merge($users_ids, $user);   
                if(isset($users_ids[User_To_Load-1])){
                    $loadMore = TRUE;
                    }
        }
        #user_info of Users
        foreach ($users_ids as $value) {
             $users = $this->UserInfo->view($value,$network_id);
             $us_net = $this->UserNetwork->getUserNetworkId($users[0]['UserInfo']['user_id'],$network_id);
             if(!empty($us_net)){
             $users[0]['UserInfo']['user_network_id'] = $us_net[0];    
             array_push($result, $users[0]);
             }
        }
        
        $returnObject = array(
                    'status' => 'success',
                    'data' => array('users'=>$result),
                    'LoadMore' => $loadMore
//                    'LoadMoreCount' => count($result)
                );
                return $returnObject;
//        $this->set(array(
//            'datas' => $returnObject,
//            '_serialize' => 'datas'
//        ));
    }
    
 /**
 * fetch the data of users and count of next users
 * @param type $page page number
 * @param type $loadMoreCount return current loadmorecount
 * @param type $id id of last user
 */
     public function getUsers($page = 1, $id=0) {
         $this->viewClass = 'Json';
         $returnObject = $this->getUser($page,$id);
         $count = 0;
         if(!empty($returnObject['data']['users'])){
            $id = $returnObject['data']['users'][count($returnObject['data']['users'])-1]['UserInfo']['user_network_id'];
            $return = $this->getUser($page+1,$id);
            if(!empty($return['data']['users'])){
                $returnObject['LoadMoreCount'] = count($return['data']['users']);
                $returnObject['LoadMore'] = true;
                }
                else{
                    $returnObject['LoadMoreCount'] = $count;  
                    $returnObject['LoadMore'] = FALSE;
                }
         }
         $this->set(array(
            'datas' => $returnObject,
            '_serialize' => 'datas'
        ));
    }
    
    /**
     * this method is used for getting information of all users
     * @param type $page
     */
    public function  getAllNetworkUser(){
        $this->viewClass = 'Json';
//        $user_id = $this->Auth->User('id');
        $network_id = $this->Session->read('CurrentNetwork');
        $result = array();

        $users_ids = $this->UserNetwork->findAllByNetworkId($network_id,array('UserNetwork.user_id','UserNetwork.role'),'UserNetwork.id asc');

        foreach ($users_ids as $value) {
             $users = $this->UserInfo->view($value['UserNetwork']['user_id'],$network_id);
             $users[0]['UserInfo']['role'] = $value['UserNetwork']['role'];
             array_push($result, $users[0]);
        }
         $returnObject = array(
                    'status' => 'success',
                    'data' => array('users'=>$result)
                );
         $this->set(array(
            'datas' => $returnObject,
            '_serialize' => 'datas'
        ));
    }

    /**
 * give group info with pagination
 * @param type $page
 */
    
    public function getGroup($page = 1,$id = null) {
        $this->viewClass = 'Json';
        $user_id = $this->Auth->User('id');
        $network_id = $this->Session->read('CurrentNetwork');
        $loadMore = FALSE;
        $result = array();
        $limit = User_To_Load;
 //fetch all group of users
        $groups = $this->Group->UsersGroup->getActiveGroups($user_id, $network_id);
        /*fetch group with tasks*/
        $group_ids = $this->TaskAssignment->getTaskGroupId($groups, $user_id, $page, $limit);
        /*if more group has tasks*/
        if(isset($group_ids[User_To_Load-1])){
                $loadMore = TRUE;
        }/*if all group are non task-assigned*/
        elseif (empty ($group_ids)) {
          $group_net = $this->TaskAssignment->getGroupId($network_id);
//            $group_rem = $this->Group->getAllGroupId($group_net, $network_id, 1, $limit, $id);
            $group_rem = $this->Group->UsersGroup->getAllGroupId($group_net, $network_id,$user_id, 1, $limit,$id);
            if(isset($group_rem[$limit-1])){
                    $loadMore = TRUE; 
                }
            $group_ids = array_merge($group_ids, $group_rem);
        }/*task-assigned group are done then check for non-task-assigned task*/
        else if(!isset($group_ids[User_To_Load-1])){
            $limit=User_To_Load-count($group_ids);
            $group_net = $this->TaskAssignment->getGroupId($network_id);
            $group_rem = $this->Group->UsersGroup->getAllGroupId($group_net, $network_id,$user_id, 1, $limit,$id);
//            $group_rem = $this->Group->getGroupId($group_net, $network_id, 1, $limit,$id);
            $group_ids = array_merge($group_ids, $group_rem);
            
             if(isset($group_ids[User_To_Load-1])){
                $loadMore = TRUE;
            }
        }
        /*display data of groups*/
        foreach ($group_ids as $value) {
            $groups = $this->Group->getGroups($value, $network_id);
            $groups[0] = $this->Group->getGroupUserInfo($groups);
            $gg = $this->Group->UsersGroup->getUserGroupId($value,$network_id);
            if(!empty($gg)){
                $groups[0]['Group']['user_group_id'] = $gg[0];    
                array_push($result, $groups);
                }
        }
        $result = Hash::extract($result,'{n}.{n}');
        $returnObject = array(
                    'status' => 'success',
                    'data' => array('groups' => $result),
                    'LoadMore' => $loadMore,
                );
                return $returnObject;
//        $this->set(array(
//            'data' => $returnObject,
//            '_serialize' => 'data'
//        ));
    }
/**
 * this return the group and next group count
 * @param type $page
 * @param type $id
 */
    public function getGroups($page = 1,$id = null) {
        $this->viewClass = 'Json';
        $count =0; 
        $returnObject = $this->getGroup($page,$id);
         if(!empty($returnObject['data']['groups'])){
            $id = $returnObject['data']['groups'][count($returnObject['data']['groups'])-1]['Group']['user_group_id'];
            $return = $this->getGroup($page+1,$id);
            if(!empty($return['data']['groups'])){
                $returnObject['LoadMoreCount'] = count($return['data']['groups']);
                $returnObject['LoadMore'] = true;
                }
                else{
                    $returnObject['LoadMoreCount'] = $count;
                    $returnObject['LoadMore'] = FALSE;
                }
            }
         $this->set(array(
            'datas' => $returnObject,
            '_serialize' => 'datas'
        ));
    }

    /**
     * get information of all persones that are in group.
     * perticuler info of group
     * @param type $group_id
     */
    public function getGroupInfo($group_id = null){
        $this->log("in get groupinfo");
        $this->viewClass = 'Json';
        $user_id = $this->Auth->User('id');
        $network_id = $this->Session->read('CurrentNetwork');
        $group = $this->Group->UsersGroup->getGroup( $network_id, $group_id);
//        $this->log($group);
        
         $returnObject = array(
                    'status' => 'success',
                    'data' => $group);
        
        
        
//        $this->log($returnObject);
        $this->set(array(
            'data' => $returnObject,
            '_serialize' => 'data'
        ));
        
    }

    

 /**
 * it gives the information of user,network,UserNetwork,UserInfo of user_id
 * personal info of perticular user
 * @param type $user_id
 * @param type $network_id
 * @return type
 */
    public function getUserAndNetwork($user_id = null, $network_id = null) {
        if(is_null($user_id)){
        $user_id = $this->Auth->User('id');    
        }
        if (is_null($network_id)) {
            $network_id = $this->Session->read('CurrentNetwork');
        } else {
            $Networks = $this->Session->read('Networks');
            if (empty($Networks[$network_id])) {
                $returnObject['status'] = 'error';
                $returnObject['message'] = 'Network Not found';
                $this->set('returnObject', $returnObject);
                return $this->render('/Elements/ajaxreturn');
            }
        }
        if(!empty($network_id)){
        $UserInfo = $this->UserNetwork->find('all', array(
            'conditions' => array(
                'UserNetwork.network_id' => $network_id, 
                'UserNetwork.user_id' => $user_id), 
            'contain' => array('UserInfo','Network','User'))); 
        }else{
            $UserInfo = $this->UserInfo->view($user_id);
        }
        

        $this->set('UserNetworks', $UserInfo[0]);
        $returnObject = array(
                    'status' => 'success',
                    'data' => array('user_networks' => $UserInfo[0], 
                                    'isAdminNetwork' => $this->UserNetwork->isAdmin($user_id, $network_id)));
        
        $this->set(array(
            'data' => $returnObject,
            '_serialize' => 'data'
        ));
    }
    /**
     * 
     * @param type $str search String
     * @param type $type pass type user
     */
    public function search($str = null,$type = NULL){
        $this->viewClass = 'Json';
        $user_curr = $this->Auth->User('id');
        $network_id = $this->Session->read('CurrentNetwork');
        
        $user_id = $this->UserNetwork->find('list', array(
            'conditions' => array('UserNetwork.network_id' => $network_id,
                                  'UserNetwork.user_id !=' => $user_curr),
            'fields'=>array('UserNetwork.user_id')));
        #search for users
        $users = $this->UserInfo->findLike($user_id, $str);
        #search for groups
        if(empty($type)){
        $group_id = $this->Group->UsersGroup->getActiveGroups($user_curr, $network_id, $user_curr);
        $groups = $this->Group->findLike($group_id,$str);
        #mearge user and group search result
            if(!empty($groups['Tags'])){
                if(!empty($users['Tags'])){
                    $users['Tags'] = array_merge($users['Tags'], $groups['Tags']);
                }else{
                    $users['Tags'] = $groups['Tags'];
                }
            }
        }
        $returnObject = array(
                    'status' => 'success',
                    'data' => $users,
                    );
        
        $this->set(array(
            'data' => $returnObject,
            '_serialize' => 'data'
        ));
    }

    public function getNetworks() {

        $returnObject['data'] = array(
            'networks' => $this->Session->read('Networks'),
            'networks_expired' => $this->Session->read('Networks_Expired'),
            'currentnetwork' => $this->Session->read('CurrentNetwork')
        );
        $returnObject['status'] = 'success';
        $this->set(array(
            'data' => $returnObject,
            '_serialize' => 'data'
        ));
    }

    public function getNotificationCount() {
        $user_id = $this->Auth->User('id');
        $network_id = $this->Session->read('CurrentNetwork');
        $returnObject['data'] = $this->UserNetwork->getUserNetworkField($user_id, $network_id, 'unseen_notification');
        $returnObject['status'] = 'success';
        $this->set(array(
            'data' => $returnObject,
            '_serialize' => 'data'
        ));
        return $returnObject;
    }

    public function getConversations($conversation_id = NULL) {
        $username = $this->Auth->User('id');
        $network_id = $this->Session->read('CurrentNetwork');
        $totalpages = $this->Conversation->UserConversation->getconversationtotalpages($username, $network_id);
        if ($totalpages > 0) {
            if (!empty($conversation_id)) {
                $conversations = [];
                $page = 1;
                $found = true;
                while ($page <= $totalpages) {
                    $fetched = $this->Conversation->UserConversation->loadconversations($username, $network_id, $page);
                    $conversations = array_merge($conversations, $fetched);
                    $page++;
                    if (array_search($conversation_id, Hash::extract($fetched, '{n}.Conversation.id'))) {
                        break;
                    }
                }
                $totalpages = $totalpages - $page;
            } else {
                $conversations = $this->Conversation->UserConversation->loadconversations($username, $network_id);
            }
        } else {
            $conversations = null;
        }
        $latestmessage = $this->Conversation->Message->getlatestid();
        $returnObject['data'] = array('latestmessage' => $latestmessage, 'totalsets' => $totalpages, 'conversations' => $conversations);
        $returnObject['status'] = 'success';
        $returnObject['message'] = "Chat Page Data";
        $this->set(array(
            'data' => $returnObject,
            '_serialize' => 'data'
        ));
    }

    public function getUserDetails($user_id = null) {
        $network_id = $this->Session->read('CurrentNetwork');
        $this->UserNetwork->defaultConditions = NULL;
        if (isset($user_id) && $this->UserNetwork->isNetworkUsers($network_id, $user_id)) {
            $user = $this->UserInfo->view($user_id);
            $returnObject['status'] = 'success';
            $returnObject['data'] = array('user' => $user[0]);
        }
        else
        {
            throw new Exception('User not found', 400);
        }
        
        $this->set(array(
            'data' => $returnObject,
            '_serialize' => 'data'
        ));
        
    }

    public function getConversationUnreadCount() {
        
        $network_id = $this->Session->read('CurrentNetwork');
        $author = $this->Auth->user('id');
        
        $returnObject=array(
            'status'=>'success',
            'message'=>'Conversation status changed',
            'data'=>$this->UserConversation->find('count', array('conditions' =>
            array(
                'UserConversation.network_id' => $network_id,
                'UserConversation.user_id' => $author,
                'UserConversation.isUnread' => NOT_SEEN
            )
        ))
        );
        
        $this->set(array(
            'data' => $returnObject,
            '_serialize' => 'data'
        ));
    }
    
   

}

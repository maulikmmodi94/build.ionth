<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Gridle Plugin
 *
 * Gridle's NetworkControl Component
 *
 * @package gridle
 * @subpackage gridle.controllers.components
 */
App::uses('Component', 'Controller');
App::uses('Hash', 'Utility');

class AccessControlComponent extends Component {

    public $components = array('Session', 'Auth', 'RequestHandler');

    /**
     * Controller object instance
     *
     * @var Controller
     */
    public $controller;

    /*
     * Allowed actions without any active network
     * eg:  array('networks'=>array('index'),'pages')
     * Will allow only index of networks controller and all action of pages controller 
     */
    public $allowedactions = array();

    /*
     * Parameters Stored in session  
     * networks  -   variable name in session containing all networks
     * activeNetwork   -     variable name in session containing active network
     */
    public $sessionVariables = array();

    /**
     * Initialize Callback
     *
     * @param Controller object
     */
    public function initialize(Controller $controller) {
        $this->request = $controller->request;
        $this->controller = $this->request->params['controller'];
        $this->response = $controller->response;
        $this->_methods = $controller->methods;
    }

    /*
     * Checks for
     */

    public function authorizedCurrentNetwork() {
        //Check for network in session and they should not be empty
        if ($this->Session->check($this->sessionVariables['networks']) && !empty($this->Session->read($this->sessionVariables['networks']))) {
            $networks = $this->Session->read($this->sessionVariables['networks']);

            $activeNetwork = FALSE;
            if ($this->Session->check($this->sessionVariables['activeNetwork'])) {
                $activeNetwork = $this->Session->read($this->sessionVariables['activeNetwork']);
            }
            //Check for active networks
            if (!$activeNetwork) {
                //Delete the empty variable when no activeNetwork
                $this->Session->delete($this->sessionVariables['activeNetwork']);
                return false;
            }
            //Check if user part of active network
            else {
                //Not part of activenetwork
                if (empty($networks[$activeNetwork])) {
                    return false;
                }
                //If the network has Ipchecking on
                else
                if ($networks[$activeNetwork]['ip_check'] && !($networks[$activeNetwork]['created_by'] == $this->Auth->user('id'))) {
                    //Action is not allowed without authorization
                    $networkAccess = $this->getNetworkAccessList($activeNetwork);
                    if (!$this->ipAuthorization($networkAccess))
                        return false;
                    if (!$this->_isAllowed()) {
                        $networkAccess = $this->getNetworkAccessList($activeNetwork);
                        if (!$this->ipAuthorization($networkAccess)) {
                            $this->Session->delete($this->sessionVariables['activeNetwork']);
                            return false;
                        }
                    }
                }
            }
            return true;
        } else {
            return false;
        }
    }

    /*
     * Checks the current user or his role is authorize to network's allowed IPs
     */

    protected function ipAuthorization($networkAccess) {

        //Get current IP
        $ip_address = $this->request->clientIp();
        //Filter all the access entries for current IP and 255.255.255.255 (Allowed from all)
        $network_access = Hash::extract($networkAccess, 'Network.NetworkAccess.{n}[ip_address=/' . $ip_address . "|" . ALLOW_FROM_ALL_IP . '/]');
        foreach ($network_access as $key => $access) {
            if ($this->accessRolePriority($networkAccess['UserNetwork']['role']) <= $this->accessRolePriority($access['role'])) {
                return true;
            } else if ($access['user_id'] === $networkAccess['UserNetwork']['user_id']) {
                return true;
            }
        }
        return false;
    }

    /*
     * Provides Priority of given roles
     */

    protected function accessRolePriority($role) {
        switch ($role) {
            case ROLE_OWNER:
                return 0;
                break;
            case ROLE_ADMIN:
                return 1;
                break;
            case ROLE_USER:
                return 2;
                break;
            default:
                return 3;
                break;
        }
    }

    /**
     * Checks whether current action is accessible without active network.
     *     
     * @return boolean True if action is accessible else false
     */
    public function _isAllowed() {
        $action = strtolower($this->request->params['action']);
        if (in_array($action, $this->_allowedActions())) {
            return true;
        }
        return false;
    }

    protected function _allowedActions() {
        if (in_array($this->controller, $this->allowedactions) || array_key_exists($this->controller, $this->allowedactions)) {
            if (empty($this->allowedactions[$this->controller]) || !is_array($this->allowedactions[$this->controller])) {
                return array_map('strtolower', $this->_methods);
            } else {
                return array_map('strtolower', $this->allowedactions[$this->controller]);
            }
        }
        return [];
    }

    protected function getNetworkAccessList($network_id) {
        $this->UserNetwork = ClassRegistry::init('UserNetwork');
        return $this->UserNetwork->find('first', array(
                    'conditions' => array('UserNetwork.user_id' => $this->Auth->user('id'), 'UserNetwork.network_id' => $network_id),
                    'fields' => array('UserNetwork.network_id', 'UserNetwork.role', 'UserNetwork.user_id'),
                    'contain' => array('Network.NetworkAccess' => array(
                            'conditions' => array(
                                'OR' => array(
                                    'role !=' => array('special'),
                                    'user_id' => array($this->Auth->user('id'))
                                )
                            )
                        ))
        ));
    }

}

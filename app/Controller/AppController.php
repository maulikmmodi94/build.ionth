<?php

/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
App::uses('Controller', 'Controller');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {

    public $components = array(
        'Auth' => array(
            'ajaxLogin' => '/Elements/ajaxlogin',
            'loginRedirect' => array(
                'controller' => 'networks',
                'action' => 'index'
            )),
        'Session',
        'AccessControl' => array(
            'allowedactions' => array('networks' => array('index', 'switchNetwork', 'add'), 'datas', 'pages', 'users', 'logs', 'admins'),
            'sessionVariables' => array('networks' => 'Networks', 'activeNetwork' => 'CurrentNetwork')
        )
    );
    public $primaryKey = 'id';
    public $uses = array('User');

    /**
     * beforeFilter callback
     *
     * @return void
     */
    public function beforeFilter() {
        parent::beforeFilter();
        if ($this->Auth->loggedIn()) {
            /* Fetch User's networks and set to Networks variable to use in View */
            $Networks = $this->_setNetworkList();
            $this->set('Networks', $Networks);
            /* Set CurrentNetwork Id */
            if (!$this->AccessControl->_isAllowed()) {
                if (!$this->AccessControl->authorizedCurrentNetwork()) {
                    $this->Session->setFlash(__d('networks', 'You are not authorized for this actions.'));
                    if ($this->request->is('Ajax')) {
                        $this->viewClass = 'Json';

                        $returnObject = array(
                            'status' => 'redirect',
                            'message' => '/networks',
                        );

                        $this->ajax_return($returnObject['status'], $returnObject['message']);

                        $this->response->send();
                        $this->_stop();
                    } else {
                    return $this->redirect(array('plugin' => null, 'controller' => 'networks', 'action' => 'index'));
                    }
                }
            }
            /* Set CurrentNetwork Id */
            $CurrentNetwork = $this->Session->read('CurrentNetwork');
            $this->set('CurrentNetwork', $CurrentNetwork);
        } else if (!$this->Auth->loggedIn()) {
            //if user has not yet logged in, then suppress the Auth Error messages
            $this->Auth->authError = false;
        }
    }

    public function beforeRender() {
        parent::beforeRender();

        if ($this->Auth->loggedIn()) {

            /* Fetch User.email_verified field from Database on each request */
            if (!$this->Auth->user('email_verified')) {
                $this->User->id = $this->Auth->user('id');
                $email_verified = $this->User->field('email_verified');
                $this->User->id = null;

                /* Store User.email_verified in Auth Session variable */
                $this->Session->write('Auth.User.email_verified', $email_verified);

                /* Logout if User's Expiration period is over */
                $created = $this->Auth->user('created');
                $expiration_time = strtotime($created) + EMAIL_TOKEN_EXPIRATION;

                if (!$email_verified && $expiration_time < time()) {
                    $this->redirect('/users/logout');
                }
            }

            /* Serialize the data set to be returned as response */
            $this->_serializeData();

            /* Set AuthUser variable to use in view */
            $this->set('AuthUser', $this->Session->read('Auth.User'));
        }
    }

    protected function _serializeData($response = 'returnObject') {
        if (!empty($this->viewVars[$response])) {
            $this->set(array(
                'data' => $this->viewVars[$response],
                '_serialize' => 'data'
            ));
        }
        return;
    }

    protected function _setCronJob($job_id, $schedule_time){
        $statusName = array(
                    Resque_Job_Status::STATUS_WAITING => __d('cake_resque', 'waiting'),
                    Resque_Job_Status::STATUS_RUNNING => __d('cake_resque', 'running'),
                    Resque_Job_Status::STATUS_FAILED => __d('cake_resque', 'failed'),
                    Resque_Job_Status::STATUS_COMPLETE => __d('cake_resque', 'complete')
                    );

                    if (Configure::read('CakeResque.Scheduler.enabled') === true) {
                        $statusName[ResqueScheduler\Job\Status::STATUS_SCHEDULED] = __d('cake_resque', 'scheduled');
                    }

                    $job_status = CakeResque::getJobStatus($job_id);
                    $status = isset($statusName[$job_status]) ? $statusName[$job_status] : 'unknown';

                    $data = array(
                        'Cronjob' => array(
                            'job_id' => $job_id,
                            'name' => 'TaskDeadlineReminder',
                            'schedule_date' => $schedule_time->format('Y:m:d H:i:s'),
                            'status' => $status
                        ));

                    $this->loadModel('Cronjob');
                    $this->Cronjob->create();
                    $this->Cronjob->save($data);
                    return;
    }

        protected function _setNetworkList($user_id = null) {
        if (empty($user_id)) {
            $user_id = $this->Auth->user('id');
        }
        $this->loadModel('UserNetwork');
        $network_ids = $this->UserNetwork->find('list', array(
            'conditions' => array(
                'UserNetwork.user_id ' => $user_id
            ),
            'fields' => array('UserNetwork.network_id')
                )
        );


        $this->UserNetwork->Network->defaultConditions = array(
            'Network.isDeleted' => NOT_DELETED
        );
        $Networks = $this->UserNetwork->Network->find('all', array(
            'conditions' => array(
                'Network.id' => $network_ids
            ),
            'contain' => array(
                'NetworkAccess'
            )
                )
        );

        $date = date('Y-m-d H:i:s');
        $Networks_Expired = array();

        foreach ($Networks as $key => $network) {
            $Networks[$key]['Network']['NetworkAccess'] = $Networks[$key]['NetworkAccess'];
            if ($date > $Networks[$key]['Network']['expires']) {
                array_push($Networks_Expired, $network);
                unset($Networks[$key]);
            }
        }
        $Networks = Hash::combine($Networks, '{n}.Network.id', '{n}.Network');
        $Networks_Expired = Hash::combine($Networks_Expired, '{n}.Network.id', '{n}.Network');
        $this->Session->write('Networks', $Networks);
        $this->Session->write('Networks_Expired', $Networks_Expired);

        return $Networks;
    }

    public function getNetwork($value, $field = 'url') {

        /* Set NetworkList to Session */
        $Networks = $this->_setNetworkList();

        /* Set URL as Index value */
        $Networks = Hash::combine($Networks, '{n}.' . $field, '{n}');
        return $Networks[$value];
    }

    protected function getIdByUsername($username) {
        $User = $this->User->find('list', array('conditions' => array('User.username' => $username), 'fields' => array('User.id')));
        if (!empty($User)) {
            return array_pop($User);
        } else {
            return FALSE;
        }
    }

    public function setTimeZone($timezone) {
        //Set the timezone of user
        if ((int) $timezone) {
            $this->Session->write('Auth.User.timezone', (int) $timezone);
        } else {
            $this->Session->write('Auth.User.timezone', 0);
        }
    }

    /**
     * Ajax_Return
     *
     * @author Hardik Sondagar
     * @abstract Ajax_Return
     * @param string $status Response's status.
     * @param string $message Response's message.
     * @param array $data Response's data. 
     * @return send response
     * @since 08/09/14
     */
    public function ajax_return($status, $message, $data = array()) {
        $this->autoRender = false;
        $this->layout = 'ajax';

        $returnObject;

        $returnObject['status'] = $status;
        $returnObject['message'] = $message;
        $returnObject['data'] = $data;

        $this->set('returnObject', $returnObject);
        $this->render('/Elements/ajaxreturn');
    }
    
    /**
     * check wheather user is logged in or not
     * @return boolean
     */
    protected function _isUserLoggedIn(){
        /* check whether user is login or not */
        if ($this->Auth->loggedIn()) {
            return TRUE;
        }
        return FALSE;
    }

}

<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
App::uses('AppShell', 'Console/Command');
App::uses('Router', 'Routing');
App::uses('User', 'Model');
config('routes');

class UserShell extends AppShell {

    /**
     * Sends email on password change
     *
     * it is an acknowledgement mail that is sent to user
     * 
     * @param string $to Receiver email address
     * @param array $options EmailComponent options
     * @return void
     */
    public function onPasswordChange() {
        $data = $this->args[0];
        $this->loadModel('UserInfo');
        $user = $this->UserInfo->find('first', array
            (
            'conditions' => array(
                'UserInfo.user_id' => $data['id']
            ),
            'fields' => array(
                'UserInfo.first_name', 'UserInfo.last_name', 'UserInfo.email'
            ))
        );
        $data['email'] = $user['UserInfo']['email'];
        $data['fname'] = $user['UserInfo']['first_name'];
        $data['assigned_by_email'] = 'anupamapanchal@gridle.in';
        $options = array(
            'from' => array(Configure::read('App.defaultEmail') => 'Donna from Build'),
            'subject' => __d('users', 'Your password for Build has been changed.'),
            'template' => 'Users.password_change',     
            'replyTo'=> array($data['assigned_by_email'] => 'The Team Build')
        );
        
        $data['function'] = 'onPasswordChange';
        $this->sendMail($data, $options);
    }

    public function NotVerified() {
        $this->loadModel('User');
        $date = date('Y-m-d H:i:s', time());
        $this->User->bindModel(
                array('hasOne' => array(
                        'UserInfo' => array(
                            'className' => 'UserInfo'
                        )
                    )
                )
        );

        $result = $this->User->find('all', array(
            'conditions' => array(
                'User.email_verified' => 0,
                'User.active' => 1,
                /* Send reminder to verify link on 2nd and 7th day */
                "OR" => array(
                    array(
                        'User.created  BETWEEN ? AND ?' => array(
                            date('Y-m-d H:i:s', time() + 86400 * 2),
                            date('Y-m-d H:i:s', time() + 86400 * 3),
                        )
                    ),
                    array('User.created  BETWEEN ? AND ?' => array(
                            date('Y-m-d H:i:s', time() + 86400 * 6),
                            date('Y-m-d H:i:s', time() + 86400 * 7),
                        )
                    ),
                ),
            ),
            'fields' => array(
                'id', 'email', 'email_token', 'email_token_expires', 'UserInfo.first_name'
            ),
            'contain' => array(
                'UserInfo'
            )
        ));



        foreach ($result as $value) {

            CakeResque::enqueue('default', 'UserShell', array('NotVerifiedChild', array(
                    'name' => $value['UserInfo']['first_name'] . ' ' . $value['UserInfo']['last_name'],
                    'email' => $value['UserInfo']['email'],
                    'url' => Router::url(array(
                        'admin' => false,
                        'plugin' => 'users',
                        'controller' => 'users',
                        'action' => 'verify',
                        'email',
                        $value['User']['email_token']
                            ), true
                    )
                ))
            );
        }
    }

    public function NotVerifiedChild() {
        $data = $this->args[0];

        $options = array(
            'subject' => __d('users', 'Account not verified'),
            'template' => 'Users.notverified',
        );
        $data['function'] = 'NotVerifiedChild';

        $this->sendMail($data, $options);
    }

    public function VerificationExpired() {
        $User = ClassRegistry::init('User');
        $this->loadModel('User');
        $date = date('Y-m-d H:i:s', time());
        $this->User->bindModel(
                array('hasOne' => array(
                        'UserInfo' => array(
                            'className' => 'UserInfo'
                        )
                    )
                )
        );

        $result = $this->User->find('all', array(
            'conditions' => array(
                'User.email_verified' => 0,
                'User.active' => 1,
                /* Send notification for account expiration */
                'User.email_token_expires  BETWEEN ? AND ?' => array(
                    date('Y-m-d H:i:s', time()),
                    date('Y-m-d H:i:s', time() + 86400 * 1),
                )
            ),
            'fields' => array(
                'id', 'email', 'email_token_expires', 'UserInfo.first_name', 'UserInfo.last_name'
            ),
            'contain' => array(
                'UserInfo'
            )
        ));

        foreach ($result as $value) {

            CakeResque::enqueue('default', 'UserShell', array('VerificationExpiredChild', array(
                    'name' => $value['UserInfo']['first_name'] . ' ' . $value['UserInfo']['last_name'],
                    'email' => $value['UserInfo']['email']
                ))
            );
        }
    }

    public function VerificationExpiredChild() {
        $data = $this->args[0];

        $options = array(
            'subject' => __d('users', 'Verification Expires'),
            'template' => 'Users.verificationexpire',
        );
        $data['function'] = 'VerificationExpiredChild';

        $this->sendMail($data, $options);
    }

    public function InvitationNotAccepted() {
        $this->loadModel('User');
        $date = date('Y-m-d H:i:s', time());
        $this->User->bindModel(
                array('hasOne' => array(
                        'UserInfo' => array(
                            'className' => 'UserInfo'
                        )
                    )
                )
        );

        $time = time();
        
        $result = $this->User->find('all', array(
            'conditions' => array(
                'User.email_verified' => 0,
                'User.active' => 0,
            /* Send reminder to sign up and join network after 7 day,15 day and 1 month of invitation */
                "OR" => array(
                    array(
                        'User.created  BETWEEN ? AND ?' => array(
                            date('Y-m-d H:i:s', $time + 86400 * 7),
                            date('Y-m-d H:i:s', $time + 86400 * 8),
                        )
                    ),
                    array('User.created  BETWEEN ? AND ?' => array(
                            date('Y-m-d H:i:s', $time + 86400 * 15),
                            date('Y-m-d H:i:s', $time + 86400 * 16),
                        )
                    ),
                    array('User.created  BETWEEN ? AND ?' => array(
                            date('Y-m-d H:i:s', $time + 86400 * 30),
                            date('Y-m-d H:i:s', $time + 86400 * 31),
                        )
                    ),
                ),
            ),
            'fields' => array(
                'id', 'email', 'email_token_expires', 'UserInfo.*'
            ),
            'contain' => array(
                'UserInfo'
            )
        ));

        foreach ($result as $value) {

            CakeResque::enqueue('default', 'UserShell', array('InvitationNotAcceptedChild', array(
                    'name' => $value['UserInfo']['first_name'] . ' ' . $value['UserInfo']['last_name'],
                    'email' => $value['User']['email'],
                    'url' => Router::url(
                            array(
                        'admin' => false,
                        'plugin' => 'users',
                        'controller' => 'users',
                        'action' => 'add'
                            ), true)
                ))
            );

           
        }
    }

    public function InvitationNotAcceptedChild() {
        $data = $this->args[0];

        $options = array(
            'subject' => __d('users', 'Join Network'),
            'template' => 'Users.invitationnotaccepted',
        );
        $data['function'] = 'InvitationNotAcceptedChild';
        $data['event_type'] = 'USER_INVITATION';

        $this->sendMail($data, $options);
    }
    
    /**
     * If a user has not signed up after getting the invitation to join the network for 3 days
     * then this mail sent
     */
    public function AfterInvite() {
        $data = $this->args[0];
        $this->loadModel('User');

        $result = $this->User->find('first', array(
            'conditions' => array(
                'User.active' => 0,
                'User.email' => $data['email']
            ),
            'fields' => array(
                'User.id', 'User.email', 'User.username'
            )
        ));
        if(!empty($result['User']['email'])) {

            $options = array(
                'from' => array(Configure::read('App.defaultEmail') => $data['invited_by']),
            'subject' => __d('networks', ' Hey, I am waiting for you to join '.$data['network_name'] .' on Build.'),
            'template' => 'Users.after_invite',
        );
        $data['function'] = 'AfterInvite';
        $data['event_type'] = 'USER_AFTER_INVITATION';
        $this->sendMail($data, $options);
        
        }
    }
    

}

<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
App::uses('AppShell', 'Console/Command');
App::uses('Router', 'Routing');
App::uses('HttpSocket', 'Network/Http');
config('routes');

class DocShell extends AppShell {

    /**
     * when doc is share to another user
     * @return type
     */
    public function DocAdd() {

        $data = $this->args[0];
        $id = $data['id'];
        $url = $data['url'];
        $user_id = $data['user_id'];
        $group_id = Hash::extract($data, 'group_id.{n}.value');
        $user_info_from_group = array();

        CakeLog::write('cronjob', 'DocAdd initiate for File:' . $id);

        $this->loadModel('Doc');
        $this->loadModel('UserInfo');
        $this->loadModel('DocAssignment');
        $this->loadModel('UsersGroup');


        if (!$this->Doc->exists($id)) {
            CakeLog::write('cronjob', 'Doc:' . $id . ' not found');
            $this->out('Doc:' . $id . ' not found');
            return;
        }

        $doc = $this->Doc->findById($id);
        if(isset($group_id) && !empty($group_id)){
        
            $this->UsersGroup->bindModel(
                array('hasOne' => array(
                        'UserInfo' => array(
                            'className' => 'UserInfo',
                            'foreignKey' => FALSE,
                            'conditions' => array('UserInfo.user_id = UsersGroup.user_id'),
                            'fields' => array('UserInfo.*')
                        )
                    )
                )
            );
            
            $user_list = $this->UsersGroup->find('all',array(
            'conditions' => array(
                'UsersGroup.group_id' => $group_id,
                'UsersGroup.user_id !=' => $user_id,
                'UsersGroup.isDeleted' => NOT_DELETED,
                'UsersGroup.status' => STATUS_ACTIVE
            ),
            'contain' => array(
                'UserInfo'
            ),
            'fields' => array(
                'DISTINCT UsersGroup.user_id'
            )
        ));
        $user_info_from_group = array();
        foreach ($user_list as $user_info) {
            $user_info['DocAssignment'] = $user_info['UsersGroup'];
            unset($user_info['UsersGroup']);
            array_push($user_info_from_group, $user_info);
        }
    }
        $this->DocAssignment->bindModel(
                array('belongsTo' => array(
                        'UserInfo' => array(
                            'className' => 'UserInfo',
                            'foreignKey' => FALSE,
                            'type' => 'inner',
                            'conditions' => array('UserInfo.user_id = DocAssignment.user_id'),
                            'fields' => array('UserInfo.*')
                        )
                    )
                )
        );

        $doc_assignments = $this->DocAssignment->find('all', array(
            'conditions' => array(
                'DocAssignment.doc_id' => $id,
                'DocAssignment.created >=' => date('Y-m-d H:i:s', $data['before_save_timestamp'])
            ),
            'contain' => array(
                'UserInfo'
            ),
            'fields' => array(
                'DISTINCT DocAssignment.user_id'
            )
        ));

        $data = array(
            'function' => 'DocAddChild',
            'url' => $url,
            's_fname' => $data['s_fname']
        );

        $data = array_merge($data, $doc);

        $doc_assignments = array_merge($doc_assignments, $user_info_from_group);
        $doc_assignments = array_unique($doc_assignments, SORT_REGULAR);

        foreach ($doc_assignments as $value) {
            $user = array(
                'name' => $value['UserInfo']['first_name'] . ' ' . $value['UserInfo']['last_name'],
                'email' => $value['UserInfo']['email'],
                'fname'=> $value['UserInfo']['first_name']
            );

            $data = array_merge($data, $user);

            $this->out(CakeResque::enqueue('default', 'DocShell', array($data['function'], $data)));
        }

        CakeLog::write('cronjob', 'DocAdd finish for Doc:' . $id);
    }

    public function DocAddChild() {
        $data = $this->args[0];

        $options = array(
            'from' => array(Configure::read('App.defaultEmail') => 'Donna from build'),
            'subject' => __d('docs', ' %s has shared a file with you on build.', $data['s_fname']),
            'template' => 'Doc/add',
        );

        $data['function'] = 'DocAddChild';
        $data['event_type'] = 'DOC_SHARED';

        $this->sendMail($data, $options);
    }

    /**
     * it fetch the preview link and thumbnail from the framebench
     * @throws Exception
     */
    public function DocThumbnail() {
        $data = $this->args[0];
        $this->loadModel('Doc');

        $httpsocket = new HttpSocket();
            #request for preview part
                $results = $httpsocket->post('https://lite.framebench.com/api/file', $data);
                if ($results->isOk()) {
                    $response = json_decode($results->body());
                    $returnObject['preview'] = $response->link;
                } else {
                    throw new Exception('Some error occured. Please try again later.', 403);
                }
                $doc = $this->Doc->updateAll(
                array(
                    'Doc.preview_url'=>'"'.$returnObject['preview'].'"'
                        ),
                array(
                    'Doc.id'=> $data['fileId']
                    ));

                #request for thumbnail part
                $d = array('apiKey' => $data['apiKey'], 'fileId' => $data['fileId']);

                $result = $httpsocket->post('https://lite.framebench.com/api/file/metadata', $d);
                if ($result->isOk()) {
                    $response = json_decode($result->body());
                    $returnObject['thumbnail'] = $response->thumbnail;
                } else {
                    throw new Exception('Some error occured. Please try again later.', 403);
                }
                $docs = $this->Doc->updateAll(
                array(
                    'Doc.thumbnailLink'=> '"'.$returnObject['thumbnail'].'"'
                        ),
                array(
                    'Doc.id'=> $data['fileId']
                    ));
    }
}

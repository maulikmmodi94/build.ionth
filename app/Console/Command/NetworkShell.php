<?php

App::uses('AppShell', 'Console/Command');
App::uses('Router', 'Routing');
App::uses('Network', 'Model');
App::uses('UserNetwork', 'Model');
App::uses('UserInfo', 'Model');

config('routes');

class NetworkShell extends AppShell {

    /**
     * this mail is send to user who is invitee for current network
     */
    public function invite() {


        $data = $this->args[0];

        CakeLog::write('cronjob', 'Network:invite initiate for Network:' . $data['Network']['name']);

        $this->loadModel('UserInfo');

        $user_networks = $this->UserInfo->findAllByUserId($data['UserNetwork']);

        $data['function'] = 'inviteChild';

        foreach ($user_networks as $value) {

            $user = array(
                'name' => $value['UserInfo']['first_name'] . ' ' . $value['UserInfo']['last_name'],
                'email' => $value['UserInfo']['email']
            );

            $data = array_merge($data, $user);

            $this->out(CakeResque::enqueue('default', 'NetworkShell', array($data['function'], $data)));
        }

        CakeLog::write('cronjob', 'Network:invite finish for Network:' . $data['Network']['name']);
    }

    public function inviteChild() {
        $data = $this->args[0];

        $options = array(
            'subject' => __d('networks', 'Working on Build going forward. Invite.'),
            'template' => 'Network/invite',
            'from' => array(Configure::read('App.defaultEmail') => $data['invited_by'])
        );
        $data['function'] = 'inviteChild';
        $data['event_type'] = 'NETWORK_INVITATION';
        $this->sendMail($data, $options);
    }

    /**
     * when user is assigned role of admin
     */
    public function assignRole() {
        $data = $this->args[0];

        CakeLog::write('cronjob', 'Network:assignRole initiate for Network:' . $data['Network']['name']);

        $this->loadModel('UserInfo');

        $user = $this->UserInfo->findByUserId($data['user_id']);

        $data['name'] = $user['UserInfo']['first_name'] . ' ' . $user['UserInfo']['last_name'];
        $data['email'] = $user['UserInfo']['email'];
        $options = array(
            'subject' => __d('networks', 'You are assigned role'),
            'template' => 'Network/assignrole',
            'from' => array(Configure::read('App.defaultEmail') => $data ['assignBy'])
        );

        $data['function'] = 'assignRole';
        $data['event_type'] = 'NETWORK_ROLE_ASSIGN';
        $this->sendMail($data, $options);

        CakeLog::write('cronjob', 'Network:assignRole finish for Network:' . $data['Network']['name']);
    }

    public function activation() {
        $data = $this->args[0];

        $this->loadModel('UserInfo');

        CakeLog::write('cronjob', 'Network:activation initiate');

        $this->Network->bindModel(
                array('hasOne' => array(
                        'UserNetwork' => array(
                            'className' => 'UserNetwork',
                            'conditions' => array(
                                'UserNetwork.role' => ROLE_OWNER
                            )
                        )
                    )
                )
        );

        $this->UserInfo->virtualFields = array(
            'name' => 'CONCAT(UserInfo.first_name, " ", UserInfo.last_name)'
        );




        $result = $this->Network->find('all', array(
            'conditions' => array(
                'Network.id' => $data['id']
            ),
            'fields' => array(
                'Network.name', 'UserNetwork.user_id', 'UserNetwork.role'
            ),
            'contain' => array(
                'UserNetwork'
            )
        ));

        $users = $this->UserInfo->find('all', array(
            'conditions' => array(
                'UserInfo.user_id' => Hash::extract($result, '{n}.UserNetwork.user_id')
            ),
            'fields' => array(
                'UserInfo.user_id', 'UserInfo.name', 'UserInfo.email'
            )
        ));

        $users = Hash::combine($users, '{n}.UserInfo.user_id', '{n}.UserInfo');

        foreach ($result as $value) {

            CakeResque::enqueue('default', 'NetworkShell', array('activationChild', array(
                    'name' => $users[$value['UserNetwork']['user_id']]['name'],
                    'email' => $users[$value['UserNetwork']['user_id']]['email'],
                    'Network' => $value['Network']
                ))
            );
        }
        CakeLog::write('cronjob', 'Network:activation finished');
    }

    public function activationChild() {
        $data = $this->args[0];

        $options = array(
            'subject' => __d('networks', 'Network has been activated'),
            'template' => 'Network/activation',
        );
        $data['function'] = 'activationChild';

        $this->sendMail($data, $options);
    }

    public function BeforeExpired() {

        $this->loadModel('Network');
        $this->loadModel('UserNetwork');
        $this->loadModel('UserInfo');
        $date = date('Y-m-d H:i:s', time());

        CakeLog::write('cronjob', 'Network:BeforeExpired initiate');

        $this->Network->bindModel(
                array('hasOne' => array(
                        'UserNetwork' => array(
                            'className' => 'UserNetwork',
                            'conditions' => array(
                                'UserNetwork.role' => ROLE_OWNER
                            )
                        )
                    )
                )
        );

        $this->UserInfo->virtualFields = array(
            'name' => 'CONCAT(UserInfo.first_name, " ", UserInfo.last_name)'
        );


        $this->Network->defaultConditions = array(
            'Network.isDeleted' => NOT_DELETED,
        );

        $time = time();


        $result = $this->Network->find('all', array(
            'conditions' => array(
                /* Send email for network expiration after 1 day,7 day of expiration */
                "OR" => array(
                    array(
                        'Network.expires  BETWEEN ? AND ?' => array(
                            date('Y-m-d H:i:s', $time - 86400 * 2),
                            date('Y-m-d H:i:s', $time - 86400 * 1),
                        )
                    ),
                    array(
                        'Network.expires  BETWEEN ? AND ?' => array(
                            date('Y-m-d H:i:s', $time - 86400 * 7),
                            date('Y-m-d H:i:s', $time - 86400 * 6),
                        )
                    ),
                ),
            ),
            'fields' => array(
                'Network.name', 'UserNetwork.user_id', 'UserNetwork.role'
            ),
            'contain' => array(
                'UserNetwork'
            )
        ));
        $users = $this->UserInfo->find('all', array(
            'conditions' => array(
                'UserInfo.user_id' => Hash::extract($result, '{n}.UserNetwork.user_id')
            ),
            'fields' => array(
                'UserInfo.user_id', 'UserInfo.name', 'UserInfo.email'
            )
        ));

        $users = Hash::combine($users, '{n}.UserInfo.user_id', '{n}.UserInfo');

        foreach ($result as $value) {

            CakeResque::enqueue('default', 'NetworkShell', array('BeforeExpiredChild', array(
                    'name' => $users[$value['UserNetwork']['user_id']]['name'],
                    'email' => $users[$value['UserNetwork']['user_id']]['email'],
                    'Network' => $value['Network']
                ))
            );
        }
        CakeLog::write('cronjob', 'Network:BeforeExpired finished');
    }

    public function BeforeExpiredChild() {
        $data = $this->args[0];

        $options = array(
            'subject' => __d('networks', 'Network about to expired'),
            'template' => 'Network/beforeexpired',
        );
        $data['function'] = 'BeforeExpiredChild';

        $this->sendMail($data, $options);
    }

    public function Expired() {

        $this->loadModel('Network');
        $this->loadModel('UserNetwork');
        $this->loadModel('UserInfo');
        $date = date('Y-m-d H:i:s', time());
        CakeLog::write('cronjob', 'Network:Expired initiate');
        $this->Network->bindModel(
                array('hasOne' => array(
                        'UserNetwork' => array(
                            'className' => 'UserNetwork',
                            'conditions' => array(
                                'UserNetwork.role' => ROLE_OWNER
                            )
                        )
                    )
                )
        );

        $this->UserInfo->virtualFields = array(
            'name' => 'CONCAT(UserInfo.first_name, " ", UserInfo.last_name)'
        );


        $this->Network->defaultConditions = array(
            'Network.isDeleted' => NOT_DELETED,
        );

        $time = time();


        $result = $this->Network->find('all', array(
            'conditions' => array(
                /* Send email for network expiration after 1 day,7 day of expiration */
                "OR" => array(
                    array(
                        'Network.expires  BETWEEN ? AND ?' => array(
                            date('Y-m-d H:i:s', $time + 86400 * 1),
                            date('Y-m-d H:i:s', $time + 86400 * 2),
                        )
                    ),
                    array(
                        'Network.expires  BETWEEN ? AND ?' => array(
                            date('Y-m-d H:i:s', $time + 86400 * 6),
                            date('Y-m-d H:i:s', $time + 86400 * 7),
                        )
                    ),
                ),
            ),
            'fields' => array(
                'Network.name', 'UserNetwork.user_id', 'UserNetwork.role'
            ),
            'contain' => array(
                'UserNetwork'
            )
        ));

        $users = $this->UserInfo->find('all', array(
            'conditions' => array(
                'UserInfo.user_id' => Hash::extract($result, '{n}.UserNetwork.user_id')
            ),
            'fields' => array(
                'UserInfo.user_id', 'UserInfo.name', 'UserInfo.email'
            )
        ));

        $users = Hash::combine($users, '{n}.UserInfo.user_id', '{n}.UserInfo');

        foreach ($result as $value) {

            CakeResque::enqueue('default', 'NetworkShell', array('ExpiredChild', array(
                    'name' => $users[$value['UserNetwork']['user_id']]['name'],
                    'email' => $users[$value['UserNetwork']['user_id']]['email'],
                    'Network' => $value['Network']
                ))
            );
        }
        CakeLog::write('cronjob', 'Network:Expired finished');
    }

    public function ExpiredChild() {
        $data = $this->args[0];

        $options = array(
            'subject' => __d('networks', 'Network Expired'),
            'template' => 'Network/expired',
        );
        $data['function'] = 'ExpiredChild';

        $this->sendMail($data, $options);
    }
    
    public function networkPlanUpgradeRequest() {
        $this->log("Inside NetworkShell");
        $this->log("Upgrade Plan");
        $this->log($this->args[0]);
        $data = $this->args[0];
        $options = array(
            'subject' => __d('networks', 'Network Plan Upgrade Request.'),
            'template' => 'Network/planupgrade',
            'from' => array(Configure::read('App.defaultEmail') => $data['owner_name']),
            'replyTo'=> array(Configure::read('App.defaultEmail') => $data['owner_name'])
        );
        $data['function'] = 'upgradePlan';
        $data['email'] = 'ian@ionth.com';
        $this->sendMail($data, $options);
    }  
}

<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
App::uses('AppShell', 'Console/Command');
App::uses('Router', 'Routing');
config('routes');

class TaskShell extends AppShell {

    /**
     * for task deadline reminder
     */
    public function TaskDeadlineReminder() {

        $data = $this->args[0];
        $id = $data['id'];
        $end_date = new DateTime($data['end_date']);
        $end_date = $end_date->format(('Y-m-d H:i:s'));

        CakeLog::write('cronjob', 'TaskDeadline reminder initiate for Task:' . $id);

        $this->loadModel('Task');
        $this->loadModel('UserInfo');
        $this->loadModel('TaskAssignment');


        if (!$this->Task->exists($id)) {
            CakeLog::write('cronjob', 'Task:' . $id . ' not found');
            $this->out('Task:' . $id . ' not found');
            return;
        }

        $task = $this->Task->findById($id);

        if ($task['Task']['is_completed']) {
            CakeLog::write('cronjob', 'Task:' . $id . ' already completed');
            $this->out('Task:' . $id . ' already completed');
            return;
        }


        if ($task['Task']['end_date'] != $end_date) {
            CakeLog::write('cronjob', 'Task:' . $id . ' end_date changed from ' . $end_date . ' to ' . $task['Task']['end_date']);
            $this->out('Task:' . $id . ' end_date changed from ' . $end_date . ' to ' . $task['Task']['end_date']);
            return;
        }




         $this->TaskAssignment->bindModel(
                array('hasOne' => array(
                        'UserInfo' => array(
                            'className' => 'UserInfo',
                            'foreignKey' => false,
                            'conditions' => array('UserInfo.user_id = `TaskAssignment`.`user_id`')
                        )
                    )
                )
        );


        $task_assignments = $this->TaskAssignment->find('all', array(
            'conditions' => array(
                'TaskAssignment.task_id' => $id,
                'TaskAssignment.role' => ROLE_USER,
            ),
            'contain' => array(
                'UserInfo'
            ),
            'fields' => array(
                'DISTINCT TaskAssignment.user_id',
                'UserInfo.*',
            )
        ));




        $data = array(
            'function' => 'TaskDeadlineReminderChild'
        );

        $data = array_merge($data, $task);

        foreach ($task_assignments as $value) {
            $user = array(
                'name' => $value['UserInfo']['first_name'] . ' ' . $value['UserInfo']['last_name'],
                'email' => $value['UserInfo']['email'],
                'fname'=>$value['UserInfo']['first_name']
            );

            $data = array_merge($data, $user);

            $this->out(CakeResque::enqueue('default', 'TaskShell', array($data['function'], $data)));
        }


        CakeLog::write('cronjob', 'Deadline reminder for Task:' . $id);
    }

    public function TaskDeadlineReminderChild() {
        $data = $this->args[0];

        $options = array(
            'from' => array(Configure::read('App.defaultEmail') => 'Donna from Build'),
            'subject' => __d('tasks', 'Reminder | Your task %s is approaching.',$data['Task']['title']),
            'template' => 'Task/deadlinereminder',
        );
        $data['function'] = 'TaskDeadlineReminderChild';
        $data['event_type'] = 'TASK_DEADLINE';
        $this->sendMail($data, $options);
    }

    /**
     * when task is add
     * 
     * this mail is send to all the task-assignies
     * 
     * 
     */
    public function TaskAdd() {
        $this->log('task add--');
        $data = $this->args[0];
        $id = $data['id'];
        $current_user = $data['assigned_by_userid'];
        CakeLog::write('cronjob', 'TaskAdd initiate for Task:' . $id);

        $this->loadModel('Task');
        $this->loadModel('UserInfo');
        $this->loadModel('TaskAssignment');


        if (!$this->Task->exists($id)) {
            CakeLog::write('cronjob', 'Task:' . $id . ' not found');
            $this->out('Task:' . $id . ' not found');
            return;
        }

        $task = $this->Task->findById($id);


         $this->TaskAssignment->bindModel(
                array('hasOne' => array(
                        'UserInfo' => array(
                            'className' => 'UserInfo',
                            'foreignKey' => false,
                            'conditions' => array('UserInfo.user_id = `TaskAssignment`.`user_id`')
                        )
                    )
                )
        );

        $task_assignments = $this->TaskAssignment->find('all', array(
            'conditions' => array(
                'TaskAssignment.task_id' => $id,
                'TaskAssignment.role' => ROLE_USER,
                'TaskAssignment.created >=' => date('Y-m-d H:i:s', $data['before_save_timestamp']),
                'TaskAssignment.user_id !=' => $current_user,
            ),
            'contain' => array(
                'UserInfo'
            ),
            'fields' => array(
                'DISTINCT TaskAssignment.user_id',
                'UserInfo.*',
            )
        ));




        $data['function'] = 'TaskAddChild';

        $data = array_merge($data, $task);

        foreach ($task_assignments as $value) {
            $user = array(
                'name' => $value['UserInfo']['first_name'],
                'email' => $value['UserInfo']['email']
            );

            $data = array_merge($data, $user);

            $this->out(CakeResque::enqueue('default', 'TaskShell', array($data['function'], $data)));
        }


        CakeLog::write('cronjob', 'TaskAdd finish for Task:' . $id);
    }

    public function TaskAddChild() {
        $data = $this->args[0];
        $this->log('$data-----');
        $this->log($data);
        $options = array(
            'subject' => __d('tasks', '%s has shared a task with you on Build.',$data ['assigned_by']),
            'template' => 'Task/add',
            'from' => array(Configure::read('App.defaultEmail') => 'Donna from Build'),
            'replyTo' => array($data ['assigned_by_email']=> $data ['assigned_by']),
        );

        $data['function'] = 'TaskAddChild';
        $data['event_type'] = 'TASK_SHARED';
        $this->sendMail($data, $options);
    }

    /**
     * when task is completed
     * 
     * this mail is send to all task-assignies except the user who completed the task
     * 
     */
    public function TaskComplete() {
        $data = $this->args[0];
        $id = $data['id'];
        $url = $data['url'];
        $completed_by_id = $data['user_id'];
        CakeLog::write('cronjob', 'TaskComplete initiate for Task:' . $id);

        $this->loadModel('Task');
        $this->loadModel('UserInfo');
        $this->loadModel('TaskAssignment');


        if (!$this->Task->exists($id)) {
            CakeLog::write('cronjob', 'Task:' . $id . ' not found');
            $this->out('Task:' . $id . ' not found');
            return;
        }

        $task = $this->Task->findById($id);


        $this->TaskAssignment->bindModel(
                array('hasOne' => array(
                        'UserInfo' => array(
                            'className' => 'UserInfo',
                            'foreignKey' => false,
                            'conditions' => array('UserInfo.user_id = `TaskAssignment`.`user_id`')
                        )
                    )
                )
        );


        $task_assignments = $this->TaskAssignment->find('all', array(
            'conditions' => array(
                'TaskAssignment.task_id' => $id,
//                'TaskAssignment.role' => ROLE_ADMIN,
                'TaskAssignment.user_id !=' => $completed_by_id,
            ),
            'contain' => array(
                'UserInfo'
            ),
            'fields' => array(
                'DISTINCT TaskAssignment.user_id',
                'UserInfo.*',
            )
        ));

        $completed_by = $this->UserInfo->findByUserId($completed_by_id);
        $data = array(
            'function' => 'TaskComplete',
            'completed_by' => $completed_by['UserInfo']['first_name'],
            'url' => $url
        );

        $data = array_merge($data, $task);

        foreach ($task_assignments as $value) {

            $user = array(
                'name' => $value['UserInfo']['first_name'] . ' ' . $value['UserInfo']['last_name'],
                'email' => $value['UserInfo']['email'],
                'fname' =>$value['UserInfo']['first_name']
            );

            $data = array_merge($data, $user);

            $options = array(
                'subject' => __d('tasks', '%s has marked a task as complete on Build.',$data ['completed_by']),
                'template' => 'Task/complete',
                'from' => array(Configure::read('App.defaultEmail') => 'Donna from Build')
            );
            $data['event_type'] = 'TASK_SUBMIT';
            $this->sendMail($data, $options);
        }


        CakeLog::write('cronjob', 'TaskComplete finish for Task:' . $id);
    }

    /**
     * when task is edited
     * 
     * this mail is send to all the task-assignies except yhe user who edit that task
     * 
     */
    public function TaskEdit() {
        $data = $this->args[0];
        $id = $data['task_edited']['Task']['id'];
        $task_unedited = $data['task_unedited'];
        $task_edited = $data['task_edited'];
        $url = $data['url'];
        CakeLog::write('cronjob', 'TaskEdit initiate for Task:' . $id);

        $this->loadModel('Task');
        $this->loadModel('UserInfo');
        $this->loadModel('TaskAssignment');



        $fields_to_check = array('title', 'end_date', 'priority');
        $task_object['Task'] = array();
        foreach ($fields_to_check as $value) {
//            if ($task_unedited['Task'][$value] != $task_edited['Task'][$value]) {
                $task_object['Task'][$value] = array(
                    'old' => $task_unedited['Task'][$value],
                    'new' => $task_edited['Task'][$value]
                );
//            }
        }

        if (count($task_object['Task']) == 0) {
            CakeLog::write('cronjob', 'TaskEdit finish after no edit found for Task:' . $id);
            return;
            
        }

        $task_object['Task']['title']['new'] = $task_edited['Task']['title'];

        if (isset($task_object['Task']['priority'])) {
            $task_object['Task']['priority']['new'] = ($task_object['Task']['priority']['new'] == 1) ? 'Important' : 'Not important';
            $task_object['Task']['priority']['old'] = ($task_object['Task']['priority']['old'] == 1) ? 'Important' : 'Not important';
        }


         $this->TaskAssignment->bindModel(
                array('hasOne' => array(
                        'UserInfo' => array(
                            'className' => 'UserInfo',
                            'foreignKey' => false,
                            'conditions' => array('UserInfo.user_id = `TaskAssignment`.`user_id`')
                        )
                    )
                )
        );

        $task_assignments = $this->TaskAssignment->find('all', array(
            'conditions' => array(
                'TaskAssignment.task_id' => $id,
                'TaskAssignment.role' => ROLE_USER,
//                'TaskAssignment.created >=' => date('Y-m-d H:i:s', $data['before_save_timestamp'])
            ),
            'contain' => array(
                'UserInfo'
            ),
            'fields' => array(
                'DISTINCT TaskAssignment.user_id',
                'UserInfo.*',
            )
        ));
        #get all users that associate with that task
        $users = array();
        foreach ($task_assignments as $value) {
            $user = array(
                'name' => $value['UserInfo']['first_name'] . ' ' . $value['UserInfo']['last_name'],
            );
            array_push($users, $user);
        }
        
        $users = Hash::extract($users,'{n}.name');    
        $old_date = date_create($task_unedited['Task']['end_date']);
        $new_date = date_create($task_edited['Task']['end_date']);
//                    $user['User']['expiration'] = date_format($date, 'jS F');//g:ia \o\n l jS F Y

        $data = array(
            'function' => 'TaskEditChild',
            'edited_by' => $data['edited_by'],
            'old_enddate' => date_format($old_date, 'g:ia \o\n jS F'),//g:ia \o\n l jS F Y
            'new_enddate' => date_format($new_date, 'g:ia \o\n jS F'),
            'all_user' => $users,
            'old_all_users' => $data['old_user_all'],
           'url' => $url
        );

        $data = array_merge($data, $task_object);
        
        foreach ($task_assignments as $value) {
            $user = array(
                'name' => $value['UserInfo']['first_name'] . ' ' . $value['UserInfo']['last_name'],
                'email' => $value['UserInfo']['email'],
                'fname' => $value['UserInfo']['first_name']
            );

            $data = array_merge($data, $user);

            $this->out(CakeResque::enqueue('default', 'TaskShell', array($data['function'], $data)));
        }


        CakeLog::write('cronjob', 'TaskEdit finish for Task:' . $id);
    }

    public function TaskEditChild() {
        $data = $this->args[0];
        $options = array(
            'from' => array(Configure::read('App.defaultEmail') => 'Donna from Build'),
            'subject' => __d('tasks', '%s has changed details about a task on Build.',$data['edited_by']),
            'template' => 'Task/edit',
        );

        $data['function'] = 'TaskEditChild';
        $data['event_type'] = 'TASK_EDIT';

        $this->sendMail($data, $options);
    }
    
    
    
    /**
     * this mail is daily task remainder mail
     * 
     */
        public function TaskReminder() {
        $today_date = new DateTime('NOW');
        $today_date = date('Y-m-d');

        $this->loadModel('Task');
        $this->loadModel('UserInfo');
        $this->loadModel('TaskAssignment');
        #bind task to taskassociation for getting result
        $this->Task->bindModel(
                array('hasMany' => array(
                        'TaskAssignment' => array(
                            'className' => 'TaskAssignment',
                             'foreignKey' => 'task_id',
                             'dependent' => true,
                             'conditions' => array('TaskAssignment.isDeleted' => NOT_DELETED,
                                                    'TaskAssignment.removed_by' => 0
                                    )
                        )
                    )
                )
        );
        
        $tasks = $this->Task->find('all', array(
            'conditions' => array(
                /* Send email for network expiration after 1 day,7 day of expiration */
                    'Task.end_date  BETWEEN ? AND ?' => array(
                            date('Y-m-d H:i:s'),
                            date('Y-m-d H:i:s', strtotime('+1 days')),
                        ),
                        'Task.is_completed' => 0
            ),
            'contain' => array('TaskAssignment')
        ));

        $t = array();
        #check if task is listed or not
    if(!empty($tasks)){
        foreach ($tasks as $task) {
            
        $end_date = new DateTime($task['Task']['end_date']);
        $end_date = $end_date->format(('Y-m-d'));

        if (!$this->Task->exists($task['Task']['id'])) {
            CakeLog::write('cronjob', 'Task:' . $task['Task']['id'] . ' not found');
            $this->out('Task:' . $task['Task']['id'] . ' not found');
            continue;
        }
        
        if ($end_date != $today_date) {
            CakeLog::write('cronjob', 'Task:' . $task['Task']['id'] . ' end_date changed from ' . $today_date . ' to ' . $end_date);
            $this->out('Task:' . $task['Task']['id'] . ' end_date changed from ' . $today_date . ' to ' . $end_date);
            continue;
        }
            
            #get value of task-assigney's
            foreach ($task['TaskAssignment'] as $value) {
                
                $value['Task'] = $task['Task'];
                array_push($t, $value);
            }
        }
    
        $users = Hash::extract($tasks, '{n}.TaskAssignment.{n}.user_id');
        
        $users = array_unique($users);
        
        $task_assigny = array();
        foreach ($users as $key=>$user_id) {
            $temp = array();
            foreach ($t as $value) {
                if($user_id == $value['user_id']){
                    array_push($temp, $value);
                }
            }
            $tas = array($key => $temp);
            $task_assigny = array_merge($task_assigny, $tas);
        }
        $info = array();
        $data = array(
            'function' => 'TaskReminderChild'
        );

        foreach ($task_assigny as $value) {
            $task = Hash::extract($value, '{n}.Task');
            $user_id = Hash::extract($value, '{n}.user_id');
            $user = $this->UserInfo->findByUserId($user_id[0],array('UserInfo.first_name','UserInfo.email'));
            $task_temp = array(
                'Task' => $task,
                'fname' => $user['UserInfo']['first_name'],
                'email'=>$user['UserInfo']['email'],
                'url' => Router::url(array('admin' => false, 'plugin' => 'users', 'controller' => 'users', 'action' => 'login'), false)
             );
            $base =  Configure::read('fullBaseUrl');
            $task_temp['url'] = $base . $task_temp['url'];
            $this->out(CakeResque::enqueue('default', 'TaskShell', array($data['function'], $task_temp)));
        }
    }
    }

    public function TaskReminderChild() {
        $data = $this->args[0];
        $options = array(
            'from' => array(Configure::read('App.defaultEmail') => 'Donna from Build'),
            'subject' => __d('tasks', 'Here’s your agenda for the day.'),
            'template' => 'Task/task_reminder',
        );

        $data['function'] = 'TaskReminderChild';
        $data['event_type'] = 'TASK_REMINDER';
        $this->sendMail($data, $options);
    }

}

<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
App::uses('AppShell', 'Console/Command');
App::uses('Router', 'Routing');
App::uses('User', 'Model');
config('routes');

class MailShell extends AppShell {

    /**
     * Sends the invitation email
     *
     * This method is protected and not private so that classes that inherit this
     * controller can override this method to change the varification mail sending
     * in any possible way.
     *
     * @param string $to Receiver email address
     * @param array $options EmailComponent options
     * @return void
     */
    public function sendInvitationEmail() {
        $data = $this->args[0];
        $options = array(
            'subject' => __d('users', 'Working on Build going forward. Invite.'),
            'template' => 'Users.invitation',
            'from' => array(Configure::read('App.defaultEmail') => $data['invited_by'])
        );
        $data['function'] = 'sendInvitationEmail';

        $this->sendMail($data, $options);

        $this->notifyTo($data);
    }
    /**
     * send appear.in invitation like
     * 
     */
    public function sendInvitationLink() {
        $data = $this->args[0];

        $options = array(
            'from' => array(Configure::read('App.defaultEmail') => 'Donna from Build'),
            'subject' => __d('users', ' Invitation from %s for having a video call. Now.',$data['name']),
            'template' => 'Users.appear_invitation',
        );
        $data['function'] = 'sendInvitationLink';

        $this->sendMail($data, $options);
    }
    

    /**
     * Sends the verification email
     *
     * This method is protected and not private so that classes that inherit this
     * controller can override this method to change the varification mail sending
     * in any possible way.
     *
     * @param string $to Receiver email address
     * @param array $options EmailComponent options
     * @return void
     */
    protected function sendVerificationEmail() {
        $data = $this->args[0];
        $options = array(
            'from' => array(Configure::read('App.defaultEmail') => 'Donna from Build'),
            'subject' => __d('users', 'Welcome to Build. One small thing.'),
            'template' => 'Users.account_verification',
        );
        $data['function'] = 'sendVerificationEmail';

        $this->sendMail($data, $options);
        if (!isset($data['resend'])) {
            $this->notifyTo($data);
        }
    }

     /**
     * check if email is varified or not
     * if email is not verified then send mail
     */
    protected function checkVerificationEmail() {
        $data = $this->args[0];
        $this->loadModel('User');

        $result = $this->User->find('first', array(
            'conditions' => array(
                'User.email_verified' => 0,
                'User.email' => $data['email']
            ),
            'fields' => array(
                'User.id', 'User.email', 'User.username'
            )
        ));
        if(!empty($result['User']['email'])) {
        
        $options = array(
            'from' => array(Configure::read('App.defaultEmail') => 'Donna from Build'),
            'subject' => __d('users', 'Urgent. Email verification required for your Build account.'),
            'template' => 'Users.check_account_verification',
        );
        $data['function'] = 'checkVerificationEmail';

        $this->sendMail($data, $options);
        if (!isset($data['resend'])) {
            $this->notifyTo($data);
        }
        }
    }
    
         /**
     * check if email is varified or not
     * if email is not verified then send mail
     */
    protected function EndVerificationEmail() {
        $data = $this->args[0];
        $this->loadModel('User');

        $result = $this->User->find('first', array(
            'conditions' => array(
                'User.email_verified' => 0,
                'User.email' => $data['email']
            ),
            'fields' => array(
                'User.id', 'User.email', 'User.username'
            )
        ));
        if(!empty($result['User']['email'])) {
//        $thisYear = date('Y-m-d H:i:s', strtotime('-1 year'));
            $schedule_time = new DateTime($data['User']['email_expire']);
            $schedule_time->add(new DateInterval('P'. USER_REVERIFICATION_TIME.'D'));
            $res = $this->User->updateAll(
            array('User.email_token_expires' => $schedule_time),
                array('User.email' => $data['email'])
            );
        $options = array(
            'from' => array(Configure::read('App.defaultEmail') => 'Donna from Build'),
            'subject' => __d('users', 'Your verification link had expired. Here’s a new one.'),
            'template' => 'Users.end_account_verification',
        );
        $data['function'] = 'EndVerificationEmail';

        $this->sendMail($data, $options);
        }
    }
    
    
    
    
    /**
     * Sends the password reset email
     *
     * @param array
     * @return void
     */
    protected function sendNewPassword() {

        $data = $this->args[0];
        $options = array(
            'subject' => __d('users', 'New Password'),
            'template' => 'Users.new_password',
        );
        $data['function'] = 'sendNewPassword';


        $this->sendMail($data, $options);
    }

    /**
     * Checks if the email is in the system and authenticated, if yes create the token
     * save it and send the user an email
     *
     * @param boolean $admin Admin boolean
     * @param array $options Options
     * @return void
     */
    protected function sendPasswordReset() {
        $data = $this->args[0];
         $this->loadModel('UserInfo');

         $user = $this->UserInfo->find('first', array
            (
            'conditions' => array(
                'UserInfo.email' => $data['email']
            ),
            'fields' => array(
                'UserInfo.first_name'
            ),
              'order' => array('UserInfo.id desc')
            ));
        $data['fname'] = $user['UserInfo']['first_name'];
        
        
        $options = array(
            'from' => array(Configure::read('App.defaultEmail') => 'Donna from Build'),
            'subject' => __d('users', 'Lost your password? Here’s the password reset link.'),
            'template' => 'Users.password_reset_request',
        );
        $data['function'] = 'sendPasswordReset';
        $this->sendMail($data, $options);
    }

        /**
     * Sends email on password change
     *
     * This method is protected and not private so that classes that inherit this
     * controller can override this method to change the varification mail sending
     * in any possible way.
     *
     * @param string $to Receiver email address
     * @param array $options EmailComponent options
     * @return void
     */
    public function onPasswordChange() {
        $data = $this->args[0];
        $this->loadModel('UserInfo');
        $user = $this->UserInfo->find('first', array
            (
            'conditions' => array(
                'UserInfo.user_id' => $data['id']
            ),
            'fields' => array(
                'UserInfo.first_name','UserInfo.email'
            ))
        );
        $data['fname'] = $user['UserInfo']['first_name'];
        $data['email'] = $user['UserInfo']['email'];
        $options = array(
//            'from' => array(Configure::read('App.defaultEmail') => 'Donna from Build'),
            'subject' => __d('users','Your password for Build has been changed.'),
            'template' => 'Users.password_change'     
//            'replyTo'=> array(Configure::read('App.defaultEmail') => 'anupamapanchal@Build.in')
        );
        $data['function'] = 'onPasswordChange';

        $this->sendMail($data, $options);
    }

    
    /**
     * Notify on NOTIFY_EMAIL on User SignUp and User Invite
     *
     * @author Hardik Sondagar
     * @abstract Notify on NOTIFY_EMAIL on User SignUp and User Invite
     * @param array $data Data required for mailing
     *              $data contains array('email','url','name/invited_by','network_name','invite_url/verification_url','function_name_on_notify')
     * @return boolean
     * @since 08/09/14
     */
    protected function notifyTo($data) {

        /* Checking server_mode */
        /* Notify only if server_mode is production server i.e. on Gridle.io */
        if (isset($data['url']) && ( preg_match('/gridle.io/', $data['url']))) {

            /* replacing email with NOTIFY_EMAIL */
            $data['email_to_notify'] = $data['email'];
            $data['email'] = NOTIFY_EMAIL;
            
            /* Set function name on basis of function this called */
            
            /* *
             * Set notify function name according to caller function name
             * notify_function_name= 'notifyOn'.caller_function_name
             */
            $data['function'] = 'notifyOn' . $data['function'];
            
            CakeResque::enqueue('default', 'MailShell', array($data['function'], $data));
        
            
        }
    }

    protected function notifyOnsendVerificationEmail() {
        $data = $this->args[0];
        $options = array(
            'subject' => __d('users', 'User Register on Build'),
            'template' => 'Users.onregister',
        );
        $data['function'] = 'notifyOnsendVerificationEmail';
        $this->sendMail($data, $options);
    }

    protected function notifyOnsendInvitationEmail() {
        $data = $this->args[0];
        $options = array(
            'subject' => __d('users', 'User Invited on Build'),
            'template' => 'Users.oninvite',
        );
        $data['function'] = 'notifyOnsendInvitationEmail';
        $this->sendMail($data, $options);
    }
    
}

<?php

/**
 * AppShell file
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         CakePHP(tm) v 2.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
App::uses('Shell', 'Console');
App::uses('CakeEmail', 'Network/Email');

/**
 * Application Shell
 *
 * Add your application-wide methods in the class below, your shells
 * will inherit them.
 *
 * @package       app.Console.Command
 */
class AppShell extends Shell {

    var $cronjobs = array(
        'UserShell' => array(
            'VerificationExpired',
            'InvitationNotAccepted',
            'NotVerified',
        ),
        'NetworkShell' => array(
            'Expired',
            'BeforeExpired'
        )
    );

    public function initialize(){
        return;
    }

    public function perform() {
        $this->initialize();
        $this->{array_shift($this->args)}();
    }

    public function sendMail($data, $options) { 
        try {

            if (isset($data['event_type'])) {
                $this->loadModel('EmailSetting');
                $EmailSetting = $this->EmailSetting->findByEmail($data['email']);
                if (isset($EmailSetting['EmailSetting'][$data['event_type']]) && !$EmailSetting['EmailSetting'][$data['event_type']]) {
                    return;
                } else {
                    
                }
            }


            $key = Configure::read('mcrypt_key');

            $data['email_encode'] = base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($key), $data['email'], MCRYPT_MODE_CBC, md5(md5($key))));

            $data['email_encode_url_safe'] = strtr($data['email_encode'], "+/", "-_");



            $data['unsubscribe'] = Router::url(array('admin' => false, 'plugin' => 'users', 'controller' => 'users', 'action' => 'unsubscribe', $data['email_encode_url_safe'], HASH_HMAC('md5', $data['email_encode'] . $data['email'], Configure::read('secret_key'))), false);
            
            $base=  Configure::read('fullBaseUrl');
            $data['unsubscribe'] = $base . $data['unsubscribe'];
            
//$this->autoRender = false;
//            $this->layout = FALSE;
            $defaults = array(
                'from' => array(Configure::read('App.defaultEmail') => 'Team Build'),
                'subject' => __d('users', 'Build Invitation'),
                'template' => 'Users.invitation',
                'layout' => 'default',
                'emailFormat' => CakeEmail::MESSAGE_HTML,
                'replyTo'=> array(Configure::read('App.defaultEmail') => 'Team Build')
            );

            $options = array_merge($defaults, $options);
            $Email = $this->_getMailInstance();
            $Email->to($data['email'])
                    ->from($options['from'])
                    ->emailFormat($options['emailFormat'])
                    ->subject($options['subject'])
                    ->replyTo($options['replyTo'])
                    ->template($options['template'], $options['layout'])
                    ->viewVars(array(
                        'data' => $data
                    ))
                    ->send();
            CakeLog::write('email', 'Email sent to ' . $data['email'] . ' for subject ' . $options['subject']);
        } catch (Exception $e) {
            CakeLog::write('email', 'FAILED: Email send to ' . $data['email'] . ' for subject ' . $options['subject'] . $e->getMessage());
            // Resend the email if fail
            // We're assuming here that the only possible SocketException is the one thrown
            // when the email fail to be sent
            // CakeResque::enqueue('default', 'MailShell', array($data['function'], $data));
        }
    }

    /**
     * Returns a CakeEmail object
     *
     * @return object CakeEmail instance
     * @link http://book.cakephp.org/2.0/en/core-utility-libraries/email.html
     */
    protected function _getMailInstance() {
        $emailConfig = Configure::read('Users.emailConfig');
        if ($emailConfig) {
            return new CakeEmail($emailConfig);
        } else {
            return new CakeEmail('default');
        }
    }

    public function init() {

        $statusName = array(
            Resque_Job_Status::STATUS_WAITING => __d('cake_resque', 'waiting'),
            Resque_Job_Status::STATUS_RUNNING => __d('cake_resque', 'running'),
            Resque_Job_Status::STATUS_FAILED => __d('cake_resque', 'failed'),
            Resque_Job_Status::STATUS_COMPLETE => __d('cake_resque', 'complete')
        );

        if (Configure::read('CakeResque.Scheduler.enabled') === true) {
            $statusName[ResqueScheduler\Job\Status::STATUS_SCHEDULED] = __d('cake_resque', 'scheduled');
        }

        $schedule_time = new DateTime();
        $this->loadModel('Cronjob');
        foreach ($this->cronjobs as $shell => $cronjobs) {
            foreach ($cronjobs as $cronjob) {
                $job_id = CakeResque::enqueueIn(86400, 'default', $shell, array($cronjob, array()), true);
                if ($job_id) {

                    $job_status = CakeResque::getJobStatus($job_id);
                    $status = isset($statusName[$job_status]) ? $statusName[$job_status] : 'unknown';

                    $data = array(
                        'Cronjob' => array(
                            'job_id' => $job_id,
                            'name' => $cronjob,
                            'schedule_date' => $schedule_time->format('Y:m:d H:i:s'),
                            'status' => $status
                        )
                    );

                    $this->Cronjob->create();
                    $this->Cronjob->save($data);
                }
            }
        }
    }

    protected function testres() {
        $this->out(CakeResque::enqueue('default', 'AppShell', array('testshell', array())));
    }

    protected function testshell() {

        try {
            $this->out('Encryption Intiate');
            $this->out(extension_loaded('mcrypt'));
            $this->out(phpversion());
            $data['email'] = 'hardikmsondagar@gmail.com';

            $key = Configure::read('mcrypt_key');
            $data['email_encode'] = base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($key), $data['email'], MCRYPT_MODE_CBC, md5(md5($key))));
            $data['email_encode_url_safe'] = strtr($data['email_encode'], "+/", "-_");
            $data['unsubscribe'] = Router::url(array('admin' => false, 'plugin' => 'users', 'controller' => 'users', 'action' => 'unsubscribe', $data['email_encode_url_safe'], HASH_HMAC('md5', $data['email_encode'] . $data['email'], Configure::read('secret_key'))), false);

            $this->out($data['unsubscribe']);
            $base=  Configure::read('fullBaseUrl');
            $data['unsubscribe'] = $base . $output;
            return;
        } catch (Exception $e) {

            $this->out($e->getMessage());
        }

        return;
    }

}

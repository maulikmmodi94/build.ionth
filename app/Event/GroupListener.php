<?php

App::uses('CakeEventListener', 'Event');

//Function to generate logparticipant list
function user_array($list) {
    $array = [];
    $list = array_unique($list);
    foreach ($list as $key => $value) {
        $temp = [];
        $temp['type'] = Log_type_user;
        $temp['value'] = $value;
        array_push($array, $temp);
    }
    return $array;
}

function group_array($list) {
    $array = [];
    $list = array_unique($list);
    foreach ($list as $key => $value) {
        $temp = [];
        $temp['type'] = Log_type_group;
        $temp['value'] = $value;
        array_push($array, $temp);
    }
    return $array;
}

class GroupListener implements CakeEventListener {

    public function implementedEvents() {
        return array(
            'Controller.Group.afterCreate' => 'logGroupCreate',
            'Controller.Group.afterNetworkCreate' => 'GroupCreate',
            //'Controller.Group.afterCreate' => 'chatGroupCreate',
            'Controller.Group.afterEdit' => 'logGroupEdit',
            'Controller.Group.afterleave' => 'logGroupLeave',
            'Controller.Group.afteraddmember' => 'logGroupMemberAdd',
            //'Controller.Group.afteraddmember' => 'groupChatMemberAdd',
            'Controller.Group.aftermemberremove' => 'logGroupMemberRemove',
            //'Controller.Group.aftermemberremove' => 'groupChatMemberRemove',
            'Controller.Group.afterdelete' => 'logGroupDelete',
                //'Controller.Group.afterdelete' => 'chatGroupDelete',
        );
    }

    public function logGroupCreate(CakeEvent $event) {
        $this->Log = ClassRegistry::init('Log');
        $data = $event->data;

        $log_id = $this->Log->add($data['user_id'], $data['network_id'], Log_type_group, $data['group_id'], Log_activity_create);
        //debug($log_id);
        $list_user = user_array(array($data['user_id']));
        $list_group = group_array(array($data['group_id']));
        $this->Log->LogParticipant->add($log_id, $data['network_id'], array_merge($list_user, $list_group));
        $this->chatGroupCreate($event);
    }

    public function chatGroupCreate(CakeEvent $event) {
        $this->Conversation = ClassRegistry::init('Conversation');
        $data = $event->data;
        $this->Group = ClassRegistry::init('Group');
        $this->Conversation->createconversation($data['network_id'], $data['user_id'], array($data['user_id']), Conversation_mapping_group, $data['group_id'], 'This is Group Chat');
    }

    public function GroupCreate(CakeEvent $event) {
        $data = $event->data;
        $group_data['Group']['admin'] = $data['admin'];
        $group_data['Group']['network_id'] = $data['network_id'];
        $group_data['Group']['name'] = $data['group_name'];

        $this->Group = ClassRegistry::init('Group');
        $this->Group->add($group_data);
        $data['group_id'] = $this->Group->id;
        $this->UsersGroup = ClassRegistry::init('UsersGroup');
        $this->UsersGroup->addmember($data['group_id'], $data['network_id'], $data['admin'], $data['userlist']);
    }

    public function logGroupEdit(CakeEvent $event) {
        $this->Log = ClassRegistry::init('Log');
        $data = $event->data;

        $log_id = $this->Log->add($data['user_id'], $data['network_id'], Log_type_group, $data['group_id'], Log_activity_edit);

        $list_user = user_array(array($data['user_id']));
        $list_group = group_array(array($data['group_id']));
        $this->Log->LogParticipant->add($log_id, $data['network_id'], array_merge($list_user, $list_group));

        $this->Notification = ClassRegistry::init('Notification');
        $this->Group = ClassRegistry::init('Group');
        $users = $this->Group->UsersGroup->find('list', array(
            'conditions' => array('UsersGroup.group_id' => $data['group_id'], 'UsersGroup.user_id !=' => $data['user_id']),
            'fields' => array('UsersGroup.user_id')
        ));
        $this->Notification->add($users, $data['network_id'], $log_id);
    }

    public function logGroupDelete(CakeEvent $event) {
        $this->Log = ClassRegistry::init('Log');
        $this->Group = ClassRegistry::init('Group');
        $this->TaskAssignment = ClassRegistry::init('TaskAssignment');
        $data = $event->data;
        $log_id = $this->Log->add($data['user_id'], $data['network_id'], Log_type_group, $data['group_id'], Log_activity_delete);
        $this->Group->UsersGroup->defaultConditions = null;

        $users = $this->Group->UsersGroup->find('list', array('conditions' => array('UsersGroup.group_id' => $data['group_id'], 'UsersGroup.status' => STATUS_ACTIVE), 'fields' => array('UsersGroup.user_id')));
        $list_user = user_array(array_merge(array($data['user_id']), $users));
        $list_group = group_array(array($data['group_id']));
        $this->Log->LogParticipant->add($log_id, $data['network_id'], array_merge($list_user, $list_group));

        $this->Notification = ClassRegistry::init('Notification');
        $users = array_diff($users, array($data['user_id']));
        $this->Notification->add($users, $data['network_id'], $log_id);
        $this->chatGroupDelete($event);
        $this->TaskAssignment->deleteAssignee($data['group_id'], 'group', $data['user_id']);
    }

    public function chatGroupDelete(CakeEvent $event) {
        $this->Conversation = ClassRegistry::init('Conversation');
        $data = $event->data;
        $conversation_id = $this->Conversation->findByGroupId($data['group_id']);
        $this->Conversation->conversationDelete($conversation_id['Conversation']['id']);
    }

    public function logGroupLeave(CakeEvent $event) {
        $this->Log = ClassRegistry::init('Log');
        $data = $event->data;

        $log_id = $this->Log->add($data['user_id'], $data['network_id'], Log_type_group, $data['group_id'], Log_activity_member_left);
        $this->Group = ClassRegistry::init('Group');
        $admin = $this->Group->findById($data['group_id'], array('Group.admin'));

        $list_user = user_array(array($data['user_id'], $admin['Group']['admin']));
        $list_group = group_array(array($data['group_id']));
        $this->Log->LogParticipant->add($log_id, $data['network_id'], array_merge($list_user, $list_group));

        $this->Notification = ClassRegistry::init('Notification');
        $users = $this->Group->UsersGroup->find('list', array(
            'conditions' => array('UsersGroup.group_id' => $data['group_id'], 'UsersGroup.user_id !=' => $data['user_id']),
            'fields' => array('UsersGroup.user_id')
        ));
        $this->Notification->add($users, $data['network_id'], $log_id);
    }

    public function chatGroupLeave(CakeEvent $event) {
        $this->Conversation = ClassRegistry::init('Conversation');
        $data = $event->data;
        $conversation_id = $this->Conversation->findByGroupId($data['group_id']);
        $this->Conversation->UserConversation->leaveconversation($conversation_id['Conversation']['id'], $data['user_id']);
    }

    public function logGroupMemberAdd(CakeEvent $event) {
        $this->Log = ClassRegistry::init('Log');
        $data = $event->data;
        $log_id = $this->Log->add($data['user_id'], $data['network_id'], Log_type_group, $data['group_id'], Log_activity_member_add);

        $list_user = user_array(array_merge(array($data['user_id']), $data['list']));
        $list_group = group_array(array($data['group_id']));
        $this->Log->LogParticipant->add($log_id, $data['network_id'], array_merge($list_user, $list_group));

        $this->Notification = ClassRegistry::init('Notification');
        $this->Group = ClassRegistry::init('Group');
        $users = $this->Group->UsersGroup->find('list', array(
            'conditions' => array('UsersGroup.group_id' => $data['group_id'], 'UsersGroup.user_id !=' => $data['user_id']),
            'fields' => array('UsersGroup.user_id')
        ));
        $this->Notification->add($users, $data['network_id'], $log_id);
        $this->groupChatMemberAdd($event);
    }

    public function groupChatMemberAdd(CakeEvent $event) {
        $this->Conversation = ClassRegistry::init('Conversation');
        $data = $event->data;
        $this->Group = ClassRegistry::init('Group');
        $conversation_id = $this->Conversation->findByGroupId($data['group_id'], array('Conversation.id'));
        $this->Conversation->UserConversation->addparticipants($conversation_id['Conversation']['id'], $data['network_id'], $data['list'], $data['group_id']);
    }

    public function logGroupMemberRemove(CakeEvent $event) {
        $this->Log = ClassRegistry::init('Log');
        $data = $event->data;

        $log_id = $this->Log->add($data['admin'], $data['network_id'], Log_type_group, $data['group_id'], Log_activity_member_remove);
        $list_user = user_array(array($data['user_id'], $data['admin']));
        $list_group = group_array(array($data['group_id']));
        $this->Log->LogParticipant->add($log_id, $data['network_id'], array_merge($list_user, $list_group));

//        $this->Notification = ClassRegistry::init('Notification');
//        $this->Group = ClassRegistry::init('Group');
//        $this->Group->UsersGroup->defaultConditions = NULL;
//        $users = $this->Group->UsersGroup->find('list', array(
//            'conditions' => array('UsersGroup.group_id' => $data['group_id'], 'UsersGroup.user_id !=' => $data['admin']),
//            'fields' => array('UsersGroup.user_id')
//        ));
//        $this->Notification->add($users, $data['network_id'], $log_id);
        $this->groupChatMemberRemove($event);
    }

    public function groupChatMemberRemove(CakeEvent $event) {
        $this->Conversation = ClassRegistry::init('Conversation');
        $data = $event->data;
        $this->Group = ClassRegistry::init('Group');
        $conversation_id = $this->Conversation->findByGroupId($data['group_id'], array('Conversation.id'));
        $this->Conversation->UserConversation->removeparticipants($conversation_id['Conversation']['id'], $data['network_id'], $data['user_id'], $data['group_id']);
    }

}

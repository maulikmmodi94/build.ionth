<?php

App::uses('CakeEventListener', 'Event');

class UserListener implements CakeEventListener {

    public function implementedEvents() {
        return array(
            'Controller.User.afterLogin' => 'addSession',
            'Controller.User.afterRegistration' => 'afterRegistration',

        );
    }

    public function addSession(CakeEvent $event) {
        
        $this->UserSession = ClassRegistry::init('UserSession');
        $data = $event->data['request'];
        $this->UserSession->create();
        $this->UserSession->set('user_id', $event->data['user_id']);
        $this->UserSession->set('session_id', $event->data['session_id']);
        $this->UserSession->set('ip', $data->clientIp());
        $this->UserSession->set('user_agent', $data->header('User-Agent'));
        $this->UserSession->save();
    }


    public function afterRegistration(CakeEvent $event) {

        $this->EmailSetting = ClassRegistry::init('EmailSetting');
        $data = $event->data;
        $email_setting = $this->EmailSetting->findByEmail($data['User']['email']);
        
        if (empty($email_setting)) {
            $this->EmailSetting->create();
            $this->EmailSetting->set('user_id', $data['User']['id']);
            $this->EmailSetting->set('email', $data['User']['email']);
            $this->EmailSetting->save();
        }
        if(isset($data['User']['email_token'])){
        $email_data = array(
                        'name' =>$data['User']['name'],
                        'fname' => $data['UserInfo']['first_name'],
                        'email_token' => $data['User']['email_token'],
                        'email' => $data['User']['email'],
                        'email_token_expires' => $data['User']['expiration'],
                        'email_expire' => $data['User']['email_token_expires'],
                        'url'  => Router::url(array('admin' => false, 'plugin' => 'users', 'controller' => 'users', 'action' => 'verify', 'email', $data['User']['email_token']), true)  
                    );

        CakeResque::enqueue('default', 'MailShell', array('sendVerificationEmail', $email_data));
        /*email not verifed with in 3days*/
        CakeResque::enqueueIn(VARIFICATION_EXPIRATION,'default', 'MailShell', array('checkVerificationEmail', $email_data),TRUE);
//        CakeResque::enqueue('default', 'MailShell', array('checkVerificationEmail', $email_data));
        /*when verification link expires*/
        CakeResque::enqueueIn(EMAIL_TOKEN_EXPIRATION,'default', 'MailShell', array('EndVerificationEmail', $email_data),TRUE);
//        CakeResque::enqueue('default', 'MailShell', array('EndVerificationEmail', $email_data));

        }
    }

}
<?php

App::uses('CakeEventListener', 'Event');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class NetworkListener implements CakeEventListener {

    public function implementedEvents() {
        return array(
            'Controller.Network.afterCreate' => 'logNetworkCreate',
            'Controller.Network.afterEdit' => 'logNetworkEdit',
            'Controller.Network.afterAssignRole' => 'logNetworkAssignRole',
            'Controller.Network.afterMemberRemove' => 'logNetworkMemberRemove',
            'Controller.Network.afterMemberLeave' => 'logNetworkMemberLeave',
            'Controller.Network.afterMemberAdd' => 'logNetworkMemberAdd'
        );
    }

    public function logNetworkCreate(CakeEvent $event) {
        $this->Log = ClassRegistry::init('Log');
        $data = $event->data;
        $log_id = $this->Log->add($data['user_id'], $data['network_id'], Log_type_network, $data['network_id'], Log_activity_create);
        $list_user = user_array(array($data['user_id']));
        $this->Log->LogParticipant->add($log_id, $data['network_id'], $list_user);
    }

    public function logNetworkEdit(CakeEvent $event) {
        $this->Log = ClassRegistry::init('Log');
        $data = $event->data;
        $log_id = $this->Log->add($data['user_id'], $data['network_id'], Log_type_network, $data['network_id'], Log_activity_edit);
        $list_user = user_array(array($data['user_id']));
        $this->Log->LogParticipant->add($log_id, $data['network_id'], $list_user);

        $this->Notification = ClassRegistry::init('Notification');
        $this->Network = ClassRegistry::init('Network');
        $users = $this->Network->UserNetwork->find('list', array(
            'conditions' => array('UserNetwork.network_id' => $data['network_id'], 'UserNetwork.user_id !=' => $data['user_id']),
            'fields' => array('UserNetwork.user_id')
        ));
        $this->Notification->add($users, $data['network_id'], $log_id);
    }

    public function logNetworkAssignRole(CakeEvent $event) {
        $this->Log = ClassRegistry::init('Log');
        $data = $event->data;
        $info['info'] = $data['role'];
        $log_id = $this->Log->add($data['owner'], $data['network_id'], Log_type_network, $data['network_id'], Log_activity_network_assignrole, $info);
        $list_user = user_array(array($data['user_id'], $data['owner']));
        $this->Log->LogParticipant->add($log_id, $data['network_id'], $list_user);

        $this->Notification = ClassRegistry::init('Notification');
        $this->Notification->add(array($data['user_id']), $data['network_id'], $log_id);
    }

    public function logNetworkMemberRemove(CakeEvent $event) {
        $this->Log = ClassRegistry::init('Log');
        $data = $event->data;
        $log_id = $this->Log->add($data['admin'], $data['network_id'], Log_type_network, $data['network_id'], Log_activity_member_remove);
        $list_user = user_array(array($data['user_id'], $data['admin']));
        $this->Log->LogParticipant->add($log_id, $data['network_id'], $list_user);
    }

    public function logNetworkMemberLeave(CakeEvent $event) {
        $this->Log = ClassRegistry::init('Log');
        $data = $event->data;
        $log_id = $this->Log->add($data['admin'], $data['network_id'], Log_type_network, $data['network_id'], Log_activity_member_left);
        $list_user = user_array(array($data['user_id'], $data['admin']));
        $this->Log->LogParticipant->add($log_id, $data['network_id'], $list_user);
    }


    public function logNetworkMemberAdd(CakeEvent $event) {
        debug($event->data);
        $this->Log = ClassRegistry::init('Log');
        $data = $event->data;
        $log_id = $this->Log->add($data['admin'], $data['network_id'], Log_type_network, $data['network_id'], Log_activity_member_add);
        $list_user = user_array(array_merge(array($data['admin']), $data['userlist']));
        $this->Log->LogParticipant->add($log_id, $data['network_id'], $list_user);
    }

}

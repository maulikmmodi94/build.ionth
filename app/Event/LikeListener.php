<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

App::uses('CakeEventListener', 'Event');
App::uses('CakeEventManager', 'Event');


/**
 * Description of LikeListener
 *
 * @author Manish M Demblani
 */
class LikeListener implements CakeEventListener{
    public function implementedEvents() {
        return array(
            'Behavior.Likeable.afterLike' => 'afterLike',
            'Behavior.Likeable.afterDislike' => 'afterDislike',
        );
    }
    
    /**
     * Listener called when a Like operation is performed.
     * @param CakeEvent $event
     */
    public function afterLike(CakeEvent $event) {
        $data = $event->data;
        $data['model'] = strtolower($data['model']);
        $this->Log = ClassRegistry::init('Log');
        $log_id = $this->Log->add($data['user_id'], $data['network_id'], $data['model'], $data['foreign_id'], Log_activity_like_feed);
        $list_user = user_array(array($data['user_id']));
        $this->Log->LogParticipant->add($log_id, $data['network_id'], $list_user);
        $this->Notification = ClassRegistry::init('Notification');
        //If likes get bound to different models i.e. Feeds and Tasks then use switch case
        $this->Feed = ClassRegistry::init('Feed');
        $user = $this->Feed->feedLike($data['foreign_id']);
        if(!empty($user)) {
            $this->Notification->add($user, $data['network_id'], $log_id);
        }
    }
    
    /**
     * Listener called when a Dislike operation is performed.
     * @param CakeEvent $event
     */
    public function afterDislike(CakeEvent $event) {
        $data = $event->data;
        $data['model'] = strtolower($data['model']);
        $this->Log = ClassRegistry::init('Log');
        $log_id_last = $this->Log->find('first',array(
                    'conditions' => array('Log.user_id' => $data['user_id'], 
                                          'Log.network_id' => $data['network_id'], 
                                          'Log.type' => $data['model'], 
                                          'Log.object_id' => $data['foreign_id'], 
                                          'Log.activity' => Log_activity_like_feed),
                    'fields' => array('Log.id'),
                    'order' => array('Log.id DESC')
            ));
        $log_id_last = $log_id_last['Log']['id'];
        $log_id = $this->Log->add($data['user_id'], $data['network_id'], $data['model'], $data['foreign_id'], Log_activity_dislike_feed);
        $list_user = user_array(array($data['user_id']));
        $this->Log->LogParticipant->add($log_id, $data['network_id'], $list_user);
        //Performing soft delete
        $this->Notification = ClassRegistry::init('Notification');
        $last_like_not = $this->Notification->find('first',array(
                            'conditions' => array('Notification.log_id' => $log_id_last),
                            'fields' => array('Notification.id')
        ));
        //If likes get bound to different models i.e. Feeds and Tasks then use switch case
        $this->Feed = ClassRegistry::init('Feed');
        $user = $this->Feed->feedLike($data['foreign_id']);
        //$userlist, $network_id
        if(!empty($user)) {
            $this->Notification->deleteNotification($user, $data['network_id'], $last_like_not['Notification']['id']);
        }
    }
    
    
    
}

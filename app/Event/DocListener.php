<?php

App::uses('CakeEventListener', 'Event');

class DocListener implements CakeEventListener {

    public function implementedEvents() {
        return array(
            'Controller.Doc.afterCreate' => 'logDocCreate',
            'Controller.Doc.afterEdit' => 'logDocEdit',
            'Controller.Doc.aftermemberadd' => 'logDocMemberAdd',
            'Controller.Doc.aftermemberremove' => 'logDocMemberRemove',
            'Controller.Doc.afterdelete' => 'logDocDelete'
        );
    }

    public function logDocCreate(CakeEvent $event) {
        $this->Log = ClassRegistry::init('Log');
        $data = $event->data;
        $log_id = $this->Log->add($data['user_id'], $data['network_id'], Log_type_file, $data['doc_id'], Log_activity_create);
        $list_user = user_array(array($data['user_id']));
        $this->Log->LogParticipant->add($log_id, $data['network_id'], $list_user);
    }

    public function logDocEdit(CakeEvent $event) {
        $this->Log = ClassRegistry::init('Log');
        $data = $event->data;

        $log_id = $this->Log->add($data['user_id'], $data['network_id'], Log_type_file, $data['doc_id'], Log_activity_edit);
        $list_user = user_array(array($data['user_id']));

        $this->Log->LogParticipant->add($log_id, $data['network_id'], $list_user);

        $this->Notification = ClassRegistry::init('Notification');
        $this->Doc = ClassRegistry::init('Doc');
        $users = $this->Doc->DocAssignment->find('list', array(
            'conditions' => array('DocAssignment.doc_id' => $data['doc_id'], 'DocAssignment.user_id !=' => $data['user_id']),
            'fields' => array('DocAssignment.user_id')
        ));
        $this->Notification->add($users, $data['network_id'], $log_id);
    }

    public function logDocMemberAdd(CakeEvent $event) {
        $this->Log = ClassRegistry::init('Log');
        $data = $event->data;
        $log_id = $this->Log->add($data['user_id'], $data['network_id'], Log_type_file, $data['doc_id'], Log_activity_member_add);

        $this->Doc = ClassRegistry::init('Doc');
        $list_user = user_array(array_merge(array($data['user_id']), $data['userlist']));
        $group_user = group_array($data['grouplist']);
        $this->Log->LogParticipant->add($log_id, $data['network_id'], array_merge($list_user, $group_user));

        $this->Notification = ClassRegistry::init('Notification');
        $this->Doc = ClassRegistry::init('Doc');
        $users = $this->Doc->DocAssignment->find('list', array(
            'conditions' => array('DocAssignment.doc_id' => $data['doc_id'], 'DocAssignment.user_id !=' => $data['user_id']),
            'fields' => array('DocAssignment.user_id')
        ));
        $this->Notification->add($users, $data['network_id'], $log_id);

        CakeResque::enqueue('default', 'DocShell', array('DocAdd', array(
                'id' => $data['doc_id'],
                'user_id' => $data['user_id'],
                'before_save_timestamp' => $data['before_save_timestamp'],
                'group_id' => $group_user,
                's_fname' => $data['s_fname'],
            'url' => Router::url(array('admin' => false, 'plugin' => 'users', 'controller' => 'users', 'action' => 'login'), true)
        )));

    }

    public function logDocMemberRemove(CakeEvent $event) {
        $this->Log = ClassRegistry::init('Log');
        $data = $event->data;

        $log_id = $this->Log->add($data['user_id'], $data['network_id'], Log_type_file, $data['doc_id'], Log_activity_member_remove);

        $this->Doc = ClassRegistry::init('Doc');
        $list_user = user_array(array_merge(array($data['user_id']), $data['userlist']));
        $group_user = group_array($data['grouplist']);
        $this->Log->LogParticipant->add($log_id, $data['network_id'], array_merge($list_user, $group_user));

        $this->Notification = ClassRegistry::init('Notification');
        $this->Doc = ClassRegistry::init('Doc');
        $users = $this->Doc->DocAssignment->find('list', array(
            'conditions' => array('DocAssignment.doc_id' => $data['doc_id'], 'DocAssignment.user_id !=' => $data['user_id']),
            'fields' => array('DocAssignment.user_id')
        ));
        $this->Notification->add($users, $data['network_id'], $log_id);
    }

    public function logDocDelete(CakeEvent $event) {
        $this->Log = ClassRegistry::init('Log');
        $data = $event->data;

        $log_id = $this->Log->add($data['user_id'], $data['network_id'], Log_type_file, $data['doc_id'], Log_activity_delete);

        $this->Doc = ClassRegistry::init('Doc');
        $this->Doc->DocAssignment->defaultConditions = null;
        $list = $this->Doc->DocAssignment->find('all', array('conditions' => array('DocAssignment.doc_id' => $data['doc_id'], 'DocAssignment.status' => STATUS_ACTIVE), 'fields' => array('DocAssignment.user_id', 'DocAssignment.group_id')));
        $users = [];
        $groups = [];
        foreach ($list as $value) {
            if ($value['DocAssignment']['user_id'] != null) {
                array_push($users, $value['DocAssignment']['user_id']);
            } else {
                array_push($groups, $value['DocAssignment']['group_id']);
            }
        }
        $list_user = user_array(array_merge(array($data['user_id']), $users));
        $group_user = group_array($groups);
        $this->Log->LogParticipant->add($log_id, $data['network_id'], array_merge($list_user, $group_user));

        $this->Notification = ClassRegistry::init('Notification');
        $users = array_diff($users, array($data['user_id']));
        $this->Notification->add($users, $data['network_id'], $log_id);
    }

}

<?php

App::uses('CakeEventListener', 'Event');

class TaskListener implements CakeEventListener {

    public function implementedEvents() {
        return array(
            'Controller.Task.afterAdd' => 'logTaskCreate',
            'Controller.Task.afterEdit' => 'logTaskEdit',
            'Controller.Task.afteraddmember' => 'logTaskMemberAdd',
            'Controller.Task.aftermemberremove' => 'logTaskMemberRemove',
            'Controller.Task.aftersetComplete' => 'logTaskMarkComplete',
            'Controller.Task.aftersetUncomplete' => 'logTaskMarkUncomplete',
            'Controller.Task.afterdelete' => 'logTaskDelete'
        );
    }

    public function logTaskCreate(CakeEvent $event) {
        $this->Log = ClassRegistry::init('Log');
        $data = $event->data;
        $log_id = $this->Log->add($data['user_id'], $data['network_id'], Log_type_task, $data['task_id'], Log_activity_create);
        $list_user = user_array(array($data['user_id']));
        $this->Log->LogParticipant->add($log_id, $data['network_id'], $list_user);
    }

    public function logTaskEdit(CakeEvent $event) {
        $this->Log = ClassRegistry::init('Log');
        $data = $event->data;

        $log_id = $this->Log->add($data['user_id'], $data['network_id'], Log_type_task, $data['task_id'], Log_activity_edit);

        $list_user = user_array(array($data['user_id']));
        $this->Log->LogParticipant->add($log_id, $data['network_id'], $list_user);

        $this->Notification = ClassRegistry::init('Notification');
        $this->Task = ClassRegistry::init('Task');
        $users = $this->Task->TaskAssignment->find('list', array(
            'conditions' => array('TaskAssignment.task_id' => $data['task_id'], 'TaskAssignment.user_id !=' => $data['user_id']),
            'fields' => array('TaskAssignment.user_id')
        ));
        $this->Notification->add($users, $data['network_id'], $log_id);
    }

    public function logTaskMarkComplete(CakeEvent $event) {
        $this->Log = ClassRegistry::init('Log');
        $this->Task = ClassRegistry::init('Task');
        $data = $event->data;

        $log_id = $this->Log->add($data['user_id'], $data['network_id'], Log_type_task, $data['task_id'], Log_activity_task_complete);

        $task_creator = $this->Task->findById($data['task_id'], array('Task.created_by'));
        $list_user = user_array(array($data['user_id'], $task_creator['Task']['created_by']));
        $this->Log->LogParticipant->add($log_id, $data['network_id'], $list_user);

        $this->Notification = ClassRegistry::init('Notification');
        $this->Task = ClassRegistry::init('Task');
        $users = $this->Task->TaskAssignment->find('list', array(
            'conditions' => array('TaskAssignment.task_id' => $data['task_id'], 'TaskAssignment.user_id !=' => $data['user_id']),
            'fields' => array('TaskAssignment.user_id')
        ));
        $unique_users = array_unique($users);
        $this->Notification->add($unique_users, $data['network_id'], $log_id);
    }

    public function logTaskMarkUncomplete(CakeEvent $event) {
        $this->Log = ClassRegistry::init('Log');
        $this->Task = ClassRegistry::init('Task');
        $data = $event->data;

        $log_id = $this->Log->add($data['user_id'], $data['network_id'], Log_type_task, $data['task_id'], Log_activity_task_uncomplete);

        $task_creator = $this->Task->findById($data['task_id'], array('Task.created_by'));
        $list_user = user_array(array($data['user_id'], $task_creator['Task']['created_by']));
        $this->Log->LogParticipant->add($log_id, $data['network_id'], $list_user);

        $this->Notification = ClassRegistry::init('Notification');
        $this->Task = ClassRegistry::init('Task');
        $users = $this->Task->TaskAssignment->find('list', array(
            'conditions' => array('TaskAssignment.task_id' => $data['task_id'], 'TaskAssignment.user_id !=' => $data['user_id']),
            'fields' => array('TaskAssignment.user_id')
        ));
        $unique_users = array_unique($users);
        $this->Notification->add($unique_users, $data['network_id'], $log_id);
    }

    public function logTaskDelete(CakeEvent $event) {
        $this->Log = ClassRegistry::init('Log');
        $this->Task = ClassRegistry::init('Task');
        $data = $event->data;
        $log_id = $this->Log->add($data['user_id'], $data['network_id'], Log_type_task, $data['task_id'], Log_activity_delete);

        $this->Task->TaskAssignment->defaultConditions = null;
        $list = $this->Task->TaskAssignment->find('all', array('conditions' => array('TaskAssignment.task_id' => $data['task_id'], 'TaskAssignment.status' => STATUS_ACTIVE), 'fields' => array('TaskAssignment.user_id', 'TaskAssignment.group_id')));
        $users = [];
        $groups = [];
        foreach ($list as $value) {
            if ($value['TaskAssignment']['user_id'] != null) {
                array_push($users, $value['TaskAssignment']['user_id']);
            } else {
                array_push($groups, $value['TaskAssignment']['group_id']);
            }
        }
        $list_user = user_array(array_merge(array($data['user_id']), $users));
        $group_user = group_array($groups);
        $this->Log->LogParticipant->add($log_id, $data['network_id'], array_merge($list_user, $group_user));

        $this->Notification = ClassRegistry::init('Notification');
        $users = array_diff($users, array($data['user_id']));
        $this->Notification->add($users, $data['network_id'], $log_id);
    }

    public function logTaskMemberAdd(CakeEvent $event) {
        $this->Log = ClassRegistry::init('Log');
        $data = $event->data;

        $log_id = $this->Log->add($data['user_id'], $data['network_id'], Log_type_task, $data['task_id'], Log_activity_member_add);

        $list_user = user_array(array_merge(array($data['user_id']),$data['listuser']));
        $list_group = group_array($data['listgroup']);
        $this->Log->LogParticipant->add($log_id, $data['network_id'], array_merge($list_user, $list_group));

        $this->Notification = ClassRegistry::init('Notification');
        $this->Task = ClassRegistry::init('Task');
        $users = $this->Task->TaskAssignment->find('list', array(
            'conditions' => array('TaskAssignment.task_id' => $data['task_id'], 'TaskAssignment.user_id !=' => $data['user_id']),
            'fields' => array('TaskAssignment.user_id')
        ));
        $this->Notification->add($users, $data['network_id'], $log_id);
    }

    public function logTaskMemberRemove(CakeEvent $event) {
        $this->Log = ClassRegistry::init('Log');
        $data = $event->data;

        $log_id = $this->Log->add($data['user_id'], $data['network_id'], Log_type_task, $data['task_id'], Log_activity_member_remove);

        $list_user = user_array(array_merge(array($data['user_id']), $data['userlist']));
        $group_user = group_array($data['grouplist']);
        $this->Log->LogParticipant->add($log_id, $data['network_id'], array_merge($list_user, $group_user));

        $this->Notification = ClassRegistry::init('Notification');
        $this->Task = ClassRegistry::init('Task');
        $users = $this->Task->TaskAssignment->find('list', array(
            'conditions' => array('TaskAssignment.task_id' => $data['task_id'], 'TaskAssignment.user_id !=' => $data['user_id']),
            'fields' => array('TaskAssignment.user_id')
        ));
        $this->Notification->add($users, $data['network_id'], $log_id);
    }

}

<?php

App::uses('CakeEventListener', 'Event');
App::uses('CakeEventManager', 'Event');

class CommentListener implements CakeEventListener {

    public function implementedEvents() {
        return array(
            'Behavior.Commentable.afterCreateComment' => 'afterCommentAdd',
        );
    }

    public function afterCommentAdd(CakeEvent $event) {
        $data = $event->data;
        $data['Comment']['model'] = $this->getModelName($data['Comment']['model']);
        $data = $data['Comment'];
        switch ($data['model']) {
            case 'task':
                $this->taskComment($data);
                break;
            case 'doc':
                $this->docComment($data);
            default:
                break;
        }
    }

    protected function getModelName($name) {
        return strtolower($name);
    }

    public function taskComment($data) {
        $this->Log = ClassRegistry::init('Log');
        $log_id = $this->Log->add($data['user_id'], $data['network_id'], $data['model'], $data['entity_id'], Log_activity_comment_add);
        $list_user = user_array(array($data['user_id']));
        $this->Log->LogParticipant->add($log_id, $data['network_id'], $list_user);

        $this->Notification = ClassRegistry::init('Notification');
        $this->Task = ClassRegistry::init('Task');
        $users = $this->Task->TaskAssignment->find('list', array(
            'conditions' => array('TaskAssignment.task_id' => $data['entity_id'], 'TaskAssignment.user_id !=' => $data['user_id']),
            'fields' => array('TaskAssignment.user_id')
        ));
        $this->Notification->add($users, $data['network_id'], $log_id);
    }

    public function docComment($data) {
        $this->Log = ClassRegistry::init('Log');

        $log_id = $this->Log->add($data['user_id'], $data['network_id'], Log_type_file, $data['entity_id'], Log_activity_comment_add);
        $list_user = user_array(array($data['user_id']));

        $this->Log->LogParticipant->add($log_id, $data['network_id'], $list_user);

        $this->Notification = ClassRegistry::init('Notification');
        $this->Doc = ClassRegistry::init('Doc');
        $users = $this->Doc->DocAssignment->find('list', array(
            'conditions' => array('DocAssignment.doc_id' => $data['entity_id'], 'DocAssignment.user_id !=' => $data['user_id']),
            'fields' => array('DocAssignment.user_id')
        ));
        $this->Notification->add($users, $data['network_id'], $log_id);
    }

}

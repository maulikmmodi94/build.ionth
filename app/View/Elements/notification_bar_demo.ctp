<div class="notificationBar padding-lr-0" ng-show="showNotifications" style="width: 28%;" ng-controller="notificationCtrl">
    <!--NOTIFICATION HEAD--> 
    <div class="notificationHead">
        <p class="text-bold">
            <span class="fa fa-bell"></span> &nbsp;&nbsp;Notifications
            <span class="pull-right">&times;</span>
        </p>
    </div>
    <!--NOTIFICATIOSN LIST-->
    <div class="container-fluid notifications-list">
        
        <!--task notification--> 
        <div class="row notification-item" ng-click="openTaskNotification()">
            <div class="col-sm-2  notification-icon">
                <p class="text-right text-muted">
                    <span class="fa fa-share-square-o"></span>
                </p>
            </div>
            <div class="col-sm-10">
                <p>Yash Shah shared a task with you.</p>
                <p class="text-muted font-small-2">Need the designs completed before 10th June...</p>
            </div>
        </div>
        
        <!--task complete--> 
        <div class="row notification-item active">
            <div class="col-sm-2  notification-icon">
                <p class="text-right color-green">
                    <span class="fa fa-check-square"></span>
                </p>
            </div>
            <div class="col-sm-10">
                <p>Abhishek doshi completed the task.</p>
                <p class="text-muted font-small-2">Need the designs completed before 10th June...</p>
            </div>
        </div>
        
        <!--Meeting--> 
        <div class="row notification-item">
            <div class="col-sm-2  notification-icon">
                <p class="text-right text-muted">
                    <span class="fa fa-clock-o"></span>
                </p>
            </div>
            <div class="col-sm-10">
                <p>Yash Shah scheduled a meeting</p>
                <p class="text-muted font-small-2">Meeting with Rohit, Anupama and Abhishek. <span class="btn-link">Sunday, 4pm</span> </p>
            </div>
        </div>
        <!--adding in team--> 
        <div class="row notification-item" ng-click="openTeamNotification()">
            <div class="col-sm-2  notification-icon">
                <p class="text-right text-muted">
                    <span class="fa fa-group "></span>
                </p>
            </div>
            <div class="col-sm-10">
                <p>You were added to a team</p>
                <p class="text-muted font-small-2">Gridle revamp</p>
            </div>
        </div>
        
        <!--File notification-->
        <div class="row notification-item" ng-click="openFileNotification()">
            <div class="col-sm-2  notification-icon">
                <p class="text-right text-muted">
                    <span class="fa fa-file-o"></span>
                </p>
            </div>
            <div class="col-sm-10">
                <p>Yash Shah shared a file with you.</p>
                <p class="text-muted font-small-2">Investordeck.pdf</p>
            </div>
        </div>
        <!--TASK--> 
        <div class="row notification-item">
            <div class="col-sm-2  notification-icon">
                <p class="text-right text-muted">
                    <span class="fa fa-comment-o"></span>
                </p>
            </div>
            <div class="col-sm-10">
                <p>Yash Shah commented on task.</p>
                <p class="text-muted font-small-2">Need the designs completed before 10th June...</p>
            </div>
        </div>
        <!--Deadline reminder--> 
        <div class="row notification-item">
            <div class="col-sm-2  notification-icon">
                <p class="text-right color-red">
                    <span class="fa fa-exclamation "></span>
                </p>
            </div>
            <div class="col-sm-10">
                <p>Task deadline by yash shah.</p>
                <p class="text-muted font-small-2">Need the designs completed before 10th June...</p>
            </div>
        </div>
        <!--task notification--> 
        <div class="row notification-item">
            <div class="col-sm-2  notification-icon">
                <p class="text-right text-muted">
                    <span class="fa fa-share-square-o"></span>
                </p>
            </div>
            <div class="col-sm-10">
                <p>Yash Shah shared a task with you.</p>
                <p class="text-muted font-small-2">Need the designs completed before 10th June...</p>
            </div>
        </div>
        <!--task notification--> 
        <div class="row notification-item">
            <div class="col-sm-2  notification-icon">
                <p class="text-right text-muted">
                    <span class="fa fa-share-square-o"></span>
                </p>
            </div>
            <div class="col-sm-10">
                <p>Yash Shah shared a task with you.</p>
                <p class="text-muted font-small-2">Need the designs completed before 10th June...</p>
            </div>
        </div>
        
    </div>
</div>
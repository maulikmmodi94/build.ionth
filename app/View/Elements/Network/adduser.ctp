<!--        MODAL WINDOW FOR NEW TEAM          -->
<div id='networkModalAddMembers' class="modal fade">
    <div class="g-modal modal-dialog" >
        <!--        CONTENT          -->
        <div class="g-modal-content modal-content">
            <!--        HEADER          -->
            <div class="g-modal-header modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="g-modal-title modal-title ">
                    <span class="g-modal-header-icon fa fa-group"></span> 
                    <span class="g-modal-header-title-text">Add Member</span>
                </h4>
            </div>
            <!--        BODY          -->
            <div class="g-modal-body modal-body">
                <div class="users form">
                    <?php
                    echo $this->Form->create('User', array('action' => 'admin_add','id'=>'formNetworksInvite'));
                    $this->Form->unlockField('email');
                    ?>
                    <fieldset>
                        <small class="text-right">{{networks[currentnetwork]['user_limit']-networks[currentnetwork]['user_count']-Emailtags.length}} remaining</small>
                        <tags-input ng-model="Emailtags" display-property="email" placeholder="Add Member" allowed-tags-pattern='^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$'>
                        </tags-input>                        
                        
                        <div class="input text required" ng-repeat="tag in Emailtags">
                            <label for="email{{$index + 1}}"></label>
                            <input name="data[email][{{$index + 1}}]" type="hidden" id="email{{$index + 1}}" ng-value="tag.email" required="required"></input>
                        </div>
                        
                    </fieldset>

                </div>
            </div>
            <div class="modal-footer">
                <?php
                echo $this->Form->submit(__d('networks', 'Add Members'), array('class' => 'btn btn-success','id'=>'btn-network-add-member'));
                echo $this->Form->end();
                ?>
            </div>

        </div>
    </div>
</div>
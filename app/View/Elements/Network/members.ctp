<!--        MODAL WINDOW FOR Network members          -->
<div id='networkModalMembers' class="modal fade">
    <div class="g-modal modal-dialog" >
        <!--        CONTENT          -->
        <div class="g-modal-content modal-content">
            <!--        HEADER          -->
            <div class="g-modal-header modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="g-modal-title modal-title ">
                    <span class="g-modal-header-icon fa fa-group"></span> 
                    <span class="g-modal-header-title-text"><span ng-bind="networks[network_selected].name"></span>'s Members</span>
                </h4>
            </div>
            <!--        BODY          -->
            <div class="g-modal-body modal-body">
                <div class="g-modal-sm-body">
                    <div class="g-network-members-list g-scrollbar row">
                        <div id="itemUser{{$index}}" class="g-desk-tile g-background-offwhite" ng-repeat="user in network_users[network_selected]">
                            <div class="g-desk-tile-item">
                                <div class="g-desk-tile-name" ng-class='{}'>
                                    <p>
                                    <g-profile-img type='user' path='{{user.UserInfo.vga}}' height="25px" width="25px" name='{{user.UserInfo.name}}' myclass='g-desk-tile-user-img'></g-profile-img>
                                    <span class="" ng-bind='user.UserInfo.name'></span>
                                    </p>    
                                </div>

                            </div>
                        </div>
                    </div>  

                </div>
            </div>
        </div>
    </div>
</div>
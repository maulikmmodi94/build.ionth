<!-- HEADER -->
<header id="g-header" class="navbar navbar-default navbar-fixed-top one-edge-shadow text-center container-fluid" >
    <div class="row">
        <!--    gridle logo -->
        <p class="g-logo-gridle-cover p-margin-0">
            <a id="g-logo-gridle" href="/pages/home" >
                <object data="/img/logos/gridle_logo.svg" type="image/svg+xml">
                    <img src="/img/logos/gridle_logo.png" alt="gridle logo" />
                </object>
            </a>
        </p>
        <!--        if logged in            -->
        <div id="g-header-right" class="header_nav pull-right"> 
            <div class="g-header-dropdown dropdown pull-left">
                <a href="#" class="g-header-dropdown-btn dropdown-toggle" data-toggle="dropdown"><button class="btn btn-default"><i class="fa fa-bars"></i></button></a>
                <ul class="g-header-dropdown-menu dropdown-menu dropdown-menu-right">
                    <li><a class="g-header-dropdown-link" href="/pages/home">Home</a></li>
                    <li class="divider"></li>
                    <li><a class="g-header-dropdown-link" href="/users/add">Sign up</a></li> 
                    <li><a class="g-header-dropdown-link"  href="/users/login">Login</a></li>
                    <li class="divider"></li>
                    <li><a class="g-header-dropdown-link" href="/pages/pricing">Pricing</a></li>
                    <li><a class="g-header-dropdown-link" href="/pages/support">Support</a></li>
                    <li><a class="g-header-dropdown-link" href="/pages/contact">Contact</a></li>
                </ul>
            </div>
        </div>
    </div>
</header>
<?php echo $this->Html->script('bootstrap/collapse.js', false); ?>
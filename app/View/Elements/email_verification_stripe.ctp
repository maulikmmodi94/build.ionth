<?php if (!$AuthUser['email_verified']) { ?>
    <p class="email-verify-strip font-small-1 " style="">Your email is not yet verfied. Please verify it before <?php echo date("F j, Y", strtotime("+7 day", strtotime($AuthUser['created']))) ?> | Didn't get the mail ? <a href="/users/resend_verification/" style="">Click here to resend</a></p>
<?php } ?>

<nav class="navbar g-navbar" role="navigation" ng-controller="headerController">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">

        <!--        <button type="button" class="navbar-toggle" ng-click="navCollapsed = !navCollapsed">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>-->
        <a class="navbar-brand visible-xs" href="#"><img src="/img/logos/ionth_logo.svg"/></a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" ng-class="!navCollapsed">

        <!--LOGO--> 
        <div class="col-sm-1">
            <img src="/img/logos/ionth_logo.svg" alt="build | ionth logo" height="50"/>
        </div>
        <!--SEARCH BOX--> 
        <div class="col-sm-3 col-sm-offset-2 padding-top-10">
            <div class="search-task-box ">
                <input class="form-control input-sm font-small-2" placeholder="Search a Task" ng-model="searchTask" ng-enter="taskSearchHeader(searchTask)" ng-show="issearchTask" style="padding-right: 25px;" ng-change="checktaskSearch()"/>

                <input class="form-control input-sm font-small-2" placeholder="Search File by Name" ng-model="filequery" ng-change="searchFile(filequery)" typeahead="tag as tag.Doc for tag in searchFileTags" typeahead-on-select="getFileSelected(filequery)" typeahead-min-length="2" typeahead-wait-ms="500" typeahead-template-url="/scripts/View/typeaheadFile.html" ng-hide="issearchTask" style="padding-right: 25px;"/>
                <span ng-hide="searchTask.length > 0" class="fa fa-search"></span>
                <span ng-show="searchTask.length > 0" class="fa fa-times" ng-click="clearSearch()" style="cursor: pointer"></span>
            </div>
        </div>


        <!--NAVBAR RIGHT--> 
        <ul class="nav navbar-nav navbar-right" style="margin-right: 0;">
            <li>
                <a href="/feeds">
                    <div class="navBarLink color-black-alpha-low text-bold" ng-class="{
                            'active'
                            :currenturlPath == 'feeds'}">
                        <p class="icon icon-basic-rss pull-left font-big-3 margin-right-5" style="padding-top: 2px"></p>
                        <p class="pull-left" >Feed</p>     
                    </div>
                </a>
            </li>
            <li>
                <a href="/desks">
                    <div class="navBarLink color-black-alpha-low text-bold" ng-class="{
                            'active'
                            :currenturlPath == 'desks'}">
                        <p class="icon icon2-arrows-glide pull-left font-big-3 margin-right-5" style="padding-top: 2px"></p>
                        <p class="pull-left" >Desk</p>     
                    </div>
                </a>
            </li>
            <li>
                <a href="/docs">
                    <div class="navBarLink color-black-alpha-low text-bold" ng-class="{
                            'active'
                            :currenturlPath == 'docs'}">
                        <p class="icon icon-basic-usb pull-left font-big-3 margin-right-5" style="padding-top: 4px"></p>
                        <p class="pull-left">Files</p>     
                    </div>
                </a>
            </li>
            <li class="dropdown li-header-profile" dropdown on-toggle="toggled(open)" ng-controller="usersController">
                <a href="#" class="dropdown-toggle" dropdown-toggle>
                    <div class="navBarLink color-black-alpha-low">
                        <p class="">
                            <span ng-if="user_networks.UserInfo">
                                <g-profile-img path="user_networks.UserInfo.vga" type="user" name="loggeduser_name" myclass="g-img-25 font-small-2 header-user" ></g-profile-img>
                            </span>
                        </p>
                        <!--<p class="pull-left font-big-2 icon icon2-arrows-down " style='margin-bottom: -10px;' ></p>-->
                    </div>
                </a>
                <ul class="dropdown-menu dropdown-animation" role="menu">
                    <li class="container-fluid" ng-click="OpenProfileSettings()" >
                        <a href="#" class="row">
                            <div class="col-sm-2 padding-lr-0" style="padding-top:5px;">
                                <span class="icon icon-basic-gear font-big-2" style=""></span>
                            </div>
                            <div class="col-sm-10 padding-tb-5">
                                <p class="" >
                                    <span>My Profile</span>
                                </p>    
                            </div>
                        </a>
                    </li>
                    <li class="container-fluid">
                        <a href="/users/logout" class="row">
                            <div class="col-sm-2 padding-lr-0" style="padding-top:5px;">
                                <span class="icon icon2-arrows-button-off font-big-2" style=""></span>
                            </div>
                            <div class="col-sm-10 padding-tb-5">
                                <p class="" >
                                    <span>Logout</span>
                                </p>    
                            </div>
                        </a>
                    </li>
                </ul>
            </li>
            <li ng-click="toggleNotifications()" >
                <a href="" ng-click="notificationClick()">
                    <div class="navBarLink text-dim" tooltip-placement="top" tooltip="Notifications" ng-class="" >
                        <p class="fa fa-bell-o font-big-1" ng-class="{
                                'color-red-secondary'
                                :showNotifications  }">
                            <span ng-bind="notificationsCount" ng-show="notificationsCount > 0" class="font-small-4 notification-count-style" tooltip-placement="bottom" tooltip="Notifications"></span>
                        </p>
                    </div>
                </a>
            </li>
            <li ng-click="toogleRightBar()" tooltip-placement="top" tooltip="Toggle Taskbar">
                <a href="#">
                    <div class="navBarLink color-black-alpha-low margin-right-5">
                        <p><span class="fa fa-bars"></span></p>
                    </div>
                </a> 
            </li>
        </ul>
    </div><!-- /.navbar-collapse -->
</nav>

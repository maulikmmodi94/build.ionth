<div class="sidebarLeft col-sm-2" ng-controller="sidebarCtrl">

    <!--NETWORK INFO--> 
    <div class="network-info row" ng-controller="DropdownCtrl">
        <li class="dropdown networkOptions" dropdown on-toggle="toggled(open)">
            <a href="#" class="dropdown-toggle" dropdown-toggle >
                <div class="col-sm-2 padding-lr-0">
                    <p class="network-icon">G</p>
                </div>
                <div class="network-name col-sm-8 padding-lr-0">
                    <p class="text-muted font-small-3">Workspace</p>
                    <p class="font-small-1 text-muted text-bold">Gridle</p>
                </div>
                <div class="col-sm-1 padding-lr-0">
                    <p class="text-center dropdown-caret color-white "><span class="fa fa-caret-down"></span></p>
                </div>
            </a>
            <ul class="dropdown-menu" role="menu" ng-controller="workspaceCtrl">
                <li ng-click="openModalRenameWS()">
                    <a href="#"><span class="fa fa-pencil"></span> Rename workspace</a>
                </li>
                <li>
                    <a href="#"><span class="fa fa-anchor"></span> Manage workspace</a>
                </li>
                <li>
                    <a href="/networks/index_demo">
                        <span class="fa fa-globe">  All workspaces</span>
                    </a> 
                </li>
            </ul>
        </li>
    </div>

    <!--SEARCH BAR--> 
    <div class="row search-box">
        <div class="form-group">
            <input class="form-control search-bar input-sm" placeholder="Search for people or team" >
        </div> 
    </div>
    <!--FILTER ON--> 
    <div class=" row" ng-show="taskFilterOn">
        <div class="bg-blue color-white filter-bar">
            <p> <img class="round-25" src="/img/demo/anupama.png" /> <span class="text-bold">{{selectedUser.name}}</span> <span class="pull-right text-bold cursor-pointer" ng-click="clearFilter()">&times;</span></p>
        </div>
    </div>


    <!--TEAMS--> 
    <div class="row teams-list" ng-init="collapseTeams = true;">
        <p class="btn-team-add color-blue text-bold" ng-click="addTeam()" >Add a team</p>
        <p class="color-blue">Things I am working on</p>
        <p class="list-item">Sales <span class="pull-right fa fa-gear text-muted"></span></p>
        <p class="list-item">SuperDevs</p>
        <p class="list-item">Content</p>
        <p class="list-item">Design issues</p>
        <p class="list-item">Error messages</p>
        <p class="list-item">Hackathon</p>

        <p class="text-muted font-small-2 cursor-pointer" ng-click="toggleTeams()" ng-show="collapseTeams">+5 more</p>

        <div ng-show="collapseTeams == false">
            <p class="list-item">Hackathon</p>
            <p class="list-item">wwhere</p>
            <p class="list-item">iBelieve</p>
            <p class="list-item">Locator</p>
            <p class="list-item">YMCA</p>
            <p class="list-item">Karnavati</p>
            <p class="list-item">Hackathon</p>
            <p class="list-item">Rajpath</p>
            <p class="btn-link cursor-pointer font-small-2">Load more..</p>

        </div>
        <p class="text-muted font-small-2 cursor-pointer" ng-show="!collapseTeams" ng-click="toggleTeams()">see less</p>

    </div>

    <!--USERS--> 
    <div class="row users-list">
        <p class="btn-invite color-yellow text-bold" ng-click="inviteToNetwork()" ng-show="checkPeople()">It's lonely in here.</p>
        <p class="btn-invite color-yellow text-bold" ng-click="inviteToNetwork()">Add People</p>
        <p class="list-item">Abhishek Doshi</p>
        <p class="list-item">Anupama Panchal</p>
        <p class="list-item">Yash Shah</p>
        <p class="list-item">Manish Demblani</p>
        <p class="list-item">Maulik Modi</p>
        <p class="list-item">Shashwat Bhatt</p>

        <p class="text-muted font-small-2 cursor-pointer" ng-click="togglePeople()" ng-show="collapsePeople">+6 more</p>

        <div ng-show="collapsePeople == false">
            <p class="list-item">Rajvi Gandhi</p>
            <p class="list-item">Raj Miglani</p>
            <p class="list-item">Yash Goleccha</p>
            <p class="list-item">Anshuman Agarwal</p>
            <p class="list-item">Ritam Bhatnagar</p>
            <p class="list-item">Megha </p>
            <p class="list-item">Tanvi </p>
            <p class="btn-link cursor-pointer font-small-2">Load more..</p>

        </div>
        <p class="text-muted font-small-2 cursor-pointer" ng-show="!collapsePeople" ng-click="togglePeople()">see less</p>
    </div>
    <div class="sidebar-shortcut-box cursor-pointer" ng-controller="appearCtrl" ng-click="launchAppearChat()">
        <div class="col-sm-3 col-sm-offset-1 padding-lr-0 shortcut-box">
            <img src="/img/logos/vendor/appear.png" height="40" class="pull-right" />
        </div>
        <div class="col-sm-8 padding-lr-0 shortcut-box">
            <p class="text-bold">Start a video meeting</p>
        </div>
    </div>
</div>
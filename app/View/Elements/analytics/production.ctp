<!--------------------   Begin Google Analytics  ----------------------------------------------->
<script>
    //google analytics code for production
    (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function () {
            (i[r].q = i[r].q || []).push(arguments)
        }, i[r].l = 1 * new Date();
        a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

<?php
// New Google Analytics code to set User ID.
// $userId is a unique, persistent, and non-personally identifiable string ID.
if (isset($AuthUser['email'])) {
    $gacode = "ga('create', 'UA-48117396-1', { 'userId': '%s' });";
    echo sprintf($gacode, $AuthUser['email']);
} else {
    echo "ga('create', 'UA-48117396-1', 'auto');";
}
?>            ga('send', 'pageview');

</script>
<!--------------------------------    End Google Analytics   ------------------------------------------>

<!--    MOUSEFLOW ANALYTICS     -->
<script type="text/javascript">
    var _mfq = _mfq || [];
    (function () {
        var mf = document.createElement("script");
        mf.type = "text/javascript";
        mf.async = true;
        mf.src = "//cdn.mouseflow.com/projects/8218922d-70da-4b80-a257-0559ceaf826e.js";
        document.getElementsByTagName("head")[0].appendChild(mf);
    })();
</script>

<!--Start of Zopim Live Chat Script-->
<script type="text/javascript">
    window.$zopim || (function (d, s) {
        var z = $zopim = function (c) {
            z._.push(c)
        }, $ = z.s =
                d.createElement(s), e = d.getElementsByTagName(s)[0];
        z.set = function (o) {
            z.set.
                    _.push(o)
        };
        z._ = [];
        z.set._ = [];
        $.async = !0;
        $.setAttribute('charset', 'utf-8');
        $.src = '//v2.zopim.com/?3EzQLo8U8O0jrhFhbOt7PLN2lLJZRqie';
        z.t = +new Date;
        $.
                type = 'text/javascript';
        e.parentNode.insertBefore($, e)
    })(document, 'script');
</script>
<!--End of Zopim Live Chat Script-->




<?php if (isset($AuthUser['email'])) { ?>

    <!---------------------     Post login analytics for sass    -----------------------> 


    <!--            Intercom begins             --> 
    <script>
        window.intercomSettings = {
            // TODO: The current logged in user's full name
            name: "<?php echo $AuthUser['UserInfo']['first_name'] . ' ' . $AuthUser['UserInfo']['last_name']; ?>",
            // TODO: The current logged in user's email address.
            email: "<?php echo $AuthUser['email']; ?>",
            // TODO: The current logged in user's sign-up date as a Unix timestamp.
            created_at: new Date('<?php echo CakeSession::read('Auth.User.UserInfo.created'); ?>').getTime() / 1000,
            app_id: "eded34726767b7a63a8bb8a6c8be178c9bd5bf24"
        };
    </script>
    <script>(function () {
            var w = window;
            var ic = w.Intercom;
            if (typeof ic === "function") {
                ic('reattach_activator');
                ic('update', intercomSettings);
            } else {
                var d = document;
                var i = function () {
                    i.c(arguments)
                };
                i.q = [];
                i.c = function (args) {
                    i.q.push(args)
                };
                w.Intercom = i;
                function l() {
                    var s = d.createElement('script');
                    s.type = 'text/javascript';
                    s.async = true;
                    s.src = 'https://widget.intercom.io/widget/eded34726767b7a63a8bb8a6c8be178c9bd5bf24';
                    var x = d.getElementsByTagName('script')[0];
                    x.parentNode.insertBefore(s, x);
                }
                if (w.attachEvent) {
                    w.attachEvent('onload', l);
                } else {
                    w.addEventListener('load', l, false);
                }
            }
        })()
    //zopim after login 
        $zopim(function () {
            $zopim.livechat.setName("<?php echo $AuthUser['UserInfo']['first_name'] . ' ' . $AuthUser['UserInfo']['last_name']; ?>");
            $zopim.livechat.setEmail("<?php echo $AuthUser['email']; ?>");
        });

    </script>



    <!------------------------      Intercom ends       --------------------------------> 
<?php } ?>

<!-- Begin Inspectlet Embed Code -->
<script type="text/javascript" id="inspectletjs">
    window.__insp = window.__insp || [];
    __insp.push(['wid', 1645017761]);
    (function () {
        function __ldinsp() {
            var insp = document.createElement('script');
            insp.type = 'text/javascript';
            insp.async = true;
            insp.id = "inspsync";
            insp.src = ('https:' == document.location.protocol ? 'https' : 'http') + '://cdn.inspectlet.com/inspectlet.js';
            var x = document.getElementsByTagName('script')[0];
            x.parentNode.insertBefore(insp, x);
        }
        if (window.attachEvent) {
            window.attachEvent('onload', __ldinsp);
        } else {
            window.addEventListener('load', __ldinsp, false);
        }
    })();
</script>
<!-- End Inspectlet Embed Code -->


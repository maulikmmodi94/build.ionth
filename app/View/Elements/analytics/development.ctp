
<?php if (isset($AuthUser['email'])) { ?>

    <!---------------------     Post login analytics for sass    -----------------------> 


    <!--            Intercom begins             --> 
    <script>
        window.intercomSettings = {
            // TODO: The current logged in user's full name
            name: "<?php echo $AuthUser['UserInfo']['first_name'].' '.$AuthUser['UserInfo']['last_name']; ?>",
            // TODO: The current logged in user's email address.
            email: "<?php echo $AuthUser['email']; ?>",
            // TODO: The current logged in user's sign-up date as a Unix timestamp.
            created_at: new Date('<?php echo CakeSession::read('Auth.User.UserInfo.created'); ?>').getTime(),
            app_id: "eded34726767b7a63a8bb8a6c8be178c9bd5bf24"
        };
    </script>
    <script>(function() {
            var w = window;
            var ic = w.Intercom;
            if (typeof ic === "function") {
                ic('reattach_activator');
                ic('update', intercomSettings);
            } else {
                var d = document;
                var i = function() {
                    i.c(arguments)
                };
                i.q = [];
                i.c = function(args) {
                    i.q.push(args)
                };
                w.Intercom = i;
                function l() {
                    var s = d.createElement('script');
                    s.type = 'text/javascript';
                    s.async = true;
                    s.src = 'https://widget.intercom.io/widget/eded34726767b7a63a8bb8a6c8be178c9bd5bf24';
                    var x = d.getElementsByTagName('script')[0];
                    x.parentNode.insertBefore(s, x);
                }
                if (w.attachEvent) {
                    w.attachEvent('onload', l);
                } else {
                    w.addEventListener('load', l, false);
                }
            }
        })()</script>
    <!------------------------      Intercom ends       --------------------------------> 
<?php } ?>


<!-- Begin Freshdesk widget    
<script type="text/javascript" src="https://s3.amazonaws.com/assets.freshdesk.com/widget/freshwidget.js"></script>
<script type="text/javascript">
    FreshWidget.init("", {"queryString": "&widgetType=popup&formTitle=How+could+we+help+you+today%3F&submitThanks=Thank+you.+We+will+revert+asap.+Meanwhile%2C+you+can+also+drop+us+a+line+at+yashshah%5Bat%5Dgridle%5Bdot%5Dio+or+tweet+us+%40gridle_io&searchArea=no", "widgetType": "popup", "buttonType": "text", "buttonText": "Get in touch", "buttonColor": "#555", "buttonBg": "#F7F5EB", "alignment": "4", "offset": "550px", "submitThanks": "Thank you. We will revert asap. Meanwhile, you can also drop us a line at yashshah[at]gridle[dot]io or tweet us @gridle_io", "formHeight": "500px", "url": "https://gridle.freshdesk.com"});
</script>-->
<!--   End Freshdesk widget    -->
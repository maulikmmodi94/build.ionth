<!-- HEADER -->
<?php

function select_header() {
    switch ($_SERVER['REQUEST_URI']) {
        case '/networks' : return 'focused';
        case '/pages/new_network' : return 'focused';
        case '/networks/add' : return 'no_links';
        case '/networks/add_members': return 'no_links';
    };
}
?>
<header id="g-header" class="navbar navbar-default navbar-fixed-top one-edge-shadow text-center container-fluid" role="navigation" ng-controller="navigationController">
    <div class="g-header-wrap">
        
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" style="z-index:1;">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>


        <!--    gridle logo -->
        <p class="g-logo-gridle-cover p-margin-0">
            <a id="g-logo-gridle" href="/desks">
                <object data="/img/logos/gridle_logo.svg" type="image/svg+xml">
                    <img src="/img/logos/gridle_logo.png" alt="gridle logo" height="50px" />
                </object>
            </a>
        </p>
        <?php if (select_header() !== 'no_links') { ?>
            <!--  left navigation  -->
            <div id="g-header-left" class="pull-left"> 
                <!--    All networks menu       -->
                <ul class="nav navbar-nav">

                    <li class="g-header-dropdown dropdown text-muted" ng-if="networks[currentnetwork].name" >
                        <a href='#' class='g-header-dropdown-btn dropdown-toggle' data-toggle='dropdown'>
                            <g-profile-img ng-if='networks' type='network' name='{{networks[currentnetwork].name}}' myclass='header-network-img font-small-1' ></g-profile-img> 
                            <span ng-bind='networks[currentnetwork].name' class='network-name-c'></span>&nbsp;&nbsp;
                            <span class="caret"></span>
                        </a>
                        <ul class='dropdown-menu text-muted' role='menu'>
                            <li class='dropdown-header text-capitalize'>
                                <h4 class="text-warning text-center"><?= $Networks[$CurrentNetwork]['name']; ?> Network&nbsp;&nbsp;
                            </li>
                            <!--    DASHBOARD        -->
                            <li>
                                <a href="/networks/view/{{networks[currentnetwork].url}}"><span class='fa fa-compass'></span> Dashboard</a>
                            </li>
                            <!--    ACTIVITY LOG       -->
                            <li><a href='/logs'><span class='fa fa-history'></span> Activity Log</a></li>

                            <li class='divider'>
                                <!--    See all networks    -->
                            <li class='bg-warning'><a href='/networks' ><span class='fa fa-globe'></span> All networks</a></li>
                        </ul>
                    </li>

                    <li class='visible-lg visible-md'><a href='#'>What's new?</a></li>
                </ul>
            </div>

            <!--    right navigation -->
            <div id="g-header-right" class=" pull-right collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav" ng-controller="headerController">
                    <?php if (select_header() !== 'focused') { ?>
                        <!--    NOTIFICATION        -->
                        <li class='header-in-nav-link g-header-dropdown dropdown' ng-init='getNotificationUnseenCount();getNotificationsData(true)'>
                            <a href='#' ng-click="notificationInit();" class='g-header-dropdown-btn dropdown-toggle text-capitalize <?php if (strpos($_SERVER['REQUEST_URI'], '/notifications') !== false) echo 'active'; ?>' data-toggle="dropdown" data-placement="bottom" title="notifications" >Notification <span class="badge badge-danger notification-badge" ng-if="notificationsUnseenCount>0" ng-bind="notificationsUnseenCount"></span></a>
                            <ul class='notifications-dropdown dropdown-menu-right dropdown-menu' notification-drop-down role='menu'>
                            </ul>
                        </li>
                        <!--    DESK        -->
                        <li class="header-in-nav-link"><a href="/desks" class="<?php if (strpos($_SERVER['REQUEST_URI'], '/desks') !== false) echo 'active'; ?>" >Desk</a></li>
                        <!--    PLANNER        -->
                        <li class="header-in-nav-link" ><a href="/tasks" class="<?php if (strpos($_SERVER['REQUEST_URI'], '/tasks') !== false) echo 'active'; ?>">Planner</a></li>
                        <!--    DOCUMENTS        -->
                        <li class="header-in-nav-link" ><a href="/docs" class="<?php if (strpos($_SERVER['REQUEST_URI'], '/docs') !== false) echo 'active'; ?>">Files</a></li>
                        <!--    CONVERSATIONS        -->
                        <li class="header-in-nav-link" ><a href="/conversations" class="<?php if (strpos($_SERVER['REQUEST_URI'], '/conversations') !== false) echo 'active'; ?>" ng-init='getConversationUnreadCount()'>Chat <span class="badge badge-danger notification-badge" ng-if="convUnreadCount" ng-bind="convUnreadCount"></span></a></li>
                    <?PHP } ?>
                    <!--    USER IMAGE       -->
                    <li class='g-header-dropdown dropdown'>
                        <a href='#' class='g-header-dropdown-btn dropdown-toggle text-capitalize' data-toggle="dropdown">
                            <g-profile-img type='user' path="<?= $AuthUser['UserInfo']['vga']; ?>" height="25px" width="25px" myclass="header-user-img" name="<?= $AuthUser['UserInfo']['name']; ?>" ></g-profile-img>&nbsp;&nbsp; <span class="caret"></span> </a>
                        <ul class='dropdown-menu-right dropdown-menu' role='menu'>
                            <li class='dropdown-header text-capitalize'>
                                <h4 class="text-muted"><?= $AuthUser['UserInfo']['name']; ?>&nbsp;&nbsp;
                                    <g-profile-img type='user' path="<?= $AuthUser['UserInfo']['vga']; ?>" height="25px" width="25px" myclass="header-user-img" name="<?= $AuthUser['UserInfo']['name']; ?>" ></g-profile-img>                 
                                </h4>

                            </li>

                            <li class='divider'></li>
                            <!--    SETTINGS        -->
                            <li><a href='/users/settings'><span class='fa fa-gear'></span> Settings &amp; Profile</a></li>
                            <!--    LOG OUT        -->
                            <li><a href='/users/logout'><span class='fa fa-power-off'></span> Log out</a></li>
                            <li class='divider'></li>
                            <li class='dropdown-header bg-warning'>Upgrade your account</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        <?php } //end of no_links header ?>
    </div>
</header>
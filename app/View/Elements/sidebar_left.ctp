<div class="sidebarLeft col-sm-2 hidden-xs" ng-controller="navBarController" ng-class="{'top-on-tour': $root.tourStep == 3 && $root.tourOn}">
    <!--NETWORK INFO--> 
    <div class=" row">
        <div class="col-sm-12 padding-lr-0 network-info padding-tb-10 cursor-pointer">
            <li class="dropdown networkOptions" dropdown on-toggle="toggled(open)">
                <a href="#" class="dropdown-toggle" dropdown-toggle>
                    <div class="col-sm-3">
                        <p class="network-icon"><span ng-bind="currentNetwork[0]" class="text-capitalize"></span></p>
                    </div>
                    <div class="network-name col-sm-7">
                        <p class="text-bold line-height-30"><span ng-bind="currentNetwork"></span></p>
                    </div>
                    <div class="col-sm-1 padding-lr-0">
                        <p class="text-center dropdown-caret color-white font-big-1"><span class="fa fa-chevron-circle-down"></span></p>
                    </div>
                </a>
                <ul class="dropdown-menu dropdown-animation" role="menu" id="networkMenu12">
                    <li class="container-fluid" ng-click="openModalEditWorkspace()" ng-show="user_role_network == 'owner'">
                        <a href="#" class="row">
                            <div class="col-sm-2 padding-lr-0" style="padding-top:5px;">
                                <span class="icon icon-basic-settings font-big-2" style=""></span>
                            </div>
                            <div class="col-sm-10 padding-tb-5" ng>
                                <p class="" >
                                    <span>Rename workspace</span>
                                </p>    
                            </div>
                        </a>
                    </li>
                    <li role="separator" class="divider" ng-show="user_role_network == 'owner'"></li>
                    <li class="container-fluid">
                        <a  href="/networks/view/{{user_networks.Network.url}}" class="row">
                            <div class="col-sm-2 padding-lr-0" style="padding-top:5px;">
                                <span class="icon icon-basic-mixer2 font-big-2" style=""></span>
                            </div>
                            <div class="col-sm-10 padding-tb-5">
                                <p class="" >
                                    <span ng-show="user_role_network == 'owner'">Manage  workspace</span>
                                    <span ng-show="user_role_network != 'owner'">View workspace</span>
                                </p>    
                            </div>
                        </a>
                    </li>
                    <li role="separator" class="divider"></li>
                    <li class='container-fluid'>    
                        <a href="/networks" class='row'>
                            <div class="col-sm-2 padding-lr-0" style="padding-top:5px;">
                                <span class="icon icon-basic-notebook-pencil font-big-2" style=""></span>
                            </div>
                            <div class="col-sm-10 padding-tb-5">
                                <p class="" >
                                    <span>All workspaces</span>
                                </p>    
                            </div>
                        </a> 
                    </li>
                    <li role="separator" class="divider" ng-show="user_role_network == 'owner'"></li>
                    <li class='container-fluid' ng-click="openModalUpgradeWorkSpaceRequest()" ng-show="user_role_network == 'owner'">    
                        <a href="#" class='row'>
                            <div class="col-sm-2 padding-lr-0" style="padding-top:5px;">
                                <span class="icon icon-basic-paperplane font-big-2" style=""></span>
                            </div>
                            <div class="col-sm-10 padding-tb-5">
                                <p class="" >
                                    <span>Upgrade Plan</span>
                                </p>    
                            </div>
                        </a> 
                    </li>
                </ul>
            </li>    
        </div>
    </div>

    <!--SEARCH BAR--> 
    <div class="row search-box">
        <div class="form-group">
            <input class="form-control search-bar input-sm" placeholder="Search for people/team" ng-model="customSelected" ng-change="searchUserAndGroup(customSelected)" typeahead="tag as tag.name for tag in UserAndGroupTag" typeahead-on-select="selectMatch(customSelected)" typeahead-min-length="1" typeahead-wait-ms="500" style="padding-right:25px;">
            <span class="fa fa-search search-icon"></span>
        </div> 
    </div>

    <!--TEAMS--> 
    <div class="row teams-list">
        <!--ADD TEAM BUTTON--> 
        <div class="col-sm-12 team-add-box padding-lr-0" ng-click="addTeam()">
            <div class="col-sm-11 padding-lr-0">
                <p class="btn-team-add text-bold col-xs-9" >  Add a team</p>
                <p class="col-xs-1 cursor-pointer text-center">
                    <span class="fa fa-plus-circle" style=""></span>
                </p>
            </div>

        </div>

        <!--single team-->
        <div class="col-sm-12 padding-lr-0 teams-list-cover">
            <div class="col-sm-11 padding-lr-0 list-item" ng-repeat="group in groups" ng-class="{'active text-bold':selectedGroup.id === group.Group.id}">
                <!--group name--> 
                <p class="col-sm-9 group_name"  ng-click="selectGroup(group.Group)" >
                    <span ng-bind="group.Group.name" ></span>
                </p>

                <!--group settings--> 
                <p class="col-sm-1"><span class="fa fa-gear" ng-click="editGroupModal(group)"></span></p>
                <!--clear-->
                <p ng-show="selectedGroup.id === group.Group.id" class="text-bold col-sm-1 padding-left-0" ng-click="clearFilter()"><span class="fa fa-times"></span></p>
            </div>
        </div>
        <span class="clearfix"></span>
        <p class="font-small-2 btn-load-more" style="cursor:pointer" ng-show="loadMoreGroup && LoadMoreCountGroup" ng-click="loadMoreGroup()">+<span ng-bind="LoadMoreCountGroup"></span>, load more</p>
    </div>

    <!--Users-->
    <span class="clearfix"></span>
    <div class="row users-list">

        <!--ADD PEOPLE BUTTON--> 
        <div class="col-sm-12 padding-lr-0 team-add-box" ng-click="inviteToNetwork()">
            <div class="col-sm-11 padding-lr-0">
                <p class="btn-team-add text-bold col-xs-9" >  Add People</p>
                <p class="col-xs-1 cursor-pointer text-center">
                    <span class="fa fa-plus-circle" style="margin-left: 2px;"></span>
                </p>
            </div>
        </div>
        <!--single user-->
        <div class="col-sm-12 padding-lr-0 users-list-cover">
            <div class="list-item col-sm-11 padding-lr-0" ng-repeat="user in users" ng-class="{'active text-bold':selectedUser.user_id == user.UserInfo.user_id}">
                <!--user name--> 
                <p class="col-xs-11 user_name" ng-click="selectUser(user.UserInfo)">
                    <span ng-show="user.user_id === loggeduser">Me</span>
                    <span ng-hide="user.user_id === loggeduser" ng-bind="user.UserInfo.name"></span>
                </p>

                <!--clear-->
                <p ng-show="selectedUser.user_id == user.UserInfo.user_id" class="col-xs-1 padding-left-0 text-bold" ng-click="clearFilter()">
                    <span class="fa fa-times"></span>
                </p>

            </div>
        </div>
        <span class="clearfix"></span>
        <p class="font-small-2 btn-load-more clearfix" style="cursor:pointer" ng-show="loadMoreUser && LoadMoreCountUser" ng-click="loadMoreUser()">+<span ng-bind="LoadMoreCountUser"></span>, load more</p>

    </div>

    <!--APPEAR CHAT--> 
    <div class="sidebar-shortcut-box cursor-pointer padding-tb-10" ng-click="launchAppearChat()">
        <!--        <div class="col-sm-4 padding-lr-0 shortcut-box">
                    
                </div>-->
        <div class="col-sm-10 col-sm-offset-1 padding-lr-0 shortcut-box text-center">
            <img src="/img/logos/vendor/appear.png" height="50" class="pull-left" /><p class="pull-left margin-left-5 text-bold padding-tb-5">Start a video<br> meeting</p>
        </div>
    </div>

    <!--ADD PEOPLE NOTIFICATION--> 

    <span ng-init="showThis = true" ng-show="user_role_network == 'admin' || user_role_network == 'owner'" ng-cloak>
        <div class="notification-box" ng-show="users.length < 1 && !tourOn && showThis" >
            <div class="ns-box ns-other ns-effect-thumbslider ns-type-notice ns-show">
                <div class="ns-box-inner">
                    <!--side tile--> 
                    <div class="ns-thumb">
                        <p class="ns-gridle text-center">
                            <img src="/img/logos/producitvity_ring.svg" height="35px" />
                        </p>
                    </div>
                    <div class="ns-content cursor-pointer">
                        <!--close icon--> 
                        <p class="pull-right icon icon2-arrows-remove font-big-3" style="padding-top:5px; margin-top: -8px; z-index: 2000" ng-click="showThis = false" ></p>
                        <p class="pull-left ns-msg" >
                            <span class="text-underline cursor-pointer" ng-click="inviteToNetwork()">Invite your colleagues.</span> Life is short.<br/>Work smart. Save time. <span class='text-underline text-italic cursor-pointer text-dim' ng-click="$root.tourOn=true;">Take a tour.</span>
                        </p>
                    </div>
                </div>

            </div>
        </div>
    </span>
    <!--        ADD PEOPLE NOTIFICATION ENDS  --> 

</div>

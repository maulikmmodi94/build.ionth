<!-- start Mixpanel -->
<script type="text/javascript">(function(f, b) {
        if (!b.__SV) {
            var a, e, i, g;
            window.mixpanel = b;
            b._i = [];
            b.init = function(a, e, d) {
                function f(b, h) {
                    var a = h.split(".");
                    2 == a.length && (b = b[a[0]], h = a[1]);
                    b[h] = function() {
                        b.push([h].concat(Array.prototype.slice.call(arguments, 0)))
                    }
                }
                var c = b;
                "undefined" !== typeof d ? c = b[d] = [] : d = "mixpanel";
                c.people = c.people || [];
                c.toString = function(b) {
                    var a = "mixpanel";
                    "mixpanel" !== d && (a += "." + d);
                    b || (a += " (stub)");
                    return a
                };
                c.people.toString = function() {
                    return c.toString(1) + ".people (stub)"
                };
                i = "disable track track_pageview track_links track_forms register register_once alias unregister identify name_tag set_config people.set people.set_once people.increment people.append people.track_charge people.clear_charges people.delete_user".split(" ");
                for (g = 0; g < i.length; g++)
                    f(c, i[g]);
                b._i.push([a, e, d])
            };
            b.__SV = 1.2;
            a = f.createElement("script");
            a.type = "text/javascript";
            a.async = !0;
            a.src = "//cdn.mxpnl.com/libs/mixpanel-2.2.min.js";
            e = f.getElementsByTagName("script")[0];
            e.parentNode.insertBefore(a, e)
        }
    })(document, window.mixpanel || []);
    mixpanel.init("d8cc83ecd1ff3b2689d9203093f8910a");</script><!-- end Mixpanel -->
<!-- Begin Inspectlet Embed Code -->
<script type="text/javascript" id="inspectletjs">
    window.__insp = window.__insp || [];
    __insp.push(['wid', 1645017761]);
    (function() {
        function __ldinsp() {
            var insp = document.createElement('script');
            insp.type = 'text/javascript';
            insp.async = true;
            insp.id = "inspsync";
            insp.src = ('https:' == document.location.protocol ? 'https' : 'http') + '://cdn.inspectlet.com/inspectlet.js';
            var x = document.getElementsByTagName('script')[0];
            x.parentNode.insertBefore(insp, x);
        }
        if (window.attachEvent) {
            window.attachEvent('onload', __ldinsp);
        } else {
            window.addEventListener('load', __ldinsp, false);
        }
    })();
</script>
<!-- End Inspectlet Embed Code -->
<!--        Intercom    settings            -->
<script id="IntercomSettingsScriptTag">
    window.intercomSettings = {
        // TODO: The current logged in user's full name
        name: "John Doe",
        // TODO: The current logged in user's email address.
        email: "john.doe@example.com",
        // TODO: The current logged in user's sign-up date as a Unix timestamp.
        created_at: 1234567890,
        app_id: "eded34726767b7a63a8bb8a6c8be178c9bd5bf24"
    };
</script>
<script>(function() {
        var w = window;
        var ic = w.Intercom;
        if (typeof ic === "function") {
            ic('reattach_activator');
            ic('update', intercomSettings);
        } else {
            var d = document;
            var i = function() {
                i.c(arguments)
            };
            i.q = [];
            i.c = function(args) {
                i.q.push(args)
            };
            w.Intercom = i;
            function l() {
                var s = d.createElement('script');
                s.type = 'text/javascript';
                s.async = true;
                s.src = 'https://static.intercomcdn.com/intercom.v1.js';
                var x = d.getElementsByTagName('script')[0];
                x.parentNode.insertBefore(s, x);
            }
            if (w.attachEvent) {
                w.attachEvent('onload', l);
            } else {
                w.addEventListener('load', l, false);
            }
        }
    })()</script>
<!--        Intercom    ends            -->

<?php
//echo $this->Html->script('Doc/dropzone.min');
?>
<script type="text/javascript" src="https://www.dropbox.com/static/api/2/dropins.js" id="dropboxjs" data-app-key="tlsnbsp5n03bwru"></script>

<!--    UPLOAD FILE MODAL WINDOW        -->
<div id='fileUploadModal' class="modal fade">
    <div class="g-modal-large g-modal modal-dialog">
        <!--    CONTENT         -->
        <div class="g-modal-content modal-content ">
            <!--        HEADER             -->
            <div class="g-modal-header modal-header">
                <!-- <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button> -->
                <h4 id="" class="g-modal-title modal-title">
                    <span class="g-modal-header-icon fa fa-upload"></span>
                    <span class="g-modal-header-title-text" >Upload File</span>
                </h4>   
            </div>
            <!--        BODY                 -->
            <div class="g-modal-body modal-body">
                <div class="container-fluid">
                    <!--Navigation--> 
                    <div class="row">
                        <p>Upload from computer  | Upload from dropbox</p>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <form class="form-file-upload dropzone row" id="my-awesome-dropzone" enctype="multipart/form-data">
                                <!--<div class="dropzone"></div>-->
                                <div class="dropzone-previews"></div>
                            </form>
                            <!--Tags Input-->
                            <div class="row" id="fileUploadShareInput">
                                <tags-input ng-model="tags" add-from-autocomplete-only="true"  display-property="name" min-length="1" placeholder="Share with people...">
                                    <auto-complete source="fileTagsAutocompleteLoad()"></auto-complete>
                                </tags-input>
                            </div>
                            <!--Tags Input End-->
                            <div class="row">
                                <button id="fileUploadBtn" class="btn btn-success col-lg-4 col-lg-offset-4" data-loading-text="Uploading...">Upload</button>
                            </div>
                            <div id="fileUploadShareDisplay" hidden>
                                <h4>File Shared With : </h4>
                                <span ng-repeat="tag in tags" ng-bind="tag.name"></span>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <button class="btn btn-green" ng-click="DropboxUpload()">Upload from Dropbox</button>
                            <div class="thumbnails"  ng-if="newFile">
                                <h3 ng-bind="newFile.name"></h3>
                                <small ng-bind="newFile.bytes"></small>
                                <input type="hidden" ng-model="newFile.url" ng-value="newFile.link">
                                <img class="img-circle" ng-if="newFile.thumbnailLink" ng-src="{{newFile.thumbnailLink}}">
                                <img class="img-circle" ng-if="!newFile.thumbnailLink" ng-src="{{newFile.icon}}">
                                <tags-input ng-model="tags" add-from-autocomplete-only="true"  display-property="name" min-length="1" placeholder="Share with people...">
                                    <auto-complete source="fileTagsAutocompleteLoad()"></auto-complete>
                                </tags-input>
                                <button class="btn btn-info" ng-click="Upload()">Upload</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- /.modal-dialog -->
            <div class="modal-footer gridle-modal-footer">
                <button type="button" class="btn btn-default pull-right btn-sm" onclick="fileModalUploadClose();">Close</button>

            </div>
        </div>
    </div>
</div>
<!--/* The MIT License */-->
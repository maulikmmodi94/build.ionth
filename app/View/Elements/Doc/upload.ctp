
<script type="text/javascript" src="https://www.dropbox.com/static/api/2/dropins.js" id="dropboxjs" data-app-key="tlsnbsp5n03bwru"></script>

<!--    UPLOAD FILE MODAL WINDOW        -->
<div id='fileUploadModal' class="modal fade">
    <div class="g-modal-large g-modal modal-dialog">
        <!--    CONTENT         -->
        <div class="g-modal-content modal-content ">
            <!--        HEADER             -->
            <div class="g-modal-header modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="fileModalUploadClose();">&times;</button> 
                <h4 id="" class="g-modal-title modal-title">
                    <span class="g-modal-header-icon fa fa-upload"></span>
                    <span class="g-modal-header-title-text" >Upload and share files with your team.</span>
                </h4>   
            </div>
            <!--        BODY                 -->
            <div class="g-modal-body modal-body">
                <div class="container-fluid padding-lr-0" ng-init="uploadFrom = 'computer'">
                    <div class="row" >
                        <!--Preview area--> 
                        <div class="upload-computer">
                            
                            <!--Computer upload preview area--> 
                            <form id="my-awesome-dropzone"  class="form-file-upload dropzone"  enctype="multipart/form-data" ng-hide="newFile" >
                                <div id="dropzone_backdrop" style="">
                                    <p class="text-center text-muted " style="font-size: 15em;line-height: 0;"><span class="fa fa-cloud-upload"></span></p>
                                    <p class="text-center text-muted font-big-1">Drag and drop files here or simply click to upload file.</p>
                                </div>
                                <div class="fallback">
                                    <input name="file" type="file" multiple />
                                </div>
                            </form>

                            <!-- Dropbox file preview --> 
                            <div class="dropbox-preview-zone" ng-hide="uploadFrom !== 'dropbox'" ng-if="newFile">
                                <div class="thumbnail col-sm-8 col-sm-offset-2 text-center">
                                    <input type="hidden" ng-model="newFile.url" ng-value="newFile.link">
                                    <img class="img-circle" ng-if="newFile.thumbnailLink" ng-src="{{newFile.thumbnailLink}}">
                                    <img class="img-circle" ng-if="!newFile.thumbnailLink" ng-src="{{newFile.icon}}">
                                    <h3 class="text-emphasize" ng-bind="newFile.name"></h3>
                                </div>
                            </div>
                        </div>
                        <!--Upload action buttons-->
                        <div class="upload-action-buttons">
                            <!--Browse button--> 
                            <div class="upload-action-button-cover col-sm-offset-2 col-sm-4 padding-lr-0" ng-click="uploadFrom = 'computer'" ng-disabled="newFile">
                                <button class="btn btn-sm btn-block btn-default content_highlight"  onclick="$('#my-awesome-dropzone').trigger('click');" >Upload from computer </button>
                            </div>
                            <!--Dropox chooser--> 
                            <div class="upload-action-button-cover col-sm-4 padding-lr-0">
                                <button class="btn btn-sm btn-block btn-default content_highlight" ng-click="DropboxUpload();
                                    uploadFrom = 'dropbox'" ><span class="fa fa-dropbox"></span> Dropbox</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- /.modal-dialog -->
            <div class="modal-footer gridle-modal-footer g-background-offwhite">

                <div class="row upload-share-area text-left">
                    <!--Form upload from computer-->
                    <div class="col-sm-12" ng-hide="uploadFrom !== 'computer'">
                        <div class="col-sm-8" id="fileUploadShareInput">
                            <tags-input ng-model="tags" add-from-autocomplete-only="true"  display-property="name" min-length="1" placeholder="Share with people...">
                                <auto-complete source="fileTagsAutocompleteLoad()"></auto-complete>
                            </tags-input>
                        </div>
                        <div class="col-sm-4">
                            <button id="fileUploadBtn" class="btn btn-success btn-block" data-loading-text="Uploading...">Upload</button>
                        </div>
                    </div>

                    <!--Form upload from dropbox --> 
                    <div class="col-sm-12" ng-hide="uploadFrom !== 'dropbox'">
                        <!--share for dropbox-->
                        <div class="col-lg-8">
                            <tags-input ng-model="tags" add-from-autocomplete-only="true"  display-property="name" min-length="1" placeholder="Share with people...">
                                <auto-complete source="fileTagsAutocompleteLoad()"></auto-complete>
                            </tags-input>
                        </div>
                        <!--UPload dropbox--> 
                        <div class="col-sm-4">
                            <button class="btn btn-success btn-block" ng-click="Upload()">Upload</button>
                        </div>
                    </div>
                </div>  
            </div>
        </div>
    </div>
</div>
<!--/* The MIT License */-->
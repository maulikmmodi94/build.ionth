<!--    EDIT FILE MODAL WINDOW         -->
<div id='fileManageModal' class="modal fade" ng-if="fileCurrentIndex != null">
    <div class="g-modal modal-dialog">
        <!--    CONTENT         -->
        <div class="g-modal-content modal-content">
            <!--    HEADER        -->
            <div class="g-modal-header modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="g-modal-title modal-title">
                    <span class="g-modal-header-icon fa fa-file" ></span>          
                    <span class="g-modal-header-title-text text-center">File settings</span>
                </h4>
            </div>
            <!--    BODY        -->
            <div class="g-modal-body modal-body">
                <p class="modalnotify"></p>
                <div class='g-doc-modal-heading text-muted text-center font-big-1' ng-init='editMode=false;'>
                    <p class='g-doc-modal-heading-cover-icon text-center font-big-2 p-margin-0'  ng-switch="fileType(files[fileCurrentIndex]['Doc']['file_mime'])">
                        <span class="g-doc-modal-heading-icon fa fa-file-image-o" ng-switch-when="image"></span>
                        <span class="g-doc-modal-heading-icon fa fa-file-audio-o" ng-switch-when="audio"></span>
                        <span class="g-doc-modal-heading-icon fa fa-file-movie-o" ng-switch-when="video"></span>
                        <span class="g-doc-modal-heading-icon fa fa-file-text-o" ng-switch-when="text"></span>
                        <span class="g-doc-modal-heading-icon fa fa-file" ng-switch-when="other"></span>
                    </p>
                    <p class='g-doc-modal-heading-name p-margin-0 text-muted font-big-1'><span ng-show='!editMode'>{{files[fileCurrentIndex]['Doc']['name']}}.{{files[fileCurrentIndex].Doc.file_extension}}</span><span class="" ng-show="!editMode && files[fileCurrentIndex]['Doc']['user_id'] == loggeduser" ng-click="oldName = files[fileCurrentIndex].Doc.name;editMode = !editMode;" title="Rename the document" tooltip  > <i class="fa fa-pencil"></i></span></p>
                    <span ng-show="(files[fileCurrentIndex]['Doc']['user_id'] == loggeduser) && editMode" >
                        <input class="g-doc-modal-heading-name" ng-model="files[fileCurrentIndex].Doc.name" ng-disabled="!editMode" ng-pattern="/^[\w\d ]+/" ng-class="{'form-control': editMode}" ng-show='editMode' >                        
                        <!--<p>{{files[fileCurrentIndex].Doc.file_extension}}</p>-->
                        <button class='btn btn-sm btn-default' ng-show="editMode" ng-click='editMode = !editMode;files[fileCurrentIndex].Doc.name = oldName'>Cancel</button>
                        <button class="btn btn-primary btn-sm"  ng-show="editMode" ng-click="editMode = !editMode;fileEditBtn(files[fileCurrentIndex].Doc.id, files[fileCurrentIndex].Doc.name, oldName)">Save</button>
                    </span>
                </div>
                <!--    Sharing with member form       -->
                <div id="admemberinput" class="g-share-doc-box">
                    <form class="" ng-submit="filesAddMembers(files[fileCurrentIndex].Doc.id, data)">
                        <!--Tags Input-->
                        <div>
                            <tags-input class="" ng-model="tags" add-from-autocomplete-only="true"  display-property="name" min-length="1" placeholder="Share with people">
                                <auto-complete source="fileTagsAutocompleteLoad(files[fileCurrentIndex].Doc.id)"></auto-complete>
                            </tags-input>
                        </div>                        
                        <!--Tags Input End-->
                        <p id="addmembererrors" class="colorred p-margin-0"></p>
                        <button type="submit" class="btn btn-success btn-sm col-lg-6 col-lg-offset-3" id="addmemberbtn" ng-click="data.userlist = tags"  data-loading-text="Adding..." style="margin-top: 10px;">Share</button>
                    </form>
                </div>
                <div class="clearfix"></div>
                <!--    SHARED USER INFORMATION     -->
                <div class="g-doc-user-list g-scrollbar">
                    <p class="text-muted p-margin-0">Shared with</p>
                    <div class="g-doc-user-box" ng-repeat="user in files[fileCurrentIndex]['DocAssignment']" >
                        <!--    if user     -->
                        <div class="g-doc-user-info" ng-if="user.user_id != null" ng-class="{'g-doc-user-admin': files[fileCurrentIndex]['Doc']['user_id'] === user.user_id }">
                            <p class="g-doc-user-name p-margin-0">
                            <g-profile-img type="user" name="{{users[findUser(user.user_id)]['UserInfo']['name']}}" path="{{users[findUser(user.user_id)]['UserInfo']['vga']}}" myclass='g-doc-user-img' width='25px' height='25px' ></g-profile-img>
                                <span class="g-doc-user-name-text" >{{users[findUser(user.user_id)]['UserInfo']['name']}}</span>
                            </p>

                            <p class="g-btn-doc-unshare text-warning pull-right p-margin-0" ng-if="files[fileCurrentIndex]['Doc']['user_id'] != user.user_id" ng-click="filesRemoveMember(files[fileCurrentIndex]['Doc']['id'], 'user', user.user_id)"><i class="fa fa-minus-circle"></i> Unshare</p>
                            <!--<div class="clearfix"></div>-->
                        </div>
                        <!--    if group     -->
                        <div class="g-doc-user-info" ng-if="user.group_id != null">
                            <p class="g-doc-user-name p-margin-0">
                            <g-profile-img type='group' name="{{groups[findGroup(user.group_id)]['Group']['name']}}" myclass='g-doc-grp-default-img' width="25px" height="25px" ></g-profile-img>
                                <!--<span class="g-doc-grp-default-img">G</span>-->
                                <span class="g-doc-user-name-text">{{groups[findGroup(user.group_id)]['Group']['name']}}</span>
                            </p>
                            <p class="g-btn-doc-unshare text-warning pull-right p-margin-0" ng-click="fileModalMemberRemove(files[fileCurrentIndex]['Doc']['name'], files[fileCurrentIndex]['Doc']['id'], 'group', user.group_id, groups[findGroup(user.group_id)]['Group']['name'])"><i class="fa fa-minus-circle"></i> Unshare</p>
                            <div class="clearfix"></div>
<!--                            <button class="btn removeuser" ng-click="filesRemoveMember(files[fileCurrentIndex]['Doc']['id'], 'group', user.group_id)"><i class="fa fa-times"></i></button>-->
                        </div>                        
                    </div>
                </div>
            </div>    
            <!--    FOOTER        -->
            <div class="modal-footer gridle-modal-footer" >
                <button class="btn btn-sm btn-default"  href='' ng-if="files[fileCurrentIndex]['Doc']['user_id'] == loggeduser" ng-click="fileModalDelete(files[fileCurrentIndex]['Doc']['id'], files[fileCurrentIndex]['Doc']['name'])"> 
                    <span class="fa fa-trash-o">
                    </span> 
                    Delete File
                </button>
            </div>
        </div>
    </div>
</div>

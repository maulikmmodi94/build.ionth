        <!-- FOOTER-->
        <div id="gridle_footer" class="container-fluid" >
            <div class="row">
                <div class="footer-section col-lg-3 col-md-3 col-xs-6">
                    <p class="footer-section-heading">
                        GRIDLE MORE
                    </p>
                    <p>
                        <a class="footer-section-links" href="/pages/features">Features</a><br/>
                        <a class="footer-section-links" href="/pages/benefits">Benefits</a><br/>
                        <!--<a class="footer-section-links" href="">Clients</a><br/>-->
                        <!--<a class="footer-section-links" href="">Investors</a><br/>-->
                        <a class="footer-section-links" href="/pages/press">Press</a><br/>
                    </p>
                    <p class="footer-section-heading" style="margin-top: 20px;">
                        GRIDLE FOR EDUCATORS
                    </p>
                    <p>
                        <a class="footer-section-links" href="/pages/education">Education</a><br/>
                    </p>
                </div>
                <div class="footer-section col-lg-3 col-xs-6 col-md-3">
                    <p class="footer-section-heading">
                        GETTING STARTED
                    </p>
                    <p>
                        <a class="footer-section-links" href="/pages/pricing">Pricing</a><br/>
                        <!--<a class="footer-section-links" href="">Tutorial</a><br/>-->
                        <a class="footer-section-links" href="/pages/terms">Policies</a><br/>
                        <a class="footer-section-links" href="/pages/case-studies">Case studies</a><br/>
                    </p>
                </div>
                <div class="footer-section col-lg-3 col-md-3 col-xs-6">
                    <p class="footer-section-heading">
                        RESOURCES & SUPPORT
                    </p>
                    <p>
                        <a class="footer-section-links" href="/pages/support">Support</a><br/>
                        <!--<a class="footer-section-links" href="">Resources</a><br/>-->
                        <!--<a class="footer-section-links" href="">White-paper</a><br/>-->
                    </p>
                </div>
                <div class="footer-section col-lg-3 col-md-3 col-xs-6">
                    <p class="footer-section-heading">
                        CONTACT & INFO
                    </p>
                    <p>
                        <a class="footer-section-links" href="https://medium.com/@gridle" target="_blank">Blog</a><br/>
                        <a class="footer-section-links" href="/pages/about">About</a><br/>
                        <a class="footer-section-links" href="/pages/contact">Contact</a><br/>
                        <a class="footer-section-links" href="https://www.facebook.com/gridle.in" target='_blank'>Facebook</a><br/>
                        <a class="footer-section-links" href="https://twitter.com/gridle_io" target='_blank'>Twitter</a>
                    </p>
                </div>

            </div>
            <div id="copyright_sec" class="row">
                <p id="copyright_brand" class="pull-right">Copyright ©  2014,Pivoting Softwares Pvt. Ltd.</p>
            </div>

        </div>
        <div id="copyright_logos" >
            <p class="">
                <a href="http://global.sap.com/corporate-en/innovation/start-up-focus-program/index.epx" target="blank">
                    <img class="investor_logo" src="/img/investors/sap.png" alt="gridle, SAP startup focus member" height="40" alt="sap" />
                </a>
                <a href="http://hiracoventures.com" target="blank">
                    <img class="investor_logo" src="/img/investors/hiracoventures.png" alt="gridle investor Hiraco Ventures" height="40" alt="hiracoventures"/></a>
                <a href="http://ciieindia.org/" target="blank">
                    <img class="investor_logo" src="/img/investors/ciie_logo.png" alt="gridle investor CIIE" height="40" style="margin:0 30px;" alt="ciie"/>
                </a>
                <a href="http://www.microsoft.com/bizspark/" target="blank">
                    <img class="investor_logo" src="/img/investors/microsoft_bizspark.jpg" alt="gridle Microsoft Bizspark plus member" height="40" alt="bizspark" />
                </a>
            </p>
        </div>

        <!--        FOOTER ENDS             -->
<nav class="navbar g-navbar " role="navigation" ng-controller="DropdownCtrl"  style="width:100%">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">

        <button type="button" class="navbar-toggle" ng-init="navCollapsed = true" ng-click="navCollapsed = !navCollapsed">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand visible-xs" href="#"><img src="/img/logos/logo_blue.svg"/></a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" ng-class="!navCollapsed && 'in'">


        <img class="col-sm-offset-0" src="/img/logos/logo_blue.svg" height="30px" />

        <ul class="nav navbar-nav navbar-right">
            <li class="dropdown" dropdown on-toggle="toggled(open)" ng-controller="userCtrl">
                <a href="#" class="dropdown-toggle" dropdown-toggle>
                    <img class="round-30" src="/img/demo/anupama.png" />&nbsp;&nbsp;<b class="fa fa-caret-down"></b>
                </a>
                <ul class="dropdown-menu" role="menu">
                    <li ng-click="OpenProfileSettings()"><a href="#"><span class="fa fa-gear"></span> Settings &amp; Profile</a></li>
                    <li><a href="#"><span class="fa fa-power-off"></span> Logout</a></li>
                </ul>
            </li>
        </ul>
    </div><!-- /.navbar-collapse -->
</nav>

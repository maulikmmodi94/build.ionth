<div class="notificationBar padding-lr-0 padding-tb-5" ng-show="showNotifications" ng-controller="notificationController">
    <!--NOTIFICATION HEAD--> 
    <div class="notificationHead">
        <p class="text-bold">
            <span class="fa fa-bell-o"></span> &nbsp;&nbsp;Notifications&nbsp;&nbsp;&nbsp;&nbsp;
                <a class="btn-link font-small-1 " ng-click="markAllNotificationAsRead()">
                    Mark All As Read
                </a>
            <span class="pull-right" ng-click="toggleNotifications()" style="cursor: pointer">&times;</span>
        </p>
    </div>
    <!--NOTIFICATIONS LIST-->
    <div class="container-fluid notifications-list">
        <!--single notification--> 
        <div class="row notification-item" ng-class="{'notificationSeen':notification.isSeen == true, 'notificationNotSeen':notification.isSeen == false}" ng-repeat="notification in notifications">
            <div ng-click="openNotificationmodal(notification)">
                <!--icon--> 
                <div class="col-xs-1 text-center notification-msg-icon" ng-switch on="notification.type">

                    <!--task icon--> 
                    <p class="" ng-switch-when="task">
                        <span class="icon" 
                              ng-class="{
                                    taskcomplete:'icon2-arrows-square-check',
                                    taskincomplete:'icon2-arrows-check',
                                    edit:'icon2-arrows-hamburger1',
                                    addmember:'icon2-arrows-square-plus',
                                    addcomment:'icon-basic-message'
                                    }[notification.activity]"
                              ></span>
                    </p>
                    <!--file icon--> 
                    <p class="" ng-switch-when="file">
                        <span class="icon" 
                              ng-class="{
                                          edit:'icon-basic-sheet-pencil',
                                          addmember:'icon2-arrows-square-plus',
                                          addcomment:'icon-basic-message'
                                          }[notification.activity]"
                              ></span>
                    </p>
                    <!--team icon--> 
                    <p class="" ng-switch-when="group">
                        <span class="icon" 
                              ng-class="{
                                          edit:'icon-basic-gear',
                                          addmember:'icon2-arrows-square-plus',
                                          }[notification.activity]"
                              ></span>
                    </p>
                    <!--network icon--> 
                    <p class="" ng-switch-when="network">
                        <span class="icon icon-basic-world"></span>
                    </p>
                </div>
                <!--message--> 
                <div class="col-sm-11 padding-lr-0 notification-msg-text">
                    <p class="color-black-alpha">
                        <span ng-bind-html="notification.message"></span>
                    </p>
                </div>
            </div>
        </div>
        <p class="text-center btn-link font-small-1 padding-tb-10" ng-click="loadMoreNotifications()" ng-show="loadMoreNotification" >Load more notifications</p>
        <!--no notifications--> 
        <div class="row" ng-show="notifications.length == 0">
            <div class="col-sm-12 padding-tb-20 text-center font-big-2">
                <p class="text-muted" >You don't have any notifications</p>
            </div>
        </div>
    </div>
</div>
<style>
    .notifications-list{
        background-color: white;
        padding-bottom: 50px;
    }
</style>

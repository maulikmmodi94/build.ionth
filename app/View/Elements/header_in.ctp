<!-- HEADER -->
<?php

function select_header() {
    switch ($_SERVER['REQUEST_URI']) {
        case '/networks' : return 'focused';
        case '/pages/new_network' : return 'focused';
        case '/networks/add' : return 'no_links';
        case '/networks/add_members': return 'no_links';
        case '/pages/activate': return 'focused';
    };
}
?>
<header id="g-header" class="navbar navbar-default navbar-fixed-top one-edge-shadow container-fluid" role="navigation" ng-controller="navigationController">
    <div class="g-header-wrap">

        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#g-header-right" style="z-index:1;">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>


        <!--    gridle logo -->
        <p class="g-logo-gridle-cover p-margin-0">
            <a id="g-logo-gridle" href="/networks">
                <object data="/img/logos/gridle_logo.svg" type="image/svg+xml">
                    <img src="/img/logos/gridle_logo.png" alt="gridle logo" height="50px" />
                </object>
            </a>
        </p>
        <?php if (select_header() !== 'no_links') { ?>
            <!--  left navigation  -->
            <div id="g-header-left" class="pull-left">
                <!--    All networks menu       -->
                <ul class="nav navbar-nav">

                    <li id="dropdown-network" class="g-header-dropdown dropdown text-muted" ng-if="networks[currentnetwork].name" >
                        <a id="network-logo" href='#' class='dropdown-toggle g-header-dropdown-btn' data-toggle="dropdown" style="">
                            <g-profile-img ng-if='networks' type='network' name='{{networks[currentnetwork].name}}' myclass='header-network-img font-small-1' ></g-profile-img> 
                            <span ng-bind='networks[currentnetwork].name' class='network-name-c'></span>&nbsp;&nbsp;
                            <span class="caret"></span>
                        </a>
                        <ul id="network-dropdown" class='dropdown-menu text-muted' role='menu'>
                            <li class='dropdown-header text-capitalize'>
                                <h4 class="text-warning text-center" ng-bind-template="{{networks[currentnetwork].name}} Network"></h4>
                            </li>
                            <!--    DASHBOARD        -->
                            <li id="network-dashboard-menu">
                                <a href="/networks/view/{{networks[currentnetwork].url}}"><span class='fa fa-compass'></span> Dashboard</a>
                            </li>
                            <li ng-show="networks[currentnetwork].role == 'owner' || networks[currentnetwork].role == 'admin'">
                                <a data-toggle="modal" data-target="#networkModalAddMembers" ><span class='fa fa-user'></span>  Invite People</a>
                            </li>


                            <!--    ACTIVITY LOG       -->
                            <li><a href='/logs'><span class='fa fa-history'></span> Activity Log</a></li>

                            <li class='divider'>
                                <!--    See all networks    -->
                            <li class='bg-warning'><a href='/networks' ><span class='fa fa-globe'></span> All networks</a></li>
                        </ul>
                    </li>
                    <!--    NOTIFICATION        -->
                    <?php if (select_header() !== 'focused') { ?>
                        <li class='header-in-nav-link g-header-dropdown dropdown' ng-init='getNotificationUnseenCount();
                                        getNotificationsData(true)'>
                            <a href='#' ng-click="notificationInit();" class='g-header-dropdown-btn dropdown-toggle text-capitalize <?php if (strpos($_SERVER['REQUEST_URI'], '/notifications') !== false) echo 'active text-regular'; ?>' data-toggle="dropdown" data-placement="bottom" title="notifications" ><span class="fa fa-bell-o" ></span> <span class="badge badge-danger notification-badge" ng-if="notificationsUnseenCount > 0" ng-bind="notificationsUnseenCount"></span></a>
                            <ul class='notifications-dropdown dropdown-menu' notification-drop-down role='menu'>
                            </ul>
                        </li>
                    <?php } ?>
                </ul>
            </div>

            <!--    right navigation -->
            <div id="g-header-right" class="collapse navbar-collapse pull-right">
                <ul class="nav navbar-nav" ng-controller="headerController">
                    <?php if (select_header() !== 'focused') { ?>
                        <!--    DESK        -->
                        <li class="header-in-nav-link"><a href="/desks" class="<?php if (strpos($_SERVER['REQUEST_URI'], '/desks') !== false) echo 'active text-regular'; ?>" >Desk</a></li>
                        <!--    FEED        -->
                        <?php if (Configure::read('enterprise_feeds') === true) { ?>
                            <li class="header-in-nav-link"><a href="/feeds" class="<?php if (strpos($_SERVER['REQUEST_URI'], '/feeds') !== false) echo 'active text-regular'; ?>" >Feed</a></li>
        <?php } ?>
                        <!--    PLANNER        -->
                        <li class="header-in-nav-link" ><a href="/tasks" class="<?php if (strpos($_SERVER['REQUEST_URI'], '/tasks') !== false) echo 'active text-regular'; ?>">Planner</a></li>
                        <!--    DOCUMENTS        -->
                        <li class="header-in-nav-link" ><a href="/docs" class="<?php if (strpos($_SERVER['REQUEST_URI'], '/docs') !== false) echo 'active text-regular'; ?>">Files</a></li>
                        <!--    CONVERSATIONS        -->
                        <li class="header-in-nav-link" ><a href="/conversations" class="<?php if (strpos($_SERVER['REQUEST_URI'], '/conversations') !== false) echo 'active'; ?>" ng-init='getConversationUnreadCount()'>Chat <span class="badge badge-danger notification-badge" ng-if="convUnreadCount" ng-bind="convUnreadCount"></span></a></li>
                    <?PHP } ?>
                    <!--    USER IMAGE       -->
                    <li class='g-header-dropdown dropdown'>
                        <a href='#' class='g-header-dropdown-btn dropdown-toggle text-capitalize' data-toggle="dropdown">
                            <g-profile-img type='user' path="<?= $AuthUser['UserInfo']['vga']; ?>" height="20px" width="20px" myclass="header-user-img" name="<?= $AuthUser['UserInfo']['name']; ?>" ></g-profile-img>&nbsp;&nbsp; <span class="caret"></span> </a>
                        <ul class='dropdown-menu-right dropdown-menu' role='menu'>
                            <li class='dropdown-header text-capitalize'>
                                <h4 class="text-muted"><?= $AuthUser['UserInfo']['name']; ?>&nbsp;&nbsp;
                                    <g-profile-img type='user' path="<?= $AuthUser['UserInfo']['vga']; ?>" height="25px" width="25px" myclass="header-user-img" name="<?= $AuthUser['UserInfo']['name']; ?>" ></g-profile-img>                 
                                </h4>

                            </li>

                            <li class='divider'></li>
                            <!--    SETTINGS        -->
                            <li><a href='/users/settings'><span class='fa fa-gear'></span> Settings &amp; Profile</a></li>
                            <!--    LOG OUT        -->
                            <li><a href='/users/logout'><span class='fa fa-power-off'></span> Log out</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
<?php } //end of no_links header     ?>
    </div>
</header>

<?php echo $this->element('Network/adduser', array('return_to' => Router::url($this->here, false))); ?>

<!--Conversation Manage Modal-->
<div id="convModalManage" class="modal fade"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
    <div class="g-modal modal-dialog">
        <div class="g-modal-content modal-content">
            <!--Header--> 
            <div id="" class="g-modal-header modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="g-modal-title modal-title">
                    <span class="g-modal-header-icon fa fa-gear"></span>&nbsp;&nbsp;
                    <span class="g-modal-header-title-text">Add members</span>
                </h4>
            </div>
            <!--    BODY    -->
            <div class="g-modal-body modal-body">
                <div ng-if="convSelectedItem['Conversation']['user_id'] === loggeduser">
                    <form class="container-fluid" id="" role="form" ng-submit="convMemberAdd(convSelectedItem['Conversation']['id'], convData)">
                        <div class="form-group">
                            <div class="">                                
                                <div class="message-a">
                                    <p class="">Add members</p>
                                    <tags-input ng-model="tags" add-from-autocomplete-only="true"  display-property="name" min-length="1" placeholder="Invite others">
                                        <auto-complete source="convTagsAutocompleteLoad(convSelectedItem['Conversation']['id'])"></auto-complete>
                                    </tags-input>
                                    <div class="">
                                        <button type="submit" class="convMemberAddbtn btn btn-success btn-sm pull-right" ng-click="convData.userlist = tags;" data-loading-text="Adding...">Add Members</button>
                                    </div>
                                </div>
                            </div>
                        </div>                        
                    </form>
                </div>
                <!--Conversation's Users-->
                <div class="g-chat-user-list">
                    <p class="">Existing users</p>
                    <div class="g-chat-user-box" ng-repeat="user in convSelectedItem['UserConversation']" ng-class="(user.role==0 ? 'g-background-blue':'g - background - offwhite')" >
                        <p class="g-text-capitalcase">
                        <g-profile-img type="user" path="{{users[findUser(user.user_id)]['UserInfo']['vga']}}" name="{{users[findUser(user.user_id)]['UserInfo']['name']}}" width="25px" height="25px" myclass="g-chat-img-user"  ></g-profile-img>
                        <span ng-bind ="users[findUser(user.user_id)]['UserInfo']['name']"></span>
                        </p>
                        <p class="g-icon-user-remove btn btn-link pull-right" ng-if="convSelectedItem['Conversation']['user_id'] === loggeduser && user.user_id !== loggeduser" ng-click="convRemoveMember(convSelectedItem['Conversation']['id'], user.user_id)"><i class="fa fa-times-circle"></i></p>
                    </div>                    
                </div>
            </div>
        </div>  
    </div> 
</div>
<!--Conversation Manage Modal-->
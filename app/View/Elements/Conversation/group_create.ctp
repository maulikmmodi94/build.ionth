<!--CREATE CONVERSATION MODAL WINDOW-->
<div id="convModalCreateGroup" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog gridle-modal">
        <div class="modal-content gridle-modal-content">
            <!--Header--> 
            <div id="" class="modal-header gridle-modal-header g-background-darkgreen g-color-white">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title gridle-modal-title">
                    <span class="glyphicon glyphicon-comment gridle-modal-header-icon g-color-darkgreen"></span>&nbsp;&nbsp;
                    <span class="gridle-modal-header-title">New Message</span>
                </h4>
            </div>
            <!--    BODY    -->
            <div class="modal-body gridle-modal-body">
                <form ng-submit="groupReply(data, tags[0].value)" name="convFormCreate">
                    <div id="message_modal_body">
                        <div class="message-add-members">
                            Group : {{tags[0].name}}
                        </div>                        
                        <div class="message-body">
                            <p class="">Message</p>
                            <div class="form-group">
                                <textarea class="g-textarea-message-body form-control" placeholder="Write your message here" style=""  ng-model="data.body" maxlength="1000" required rows="3"></textarea>
                            </div>
                        </div>
                        <div class="row">
                            <button class="g-btn-create-conversation btn btn-success btn-sm col-lg-offset-4 col-lg-4" type="submit" ng-click="data.userlist = tags;">Send</button>
                        </div>
                    </div>
                </form>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


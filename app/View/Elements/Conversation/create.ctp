<!--CREATE CONVERSATION MODAL WINDOW-->
<div id="convModalCreate" class="modal fade">
    <div class="g-modal modal-dialog">
        <div class="g-modal-content modal-content">
            <!--Header--> 
            <div id="" class="g-modal-header modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="g-modal-title modal-title">
                    <span class="g-modal-header-icon fa fa-comment-o"></span>
                    <span class="g-modal-header-title-text">New Message</span>
                </h4>
            </div>
            <!--    BODY    -->
            <div class="g-modal-body modal-body">
                <form ng-submit="convCreate(data)" name="convFormCreate">
                    <div id="message_modal_body">
                        <div class="message-add-members">
                            <p class="">Add members</p>
                            <tags-input ng-model="tags" add-from-autocomplete-only="true"  display-property="name"  min-tags="1" placeholder="Add People..." allow-leftover-text="false">
                            <auto-complete source="convTagsAutocompleteLoad()"></auto-complete>
                        </tags-input>
                    </div>                        
                    <div class="message-body">
                        <p class="">Message</p>
                        <div class="form-group">
                            <textarea class="g-textarea-message-body form-control" placeholder="Write your message here" ng-model="data.body" maxlength="1000" required rows="3"></textarea>
                        </div>
                    </div>
                    <div class="row">
                        <button class="g-btn-create-conversation btn btn-success btn-sm col-lg-offset-4 col-lg-4" type="submit" ng-click="data.userlist = tags;" data-loading-text="Sending...">Send</button>
                    </div>
                </div>
            </form>
        </div>
    </div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->
</div><!-- /.modal -->


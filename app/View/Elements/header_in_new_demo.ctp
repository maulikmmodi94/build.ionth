<nav class="navbar g-navbar" role="navigation" ng-controller="DropdownCtrl">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">

        <button type="button" class="navbar-toggle" ng-init="navCollapsed = true" ng-click="navCollapsed = !navCollapsed">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand visible-xs" href="#"><img src="/img/logos/logo_blue.svg"/></a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" ng-class="!navCollapsed && 'in'">


        <img class="col-sm-offset-4" src="/img/logos/logo_blue.svg" height="30px" />

        <ul class="nav navbar-nav navbar-right">
            <li>
                <a href="/desks/index_demo"><span class="fa fa-home"></span> Desk</a>
            </li>
            <li>
                <a href="/docs/index_demo"><span class="fa fa-files-o"></span> Files</a>
            </li>
            <li class="dropdown" dropdown on-toggle="toggled(open)" ng-controller="userCtrl">
                <a href="#" class="dropdown-toggle" dropdown-toggle>
                    <img class="round-30" src="/img/demo/anupama.png" />&nbsp;&nbsp;<b class="fa fa-caret-down"></b>
                </a>
                <ul class="dropdown-menu" role="menu">
                    <li ng-click="OpenProfileSettings()"><a href="#"><span class="fa fa-gear"></span> Settings &amp; Profile</a></li>
                    <li><a href="#"><span class="fa fa-power-off"></span> Logout</a></li>
                </ul>
            </li>
            <li ng-click="toggleNotifications()">
                <a href=""><span class="fa fa-bell" ng-class="{showNotifications : 'color-yellow'}"></span></a>
            </li>
            <li>
                <a><span class="fa fa-bars"></span></a> 
            </li>
        </ul>
    </div><!-- /.navbar-collapse -->
</nav>

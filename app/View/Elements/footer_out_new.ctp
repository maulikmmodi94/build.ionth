<?php

function select_footer() {

    $getUrl = @$_SERVER['REQUEST_URI'];
    $checkUrl = 'reset_password';
    $pos = strpos($getUrl, $checkUrl);



    switch (@$_SERVER['REQUEST_URI']) {
        case '/': return 'white_footer';
        case '/pages/home': return 'white_footer';
        case '/pages/terms_of_use': return 'white_footer';
        default: return 'blue_footer';
    };
}
?>
<?php if (select_footer() == 'blue_footer') { ?>
    <div class="footer-light-box footer-down ">
        <p class="text-center footer-light">
            <a href="/pages/contact" class="footer-link" >Contact</a>
            <a href="https://twitter.com/ionthglobal" target="_blank" class="footer-link">Twitter</a>
            <a href="https://www.facebook.com/ionth" target="_blank" class="footer-link">Facebook</a>
            <a href="https://www.linkedin.com/company/ionth" target="_blank" class="footer-link" >Linkedin</a>
            <a href="/pages/terms_of_use" target="_blank" class="footer-link" >Terms of use</a>
        </p>
    </div>
<?php } ?>
<?php if (select_footer() == 'white_footer') { ?>
    <div class="container-fluid background-white">
        <div class="footer-light-box-notfixed footer-down ">
            <p class="text-center footer-light-blue">
                <a href="/pages/contact" class="footer-link" >Contact</a>
            <a href="https://twitter.com/ionthglobal" target="_blank" class="footer-link">Twitter</a>
            <a href="https://www.facebook.com/ionth" target="_blank" class="footer-link">Facebook</a>
            <a href="https://www.linkedin.com/company/ionth" target="_blank" class="footer-link" >Linkedin</a>
            <a href="/pages/terms_of_use" target="_blank" class="footer-link" >Terms of use</a>
            </p>
        </div>
    </div>
<?php } ?>
<?php if (select_footer() == 'white_footer_div') { ?>
    <div class="container-fluid background-white">

        <div class="footer-down">
            <p class="text-center footer-light-blue">
                <font color="#148eae">
                <a href="/pages/contact" class="footer-link" >Contact</a>
            <a href="https://twitter.com/ionthglobal" target="_blank" class="footer-link">Twitter</a>
            <a href="https://www.facebook.com/ionth" target="_blank" class="footer-link">Facebook</a>
            <a href="https://www.linkedin.com/company/ionth" target="_blank" class="footer-link" >Linkedin</a>
            <a href="/pages/terms_of_use" target="_blank" class="footer-link" >Terms of use</a>
                </font>
            </p>
        </div>

    </div>
<?php } ?>



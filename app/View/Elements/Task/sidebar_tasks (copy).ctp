<div class="sidebarTasks container-fluid" ng-controller="sideTasksCtrl" ng-hide="hideRightBar">
    <!--DROP DOWN--> 
    <div class="sidebarTasksHead row" ng-class="{'Pending':'pending - head', 'Completed':'completed - head'}[tasksType]">
        <li class="dropdown" dropdown>
            <p class="dropdown-toggle text-bold " dropdown-toggle>
                <span ng-bind="tasksType"></span>&nbsp;&nbsp;&nbsp;&nbsp; <span class="fa fa-caret-down"></span>
            </p>
            <ul class="dropdown-menu" role="menu">
                <li ng-show="tasksType !== 'Pending'" ng-click="changeTaskType('Pending')"><a href="#">Pending Tasks</a></li>
                <li ng-show="tasksType !== 'Completed'" ng-click="changeTaskType('Completed')"><a href="#">Completed Tasks</a></li>
            </ul>
        </li>
    </div>    
    <!--PENDING TASKS-->
    <div class="pending-tasks row" ng-show="tasksType == 'Pending'">
        <!--pending meta data--> 
        <div class="container-fluid pending-meta-box">
            <div class="row ">
                <div class="pending-meta col-sm-12 padding-lr-0">
                    <div class="col-sm-3">
                        <p class="color-red text-center font-big-5 text-bold"><span ng-bind="TaskData.Pending.count"></span></p>
                    </div>
                    <div class="col-sm-9">
                        <p class="text-bold">Tasks Pending</p>
                        <p class="text-muted font-small-2">Those tasks which have missed the deadlines.</p>
                    </div>
                </div>        
            </div>
        </div>
        <!--listing of tasks--> 
        <div class="container-fluid pending-tasks-list">
            <div class="tasks-list g-scrollbar">

                <div ng-repeat="task in TaskData.Pending| orderBy:'Task.end_date'" >


                    <div class="task-tile col-sm-12" ng-click="selectedTask($index)" ng-class="{ 'task-tile-active': $index == selectedIndex}">
                        <!--    task color  -->
                        <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 padding-lr-0 task-tile-color" ng-class="{'list-group-item-danger': task.Task.id === TaskSelected.Task.id, 'task-group':getTaskType(task.Tags) === 'group', 'task-me':getTaskType(task.Tags) === 'self', 'task-individual':getTaskType(task.Tags) === 'user', 'task-other':getTaskType(task.Tags) === 'many', 'task-missed' : is_deadline_missed(task.Task.end_date)}"></div>
                        <!--    task creator  -->
                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 padding-lr-0 task-tile-creator">
                            <g-profile-img type='user'  path="{{task.user_info.vga}}" name="{{task.user_info.name}}" myclass="task-tile-ppl-user center-block font-small-1" width="20px" height="20px" popover="{{task.user_info.name}}" popover-trigger="mouseenter"></g-profile-img>
                        </div>
                        <!--    task body  -->
                        <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7 padding-lr-0 task-tile-msg" ng-class="{'task-complete': task.Task.is_completed}">
                            <p class="" ng-class="{'task-completing': ($index === task_completing), 'task-uncompleting':($index === task_uncompleting)}" ><span ng-bind='task.Task.title'></span> <br/> <span class="text-muted fa fa-clock-o"></span> <span class="text-muted font-small-1 p-margin-0" ng-bind='task.Task.end_date' ></span></p>

                        </div>

                        <!--    Mark complete option  -->
                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 pull-right padding-lr-0 task-tile-complete">
                            <p class="text-right" >
                                <span class="task-important-mark text-danger font-small-1" ng-show="task.Task.priority" tooltip data-placement="left" title="Important  "><i class="fa fa-square"></i></span>
                                <span class='btn-mark-complete text-muted text-emphasize' ng-class="{'del': task.Task.is_completed}" ng-click="markTaskComplete(task.Task.id, task.Task.is_completed, 'pending');
                                        $event.stopPropagation();" >
                                    <span class="text-success font-big-1" ng-show="task.Task.is_completed"  ><span class="fa fa-check"></span></span>
                                    <span ng-show="!task.Task.is_completed" ng-class="{'text-success': $index === task_completing}"  ><span class="fa fa-check"></span></span>
                                </span>
                            </p>
                        </div>
                        <div class="clearfix"></div>

                        <div class="clearfix"></div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding-lr-0 task-tile-bottom">
                            <!-- stats of task  -->
                            <div class="col-xs-9 padding-lr-0 text-muted">
                                <!--shared with--> 
                                <li ng-if="task.Tags.length > 1" class="pull-left btn-on-task" ng-click="taskSelectItem(task.Task.id);" data-toggle="modal" data-target="#singleTaskView" >+ <span class="fa fa-user"></span>&nbsp;<span ng-bind="task.Tags.length - 1" ></span></li>
                                <!--comment count--> 
                                <li class="pull-left btn-on-task" ng-click="taskSelectItem(task.Task.id);" data-toggle="modal" data-target="#singleTaskView"  title="&nbsp;&nbsp;&nbsp;Comments" tooltip><span class="fa fa-comments"></span>&nbsp;<span ng-if="task.Task.comment_count" ng-bind="task.Task.comment_count" ></span></li>
                                <!--view task--> 
                                <li class="pull-left btn-on-task" ng-click="taskSelectItem(task.Task.id);" data-toggle="modal" data-target="#singleTaskView" title="View task details" tooltip><span class="fa fa-eye"></span></li>
                            </div>

                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 padding-lr-0" >
                                <!--    edit delete -->
                                <li class='btn-on-task pull-right' ng-show="isTaskAdmin(task.Task, loggeduser) && !task.Task.is_completed"  title="Edit Task" data-toggle="modal" data-target="#taskAddModal" ng-click="taskSelectItem(task.Task.id);" tooltip data-placement="top" ><span class="fa fa-gear"></span></li>
                                <!--    delete  -->
                                <li class='btn-on-task pull-right' title="Delete Task" ng-click="taskDeleteModal(task.Task.id, 'pending');
                                        $event.stopPropagation();" ng-show="isTaskAdmin(task.Task, loggeduser)" tooltip data-placement="top"  ><span class="task-delete" ><i class="fa fa-trash-o"></i></span></li>
                            </div>
                        </div>
                    </div>            
                </div>
            </div>
        </div>


    </div>
    <!--COMPLETED TASKS-->
    <div class="completed-tasks row" ng-show="tasksType == 'Completed'">
        <!--pending meta data--> 
        <div class="container-fluid completed-meta-box">
            <div class="row ">
                <div class="completed-meta col-sm-12 padding-lr-0">
                    <div class="col-sm-3">
                        <p class="color-green text-center font-big-5 text-bold"><span ng-bind="TaskData.Completed.count"></span></p>
                    </div>
                    <div class="col-sm-9">
                        <p class="text-bold">Tasks Completed</p>
                        <p class="text-muted font-small-2">Completed tasks are indexed here.</p>
                    </div>
                </div>        
            </div>
        </div>
        <!--listing of tasks--> 
        <div class="container-fluid pending-tasks-list">
            <div ng-show="!TaskData.Completed">
                <div class='row center-block'>
                    <object data='/img/empty-templates/task_empty.svg' width="100%"></object>
                </div>
            </div>

            <div class="tasks-list g-scrollbar">

                <div ng-repeat="task in TaskData.Completed| orderBy:'Task.end_date'" >


                    <div class="task-tile col-sm-12" ng-click="selectedTask($index)" ng-class="{'task-tile-active': $index == selectedIndex}">
                        <!--    task color  -->
                        <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 padding-lr-0 task-tile-color" ng-class="{'list-group-item-danger': task.Task.id === TaskSelected.Task.id, 'task-group':getTaskType(task.Tags) === 'group', 'task-me':getTaskType(task.Tags) === 'self', 'task-individual':getTaskType(task.Tags) === 'user', 'task-other':getTaskType(task.Tags) === 'many', 'task-missed' : is_deadline_missed(task.Task.end_date)}"></div>
                        <!--    task creator  -->
                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 padding-lr-0 task-tile-creator">
                            <g-profile-img type='user'  path="{{task.user_info.vga}}" name="{{task.user_info.name}}" myclass="task-tile-ppl-user center-block font-small-1" width="20px" height="20px" popover="{{task.user_info.name}}" popover-trigger="mouseenter"></g-profile-img>
                        </div>
                        <!--    task body  -->
                        <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7 padding-lr-0 task-tile-msg" ng-class="{'task-complete': task.Task.is_completed}">
                            <p class="" ng-class="{'task-completing': ($index === task_completing), 'task-uncompleting':($index === task_uncompleting)}" ><span ng-bind='task.Task.title'></span> <br/> <span class="text-muted fa fa-clock-o"></span> <span class="text-muted font-small-1 p-margin-0" ng-bind='task.Task.end_date' ></span></p>

                        </div>

                        <!--    Mark complete option  -->
                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 pull-right padding-lr-0 task-tile-complete">
                            <p class="text-right" >
                                <span class="task-important-mark text-danger font-small-1" ng-show="task.Task.priority" tooltip data-placement="left" title="Important  "><i class="fa fa-square"></i></span>
                                <span class='btn-mark-complete text-muted text-emphasize' ng-class="{'del': task.Task.is_completed}" ng-click="markTaskComplete(task.Task.id, task.Task.is_completed, 'completed');
                                        $event.stopPropagation();" >
                                    <span class="text-success font-big-1" ng-show="task.Task.is_completed"  ><span class="fa fa-check"></span></span>
                                    <span ng-show="!task.Task.is_completed" ng-class="{'text-success': $index === task_completing}"  ><span class="fa fa-check"></span></span>
                                </span>
                            </p>
                        </div>
                        <div class="clearfix"></div>

                        <div class="clearfix"></div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding-lr-0 task-tile-bottom">

                            <div class="col-xs-9 padding-lr-0 text-muted" ng-show="task.Task.is_completed && task.Task.completed_by" style="padding:5px">

                                Submitted By<g-profile-img type='user'  path="{{task.Completed_by.vga}}" name="{{task.Completed_by.name}}" myclass="task-tile-ppl-user center-block font-small-1" width="20px" height="20px" tooltip title="{{users[getUserIndex(task.Task.completed_by)].UserInfo.name}}" data-placement="right" ></g-profile-img> at <span ng-bind="task.Task.completed" ></span>        
                            </div>

                            <!-- stats of task  -->
                            <div class="col-xs-9 padding-lr-0 text-muted">
                                <!--shared with--> 
                                <li ng-if="task.Tags.length > 1" class="pull-left btn-on-task" ng-click="taskSelectItem(task.Task.id);" data-toggle="modal" data-target="#singleTaskView" >+ <span class="fa fa-user"></span>&nbsp;<span ng-bind="task.Tags.length - 1" ></span></li>
                                <!--comment count--> 
                                <li class="pull-left btn-on-task" ng-click="taskSelectItem(task.Task.id);" data-toggle="modal" data-target="#singleTaskView"  title="&nbsp;&nbsp;&nbsp;Comments" tooltip><span class="fa fa-comments"></span>&nbsp;<span ng-if="task.Task.comment_count" ng-bind="task.Task.comment_count" ></span></li>
                                <!--view task--> 
                                <li class="pull-left btn-on-task" ng-click="taskSelectItem(task.Task.id);" data-toggle="modal" data-target="#singleTaskView" title="View task details" tooltip><span class="fa fa-eye"></span></li>
                            </div>

                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 padding-lr-0" >
                                <!--    edit delete -->
                                <li class='btn-on-task pull-right' title="Edit Task" data-toggle="modal" data-target="#taskAddModal" ng-show="isTaskAdmin(task.Task, loggeduser)" ng-click="taskSelectItem(task.Task.id);" tooltip data-placement="top" ><span class="fa fa-gear"></span></li>
                                <!--    delete  -->
                                <li class='btn-on-task pull-right' title="Delete Task" ng-show="isTaskAdmin(task.Task, loggeduser)" ng-click="taskDeleteModal(task.Task.id, 'completed');
                                        $event.stopPropagation();"><span class="task-delete" ><i class="fa fa-trash-o"></i></span></li>
                            </div>
                        </div>
                    </div>            
                </div>
            </div>
        </div>


    </div>

</div>
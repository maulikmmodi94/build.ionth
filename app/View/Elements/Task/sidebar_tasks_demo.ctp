<div class="sidebarTasks container-fluid" ng-controller="sideTasksCtrl" ng-show="rightSideBar">
    <!--DROP DOWN--> 
    <div class="sidebarTasksHead row" ng-controller="DropdownCtrl" ng-class="{'Pending':'pending - head', 'Completed':'completed - head'}[tasksType]">
        <li class="dropdown" dropdown>
            <p class="dropdown-toggle text-bold " dropdown-toggle>
                <span ng-bind="tasksType"></span>&nbsp;&nbsp;&nbsp;&nbsp; <span class="fa fa-caret-down"></span>
            </p>
            <ul class="dropdown-menu" role="menu">
                <li ng-show="tasksType !== 'Pending'" ng-click="changeTaskType('Pending')"><a href="#">Pending Tasks</a></li>
                <li ng-show="tasksType !== 'Completed'" ng-click="changeTaskType('Completed')"><a href="#">Completed Tasks</a></li>
            </ul>
        </li>
    </div>    
    <!--PENDING TASKS-->
    <div class="pending-tasks row" ng-show="tasksType == 'Pending'">
        <!--pending meta data--> 
        <div class="container-fluid pending-meta-box">
            <div class="row ">
                <div class="pending-meta col-sm-12 padding-lr-0">
                    <div class="col-sm-3">
                        <p class="color-red text-center font-big-5 text-bold">15</p>
                    </div>
                    <div class="col-sm-9">
                        <p class="text-bold">Tasks Pending</p>
                        <p class="text-muted font-small-2" ng-show="pendingNumber == '0'">Awesome, ninja! Enjoy the rest of your day.</p>
                        <p class="text-muted font-small-2" ng-show="pendingNumber !== '0'">Complete these tasks, they are past the deadlines.</p>
                    </div>
                </div>        
            </div>
        </div>
        <!--listing of tasks--> 
        <div class="container-fluid pending-tasks-list">

            <!--SINGLE TASK--> 
            <div class="singleTask container-fluid">
                <!--Task detail--> 
                <div class="taskDetails row">
                    <div class="col-sm-2 padding-right-0">
                        <img class="round-30" src="/img/demo/anupama.png" />
                    </div>
                    <div class="col-sm-8 padding-lr-0">
                        <div>
                            <p class="text-bold font-small-3">Anupama Panchal, <span class="text-muted">today</span></p>
                            <p class="task-title font-small-2">Content of the following pages, contact us, signup, login, resend verification, forgot password, tour, together we can.</p>

                        </div>
                    </div>
                    <div class="col-sm-2">                              <li class="dropdown" dropdown>
                            <p class="pull-right dropdown-toggle cursor-pointer" dropdown-toggle>
                                <span class="fa fa-gear"></span>
                            </p>
                            <ul class="dropdown-menu task-settings-menu dropdown-menu-right text-center" role="menu">
                                <li class=""><span class="fa fa-gears"></span> Edit Task</li>
                                <li class=""><span class="fa fa-trash-o"></span> Delete Task</li>

                            </ul>
                        </li>
                    </div>
                </div>
                <div class="taskShare row">
                    <div class="col-sm-4 col-sm-offset-2 padding-lr-0">
                        <p class="text-muted font-small-3 line-height-25 btn-link cursor-pointer">
                            Shared with 4    
                        </p>
                    </div>
                    <div class="col-sm-6">
                        <div class="mark-complete-checkbox checkbox pull-right">
                            <label class="font-small-3 color-green" style="">
                                <input type="checkbox" style=""> Mark Done
                            </label>
                        </div>
                    </div>
                </div>
                <!--task comments--> 
                <div class="row">
                    <div class="container-fluid task-comments-box" style="padding-top:5px">
                        <div class="row">
                            <div class="col-sm-2 padding-right-0">
                                <img class="round-25 shared-img" src="/img/demo/anupama.png" />
                            </div>
                            <div class="col-sm-10 padding-left-0">
                                <div class="">
                                    <input class="form-control input-sm" placeholder="write a comment" />
                                </div>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <p class="text-muted font-small-2 text-right">See all comments >></p>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

            <!--SINGLE TASK--> 
            <div class="singleTask container-fluid">
                <!--Task detail--> 
                <div class="taskDetails row">
                    <div class="col-sm-2 padding-right-0">
                        <img class="round-30" src="/img/demo/anupama.png" />
                    </div>
                    <div class="col-sm-8 padding-lr-0">
                        <div>
                            <p class="text-bold font-small-3">Anupama Panchal, <span class="text-muted">today</span></p>
                            <p class="task-title font-small-2">Content of the following pages, contact us, signup, login, resend verification, forgot password, tour, together we can.</p>

                        </div>
                    </div>
                    <div class="col-sm-2">                              <li class="dropdown" dropdown>
                            <p class="pull-right dropdown-toggle cursor-pointer" dropdown-toggle>
                                <span class="fa fa-gear"></span>
                            </p>
                            <ul class="dropdown-menu task-settings-menu dropdown-menu-right text-center" role="menu">
                                <li class=""><span class="fa fa-gears"></span> Edit Task</li>
                                <li class=""><span class="fa fa-trash-o"></span> Delete Task</li>

                            </ul>
                        </li>
                    </div>
                </div>
                <div class="taskShare row">
                    <div class="col-sm-4 col-sm-offset-2 padding-lr-0">
                        <p class="text-muted font-small-3 line-height-25">
                            Shared with 4    
                        </p>
                    </div>
                    <div class="col-sm-6">
                        <div class="mark-complete-checkbox checkbox pull-right">
                            <label class="font-small-3 color-green" style="">
                                <input type="checkbox" style=""> Mark Done
                            </label>
                        </div>
                    </div>
                </div>
                <!--task comments--> 
                <div class="row">
                    <div class="container-fluid task-comments-box" style="padding-top:5px">
                        <div class="row">
                            <div class="col-sm-2 padding-right-0">
                                <img class="round-25 shared-img" src="/img/demo/anupama.png" />
                            </div>
                            <div class="col-sm-10 padding-left-0">
                                <div class="">
                                    <input class="form-control input-sm" placeholder="write a comment" />
                                </div>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <p class="text-muted font-small-2 text-right">See all comments >></p>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

            <!--SINGLE TASK--> 
            <div class="singleTask container-fluid">
                <!--Task detail--> 
                <div class="taskDetails row">
                    <div class="col-sm-2 padding-right-0">
                        <img class="round-30" src="/img/demo/anupama.png" />
                    </div>
                    <div class="col-sm-8 padding-lr-0">
                        <div>
                            <p class="text-bold font-small-3">Anupama Panchal, <span class="text-muted">today</span></p>
                            <p class="task-title font-small-2">Content of the following pages, contact us, signup, login, resend verification, forgot password, tour, together we can.</p>

                        </div>
                    </div>
                    <div class="col-sm-2">                              <li class="dropdown" dropdown>
                            <p class="pull-right dropdown-toggle cursor-pointer" dropdown-toggle>
                                <span class="fa fa-gear"></span>
                            </p>
                            <ul class="dropdown-menu task-settings-menu dropdown-menu-right text-center" role="menu">
                                <li class=""><span class="fa fa-gears"></span> Edit Task</li>
                                <li class=""><span class="fa fa-trash-o"></span> Delete Task</li>

                            </ul>
                        </li>
                    </div>
                </div>
                <div class="taskShare row">
                    <div class="col-sm-4 col-sm-offset-2 padding-lr-0">
                        <p class="text-muted font-small-3 line-height-25">
                            Shared with 4    
                        </p>
                    </div>
                    <div class="col-sm-6">
                        <div class="mark-complete-checkbox checkbox pull-right">
                            <label class="font-small-3 color-green" style="">
                                <input type="checkbox" style=""> Mark Done
                            </label>
                        </div>
                    </div>
                </div>
                <!--task comments--> 
                <div class="row">
                    <div class="container-fluid task-comments-box" style="padding-top:5px">
                        <div class="row">
                            <div class="col-sm-2 padding-right-0">
                                <img class="round-25 shared-img" src="/img/demo/anupama.png" />
                            </div>
                            <div class="col-sm-10 padding-left-0">
                                <div class="">
                                    <input class="form-control input-sm" placeholder="write a comment" />
                                </div>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <p class="text-muted font-small-2 text-right">See all comments >></p>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

            <!--SINGLE TASK--> 
            <div class="singleTask container-fluid">
                <!--Task detail--> 
                <div class="taskDetails row">
                    <div class="col-sm-2 padding-right-0">
                        <img class="round-30" src="/img/demo/anupama.png" />
                    </div>
                    <div class="col-sm-8 padding-lr-0">
                        <div>
                            <p class="text-bold font-small-3">Anupama Panchal, <span class="text-muted">today</span></p>
                            <p class="task-title font-small-2">Content of the following pages, contact us, signup, login, resend verification, forgot password, tour, together we can.</p>

                        </div>
                    </div>
                    <div class="col-sm-2">                              <li class="dropdown" dropdown>
                            <p class="pull-right dropdown-toggle cursor-pointer" dropdown-toggle>
                                <span class="fa fa-gear"></span>
                            </p>
                            <ul class="dropdown-menu task-settings-menu dropdown-menu-right text-center" role="menu">
                                <li class=""><span class="fa fa-gears"></span> Edit Task</li>
                                <li class=""><span class="fa fa-trash-o"></span> Delete Task</li>

                            </ul>
                        </li>
                    </div>
                </div>
                <div class="taskShare row">
                    <div class="col-sm-4 col-sm-offset-2 padding-lr-0">
                        <p class="text-muted font-small-3 line-height-25">
                            Shared with 4    
                        </p>
                    </div>
                    <div class="col-sm-6">
                        <div class="mark-complete-checkbox checkbox pull-right">
                            <label class="font-small-3 color-green" style="">
                                <input type="checkbox" style=""> Mark Done
                            </label>
                        </div>
                    </div>
                </div>
                <!--task comments--> 
                <div class="row">
                    <div class="container-fluid task-comments-box" style="padding-top:5px">
                        <div class="row">
                            <div class="col-sm-2 padding-right-0">
                                <img class="round-25 shared-img" src="/img/demo/anupama.png" />
                            </div>
                            <div class="col-sm-10 padding-left-0">
                                <div class="">
                                    <input class="form-control input-sm" placeholder="write a comment" />
                                </div>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <p class="text-muted font-small-2 text-right">See all comments >></p>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

            <!--SINGLE TASK--> 
            <div class="singleTask container-fluid">
                <!--Task detail--> 
                <div class="taskDetails row">
                    <div class="col-sm-2 padding-right-0">
                        <img class="round-30" src="/img/demo/anupama.png" />
                    </div>
                    <div class="col-sm-8 padding-lr-0">
                        <div>
                            <p class="text-bold font-small-3">Anupama Panchal, <span class="text-muted">today</span></p>
                            <p class="task-title font-small-2">Content of the following pages, contact us, signup, login, resend verification, forgot password, tour, together we can.</p>

                        </div>
                    </div>
                    <div class="col-sm-2">                              <li class="dropdown" dropdown>
                            <p class="pull-right dropdown-toggle cursor-pointer" dropdown-toggle>
                                <span class="fa fa-gear"></span>
                            </p>
                            <ul class="dropdown-menu task-settings-menu dropdown-menu-right text-center" role="menu">
                                <li class=""><span class="fa fa-gears"></span> Edit Task</li>
                                <li class=""><span class="fa fa-trash-o"></span> Delete Task</li>

                            </ul>
                        </li>
                    </div>
                </div>
                <div class="taskShare row">
                    <div class="col-sm-4 col-sm-offset-2 padding-lr-0">
                        <p class="text-muted font-small-3 line-height-25">
                            Shared with 4    
                        </p>
                    </div>
                    <div class="col-sm-6">
                        <div class="mark-complete-checkbox checkbox pull-right">
                            <label class="font-small-3 color-green" style="">
                                <input type="checkbox" style=""> Mark Done
                            </label>
                        </div>
                    </div>
                </div>
                <!--task comments--> 
                <div class="row">
                    <div class="container-fluid task-comments-box" style="padding-top:5px">
                        <div class="row">
                            <div class="col-sm-2 padding-right-0">
                                <img class="round-25 shared-img" src="/img/demo/anupama.png" />
                            </div>
                            <div class="col-sm-10 padding-left-0">
                                <div class="">
                                    <input class="form-control input-sm" placeholder="write a comment" />
                                </div>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <p class="text-muted font-small-2 text-right">See all comments >></p>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>


    </div>
    <!--COMPLETED TASKS-->
    <div class="completed-tasks row" ng-show="tasksType == 'Completed'" >        
        <!--pending meta data--> 
        <div class="container-fluid completed-meta-box">
            <div class="row ">
                <div class="completed-meta col-sm-12 padding-lr-0">
                    <div class="col-sm-3">
                        <p class="color-green text-center font-big-5 text-bold">15</p>
                    </div>
                    <div class="col-sm-9">
                        <p class="text-bold">Tasks Completed</p>
                        <p class="text-muted font-small-2">All Tasks marked complete, are pinned here</p>
                    </div>
                </div>        
            </div>
        </div>
        <!--listing of tasks--> 
        <div class="container-fluid completed-tasks-list text-muted">
            <!--SINGLE TASK--> 
            <div class="singleTask container-fluid">
                <!--Task detail--> 
                <div class="taskDetails row">
                    <div class="col-sm-2 padding-right-0">
                        <img class="round-30" src="/img/demo/anupama.png" />
                    </div>
                    <div class="col-sm-8 padding-lr-0">
                        <div>
                            <p class="text-bold font-small-3">Anupama Panchal, <span class="text-muted taskdeadline">today</span></p>
                            <p class="task-title font-small-2">Content of the following pages, contact us, signup, login, resend verification, forgot password, tour, together we can.</p>

                        </div>
                    </div>
                    <div class="col-sm-2">                              <li class="dropdown" dropdown>
                            <p class="pull-right dropdown-toggle cursor-pointer" dropdown-toggle>
                                <span class="fa fa-gear text-muted"></span>
                            </p>
                            <ul class="dropdown-menu task-settings-menu dropdown-menu-right text-center" role="menu">
                                <li class=""><span class="fa fa-gears"></span> Edit Task</li>
                                <li class=""><span class="fa fa-trash-o"></span> Delete Task</li>

                            </ul>
                        </li>
                    </div>
                </div>
                <div class="taskShare row">
                    <div class="col-sm-4 col-sm-offset-2 padding-lr-0">
                        <p class="text-muted font-small-3 line-height-25 sharedWith">
                            Shared with 4    
                        </p>
                    </div>
                    <div class="col-sm-6">
                        <div class="mark-complete-checkbox checkbox pull-right">
                            <label class="font-small-3 color-green text-bold" style="">
                                <input type="checkbox" style=""> Mark incomplete
                            </label>
                        </div>
                    </div>
                </div>
                <!--task comments--> 
                <div class="row">
                    <div class="container-fluid task-comments-box" style="padding-top:5px">
                        <div class="row">
                            <div class="col-sm-2 padding-right-0">
                                <img class="round-25 shared-img" src="/img/demo/anupama.png" />
                            </div>
                            <div class="col-sm-10 padding-left-0">
                                <div class="">
                                    <input class="form-control input-sm" placeholder="write a comment" />
                                </div>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <p class="text-muted font-small-2 text-right">See all comments >></p>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

            <!--SINGLE TASK--> 
            <div class="singleTask container-fluid">
                <!--Task detail--> 
                <div class="taskDetails row">
                    <div class="col-sm-2 padding-right-0">
                        <img class="round-30" src="/img/demo/anupama.png" />
                    </div>
                    <div class="col-sm-8 padding-lr-0">
                        <div>
                            <p class="text-bold font-small-3">Anupama Panchal, <span class="text-muted taskdeadline">today</span></p>
                            <p class="task-title font-small-2">Content of the following pages, contact us, signup, login, resend verification, forgot password, tour, together we can.</p>

                        </div>
                    </div>
                    <div class="col-sm-2">                              <li class="dropdown" dropdown>
                            <p class="pull-right dropdown-toggle cursor-pointer" dropdown-toggle>
                                <span class="fa fa-gear text-muted"></span>
                            </p>
                            <ul class="dropdown-menu task-settings-menu dropdown-menu-right text-center" role="menu">
                                <li class=""><span class="fa fa-gears"></span> Edit Task</li>
                                <li class=""><span class="fa fa-trash-o"></span> Delete Task</li>

                            </ul>
                        </li>
                    </div>
                </div>
                <div class="taskShare row">
                    <div class="col-sm-4 col-sm-offset-2 padding-lr-0">
                        <p class="text-muted font-small-3 line-height-25 sharedWith">
                            Shared with 4    
                        </p>
                    </div>
                    <div class="col-sm-6">
                        <div class="mark-complete-checkbox checkbox pull-right">
                            <label class="font-small-3 color-green text-bold" style="">
                                <input type="checkbox" style=""> Mark incomplete
                            </label>
                        </div>
                    </div>
                </div>
                <!--task comments--> 
                <div class="row">
                    <div class="container-fluid task-comments-box" style="padding-top:5px">
                        <div class="row">
                            <div class="col-sm-2 padding-right-0">
                                <img class="round-25 shared-img" src="/img/demo/anupama.png" />
                            </div>
                            <div class="col-sm-10 padding-left-0">
                                <div class="">
                                    <input class="form-control input-sm" placeholder="write a comment" />
                                </div>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <p class="text-muted font-small-2 text-right">See all comments >></p>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

            <!--SINGLE TASK--> 
            <div class="singleTask container-fluid">
                <!--Task detail--> 
                <div class="taskDetails row">
                    <div class="col-sm-2 padding-right-0">
                        <img class="round-30" src="/img/demo/anupama.png" />
                    </div>
                    <div class="col-sm-8 padding-lr-0">
                        <div>
                            <p class="text-bold font-small-3">Anupama Panchal, <span class="text-muted taskdeadline">today</span></p>
                            <p class="task-title font-small-2">Content of the following pages, contact us, signup, login, resend verification, forgot password, tour, together we can.</p>

                        </div>
                    </div>
                    <div class="col-sm-2">                              <li class="dropdown" dropdown>
                            <p class="pull-right dropdown-toggle cursor-pointer" dropdown-toggle>
                                <span class="fa fa-gear text-muted"></span>
                            </p>
                            <ul class="dropdown-menu task-settings-menu dropdown-menu-right text-center" role="menu">
                                <li class=""><span class="fa fa-gears"></span> Edit Task</li>
                                <li class=""><span class="fa fa-trash-o"></span> Delete Task</li>

                            </ul>
                        </li>
                    </div>
                </div>
                <div class="taskShare row">
                    <div class="col-sm-4 col-sm-offset-2 padding-lr-0">
                        <p class="text-muted font-small-3 line-height-25 sharedWith">
                            Shared with 4    
                        </p>
                    </div>
                    <div class="col-sm-6">
                        <div class="mark-complete-checkbox checkbox pull-right">
                            <label class="font-small-3 color-green text-bold" style="">
                                <input type="checkbox" style=""> Mark incomplete
                            </label>
                        </div>
                    </div>
                </div>
                <!--task comments--> 
                <div class="row">
                    <div class="container-fluid task-comments-box" style="padding-top:5px">
                        <div class="row">
                            <div class="col-sm-2 padding-right-0">
                                <img class="round-25 shared-img" src="/img/demo/anupama.png" />
                            </div>
                            <div class="col-sm-10 padding-left-0">
                                <div class="">
                                    <input class="form-control input-sm" placeholder="write a comment" />
                                </div>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <p class="text-muted font-small-2 text-right">See all comments >></p>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

            <!--SINGLE TASK--> 
            <div class="singleTask container-fluid">
                <!--Task detail--> 
                <div class="taskDetails row">
                    <div class="col-sm-2 padding-right-0">
                        <img class="round-30" src="/img/demo/anupama.png" />
                    </div>
                    <div class="col-sm-8 padding-lr-0">
                        <div>
                            <p class="text-bold font-small-3">Anupama Panchal, <span class="text-muted taskdeadline">today</span></p>
                            <p class="task-title font-small-2">Content of the following pages, contact us, signup, login, resend verification, forgot password, tour, together we can.</p>

                        </div>
                    </div>
                    <div class="col-sm-2">                              <li class="dropdown" dropdown>
                            <p class="pull-right dropdown-toggle cursor-pointer" dropdown-toggle>
                                <span class="fa fa-gear text-muted"></span>
                            </p>
                            <ul class="dropdown-menu task-settings-menu dropdown-menu-right text-center" role="menu">
                                <li class=""><span class="fa fa-gears"></span> Edit Task</li>
                                <li class=""><span class="fa fa-trash-o"></span> Delete Task</li>

                            </ul>
                        </li>
                    </div>
                </div>
                <div class="taskShare row">
                    <div class="col-sm-4 col-sm-offset-2 padding-lr-0">
                        <p class="text-muted font-small-3 line-height-25 sharedWith">
                            Shared with 4    
                        </p>
                    </div>
                    <div class="col-sm-6">
                        <div class="mark-complete-checkbox checkbox pull-right">
                            <label class="font-small-3 color-green text-bold" style="">
                                <input type="checkbox" style=""> Mark incomplete
                            </label>
                        </div>
                    </div>
                </div>
                <!--task comments--> 
                <div class="row">
                    <div class="container-fluid task-comments-box" style="padding-top:5px">
                        <div class="row">
                            <div class="col-sm-2 padding-right-0">
                                <img class="round-25 shared-img" src="/img/demo/anupama.png" />
                            </div>
                            <div class="col-sm-10 padding-left-0">
                                <div class="">
                                    <input class="form-control input-sm" placeholder="write a comment" />
                                </div>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <p class="text-muted font-small-2 text-right">See all comments >></p>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

            <!--SINGLE TASK--> 
            <div class="singleTask container-fluid">
                <!--Task detail--> 
                <div class="taskDetails row">
                    <div class="col-sm-2 padding-right-0">
                        <img class="round-30" src="/img/demo/anupama.png" />
                    </div>
                    <div class="col-sm-8 padding-lr-0">
                        <div>
                            <p class="text-bold font-small-3">Anupama Panchal, <span class="text-muted taskdeadline">today</span></p>
                            <p class="task-title font-small-2">Content of the following pages, contact us, signup, login, resend verification, forgot password, tour, together we can.</p>

                        </div>
                    </div>
                    <div class="col-sm-2">                              <li class="dropdown" dropdown>
                            <p class="pull-right dropdown-toggle cursor-pointer" dropdown-toggle>
                                <span class="fa fa-gear text-muted"></span>
                            </p>
                            <ul class="dropdown-menu task-settings-menu dropdown-menu-right text-center" role="menu">
                                <li class=""><span class="fa fa-gears"></span> Edit Task</li>
                                <li class=""><span class="fa fa-trash-o"></span> Delete Task</li>

                            </ul>
                        </li>
                    </div>
                </div>
                <div class="taskShare row">
                    <div class="col-sm-4 col-sm-offset-2 padding-lr-0">
                        <p class="text-muted font-small-3 line-height-25 sharedWith">
                            Shared with 4    
                        </p>
                    </div>
                    <div class="col-sm-6">
                        <div class="mark-complete-checkbox checkbox pull-right">
                            <label class="font-small-3 color-green text-bold" style="">
                                <input type="checkbox" style=""> Mark incomplete
                            </label>
                        </div>
                    </div>
                </div>
                <!--task comments--> 
                <div class="row">
                    <div class="container-fluid task-comments-box" style="padding-top:5px">
                        <div class="row">
                            <div class="col-sm-2 padding-right-0">
                                <img class="round-25 shared-img" src="/img/demo/anupama.png" />
                            </div>
                            <div class="col-sm-10 padding-left-0">
                                <div class="">
                                    <input class="form-control input-sm" placeholder="write a comment" />
                                </div>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <p class="text-muted font-small-2 text-right">See all comments >></p>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

            <!--SINGLE TASK--> 
            <div class="singleTask container-fluid">
                <!--Task detail--> 
                <div class="taskDetails row">
                    <div class="col-sm-2 padding-right-0">
                        <img class="round-30" src="/img/demo/anupama.png" />
                    </div>
                    <div class="col-sm-8 padding-lr-0">
                        <div>
                            <p class="text-bold font-small-3">Anupama Panchal, <span class="text-muted taskdeadline">today</span></p>
                            <p class="task-title font-small-2">Content of the following pages, contact us, signup, login, resend verification, forgot password, tour, together we can.</p>

                        </div>
                    </div>
                    <div class="col-sm-2">                              <li class="dropdown" dropdown>
                            <p class="pull-right dropdown-toggle cursor-pointer" dropdown-toggle>
                                <span class="fa fa-gear text-muted"></span>
                            </p>
                            <ul class="dropdown-menu task-settings-menu dropdown-menu-right text-center" role="menu">
                                <li class=""><span class="fa fa-gears"></span> Edit Task</li>
                                <li class=""><span class="fa fa-trash-o"></span> Delete Task</li>

                            </ul>
                        </li>
                    </div>
                </div>
                <div class="taskShare row">
                    <div class="col-sm-4 col-sm-offset-2 padding-lr-0">
                        <p class="text-muted font-small-3 line-height-25 sharedWith">
                            Shared with 4    
                        </p>
                    </div>
                    <div class="col-sm-6">
                        <div class="mark-complete-checkbox checkbox pull-right">
                            <label class="font-small-3 color-green text-bold" style="">
                                <input type="checkbox" style=""> Mark incomplete
                            </label>
                        </div>
                    </div>
                </div>
                <!--task comments--> 
                <div class="row">
                    <div class="container-fluid task-comments-box" style="padding-top:5px">
                        <div class="row">
                            <div class="col-sm-2 padding-right-0">
                                <img class="round-25 shared-img" src="/img/demo/anupama.png" />
                            </div>
                            <div class="col-sm-10 padding-left-0">
                                <div class="">
                                    <input class="form-control input-sm" placeholder="write a comment" />
                                </div>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <p class="text-muted font-small-2 text-right">See all comments >></p>
                            </div>
                        </div>
                    </div>
                </div>

            </div>


        </div>


    </div>

</div>
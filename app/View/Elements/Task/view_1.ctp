<!--    This Model Need TaskController and Task Service -->
<div class="task-listing-area row">
    <!--    LISTING OF TASKS    --> 
    <div class="tasks-list g-scrollbar">
        <div class="container-fluid" ng-repeat="task in filteredTask = (TaskData| taskSearch:search | filterWithAssignee | filterBy | filterIncompleteTasks | orderBy:'Task.end_date')" >
            <!--Date Separator--> 
            <div class="row task-date-separator text-center" ng-if="$index == 0 || (dateFormat(filteredTask[$index].Task.end_date, 'shortDate') !== dateFormat(filteredTask[$index - 1].Task.end_date, 'shortDate'))">
                <p class="task-date-separator-badge text-muted font-small-1 " ng-bind='momentDate(task.Task.end_date)'></p>
            </div>
            <!--date separator end-->
            <div class="task-tile row" ng-click="selectedTask($index)" ng-class="{ 'task-tile-active': $index == selectedIndex}">
                <!--    task color  -->
                <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 padding-lr-0 task-tile-color" ng-class="{'list-group-item-danger': task.Task.id === TaskSelected.Task.id, 'task-group':getTaskType(task.Tags) === 'group', 'task-me':getTaskType(task.Tags) === 'self', 'task-individual':getTaskType(task.Tags) === 'user', 'task-other':getTaskType(task.Tags) === 'many', 'task-missed' : is_deadline_missed(task.Task.end_date)}"></div>
                <!--    task creator  -->
                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 padding-lr-0 task-tile-creator">
                    <g-profile-img type='user'  path="{{users[getUserIndex(task.Task.created_by)].UserInfo.vga}}" name="{{users[getUserIndex(task.Task.created_by)].UserInfo.name}}" myclass="task-tile-ppl-user center-block font-small-1" width="20px" height="20px" tooltip title="{{users[getUserIndex(task.Task.created_by)].UserInfo.name}}" data-placement="right" ></g-profile-img>
                </div>
                <!--    task body  -->
                <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7 padding-lr-0 task-tile-msg" ng-class="{'task-complete': task.Task.is_completed}">
                    <p class="" ng-class="{'task-completing': ($index === task_completing), 'task-uncompleting':($index === task_uncompleting)}" ><span ng-bind='task.Task.title'></span> <br/> <span class="text-muted fa fa-clock-o"></span> <span class="text-muted font-small-1 p-margin-0" ng-bind='dateFormat(task.Task.end_date, "MMM d, h:mm a")' ></span></p>
                </div>
                <!--    Mark complete option  -->
                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 pull-right padding-lr-0 task-tile-complete">
                    <p class="text-right" >
                        <span class="task-important-mark text-danger font-small-1" ng-show="task.Task.priority" tooltip data-placement="left" title="Important  "><i class="fa fa-square"></i></span>
                        <span class='btn-mark-complete text-muted text-emphasize' ng-class="{'del': task.Task.is_completed}" ng-click="taskSetCompleted(task.Task.id, task.Task.is_completed, $index); $event.stopPropagation();" >
                            <span class="text-success font-big-1" ng-show="task.Task.is_completed"  ><span class="fa fa-check"></span></span>
                            <span ng-show="!task.Task.is_completed" ng-class="{'text-success': $index === task_completing}"  ><span class="fa fa-check"></span></span>
                        </span>
                    </p>
                </div>
                <div class="clearfix"></div>
                <!--    SHARED PEOPLE   -->
                <!--                <div class="task-tile-ppl-area col-lg-offset-2 col-lg-9 col-md-offset-2 col-md-9 col-sm-offset-2 col-sm-9 col-xs-offset-2 col-xs-9 padding-lr-0" >
                                    <p ng-if="task.Tags.length > 1" class="text-muted font-small-1 p-margin-0"><span   >Shared with</span></p>
                                    <div class="task-tile-ppl font-small-1" ng-repeat="tags in task.Tags" data-type="{{tags.type}}" ng-show="tags.role == 'user'" ng-class="{'badge-sucess': tags.type === 'group'}"  > 
                                            if group    
                                        <p class="task-tile-ppl-grp font-small-1 p-margin-0  text-capitalize" ng-if="tags.type === 'group'" title="Team :  {{groups[findGroup(tags.value)].Group.name}}" ng-bind='groups[findGroup(tags.value)].Group.name' tooltip data-placement="bottom" ></p>
                                         if user    
                                        <span ng-if="tags.type === 'user'">
                                            <g-profile-img type='user'  path="{{users[getUserIndex(tags.value)].UserInfo.vga}}" name="{{users[getUserIndex(tags.value)].UserInfo.name}}" myclass="task-tile-ppl-user p-margin-0 font-small-1" width="20px" height="20px" myTitle="{{users[getUserIndex(tags.value)].UserInfo.name}}" tooltippos="bottom" debug="true" debug="true" ></g-profile-img>
                                        </span>
                                    </div>
                                </div>-->
<!--                <comment-form model='tasks' id='{{task.Task.id}}' add="taskService.addComment(id,body)" delete="taskService.deleteComment(id,commentId)" comments=task.Comment loadmore="{{task.moreComments}}" load="taskService.getComment(id,lastId)"></comment-form>-->
                <div class="clearfix"></div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding-lr-0 task-tile-bottom">
                    <!-- stats of task  -->
                    <div class="col-xs-9 padding-lr-0 text-muted">
                        <li ng-if="task.Tags.length > 1" class="pull-left btn-on-task" ng-click="taskSelectItem(task.Task.id);" data-toggle="modal" data-target="#singleTaskView" >+ <span class="fa fa-user"></span>&nbsp;<span ng-bind="task.Tags.length - 1" ></span></li>
                        <li ng-if="task.Task.comment_count" class="pull-left btn-on-task" ng-click="taskSelectItem(task.Task.id);" data-toggle="modal" data-target="#singleTaskView" ><span class="fa fa-comments"></span>&nbsp;<span ng-bind="task.Task.comment_count" ></span></li>
                        <li class="pull-left btn-on-task" ng-click="taskSelectItem(task.Task.id);" data-toggle="modal" data-target="#singleTaskView" ><span class="fa fa-eye"></span></li>
                    </div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 padding-lr-0" >
                        <!--    edit delete -->
                        <li class='btn-on-task pull-right' ng-show="isAssigee(task.Task.id, loggeduser, 'admin') && !task.Task.is_completed"  title="Edit Task" data-toggle="modal" data-target="#taskAddModal" ng-click="taskSelectItem(task.Task.id);" tooltip data-placement="top" ><span class="fa fa-gear"></span></li>
                        <!--    delete  -->
                        <li class='btn-on-task pull-right' title="Delete Task" ng-click="taskDelete(task.Task.id); $event.stopPropagation();" ng-show="isAssigee(task.Task.id, loggeduser, 'admin')" tooltip data-placement="top"  ><span class="task-delete" ><i class="fa fa-trash-o"></i></span></li>
                    </div>
                </div>
            </div>            
        </div>
    </div>
</div>
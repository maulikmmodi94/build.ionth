<!--CREATE CONVERSATION MODAL WINDOW-->
<div id="singleTaskView" class="modal fade">
    <div class="modal-dialog g-modal-large ">
        <div class="modal-content" style="min-height: 400px; padding-bottom: 20px;">
            <!--Header--> 
            <div id="" class="modal-header g-background-white">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title text-center ">
                    <span class="">Task from <span class="text-semi-bold" ng-bind="users[getUserIndex(TaskSelected.Task.created_by)].UserInfo.name"></span></span>
                </h4>
            </div>
            <!--    BODY    -->
            <div class="g-modal-body modal-body view-task-modal-body container-fluid">
                <div class="row">
                    <div class="col-xs-1 padding-lr-0" style="padding-top: 10%;" >
                        <button class="btn btn-default" ng-click="taskService.taskCarousel(filteredTask, selectedIndex, 'previous')" title=" Previous Task" data-placement="top" tooltip ><span class="fa fa-caret-left" ></span></button>
                    </div>
                    <div class="col-xs-10">
                        <!--Task Details--> 
                        <div class='row view-task-info'>
                            <!--    task image     -->
                            <div class="g-task-img-cover col-lg-2 col-lg-offset-0 col-md-2 col-sm-2 col-xs-2 padding-lr-0">
                                <p class='g-task-icon-cover mini text-center p-margin-0 pull-right'>
                                    <span class="g-task-icon fa fa-tasks"></span>
                                </p>
                            </div>
                            <!--    task name      -->
                            <div class='g-task-descriptio col-lg-10 col-md-10 col-sm-10 col-xs-10 '>
                                <div clas="row">
                                    <div class="col-xs-12 padding-lr-0">
                                        <p class='g-task-title-text font-big-1 ' >{{TaskSelected.Task.title}} </p>
                                    </div>
                                    <!--Mark complete action--> 
                                    <!--                                    <div class="col-xs-2">
                                                                            <span class='btn-mark-complete text-muted text-emphasize' ng-class="{'del': task.Task.is_completed}" ng-click="taskSetCompleted(TaskSelected.Task.id, TaskSelected.Task.is_completed, $index);" >
                                                                                <span class="text-success font-big-1" ng-show="TaskSelected.Task.is_completed"  ><span class="fa fa-check"></span></span>
                                                                                <span ng-show="!TaskSelected.Task.is_completed" ng-class="{'text-success': $index === task_completing}"  ><span class="fa fa-check"></span></span>
                                                                            </span>
                                                                        </div>-->
                                </div>

                                <p class=''>
<!--                                    <span class='text-muted'>Start : {{ dateFormat(TaskSelected.Task.start_date, 'medium')}}&nbsp;&nbsp;&nbsp;</span>-->
                                    <span class='text-muted font-small-1'><span class="fa fa-clock-o"></span>&nbsp;&nbsp; {{ dateFormat(TaskSelected.Task.end_date, 'medium')}}</span>&nbsp;&nbsp;
                                    <span ng-show='TaskSelected.Task.priority' class=' label badge-danger text-semi-bold' >Important</span>
<!--                                    <span ng-show="!TaskSelected.Task.is_completed" class="label label-warning text-emphasize">Uncomplete</span>
                                    <span ng-show="TaskSelected.Task.is_completed" class="label label-success text-emphasize">Complete</span>-->
                                </p>
                                <!--    SHARED PEOPLE   -->
                                <div class="task-tile-ppl-area col-xs-10" >
                                    <p ng-if="TaskSelected.Tags.length > 1" class="text-muted font-small-1"><span>Shared with <span ng-bind="TaskSelected.Tags.length - 1"></span></span></p>
                                    <div class="task-tile-ppl" ng-repeat="tags in TaskSelected.Tags" data-type="{{tags.type}}" ng-show="tags.role == 'user'" ng-class="{
                                                'badge-sucess': tags.type === 'group'}"  > 
                                        <!--if group-->    
                                        <p class=" pull-right task-tile-ppl-grp p-margin-0  text-capitalize" ng-if="tags.type === 'group'" title="Team :  {{groups[findGroup(tags.value)].Group.name}}" ng-bind='groups[findGroup(tags.value)].Group.name' tooltip data-placement="bottom" ></p>
                                        <!--if user-->    
                                        <span ng-if="tags.type === 'user'">
                                            <g-profile-img type='user'  path="{{users[getUserIndex(tags.value)].UserInfo.vga}}" name="{{users[getUserIndex(tags.value)].UserInfo.name}}" myclass="task-tile-ppl-user p-margin-0 pull-right" width="30px" height="30px" tooltip title="{{users[getUserIndex(tags.value)].UserInfo.name}}" data-placement="bottom"></g-profile-img>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--Comments-->
                        <div class="row">
                            <comment-form id="{{TaskSelected.Task.id}}" add="taskService.addComment(id,body)" delete="taskService.deleteCommentModal(id,commentId)" comments=TaskSelected.Comment loadmore="{{TaskSelected.moreComments}}" load="taskService.getComment(id,lastId)" commentcount=TaskSelected.Task.comment_count></comment-form></div>
                    </div>

                    <!--Next--> 
                    <div class="col-xs-1 padding-lr-0 text-center" style="padding-top: 10%;" >
                        <button class="btn btn-default" ng-click="taskService.taskCarousel(filteredTask, selectedIndex, 'next')" title="Next Task"  data-placement="top"  tooltip ><span class="fa fa-caret-right" ></span></button>
                    </div>
                </div>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

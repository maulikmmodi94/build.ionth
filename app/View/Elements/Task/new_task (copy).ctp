<!--        MODAL WINDOW FOR NEW TASK          -->
<div id='taskAddModal' class="modal fade  task-add-modal">
    <div class="g-modal modal-dialog" >
        <!--        CONTENT          -->
        <div class="g-modal-content modal-content" style='z-index: -2;'>
            <form id="form-new-task" role='form' name="taskAddForm" ng-submit="taskAdd(TaskSelected)" novalidate >

                <!--        HEADER          -->
                <div class="g-modal-header modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="g-modal-title modal-title ">
                        <span class="g-modal-header-icon fa fa-tasks"></span> 
                        <span class="g-modal-header-title-text" ng-hide="TaskSelected.Task.id">New Task</span>
                        <span class="g-modal-header-title-text" ng-show="TaskSelected.Task.id">Edit Task</span>
                    </h4>
                </div>
                <!--        BODY          -->
                <div class="g-modal-body modal-body">
                    <div class="new-task-cover">
                        <!--    progress bar    -->

                        <ul id="progressbar" class=''>
                            <li class="btn-step-1 active"></li>
                            <li class='btn-step-2'></li>
                        </ul>
                        <!-- multistep form -->
                        <div class='form-new-task-area text-center'>
                            <!-- first fieldset -->
                            <fieldset class='new-task-fs' id="new-task-first">
                                <div class="new-task-fs-div" id="new-task-first-div" >
                                    <p class="font-big-2">Make a new task and share it</p>
                                    <div class='form-group'>
                                        <!--    TASK TEXT AREA   --->
                                        <textarea class='form-control'  ng-model="TaskSelected.Task.title" name="data[Task][title]" placeholder="Write your task here" rows='2' required  ng-maxlength=500 maxlength=500></textarea>
                                        <div class="form-inline-error text-warning" ng-show="taskAddForm['data[Task][title]'].$dirty">
                                            <p ng-show="taskAddForm['data[Task][title]'].$error.required" class="p-margin-0">Please write the task</p>
                                            <p ng-show="taskAddForm['data[Task][title]'].$error.maxlegth" class="p-margin-0">Maximum 500 characters are allowed</p>
                                        </div>
                                    </div>
                                    <!--    Assign task to      -->
                                    <div class='form-group text-left'>
                                        <tags-input ng-model="tags" add-from-autocomplete-only="true"  display-property="name" placeholder="Share with">
                                            <auto-complete source="taskLoadTags($query)"></auto-complete>
                                        </tags-input>
                                    </div>
                                    <!--    existing users      -->
                                    <p class="">
                                        <span style="margin:2px;" ng-repeat="tags in TaskSelected.Tags" data-type="{{tags.type}}" >
                                            <!--    if existing user is group   -->
                                            <span ng-if="tags.type == 'group'" class="label label-info" ><span ng-bind="groups[findGroup(tags.value)].Group.name"></span> <i class="fa fa-times" ng-click="taskRemoveAssigneeConfirm(tags, TaskSelected.Task.id)" ng-show="isAssigee(TaskSelected.Task.id, loggeduser, 'admin')"></i></span>
                                            <!--    if existing user    -->
                                            <span ng-if="tags.type == 'user'" ng-show ="tags.role=='user'" class="label label-warning" ><span ng-bind="users[getUserIndex(tags.value)].UserInfo.name"></span> <i class="fa fa-times" ng-show="isAssigee(TaskSelected.Task.id, loggeduser, 'admin')"  ng-click="taskRemoveAssigneeConfirm(tags, TaskSelected.Task.id)"></i></span>
                                        </span>
                                    </p>
                                </div>
                                <div>
                                    <!--    next button -->
                                    <button type="button" class="btn btn-default btn-sm btn-step-2 pull-right">Next</button>
                                </div>
                            </fieldset>
                            <!-- second fieldset -->
                            <fieldset class='new-task-fs' id="new-task-second">
                                <div class="new-task-fs-div" >
                                    <p class="font-big-2">Due date and Priority</p>

                                    <div class='form-group ' id="start_date">
                                        <div class='input-group'>
                                            <input class="form-control input-sm date-task" type="text" datechange="" ng-model="TaskSelected.Task.start_date" placeholder="Start Date" name="start_date" id="start_date_inner">
                                            <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                        </div>
                                    </div>
                                    <div class='form-group input-group' id="end_date">
                                        <input class="form-control input-sm date-task" type="text" datechange="" ng-model="TaskSelected.Task.end_date" placeholder="End Date" name="end_date" id="end_date_inner">
                                        <span class="input-group-addon"><span class="fa fa-calendar"></span></span>

                                    </div>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" ng-model="TaskSelected.Task.priority" name="data[Task][priority]"> Important
                                        </label>
                                    </div>
                                </div>
                                <div>
                                    <button type="button"  class="btn btn-default btn-sm btn-step-1 pull-left" >Previous</button>    
                                </div>
                            </fieldset>
                        </div>
                    </div>
                </div>
                <div class="modal-footer gridle-modal-footer">
                    <input type="submit" name="submit" class="btn btn-success btn-sm center-block" value="Save Task" data-loading-text="Loading..." type="submit" ng-click=" TaskSelected.TaskAssignment.assignee_data = tags;" ng-disabled="taskAddForm.$invalid" style='z-index: -1;' />
                </div>
            </form>
        </div>
    </div>
</div>
<script>
    //jQuery time
    //var current_fs, next_fs, previous_fs; //fieldsets
    var current_fs, first_fs, second_fs;
    var left, opacity, scale; //fieldset properties which we will animate
    var animating; //flag to prevent quick multi-click glitches

    first_fs = $('.new-task-fs:first');
    second_fs = first_fs.next();
    current_fs = first_fs;

    $('.btn-step-2').click(function() {
        if (animating)
            return false;
        animating = true;

        if (current_fs === second_fs)
            return;

        //activate next step on progressbar using the index of next_fs
        $("#progressbar li").eq($("fieldset").index(second_fs)).addClass("active");

        //show the next fieldset
        //second_fs.show();
        second_fs.css('display', 'table-cell');
        //hide the current fieldset with style
        current_fs.animate({opacity: 0}, {
            step: function(now, mx) {
                //as the opacity of current_fs reduces to 0 - stored in "now"
                //1. scale current_fs down to 80%
                scale = 1 - (1 - now) * 0.0;
                //2. bring next_fs from the right(50%)
                left = (now * 50) + "%";
                //3. increase opacity of next_fs to 1 as it moves in
                opacity = 1 - now;
                current_fs.css({'transform': 'scale(' + scale + ')'});
                second_fs.css({'left': left, 'opacity': opacity});
            },
            duration: 800,
            complete: function() {
                current_fs.hide();
                current_fs = second_fs;
                animating = false;
            },
            //this comes from the custom easing plugin
            easing: 'easeInOutBack'
        });


    });

    $('.btn-step-1').click(function() {
        if (animating)
            return false;
        animating = true;

        if (current_fs === first_fs)
            return;

        //activate next step on progressbar using the index of next_fs
        $("#progressbar li").eq($("fieldset").index(second_fs)).removeClass("active");

        //show the next fieldset
        //first_fs.show();
        first_fs.css('display', 'table-cell');
        //hide the current fieldset with style
        current_fs.animate({opacity: 0}, {
            step: function(now, mx) {
                //as the opacity of current_fs reduces to 0 - stored in "now"
                //1. scale current_fs down to 80%
                scale = 1 - (1 - now) * 0.0;
                //2. bring next_fs from the right(50%)
                left = (now * 50) + "%";
                //3. increase opacity of next_fs to 1 as it moves in
                opacity = 1 - now;
                current_fs.css({'transform': 'scale(' + scale + ')'});
                first_fs.css({'left': left, 'opacity': opacity});
            },
            duration: 800,
            complete: function() {
                current_fs.hide();
                current_fs = first_fs;
                animating = false;
            },
            //this comes from the custom easing plugin
            easing: 'easeInOutBack'
        });
    });

    /* Task Add JS*/
    $(function() {
        $('#start_date').datetimepicker({
            icons: {
                time: "fa fa-clock-o",
                date: "fa fa-calendar",
                up: "fa fa-arrow-up",
                down: "fa fa-arrow-down"
            }
        });
        $('#end_date').datetimepicker({
            icons: {
                time: "fa fa-clock-o",
                date: "fa fa-calendar",
                up: "fa fa-arrow-up",
                down: "fa fa-arrow-down"
            }
        });
        yesterday = new Date();
        yesterday.setDate(yesterday.getDate() - 1);

        $('#start_date').data("DateTimePicker").setMinDate(yesterday);
        $("#start_date").on("dp.change", function(e) {
            $('#end_date').data("DateTimePicker").setMinDate(e.date);
            $('#start_date_inner').trigger('input');

        });

        $("#end_date").on("dp.change", function(e) {
            $('#start_date').data("DateTimePicker").setMaxDate(e.date);
            $('#end_date_inner').trigger('input');

        });

        $("#start_date").on("dp.show", function(e) {
            $('#end_date').data("DateTimePicker").setMinDate(e.date);
            $('#start_date_inner').trigger('input');

        });



        $('#end_date').on('dp.show', function(e) {
            $('#start_date').data("DateTimePicker").setMaxDate(e.date);
            $('#end_date_inner').trigger('input');

        });

        jQuery.validator.addMethod("greaterThan",
                function(value, element, params) {
                    if (!$(params).val())
                    {
                        return true;
                    }
                    else if (!value)
                    {
                        return true;
                    }


                    if (!/Invalid|NaN/.test(new Date(value))) {
                        return new Date(value) > new Date($(params).val());
                    }

                    return isNaN(value) && isNaN($(params).val())
                            || (Number(value) > Number($(params).val()));
                }, 'Must be greater than {0}.');

        //$("#end_date").rules('add', {greaterThan: "#start_date"});
        $("#form-new-task").validate({
            rules: {
                'end_date': {
                    greaterThan: '#start_date_inner'
                }
            }
        });

    });

    $(document).mouseup(function(e)
    {
        var container = $("#form-new-task");

        if (!container.is(e.target) // if the target of the click isn't the container...
                && container.has(e.target).length === 0) // ... nor a descendant of the container
        {
        }
    });



    $('#taskAddModal').on('show.bs.modal', function(e) {
        $('#start_date').data("DateTimePicker").setDate($('#start_date_inner').val());
        $('#end_date').data("DateTimePicker").setDate($('#end_date_inner').val());
    });


//dynamic height setting of the task edit / add modal
    $('#taskAddModal').on('shown.bs.modal', function(e) {
        if ($('#new-task-first').height() > 210) {
            $('.form-new-task-area').height($('#new-task-first').height());
            $('.form-new-task-area fieldset').height($('#new-task-first').height());
        }
    });
    //setting default height again
    $('#taskAddModal').on('hidden.bs.modal', function(e) {
        if ($('#new-task-first').height() > 210) {
            $('.form-new-task-area').height(210);
            $('.form-new-task-area fieldset').height(210);
        }
        //if second fieldset is open set it back to first;
        $('.btn-step-1').click();
    });

</script>
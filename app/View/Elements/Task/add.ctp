<!-- 
This Model Need TaskController and Task Service
-->

<div class="modal fade task-add-modal" id="taskAddModal1" tabindex="-1" role="dialog" aria-labelledby="taskAddModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="taskAddModalLabel">Add Task</h4>
            </div>
            <div class="modal-body">
                <div>


                    <form name="taskAddForm" ng-submit="taskAdd(TaskSelected)" id="form-new-task" role="form" style=""  method="POST">
                        <div class="form-group">
                            <input type="text" ng-model="TaskSelected.Task.title" name="data[Task][title]" class="form-control input_form" placeholder="Task details here" required>
                        </div>
                        <div class="form-group">
                            <textarea ng-model="TaskSelected.Task.description" name="data[Task][description]" class="form-control" placeholder="Desciption" style="resize: vertical"></textarea>
                        </div>
                        <!--                            <div class="form-group">
                                                        <select class="form-control" ng-init="TaskSelected.Task.priority = 1" ng-model="TaskSelected.Task.priority" name="data[Task][priority]">
                                                            <option>1</option>
                                                            <option>2</option>
                                                            <option>3</option>
                                                            <option>4</option>
                                                            <option>5</option>
                                                        </select>
                                                    </div>-->
                        <div class="form-group">
                            <input class="form-control date-task" type="text" datechange="" ng-model="TaskSelected.Task.start_date" placeholder="Start Date" name="start_date" id="start_date">
                        </div>
                        <div class="form-group">
                            <input class="form-control date-task" type="text" datechange="" ng-model="TaskSelected.Task.end_date" placeholder="End Date" name="end_date" id="end_date">
                        </div>
                        <div class="form-group">

                            <span style="margin:2px;" class="label label-default task-assignee" ng-repeat="tags in TaskSelected.Tags" data-type="{{tags.type}}" ng-show="tags.role == 'user'"  ng-class="{'label-info': tags.type == 'group'}">

                                <span ng-if="tags.type == 'group'">
                                    {{groups[findGroup(tags.value)].Group.name}} 
                                    <i class="fa fa-times" ng-show="isAssigee(task.Task.id, loggeduser, 'admin')"  ng-click="taskRemoveAssigneeConfirm(tags, task.Task.id)"></i></span>
                                <span ng-if="tags.type == 'user'">{{users[getUserIndex(tags.value)].UserInfo.name}} <i class="fa fa-times"  ng-click="taskRemoveAssigneeConfirm(tags, task.Task.id)"></i></span>
                            </span>



                        </div>
                        <div class="form-group">
                            <div>
                                <tags-input ng-model="tags" add-from-autocomplete-only="true"  display-property="name">
                                    <auto-complete source="taskLoadTags($query)"></auto-complete>
                                </tags-input>
                            </div>
                        </div>
                        <button  class="btn btn-info" data-loading-text="Loading..." type="submit" ng-click=" TaskSelected.TaskAssignment.assignee_data = tags;" ng-disabled="taskAddForm.$invalid">Add Task <i class="fa fa-check"></i></button>


                    </form>
                </div>

            </div>

        </div>
    </div>
</div>
<script>
            $(function() {
                $('#start_date').datetimepicker({
                    icons: {
                        time: "fa fa-clock-o",
                        date: "fa fa-calendar",
                        up: "fa fa-arrow-up",
                        down: "fa fa-arrow-down"
                    }
                });
                $('#end_date').datetimepicker({
                    icons: {
                        time: "fa fa-clock-o",
                        date: "fa fa-calendar",
                        up: "fa fa-arrow-up",
                        down: "fa fa-arrow-down"
                    }
                });
                $('#start_date').data("DateTimePicker").setMinDate(new Date());
                $("#start_date").on("dp.change", function(e) {
                    $('#end_date').data("DateTimePicker").setMinDate(e.date);
                    console.log($('#start_date').val());
                    $('#start_date').trigger('input');

                });
                $("#end_date").on("dp.change", function(e) {
                    start_date = $('#start_date').data("DateTimePicker").getDate();
                    end_date = $('#end_date').data("DateTimePicker").getDate();
                    console.log(start_date <= end_date);
                    $('#start_date').data("DateTimePicker").setMaxDate(e.date);
                });

                jQuery.validator.addMethod("greaterThan",
                        function(value, element, params) {
                            if (!$(params).val())
                            {
                                return true;
                            }
                            else if (!value)
                            {
                                return true;
                            }


                            if (!/Invalid|NaN/.test(new Date(value))) {
                                return new Date(value) > new Date($(params).val());
                            }

                            return isNaN(value) && isNaN($(params).val())
                                    || (Number(value) > Number($(params).val()));
                        }, 'Must be greater than {0}.');

                //$("#end_date").rules('add', {greaterThan: "#start_date"});
                $("#form-new-task").validate({
                    rules: {
                        'end_date': {
                            greaterThan: '#start_date'
                        }
                    }
                });

            });

            $(document).mouseup(function(e)
            {
                var container = $("#form-new-task");

                if (!container.is(e.target) // if the target of the click isn't the container...
                        && container.has(e.target).length === 0) // ... nor a descendant of the container
                {
                }
            });

            

            $('#taskAddModal').on('show.bs.modal', function(e) {
                // do something...
                $('#start_date').data("DateTimePicker").setDate($('#start_date').val());
                $('#end_date').data("DateTimePicker").setDate($('#end_date').val());
            })

</script>
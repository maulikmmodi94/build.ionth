<!--        MODAL WINDOW FOR NEW TASK          -->
<div id='taskAddModal' class="modal fade  task-add-modal">
    <div class="g-modal-large g-modal modal-dialog" >
        <!--        CONTENT          -->
        <div class="g-modal-content modal-content" style='z-index: -2;'>
            <form id="form-new-task" role='form' name="taskAddForm" ng-submit="taskAdd(TaskSelected)" novalidate >

                <!--        HEADER          -->
                <div class="g-modal-header modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="g-modal-title modal-title text-center ">
                        <span class="g-modal-header-icon fa fa-tasks"></span> 
                        <span class="g-modal-header-title-text" ng-hide="TaskSelected.Task.id">Make a new task</span>
                        <span class=$rootScope.tags"g-modal-header-title-text" ng-show="TaskSelected.Task.id">Edit Task</span>
                    </h4>
                </div>
                <!--        BODY          -->
                <div class="g-modal-body modal-body">
                    <div class="new-task-cover container-fluid">
                        <!--    task title      -->
                        <div class="new-task-title-area row">
                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 padding-lr-0">
                                <p class="new-task-icon-cover p-margin-0 font-big-6 center-block"><i class="fa fa-tasks" ></i></p>
                            </div>
                            <div class="new-task-textarea-cover col-lg-9 col-md-9 col-sm-9 col-xs-9" >
                                <div class="form-group">
                                    <textarea class='form-control'  ng-model="TaskSelected.Task.title" name="data[Task][title]" placeholder="Write your task here" rows='3' required  ng-maxlength=500 maxlength=500 autofocus></textarea>
                                    <div class="form-inline-error text-warning" ng-show="taskAddForm['data[Task][title]'].$dirty">
                                        <p ng-show="taskAddForm['data[Task][title]'].$error.required" class="p-margin-0">Please write the task</p>
                                        <p ng-show="taskAddForm['data[Task][title]'].$error.maxlegth" class="p-margin-0">Maximum 500 characters are allowed</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--    share with input        -->
                        <div class="new-task-share-ppl row">
                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 padding-lr-0">
                                <p class="new-task-share-icon-cover p-margin-0 font-big-2 center-block "><i class="fa fa-users" ></i></p>
                            </div>
                            <div class="new-task-textarea-cover col-lg-9 col-md-9 col-sm-9 col-xs-9" >
                                <div class="form-group">
                                    <tags-input ng-model="tags" add-from-autocomplete-only="true"  display-property="name" placeholder="Share with">
                                        <auto-complete source="taskLoadTags($query)"></auto-complete>
                                    </tags-input>
                                </div>
                            </div>
                        </div>
                        <!-- date and time  -->
                        <div class="new-task-date-area row">
                            <div class="col-lg-9 col-lg-offset-3 col-md-9 col-md-offset-3 col-sm-9 col-sm-offset-3 col-xs-9 col-xs-offset-3">
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 padding-lr-0">
                                    <!--    start date      -->
                                    <p id="select_start_date" class="btn-link p-margin-0"><i class="fa fa-calendar" ></i> Change the date </p>
                                    <input class="form-control input-sm" id="datetimepicker_start" type="text" ng-model="TaskSelected.Task.start_date" placeholder="Start Date" name="start_date" style='opacity:0;height: 1px;' value="TaskSelected.Task.start_date">
                                    <p class="p-margin-0 text-warning font-small-1"><span ng-bind="dateFormat(TaskSelected.Task.start_date,'MMM d, h:mm a')" ></span></p>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 padding-lr-0">
                                    <!--    end date      -->
                                    <span ng-show="TaskSelected.Task.start_date">
                                        <p id="select_end_date"  class="btn-link p-margin-0" ng-show="TaskSelected.Task.start_date"><i class="fa fa-calendar" ></i> Select end date</p>
                                        <input ng-show="TaskSelected.Task.start_date" class="form-control input-sm" type="text" datechange="" ng-model="TaskSelected.Task.end_date" placeholder="End Date" name="end_date" id="datetimepicker_end" style='opacity:0; height: 1px;' value="TaskSelected.Task.end_date" >
                                        <p  class="p-margin-0 text-warning font-small-1"><span ng-bind="dateFormat(TaskSelected.Task.end_date,'MMM d, h:mm a')" ></span></p>
                                    </span>

                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                    <div class="checkbox pull-right" style="">
                                        <label class="center-block">
                                            <input type="checkbox" ng-model="TaskSelected.Task.priority" name="data[Task][priority]">Important
                                        </label>
                                    </div>
                                </div>

                            </div> 
                        </div>
                        <div class="row" style="margin-top:20px;">
                            <div class="col-lg-4 col-lg-offset-8 col-md-offset-8 col-md-4 col-sm-4 col-sm-offset-8 col-xs-4 col-xs-offset-8">
                                <input type="submit" name="submit" class="btn btn-success btn-sm btn-block" value="Save Task" data-loading-text="Loading..." type="submit" ng-click=" TaskSelected.TaskAssignment.assignee_data = tags;" ng-disabled="taskAddForm.$invalid" style='z-index: -1;' />                               </div>
                        </div>
                    </div>
                </div>
                <div class="new-task-people-area modal-footer gridle-modal-footer">
                    <div class="container-fluid">
                        <!-- new task       -->
                        <div class="row" ng-hide="TaskSelected.Task.id">
                            <div class="col-lg-4 col-lg-offset-4 col-md-4 col-md-offset-4 col-sm-4 col-sm-offset-4 col-xs-4 col-xs-offset-4  padding-lr-0" >
                                <g-profile-img type="user" myclass='new-task-user-img font-big-2 center-block' height='40px' width='40px' path="{{users[getUserIndex(loggeduser)].UserInfo.vga}}" name="{{users[getUserIndex(loggeduser)].UserInfo.name}}" ng-if="users[getUserIndex(loggeduser)].UserInfo.name" ></g-profile-img>
                                <p class="text-center p-margin-0" ng-bind="users[getUserIndex(loggeduser)].UserInfo.first_name" ></p>
                                <p class="font-small-1 text-muted text-center p-margin-0">Created by</p>
                            </div>
                        </div>
                        <!--    if edit task       -->
                        <div ng-if="TaskSelected.Task.id" class="new-task-people-area row"  >
                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 padding-lr-0">
                                <!--    if user -->
                                <span>
                                    <g-profile-img type="user" myclass="new-task-user-img font-big-2 center-block" height='40px' width='40px' path="{{ users[getUserIndex(TaskSelected.Task.created_by)].UserInfo.vga}}" name="{{users[getUserIndex(TaskSelected.Task.created_by)].UserInfo.name}}" ></g-profile-img>
                                    <p class="text-center p-margin-0" ng-bind="users[getUserIndex(TaskSelected.Task.created_by)].UserInfo.first_name"></p>
                                    <p class='font-small-1 text-muted text-center p-margin-0'>Created by</p>

                                </span>
                            </div>

                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 padding-lr-0" ng-repeat="tags in TaskSelected.Tags" data-type="{{tags.type}}" ng-show='tags.role == "user"'>
                                <!--    if user -->
                                <span ng-show='tags.type == "user"'>
                                    <g-profile-img type="user" myclass="new-task-user-img font-big-2 center-block" height='40px' width='40px' path="{{ users[getUserIndex(tags.value)].UserInfo.vga}}" name="{{users[getUserIndex(tags.value)].UserInfo.name}}" ></g-profile-img>

                                    <p class="text-center p-margin-0" ng-bind="users[getUserIndex(tags.value)].UserInfo.first_name"></p>
                                    <p class='font-small-1 text-muted text-center p-margin-0'>Shared</p>
                                    <!--    if existing user is group   -->
                                    <p class="text-center btn-link font-small-1" ng-click="taskRemoveAssigneeConfirm(tags, TaskSelected.Task.id)" ng-show="isAssigee(TaskSelected.Task.id, loggeduser, 'admin')">Remove</p>
                                </span>
                                <!--    if group    -->
                                <span ng-if="tags.type == 'group'">
                                    <g-profile-img type="group" myclass="new-task-user-img font-big-2 center-block" height='50px' width='50px' name="{{groups[findGroup(tags.value)].Group.name}}" ></g-profile-img>
                                    <p class="text-center p-margin-0" ng-bind="groups[findGroup(tags.value)].Group.name"></p>
                                    <p class="font-small-1 text-muted text-center p-margin-0">Shared</p>
                                    <!--    if existing user    -->
                                    <p class="text-center btn-link font-small-1" ng-show="isAssigee(TaskSelected.Task.id, loggeduser, 'admin')"  ng-click="taskRemoveAssigneeConfirm(tags, TaskSelected.Task.id)">Remove</p>
                                </span>                                
                            </div>

                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<?php
//http://xdsoft.net/jqplugins/datetimepicker/#trigger
//echo $this->Html->script('datetimepicker/jquery-datetimepicker');
//echo $this->Html->css('jquery.datetimepicker');
?>           
<script>
    function getDate(datum) {
    fn = datum.split("-");
            return fn[0] + '/' + fn[1] + '/' + fn[2];
    }
    //start date calendar initialization
    jQuery('#datetimepicker_start').datetimepicker({
    format: 'm/d/Y h:i A',
            //formatTime: 'g:i A',
            closeOnDateSelect: true,
            //minDate: '0',
            /*onShow: function(ct) {
             this.setOptions({
             maxDate: jQuery('#datetimepicker_end').val() ? jQuery('#datetimepicker_end').val() : false
             });
             }*/
            /*onChangeDateTime: function(dp, $input) {
             //alert($input.val());
             jQuery('#datetimepicker_end').datetimepicker().setOptions({
             minDate: '2'
             });
             }*/
    });
            //end date selection calendar initialization
            jQuery('#datetimepicker_end').datetimepicker({

            format: 'm/d/Y h:i A',
            //formatTime: 'g:i A',
            closeOnDateSelect: true,
            /*onShow: function(ct) {
             this.setOptions({
             minDate: jQuery('#datetimepicker_start').val() ? jQuery('#datetimepicker_start').val() : false
             });
             }*/
    });
            //opening the calendars for start date
            jQuery('#select_start_date').click(function() {
    jQuery('#datetimepicker_start').datetimepicker('show'); //support hide,show and destroy command
    });
            //opening the calendar for end date
            jQuery('#select_end_date').click(function() {
    jQuery('#datetimepicker_end').datetimepicker('show'); //support hide,show and destroy command
    });


</script>
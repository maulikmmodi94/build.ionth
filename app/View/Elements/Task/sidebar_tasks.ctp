<div class="sidebarTasks container-fluid hidden-xs" ng-controller="sideTasksCtrl" ng-hide="hideRightBar" ng-cloak ng-class="{'top-on-tour':tourStep == 2,'sidebarAnimation':!hideRightBar && !$root.tourOn}" >
    <!--DROP DOWN--> 
    <div class="pending-tasks row" ng-show="tasksType == 'Pending'">
        <!--pending meta data--> 
        <div class="container-fluid pending-meta-box">
            <div class="row ">
                <div class="pending-meta col-sm-12 padding-lr-0">
                    <div class="col-sm-3 padding-right-0">
                        <p class="pull-left color-red text-center font-big-5 text-bold">
                            <span ng-bind="TaskData.Pending.count"></span>
                        </p>
                    </div>
                    <div class="col-sm-8 padding-left-0">
                        <div class="">
                            <p class="pull-left text-bold">Tasks Pending &nbsp;</p>

                        </div>
                        <span class="clearfix"></span>
                        <p class="text-muted font-small-2" ng-show="TaskData.Pending.length == 0">Awesome, ninja! Enjoy the rest of your day.</p>
                        <p class="text-muted font-small-2" ng-show="TaskData.Pending.length != 0">Complete these tasks, they are past the deadlines.</p>
                    </div>
                    <div class="col-sm-1 padding-lr-0">
                        <li class="dropdown" dropdown>
                            <p class="dropdown-toggle text-bold line-height-16 cursor-pointer" dropdown-toggle>

                                <span class="fa fa-caret-down"></span>
                            </p>
                            <ul class="dropdown-menu dropdown-animation dropdown-menu-right" role="menu">
                                <li ng-show="tasksType !== 'Pending'" ng-click="changeTaskType('Pending')"><a href="#">Pending Tasks</a></li>
                                <li ng-show="tasksType !== 'Completed'" ng-click="changeTaskType('Completed')"><a href="#">Completed Tasks</a></li>
                            </ul>
                        </li>
                    </div>
                </div>        
            </div>
        </div>
        <!--listing of tasks--> 
        <div class="container-fluid pending-tasks-list">

            <!--SINGLE TASK--> 
            <div class="singleTask container-fluid" ng-repeat="task in TaskData.Pending">
                <!--Task detail--> 
                <div class="taskDetails row">
                    <!--creator--> 
                    <div class="col-sm-2 padding-right-0">
                        <g-profile-img type="user" myclass='g-img-30' path="{{task.user_info.vga}}" name="{{task.user_info.name}}" ng-if="task.user_info"></g-profile-img>
                    </div>
                    <div class="col-sm-8 padding-lr-0">
                        <p class="line-height-16 color-black-alpha">
                            <!--name--> 
                            <span class="text-bold" ng-bind="task.user_info.name" ng-click="showProfileofUser(task.user_info)" style="cursor:pointer"></span>,
                            <!--deadline--> 

                            <span class="text-dim font-small-2 text-italic" >    
                                <span class="" ng-bind="momentDate(task.Task.end_date)" ></span>,
                                <span class="" ng-bind="dateFormat(task.Task.end_date, 'shortTime')"></span>
                            </span>
                        </p>
                        <!--task title--> 
                        <p class="task-title" ng-bind="task.Task.title"></p>
                    </div>
                    <div class="col-sm-offset-1 col-sm-1 padding-lr-0 ">
                        <p class="pull-right icon font-big-5 color-red icon-basic-bookmark" ng-show="task.Task.priority" style="margin-top: -15px" ></p>
                    </div>
                    <div class="col-sm-1 padding-lr-0">
                        <!--task settings-->
                        <li ng-show="task.Task.created_by == loggeduser" class="dropdown pull-right text-center" dropdown>
                            <a href="#"  class="dropdown-toggle cursor-pointer icon icon2-arrows-down color-black-alpha " dropdown-toggle>

                            </a>
                            <ul class="dropdown-menu dropdown-animation task-settings-menu dropdown-menu-right" role="menu">
                                <li class="container-fluid" ng-click="newTaskModal(task, 'pending')">
                                    <a href="#" class="row">
                                        <div class="col-sm-2 padding-lr-0" style="padding-top:5px;">
                                            <span class="icon icon-basic-gear font-big-2" style=""></span>
                                        </div>
                                        <div class="col-sm-10 padding-tb-5">
                                            <p class="" >
                                                <span>Edit task</span>
                                            </p>    
                                        </div>
                                    </a> 
                                </li>
                                <li role="separator" class="divider"></li>
                                <li class="container-fluid" ng-click="taskDeleteModal(task.Task.id, 'pending')">
                                    <a href="#" class="row">
                                        <div class="col-sm-2 padding-lr-0" style="padding-top:5px;">
                                            <span class="icon icon-basic-trashcan font-big-2" style=""></span>
                                        </div>
                                        <div class="col-sm-10 padding-tb-5">
                                            <p class="" >
                                                <span>Delete Task</span>
                                            </p>    
                                        </div>
                                    </a>
                                </li>

                            </ul>
                        </li>
                    </div>
                    <span class="clearfix"></span>
                    <div class="col-sm-offset-2 col-sm-8">
                        <!--<hr class="margin-tb-0"/>-->
                    </div>
                    <div class="col-sm-2" style="position: relative;">
                        <div ng-click="markTaskComplete(task.Task.id, task.Task.is_completed, 'pending')" class="btn-mark-complete pull-right icon icon2-arrows-check" >

                        </div>
                    </div>
                    <span class="clearfix"></span>

                    <div class='col-sm-12 padding-left-0 taskBottomBox'>
                        <!--task shared with-->
                        <div class="col-sm-8 col-sm-offset-2 padding-left-0">
                            <div class="task-shared-with padding-tb-10 " >
                                <p class="font-small-2 btn-link cursor-pointer shared-with text-dim" ng-hide="tag.user_id == task.Task.created_by" ng-repeat="tag in task.Tags" >
                                    <span ng-bind="tag.name" popover-placement="top" popover-template="profiletemplate.templateUrl"  tabindex="0" popover-trigger="focus" popover-animation="true" class="no-border" style="line-height: 1;"></span>
                                </p>
                            </div>

                        </div>
                    </div>
                </div>
                <!--task comments--> 
                <div class="row">
                    <comment-form id="{{task.Task.id}}" add="addComment(id,body,'pending')" delete="deleteCommentModal(id,commentId,'pending')" comments="task.Comment" loadmore="{{task.moreComments}}" load="deskService.getComment(id,lastId)" commentcount=task.Task.comment_count limitcount="2"></comment-form>
                </div>

            </div>
            <p class="text-center"><a href="#" ng-click="loadMorePendingTask()" ng-show="loadMorePending">Load more</a></p>
        </div>
    </div>
    <!--COMPLETED TASKS-->
    <div class="completed-tasks row" ng-show="tasksType == 'Completed'">
        <!--completed meta data--> 
        <div class="container-fluid completed-meta-box">
            <div class="row ">
                <div class="completed-meta col-sm-12 padding-lr-0">
                    <div class="col-sm-3 padding-right-0">
                        <p class="color-green text-center font-big-5 text-bold"><span ng-bind="TaskData.Completed.count"></span></p>
                    </div>
                    <div class="col-sm-8 padding-left-0">
                        <p class="text-bold">Tasks Completed</p>
                        <p class="text-muted font-small-2">All Tasks marked complete, are pinned here</p>
                    </div>
                    <div class="col-sm-1 padding-lr-0">
                        <li class="dropdown" dropdown>
                            <p class="dropdown-toggle text-bold line-height-16 cursor-pointer" dropdown-toggle>

                                <span class="fa fa-caret-down"></span>
                            </p>
                            <ul class="dropdown-menu dropdown-animation dropdown-menu-right" role="menu">
                                <li ng-show="tasksType !== 'Pending'" ng-click="changeTaskType('Pending')"><a href="#">Pending Tasks</a></li>
                                <li ng-show="tasksType !== 'Completed'" ng-click="changeTaskType('Completed')"><a href="#">Completed Tasks</a></li>
                            </ul>
                        </li>
                    </div>
                </div>        
            </div>
        </div>
        <!--listing of tasks--> 
        <div class="container-fluid pending-tasks-list text-muted">
            <!--SINGLE TASK--> 
            <div class="singleTask container-fluid" ng-repeat="task in TaskData.Completed">
                <!--Task detail--> 
                <div class="taskDetails row">
                    <!--creator--> 
                    <div class="col-sm-2 padding-right-0">
                        <g-profile-img type="user" myclass='g-img-30' path="{{task.user_info.vga}}" name="{{task.user_info.name}}" ng-if="task.user_info"></g-profile-img>
                    </div>
                    <div class="col-sm-7 padding-lr-0">
                        <p class="line-height-16 color-black-alpha">
                            <!--name--> 
                            <span class="text-bold" ng-bind="task.user_info.name" ng-click="showProfileofUser(task.user_info)" style="cursor:pointer"></span>,
                            <!--deadline--> 
                            <span class="text-dim font-small-2 text-italic" >    
                                <span class="" ng-bind="momentDate(task.Task.end_date)" ></span>,
                                <span class="" ng-bind="dateFormat(task.Task.end_date, 'shortTime')"></span>
                            </span>

                        </p>
                        <p class="task-title" ng-bind="task.Task.title"></p>
                    </div>
                    <div class="col-sm-offset-1 col-sm-1 padding-lr-0 ">
                        <p class="pull-right icon font-big-5 color-red icon-basic-bookmark" ng-show="task.Task.priority" style="margin-top: -15px" ></p>
                    </div>
                    <div class="col-sm-1">
                        <!--task settings-->
                        <li ng-show="task.Task.created_by == loggeduser" class="dropdown text-center" dropdown>
                            <a href="#"  class="dropdown-toggle cursor-pointer icon icon2-arrows-down color-black-alpha " dropdown-toggle>

                            </a>
                            <ul class="dropdown-menu dropdown-animation task-settings-menu dropdown-menu-right" role="menu">
                                <li class="container-fluid" ng-click="newTaskModal(task, 'completed')">
                                    <a href="#" class="row">
                                        <div class="col-sm-2 padding-lr-0" style="padding-top:5px;">
                                            <span class="icon icon-basic-gear font-big-2" style=""></span>
                                        </div>
                                        <div class="col-sm-10 padding-tb-5">
                                            <p class="" >
                                                <span>Edit task</span>
                                            </p>    
                                        </div>
                                    </a> 
                                </li>
                                <li role="separator" class="divider"></li>
                                <li class="container-fluid" ng-click="taskDeleteModal(task.Task.id, 'completed')">
                                    <a href="#" class="row">
                                        <div class="col-sm-2 padding-lr-0" style="padding-top:5px;">
                                            <span class="icon icon-basic-trashcan font-big-2" style=""></span>
                                        </div>
                                        <div class="col-sm-10 padding-tb-5">
                                            <p class="" >
                                                <span>Delete Task</span>
                                            </p>    
                                        </div>
                                    </a>
                                </li>

                            </ul>
                        </li>
                    </div>
                    <span class="clearfix"></span>
                    <div class="col-sm-offset-2 col-sm-8">
                        <!--<hr class="margin-tb-0"/>-->
                    </div>
                    <div class="col-sm-2" style="position: relative;">
                        <span ng-click="markTaskComplete(task.Task.id, task.Task.is_completed, 'search')" class="btn-mark-complete pull-right icon icon2-arrows-check" ng-hide="task.Task.is_completed" tooltip="Mark Task as Complete" tooltip-placement="top">
                        </span>
                        <span ng-click="markTaskComplete(task.Task.id, task.Task.is_completed, 'search')" class="btn-mark-complete pull-right icon icon2-arrows-check" ng-show="task.Task.is_completed" tooltip="Mark Task as Incomplete" tooltip-placement="top">
                        </span>
                    </div>
                    <span class="clearfix"></span>

                    <div class='col-sm-12 padding-left-0 taskBottomBox'>
                        <!--task shared with-->
                        <div class="col-sm-8 col-sm-offset-2 padding-left-0">                            <div class="task-shared-with line-height-40 " >
                                <p class="font-small-2 btn-link cursor-pointer shared-with text-dim"  popover="{{tag.name}}" ng-hide="tag.user_id == task.Task.created_by" ng-repeat="tag in task.Tags" >
                                    <span ng-bind="tag.name"></span> 
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <!--task comments--> 
                <div class="row">
                    <comment-form id="{{task.Task.id}}" add="addComment(id,body,'completed')" delete="deleteCommentModal(id,commentId,'completed')" comments="task.Comment" loadmore="{{task.moreComments}}" load="deskService.getComment(id,lastId)" commentcount=task.Task.comment_count limitcount="2"></comment-form>
                </div>
            </div>
            <input type="button" class="btn btn-link" ng-click="loadMoreCompletedTask()" ng-show="loadMoreComplete" value="Load More">
        </div>


    </div>

</div>
<!--        MODAL WINDOW FOR NEW TEAM          -->
<div id='groupModalCreate' class="modal fade">
    <div class="g-modal modal-dialog" >
        <!--        CONTENT          -->
        <div class="g-modal-content modal-content">
            <!--        HEADER          -->
            <div class="g-modal-header modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="g-modal-title modal-title ">
                    <span class="g-modal-header-icon fa fa-group"></span> 
                    <span class="g-modal-header-title-text">Make a team</span>
                </h4>
            </div>
            <!--        BODY          -->
            <div class="g-modal-body modal-body">
                <div class="g-modal-sm-body">
                    <p class="g-modal-sm-body-heading">Gridle works best with teams..</p>
                    <form id="addgroupform" class="g-modal-sm-body-form input-group form-inline" ng-submit="groupCreate(input);
                            input = null">
                        <div class="input-group">
                            <input id="groupname" type="text" class="form-control" name="groupname"  placeholder="Give name to your team" maxlength="30" required ng-model="input.Group.name">
                            <span class="input-group-btn" id="creategrpbtnwrap">
                                <button type="submit"  data-loading-text="Creating..." id="creategroupbtn" class="btn btn-success btn-green-addon">Create</button>
                            </span>
                        </div>
                    </form>    
                </div>
            </div>
        </div>
    </div>
</div>

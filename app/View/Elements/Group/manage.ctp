<!--    MANAGING GROUP MODAL WINDOW         -->
<div id='groupManageModal' class="modal fade">
    <div class="g-modal modal-dialog">
        <!--    CONTENT         -->
        <div class="g-modal-content modal-content">
            <!--    HEADER         -->
            <div class="g-modal-header modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="g-modal-title modal-title" >
                    <span class="g-modal-header-icon fa fa-group" ></span>
                    <span class="g-modal-header-title-text" ng-if="groups[groupCurrentIndex].Group.admin != loggeduser" ng-bind='groups[groupCurrentIndex].Group.name' ></span>
                    <span ng-if="groups[groupCurrentIndex].Group.admin == loggeduser">
                        <!--    rename of group -->
                        <input ng-model="groups[groupCurrentIndex].Group.name" ng-disabled="!editMode" ng-pattern="/^[\w\d ]+/" ng-class="{'form-control': editMode, 'g-inline-edit': !editMode}" >
                                <!--<span ng-if="groups[groupCurrentIndex].Group.admin == loggeduser" >-->
                        <span ng-show="!editMode" ng-click="oldName = groups[groupCurrentIndex].Group.name; editMode = !editMode;" ><span class='fa fa-pencil' title="Rename team" tooltip ></span></span>
                        <button class='btn btn-sm btn-default' ng-show="editMode" ng-click='editMode = !editMode;
                                groups[groupCurrentIndex].Group.name = oldName'>Cancel</button>
                        <button class="btn btn-primary btn-sm"  ng-show="editMode" ng-click="editMode = !editMode;
                                groupEditBtn(groups[groupCurrentIndex].Group.id, groups[groupCurrentIndex].Group.name, oldName)">Save</button>
                    </span>
                </h4>
            </div>
            <!--    BODY        -->
            <div class="g-modal-body modal-body" >
                <p id="addmembermodalnotify" class="modalnotify"></p>
                <!--    ADD MEMBER FORMS        -->
                <div class="g-modal-add-member-area pull-left" ng-if="groups[groupCurrentIndex].Group.admin == loggeduser">
                    <p class="g-link-add-member btn-link" ng-init="groupAddMoreExpand = false" ng-click="groupAddMoreExpand = !groupAddMoreExpand"><span class="fa fa-plus"></span>&nbsp;&nbsp;Add more members</p>

                    <div id="admemberinput" ng-show="groupAddMoreExpand" style="position: relative;" ng-if="loggeduser == groups[groupCurrentIndex].Group.admin">
                        <form id="addmemberform" class="g-modal-form" ng-submit="groupAddMembers(groups[groupCurrentIndex].Group.id, data)" role="form">
                            <div style="position: relative;">
                                <tags-input classs="form-control" ng-model="tags" add-from-autocomplete-only="true"  display-property="name" min-length="1" placeholder="Add people to {{groups[groupCurrentIndex].Group.name}}">
                                    <auto-complete source="groupTagsAutocompleteLoad(groups[groupCurrentIndex].Group.id)"></auto-complete>
                                </tags-input>

                                <p id="addmembererrors" class="colorred"></p>
                                <button id="addmemberbtn" class="btn btn-success btn-sm pull-right" type="submit" ng-click="data.userlist = tags"  data-loading-text="Adding...">Add member</button>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="clearfix"></div>
                <!--    GROUP USERS        -->
                <div class="g-manage-grp-usr-area g-scrollbar ">
                    <div ng-class="(user.role=='admin' ? 'g-manage-grp-admin':'g-manage-grp-usr')" ng-repeat="user in groups[groupCurrentIndex]['UsersGroup']" >
                        <g-profile-img type="user" myclass="g-manage-grp-user-img" width="25px" height="25px" path="{{users[findUser(user.user_id)]['UserInfo']['vga']}}" name="{{users[findUser(user.user_id)]['UserInfo']['name']}}"></g-profile-img>
                        <p class="g-manage-grp-usr-name p-margin-0" ng-bind="users[findUser(user.user_id)]['UserInfo']['name']"></p>
                        <p class="g-manage-grp-usr-remove p-margin-0 pull-right" ng-show="(groups[groupCurrentIndex]['Group']['admin'] == loggeduser) && (user.role != 'admin')" ng-click="groupModalMemberRemove(groups[groupCurrentIndex]['Group']['id'], groups[groupCurrentIndex]['Group']['name'], user.user_id, users[findUser(user.user_id)]['UserInfo']['name'])" title="Remove {{users[findUser(user.user_id)]['UserInfo']['name']}} from {{groups[groupCurrentIndex]['Group']['name']}} " tooltip ><span class="font-big-1 fa fa-times-circle"></span></p>
                    </div>
                </div>
            </div>
            <!--    FOOTER         -->
            <div class="g-modal-footer modal-footer">
                <p class='g-modal-footer-btn pull-right' ng-if="groups[groupCurrentIndex]['Group']['admin'] == loggeduser" ng-click="groupModalDelete(groups[groupCurrentIndex]['Group']['id'], groups[groupCurrentIndex]['Group']['name'])"><span class="fa fa-trash-o"></span> Delete Team</p>
                <p class='g-modal-footer-btn pull-right' ng-if="groups[groupCurrentIndex]['Group']['admin'] != loggeduser" ng-click="groupModalLeave(groups[groupCurrentIndex]['Group']['id'], groups[groupCurrentIndex]['Group']['name'])"><span class="fa fa-sign-out"></span> Leave Team</p>    
            </div>
        </div>
    </div>
</div>

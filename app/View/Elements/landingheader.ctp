<?php

function select_header() {
    switch (@$_SERVER['REQUEST_URI']) {
        case '/login' : return 'focused';
        case '/users/login' : return 'focused';
        case '/signup' : return 'focused';
        case '/users/add' : return 'focused';
        case '/users/reset_password' : return 'focused';
        case '/users/freelancer_landing' : return 'focused';    
    };
}
?>
<!-- HEADER -->
<header id="g-header" class="navbar navbar-default navbar-fixed-top one-edge-shadow text-center container-fluid" role="navigation" >
    <div class="row">
        <!--    gridle logo -->
        <p class="g-logo-gridle-cover p-margin-0">
            <a id="g-logo-gridle" href="/">
                <object data="/img/logos/gridle_logo.svg" type="image/svg+xml">
                    <img src="/img/logos/gridle_logo.png" alt="gridle logo" />
                </object>
            </a>
        </p>



        <?php if (select_header() !== 'focused') { ?>
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#g-header-left">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>
            <!--  left navigation  -->
            <div id="g-header-left" class="navbar-collapse collapse"> 
                <ul class="navbar navbar-nav navbar-left">
                    <!--For teams--> 
                    <li class="g-header-dropdown dropdown" >
                        <a href="#" class="g-header-dropdown-btn dropdown-toggle btn-link " data-toggle="dropdown"> For Teams <b class="caret"></b> </a>
                        <ul class="g-header-dropdown-menu dropdown-menu">
                            <li><a class="g-header-dropdown-link" href="/pages/features">Features</a></li>
                            <li><a class="g-header-dropdown-link" href="/pages/benefits">Benefits</a></li>
                        </ul>
                    </li>
                    <li class='hidden-xs'><span>&nbsp;&nbsp;|&nbsp;&nbsp;</span></li>
                    <!--For enterprise--> 
                    <li class='g-header-dropdown dropdown'>
                        <a href="#" class="g-header-dropdown-btn dropdown-toggle" data-toggle="dropdown"> For Enterprises <b class="caret"></b></a>
                        <ul class="g-header-dropdown-menu dropdown-menu">
                            <li><a class="g-header-dropdown-link" href="/pages/benefits">Benefits</a></li>
                            <li><a class="g-header-dropdown-link" href="/pages/case-studies">Case studies</a></li>
                            <li><a class="g-header-dropdown-link" href="/pages/pricing">Pricing</a></li>
                            <li><a class="g-header-dropdown-link" href="/pages/support">Support</a></li>
                            <li><a class="g-header-dropdown-link" href="/pages/contact">Contact</a></li>
                        </ul>
                    </li>
                </ul>
                <?php if (!is_null(CakeSession::read('Auth.User.username'))) { ?>
                    <!--        if logged in            -->
                    <ul id="g-header-right" class="nav navbar-nav navbar-right">
                        <li><a href="/networks" class="btn-link" ><?php echo CakeSession::read('Auth.User.UserInfo.name'); ?></a></li>
                        <li><a href="/users/logout" ><button class="btn btn-default btn-sm">Log out</button></a></li>
                    </ul>
                <?php } else { ?>
                    <ul id="g-header-right" class="navbar navbar-nav navbar-right">
                        <!--        if logged out            -->
                        
                        <li><a href="/users/login" ><button class="btn btn-default btn-sm content_highlight" >Login</button></a></li>
                    </ul>
                <?php } ?>

            </div>
        <?php } ?>
        <?php if (select_header() == 'focused') { ?>
            <div id="g-header-right" class="header_nav pull-right"> 
                <div class="g-header-dropdown dropdown pull-left">
                    <a href="#" class="g-header-dropdown-btn dropdown-toggle" data-toggle="dropdown"><button class="btn btn-default"><i class="fa fa-bars"></i></button></a>
                    <ul class="g-header-dropdown-menu dropdown-menu dropdown-menu-right">
                        <li><a class="g-header-dropdown-link" href="/pages/home">Home</a></li>
                        <li class="divider"></li>
                        <li><a class="g-header-dropdown-link"  href="/users/login">Login</a></li>
                        <li class="divider"></li>
                        <li><a class="g-header-dropdown-link" href="/pages/pricing">Pricing</a></li>
                        <li><a class="g-header-dropdown-link" href="/pages/support">Support</a></li>
                        <li><a class="g-header-dropdown-link" href="/pages/contact">Contact</a></li>
                    </ul>
                </div>
            </div>
        <?php } ?>
    </div>
</header>
<?php echo $this->Html->script('bootstrap/collapse.js', false); ?>
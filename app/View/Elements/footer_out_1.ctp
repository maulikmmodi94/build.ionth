<div id="" class="g-footer-out container-fluid">
    <div class="row">
        <div class="footer-section col-lg-3 col-xs-6">
            <p class="footer-section-heading">
                GRIDLE MORE
            </p>
            <p>
                <a class="footer-section-links" href="/pages/features">Features</a><br/>
                <a class="footer-section-links" href="/pages/benefits">Benefits</a><br/>
                <!--<a class="footer-section-links" href="">Clients</a><br/>-->
                <!--<a class="footer-section-links" href="">Investors</a><br/>-->
                <a class="footer-section-links" href="/pages/press">Press</a><br/>
            </p>
            <p class="footer-section-heading" style="margin-top: 20px;">
                GRIDLE FOR EDUCATORS
            </p>
            <p>
                <a class="footer-section-links" href="/pages/education">Education</a><br/>
            </p>
        </div>
        <div class="footer-section col-lg-3 col-xs-6">
            <p class="footer-section-heading">
                GETTING STARTED
            </p>
            <p>
                <a class="footer-section-links" href="/pages/pricing">Pricing</a><br/>
                <!--<a class="footer-section-links" href="">Tutorial</a><br/>-->
                <a class="footer-section-links" href="/pages/terms">Policies</a><br/>
                <a class="footer-section-links" href="/pages/testimonial">Testimonials</a><br/>
            </p>
        </div>
        <div class="footer-section col-lg-3 col-xs-6">
            <p class="footer-section-heading">
                RESOURCES & SUPPORT
            </p>
            <p>
                <a class="footer-section-links" href="/pages/support">Support</a><br/>
                <!--<a class="footer-section-links" href="">Resources</a><br/>-->
                <!--<a class="footer-section-links" href="">White-paper</a><br/>-->
            </p>
        </div>
        <div class="footer-section col-lg-3 col-xs-6">
            <p class="footer-section-heading">
                CONTACT & INFO
            </p>
            <p>
                <a class="footer-section-links" href="/pages/blog">Blog</a><br/>
                <a class="footer-section-links" href="/pages/about">About</a><br/>
                <a class="footer-section-links" href="/pages/contact">Contact</a><br/>
                <a class="footer-section-links" href="https://www.facebook.com/gridle.in" target='_blank'>Facebok</a><br/>
                <a class="footer-section-links" href="https://twitter.com/gridle_in" target='_blank'>Twitter</a>
            </p>
        </div>
    </div>
</div>
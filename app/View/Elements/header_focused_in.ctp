<!-- HEADER -->
<header id="g-header" class="navbar navbar-default navbar-fixed-top one-edge-shadow text-center container-fluid" role="navigation" ng-controller="navigationController">
    <div class="g-header-wrap row">
        <!--    gridle logo -->
        <p class="g-logo-gridle-cover p-margin-0">
            <a id="g-logo-gridle" href="/networks">
                <!--<object data="/img/logos/gridle_logo.svg" type="image/svg+xml">-->
                <img src="/img/logos/gridle_logo.png" alt="gridle logo" />
                <!--</object>-->
            </a>

        </p>
        <?php //debug($AuthUser); ?>

        <!--  left navigation  -->
        <div id="g-header-left" class="pull-left"> 
            <!--    All networks menu       -->
            <ul class="nav navbar-nav">
                <!--<li class="g-header-dropdown dropdown">-->
                <!--<a href="#" class="network-name-c g-header-dropdown-btn dropdown-toggle"  data-toggle="dropdown">{{networks[currentnetwork].name}}<b class="caret"></b></a>-->
                <!--<ul class="dropdown-menu" role="menu">-->
                <!--<li ng-repeat="network in networks"><a class="network-name-c g-header-dropdown-link" href="/networks/switchNetwork/{{network.url}}">{{network.name}}</a>-->
                <!--</li>-->

                <!--<li class="divider"></li>-->
                <!--<li><a class="g-header-dropdown-link" href="/networks">Networks</a></li>-->
                <!--</ul>-->
                <!--</li>-->
                <li class=''><a href='#'>What's new?</a></li>
            </ul>
        </div>

        <!--    right navigation -->
        <div id="g-header-right" class="pull-right">
            <ul class="nav navbar-nav">
                <li class='g-header-dropdown dropdown'>
                    <a href='#' class='g-header-dropdown-btn dropdown-toggle text-capitalize' data-toggle="dropdown"><img class='header-user-img' src="userInSession.vga" height="25px" width="25px"> <span class="caret"></span> </a>
                    <ul class='dropdown-menu-right dropdown-menu' role='menu'>
                        <li class='dropdown-header text-capitalize'>
                            <h4 class="text-muted">
                                {{userInSession.name}}&nbsp;&nbsp;
                                <g-profile-img type='user' path="userInSession.vga" height="25px" width="25px" myclass="header-user-img" name="userInSession.name" ></g-profile-img>
                            </h4>

                        </li>
                        <li class='divider'></li>
                        <li><a href='/users/edit'><span class='fa fa-gear'></span> Settings &amp; Profile</a></li>
                        <li><a href='/users/logout'><span class='fa fa-power-off'></span> Log out</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</header>
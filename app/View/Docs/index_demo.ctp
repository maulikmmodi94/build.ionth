<?php
$this->layout = 'layout_in_demo';
$this->set('title_for_layout', 'Docs');
?>
<div class="container-fluid" ng-controller="docsCtrl">
    <div class="row">
        <div class="col-sm-8">
            <div class="container-fluid">
                <!--PAGE TITLE--> 
                <div class="row padding-tb-15" ng-init="showSearch=false">
                    <div class="col-sm-11">
                        <p class="" ng-hide="showSearch">
                            <span class="color-blue">Showing all files</span>
                        </p>
                        <input ng-show="showSearch" placeholder="Search a file by name" class="form-control pull-right" />
                        
                    </div>
                    <div class="col-sm-1 padding-lr-0">
                        <p ng-hide="showSearch" class="line-height-30 cursor-pointer" ng-click="showSearch = true"><span class=" fa fa-search"></span></p>
                        <p ng-show="showSearch" class="line-height-30 cursor-pointer" ng-click="showSearch = false"><span class=" fa fa-times"></span></p>
                    </div>
                </div>
                <!--FILES LISTING--> 
                <div class="row">
                    <div class="container-fluid files-listing-head">
                        <div class="row text-center files-head">
                            <div class="col-sm-8 files-head-cell" >
                                <p class="text-bold text-muted text-left" style="padding-left: 15px;"><span class="btn-link">File name</span> <span class="fa fa-chevron-up btn-link font-small-2"></span> <span class="fa fa-chevron-down btn-link font-small-2"></span></p>
                            </div>
                            <div class="col-sm-2 padding-lr-0 files-head-cell">
                                <p class="text-bold text-muted">Owner</p>
                            </div>
                            <div class="col-sm-2 padding-lr-0 files-head-cell">
                                <p class="text-bold text-muted"><span class="btn-link">Size</span> <span class="fa fa-chevron-up btn-link font-small-2"></span> <span class="fa fa-chevron-down btn-link font-small-2"></span></p>
                            </div>
                        </div>
                    </div>
                    <div class="container-fluid files-listing text-center">
                        <!--single file--> 
                        <div class="row singleFileRow">
                            <div class="col-sm-8 file-list-cell">
                                <p class="pull-left">
                                    <span class="fa fa-file-o text-muted fa-2x"></span>
                                </p>
                                <p class="pull-left text-left file-name">
                                    <span>Gridle_report.docx</span><br/>
                                    <span class="text-muted font-small-2">1st July, 2015</span>
                                </p>
                            </div>
                            <div class="col-sm-2 padding-lr-0 file-list-cell">
                                <p class="text-bold text-muted">
                                    <img src="/img/demo/anupama.png" class="round-25" /> 
                                </p>
                            </div>
                            <div class="col-sm-2 padding-lr-0 file-list-cell">
                                <p class="text-muted font-small-1 line-height-30">25 MB</p>
                            </div>
                        </div>
                        <!--single task end--> 
                        
                        <!--single file--> 
                        <div class="row singleFileRow active">
                            <div class="col-sm-8 file-list-cell">
                                <p class="pull-left">
                                    <span class="fa fa-file-o text-muted fa-2x"></span>
                                </p>
                                <p class="pull-left text-left file-name">
                                    <span>Gridle_report.docx</span><br/>
                                    <span class="text-muted font-small-2">1st July, 2015</span>
                                </p>
                            </div>
                            <div class="col-sm-2 padding-lr-0 file-list-cell">
                                <p class="text-bold text-muted">
                                    <img src="/img/demo/anupama.png" class="round-25" /> 
                                </p>
                            </div>
                            <div class="col-sm-2 padding-lr-0 file-list-cell">
                                <p class="text-muted font-small-1 line-height-30">25 MB</p>
                            </div>
                        </div>
                        <!--single task end-->
                        <!--single file--> 
                        <div class="row singleFileRow">
                            <div class="col-sm-8 file-list-cell">
                                <p class="pull-left">
                                    <span class="fa fa-file-o text-muted fa-2x"></span>
                                </p>
                                <p class="pull-left text-left file-name">
                                    <span>Gridle_report.docx</span><br/>
                                    <span class="text-muted font-small-2">1st July, 2015</span>
                                </p>
                            </div>
                            <div class="col-sm-2 padding-lr-0 file-list-cell">
                                <p class="text-bold text-muted">
                                    <img src="/img/demo/anupama.png" class="round-25" /> 
                                </p>
                            </div>
                            <div class="col-sm-2 padding-lr-0 file-list-cell">
                                <p class="text-muted font-small-1 line-height-30">25 MB</p>
                            </div>
                        </div>
                        <!--single task end-->
                        <!--single file--> 
                        <div class="row singleFileRow">
                            <div class="col-sm-8 file-list-cell">
                                <p class="pull-left">
                                    <span class="fa fa-file-o text-muted fa-2x"></span>
                                </p>
                                <p class="pull-left text-left file-name">
                                    <span>Gridle_report.docx</span><br/>
                                    <span class="text-muted font-small-2">1st July, 2015</span>
                                </p>
                            </div>
                            <div class="col-sm-2 padding-lr-0 file-list-cell">
                                <p class="text-bold text-muted">
                                    <img src="/img/demo/anupama.png" class="round-25" /> 
                                </p>
                            </div>
                            <div class="col-sm-2 padding-lr-0 file-list-cell">
                                <p class="text-muted font-small-1 line-height-30">25 MB</p>
                            </div>
                        </div>
                        <!--single task end-->
                        <!--single file--> 
                        <div class="row singleFileRow">
                            <div class="col-sm-8 file-list-cell">
                                <p class="pull-left">
                                    <span class="fa fa-file-o text-muted fa-2x"></span>
                                </p>
                                <p class="pull-left text-left file-name">
                                    <span>Gridle_report.docx</span><br/>
                                    <span class="text-muted font-small-2">1st July, 2015</span>
                                </p>
                            </div>
                            <div class="col-sm-2 padding-lr-0 file-list-cell">
                                <p class="text-bold text-muted">
                                    <img src="/img/demo/anupama.png" class="round-25" /> 
                                </p>
                            </div>
                            <div class="col-sm-2 padding-lr-0 file-list-cell">
                                <p class="text-muted font-small-1 line-height-30">25 MB</p>
                            </div>
                        </div>
                        <!--single task end-->
                        <!--single file--> 
                        <div class="row singleFileRow">
                            <div class="col-sm-8 file-list-cell">
                                <p class="pull-left">
                                    <span class="fa fa-file-o text-muted fa-2x"></span>
                                </p>
                                <p class="pull-left text-left file-name">
                                    <span>Gridle_report.docx</span><br/>
                                    <span class="text-muted font-small-2">1st July, 2015</span>
                                </p>
                            </div>
                            <div class="col-sm-2 padding-lr-0 file-list-cell">
                                <p class="text-bold text-muted">
                                    <img src="/img/demo/anupama.png" class="round-25" /> 
                                </p>
                            </div>
                            <div class="col-sm-2 padding-lr-0 file-list-cell">
                                <p class="text-muted font-small-1 line-height-30">25 MB</p>
                            </div>
                        </div>
                        <!--single task end-->
                        <!--single file--> 
                        <div class="row singleFileRow">
                            <div class="col-sm-8 file-list-cell">
                                <p class="pull-left">
                                    <span class="fa fa-file-o text-muted fa-2x"></span>
                                </p>
                                <p class="pull-left text-left file-name">
                                    <span>Gridle_report.docx</span><br/>
                                    <span class="text-muted font-small-2">1st July, 2015</span>
                                </p>
                            </div>
                            <div class="col-sm-2 padding-lr-0 file-list-cell">
                                <p class="text-bold text-muted">
                                    <img src="/img/demo/anupama.png" class="round-25" /> 
                                </p>
                            </div>
                            <div class="col-sm-2 padding-lr-0 file-list-cell">
                                <p class="text-muted font-small-1 line-height-30">25 MB</p>
                            </div>
                        </div>
                        <!--single task end-->
                        <!--single file--> 
                        <div class="row singleFileRow">
                            <div class="col-sm-8 file-list-cell">
                                <p class="pull-left">
                                    <span class="fa fa-file-o text-muted fa-2x"></span>
                                </p>
                                <p class="pull-left text-left file-name">
                                    <span>Gridle_report.docx</span><br/>
                                    <span class="text-muted font-small-2">1st July, 2015</span>
                                </p>
                            </div>
                            <div class="col-sm-2 padding-lr-0 file-list-cell">
                                <p class="text-bold text-muted">
                                    <img src="/img/demo/anupama.png" class="round-25" /> 
                                </p>
                            </div>
                            <div class="col-sm-2 padding-lr-0 file-list-cell">
                                <p class="text-muted font-small-1 line-height-30">25 MB</p>
                            </div>
                        </div>
                        <!--single task end-->
                        <!--single file--> 
                        <div class="row singleFileRow">
                            <div class="col-sm-8 file-list-cell">
                                <p class="pull-left">
                                    <span class="fa fa-file-o text-muted fa-2x"></span>
                                </p>
                                <p class="pull-left text-left file-name">
                                    <span>Gridle_report.docx</span><br/>
                                    <span class="text-muted font-small-2">1st July, 2015</span>
                                </p>
                            </div>
                            <div class="col-sm-2 padding-lr-0 file-list-cell">
                                <p class="text-bold text-muted">
                                    <img src="/img/demo/anupama.png" class="round-25" /> 
                                </p>
                            </div>
                            <div class="col-sm-2 padding-lr-0 file-list-cell">
                                <p class="text-muted font-small-1 line-height-30">25 MB</p>
                            </div>
                        </div>
                        <!--single task end-->
                        <!--single file--> 
                        <div class="row singleFileRow">
                            <div class="col-sm-8 file-list-cell">
                                <p class="pull-left">
                                    <span class="fa fa-file-o text-muted fa-2x"></span>
                                </p>
                                <p class="pull-left text-left file-name">
                                    <span>Gridle_report.docx</span><br/>
                                    <span class="text-muted font-small-2">1st July, 2015</span>
                                </p>
                            </div>
                            <div class="col-sm-2 padding-lr-0 file-list-cell">
                                <p class="text-bold text-muted">
                                    <img src="/img/demo/anupama.png" class="round-25" /> 
                                </p>
                            </div>
                            <div class="col-sm-2 padding-lr-0 file-list-cell">
                                <p class="text-muted font-small-1 line-height-30">25 MB</p>
                            </div>
                        </div>
                        <!--single task end-->
                        
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-4">
            <?php echo $this->element('Doc/doc_details') ?>
        </div>
    </div>

</div>

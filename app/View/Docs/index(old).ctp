<?php

$this->layout = 'gridle_in';
$this->set('title_for_layout', 'Documents');
?>
<div ng-controller="fileController" class='container-fluid' >
    <div class='g-docs-wrap row'>
        <!--    USER GROUP AREA      -->
        <div class="g-docs-usr-grp-area col-lg-3 col-lg-offset-1 col-md-3 col-md-offset-1 col-sm-4 col-xs-4">
            <!--    ME      -->
            <p class="g-docs-me" ng-if=" users.length > 0" ng-click="fileInitFilterItem();
                        selectItem('user', loggeduser);">
            <g-profile-img type='user' myclass='g-desk-user-img' path="{{users[findUser(loggeduser)]['UserInfo']['vga']}}" width="50px" height="50px" name="{{users[findUser(loggeduser)]['UserInfo']['name']}}" ></g-profile-img>
                <!--<img class='g-desk-user-img' ng-src="{{users[findUser(loggeduser)]['UserInfo']['vga']}}" height="50px" width="50px" style="">-->
            </p>    
            <!--    TABS      -->
            <div class="g-doc-usr-grp-sel"  ng-init="selectedType = 'user';">
                <p><btn class="text-muted" ng-click="selectedType = 'user'" ng-class="{'btn-link' : selectedType != 'user'}">People</btn>
                <span>&nbsp;|&nbsp;</span>
                <btn class="text-muted" ng-click="selectedType = 'group'" ng-class="{'btn-link' : selectedType != 'group'}" >Groups</btn></p>
            </div>
            <!--    SEARCH      -->
            <div class="g-docs-usr-grp-search">
                <input class="form-control input-sm g-search-box" placeholder="Search group or user..." type="text" ng-model="searchInput">
            </div>  
           

            <!--    Users List      -->
            <div class="g-doc-ppl-list g-scrollbar" ng-show="selectedType == 'user'">
                 <div class='container-fluid' ng-if="users.length<2">
                <div class='row center-block'>
                    <object data='/img/empty-templates/files_user_empty.svg' width="100%"></object>
                </div>
            </div>
                <div class="g-doc-ppl-tile" ng-repeat="user in users| UsersRemoveLogged:loggeduser | dataSearchFilter: searchInput :selectedType" ng-click="fileSelectFilterItem('user', user.UserInfo.user_id); selectItem('user', user.UserInfo.user_id);" 
                                ng-class="{'g-background-yellow': user.UserInfo.user_id == selectItem['user']['UserInfo.user_id'] }"
                                >
                    
                    <p class="">
                    <g-profile-img type="user" myclass="g-doc-ppl-img" path="{{user.UserInfo.vga}}" name="{{user.UserInfo.name}}" height="25px" width="25px"></g-profile-img>
                    <span class="g-doc-ppl-name" ng-bind="user.UserInfo.name"  ></span>
                    </p>    
                </div>
            </div>
            <!--        group list          -->
            <div class="g-doc-ppl-list g-scrollbar" ng-show="selectedType == 'group'">               
                <div ng-repeat="group in groups| dataSearchFilter: searchInput : selectedType" ng-click="fileSelectFilterItem('group', group.Group.id);
                                selectItem('group', group.Group.id);">
                    <div class="g-doc-ppl-tile" >
                        <p>
                        <g-profile-img type="group" myclass="g-doc-ppl-img" height="25px" width="25px" name="{{group.Group.name}}" ></g-profile-img>
                        <span class="g-doc-ppl-name" ng-bind="group.Group.name" ></span>
                        </p>
                    </div>
                </div>
            </div>        
        </div>
        <!--        FILES AREA          -->
        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
            <div class="g-docs-area container-fluid">
                <!--        FILE OPTIONS            -->
                <div class="g-docs-options row">
                    <div >
                        <div class="g-docs-upload col-lg-2 col-md-2 col-sm-3 col-xs-4">
                            <button id="upload_button" ng-if="files.length" class="btn btn-success btn-sm btn-block" ng-click="fileModalUpload();"><span class="fa fa-upload" ></span> &nbsp;Upload</button></div>
                    </div>
                    <div class="g-docs-search col-lg-5 col-lg-offset-1 col-md-5 col-md-offset-1 col-sm-6 col-xs-6">
                        <!--<span ng-bind=showFilesRelation>All your files</span>-->
                        <input class="form-control input-sm g-search-box" placeholder="Search File..." type="text" ng-model="searchFileInput.Doc.name">
                    </div>
                    <div class="g-docs-filter col-lg-2 col-lg-offset-2 col-md-2 col-md-offset-2">
                        <!--<p><span>Files</span> | <span>Images</span></p>-->
                    </div>
                </div>
                <!--        DOCUMENT LIST           -->
                <div class="g-docs-list container-fluid" ng-if="files.length">
                    <!--    DOCUMENT LIST SORTING        -->
                    <div class="g-doc-list-options row text-muted">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                            <p class="btn-link p-margin-0" ng-click="reverse = !reverse;
                                        predicate = 'Doc.name';">Name&nbsp;&nbsp; <span ng-show="!reverse && predicate == 'Doc.name'" ><i class='fa fa-chevron-down'></i></span> <span ng-show="reverse && predicate == 'Doc.name'"><i class='fa fa-chevron-up'></i></span></p>
                        </div>
                        <div class='col-lg-2 col-md-2 col-sm-2 col-xs-2'>
                            <p class="p-margin-0 text-emphasize" >Owner</p>
                        </div>
                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
                            <p class="btn-link p-margin-0" ng-click="reverse = !reverse;
                                        predicate = 'Doc.file_size';" >Size &nbsp;&nbsp; <span ng-show="!reverse && predicate == 'Doc.file_size'" ><i class='fa fa-chevron-down'></i></span> <span ng-show="reverse && predicate == 'Doc.file_size'"><i class='fa fa-chevron-up'></i></span></p>
                        </div>
                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
                            <p class="btn-link p-margin-0" ng-click="reverse = !reverse;
                                        predicate = 'Doc.created';">Created&nbsp;&nbsp; <span ng-show="!reverse && predicate == 'Doc.created'" ><i class='fa fa-chevron-down'></i></span> <span ng-show="reverse && predicate == 'Doc.created'"><i class='fa fa-chevron-up'></i></span></p>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <!--        DOCUMENT LISTING            -->
                    <div class="g-doc-tile-list panel-group g-scrollbar row" id="accordian" ng-init="predicate = 'Doc.created';
                                    reverse = true">
                        <!--Framebench Modal and Functions-->
                        <div frame-bench></div>
                        <!--        DOCUMENT TILE       -->
                        <div class="g-doc-tile panel panel-default" ng-repeat="file in files| fileDisplayFilter: fileFilterItem.type : fileFilterItem.value | filter:searchFileInput | orderBy:predicate:reverse" ng-click="fileSelectItem(file.Doc.id)">

                            <!--    DOCUMENT SUMMARY VIEW       -->                            
                            <div class="g-doc-tile-summary panel-heading container-fluid" data-toggle="collapse" data-parent="#accordian" href="#collapse{{$index}}">
                                <div class="g-doc-tile-summary-div row">
                                    <!--        DOCUMENT NAME       -->
                                    <div class='g-doc-tile-name col-lg-6 col-md-6 col-sm-6 col-xs-6'>
                                        <!--    DOCUMENT ICON       -->
                                        <p class='g-doc-icon-cover text-center p-margin-0 pull-left'  ng-switch="fileType(file.Doc.file_mime, files)">
                                            <span class="g-doc-icon fa fa-file-image-o" ng-switch-when="image"></span>
                                            <span class="g-doc-icon fa fa-file-audio-o" ng-switch-when="audio"></span>
                                            <span class="g-doc-icon fa fa-file-movie-o" ng-switch-when="video"></span>
                                            <span class="g-doc-icon fa fa-file-text-o" ng-switch-when="text"></span>
                                            <span class="g-doc-icon fa fa-file" ng-switch-when="other"></span>
                                        </p>
                                        <p class='p-margin-0'><span class="g-doc-name" ng-bind="file.Doc.name"></span>&nbsp;</p>
                                    </div>
                                    <!--        DOCUMENT OWNER       -->
                                    <div class="g-doc-tile-owner col-lg-2 col-md-2 col-sm-2 col-xs-2">
                                        <p>
                                        <g-profile-img type="user" myclass="g-doc-owner-img" path="{{users[findUser(file.Doc.user_id)]['UserInfo']['vga']}}" name="{{users[findUser(file.Doc.user_id)]['UserInfo']['name']}}" width="20px" height="20px" title="{{users[findUser(file.Doc.user_id)]['UserInfo']['name']}}" data-placement="top" tooltip ></g-profile-img>
                                        </p>
                                    </div>
                                    <!--        DOCUMENT SIZE       -->
                                    <div class='g-doc-tile-size col-lg-2 col-md-2 col-sm-2 col-xs-2'>
                                        <p class='p-margin-0' ng-bind="file.Doc.file_size | bytesConvert">

                                        </p>
                                    </div>
                                    <!--        DOCUMENT UPLOADED       -->
                                    <div class='g-doc-tile-date col-lg-2 col-md-2 col-sm-2 col-xs-2' >
                                        <p class='p-margin-0' ng-bind="dateFormat(file.Doc.created, 'mediumDate')" title="{{dateFormat(file.Doc.created, 'shortTime')}}"></p>
                                    </div>

                                </div>
                            </div>
                            <!--    DOCUMENT EXPANSION DATA       -->
                            <div id="collapse{{$index}}" class="g-doc-tile-collapse container-fluid padding-lr-0 panel-collapse collapse">
                                <div class="g-doc-collapse-body panel-body row">
                                    <div class=' g-doc-option-block col-xs-4 font-big-1'>
                                        <!--File viewer--> 
                                        <span class=" btn-file-option " title="View doc" data-placement="top" tooltip  ng-click="fileService.setPreviewUrl(file.Doc)" ng-if="fileService.frameBenchMimes(file.Doc.file_mime)"><span class="fa fa-eye" ></span></span>

                                        <a href="{{file.Doc.preview_url}}" class="btn-file-option" title="View doc" data-placement="top" tooltip target="_blank"  ng-if="file.Doc.preview_url && file.Doc.provider === 'dropbox'"><span class="fa fa-eye" ></span></a>            

                                        <!--    Document download       -->

                                        <a href="/Docs/download/{{file.Doc.id}}" class="btn-file-option" title="Download" data-placement="top" tooltip ><span class="fa fa-download" ></span></a>                                       
                                        <!--Share and edit document-->   
                                        <span  ng-click="fileModalOpenManage()" class="btn-file-option" title="Share and edit" data-placement="top" tooltip ><span class="fa fa-gear" ></span></span>




                                    </div>
                                    <div class=" g-doc-option-block col-xs-1 padding-lr-0">
                                        <!--Comment count--> 
                                        <span ng-if="file.Doc.comment_count" class="text-muted" title="Comment count" data-placement="top" tooltip ><span class="fa fa-comments-o" ></span> {{file.Doc.comment_count}}</span>
                                    </div>
                                    <!--    DOCUMENT SHARING INFORMATION            -->                                    
                                    <div class='g-doc-shared-block col-lg-6 col-md-6 col-sm-5 col-xs-5'>
                                        <div ng-if='file.DocAssignment.length > 1'>
                                            <!--<div class='col-lg-3 col-md-3'>-->
                                            <p class='doc-share-title text-muted p-margin-0 font-small-1 text-right'>Shared with <span ng-bind="file.DocAssignment.length - 1" ></span></p>
                                            <!--</div>-->
                                            <div class='doc-share-usr-tile pull-right ' ng-repeat='user in file.DocAssignment'>
                                                <!--    if group        -->
                                                <div class='doc-share-group-info' ng-if='user.group_id !== null'>
                                                    <p class='p-margin-0 font-small-1' ng-bind="groups[findGroup(user.group_id)]['Group']['name']"></p>
                                                </div>
                                                <!--    if user        -->
                                                <div class='doc-share-user-info'  ng-if='user.user_id !== null && user.user_id != file.Doc.user_id' ng-class="{'doc-share-owner':user.user_id == file.Doc.user_id}">
                                                    <p class='doc-share-user-name p-margin-0 font-small-1'>
                                                    <g-profile-img type="user" myclass="doc-share-user-img" path="{{users[findUser(user.user_id)]['UserInfo']['vga']}}" name="{{users[findUser(user.user_id)]['UserInfo']['name']}}" width="20px" height="20px" title="{{users[findUser(user.user_id)]['UserInfo']['name']}}" tooltip ></g-profile-img>
                                                </div>
                                            </div>    
                                            <a href="../../webroot/img/empty-templates/files_empty.svg"></a>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="col-xs-12 padding-lr-0">
                                        <!--Comments-->
                                        <comment-form id="{{file.Doc.id}}" add="fileService.addComment(id,body)" delete="fileService.deleteCommentModal(id,commentId)" comments=file.Comment loadmore="{{file.moreComments}}" load="fileService.getComment(id,lastId)" commentcount=file.Doc.comment_count></comment-form>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <!--    empty template  -->
                <div class='container-fluid' ng-if="!files.length">
                    <div class='row center-block'>
                        <object data='/img/empty-templates/file.svg' ng-if="!files.length" width="100%"></object>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

<?php
echo $this->element('Doc/manage');
echo $this->element('Doc/upload');
?>
<script>
    function openuploadmodel(evt) {
        $('#fileUploadModal').modal('show');
    }
</script>
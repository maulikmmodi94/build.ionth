<?php
$this->layout = 'layout_in';
$this->set('title_for_layout', 'Files');
?>
<div class="container-fluid" ng-controller="docsController">
    <div class="row">
        <!--Loader--> 
        <div id="loader-wrapper" class="col-sm-12" ng-show="loadingFiles && loadingUsers && loadingGroups">
            <div id="loader"></div>
            <div class="loading-text text-bold font-big-6 color-black-alpha-high">Loading..</div>
        </div>
        <div class="desk-content" ng-class="{
                'col-sm-8 col-sm-offset-2'
                :hideRightBar && !showNotifications, 'col-sm-8 desk-expanded' : !hideRightBar || showNotifications}" ng-cloak>
            <div class="container-fluid">
                <!--PAGE TITLE--> 
                <div class="row upload-area">
                    <!--file upload--> 
                    <div class="col-sm-6 upload-section upload-section-first cursor-pointer" ngf-select="true" ngf-multiple="true" ngf-change="filterUploadEvent($files, $file, $event, $rejectedFiles)" tooltip-placement="bottom">
                        <!--COMPUTER UPLOAD--> 
                        <div class="upload-animate">
                            <!--upload icon-->  
                            <p class="icon icon-basic-server-upload font-big-6 upload-icon pull-left"></p>
                            <!--upload computer-->
                            <p class="upload-text pull-right" >Upload from computer</p>
                        </div>
                    </div>
                    <!--DROPBOX UPLOAD-->
                    <div class="col-sm-6 upload-section upload-section-last cursor-pointer" ng-click="DropboxUpload()">
                        <div class="upload-animate">
                            <!--upload icon--> 
                            <p class="padding-tb-10 upload-icon pull-left">
                                <img src="/img/logos/vendor/dropbox.png" height="33"  />
                            </p>
                            <p class="upload-text pull-right">Upload from dropbox</p>
                        </div>

                    </div>

                </div>
                <!--filter bar--> 
                <div class=" row" ng-show="fileFilterOn">
                    <div class="bg-blue color-white filter-bar" ng-show="isSelectedUser">
                        <p>
                            <!--<g-profile-img type="user" path="{{selectedUser.vga}}" name="{{selectedUser.name}}"></g-profile-img>--> 
                            Showing files with <span class="text-bold">{{selectedUser.name}}</span> 
                            <span class="pull-right text-bold cursor-pointer" ng-click="clearFilter()">&times;</span>
                        </p>
                    </div>
                    <div class="bg-blue color-white filter-bar" ng-hide="isSelectedUser">
                        <p>Showing files with <span class="text-bold">{{selectedGroup.name}}</span> <span class="pull-right text-bold cursor-pointer" ng-click="clearFilter()">&times;</span></p>
                    </div>
                </div>

                <div class="row">
                    <!--LIST HEADING--> 
                    <div class="container-fluid files-listing-head">
                        <div class="row text-center files-head">
                            <!--file name heading--> 
                            <div class="col-sm-8 files-head-cell file-name-head" >
                                <p class="text-left" style="padding-left: 15px;">
                                    <span class="btn-link">File name</span> 
                                    <span class="fa fa-chevron-up btn-link font-small-2" tooltip-placement="top" tooltip="Alphabetically Ascending Order" ng-click="sortFiles('name0')"></span> 
                                    <span class="fa fa-chevron-down btn-link font-small-2" tooltip-placement="top" tooltip="Alphabetically Descending Order" ng-click="sortFiles('name1')"></span>
                                </p>
                            </div>
                            <!--File owner--> 
                            <div class="col-sm-2 padding-lr-0 files-head-cell file-owner-head">
                                <p class="text-dim">Owner</p>
                            </div>
                            <!--File size--> 
                            <div class="col-sm-2 padding-lr-0 files-head-cell file-size-head">
                                <p class=""><span class="btn-link">Size</span> <span class="fa fa-chevron-up btn-link font-small-2" tooltip-placement="top" tooltip="Ascending" ng-click="sortFiles('size0')"></span> <span class="fa fa-chevron-down btn-link font-small-2" tooltip-placement="top" tooltip="Descendingicon-ba" ng-click="sortFiles('size1')"></span></p>
                            </div>
                        </div>
                    </div>
                    <!--LISTING OF FILES-->
                    <div class="container-fluid files-listing text-center">

                        <!--SEARCH RESULT--> 
                        <!--single file--> 
                        <div class="row singleFileRow" ng-hide="showFiles || selectedFile == null">
                            <div class="col-sm-8 file-list-cell">
                                <!--file icon--> 
                                <p class="pull-left">
                                    <span class="fa fa-file-o fa-2x color-black-alpha-lower "></span>
                                </p>
                                <p class="pull-left text-left file-name">
                                    <!--document name-->
                                    <span ng-bind="selectedFile.Doc.name"></span>.<span ng-bind="selectedFile.Doc.file_extension"></span>

                                </p>
                                <p class="pull-left">
                                    <!--uploaded time--> 
                                    <span class="text-muted font-small-2" ng-bind="dateFormat(selectedFile.Doc.created, 'medium')"></span>
                                </p>
                                <span class="fa fa-trash-o color-black-alpha-low-lower " ng-click="fileDeleteModal(selectedFile.Doc.id)" ng-show="checkforAdmin(file.Doc.user_id)"></span>
                            </div>
                            <div class="col-sm-2 padding-lr-0 file-list-cell">
                                <p class="text-muted">
                                <g-profile-img ng-if="selectedFile" type="user" myclass="g-doc-owner-img" path="selectedFile.Doc.UserInfo.vga" name="selectedFile.Doc.UserInfo.name" width="20px" height="20px" popover="{{selectedFile.Doc.UserInfo.name}}" popover-trigger="mouseenter" popover-placement="left"></g-profile-img>
                                </p>
                            </div>
                            <div class="col-sm-2 padding-lr-0 file-list-cell">
                                <p class="text-muted font-small-1 line-height-30"><span ng-bind="selectedFile.Doc.file_size | bytesConvert"></span></p>
                            </div>

                        </div>
                        <!--FILES LISTING--> 
                        <div ng-show="showFiles">
                            <!--ng-repet if files starts here -->
                            <div class="row singleFileRow" ng-repeat="file in files" ng-class="{
                                    'active'
                                    : (selectedFile.Doc.id == file.Doc.id)}">
                                <div ng-click="selectFile(file)" >
                                    <div class="col-sm-9 file-list-cell">
                                        <!--file icon--> 
                                        <p class="pull-left padding-top-5">
                                            <img src="/img/icons/file_icon.png" height="20" />
                                            <!--<span class="fa fa-file-o color-black-alpha-lower  fa-2x"></span>-->
                                        </p>
                                        <!--file name--> 
                                        <p class="pull-left text-left file-name text-bold line-height-30 font-small-1 " title="{{file.Doc.name}}">
                                            <span ng-bind="file.Doc.name"></span>.<span ng-bind="file.Doc.file_extension"></span>,
                                        </p>
                                        <p class="pull-left line-height-30 margin-left-5">
                                            <span class="text-muted font-small-3" ng-bind="dateFormat(file.Doc.created, 'longDate')"></span>
                                        </p>
                                        <!--file size--> 
                                        <!--<span class="fa fa-trash-o" ng-click="fileDeleteModal(file.Doc.id)" ng-show="checkforAdmin(file.Doc.user_id)"></span>-->
                                    </div>
                                    <div class="col-sm-1 padding-lr-0 file-list-cell">
                                        <p class="text-muted">
                                        <g-profile-img type="user" myclass="g-doc-owner-img" path="file.Doc.UserInfo.vga" name="file.Doc.UserInfo.name" width="20px" height="20px" popover="{{file.Doc.UserInfo.name}}" popover-trigger="mouseenter" popover-placement="left"></g-profile-img>
                                        </p>
                                    </div>
                                    <div class="col-sm-2 padding-lr-0 file-list-cell">
                                        <p class="text-muted font-small-1 line-height-30"><span ng-bind="file.Doc.file_size | bytesConvert"></span></p>
                                    </div>
                                </div>
                            </div>
                            <!--single task end-->
                            <p ng-show="loadMoreFiles" class="text-center font-small-1 padding-tb-20" ng-click="loadMoreFile()">
                                <a href="#" >Load more files</a>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-4">
            <ng-include src="'/scripts/View/docs/doc_details.html'"></ng-include>
        </div>
    </div>
    <!--hidden uploader indicator--> 
    <div class="uploader-status cursor-pointer" ng-show="uploadingAnimate" ng-click="openUploadFileModal()">
        <div class="uploading-icon text-center pull-left">
            <p class="timer-loader"></p>
        </div>
        <div class="uploading-msg pull-left margin-left-10">
            <p class="text-bold" style="line-height: 50px;" >Uploading files..</p>
        </div>
    </div>

</div>
<script type="text/javascript" src="https://www.dropbox.com/static/api/2/dropins.js" id="dropboxjs" data-app-key="aed1zkp5s3lw0dp" async></script>
<style>
    body{
        overflow: hidden;
    }
</style>
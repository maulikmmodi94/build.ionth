<?php
$this->layout = 'gridle_in';
$this->set('title_for_layout', 'Docs');
?>
<div ng-controller="fileController" class='container-fluid'>
    <div class='file-page-wrap row' ng-if="files.length">        
        <!--        FILE INFOMRAITON           -->
        <div class="file-info-section container-fluid">
            <div class='g-file-info row'>
                <!--    file image     -->
                <div class="g-file-img-cover col-xs-2 ">
                    <p class='g-file-icon-cover text-center p-margin-0 pull-right'  ng-switch="fileType(files[0].Doc.file_mime)">
                        <span class="g-file-icon fa fa-file-image-o" ng-switch-when="image"></span>
                        <span class="g-file-icon fa fa-file-audio-o" ng-switch-when="audio"></span>
                        <span class="g-file-icon fa fa-file-movie-o" ng-switch-when="video"></span>
                        <span class="g-file-icon fa fa-file-text-o" ng-switch-when="text"></span>
                        <span class="g-file-icon fa fa-file" ng-switch-when="other"></span>
                    </p>
                </div>
                <!--    file name      -->
                <div class='g-file-name col-xs-8'>
                    <p class='g-file-name-text font-big-2 text-semi-bold' >{{files[0].Doc.name}}.{{files[0].Doc.file_extension}}</p>
                    <p class='g-file-size'><span class='text-muted'>Size : </span>{{ files[0].Doc.file_size | bytesConvert}}</p>
                    <!--Uploaded on--> 
                    <p class='g-file-uploaded'>
                    <g-profile-img type="user" path="{{ users[findUser(files[0].Doc.user_id)].UserInfo.vga}}" name="{{ users[findUser(files[0].Doc.user_id)].UserInfo.name}}" width="20px" height="20px" myclass="g-task-user-img" ></g-profile-img>
                    <span>{{users[findUser(files[0].Doc.user_id)].UserInfo.name}}</span>
                    <span class='text-muted'>uploaded on </span> {{dateFormat(files[0].Doc.created, 'medium')}} 
                    </p>
                    <!--shared information--> 
                    <div>
                        <p ng-if="files[0].DocAssignment.length>1" class=''><span class='text-muted'>Shared with</span> {{ files[0].DocAssignment.length -1}}</p>
                        <div>
                            <div class="g-file-user-tile-cover pull-left" ng-repeat="user in files[0]['DocAssignment']">
                                <!--    if user         -->
                                <span ng-if='user.user_id && user.user_id!==files[0].Doc.user_id'>
                                    <g-profile-img type='user' path="{{ users[findUser(user.user_id)]['UserInfo']['vga']}}" name="{{ users[findUser(user.user_id)]['UserInfo']['name']}}" myclass="task-tile-ppl-user" width="30px" height="30px" title="{{ users[findUser(user.user_id)]['UserInfo']['name']}}" tooltip ></g-profile-img>
                                </span>
                                <p ng-if="user.group_id"  class="task-tile-ppl-grp p-margin-0  text-capitalize" title="Team :  {{groups[findGroup(user.group_id)].Group.name}}" ng-bind='groups[findGroup(user.group_id)].Group.name' tooltip data-placement="top" ></p>

                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <p class="g-file-settings text-muted font-big-1 p-margin-0" >
                        <!--comment count--> 
                        <span ng-if="files[0].Doc.comment_count">{{files[0].Doc.comment_count}} <i class="fa fa-comments-o"></i></span>
                        <!--view file--> 
                        <!--<span class=" btn-file-option " title="View doc" data-placement="top" tooltip ><span class="fa fa-eye" ></span></span>-->
                        <!--File viewer--> 
                        <span class=" btn-file-option " title="View doc" data-placement="top" tooltip  ng-click="fileService.setPreviewUrl(files[0].Doc)" ng-if="fileService.frameBenchMimes(files[0].Doc.file_mime)"><span class="fa fa-eye" ></span></span>
                        <!--download file--> 
                        <a href="/Docs/download/{{files[0].Doc.id}}" class="btn-file-option" title="Download" data-placement="top" tooltip ><span class="fa fa-download" ></span></a>
                        
                        <!--share and edit file--> 
                        <span class="btn-file-option text-muted" ng-click="fileModalOpenManage()" title="Share and edit" tooltip><i class="fa fa-gear"></i></span>
                    </p>
                </div>
            </div>
        </div>
        <comment-form id='{{files[0].Doc.id}}' add="fileService.addComment(id,body)" delete="fileService.deleteCommentModal(id,commentId)" comments=files[0].Comment loadmore="{{files[0].moreComments}}" load="fileService.getComment(id,lastId)" commentcount=files[0].Doc.comment_count></comment-form>
        <div class="file-members-section-footer">

        </div>
    </div>
    <?php
    echo $this->element('Doc/manage');
    ?>
    <!--Framebench Modal and Functions-->
                        <div frame-bench></div>
</div>
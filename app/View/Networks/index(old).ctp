<?php

$this->layout = 'layout_in';
$this->set('title_for_layout', 'Networks');
?>
<div class='container-fluid'  ng-controller="networkController" ng-init='getNotificationsUnseen();'>
    <div class='g-networks-wrap row'>
        <div class='col-lg-offset-1 col-lg-10 col-md-offset-1 col-md-10 col-sm-12 col-xs-12'>
            <div class="networks-options container-fluid">
                  <div class='row'>
                    <div class='col-lg-2 col-md-3 col-sm-3 col-xs-3 padding-lr-0'>
                        <a class='btn-new-network btn btn-success btn-block' href="/networks/add" ><span class='fa fa-plus'></span>&nbsp;&nbsp;New Network</a>
                    </div>
                </div>
                <!--<p class="pull-left text-muted p-margin-0"><strong>Select a network to proceed</strong></p>-->

<!--<p class='pull-right text-muted'>Current Network : <span class='network-name-c' >{{networks[currentnetwork].name}}</span></p>-->
            </div>


            <div class="row" ng-if="networks">

                <div class="network-tile-area col-lg-3 col-md-3 col-sm-3 col-xs-6" ng-repeat="network in networks| networkAlphabetOrder" >
                    <div class='network-tile-{{$index % 7}} network-tile text-center'>

                        <!--        NAME AND INFO           -->
                        <a href="/networks/switchNetwork/{{network.url}}/desks">
                            <div class='network-tile-img-area text-center'>
                                <div class="network-tile-bg">
                                    <p class='network-tile-name p-margin-0 font-big-1' ng-bind="network.name"></p>
                                </div>
                            </div>
                        </a>
                        <!--        STATS AND UPDATES           -->
                        <div class="container-fluid">
                            <div class="network-tile-section network-tile-stats row">
                                <!--    notifications   -->
                                <div class="network-stats-updates network-stats-icon col-lg-4 col-md-4 col-sm-4" ng-click="network.dropNotifications = !network.dropNotifications">
                                    <p class="p-margin-0"><span class="network-stats-numbers" ng-bind="filterednots.length"></span><br/><span class="text-muted">Updates</span></p>
                                </div>
                                <!--    Groups   -->
                                <div class="network-stats-group-count network-stats-icon col-lg-4 col-md-4 col-sm-4">
                                    <p class="p-margin-0"><span class="network-stats-numbers" ng-bind="network.group_count"></span><br/><span class="text-muted">groups</span></p>
                                </div>
                                <!--    Users   -->
                                <div class="network-stats-user-count network-stats-icon col-lg-4 col-md-4 col-sm-4">
                                    <p class="p-margin-0" ng-click="getOtherUserNetworksData(network.id); selectNetwork(network.id)" data-toggle="modal" data-target="#networkModalMembers" ><span class="network-stats-numbers"  ng-bind="network.user_count"></span><br/><span class="text-muted">Users</span></p>
                                </div>
                            </div>
                            <div class="network-tile-activity g-scrollbar row">                                
                                <div ng-show="network.dropNotifications" class="network-tile-activity-div font-small-1" ng-if="notifications" ng-repeat="nots in filterednots = (notifications|notificationByNetwork:network.id)">
                                    <p  ng-if="nots.network_id == network.id" ng-bind-html="nots.message"></p>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>


                <div class="network-tile-area col-lg-3 col-md-3 col-sm-3 col-xs-6" ng-repeat="network in networks_expired| networkAlphabetOrder">
                    <div class='network-tile-expired network-tile text-center'>

                        <!--        NAME AND INFO           -->
                        <a href="/pages/activate">
                            <div class='network-tile-img-area text-center'>
                                <div class="network-tile-bg">
                                    <p class='network-tile-name p-margin-0 font-big-1' ng-bind="network.name"></p>
                                </div>
                            </div>
                        </a>

                    </div>
                    <div class="network-tile-section network-tile-stats">
                        <p class="text-center text-muted p-margin-0">This network has been expired. Please <a href="/pages/activate">activate</a> it.</p>
                    </div>
                </div>
            </div>
            <!--    empty template  -->
            <div class='container-fluid' ng-if="networks.length<1">
                <div class='row center-block'>
                    <object data='/img/empty-templates/network_empty.svg'></object>
                </div>
            </div>


        </div>
    </div>

    <?php echo $this->element('Network/add'); ?>
    <?php echo $this->element('Network/members'); ?>
</div>

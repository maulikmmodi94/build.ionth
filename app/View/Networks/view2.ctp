<?php
$this->layout = 'gridle_in';
$this->set('title_for_layout', $Networks[$CurrentNetwork]["name"] . ' Dashboard');
?>
<div class="container-fluid" ng-controller="networkController">
    <div class="g-dashboard-wrap row">
        <div class="container-fluid">
            <div class="dashboard-network-info row">
                <div class="dashboard-network-name col-lg-2 col-lg-offset-1 col-md-2 col-md-offset-1 col-sm-3 col-xs-3 ">
                    <div class="col-sm-10 padding-lr-0">
                        <!--    network name        -->

                        <input class="ta-network-name form-control" ng-model="networks[currentnetwork].name" ng-disabled="!editMode" ng-pattern="/^[\w\d ]+/" >
                    </div>
                    <div class="col-sm-1 padding-lr-0">
                        <!--    EDIT NETWORK INFO      -->
                        <span ng-show="user_networks[findUserNetwork(loggeduser)].UserNetwork.role == 'owner'">
                            <button class="btn-link" ng-show="!editMode" ng-click="oldName = networks[currentnetwork].name;editMode = !editMode;" title="Rename Network" tooltip ><i class="fa fa-pencil"></i></button>
                        </span>
                    </div>
                    <div class="col-sm-10 padding-lr-0">
                    <button class='btn btn-sm btn-default' ng-show="editMode" ng-click='editMode = !editMode;networks[currentnetwork].name = oldName'>Cancel</button>
                    <button class="btn btn-primary btn-sm"  ng-show="editMode" ng-click="editMode = !editMode;editNetwork(networks[currentnetwork].name, editMode, oldName)">Save</button>
                </div>
                </div>
                <div class="dashboard-network-info-content col-lg-9 col-md-9 col-sm-9 col-xs-9">
                    <!--    NETOWRK NAME EDITABLE       -->
                    <div class="col-lg-8 col-md-6 col-sm-6 col-xs-6">

                    </div>
                    
                    <!--    INVITE TO NETWORKS      -->
                    <div class="col-lg-4 col-md-6 col-sm-6 col-xs-6">
                        &nbsp;&nbsp;
                        <button class="btn btn-success btn-sm" data-toggle="modal" data-target="#networkModalAddMembers" ng-show="user_networks[findUserNetwork(loggeduser)].UserNetwork.role == 'admin' || user_networks[findUserNetwork(loggeduser)].UserNetwork.role == 'owner'">Invite to network</button>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="row">
                <!--    dashboard menu  -->
                <div class="dashboard-menu col-lg-2 col-lg-offset-1 col-md-2 col-md-offset-1 col-sm-3 col-xs-3">
                    <div class="dashboard-menu-item active" data-target="#network-people">
                        <p class="p-margin-0">
                            <!-- if admin or owner  -->
                            <span ng-show="user_networks[findUserNetwork(loggeduser)].UserNetwork.role == 'admin' || user_networks[findUserNetwork(loggeduser)].UserNetwork.role == 'owner'" >Manage</span>
                            <!-- if not admin    -->
                            <span ng-show="user_networks[findUserNetwork(loggeduser)].UserNetwork.role == 'user'" >People</span>
                        </p>
                    </div>
                    <div class="dashboard-menu-item" data-target="#network-stats">
                        <p class="p-margin-0">Stats</p>
                    </div>

                    <div class="dashboard-menu-item" ng-if='user_networks[findUserNetwork(loggeduser)].UserNetwork.role === "owner"'>
                        <a href="/networks/security"><p class="p-margin-0">Access Control</p></a>
                    </div>
                    <div class="dashboard-menu-item" ng-if='user_networks[findUserNetwork(loggeduser)].UserNetwork.role === "owner"'>
                        <?php echo $this->Form->postLink(__('Delete Network'), array('action' => 'delete', $CurrentNetwork), array('class' => ''), __('Deleting a Network is permanent. Are you sure you want to delete  network "%s"? There is no undo.', $Networks[$CurrentNetwork]['name'])); ?>
                    </div>
                </div>
                <!--    dashboard area  -->
                <div class="dashboard-expands col-lg-9 col-md-9 col-sm-9 col-xs-9 ">
                    <!--    NETWORK PEOPLE LISTING      -->
                    <div id="network-people" class="network-people-area dashboard-content container-fluid">
                        <div class='network-people-search row'>
                            <div class='col-lg-4 col-lg-offset-4 col-md-6 col-md-offset-2 col-sm-6 col-xs-6'>
                                <div class='form-group'>
                                    <div class='input-group input-group-sm'>
                                        <input class='g-search-box form-control' type='text' placeholder='Search for people by name/role' ng-model="searchMember"/>
                                        <div class='input-group-addon-round input-group-addon'>
                                            <i class='fa fa-search'></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-2 col-lg-offset-2 col-md-2 col-md-offset-2 col-sm-6 col-xs-6 ">
                                <p class='text-muted'> <strong ng-bind="networks[currentnetwork]['user_count']"></strong>/ <strong ng-bind="networks[currentnetwork]['user_limit']"></strong> members</p>
                            </div>
                        </div>
                        <div class="network-people-listing row">
                            <div class="network-people-tile-cover col-lg-3 col-md-3 col-sm-4 col-xs-6" ng-repeat="user in user_networks| orderBy:['UserInfo.name'] | orderBy:orderByUserNetworkRole |filter:searchMember">
                                <!--    USER TILE       -->
                                <div class="network-people-tile">
                                    <div class='network-people-info' ng-class="{'network-owner-info': user.UserNetwork.role === 'owner'}">
                                        <!--    USER IMAGE      -->
                                        <div class="network-people-img-cover">
                                            <p class="p-margin-0">
                                            <g-profile-img type='user' path="{{user.UserInfo.vga}}" name="{{user.UserInfo.name}}" myclass="{{ user.UserNetwork.role === 'owner' ? 'network-owner-img network-ppl-img center-block font-big-3' : user.UserNetwork.role === 'admin' ? 'network-admin-img network-ppl-img center-block font-big-3' : 'network-ppl-img center-block font-big-3'   }}" width="64px" height="64px"  ></g-profile-img>
                                            </p>
                                        </div>
                                        <!--    USER NAME       -->
                                        <div class="network-people-name text-uppercase">
                                            <p class="p-margin-0 font-big-2 btn-link" ng-bind="user.UserInfo.name" ng-click="showUserInfoModal(user.UserInfo.user_id);"></p>
                                        </div>
                                        <!--    USER ROLE       -->
                                        <div class="network-people-role text-muted text-uppercase font-small-1">
                                            <p class=""> {{user.UserNetwork.role}} </p>
                                        </div>
                                    </div>
                                    <!--    OPTIONS -->
                                    <div class="network-people-options font-small-1" ng-show="(user_networks[findUserNetwork(loggeduser)].UserNetwork.role == 'admin' || user_networks[findUserNetwork(loggeduser)].UserNetwork.role == 'owner') && user.UserNetwork.role != 'owner'">
                                        <p class="p-margin-0" ng-if='user.UserNetwork.role !== "owner"'>
                                            <span class="btn-link" ng-show="(user_networks[findUserNetwork(loggeduser)].UserNetwork.role == 'owner') && user.UserNetwork.role == 'user'" ng-click="assignRole(user.UserInfo.user_id, 'admin')" >Make admin</span>
                                            <span class='btn-link' ng-show="(user_networks[findUserNetwork(loggeduser)].UserNetwork.role == 'owner') && user.UserNetwork.role == 'admin'" ng-click="assignRole(user.UserInfo.user_id, 'user')" >Remove admin</span>
                                        </p>
                                        <p class="p-margin-0" ng-if='user.UserNetwork.role !== "owner"'>
                                            <span class="btn-link" ng-show="((user_networks[findUserNetwork(loggeduser)].UserNetwork.role == 'admin' && user.UserNetwork.role != 'admin') || user_networks[findUserNetwork(loggeduser)].UserNetwork.role == 'owner') && user.UserNetwork.role != 'owner'" ng-click="removeUserNetworkConfirm(user.UserInfo.user_id)" >Remove from network</span>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>  
                    </div>
                    <!--    NETWORK STATS      -->
                    <div id="network-stats" class="network-stats-area dashboard-content container-fluid">
                        <div class='row'>
                            <!--    USER COUNT      -->
                            <div class='col-lg-4 col-md-4 col-sm-4 col-xs-12'>
                                <div class='dashboard-stats-count dashboard-stats-user-count '>
                                    <p class='dashboard-count-cover center-block' ng-bind="networks[currentnetwork]['user_count']"></p>
                                    <p class='text-muted font-small-1'>USERS</p>
                                </div>
                            </div>
                            <!--    GROUP COUNT      -->
                            <div class=' col-lg-4 col-md-4 col-sm-4 col-xs-12 '>
                                <div class='dashboard-stats-count dashboard-stats-group-count'>
                                    <p class='dashboard-count-cover center-block' ng-bind="networks[currentnetwork]['group_count']"></p>
                                    <p class='text-muted font-small-1'>GROUPS</p>
                                </div>
                            </div>
                            <!--    FILES COUNT      -->
                            <div class='col-lg-4 col-md-4 col-sm-4 col-xs-12'>
                                <div class='dashboard-stats-count dashboard-stats-file-count'>
                                    <p class='center-block dashboard-count-cover' ng-bind="networks[currentnetwork]['file_count'] | bytesConvert"></p>
                                    /<span ng-bind="networks[currentnetwork]['file_limit'] | bytesConvert"></span>
                                    <p class='text-muted font-small-1'>Space used</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php echo $this->element('Network/adduser'); ?>
<script>
    $('.dashboard-menu-item[data-target]').on('click', function ()
    {
        $('.dashboard-content').addClass('hide');
        var id = $(this).data('target');
        $(id).removeClass('hide');
        $('.dashboard-menu-item[data-target]').removeClass('active');
        $(this).addClass('active');


    });

    $('.dashboard-content').addClass('hide');
    $('#network-people').removeClass('hide');
    $('.dashboard-menu-item[data-target=#members]').addClass('active');
</script>
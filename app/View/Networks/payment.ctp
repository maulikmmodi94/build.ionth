<?php $this->layout = 'gridle_in'; ?>
<div class="container-fluid">
    <div class="row" style="padding:10%;">
        <div class="col-lg-4">
            <form action='https://www.2checkout.com/checkout/purchase' method='post' class="">
                <!--    required fields         -->
                <input type='hidden' name='sid' value='1303908' />
                <input type='hidden' name='mode' value='2CO' />
                <input type='hidden' name='li_0_type' value='product' />
                <input type='hidden' name='li_0_name' value='Monthly Subscription' />
                <div class="form-group">
                    <label>Price :</label> 
                    <input name='li_0_price' value='5.00'  class="form-control" />
                </div>
                <!--    Additional         -->
                <div class="form-group">
                    <label>Plan : </label> 
                    <input name="li_0_product_id" value="silver plan"  class="form-control" 
                </div>
                <!--    Submit         -->
                <div class="form-group" style="margin-top: 20px;">
                    <input class="btn btn-success" name='submit' type='submit' value='Pay Now' />    
                </div>

                <input type='hidden' name='li_0_recurrence' value='1 Month' />
                <input type='hidden' name='li_0_duration' value='1 Month' />

                <input type='hidden' name='demo' value='Y' />
                <input type='hidden' name='currency_code' value='USD' />
            </form>    
        </div>
    </div>
</div>

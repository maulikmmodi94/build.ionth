<?php $this->layout = 'gridle_in' ?>
<style>

    /*@import url(//fonts.googleapis.com/css?family=Lato:400,900);*/
    /*@import url(//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css);*/
    body {
        padding: 60px 0px;
    }
    .animate {
        -webkit-transition: all 0.3s ease-in-out;
        -moz-transition: all 0.3s ease-in-out;
        -o-transition: all 0.3s ease-in-out;
        -ms-transition: all 0.3s ease-in-out;
        transition: all 0.3s ease-in-out;
    }
    .user-thumbnail {
        width: 100%;
        border: 1px solid rgb(215, 215, 215);
        position: relative;
        font-family: 'Lato', sans-serif;
        margin-bottom: 20px;
        overflow: hidden;
    }
    .user-thumbnail > img {
        width: 100px;
        margin-bottom: 60px;
    }
    .user-thumbnail .user-thumbnail-details,
    .user-thumbnail .user-thumbnail-details .user-thumbnail-header  {
        width: 100%;
        height: 100%;
        position: absolute;
        bottom: -100%;
        left: 0;
        padding: 0 15px;
        background: rgb(255, 255, 255);
        text-align: center;

    }
    .user-thumbnail .user-thumbnail-details::-webkit-scrollbar {
        width: 8px;
    }
    .user-thumbnail .user-thumbnail-details::-webkit-scrollbar-button {
        width: 8px;
        height: 0px;
    }
    .user-thumbnail .user-thumbnail-details::-webkit-scrollbar-track {
        background: transparent;
    }
    .user-thumbnail .user-thumbnail-details::-webkit-scrollbar-thumb {
        background: rgb(160, 160, 160);
    }
    .user-thumbnail .user-thumbnail-details::-webkit-scrollbar-thumb:hover {
        background: rgb(130, 130, 130);
    }           

    .user-thumbnail .user-thumbnail-details .user-thumbnail-header {
        height: auto;		
        bottom: 100%;
        padding: 10px 5px;
    }
    .user-thumbnail:hover .user-thumbnail-details {
        bottom: 0px;
        overflow: auto;
        padding-bottom: 25px;
    }
    .user-thumbnail:hover .user-thumbnail-details .user-thumbnail-header {
        position: relative;
        bottom: 0px;
        padding-top: 20px;
        padding-bottom: 0px;
    }
    .user-thumbnail .user-thumbnail-details .user-thumbnail-header h3,	
    .user-thumbnail .user-thumbnail-details .user-thumbnail-header h5 {
        color: rgb(62, 62, 62);
        font-size: 1em;
        font-weight: 900;
        text-transform: uppercase;
        margin: 0 !important;
        padding: 0;
    }
    .user-thumbnail .user-thumbnail-details .user-thumbnail-header h5 {
        color: rgb(142, 182, 52);
        font-size:0.7em;
        font-weight: 400;
        padding:5px;
        margin-top: 5px;
    }
    .user-thumbnail .user-thumbnail-details .user-thumbnail-detail .social {
        list-style: none;
        padding: 0px;
        margin-top: 25px;
        text-align: center;
    }
    .user-thumbnail .user-thumbnail-details .user-thumbnail-detail .social a {
        position: relative;
        display: inline-block;
        min-width: 40px;
        padding: 10px 0px;
        /*margin: 0px 5px;*/
        overflow: hidden;
        text-align: center;
        background-color: rgb(215, 215, 215);
        border-radius: 40px;
    }

    a.social-icon {
        text-decoration: none !important;
        box-shadow: 0px 0px 1px rgb(51, 51, 51);
        box-shadow: 0px 0px 1px rgba(51, 51, 51, 0.7);
    }
    a.social-icon:hover {
        color: rgb(255, 255, 255) !important;
    }
    a.facebook {
        color: rgb(59, 90, 154) !important;
    }
    a.facebook:hover {		
        background-color: rgb(59, 90, 154) !important;
    }
    a.twitter {
        color: rgb(45, 168, 225) !important;
    }
    a.twitter:hover {
        background-color: rgb(45, 168, 225) !important;
    }
    a.github {
        color: rgb(51, 51, 51) !important;
    }
    a.github:hover {
        background-color: rgb(51, 51, 51) !important;
    }
    a.google-plus {
        color: rgb(244, 94, 75) !important;
    }
    a.google-plus:hover {
        background-color: rgb(244, 94, 75) !important;
    }
    a.linkedin {
        color: rgb(1, 116, 179) !important;
    }
    a.linkedin:hover {
        background-color: rgb(1, 116, 179) !important;
    }
    .network-action-btn
    {
        cursor: pointer;
    }

    .network-name-textarea
    {
        outline: none;

        background: none;
        resize: none;
        overflow: hidden;
        
        border: 1px solid #ccc;
        border-radius: 4px;
    }
    .network-name-textarea:disabled
    {
        border:none;
    }

</style>
<div class="container">

    <div class="row" ng-controller="networkController">
        <div class="col-lg-12 well">
            
            <div class="col-lg-8">
                
                <h4>
                    <textarea class="network-name-textarea" ng-model="networks[currentnetwork].name" ng-disabled="!editMode" ng-pattern="/^[\w\d ]+/" ></textarea>
                    <small>gridle.in/networks/view/{{networks[currentnetwork].url}}</small>
                </h4>
            </div>

            <div class="col-lg-4">           
                 <a href="#" class="btn btn-info"  ng-show="!editMode" ng-click="oldName=networks[currentnetwork].name;editMode = !editMode;">
                    Edit Network Detail
                    
                </a> 
                <a href="#" class="btn btn-info"  ng-show="editMode" ng-click="editMode = !editMode;editNetwork(networks[currentnetwork].name,editMode,oldName)">
                    Save
                    
                </a> 
                <a href="#" class="btn btn-warning"  data-toggle="modal" data-target="#networkModalAddMembers">Invite User</a> 
            </div>

        </div>
        <div class="list-group col-lg-3">
            <a href="#" class="list-group-item disabled">
                <strong>DASHBOARD</strong>
            </a>
            <a href="#" class="list-group-item g-network-dashboard-list" data-target="#members">Manage</a>
            <a href="#" class="list-group-item g-network-dashboard-list" data-target="#stats">Stats</a>
            <a href="#" class="list-group-item g-network-dashboard-list">Plans</a>
            <a href="#" class="list-group-item g-network-dashboard-list">Support</a>



            <a href="#" class="list-group-item g-network-dashboard-list">Expires on <strong>{{ dateFormat(networks[currentnetwork].expires, "mediumDate")}}</strong></a>
            <a href="#" class="list-group-item list-group-item-danger g-network-dashboard-list" ng-show="dateFormat(networks[currentnetwork].expires, 'short') < dateFormat(getDatetime(), 'short')"><strong>Expired</strong></a>

        </div>
        <div class="col-lg-9 list-group-display" id="stats">
            <div class="list-group col-lg-3">
                <a href="#" class="list-group-item">Users 
                    <span class="badge badge-info"><?= $Networks[$CurrentNetwork]['user_count']; ?></span>           
                </a>
                <a href="#" class="list-group-item">
                    Groups
                    <span class="badge badge-success"><?= $Networks[$CurrentNetwork]['group_count']; ?></span> 
                </a>
                <a href="#" class="list-group-item">
                    File Usage
                    <span class="badge badge-success"><?= $Networks[$CurrentNetwork]['file_count']; ?></span> 
                </a>
                <!--                <a href="#" class="list-group-item">Tasks Shared</a>
                                <a href="#" class="list-group-item">Task Completed</a>-->
                <a href="#" class="list-group-item">Invites Remaining<span class="badge badge-info"><?= $Networks[$CurrentNetwork]['user_limit'] - $Networks[$CurrentNetwork]['user_count']; ?></span></a>
            </div>

        </div>
        <div class="col-lg-9 list-group-display" id="members">
            <div class="[ col-sm-3 col-md-3 ]" ng-repeat="user in user_networks">
                <div class="user-thumbnail">
                    <img style="width: 100%" ng-src="{{user.UserInfo.image}}" />
                    <div class="[ user-thumbnail-details ] animate">
                        <div class="[ user-thumbnail-header ]">
                            <h3> {{user.UserInfo.name}} </h3>
                            <h5>{{user.UserNetwork.role}} </h5>

                        </div>

                        <div class="[ user-thumbnail-detail ]">
                        <h6>{{user.UserInfo.designation}} </h6>
                        <h6>{{user.UserInfo.contact_no}} </h6>
                        <h6>{{user.UserInfo.dob}} </h6>
                        <h6>{{user.UserInfo.location}} </h6>
                            <hr style="margin:10px auto">

                            <!-- Description -->
                            <label ng-show="(user_networks[findUserNetwork(loggeduser)].UserNetwork.role == 'admin' || user_networks[findUserNetwork(loggeduser)].UserNetwork.role == 'owner') && user.UserNetwork.role == 'user'" class="network-action-btn label label-info"  ng-click="assignRole(user.UserInfo.user_id, 'admin')">Make Admin</label>
                            <label ng-show="(user_networks[findUserNetwork(loggeduser)].UserNetwork.role == 'admin' || user_networks[findUserNetwork(loggeduser)].UserNetwork.role == 'owner') && user.UserNetwork.role == 'admin'" class="network-action-btn label label-danger"  ng-click="assignRole(user.UserInfo.user_id, 'user')">Remove Admin</label>
                            <label ng-show="(user_networks[findUserNetwork(loggeduser)].UserNetwork.role == 'admin' || user_networks[findUserNetwork(loggeduser)].UserNetwork.role == 'owner') && user.UserNetwork.role != 'owner'" class="network-action-btn label label-danger"  ng-click="removeUserNetworkConfirm(user.UserInfo.user_id)"><i class="fa fa-times"></i> Remove</label>

                        </div>
                    </div>
                </div>
            </div>
        </div>


    </div>

    <?php echo $this->element('Network/adduser'); ?>
</div>

<script>
    $('.g-network-dashboard-list[data-target]').on('click', function()
    {
        $('.list-group-display').addClass('hide');
        var id = $(this).data('target');
        $(id).removeClass('hide');
        $('.g-network-dashboard-list[data-target]').removeClass('active');
        $(this).addClass('active');


    });

    $('.list-group-display').addClass('hide');
    $('#members').removeClass('hide');
    $('.g-network-dashboard-list[data-target=#members]').addClass('active');
</script>
<?php
$this->layout = 'layout_focussed_demo';
$this->set('title_for_layout', 'All workspaces');
?>
<div class="container-fluid margin-top-50" ng-controller="workspaceCtrl">
    <div class="row">
        <div class="col-sm-8 col-sm-offset-2">
            <!--workspace heading--> 
            <div class="row workspaces-head padding-tb-20 margin-top-20">
                <div class="col-sm-4 col-sm-offset-4">
                    <p class="text-bold text-muted line-height-30">
                        My workspaces
                    </p>    
                </div>
                <div class="col-sm-4">
                    <input class="form-control pull-right" placeholder="Search a workspace" />    
                </div>


            </div>
            <!--workspace listing--> 
            <div class="row workspaces-body padding-tb-15">
                <!--workspace tile-->
                <a href="/desks/index_demo">
                    <div class="col-sm-3 workspace-tile">
                        <p>
                            <img src="/img/defaults/workspace_1.jpg" height="100" />
                        </p>
                        <p class="text-muted text-bold">
                            Gridle
                        </p>
                    </div>
                </a>
                <!--workspace tile ends-->

                <!--workspace tile-->
                <a href="/desks/index_demo">
                    <div class="col-sm-3 workspace-tile">
                        <p>
                            <img src="/img/defaults/workspace_2.png" height="100" />
                        </p>
                        <p class="text-muted text-bold">
                            Blogtard
                        </p>
                    </div>
                </a>
                <!--workspace tile ends--> 

                <!--workspace tile-->
                <a href="/desks/index_demo">
                    <div class="col-sm-3 workspace-tile">
                        <p>
                            <img src="/img/defaults/workspace_3.png" height="100" />
                        </p>
                        <p class="text-muted text-bold">
                            DAIICT
                        </p>
                    </div>
                </a>
                <!--workspace tile ends--> 

                <!--workspace tile--> 
                <a href="/desks/index_demo">
                    <div class="col-sm-3 workspace-tile">
                        <p>
                            <img src="/img/defaults/workspace_4.png" height="100" />
                        </p>
                        <p class="text-muted text-bold">
                            Consultancy
                        </p>
                    </div>
                </a>
                <!--workspace tile ends--> 

                <!--workspace tile--> 
                <a href="/desks/index_demo">
                <div class="col-sm-3 workspace-tile">
                    <p>
                        <img src="/img/defaults/workspace_5.png" height="100" />
                    </p>
                    <p class="text-muted text-bold">
                        Ionth
                    </p>
                </div>
                </a>
                <!--workspace tile ends--> 

                <!--workspace tile--> 
                <a href="/desks/index_demo">
                <div class="col-sm-3 workspace-tile">
                    <p>
                        <img src="/img/defaults/workspace_6.png" height="100" />
                    </p>
                    <p class="text-muted text-bold">
                        Westend Park
                    </p>
                </div>
                </a>
                <!--workspace tile ends--> 

                <!--workspace tile--> 
                <a href="/desks/index_demo">
                <div class="col-sm-3 workspace-tile">
                    <p>
                        <img src="/img/defaults/workspace_1.jpg" height="100" />
                    </p>
                    <p class="text-muted text-bold">
                        wWhere
                    </p>
                </div>
                </a>
                <!--workspace tile ends--> 

                <!--workspace tile--> 
                <a href="/desks/index_demo">
                <div class="col-sm-3 workspace-tile">
                    <p>
                        <img src="/img/defaults/workspace_2.png" height="100" />
                    </p>
                    <p class="text-muted text-bold">
                        FoodDragon
                    </p>
                </div>
                </a>
                <!--workspace tile ends--> 

                <!--workspace tile--> 
                <a href="/desks/index_demo">
                <div class="col-sm-3 workspace-tile">
                    <p>
                        <img src="/img/defaults/workspace_3.png" height="100" />
                    </p>
                    <p class="text-muted text-bold">
                        CIIE
                    </p>
                </div>
                </a>    
                <!--workspace tile ends-->

                <!--workspace tile--> 
                <div class="col-sm-3 workspace-tile" ng-click="openModalAddWorkspace()">
                    <p style="line-height: 100px; font-size:110px;" class="workspace-new-icon">
                        <span class="fa fa-plus-circle"></span>
                    </p>
                    <p class="btn-link cursor-pointer text-muted text-bold">
                        Add new workspace
                    </p>
                </div>
                <!--workspace tile ends-->
            </div>
        </div>
    </div>
</div>
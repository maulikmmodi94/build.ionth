<?php
$this->layout = 'gridle_in';
$this->set('title_for_layout', 'Networks');
?>
<div class='container-fluid'>
    <div class='g-add-network-wrap row' >
        <div class="add-network-rocket col-lg-2 padding-lr-0">
            <object data="/img/signup-flow/rocket.svg" ></object>
        </div>
        <div class="col-lg-8">
            <div class="g-add-network-content container-fluid">
                <div class="add-network-heading-div text-center row">
                    <h1 class="add-network-heading font-big-5 text-uppercase">You are about to rocket-fuel your teams with Gridle</h1>
                    <!--<h3>You are about to rocket-fuel your teams with Gridle</h3>-->
                </div>
                <div class="add-network-form row">
                    <?php
                    echo $this->Form->create('Network', array('action' => 'add', 'name' => 'formNetworkAdd', 'novalidate', 'role' => 'form', 'inputDefaults' => array(
                            'label' => false
                    )));
                    ?>
                    <div class="col-lg-offset-1 col-lg-7 padding-lr-0">
                        <div class="form-group">
                            <?php echo $this->Form->input('name', array('id' => 'networkname', 'ng-model' => 'data.Network.name', 'type' => 'text', 'class' => 'form-control  input-lg', 'placeholder' => 'Give name to your network', 'div' => FALSE, 'required', 'ng-minlength' => 3, 'ng-maxlength' => 30, 'ng-pattern' => '/^[a-z0-9 _-]*$/i', 'maxlength' => 30, 'autofocus')); ?>
                        </div>

                    </div>
                    <?php
                    /* Return to Desk */
                    echo $this->Form->input('return_to', array('type' => 'hidden', 'div' => FALSE, 'value' => '/networks/add_members'));
                    ?>
                    <div class="col-lg-4 ">
                        <button id="btn-network-add" type="submit" class="btn btn-warning btn-block btn-lg" style="font-family: 'proxima-nova-extra-bold'; height: 46px;" ng-disabled="formNetworkAdd.$invalid" ><span class='font-big-1' >NEXT &nbsp;&nbsp;<span class="fa fa-chevron-circle-right"></span></span></button>
                    </div>
                    <div class="clearfix"></div>
                    <!--<div class="col-lg-7 col-lg-offset-1">-->
                    <?php
                    //echo $this->Form->input('NetworkPlan.0.plan_id', array('options' => $plans, 'class' => 'form-control input-sm', 'empty' => 'choose type of network', 'div' => FALSE, 'required', 'ng-model' => 'data.NetworkPlan.0.plan_id'));
                    ?>
                    <!--</div>-->
                    <div class="clearfix"></div>
                    <div ng-show="formNetworkAdd['data[Network][name]'].$dirty" class="text-warning col-lg-11 col-lg-offset-1" style="color:#fff">
                        <p ng-show="formNetworkAdd['data[Network][name]'].$error.required" class="text-warning p-margin-0">Please give a name to your new network</p>
                        <p ng-show="formNetworkAdd['data[Network][name]'].$error.minlength" class="text-warning p-margin-0">Minimum 2 characters are required</p>
                        <p ng-show="formNetworkAdd['data[Network][name]'].$error.maxlength" class="text-warning p-margin-0">Name is too long.</p>
                        <p ng-show="formNetworkAdd['data[Network][name]'].$error.pattern" class="text-warning p-margin-0">Alphabets, numbers, spaces, hyphens (-) and underscores(_) are allowed only</p>
                    </div>
                    <div class="add-network-below-content text-center">
                        <p class="font-big-1">Your network is going to be the complete set of people you work with.</p>
                        <p class='add-border-up' style='padding-top: 15px;'> Typically, it's the name of the company and comes with a dashboard, basic analytics and a dedicated management panel.</p>
                    </div>
                    <?php echo $this->Form->end(); ?>
                </div>

            </div>
        </div>
        <div class="add-netwrk-moon col-lg-2">
            <p>
                <object data="/img/signup-flow/moon.svg" style="" ></object>
            </p>
        </div>
    </div>
</div>
<script>
// jQuery plugin to prevent double submission of forms
    jQuery.fn.preventDoubleSubmission = function () {
        $(this).on('submit', function (e) {
            var $form = $(this);

            if ($form.data('submitted') === true) {
                // Previously submitted - don't submit again
                e.preventDefault();
            } else {
                // Mark it so that the next submit can be ignored
                $form.data('submitted', true);
            }
        });

        // Keep chainability
        return this;
    };

    $('form').preventDoubleSubmission();

</script>
<?php
$this->layout = 'gridle_in';
$this->set('title_for_layout', 'Invite to network');
?>
<style>
    .tag-list input
    {

    }
</style>
<div class='container-fluid'>
    <div class="g-add-members-wrap row">
        <div class="add-members-box container-fluid">
            <div class="add-members-heading-div text-center row">

                <p class="add-members-heading text-extra-bold text-uppercase font-big-5 col-lg-offset-3 col-lg-6" ><?=$Networks[$CurrentNetwork]['name'] ?> Network</p>
                <p class='add-members-subheading font-big-2 col-lg-offset-3 col-lg-6 clearfix'>Invite at least <span class="text-emphasize">2 of your colleagues</span> to your Network by entering their email addresses in the box below</p>
            </div>
            <div class="add-members-form row">

                <?php
                echo $this->Form->create('User', array('action' => 'admin_add', 'name' => 'UserAdminAddForm','id'=>'formNetworksInvite'));
                $this->Form->unlockField('email');
                ?>
                <div class="col-lg-offset-2 col-lg-5 padding-lr-0">
                    <div class="form-group" >
                        <tags-input ng-model="tags" display-property="email" placeholder="Add comma separted email addresses of your colleagues." min-tags="2" allowed-tags-pattern='^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$'>
                        </tags-input>
                        <div class="input text required" ng-repeat="tag in tags">
                            <label for="email{{$index + 1}}"></label>
                            <input id="memebers-email" name="data[email][{{$index + 1}}]" type="hidden" id="email{{$index + 1}}" ng-value="tag.email" required="required"></input>
                        </div>
<!--                        <input class="form-control input-lg font-normal" placeholder="Add comma separted email addresses of your colleagues." />-->
                    </div>
                </div>
                <?php
                /* Return to Desk */
                echo $this->Form->input('return_to', array('type' => 'hidden', 'div' => FALSE, 'value' => '/desks/'));
                ?>
                <div class="col-lg-3 ">
<?php echo $this->Form->submit(__d('networks', 'INVITE MEMBERS'), array('class' => 'btn btn-warning btn-block btn-lg','id'=>'btn-network-add-member', 'div' => false, 'ng-disabled' => 'UserAdminAddForm.$invalid')); ?>
<!--                    <button class="btn btn-warning btn-block btn-lg" ><span class='font-big-1' >INVITE MEMBERS &nbsp;&nbsp; <i class='fa fa-chevron-circle-right'></i> </span></button>-->
                </div>
                <div class='clearfix col-lg-offset-3 col-lg-6 text-center'>
                    <p class='font-big-1'>People you add in the network will be able to form teams within themselves, share files, assign tasks and chat. Possibilities are limitless.</p>    
                </div>
                <?php
                echo $this->Form->end();
                ?>
            </div>
        </div>
    </div>
</div>
<script>

    $(document).ready(function() {
        $("input").focus();
    });
</script>

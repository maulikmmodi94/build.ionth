<?php
$this->layout = 'layout_focused_new';
$this->set('title_for_layout', 'All workspaces');
?>
<div class="container-fluid margin-top-50" ng-controller="workspaceController">
    <div class="row">
        <!--Loader--> 
        <div id="loader-wrapper" class="col-sm-12" ng-show="loadingWorkspaces">
            <div id="loader" style="left:30%"></div>
            <div class="loading-text text-bold font-big-6 color-black-alpha-high" style="left:33%">Loading..</div>
        </div>
        <div class="col-sm-8 col-sm-offset-2">
            <!--workspace heading--> 
            <div class="row workspaces-head padding-tb-20 margin-top-20">
                <div class="col-sm-4 col-sm-offset-4">
                    <p class="text-bold text-center text-dim line-height-30">
                        My workspaces
                    </p>    
                </div>
                <div class="col-sm-4">
                    <div class="search-box">
                        <p class="search-icon fa fa-search text-dim"></p>
                        <input class="form-control input-sm pull-right" placeholder="Search a workspace" ng-model="networkname" />    
                    </div>
                </div>


            </div>
            <!--workspace listing--> 
            <div class="row workspaces-body padding-tb-15">
                <!--workspace tile--> 
                <div ng-if="networks">
                    <div class="col-sm-3 workspace-tile" ng-repeat="network in networks| filter : networkname">
                        <a href="/networks/switchNetwork/{{network.url}}/desks">
                            <p>
                                <img src="/img/defaults/workspace_{{$index % 3}}.png" height="100" />
                            </p>
                            <p class="text-bold network-name">
                                <span ng-bind="network.name"></span>
                            </p>
                        </a>
                    </div>
                    <!--workspace tile ends-->
                </div>

                <!--                <div ng-repeat="network in networks_expired| networkAlphabetOrder">
                                    <div class='network-tile-expired network-tile text-center'>
                
                                                NAME AND INFO           
                                        <a href="/pages/activate">
                                            <div class='network-tile-img-area text-center'>
                                                <div class="network-tile-bg">
                                                    <p class='network-tile-name p-margin-0 font-big-1' ng-bind="network.name"></p>
                                                </div>
                                            </div>
                                        </a>
                
                                    </div>
                                    <div class="network-tile-section network-tile-stats">
                                        <p class="text-center text-muted p-margin-0">This network has been expired. Please <a href="/pages/activate">activate</a> it.</p>
                                    </div>
                                </div>-->


                <!--workspace tile--> 
                <div class="col-sm-3 workspace-tile workspace-new-tile" ng-click="openModalAddWorkspace()">
                    <p style="line-height: 76px; font-size:93px; padding-top: 5px;" class="workspace-new-icon icon icon2-arrows-circle-plus">
<!--                        <span class="fa fa-plus-circle"></span>-->
                    </p>
                    <p class="text-bold network-name">
                        Add new workspace
                    </p>
                </div>
                <!--workspace tile ends-->
            </div>
        </div>
    </div>
</div>
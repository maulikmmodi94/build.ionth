<?php
$this->layout = 'layout_focused_new';
$this->set('title_for_layout', 'Workspace');
?>
<div class="container-fluid margin-top-50" ng-controller="dashboardController">
    <div class="row">
        <div class="col-sm-8 col-sm-offset-2">
            <!--HEADING--> 
            <div class="row workspaces-head padding-tb-20 margin-top-20">
                <div class="col-sm-2 font-small-1">
                    <a href="#">
                        <div class="ws-back-link">
                            <p class="pull-left" style="padding-top:6px;"><span class="icon icon2-arrows-glide"></span></p>
                            <p class="pull-left text-underline margin-left-5 line-height-30" ><a href="/desks">Back to Desk</a></p>
                        </div>     

                    </a>
                </div>
                <div class="col-sm-8">
                    <p class="text-bold text-center text-dim line-height-30">
                        Manage <span ng-bind="currentNetwork" ></span>
                    </p>    
                </div>
            </div>
            <div class="row ws-manage-area" style="width:103.5%;">
                <!--MENU--> 
                <div class="col-sm-2 ws-manage-menu">
                    <div class="row">
                    <div class="col-sm-12 menu-tile" ng-click="changeselection('people')" ng-class="{'active':displayContent == 'people'}">
                        <p>People</p>
                    </div>
                    <div class="col-sm-12  menu-tile" ng-click="changeselection('delete')" ng-class="{'active':displayContent == 'delete'}" ng-show="user_role_network == 'owner'">
                        <p>Delete</p>
                    </div>
                    <div class="col-sm-12  menu-tile" ng-click="changeselection('leave')" ng-class="{'active':displayContent == 'leave'}" ng-hide="user_role_network == 'owner'">
                        <p>Leave</p>
                    </div>
                        </div>
                </div>
                <!--CONTENT--> 
                <div class="col-sm-10 ws-manage-content ">

                    <!--------------WORKSPACE PEOPLE ------------------> 
                    <div class="row">
                    <div class="col-sm-12" ng-show="displayContent == 'people'">
                        <!--SEARCH AND ADD PEOPLE--> 
                        <div class="row padding-tb-20 ws-invite-area">
                            <div class="col-sm-2 text-center">
                                <button class="btn btn-primary btn-sm" ng-click="inviteToNetwork()">Invite people</button>
                            </div>
                            <div class="col-sm-4 col-sm-offset-6">
                                <div class="search-box">
                                    <input class="form-control input-sm"  placeholder="Search member" ng-model="searchmember"/>
                                    <span class="fa fa-search text-dim search-icon" ></span>
                                </div>
                            </div>
                        </div>
                        <!--workspace listing--> 
                        <div class="row ws-people-list "style="padding-left: 10px;">
                            <!--SINGLE TILE START-->
                            <div class="col-sm-2 ws-people-tile" ng-repeat="user in usersofNetwork| filter:searchmember" style="padding-right: 25px; padding-bottom: 10px;">
                                <div class="row">
                                <div class="ws-people-box col-sm-12" >
                                    <!--SETTINGS--> 
                                    <div class="row">
                                    <div class="ws-tile-setting text-right col-sm-12 ">
                                        <li class="dropdown" dropdown ng-show="user_role_network == 'owner'">
                                            <p class="dropdown-toggle cursor-pointer" dropdown-toggle>
                                                <span class="fa fa-gear"></span>

                                            </p>
                                            <ul class="dropdown-menu dropdown-animation dropdown-menu-right " role="menu">
                                                <li class="container-fluid">
                                                    <a href="#" class="row">
                                                        <div class="col-sm-2 padding-lr-0" style="padding-top:5px;">
                                                            <span class="icon icon-basic-trashcan font-big-2" style=""></span>
                                                        </div>
                                                       <div class="col-sm-10 padding-tb-5" ng-click="removeUserModalOpen(user,$index)">
                                                            <p class="" >
                                                                <span>Remove</span>
                                                            </p>    
                                                        </div>
                                                    </a>
                                                </li>

                                            </ul>
                                        </li>
                                    </div>
                                    </div>
                                    <!--IMAGE AND PHOTO--> 
                                    <div class="row">
                                    <div class="ws-tile-img col-sm-6 col-sm-offset-3 padding-lr-0">

                                        <g-profile-img myclass="workspace-user-img text-center font-big-3" type='user' path="user.UserInfo.vga" name="user.UserInfo.name"></g-profile-img>
                                    </div>
                                    </div>
                                    <div class="row">
                                        <div class="margin-top-10 ws-tile-name col-sm-12">      
                                            <p class="text-center font-small-2" title="{{user.UserInfo.name}}">
                                                <span ng-bind="user.UserInfo.name"></span>
                                            </p>
                                        </div>
                                    </div>
                                    </div>
                                </div>
                                </div>
                                <!--SINGLE TILE ENDS-->
                                <!--                            <div class="col-sm-12 margin-top-30">
                                                                <p class="btn-link text-center font-small-2">Load all members</p>
                                                            </div>-->
                            </div>
                        </div>
                        </div>
                        <!--DELETE WORKSPACE--> 
                        <div class="row">
                        <div class="col-sm-12" ng-show="displayContent == 'delete'">
                            <div class="row">
                            <div class="col-sm-8 col-sm-offset-2 text-center margin-top-30 padding-tb-20">
                                <div class="text-center">
                                    <p><img src="/img/defaults/workspace_6.png" width="40%" /></p>
                                </div>
                                <div class="margin-top-20">
                                    <button class="btn btn-primary" ng-click="deleteNetworkModal()">Delete <span ng-bind="currentNetwork"></span> workspace </button>
                                </div>
                                <div class="margin-top-20 text-dim font-small-1">
                                    <p>This will delete all the data permanently.</p>
                                </div>
                            </div>

                            </div>
                        </div>
                        </div>
                        <!--LEAVE WORKSPACE-->
                        <div class="row">
                        <div class="col-sm-12" ng-show="displayContent == 'leave'">
                            <div class="row">
                            <div class="col-sm-8 col-sm-offset-2 text-center margin-top-30 padding-tb-20">
                                <div class="text-center">
                                    <p><img src="/img/defaults/ico-9.png" width="40%" /></p>
                                </div>
                                <div class="margin-top-20">
                                    <button class="btn btn-primary" ng-click="leaveNetworkModalOpen()">Leave <span ng-bind="currentNetwork"></span> workspace </button>
                                </div>
                                <div class="margin-top-20 text-dim font-small-1">
                                    <p>This will delete all the data permanently.</p>
                                </div>
                            </div>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
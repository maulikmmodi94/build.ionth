<?php
/**
 * Copyright 2010 - 2013, Cake Development Corporation (http://cakedc.com)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright 2010 - 2013, Cake Development Corporation (http://cakedc.com)
 * @license MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
?>
                    <p class="g-modal-sm-body-heading font-big-1">Create your own network</p>
                    <!--    FORM    -->
                    <?php
                    echo $this->Form->create('Network', array('action' => 'add', 'name' => 'formNetworkAdd', 'novalidate', 'class' => 'g-modal-sm-body-form ', 'role' => 'form', 'inputDefaults' => array(
                            'label' => false
                    )));
                    ?>
                    <fieldset>
                        <!--    NAME OF THE NETWORK -->
                        <div class="form-group">
                            <?php echo $this->Form->input('name', array('id' => 'networkname', 'ng-model' => 'data.Network.name', 'type' => 'text', 'class' => 'form-control input-sm', 'placeholder' => 'Give name to your network', 'div' => FALSE, 'required', 'ng-minlength' => 3, 'ng-maxlength' => 30, 'ng-pattern' => '/^[a-z0-9 _-]*$/i', 'maxlength' => 30)); ?>
                            <div ng-show="formNetworkAdd['data[Network][name]'].$dirty" class="form-inline-error text-warning ">
                                <p ng-show="formNetworkAdd['data[Network][name]'].$error.required" class="p-margin-0">Please give a name to your new network</p>
                                <p ng-show="formNetworkAdd['data[Network][name]'].$error.minlength" class="p-margin-0">Use more than 2 letters</p>
                                <p ng-show="formNetworkAdd['data[Network][name]'].$error.maxlength" class="p-margin-0">Name is too long.</p>
                                <p ng-show="formNetworkAdd['data[Network][name]'].$error.pattern" class="p-margin-0">Alphabets, numbers, spaces, hyphens (-) and underscores(_) are allowed only</p>
                            </div>
                        </div>
                        <!--    CHOOSING PLAN -->
                        <div class="form-group">
                            <?php
                            echo $this->Form->input('NetworkPlan.0.plan_id', array('options' => $plans, 'class' => 'form-control input-sm', 'empty' => 'choose type of network', 'div' => FALSE,'required','ng-model'=>'data.NetworkPlan.0.plan_id'));
                            ?>
                            <div ng-show="formNetworkAdd['data[NetworkPlan][0][plan_id]'].$dirty" class="form-inline-error text-warning p-margin-0">
                                <p ng-show="formNetworkAdd['data[NetworkPlan][0][plan_id]'].$error.required" class="">Please choose a plan for your network</p>
                            </div>
                        </div>

                        <?php echo $this->Form->submit(__d('networks', 'Add network '), array('class' => 'btn btn-success btn-block', 'div' => array('class' => 'col-lg-6 col-lg-offset-3'),'ng-disabled'=>'formNetworkAdd.$invalid')); ?>
                    </fieldset>

                    <?php echo $this->Form->end(); ?>

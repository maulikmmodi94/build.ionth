<?php
$this->layout = 'gridle_in';
$this->set('title_for_layout', $Networks[$CurrentNetwork]["name"] . ' Dashboard');
?>

<!--main container-->
<div class="container-fluid" ng-controller="networkController">
    <div class="col-sm-4">

        <div class="dashboard-menu">

            <!--Network name and edit start-->     
            <div class="dashboard-network-name col-lg-6 col-md-6 col-sm-6 col-xs-4">
                <div class="col-sm-10 padding-lr-0">
                    <!--    network name        -->

                    <input class="ta-network-name form-control" ng-model="networks[currentnetwork].name" ng-disabled="!editMode" ng-pattern="/^[\w\d ]+/" >
                </div>
                <div class="col-sm-1 padding-lr-0">
                    <!--    EDIT NETWORK INFO      -->
                    <span ng-show="user_networks[findUserNetwork(loggeduser)].UserNetwork.role == 'owner'">
                        <button class="btn-link" ng-show="!editMode" ng-click="oldName = networks[currentnetwork].name;
                                    editMode = !editMode;" title="Rename Network" tooltip ><i class="fa fa-pencil"></i></button>
                    </span>
                </div>
                <div class="col-sm-10 padding-lr-0">
                    <button class='btn btn-sm btn-default' ng-show="editMode" ng-click='editMode = !editMode;
                                networks[currentnetwork].name = oldName'>Cancel</button>
                    <button class="btn btn-primary btn-sm"  ng-show="editMode" ng-click="editMode = !editMode;
                                editNetwork(networks[currentnetwork].name, editMode, oldName)">Save</button>
                </div>
            </div>
            <!--Network name and edit end--> 

            <!--Manage start-->

            <div class="dashboard-menu-item active" data-target="#network-people">
                <p class="p-margin-0">
                    <!-- if admin or owner  -->
                    <span ng-show="user_networks[findUserNetwork(loggeduser)].UserNetwork.role == 'admin' || user_networks[findUserNetwork(loggeduser)].UserNetwork.role == 'owner'" >Manage</span>
                    <!-- if not admin    -->
                    <span ng-show="user_networks[findUserNetwork(loggeduser)].UserNetwork.role == 'user'" >People</span>
                </p>
            </div>  


            <!--Manage End-->

            <!--Stats start-->

            <div class="dashboard-menu-item" data-target="#network-stats">
                <p class="p-margin-0">Stats</p>
            </div>

            <!--Stats End-->

            <!--Access Control start-->

            <div class="dashboard-menu-item" ng-if='user_networks[findUserNetwork(loggeduser)].UserNetwork.role === "owner"'>
                <a href="/networks/security"><p class="p-margin-0">Access Control</p></a>
            </div>

            <!--Access Control end-->

            <!--Delete Group start-->
            <div class="dashboard-menu-item" ng-if='user_networks[findUserNetwork(loggeduser)].UserNetwork.role === "owner"'>
<?php echo $this->Form->postLink(__('Delete Network'), array('action' => 'delete', $CurrentNetwork), array('class' => ''), __('Deleting a Network is permanent. Are you sure you want to delete  network "%s"? There is no undo.', $Networks[$CurrentNetwork]['name'])); ?>
            </div>    
            <!--Delete Group End-->
        </div>

    </div>


    <div class="col-sm-8" ng-controller="analyticscontroller">
        <!--If on Manage then invite to network should appear-->
        <div class="row">
            <!--The icons of network and group and personal will be here-->
            <div class="col-sm-3 col-sm-offset-1" ng-show="(user_networks[findUserNetwork(loggeduser)].UserNetwork.role == 'admin' || user_networks[findUserNetwork(loggeduser)].UserNetwork.role == 'owner')" >
                <!--network-->   
                <div class='dashboard-stats-count dashboard-stats-group-count' >
                    <p class='dashboard-count-cover center-block' ng-bind="networks[currentnetwork]['group_count']"></p>
                    <a class='text-muted font-small-1' href="#" ng-click="getNetworkcharts()">NETWORK</a>
                </div>

            </div>

            <div class="col-sm-3 col-sm-offset-1">
                <!--Group-->
                <div class='dashboard-stats-count dashboard-stats-group-count' ng-click="findGroupDetails()" ng-if="!admingroups.length == 0">
                    <p class='dashboard-count-cover center-block' ng-bind="admingroups.length"></p>
                    <a class='text-muted font-small-1' href="#" class="g-header-dropdown-btn dropdown-toggle" data-toggle="dropdown">GROUPS</a>
                    <ul class="g-header-dropdown-menu dropdown-menu dropdown-menu-right">
                        <span ng-repeat="group in admingroups">
                            <li><button class="g-header-dropdown-link" ng-bind="group.Group.name" ng-click="getGroupstats(group.Group.id)"></button></li>
                            <li class="divider"></li>
                        </span> 
                    </ul>


                </div>

            </div>

            <div class="col-sm-3 col-sm-offset-1">
                <!--User-->
                <div class='dashboard-stats-count dashboard-stats-user-count'>
                    <p class='dashboard-count-cover center-block' ng-bind="networks[currentnetwork]['user_count']"></p>
                    <p class='text-muted font-small-1'>USERS</p>
                </div>
            </div>
        </div>

        <!--for Personal-->

        <!--for Group--> 
        <div class="row">
            <!--main content personal stats wii be by default loaded-->    
            <div class="col-sm-9">
                <!-- Bar Chart--><canvas tc-chartjs-bar id="mycanvas" chart-options="optionsBar" chart-data="dataBar"></canvas>
            </div>

            <div class="col-sm-3">
                <canvas tc-chartjs-pie chart-options="optionspie" chart-data="datapie"></canvas>    
            </div>

        </div>

        <div class="row">
            <!--for top three users-->
            <div class="col-sm-4">
                <g-profile-img type="user"  path="{{user1.UserInfo.vga}}" height="25px" width="25px" myclass="g-desk-tile-user-img font-small-1 " name="{{user1.UserInfo.name}}"  ></g-profile-img>
                <p class="g-desk-tile-user-name p-margin-0" ng-bind="user1.UserInfo.name" ></p>
                <p ng-bind="user_productivity1">%</p>
            </div>  

            <div class="col-sm-4">
                <g-profile-img type="user"  path="{{user2.UserInfo.vga}}" height="25px" width="25px" myclass="g-desk-tile-user-img font-small-1 " name="{{user2.UserInfo.name}}"  ></g-profile-img>
                <p class="g-desk-tile-user-name p-margin-0" ng-bind="user2.UserInfo.name" ></p>
                <p ng-bind="user_productivity2">%</p>
            </div>

            <div class="col-sm-4">
                <g-profile-img type="user"  path="{{user3.UserInfo.vga}}" height="25px" width="25px" myclass="g-desk-tile-user-img font-small-1 " name="{{user3.UserInfo.name}}"  ></g-profile-img>
                <p class="g-desk-tile-user-name p-margin-0" ng-bind="user3.UserInfo.name" ></p>
                <p ng-bind="user_productivity3">%</p>
            </div>

        </div>

        <div class="row">
            <!--for top three Groups-->
            <div class="col-sm-4">
                <p class="g-desk-tile-user-name p-margin-0" ng-bind="group1.Group.name" ></p>
                <p ng-bind="gorup_productivity1">%</p>
            </div>  

            <div class="col-sm-4">  
                <p class="g-desk-tile-user-name p-margin-0" ng-bind="group2.Group.name" ></p>
                <p ng-bind="gorup_productivity2">%</p>
            </div>

            <div class="col-sm-4">
                <p class="g-desk-tile-user-name p-margin-0" ng-bind="group3.Group.name" ></p>
                <p ng-bind="gorup_productivity3">%</p>
            </div>

        </div>

        <div class="row">

            <div class="col-sm-6">
                <canvas tc-chartjs-doughnut chart-options="optionsDoughnut" chart-data="dataDoughnut"></canvas>

            </div>

            <div class="col-sm-6">
                <!--                For timeline like perfomace good or bad-->
                <p>Average delay is </p> <p ng-bind="avg_delay"></p>
                <p>Average delay for group </p> <p ng-bind="grp_avg_delay"></p>
                <p>Average delay for Network</p> <p ng-bind="network_avg_delay"></p>
            </div>

        </div>

        <!--For network Search option-->
        <div>
            Select User:
            <div class="dropdown">
                <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-expanded="true">
                    Users
                    <span class="caret"></span>
                </button>
                <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
                    <li role="presentation" ng-repeat="networkuser in networkusers">
                        <span ng-click="selectuserNetwork(networkuser.UserInfo.user_id)"> <a role="menuitem" tabindex="-1" href="#" ng-bind="networkuser.UserInfo.name"></a></span>
                    </li>
                </ul>
            </div>
        </div>
        <div class="col-sm-4 col-sm-offset-1">
            <canvas tc-chartjs-bar id="mycanvas" chart-options="optionsBar" chart-data="usernetworkdataBar"></canvas>      
        </div>
        <div class="col-sm-3">
            <canvas tc-chartjs-doughnut chart-options="optionsDoughnut" chart-data="usernetworkdataDoughnut"></canvas>
        </div>
        <div>
            <canvas tc-chartjs-pie chart-options="optionspie" chart-data="usernetworkdatapiedatapie"></canvas>
        </div>
    </div>
</div>
</div>
</div>
<!--main container end-->
<?php echo $this->element('Network/adduser'); ?>
<script>
    $('.dashboard-menu-item[data-target]').on('click', function ()
    {
        $('.dashboard-content').addClass('hide');
        var id = $(this).data('target');
        $(id).removeClass('hide');
        $('.dashboard-menu-item[data-target]').removeClass('active');
        $(this).addClass('active');


    });

    $('.dashboard-content').addClass('hide');
    $('#network-people').removeClass('hide');
    $('.dashboard-menu-item[data-target=#members]').addClass('active');
</script>
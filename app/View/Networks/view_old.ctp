<?php
$this->layout = 'gridle_in';
$this->set('title_for_layout', $Networks[$CurrentNetwork]["name"] . ' Dashboard');
?>
<!--main container-->
<div class="container-fluid" ng-controller="networkController">
    <div class="g-dashboard-wrap row" ng-controller="analyticscontroller">
        <!--Dashboard menu--> 
        <div class="col-sm-2 dashboard-menu font-big-1 g-background-offwhite text-emphasize">
            <!--Network name--> 
            <div class="dashboard-menu-item dashboard-network-name row">
                <div class="col-sm-10 padding-lr-0">
                    <!--    network name        -->

                    <input class="ta-network-name form-control" ng-model="networks[currentnetwork].name" ng-disabled="!editMode" ng-pattern="/^[\w\d ]+/" >
                </div>
                <div class="col-sm-1 padding-lr-0">
                    <!--    EDIT NETWORK INFO      -->
                    <span ng-show="user_networks[findUserNetwork(loggeduser)].UserNetwork.role == 'owner'">
                        <button class="btn-link" ng-show="!editMode" ng-click="oldName = networks[currentnetwork].name;
                                    editMode = !editMode;" title="Rename Network" tooltip ><i class="fa fa-pencil"></i></button>
                    </span>
                </div>
                <div class="col-sm-10 padding-lr-0">
                    <button class='btn btn-sm btn-default' ng-show="editMode" ng-click='editMode = !editMode;
                                networks[currentnetwork].name = oldName'>Cancel</button>
                    <button class="btn btn-primary btn-sm"  ng-show="editMode" ng-click="editMode = !editMode;
                                editNetwork(networks[currentnetwork].name, editMode, oldName)">Save</button>
                </div>
            </div>
            <!--Menu items--> 
            <div data-target="#network-stats">
                <div class="p-margin-0 dashboard-menu-item" data-toggle="collapse" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne" id="collapseMenu"><p ng-click="toogleSelection('Analytics')">Analytics<span class="caret"></span><br/></p>
                </div>
                    <div id="collapseOne" class="panel-collapse collapse">
                     <p ng-click="toogleSelection('Analytics');selectpersonal()" class="menu-collapse" ng-class="{'active': selected == 1}"> Personal analytics</p>
                     <p ng-show="!admingroups.length==0" class="menu-collapse" ng-click="toogleSelection('Analytics');selectgroup();" ng-class="{'active': selected == 2}"> Group analytics</p>
                    <div ng-show="(user_networks[findUserNetwork(loggeduser)].UserNetwork.role == 'admin' || user_networks[findUserNetwork(loggeduser)].UserNetwork.role == 'owner')"> 
                    <p ng-click="toogleSelection('Analytics');selectnetowrk()" href="#" class="menu-collapse" ng-class="{'active': selected == 3}"> Network analytics</p>
                    <p href="#" ng-click="toogleSelection('Analytics');selectindividual()" class="menu-collapse" ng-class="{'active': selected == 4}">Individual Analytics</p>
                    </div>
                    </div> 
            </div>
            <div class="dashboard-menu-item " data-target="#network-people" ng-class="{'active': manageSelected == true}">
                <p class="p-margin-0" ng-click="toogleSelection('manage')">
                    <!-- if admin or owner  -->
                    <span ng-show="user_networks[findUserNetwork(loggeduser)].UserNetwork.role == 'admin' || user_networks[findUserNetwork(loggeduser)].UserNetwork.role == 'owner'">Manage</span>
                    <!-- if not admin    -->
                    <span ng-show="user_networks[findUserNetwork(loggeduser)].UserNetwork.role == 'user'">People</span>
                </p>
            </div>


            <!--            <div class="dashboard-menu-item" ng-if='user_networks[findUserNetwork(loggeduser)].UserNetwork.role === "owner"'>
                            <a href="/networks/security"><p class="p-margin-0">Access Control</p></a>
                        </div>-->
            <div class="dashboard-menu-item" ng-if='user_networks[findUserNetwork(loggeduser)].UserNetwork.role === "owner"'>
                <?php echo $this->Form->postLink(__('Delete Network'), array('action' => 'delete', $CurrentNetwork), array('class' => ''), __('Deleting a Network is permanent. Are you sure you want to delete  network "%s"? There is no undo.', $Networks[$CurrentNetwork]['name'])); ?>
            </div>
        </div>
        <div class="col-sm-10 dashboard-expands">

            <!--    NETWORK STATS      -->

            <div id="network-stats" class="network-stats-area dashboard-content container-fluid" ng-hide="manageSelected">
                <!--First section of network, group and user--> 
                <div class="row analytics-partition" style="padding-bottom: 10px">
                    <!--Personal Start-->
                    <div ng-show="selected == 1">
                        <div class="col-sm-4">
                            <p class="font-big-3 g-color-white text-emphasize"><?= $AuthUser['UserInfo']['first_name']; ?>'s Analytics</p>
                        </div>
                        
                    </div>
                    <!--Personal End-->
                    
                    <!--Group start-->
                    <div ng-show="selected == 2">
                        <div class="col-sm-4">
                            <p class="font-big-3 g-color-white text-emphasize"><span ng-bind="currentgroup"></span> Group Analytics</p>
                        </div>
                        <div class="col-sm-4">
                            <div class="dropdown">
                            <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-expanded="true">
                                Select A Group
                                <span class="caret"></span>
                            </button>
                            <ul class="g-header-dropdown-menu dropdown-menu">
                                <span ng-repeat="group in admingroups">
                                    <li class="text-center set-cursor">
                                        <a class="g-header-dropdown-link" ng-bind="group.Group.name" ng-click="getGroupstats(group.Group.id,group.Group.name)">
                                        </a>
                                    </li>
                                    <li class="divider" ng-show="groups[$index+1]"></li>
                                </span> 
                            </ul>
                        </div>
                        </div>
                    </div>
                    <!--Group End-->
                    
                    <!--Network Start-->
                    <div ng-show="selected == 3">
                        <div class="col-sm-4">
                            <p class="font-big-3 g-color-white text-emphasize">Network Analytics</p>
                        </div>
                    </div>
                    <!--Network End-->
                    
                    <!--Individual Start-->
                    <div ng-show="selected == 4">
                        <div class="col-sm-4">
                            <p class="font-big-3 g-color-white text-emphasize"><span ng-bind="usernetworkplaceholder"></span> Analytics</p>
                        </div>
                        <div class="col-sm-4">
                            <tags-input ng-model="tags" add-on-enter="true" add-from-autocomplete-only="true" max-tags="1" on-tag-added="userNetworkAutoComplete(tags)" display-property="name" placeholder="Search People" allow-leftover-text="false" on-tag-removed="initializeusernetworkchart()">                <auto-complete source="loadanalyticsTags($query)" min-length="2"></auto-complete>
                        </tags-input>
                        </div>
                    </div>
                   <!--Individual End-->
                        
                    
                </div>
                <!--Second section-->
            <div ng-hide="selected == 4"> <!--   This is to hide the network stats and show personal-->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="col-sm-12 analytics-partition g-bg-white-transparent">
                            <div class="col-sm-8 padding-lr-0">
                                <div class="g-background-offwhite graph-area" style="height: 400px; margin: 10px;">  
                                    <!-- Bar Chart-->
                                    <canvas tc-chartjs-bar id="mycanvas" chart-options="optionsBar" chart-data="dataBar"></canvas>
                                </div>

                            </div>
                            <div class="col-sm-4 padding-lr-0">
                                <div class=" analytics-col g-background-offwhite" style="height: 400px; margin: 10px;">
                                    <!--graph area-->
                                    <div class="text-bold text-center" style="height:200px;padding-left: 20%; padding-right: 20%;"><p class="g-color-dark-blue" style="font-size: 80px; line-height: 200px; border-bottom:1px solid lightgray;"><span ng-bind="productivity" ng-show="selected == 1"></span> 
                                            <span ng-bind="grp_productivity" ng-show="selected == 2"></span>
                                            <span ng-bind="network_productivity" ng-show="selected == 3"></span>
                                            <span>%</span></p>
                                    </div>
                                    <!--content--> 
                                    <div class="text-justify analytics-content text-muted text-emphasize" style="height:200px;">
                                        This is your productivity percentage. Productiviy percentage gives you an idea of how your work hour is being efficiently utilized. This number is caluculated taking your acitivity on gridle related to the tasks and the deadlines.
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <!--Third section--> 
                <div class="row analytics-partition">
                    <div class="col-sm-6">
                        <div class=" analytics-col g-background-offwhite" style="height: 300px;">                    
                            <!--graph area--> 
                            <div class="col-sm-8 graph-area" style="height:300px; padding-top: 60px;">
                                <canvas tc-chartjs-doughnut chart-options="optionsDoughnut" chart-data="dataDoughnut"></canvas>
                            </div>
                            <!--content--> 
                            <div class="text-justify text-emphasize analytics-content text-muted col-sm-4" style="height:300px; padding: 20px;">
                                Your activity on Gridle with respect to your total tasks and tasks completed on or before deadlines. The more are the tasks completed before or on deadlines, the better would be your productivity on Gridle.
                            </div>
                        </div>

                    </div>
                    <div class="col-sm-6">
                        <div class="analytics-col g-background-offwhite" style="height: 300px;">                    
                            <!--graph area--> 
                            <div class="g-color-dark-blue col-sm-8 text-emphasize font-big-4 graph-area text-center" style="height:300px; padding-top: 80px;">
                                <p ng-show="selected == 1">
                                    <span >Average delay is </span><br/>
                                    <span class="g-color-dark-blue" ng-bind="avg_delay"></span>
                                </p>
                                <p ng-show="selected == 2">
                                    <span >Average delay of group is</span><br/>
                                    <span ng-bind="grp_avg_delay"></span>
                                </p>
                                <p ng-show="selected == 3">
                                    <span >Average delay of Network is</span><br/>
                                    <span ng-bind="network_avg_delay"></span>
                                </p>
                            </div>
                            <!--content--> 
                            <div class="text-emphasize analytics-content text-muted col-sm-4" style="height:300px;">
                                This number is an approximation of your average delay on completing a task. The longer you take to complete a single task, would reflect on this number.
                            </div>
                        </div>

                    </div>
                </div>
                <!--Fourth section--> 
                <div class="row analytics-partition">
                    
                    <div ng-show="selected !== 2">
                        <p class="top-text">
                            Top Three users of your network
                        </p>
                    </div>
                    
                    <div ng-show="selected === 2">
                        <p class="top-text">
                            Top Three Groups of your network
                        </p>
                    </div>
                    
                    <div class="col-sm-4">
                        <div class="analytics-col g-background-offwhite graph-area" style="">                              
                            <div ng-show="selected !== 2">                      
                                <g-profile-img type="user" ng-if="user1" path="{{user1.UserInfo.vga}}" height="40px" width="40px" myclass="analytics-top-user-img" name="{{user1.UserInfo.name}}"  ></g-profile-img>
                                <p class="text-emphasize font-big-1 text-capitalize g-desk-tile-user-name p-margin-0" ng-bind="user1.UserInfo.name" ></p>
                                <p class="text-semi-bold g-color-dark-blue font-big-4" style="margin-left:50px;"><span ng-bind="user_productivity1"></span> %</p>
                            </div>
                            <div ng-show="selected === 2" ng-if="topgrouplength >= 1">
                                <p class="text-emphasize font-big-1 text-capitalize g-desk-tile-user-name p-margin-0" ng-bind="group1.Group.name"></p>
                                <p class="text-semi-bold g-color-dark-blue font-big-4" style="margin-left:50px;"><span ng-bind="group_productivity1"></span> %</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="analytics-col g-background-offwhite graph-area" style="">                             <div ng-show="selected != 2">
                                <g-profile-img ng-if="user2" type="user"  path="{{user2.UserInfo.vga}}" height="25px" width="25px" myclass="analytics-top-user-img" name="{{user2.UserInfo.name}}"  ></g-profile-img>
                                <p class="text-emphasize font-big-1 text-capitalize g-desk-tile-user-name p-margin-0" ng-bind="user2.UserInfo.name" ></p>
                                <p class="text-semi-bold g-color-dark-blue font-big-4" style="margin-left:50px;"><span ng-bind="user_productivity2"></span> %</p>
                     </div>
                            <span ng-show="selected == 2" ng-if="topgrouplength >= 2">
                                <p class="text-emphasize font-big-1 text-capitalize g-desk-tile-user-name p-margin-0" ng-bind="group2.Group.name" ></p>
                                <p class="text-semi-bold g-color-dark-blue font-big-4" style="margin-left:50px;"><span ng-bind="group_productivity2"></span> %</p>
                            </span>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="analytics-col g-background-offwhite graph-area" style="">                              <div ng-show="selected != 2">  
                                <g-profile-img ng-if="user3" type="user"  path="{{user3.UserInfo.vga}}" height="25px" width="25px" myclass="analytics-top-user-img" name="{{user3.UserInfo.name}}"  ></g-profile-img>
                                <p class="text-emphasize font-big-1 text-capitalize g-desk-tile-user-name p-margin-0" ng-bind="user3.UserInfo.name" ></p>
                                <p class="text-semi-bold g-color-dark-blue font-big-4" style="margin-left:50px;"><span ng-bind="user_productivity3"></span> %</p>
                     </div>
                            <div ng-show="selected == 2" ng-if="topgrouplength == 3">
                                <p class="text-emphasize font-big-1 text-capitalize g-desk-tile-user-name p-margin-0" ng-bind="group3.Group.name" ></p>
                                <p class="text-semi-bold g-color-dark-blue font-big-4" style="margin-left:50px;"><span ng-bind="group_productivity3"></span> %</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
                <!--Fifth section only for Network analytics only for admin-->
                      
           <div ng-show="selected == 4">
                      <!--This section is for all the charts for that particular user-->  
                      <!--The barchart section start-->
                  <div class="row">
                    <div class="col-sm-12">
                        <div class="col-sm-12 analytics-partition g-bg-white-transparent">
                            <div class="col-sm-8 padding-lr-0">
                                <div class="g-background-offwhite graph-area" style="height: 400px; margin: 10px;">  
                                    <!-- Bar Chart-->
                                    <canvas tc-chartjs-bar id="mycanvas" chart-options="optionsBar" chart-data="usernetworkdataBar"></canvas>
                                </div>

                            </div>
                            <div class="col-sm-4 padding-lr-0">
                                <div class=" analytics-col g-background-offwhite" style="height: 400px; margin: 10px;">
                                    <!--graph area-->
                                    <div class="text-bold text-center" style="height:200px;padding-left: 20%; padding-right: 20%;"><p class="g-color-dark-blue" style="font-size: 80px; line-height: 200px; border-bottom:1px solid lightgray;">
                                            <span ng-bind="usernetwork_productivity"></span> 
                                            <span>%</span></p>
                                    </div>
                                    <!--content--> 
                                    <div class="text-justify analytics-content text-muted text-emphasize" style="height:200px;">
                                        This is your productivity percentage. Productiviy percentage gives you an idea of how your work hour is being efficiently utilized. This number is caluculated taking your acitivity on gridle related to the tasks and the deadlines.
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                      <!--The barchart section complete-->
                      
                      <!--third section of donut and average delay section start-->
                  <div class="row analytics-partition">
                    <div class="col-sm-6">
                        <div class=" analytics-col g-background-offwhite" style="height: 300px;">                    
                            <!--graph area--> 
                            <div class="col-sm-8 graph-area" style="height:300px; padding-top: 60px;">
                                <canvas tc-chartjs-doughnut chart-options="optionsDoughnut" chart-data="usernetworkdataDoughnut"></canvas>
                            </div>
                            <!--content--> 
                            <div class="text-justify text-emphasize analytics-content text-muted col-sm-4" style="height:300px; padding: 20px;">
                                Your activity on Gridle with respect to your total tasks and tasks completed on or before deadlines. The more are the tasks completed before or on deadlines, the better would be your productivity on Gridle.
                            </div>
                        </div>

                    </div>
                    <div class="col-sm-6">
                        <div class="analytics-col g-background-offwhite" style="height: 300px;">                    
                            <!--graph area--> 
                            <div class="g-color-dark-blue col-sm-8 text-emphasize font-big-4 graph-area text-center" style="height:300px; padding-top: 80px;">
                                <p>
                                    <span >Average delay is </span><br/>
                                    <span class="g-color-dark-blue" ng-bind="usernetwork_avg_delay"></span>
                                </p>
                            </div>
                            <!--content--> 
                            <div class="text-emphasize analytics-content text-muted col-sm-4" style="height:300px;">
                                This number is an approximation of your average delay on completing a task. The longer you take to complete a single task, would reflect on this number.
                            </div>
                        </div>

                    </div>
                </div>
                      <!--third section of donut and average delay section end-->
                    </div>
                </div>
        
            <!--    NETWORK PEOPLE LISTING      -->
            <div id="network-people" class="network-people-area dashboard-content container-fluid" ng-show="manageSelected">
                 <div class='network-people-search row'>
                    <div class="col-lg-2 col-md-2 col-sm-6 col-xs-6 ">
                        <p class='g-color-white'> <strong ng-bind="networks[currentnetwork]['user_count']"></strong>/ <strong ng-bind="networks[currentnetwork]['user_limit']"></strong> members</p>
                    </div>
                    <div class='col-lg-4 col-lg-offset-2 col-md-6 col-md-offset-2 col-sm-6 col-xs-6'>
                        <div class='form-group'>
                            <div class='input-group input-group-sm'>
                                <input class='g-search-box form-control' type='text' placeholder='Search for people by name/role' ng-model="searchMember"/>
                                <div class='input-group-addon-round input-group-addon'>
                                    <i class='fa fa-search'></i>
                                </div>
                            </div>
                        </div>
                    </div>                    
                    <!--    INVITE TO NETWORKS      -->
                    <div class="col-lg-2 col-lg-offset-2 col-md-6 col-sm-6 col-xs-6 pull-right">
                        &nbsp;&nbsp;
                        <button class="btn btn-success btn-sm" data-toggle="modal" data-target="#networkModalAddMembers" ng-show="user_networks[findUserNetwork(loggeduser)].UserNetwork.role == 'admin' || user_networks[findUserNetwork(loggeduser)].UserNetwork.role == 'owner'">Invite to network</button>
                    </div>
                </div>
                <div class="network-people-listing row">

                    <div class="network-people-tile-cover col-lg-3 col-md-3 col-sm-4 col-xs-6" ng-repeat="user in user_networks| orderBy:['UserInfo.name'] | orderBy:orderByUserNetworkRole |filter:searchMember">

                        <!--    USER TILE       -->
                        <div class="network-people-tile">
                            <div class='network-people-info' ng-class="{'network-owner-info': user.UserNetwork.role === 'owner'}">
                                <!--    USER IMAGE      -->
                                <div class="network-people-img-cover">
                                    <p class="p-margin-0">
                                    <g-profile-img type='user' path="{{user.UserInfo.vga}}" name="{{user.UserInfo.name}}" myclass="{{ user.UserNetwork.role === 'owner' ? 'network-owner-img network-ppl-img center-block font-big-3' : user.UserNetwork.role === 'admin' ? 'network-admin-img network-ppl-img center-block font-big-3' : 'network-ppl-img center-block font-big-3'   }}" width="64px" height="64px"  ></g-profile-img>
                                    </p>
                                </div>
                                <!--    USER NAME       -->
                                <div class="network-people-name text-uppercase">
                                    <p class="p-margin-0 font-big-2 btn-link" ng-bind="user.UserInfo.name" ng-click="showUserInfoModal(user.UserInfo.user_id);"></p>
                                </div>
                                <!--    USER ROLE       -->
                                <div class="network-people-role text-muted text-uppercase font-small-1">
                                    <p class=""> {{user.UserNetwork.role}} </p>
                                </div>
                            </div>
                            <!--    OPTIONS -->
                            <div class="network-people-info font-small-1" ng-show="(user_networks[findUserNetwork(loggeduser)].UserNetwork.role == 'admin' || user_networks[findUserNetwork(loggeduser)].UserNetwork.role == 'owner') && user.UserNetwork.role != 'owner'">
                                <p class="p-margin-0" ng-if='user.UserNetwork.role !== "owner"'>
                                    <span class="btn-link" ng-show="(user_networks[findUserNetwork(loggeduser)].UserNetwork.role == 'owner') && user.UserNetwork.role == 'user'" ng-click="assignRole(user.UserInfo.user_id, 'admin')" >Make admin</span>
                                    <span class='btn-link' ng-show="(user_networks[findUserNetwork(loggeduser)].UserNetwork.role == 'owner') && user.UserNetwork.role == 'admin'" ng-click="assignRole(user.UserInfo.user_id, 'user')" >Remove admin</span>
                                </p>
                                <p class="p-margin-0" ng-if='user.UserNetwork.role !== "owner"'>
                                    <span class="btn-link" ng-show="((user_networks[findUserNetwork(loggeduser)].UserNetwork.role == 'admin' && user.UserNetwork.role != 'admin') || user_networks[findUserNetwork(loggeduser)].UserNetwork.role == 'owner') && user.UserNetwork.role != 'owner'" ng-click="removeUserNetworkConfirm(user.UserInfo.user_id)" >Remove from network</span>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>  
            </div>
        </div>
    </div>
</div>

<?php echo $this->element('Network/adduser'); ?>
<script>
//    $('.dashboard-menu-item[data-target]').on('click', function ()
//    {
//        $('.dashboard-content').addClass('hide');
//        var id = $(this).data('target');
//        $(id).removeClass('hide');
//        $('.dashboard-menu-item[data-target]').removeClass('active');
//        $(this).addClass('active');
//    });
//    $('.dashboard-content').addClass('hide');
//    $('#network-stats').removeClass('hide');
//    $('.dashboard-menu-item[data-target=#members]').addClass('active');
    
</script>
<style>
    .menu-collapse{
        padding-left: 10px;
        line-height: 40px;
        background-color: white;
        margin-bottom: 0px;
        cursor: pointer;
    }  
    
    .graph-area{
        padding: 10px;
        overflow: hidden;
    }
    
    .analytics-top-user-img{
        height: 50px;
        width: 50px;
        line-height: 50px;
        background-color: #446580;
        color:white;
        text-align: center;
        font-size: 1.2em;
    }
    
    .set-cursor{
        cursor:pointer;    
    }
    
    .top-text{
        color: white;
        font-family: 'proxima-nova-semi-bold',sans-serif;
        font-size: 24px;
        padding-left: 15px;
    }
</style>
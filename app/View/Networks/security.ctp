<?php
$this->layout = 'gridle_in';
$this->set('title_for_layout', 'Security');
?>
<div class='container-fluid'>
    <div class="row" style="margin:20px">

        <div class="col-xs-8 col-xs-offset-2">
            <blockquote class="success">
                ACCESS CONTROL
            </blockquote>
            All your network access criteria are listed here with details for review. Only you (owner of the network) will be able to access your network from anywhere independent of the restrictions.
            <table class="table">
                <tbody>
                    <?php echo $this->Form->create('NetworkAccess', array('name' => 'NetworkAccessAdd', 'class' => 'col-xs-6 col-xs-offset-3 form-inline')); ?>
                    <tr class="active">

                        <th>                            
                            <?php
                            echo $this->Form->input('name', array('class' => 'form-control', 'placeholder' => 'Name', 'required' => false, 'div' => FALSE, 'label' => FALSE));
                            ?>
                        </th>
                        <th>
                            <span class="fa fa-info-circle"  data-toggle="tooltip" data-placement="right" title='This field is for your internal reference only. It should be something that describes the restriction. It could be location of your office or name of the person. For ex: Head office/John Doe. It has no effect on restrictions and is not mandatory.'   style="margin:5px 0;cursor:help">
                            </span>
                        </th>
                        <th>
                            <label class="control-label sr-only" for="inputSuccess5">Hidden label</label>
                            <?php
                            echo $this->Form->input('ip_address', array('class' => 'form-control', 'placeholder' => 'IP Address', 'div' => FALSE, 'label' => false, 'pattern' => '((^|\.)((25[0-5])|(2[0-4]\d)|(1\d\d)|([1-9]?\d))){4}$')
                            );
                            echo '<label id="setcurrentip" style="margin:5px;cursor:pointer" class="label label-info pull-right">Current IP address</label>';
                            echo '<label id="setallip" style="margin:5px;cursor:pointer" class="label label-info pull-right">Global Access</label>';
                            ?>

                        </th>
                        <th>
                            <span class="fa fa-info-circle"  data-toggle="tooltip" data-placement="right" title='Access to your network will be restricted to the IP address you fill here. Select "Global Access" for no restrictions. Select "Current IP address" to autofill your current IP address. Otherwise,enter a custom IP.'   style="margin:5px 0;cursor:help">
                            </span>
                        </th>
                        <th>

                            <?php
                            echo $this->Form->input('role', array('type' => 'select', 'class' => 'form-control', 'placeholder' => 'Role', 'div' => FALSE, 'options' => $roles, 'label' => false)
                            );
                            echo $this->Form->input('user_id', array('class' => 'form-control  input-sm', 'div' => FALSE, 'label' => false, 'style' => 'margin: 5px auto;'));

//                    echo '<i class="fa fa-info-circle pull-right" style="margin:5px;cursor:help" data-toggle="tooltip" data-placement="right" title="Tooltip on right"></i>';
                            ?>


                        </th>
                        <th>
                            <span class="fa fa-info-circle"  data-toggle="tooltip" data-placement="right" title='Choosing "All users" will apply to everyone in the network, "All admins" to only admins and "Select user" will allow you to manage exceptions. You can allow or restrict access of specific people in your network.'  style="margin:5px 0;cursor:help"></span>
                        </th>
                        <th> 
                            <?php echo $this->Form->submit(__d('users', 'Add'), array('class' => 'btn btn-success ', 'div' => FALSE)); ?>
                        </th>
                    </tr>

                    <?php
                    echo $this->Form->end();
                    ?>
                </tbody>
            </table>



            <table class="table table-hover ">
                <thead>
                    <tr  class="active">
                        <th>Name</th>
                        <th>IP address</th>
                        <th>Allowed to</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    foreach ($NetworkAccess as $key => $value) {
                        ?>
                        <tr>                            
                            <td><?= $value['NetworkAccess']['name'] ?></td>
                            <td><?= $value['NetworkAccess']['ip_address'] ?></td>
                            <td>
                                <?php if ($value['NetworkAccess']['role'] != ROLE_SPECIAL) {
                                    ?>
                                    All <strong><?= $value['NetworkAccess']['role'] ?></strong>

                                    <?php
                                } else {
                                    ?>
                                    <?= $users[$value['NetworkAccess']['user_id']] ?>
                                <?php }
                                ?>
                            </td>
                            <td><?php echo $this->Form->postLink(__('Delete'), array('action' => 'security_delete', $value['NetworkAccess']['id']), array('class' => ''), __('Are you sure you want to delete security group %s ?', $value['NetworkAccess']['name']));
                                ?></td>

                        </tr>
                        <?php
                    }
                    ?>
                </tbody>
            </table>
        </div>

    </div>

</div>
<script>
    var current_ip_address = '<?= isset($current_ip_address) ? $current_ip_address : NULL ?>';
    var all_ip_address = '<?= ALLOW_FROM_ALL_IP ?>';
    function onSpecialRole()
    {
        var current_role = $("#NetworkAccessRole option:selected").text();
        if (current_role == "<?php echo $roles[ROLE_SPECIAL] ?>")
        {
            $('#NetworkAccessUserId').fadeIn();
        }
        else
        {
            $('#NetworkAccessUserId').fadeOut(0);

        }
    }

    onSpecialRole();

    $('#NetworkAccessRole').on('change', onSpecialRole);

    $('#setcurrentip').click(function(e)
    {
        e.preventDefault();
        $('input#NetworkAccessIpAddress').val(current_ip_address);
    });

    $('#setallip').click(function(e)
    {
        e.preventDefault();
        $('input#NetworkAccessIpAddress').val(all_ip_address);
    });

    $(function() {
        $("[data-toggle='tooltip']").tooltip();
    });

</script>



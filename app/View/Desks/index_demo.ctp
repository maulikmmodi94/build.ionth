<?php
$this->layout = 'layout_in_demo';
$this->set('title_for_layout', 'Desk');
?>
<div class="container-fluid" ng-controller="tasksController">
    <div class="row">
        <div class="col-sm-8">
            <div class="container-fluid">
                <!--NEW TASK FORM--> 
                <div class="tasks-input-box row">
                    <div class="form-group task-input">
                        <textarea class="form-control" 
                                  rows =" 1"
                                  placeholder="Create a task. Ex: Meet @Stacy for coffee at 9 AM tomorrow." 
                                  ng-model="currentTask" 
                                  ng-change="checkPatterns(currentTask)"
                                  ng-enter="enterNewTaskActivate ? taskAdd(TaskSelected) : false "
                                  mentio
                                  mentio-select-not-found="true"
                                  mention-typed-term="currentMention"
                                  mentio-require-leading-space="true"
                                  mentio-id="'htmlContent'" ></textarea>
                        <button class='btn btn-sm btnMakeTask'>Create task</button>
                        <mentio-menu
                            mentio-for="'htmlContent'"
                            mentio-trigger-char="'@'"
                            mentio-items="mentionSuggestions"
                            mentio-template-url="/js/mention/mentio-menu.tpl.html"
                            mentio-select="getPeopleText(item)"
                            mentio-search="searchPeople(term, currentMention)"
                            ></mentio-menu>
                        <div class="container-fluid task-suggestion-card" ng-show="currentTask.length > 0">
                            <div class="row font-small-1">
                                <div class="suggestion-card col-sm-6">
                                    <p class="font-small-2 text-muted">Task</p>
                                    <p ng-bind="currentTask"></p>
                                </div>
                                <div class="col-sm-3 suggestion-card">
                                    <p class="font-small-2 text-muted" >Shared with</p>
                                    <p>Yash Shah</p>
                                </div>
                                <div class="suggestion-card col-sm-3">
                                    <p class="font-small-2 text-muted" >Deadline</p>
                                    <!--<p ng-bind="dateFormat(FoundEndDate, 'medium')"></p>-->
                                    <p class="font-small-3" ng-bind="FoundEndDate"></p>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--SEARCH AREA--> 
                <div class="row">
                    <div class="form-group col-sm-6 col-sm-offset-3">
                        <input class="form-control" placeholder="Search a task" />
                    </div>
                </div>
                <!--FILTER ON--> 
                <div class=" row" ng-show="taskFilterOn">
                    <div class="bg-blue color-white filter-bar">
                        <p>Showing tasks with <span class="text-bold">{{selectedUser.name}}</span> <span class="pull-right text-bold cursor-pointer" ng-click="clearFilter()">&times;</span></p>
                    </div>
                </div>
                <div jrg-datetimepicker ng-model='ngModel' validate='validateDate' ng-change='onchangeDate' opts='opts'></div>

                <!--TASKS LSIT--> 
                <div class="tasks-list row">
                    <!--SINGLE TASK--> 
                    <div class="singleTask container-fluid">
                        <!--Date--> 
                        <div class="task-date-head row">
                            <div class="col-sm-5 date-lines"></div>
                            <div class="col-sm-2 padding-lr-0">
                                <p class="text-center">Today</p>
                            </div>
                            <div class="col-sm-5 date-lines"></div>
                        </div>
                        <!--Task detail--> 
                        <div class="taskDetails row">
                            <div class="col-sm-1">
                                <img class="round-25" src="/img/demo/anupama.png" />
                            </div>
                            <div class="col-sm-8 padding-lr-0">
                                <div>
                                    <p class="text-bold font-small-3 cursor-pointer" ng-click="showProfile()">Anupama Panchal, 
                                        <span class="text-muted">today</span>
                                    </p>
                                    <p class="task-title">Content of the following pages, contact us, signup, login, resend verification, forgot password, tour, together we can.</p>

                                </div>
                            </div>
                            <div class="col-sm-2 col-sm-offset-1">
                                <li class="dropdown pull-right margin-left-10" dropdown>
                                    <p class="pull-right dropdown-toggle cursor-pointer" dropdown-toggle>
                                        <span class="fa fa-gear"></span>
                                    </p>
                                    <ul class="dropdown-menu task-settings-menu dropdown-menu-right text-center" role="menu">
                                        <li class="" ng-click="openTaskModal()"><span class="fa fa-gears"></span> Edit Task</li>
                                        <li class="" ng-click="openDeleteTaskModal()" ><span class="fa fa-trash-o"></span> Delete Task</li>

                                    </ul>
                                </li>
                                <span class="fa fa-bookmark color-red pull-right"></span>
                            </div>
                        </div>
                        <div class="taskShare row">
                            <div class="col-sm-7 col-sm-offset-1 padding-lr-0">
                                <p class="">
                                    <img class="round-25 shared-img" src="/img/demo/anupama.png" /> <img class="round-25 shared-img" src="/img/demo/yash.jpg" /> <img class="round-25 shared-img" src="/img/demo/leena.jpg" /> <img class="round-25 shared-img" src="/img/demo/ishani.jpg" /> <span class="text-muted">+4 more</span>
                                </p>
                            </div>
                            <div class="col-sm-3 col-sm-offset-1">
                                <button class="btn-mark-complete btn btn-sm btn-green btn-block font-small-2">Mark done</button>
                            </div>
                        </div>
                        <!--task comments--> 
                        <div class="row">
                            <div class="container-fluid task-comments-box">
                                <div class="row">
                                    <div class="col-sm-1 padding-right-0">
                                        <img class="round-25 shared-img" src="/img/demo/anupama.png" />
                                    </div>
                                    <div class="col-sm-11 padding-left-0">
                                        <div class="form-group">
                                            <input class="form-control input-sm" placeholder="Write a comment and press enter." />
                                        </div>
                                    </div>

                                </div>
                                <div class="row task-comment">
                                    <div class="col-sm-1">
                                        <img class="round-25 shared-img" src="/img/demo/leena.jpg" />
                                    </div>
                                    <div class="col-sm-11 padding-left-0">
                                        <div class="form-group">
                                            <p class="font-small-2">Dude, you still need to send me the post <span class="pull-right">&times;</span></p>
                                            <p class="text-muted font-small-4">2 minutes ago</p>
                                        </div>
                                    </div>  
                                </div>
                                <div class="row task-comment">
                                    <div class="col-sm-1">
                                        <img class="round-25 shared-img" src="/img/demo/yash.jpg" />
                                    </div>
                                    <div class="col-sm-11 padding-left-0">
                                        <div class="form-group">
                                            <p class="font-small-2">Send me the post as soon as you get my message <span class="pull-right">&times;</span></p>
                                            <p class="text-muted font-small-4">2 minutes ago</p>
                                        </div>
                                    </div>  
                                </div>        
                            </div>
                        </div>

                    </div>
                    <!--SINGLE TASK END--> 
                    <!--SINGLE TASK--> 
                    <div class="singleTask container-fluid">
                        <!--Date--> 
                        <!--                        <div class="task-date-head row">
                                                    <p class="text-center">Today</p>
                                                </div>-->
                        <!--Task detail--> 
                        <div class="taskDetails row">
                            <div class="col-sm-1">
                                <img class="round-30" src="/img/demo/anupama.png" />
                            </div>
                            <div class="col-sm-8 padding-lr-0">
                                <div>
                                    <p class="text-bold font-small-3">Anupama Panchal, <span class="text-muted">today</span></p>
                                    <p class="task-title">Content of the following pages, contact us, signup, login, resend verification, forgot password, tour, together we can.</p>

                                </div>
                            </div>
                            <div class="col-sm-2 col-sm-offset-1">                              <li class="dropdown pull-right margin-left-10" dropdown>
                                    <p class="pull-right dropdown-toggle cursor-pointer" dropdown-toggle>
                                        <span class="fa fa-gear"></span>
                                    </p>
                                    <ul class="dropdown-menu task-settings-menu dropdown-menu-right text-center" role="menu">
                                        <li class="" ng-click="openTaskModal()"><span class="fa fa-gears"></span> Edit Task</li>
                                        <li class="" ng-click="openDeleteTaskModal()" ><span class="fa fa-trash-o"></span> Delete Task</li>

                                    </ul>
                                </li>
                                <span class="fa fa-bookmark color-red pull-right"></span>
                            </div>
                        </div>
                        <div class="taskShare row">
                            <div class="col-sm-7 col-sm-offset-1 padding-lr-0">
                                <p class="">
                                    <img class="round-30 shared-img" src="/img/demo/anupama.png" /> <img class="round-30 shared-img" src="/img/demo/yash.jpg" /> <img class="round-30 shared-img" src="/img/demo/leena.jpg" /> <img class="round-30 shared-img" src="/img/demo/ishani.jpg" /> <span class="text-muted">+4 more</span>
                                </p>
                            </div>
                            <div class="col-sm-3 col-sm-offset-1">
                                <button class="btn-mark-complete btn btn-sm btn-green btn-block font-small-2">Mark done</button>
                            </div>
                        </div>
                        <!--task comments--> 
                        <div class="row">
                            <div class="container-fluid task-comments-box">
                                <div class="row">
                                    <div class="col-sm-1">
                                        <img class="round-25 shared-img" src="/img/demo/anupama.png" />
                                    </div>
                                    <div class="col-sm-10 padding-lr-0">
                                        <div class="form-group">
                                            <input class="form-control input-sm" placeholder="Write a comment and press enter." />
                                        </div>
                                    </div>

                                </div>
                                <div class="row task-comment">
                                    <div class="col-sm-1">
                                        <img class="round-25 shared-img" src="/img/demo/leena.jpg" />
                                    </div>
                                    <div class="col-sm-10 padding-lr-0">
                                        <div class="form-group">
                                            <p class="font-small-2">Dude, you still need to send me the post <span class="pull-right">&times;</span></p>
                                            <p class="text-muted font-small-4">2 minutes ago</p>
                                        </div>
                                    </div>  
                                </div>
                                <div class="row task-comment">
                                    <div class="col-sm-1">
                                        <img class="round-25 shared-img" src="/img/demo/yash.jpg" />
                                    </div>
                                    <div class="col-sm-10 padding-lr-0">
                                        <div class="form-group">
                                            <p class="font-small-2">Send me the post as soon as you get my message <span class="pull-right">&times;</span></p>
                                            <p class="text-muted font-small-4">2 minutes ago</p>
                                        </div>
                                    </div>  
                                </div>        
                            </div>
                        </div>

                    </div>
                    <!--single task end--> 
                    <div class="singleTask container-fluid">
                        <!--Date--> 
                        <!--                        <div class="task-date-head row">
                                                    <p class="text-center">Today</p>
                                                </div>-->
                        <!--Task detail--> 
                        <div class="taskDetails row">
                            <div class="col-sm-1">
                                <img class="round-30" src="/img/demo/anupama.png" />
                            </div>
                            <div class="col-sm-8 padding-lr-0">
                                <div>
                                    <p class="text-bold font-small-3">Anupama Panchal, <span class="text-muted">today</span></p>
                                    <p class="task-title">Content of the following pages, contact us, signup, login, resend verification, forgot password, tour, together we can.</p>

                                </div>
                            </div>
                            <div class="col-sm-2 col-sm-offset-1">
                                <p class="pull-right">
                                    <span class="fa fa-gear"></span>
                                </p>
                            </div>
                        </div>
                        <div class="taskShare row">
                            <div class="col-sm-7 col-sm-offset-1 padding-lr-0">
                                <p class="">
                                    <img class="round-30 shared-img" src="/img/demo/anupama.png" /> <img class="round-30 shared-img" src="/img/demo/yash.jpg" /> <img class="round-30 shared-img" src="/img/demo/leena.jpg" /> <img class="round-30 shared-img" src="/img/demo/ishani.jpg" /> <span class="text-muted">+4 more</span>
                                </p>
                            </div>
                            <div class="col-sm-3 col-sm-offset-1">
                                <button class="btn-mark-complete btn btn-sm btn-green btn-block font-small-2">Mark done</button>
                            </div>
                        </div>
                        <!--task comments--> 
                        <div class="row">
                            <div class="container-fluid task-comments-box">
                                <div class="row">
                                    <div class="col-sm-1">
                                        <img class="round-25 shared-img" src="/img/demo/anupama.png" />
                                    </div>
                                    <div class="col-sm-10 padding-lr-0">
                                        <div class="form-group">
                                            <input class="form-control input-sm" placeholder="Write a comment and press enter." />
                                        </div>
                                    </div>

                                </div>
                                <div class="row task-comment">
                                    <div class="col-sm-1">
                                        <img class="round-25 shared-img" src="/img/demo/leena.jpg" />
                                    </div>
                                    <div class="col-sm-10 padding-lr-0">
                                        <div class="form-group">
                                            <p class="font-small-2">Dude, you still need to send me the post <span class="pull-right">&times;</span></p>
                                            <p class="text-muted font-small-4">2 minutes ago</p>
                                        </div>
                                    </div>  
                                </div>
                                <div class="row task-comment">
                                    <div class="col-sm-1">
                                        <img class="round-25 shared-img" src="/img/demo/yash.jpg" />
                                    </div>
                                    <div class="col-sm-10 padding-lr-0">
                                        <div class="form-group">
                                            <p class="font-small-2">Send me the post as soon as you get my message <span class="pull-right">&times;</span></p>
                                            <p class="text-muted font-small-4">2 minutes ago</p>
                                        </div>
                                    </div>  
                                </div>        
                            </div>
                        </div>

                    </div>
                    <!--SINGLE TASK--> 
                    <div class="singleTask container-fluid">
                        <!--Date--> 
                        <div class="task-date-head row">
                            <p class="text-center">Tomorrow</p>
                        </div>
                        <!--Task detail--> 
                        <div class="taskDetails row">
                            <div class="col-sm-1">
                                <img class="round-30" src="/img/demo/anupama.png" />
                            </div>
                            <div class="col-sm-8 padding-lr-0">
                                <div>
                                    <p class="text-bold font-small-3">Anupama Panchal, <span class="text-muted">today</span></p>
                                    <p class="task-title">Content of the following pages, contact us, signup, login, resend verification, forgot password, tour, together we can.</p>

                                </div>
                            </div>
                            <div class="col-sm-2 col-sm-offset-1">
                                <p class="pull-right">
                                    <span class="fa fa-gear"></span>
                                </p>
                            </div>
                        </div>
                        <div class="taskShare row">
                            <div class="col-sm-7 col-sm-offset-1 padding-lr-0">
                                <p class="">
                                    <img class="round-30 shared-img" src="/img/demo/anupama.png" /> <img class="round-30 shared-img" src="/img/demo/yash.jpg" /> <img class="round-30 shared-img" src="/img/demo/leena.jpg" /> <img class="round-30 shared-img" src="/img/demo/ishani.jpg" /> <span class="text-muted">+4 more</span>
                                </p>
                            </div>
                            <div class="col-sm-3 col-sm-offset-1">
                                <button class="btn-mark-complete btn btn-sm btn-green btn-block font-small-2">Mark done</button>
                            </div>
                        </div>
                        <!--task comments--> 
                        <div class="row">
                            <div class="container-fluid task-comments-box">
                                <div class="row">
                                    <div class="col-sm-1">
                                        <img class="round-25 shared-img" src="/img/demo/anupama.png" />
                                    </div>
                                    <div class="col-sm-10 padding-lr-0">
                                        <div class="form-group">
                                            <input class="form-control input-sm" placeholder="Write a comment and press enter." />
                                        </div>
                                    </div>

                                </div>
                                <div class="row task-comment">
                                    <div class="col-sm-1">
                                        <img class="round-25 shared-img" src="/img/demo/leena.jpg" />
                                    </div>
                                    <div class="col-sm-10 padding-lr-0">
                                        <div class="form-group">
                                            <p class="font-small-2">Dude, you still need to send me the post <span class="pull-right">&times;</span></p>
                                            <p class="text-muted font-small-4">2 minutes ago</p>
                                        </div>
                                    </div>  
                                </div>
                                <div class="row task-comment">
                                    <div class="col-sm-1">
                                        <img class="round-25 shared-img" src="/img/demo/yash.jpg" />
                                    </div>
                                    <div class="col-sm-10 padding-lr-0">
                                        <div class="form-group">
                                            <p class="font-small-2">Send me the post as soon as you get my message <span class="pull-right">&times;</span></p>
                                            <p class="text-muted font-small-4">2 minutes ago</p>
                                        </div>
                                    </div>  
                                </div>        
                            </div>
                        </div>

                    </div>
                    <!--SINGLE TASK END--> 
                    <!--SINGLE TASK--> 
                    <div class="singleTask container-fluid">
                        <!--Date--> 
                        <!--                        <div class="task-date-head row">
                                                    <p class="text-center">Today</p>
                                                </div>-->
                        <!--Task detail--> 
                        <div class="taskDetails row">
                            <div class="col-sm-1">
                                <img class="round-30" src="/img/demo/anupama.png" />
                            </div>
                            <div class="col-sm-8 padding-lr-0">
                                <div>
                                    <p class="text-bold font-small-3">Anupama Panchal, <span class="text-muted">today</span></p>
                                    <p class="task-title">Content of the following pages, contact us, signup, login, resend verification, forgot password, tour, together we can.</p>

                                </div>
                            </div>
                            <div class="col-sm-2 col-sm-offset-1">
                                <p class="pull-right">
                                    <span class="fa fa-gear"></span>
                                </p>
                            </div>
                        </div>
                        <div class="taskShare row">
                            <div class="col-sm-7 col-sm-offset-1 padding-lr-0">
                                <p class="">
                                    <img class="round-30 shared-img" src="/img/demo/anupama.png" /> <img class="round-30 shared-img" src="/img/demo/yash.jpg" /> <img class="round-30 shared-img" src="/img/demo/leena.jpg" /> <img class="round-30 shared-img" src="/img/demo/ishani.jpg" /> <span class="text-muted">+4 more</span>
                                </p>
                            </div>
                            <div class="col-sm-3 col-sm-offset-1">
                                <button class="btn-mark-complete btn btn-sm btn-green btn-block font-small-2">Mark done</button>
                            </div>
                        </div>
                        <!--task comments--> 
                        <div class="row">
                            <div class="container-fluid task-comments-box">
                                <div class="row">
                                    <div class="col-sm-1">
                                        <img class="round-25 shared-img" src="/img/demo/anupama.png" />
                                    </div>
                                    <div class="col-sm-10 padding-lr-0">
                                        <div class="form-group">
                                            <input class="form-control input-sm" placeholder="Write a comment and press enter." />
                                        </div>
                                    </div>

                                </div>
                                <div class="row task-comment">
                                    <div class="col-sm-1">
                                        <img class="round-25 shared-img" src="/img/demo/leena.jpg" />
                                    </div>
                                    <div class="col-sm-10 padding-lr-0">
                                        <div class="form-group">
                                            <p class="font-small-2">Dude, you still need to send me the post <span class="pull-right">&times;</span></p>
                                            <p class="text-muted font-small-4">2 minutes ago</p>
                                        </div>
                                    </div>  
                                </div>
                                <div class="row task-comment">
                                    <div class="col-sm-1">
                                        <img class="round-25 shared-img" src="/img/demo/yash.jpg" />
                                    </div>
                                    <div class="col-sm-10 padding-lr-0">
                                        <div class="form-group">
                                            <p class="font-small-2">Send me the post as soon as you get my message <span class="pull-right">&times;</span></p>
                                            <p class="text-muted font-small-4">2 minutes ago</p>
                                        </div>
                                    </div>  
                                </div>        
                            </div>
                        </div>

                    </div>
                    <!--single task end--> 
                    <div class="singleTask container-fluid">
                        <!--Date--> 
                        <!--                        <div class="task-date-head row">
                                                    <p class="text-center">Today</p>
                                                </div>-->
                        <!--Task detail--> 
                        <div class="taskDetails row">
                            <div class="col-sm-1">
                                <img class="round-30" src="/img/demo/anupama.png" />
                            </div>
                            <div class="col-sm-8 padding-lr-0">
                                <div>
                                    <p class="text-bold font-small-3">Anupama Panchal, <span class="text-muted">today</span></p>
                                    <p class="task-title">Content of the following pages, contact us, signup, login, resend verification, forgot password, tour, together we can.</p>

                                </div>
                            </div>
                            <div class="col-sm-2 col-sm-offset-1">
                                <p class="pull-right">
                                    <span class="fa fa-gear"></span>
                                </p>
                            </div>
                        </div>
                        <div class="taskShare row">
                            <div class="col-sm-7 col-sm-offset-1 padding-lr-0">
                                <p class="">
                                    <img class="round-30 shared-img" src="/img/demo/anupama.png" /> <img class="round-30 shared-img" src="/img/demo/yash.jpg" /> <img class="round-30 shared-img" src="/img/demo/leena.jpg" /> <img class="round-30 shared-img" src="/img/demo/ishani.jpg" /> <span class="text-muted">+4 more</span>
                                </p>
                            </div>
                            <div class="col-sm-3 col-sm-offset-1">
                                <button class="btn-mark-complete btn btn-sm btn-green btn-block font-small-2">Mark done</button>
                            </div>
                        </div>
                        <!--task comments--> 
                        <div class="row">
                            <div class="container-fluid task-comments-box">
                                <div class="row">
                                    <div class="col-sm-1">
                                        <img class="round-25 shared-img" src="/img/demo/anupama.png" />
                                    </div>
                                    <div class="col-sm-10 padding-lr-0">
                                        <div class="form-group">
                                            <input class="form-control input-sm" placeholder="Write a comment and press enter." />
                                        </div>
                                    </div>

                                </div>
                                <div class="row task-comment">
                                    <div class="col-sm-1">
                                        <img class="round-25 shared-img" src="/img/demo/leena.jpg" />
                                    </div>
                                    <div class="col-sm-10 padding-lr-0">
                                        <div class="form-group">
                                            <p class="font-small-2">Dude, you still need to send me the post <span class="pull-right">&times;</span></p>
                                            <p class="text-muted font-small-4">2 minutes ago</p>
                                        </div>
                                    </div>  
                                </div>
                                <div class="row task-comment">
                                    <div class="col-sm-1">
                                        <img class="round-25 shared-img" src="/img/demo/yash.jpg" />
                                    </div>
                                    <div class="col-sm-10 padding-lr-0">
                                        <div class="form-group">
                                            <p class="font-small-2">Send me the post as soon as you get my message <span class="pull-right">&times;</span></p>
                                            <p class="text-muted font-small-4">2 minutes ago</p>
                                        </div>
                                    </div>  
                                </div>        
                            </div>
                        </div>

                    </div>

                </div>
            </div>
        </div>
        <div class="col-sm-4">
            <?php echo $this->element('Task/sidebar_tasks_demo') ?>  
        </div>
    </div>

</div>
<script type="text/javascript" src="/js/date.js"></script>
<script type="text/javascript" src="/js/moment/moment.js"></script>
<script type="text/javascript" src="/js/datetimepicker/pikaday-edit.js"></script>
<script type="text/javascript" src="/js/datetimepicker/datetimepicker.min.js"></script>
<link href="/css/vendors/datetimepicker/datetimepicker.min.css" type="stylesheet"/>
<link href="/css/vendors/datetimepicker/pikaday.css" type="stylesheet"/>

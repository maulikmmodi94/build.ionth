<?php
$this->layout = 'layout_in';
$this->set('title_for_layout', 'Desk');
?>
<div class="container-fluid pageDesk" style=''>
    <div class="row" ng-controller="deskController">
        <!--Loader--> 
        <div id="loader-wrapper" class="col-sm-12" ng-show="loadingTasks && loadingUsers && loadingGroups">
            <div id="loader"></div>
            <div class="loading-text text-bold font-big-6 color-black-alpha-high">Loading..</div>
        </div>
        <div class="" ng-class="{'desk-expanded col-sm-10 col-sm-offset-2':hideRightBar && !showNotifications, 'col-sm-8 desk-content' : !hideRightBar || showNotifications}" ng-cloak>
            <div class="container-fluid">
                <div class="" ng-class="{'tasks-input-expanded':hideRightBar && !showNotifications, 'tasks-input-shirnk' : !hideRightBar || showNotifications}" >
                    <!--NEW TASK FORM--> 
                    <div class="tasks-input-box row">
                        <div class="padding-left-0 col-sm-10 col-sm-offset-1">
                            <div class="task-input" ng-class="{'top-on-tour':tourOn && tourStep == 1}">
                                <textarea class="animatetextarea"
                                          msd-elastic
                                          rows ="1"
                                          placeholder="Create a task here. Ex: Meet @Stacy for coffee at 9am tomorrow." 
                                          ng-model="currentTask" 
                                          ng-change="checkDateandTime(currentTask)"
                                          ng-enter="taskAddNlp(currentTask)"
                                          mentio
                                          mentio-select-not-found="true"
                                          mention-typed-term="currentMention"
                                          mentio-require-leading-space="true"
                                          mentio-id="'htmlContent'"
                                          autofocus
                                          ></textarea>
                                <mentio-menu
                                    mentio-for="'htmlContent'"
                                    mentio-trigger-char="'@'"
                                    mentio-items="UserAndGroupTag"
                                    mentio-template-url="/scripts/View/mentio.tpl.html"
                                    mentio-select="getPeopleText(item)"
                                    mentio-search="searchUserAndGroup(term, currentMention)"
                                    ></mentio-menu>
                                <!--suggestion card--> 
                                <div class="container-fluid task-suggestion-card" ng-show="currentTask.length > 0">
                                    <div class="row font-small-1" style="display:table;">
                                        <!--Task--> 
                                        <div class="suggestion-card col-sm-6">
                                            <p class="font-small-2 suggestion-head">Task</p>
                                            <p ng-bind="currentTask"></p>
                                        </div>
                                        <!--shared with--> 
                                        <div class="col-sm-3 suggestion-card" ng-init="removeShared = true">
                                            <p class="font-small-2 suggestion-head" >Shared with</p>
                                            <div ng-repeat="taskTag in tagsforTask" >
                                                <!--user/group img--> 
                                                <div ng-show="removeShared" ng-mouseenter="removeShared = false;">                                            
                                                    <g-profile-img type={{taskTag.type}} path="taskTag.vga" name="taskTag.name" myclass="g-img-25 suggestion-user font-small-2 margin-right-10"></g-profile-img>       
                                                </div>
                                                <!--remove--> 
                                                <div ng-hide="removeShared" ng-mouseleave="removeShared = true;" ng-click="removetaskTag($index)">
                                                    <p  class="removeShared g-img-25 round-30 text-center cursor-pointer margin-right-10 pull-left" popover="{{taskTag.name}}" popover-trigger="mouseenter" >
                                                        <span class="fa fa-times"></span>   
                                                    </p>   
                                                </div>

                                            </div>
                                        </div>
                                        <!--deadline--> 
                                        <div class="suggestion-card col-sm-3">
                                            <p class="font-small-2 suggestion-head" >Deadline</p>

                                            <p class="font-small-3" ng-bind="FoundEndDate.toString('MMM dd, yyyy')"></p>
                                            <p class="font-small-3" ng-bind="FoundEndDate.toString('h:mm tt')"></p>
                                        </div>
                                    </div>
                                    <span class="clearfix"></span>
                                    <div class="row suggestion-card-bottom">
                                        <div class="col-sm-8 font-small-2">
                                            <p class="" ng-show="false" >Do you want to add @someone to your workspace? <span class="">Yes</span></p>
                                        </div>
                                        <div class="col-sm-4 font-small-2 padding-lr-0">
                                            <p class="cursor-pointer text-underline text-italic text-right" ng-click="newTaskModal(currentTask)">Advance options >></p>
                                        </div>    
                                    </div>
                                </div>
                                <p  class="font-small-2 padding-top-5 task-msg-box">
                                    <span ng-show="AddedTask" class="task-msg" >
                                        <span  class="fa fa-check-circle"></span> Your task has been created. 
                                        <a href="#" ng-click="scrollTo('task' + latestTaskId)" ng-show="latestTaskId">Click to see</a>
                                    </span>
                                </p>
                            </div>    
                        </div>
                        <!--tour section--> 
                        <div class="make-task-tour tour-cover col-sm-offset-3 col-sm-8 tourAnimation" ng-show="$root.tourOn && $root.tourStep == 1">
                            <!--                        <div class="tour-horizontal-line">
                                                        <p></p>
                                                    </div>-->
                            <div class="col-sm-12 tour-desc">
                                <p>This is where you create your tasks. The starting point of your journey at being more productive!</p>
                                <p class="pull-right cursor-pointer" ng-click="$root.tourStep = 2;"><span class="fa fa-chevron-circle-right fa-2x"></span></p>
                            </div>
                        </div>
                        <!--tour end-->  

                    </div>

                    <!--TASK FILTER--> 
                    <div class=" row" ng-show="taskFilterOn">
                        <div class="bg-blue color-white filter-bar" ng-show="isSelectedUser">
                            <p>
                                <span>Showing tasks with </span>
                                <!--<g-profile-img type="user" path="{{selectedUser.vga}}" name="{{selectedUser.name}}" myclass="g-img-25 margin-right-10"></g-profile-img>--> 
                                <span class="text-bold">{{selectedUser.name}}</span> 
                                <span class="pull-right text-bold cursor-pointer" ng-click="clearFilter()">&times;</span>
                            </p>
                        </div>
                        <div class="bg-blue color-white filter-bar" ng-hide="isSelectedUser">
                            <p>Showing tasks with <span class="text-bold">{{selectedGroup.name}}</span> <span class="pull-right text-bold cursor-pointer" ng-click="clearFilter()">&times;</span></p>
                        </div>
                    </div>
                </div>


                <!--TASKS LSITING--> 
                <div class="row tasks-list">
                    <div class="col-sm-12 padding-lr-0" style="overflow:visible;">
                        <!--SEARCH RESULTS--> 
                        <div class="singleTask container-fluid" ng-show="searchActive">
                            <!--NO SEARCH RESULT--> 
                            <div ng-show="searchedTasks.length <= 0" class="text-center">
                                <hr/>
                                <span class="text-muted">No match found</span>
                                <hr/>
                            </div>
                            <!--Task detail--> 
                            <div ng-repeat="task in searchedTasks">
                                <!--Task detail--> 
                                <div class="taskDetails row">
                                    <!--creator--> 
                                    <div class="col-sm-1 padding-right-0">
                                        <g-profile-img type="user" myclass='g-img-30' path="task.user_info.vga" name="task.user_info.name" ng-if="task.user_info"></g-profile-img>
                                    </div>
                                    <div class="col-sm-8 padding-lr-0">
                                        <p class="text-bold line-height-16 color-black-alpha">
                                            <!--name--> 
                                            <span ng-bind="task.user_info.name" ng-click="showProfileofUser(task.user_info)" style="cursor:pointer" class="text-bold"></span>,
                                            <!--deadline-->
                                            <span class="text-dim font-small-2 text-italic" popover="{{dateFormat(task.Task.end_date, 'MMM d, y h:mm a')}}" popover-placement="top" popover-trigger="mouseenter"  >    
                                                <span class="" ng-bind="momentDate(task.Task.end_date)" ></span>,
                                                <span class="" ng-bind="dateFormat(task.Task.end_date, 'shortTime')"></span>
                                            </span>
                                        </p>
                                        <p class="task-title" ng-bind="task.Task.title" style=""></p>
                                    </div>
                                    <div class="col-sm-offset-1 col-sm-1 padding-lr-0 ">
                                        <p class="pull-right icon font-big-5 color-red icon-basic-bookmark" ng-show="task.Task.priority" style="margin-top: -15px" ></p>
                                    </div>
                                    <div class="col-sm-1">
                                        <!--task settings-->
                                        <li ng-show="task.Task.created_by == loggeduser" class="dropdown text-center" dropdown>
                                            <a href="#"  class="dropdown-toggle cursor-pointer icon icon2-arrows-down color-black-alpha " dropdown-toggle>

                                            </a>
                                            <ul class="dropdown-menu dropdown-animation task-settings-menu dropdown-menu-right" role="menu">
                                                <li class="container-fluid" ng-click="newTaskModal(task, 'search')">
                                                    <a href="#" class="row">
                                                        <div class="col-sm-2 padding-lr-0" style="padding-top:5px;">
                                                            <span class="icon icon-basic-gear font-big-2" style=""></span>
                                                        </div>
                                                        <div class="col-sm-10 padding-tb-5">
                                                            <p class="" >
                                                                <span>Edit task</span>
                                                            </p>    
                                                        </div>
                                                    </a> 
                                                </li>
                                                <li role="separator" class="divider"></li>
                                                <li class="container-fluid" ng-click="taskDeleteModal(task.Task.id, 'search')">
                                                    <a href="#" class="row">
                                                        <div class="col-sm-2 padding-lr-0" style="padding-top:5px;">
                                                            <span class="icon icon-basic-trashcan font-big-2" style=""></span>
                                                        </div>
                                                        <div class="col-sm-10 padding-tb-5">
                                                            <p class="" >
                                                                <span>Delete Task</span>
                                                            </p>    
                                                        </div>
                                                    </a>
                                                </li>

                                            </ul>
                                        </li>
                                    </div>
                                    <span class="clearfix"></span>
                                    <div class="col-sm-offset-2 col-sm-8 margin-top-5">
                                        <!--<hr class="margin-tb-0"/>-->
                                    </div>
                                    <div class="col-sm-2" style="position: relative;">
                                        <div ng-click="markTaskComplete(task.Task.id, task.Task.is_completed, 'search')" class="btn-mark-complete pull-right icon icon2-arrows-check" ng-hide="task.Task.is_completed" tooltip="Mark Task as Complete" tooltip-placement="left">
                                        </div>
                                        <div ng-click="markTaskComplete(task.Task.id, task.Task.is_completed, 'search')" class="btn-mark-incomplete pull-right icon icon2-arrows-anticlockwise-dashed" ng-show="task.Task.is_completed" tooltip="Mark Task as Incomplete" tooltip-placement="left">
                                        </div>
                                    </div>
                                    <span class="clearfix"></span>

                                    <div class='col-sm-12 padding-left-0 taskBottomBox'>
                                        <!--task shared with-->
                                        <div class="col-sm-8 col-sm-offset-1 padding-left-0">
                                            <div class="task-shared-with" >
                                                <p class="font-small-2 text-dim cursor-pointer shared-with text-dim" ng-repeat="tag in task.Tags" ng-hide="tag.user_id == task.Task.created_by">                                                    <span ng-bind="tag.name" popover-placement="top" popover-template="profiletemplate.templateUrl"  tabindex="0" popover-trigger="focus" popover-animation="true" class="no-border" style="line-height: 1;"></span>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--task comments--> 
                                <div class="row">
                                    <comment-form id="{{task.Task.id}}" add="addComment(id,body,'search')" delete="deleteCommentModal(id,commentId,'search')" comments="task.Comment" loadmore="{{task.moreComments}}" load="deskService.getComment(id,lastId)" commentcount=task.Task.comment_count limitcount="2"></comment-form>
                                </div>
                            </div>
                        </div>        
                    </div>
                    <!--search task ends--> 

                    <div class=" col-sm-12" ng-hide="searchActive" ng-class="{'tasks-expanded':hideRightBar && !showNotifications, 'tasks-shirnk' : !hideRightBar || showNotifications}">
                        <!--EMPTY TEMPLATES-->               
                        <div class="col-sm-12 padding-tb-20" ng-show="(TaskData.Today.length == 0 && TaskData.Tomorrow.length == 0 && TaskData.This_week.length == 0 && TaskData.Other.length == 0)">
                            <p class="text-center font-small-2">
                                <img src="/img/empty-templates/empty_2.png" height="350" ng-click=""/>
                            <p class="text-center margin-top-30 text-dim">
                                Awesome! You have no upcoming tasks. <br/>Listen to a melody or create a task above.
                            </p>
                        </div>

                        <!--TODAY HEADING--> 
                        <div class="container-fluid" ng-show="TaskData.Today.length">
                            <div class="task-date-head row">
                                <div class="col-sm-5 date-lines"></div>
                                <div class="col-sm-2 padding-lr-0">
                                    <p class="text-center text-bold font-small-1 date-heading ">Today</p>
                                </div>
                                <div class="col-sm-5 date-lines"></div>

                            </div>
                        </div>

                        <!-- today's single task --> 
                        <div id="task{{task.Task.id}}" class="singleTask container-fluid" ng-repeat="task in TaskData.Today| orderBy:'Task.end_date'">
                            <!--Task detail--> 
                            <div class="taskDetails row" >
                                <!--creator--> 
                                <div class="col-sm-1 padding-right-0">
                                    <g-profile-img type="user" myclass='g-img-30' path="task.user_info.vga" name="task.user_info.name" ng-if="task.user_info"></g-profile-img>
                                </div>
                                <div class="col-sm-8 padding-lr-0">
                                    <p class=" line-height-16 color-black-alpha">
                                        <!--name--> 
                                        <span ng-bind="task.user_info.name" ng-click="showProfileofUser(task.user_info)" style="cursor:pointer" class="text-bold"></span>, 

                                        <!--deadline--> 
                                        <span class="text-dim font-small-2 text-italic" >
                                            <span class="" ng-bind="timeagoMoment(task.Task.end_date)" popover="{{momentDate(task.Task.end_date)}},{{dateFormat(task.Task.end_date, 'shortTime')}}" popover-placement="top" popover-trigger="mouseenter"></span>
                                        </span>

                                    </p>
                                    <!--task--> 
                                    <p class="task-title" ng-bind="task.Task.title" style=""></p>
                                </div>
                                <div class="col-sm-offset-1 col-sm-1 padding-lr-0 ">
                                    <p class="pull-right icon font-big-5 color-red icon-basic-bookmark" ng-show="task.Task.priority" style="margin-top: -15px" ></p>
                                </div>
                                <div class="col-sm-1">
                                    <!--task settings-->
                                    <li ng-show="task.Task.created_by == loggeduser" class="dropdown text-center" dropdown>
                                        <a href="#"  class="dropdown-toggle cursor-pointer icon icon2-arrows-down color-black-alpha " dropdown-toggle>

                                        </a>
                                        <ul class="dropdown-menu dropdown-animation task-settings-menu dropdown-menu-right" role="menu">
                                            <li class="container-fluid" ng-click="newTaskModal(task, 'today')">
                                                <a href="#" class="row">
                                                    <div class="col-sm-2 padding-lr-0" style="padding-top:5px;">
                                                        <span class="icon icon-basic-gear font-big-2" style=""></span>
                                                    </div>
                                                    <div class="col-sm-10 padding-tb-5">
                                                        <p class="" >
                                                            <span>Edit task</span>
                                                        </p>    
                                                    </div>
                                                </a> 
                                            </li>
                                            <li role="separator" class="divider"></li>
                                            <li class="container-fluid" ng-click="taskDeleteModal(task.Task.id, 'today')">
                                                <a href="#" class="row">
                                                    <div class="col-sm-2 padding-lr-0" style="padding-top:5px;">
                                                        <span class="icon icon-basic-trashcan font-big-2" style=""></span>
                                                    </div>
                                                    <div class="col-sm-10 padding-tb-5">
                                                        <p class="" >
                                                            <span>Delete Task</span>
                                                        </p>    
                                                    </div>
                                                </a>
                                            </li>

                                        </ul>
                                    </li>
                                </div>
                                <span class="clearfix"></span>
                                <div class="col-sm-offset-2 col-sm-8 margin-top-5">
                                </div>
                                <div class="col-sm-2" style="position: relative;">
                                    <div ng-click="markTaskComplete(task.Task.id, task.Task.is_completed, 'today')" class="btn-mark-complete pull-right icon icon2-arrows-check" ng-hide="task.Task.is_completed" tooltip="Mark Task as Complete" tooltip-placement="left">
                                    </div>
                                    <div ng-click="markTaskComplete(task.Task.id, task.Task.is_completed, 'today')" class="btn-mark-incomplete pull-right icon icon2-arrows-anticlockwise-dashed" ng-show="task.Task.is_completed" tooltip="Mark Task as Incomplete" tooltip-placement="left">
                                    </div>
                                </div>
                                <span class="clearfix"></span>

                                <div class='col-sm-12 padding-left-0 taskBottomBox'>
                                    <!--task shared with-->
                                    <div class="col-sm-8 col-sm-offset-1 padding-left-0">
                                        <div class="task-shared-with " >
                                            <p class="font-small-2 text-dim cursor-pointer shared-with text-dim" ng-repeat="tag in task.Tags" ng-hide="tag.user_id == task.Task.created_by">
<!--                                                <span ng-bind="tag.name"  popover="{{tag.name}}" popover-trigger="mouseenter"></span> -->
                                                <span ng-bind="tag.name" popover-placement="top" popover-template="profiletemplate.templateUrl"  tabindex="0" popover-trigger="focus" popover-animation="true" class="no-border" style="line-height: 1;"></span>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--task comments--> 
                            <div class="row">
                                <comment-form id="{{task.Task.id}}" add="addComment(id,body,'today')" delete="deleteCommentModal(id,commentId,'today')" comments="task.Comment" loadmore="{{task.moreComments}}" load="deskService.getComment(id,lastId)" commentcount=task.Task.comment_count limitcount="2"></comment-form>
                            </div>
                        </div>
                        <!--SINGLE TASK END--> 

                        <!--TOMORROW HEADING--> 
                        <div class='container-fluid' ng-show="TaskData.Tomorrow.length > 0">
                            <!--Date-->
                            <div class="task-date-head row">

                                <div class="col-sm-5 date-lines"></div>
                                <div class="col-sm-2 padding-lr-0">
                                    <p class="text-center font-small-1 date-heading text-bold">Tomorrow</p>
                                </div>
                                <div class="col-sm-5 date-lines"></div>
                            </div>
                        </div>
                        <!--TOMORROW task start--> 
                        <div id="task{{task.Task.id}}" class="singleTask container-fluid" ng-show="TaskData.Tomorrow.length > 0" ng-repeat="task in TaskData.Tomorrow | orderBy:'Task.end_date'">
                            <!--Task detail--> 
                            <div class="taskDetails row">
                                <!--creator--> 
                                <div class="col-sm-1 padding-right-0">
                                    <g-profile-img type="user" myclass='g-img-30' path="task.user_info.vga" name="task.user_info.name" ng-if="task.user_info"></g-profile-img>
                                </div>
                                <div class="col-sm-8 padding-lr-0">
                                    <p class="line-height-16 color-black-alpha">
                                        <!--name--> 
                                        <span ng-bind="task.user_info.name" ng-click="showProfileofUser(task.user_info)" style="cursor:pointer" class="text-bold"></span>,
                                        <!--deadline--> 
                                        <span class="text-dim font-small-2 text-italic" popover="{{dateFormat(task.Task.end_date, 'MMM d, y h:mm a')}}" popover-placement="top" popover-trigger="mouseenter" >    
                                            <span class="" ng-bind="momentDate(task.Task.end_date)" ></span>,
                                            <span class="" ng-bind="dateFormat(task.Task.end_date, 'shortTime')"></span>
                                        </span>

                                    </p>
                                    <!--task title--> 
                                    <p class="task-title" ng-bind="task.Task.title"></p>
                                </div>
                                <div class="col-sm-offset-1 col-sm-1 padding-lr-0 ">
                                    <p class="pull-right icon font-big-5 color-red icon-basic-bookmark" ng-show="task.Task.priority" style="margin-top: -15px" ></p>
                                </div>
                                <div class="col-sm-1">
                                    <!--task settings-->
                                    <li ng-show="task.Task.created_by == loggeduser" class="dropdown text-center" dropdown>
                                        <a href="#"  class="dropdown-toggle cursor-pointer icon icon2-arrows-down color-black-alpha " dropdown-toggle>

                                        </a>
                                        <ul class="dropdown-menu dropdown-animation task-settings-menu dropdown-menu-right" role="menu">
                                            <li class="container-fluid" ng-click="newTaskModal(task, 'tomorrow')">
                                                <a href="#" class="row">
                                                    <div class="col-sm-2 padding-lr-0" style="padding-top:5px;">
                                                        <span class="icon icon-basic-gear font-big-2" style=""></span>
                                                    </div>
                                                    <div class="col-sm-10 padding-tb-5">
                                                        <p class="" >
                                                            <span>Edit task</span>
                                                        </p>    
                                                    </div>
                                                </a> 
                                            </li>
                                            <li role="separator" class="divider"></li>
                                            <li class="container-fluid" ng-click="taskDeleteModal(task.Task.id, 'tomorrow')">
                                                <a href="#" class="row">
                                                    <div class="col-sm-2 padding-lr-0" style="padding-top:5px;">
                                                        <span class="icon icon-basic-trashcan font-big-2" style=""></span>
                                                    </div>
                                                    <div class="col-sm-10 padding-tb-5">
                                                        <p class="" >
                                                            <span>Delete Task</span>
                                                        </p>    
                                                    </div>
                                                </a>
                                            </li>

                                        </ul>
                                    </li>
                                </div>
                                <span class="clearfix"></span>
                                <div class="col-sm-offset-2 col-sm-8 margin-top-5">
                                    <!--<hr class="margin-tb-0"/>-->
                                </div>
                                <div class="col-sm-2" style="position: relative;">
                                    <div ng-click="markTaskComplete(task.Task.id, task.Task.is_completed, 'tomorrow')" class="btn-mark-complete pull-right icon icon2-arrows-check" ng-hide="task.Task.is_completed" tooltip="Mark Task as Complete" tooltip-placement="left">
                                    </div>
                                    <div ng-click="markTaskComplete(task.Task.id, task.Task.is_completed, 'tomorrow')" class="btn-mark-incomplete pull-right icon icon2-arrows-anticlockwise-dashed" ng-show="task.Task.is_completed" tooltip="Mark Task as Incomplete" tooltip-placement="left">
                                    </div>
                                </div>
                                <span class="clearfix"></span>

                                <div class='col-sm-12 padding-left-0 taskBottomBox'>
                                    <!--task shared with-->
                                    <div class="col-sm-8 col-sm-offset-1 padding-left-0">
                                        <div class="task-shared-with line-height-40 " >
                                            <p class="font-small-2 cursor-pointer text-dim shared-with" ng-repeat="tag in task.Tags" ng-hide="tag.user_id == task.Task.created_by">
<!--                                                <span ng-bind="tag.name"  popover="{{tag.name}}" popover-trigger="mouseenter"></span> -->
                                                <span ng-bind="tag.name" popover-placement="top" popover-template="profiletemplate.templateUrl"  tabindex="0" popover-trigger="focus" popover-animation="true" class="no-border" style="line-height: 1;"></span>
                                            </p>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <!--task comments--> 
                            <div class="row">
                                <comment-form id="{{task.Task.id}}" add="addComment(id,body,'tomorrow')" delete="deleteCommentModal(id,commentId,'tomorrow')" comments="task.Comment" loadmore="{{task.moreComments}}" load="deskService.getComment(id,lastId)" commentcount=task.Task.comment_count limitcount="2"></comment-form>
                            </div>

                        </div>
                        <!--Tomorrow TASK END--> 

                        <!--THIS WEEK HEADING--> 
                        <div class='container-fluid' ng-show="TaskData.This_week.length > 0">
                            <div class="task-date-head row">
                                <div class="col-sm-5 date-lines"></div>
                                <div class="col-sm-2 padding-lr-0">
                                    <p class="text-center text-bold date-heading font-small-1">This week</p>
                                </div>
                                <div class="col-sm-5 date-lines"></div>

                            </div>
                        </div>
                        <!--this week start--> 
                        <div id="task{{task.Task.id}}"  class="singleTask container-fluid" ng-show="TaskData.This_week.length > 0" ng-repeat="task in TaskData.This_week | orderBy:'Task.end_date'"> 
                            <!--Task detail--> 
                            <div class="taskDetails row">
                                <!--creator--> 
                                <div class="col-sm-1 padding-right-0">
                                    <g-profile-img type="user" myclass='g-img-30' path="task.user_info.vga" name="task.user_info.name" ng-if="task.user_info"></g-profile-img>
                                </div>
                                <div class="col-sm-8 padding-lr-0">
                                    <p class="line-height-16 color-black-alpha">
                                        <!--name--> 
                                        <span ng-bind="task.user_info.name" ng-click="showProfileofUser(task.user_info)" style="cursor:pointer" class="text-bold" ></span>, 
                                        <!--deadline--> 
                                        <span class="text-dim font-small-2 text-italic" popover="{{dateFormat(task.Task.end_date, 'MMM d, y h:mm a')}}" popover-placement="top" popover-trigger="mouseenter"  >    
                                            <span class="" ng-bind="momentDate(task.Task.end_date)" ></span>,
                                            <span class="" ng-bind="dateFormat(task.Task.end_date, 'shortTime')"></span>
                                        </span>

                                    </p>
                                    <!--task title--> 
                                    <p class="task-title" ng-bind="task.Task.title"></p>
                                </div>
                                <div class="col-sm-offset-1 col-sm-1 padding-lr-0 ">
                                    <p class="pull-right icon font-big-5 color-red icon-basic-bookmark" ng-show="task.Task.priority" style="margin-top: -15px" ></p>
                                </div>
                                <div class="col-sm-1">
                                    <!--task settings-->
                                    <li ng-show="task.Task.created_by == loggeduser" class="dropdown text-center" dropdown>
                                        <a href="#"  class="dropdown-toggle cursor-pointer icon icon2-arrows-down color-black-alpha " dropdown-toggle>

                                        </a>
                                        <ul class="dropdown-menu dropdown-animation task-settings-menu dropdown-menu-right" role="menu">
                                            <li class="container-fluid" ng-click="newTaskModal(task, 'this_week')">
                                                <a href="#" class="row">
                                                    <div class="col-sm-2 padding-lr-0" style="padding-top:5px;">
                                                        <span class="icon icon-basic-gear font-big-2" style=""></span>
                                                    </div>
                                                    <div class="col-sm-10 padding-tb-5">
                                                        <p class="" >
                                                            <span>Edit task</span>
                                                        </p>    
                                                    </div>
                                                </a> 
                                            </li>
                                            <li role="separator" class="divider"></li>
                                            <li class="container-fluid" ng-click="taskDeleteModal(task.Task.id, 'this_week')">
                                                <a href="#" class="row">
                                                    <div class="col-sm-2 padding-lr-0" style="padding-top:5px;">
                                                        <span class="icon icon-basic-trashcan font-big-2" style=""></span>
                                                    </div>
                                                    <div class="col-sm-10 padding-tb-5">
                                                        <p class="" >
                                                            <span>Delete Task</span>
                                                        </p>    
                                                    </div>
                                                </a>
                                            </li>

                                        </ul>
                                    </li>
                                </div>
                                <span class="clearfix"></span>
                                <div class="col-sm-offset-2 col-sm-8 margin-top-5">
                                    <!--<hr class="margin-tb-0"/>-->
                                </div>
                                <div class="col-sm-2" style="position: relative;">
                                    <div ng-click="markTaskComplete(task.Task.id, task.Task.is_completed, 'this_week')" class="btn-mark-complete pull-right icon icon2-arrows-check" ng-hide="task.Task.is_completed" tooltip="Mark Task as Complete" tooltip-placement="left">
                                    </div>
                                    <div ng-click="markTaskComplete(task.Task.id, task.Task.is_completed, 'this_week')" class="btn-mark-incomplete pull-right icon icon2-arrows-anticlockwise-dashed" ng-show="task.Task.is_completed" tooltip="Mark Task as Incomplete" tooltip-placement="left">
                                    </div>
                                </div>
                                <span class="clearfix"></span>

                                <div class='col-sm-12 padding-left-0 taskBottomBox'>
                                    <!--task shared with-->
                                    <div class="col-sm-8 col-sm-offset-1 padding-left-0">
                                        <div class="task-shared-with line-height-40 " >
                                            <p class="font-small-2 cursor-pointer shared-with text-dim" ng-repeat="tag in task.Tags" ng-hide="tag.user_id == task.Task.created_by">
<!--                                                <span ng-bind="tag.name"  popover="{{tag.name}}" popover-trigger="mouseenter" content="<html><body>Hello</body></html>"></span> -->
                                                <span ng-bind="tag.name" popover-placement="top" popover-template="profiletemplate.templateUrl"  tabindex="0" popover-trigger="focus" popover-animation="true" class="no-border" style="line-height: 1;"></span>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--task comments--> 
                            <div class="row">
                                <comment-form id="{{task.Task.id}}" add="addComment(id,body,'this_week')" delete="deleteCommentModal(id,commentId,'this_week')" comments="task.Comment" loadmore="{{task.moreComments}}" load="deskService.getComment(id,lastId)" commentcount=task.Task.comment_count limitcount="2"></comment-form>
                            </div>

                        </div>

                        <!--this week TASK END-->

                        <!--UPCOMING TASKS HEADING--> 
                        <div class='container-fluid' ng-show="TaskData.Other.length > 0">
                            <div class="task-date-head row">
                                <div class="col-sm-5 date-lines"></div>
                                <div class="col-sm-2 padding-lr-0">
                                    <p class="text-center text-bold font-small-1 date-heading">Upcoming</p>
                                </div>
                                <div class="col-sm-5 date-lines"></div>

                            </div>
                        </div>
                        <!--UPCOMING TASK start--> 
                        <div id="task{{task.Task.id}}" class="singleTask container-fluid" ng-repeat="task in TaskData.Other | orderBy:'Task.end_date'">
                            <!--Task detail--> 
                            <div class="taskDetails row">
                                <!--creator--> 
                                <div class="col-sm-1 padding-right-0">
                                    <g-profile-img type="user" myclass='g-img-30' path="task.user_info.vga" name="task.user_info.name" ng-if="task.user_info"></g-profile-img>
                                </div>
                                <div class="col-sm-8 padding-lr-0">
                                    <p class="line-height-16 color-black-alpha">
                                        <!--name--> 
                                        <span class="text-bold" ng-bind="task.user_info.name" ng-click="showProfileofUser(task.user_info)" style="cursor:pointer"></span>,
                                        <!--deadline--> 
                                        <span class="text-dim font-small-2 text-italic" popover="{{dateFormat(task.Task.end_date, 'MMM d, y h:mm a')}}" popover-placement="top" popover-trigger="mouseenter"  >    
                                            <span class="" ng-bind="momentDate(task.Task.end_date)" ></span>,
                                            <span class="" ng-bind="dateFormat(task.Task.end_date, 'shortTime')"></span>
                                        </span>
                                    </p>
                                    <!--task title--> 
                                    <p class="task-title" ng-bind="task.Task.title"></p>

                                </div>
                                <div class="col-sm-offset-1 col-sm-1 padding-lr-0 ">
                                    <p class="pull-right icon font-big-5 color-red icon-basic-bookmark" ng-show="task.Task.priority" style="margin-top: -15px" ></p>
                                </div>
                                <div class="col-sm-1">
                                    <!--task settings-->
                                    <li ng-show="task.Task.created_by == loggeduser" class="dropdown text-center" dropdown>
                                        <a href="#"  class="dropdown-toggle cursor-pointer icon icon2-arrows-down color-black-alpha " dropdown-toggle>

                                        </a>
                                        <ul class="dropdown-menu dropdown-animation task-settings-menu dropdown-menu-right" role="menu">
                                            <li class="container-fluid" ng-click="newTaskModal(task, 'other')">
                                                <a href="#" class="row">
                                                    <div class="col-sm-2 padding-lr-0" style="padding-top:5px;">
                                                        <span class="icon icon-basic-gear font-big-2" style=""></span>
                                                    </div>
                                                    <div class="col-sm-10 padding-tb-5">
                                                        <p class="" >
                                                            <span>Edit task</span>
                                                        </p>    
                                                    </div>
                                                </a> 
                                            </li>
                                            <li role="separator" class="divider"></li>
                                            <li class="container-fluid" ng-click="taskDeleteModal(task.Task.id, 'other')">
                                                <a href="#" class="row">
                                                    <div class="col-sm-2 padding-lr-0" style="padding-top:5px;">
                                                        <span class="icon icon-basic-trashcan font-big-2" style=""></span>
                                                    </div>
                                                    <div class="col-sm-10 padding-tb-5">
                                                        <p class="" >
                                                            <span>Delete Task</span>
                                                        </p>    
                                                    </div>
                                                </a>
                                            </li>

                                        </ul>
                                    </li>
                                </div>
                                <span class="clearfix"></span>
                                <div class="col-sm-offset-2 col-sm-8 margin-top-5">
                                    <!--<hr class="margin-tb-0"/>-->
                                </div>
                                <div class="col-sm-2" style="position: relative;">
                                    <div ng-click="markTaskComplete(task.Task.id, task.Task.is_completed, 'other')" class="btn-mark-complete pull-right icon icon2-arrows-check" ng-hide="task.Task.is_completed" tooltip="Mark Task as Complete" tooltip-placement="left">
                                    </div>
                                    <div ng-click="markTaskComplete(task.Task.id, task.Task.is_completed, 'other')" class="btn-mark-incomplete pull-right icon icon2-arrows-anticlockwise-dashed" ng-show="task.Task.is_completed" tooltip="Mark Task as Incomplete" tooltip-placement="left">
                                    </div>
                                </div>
                                <span class="clearfix"></span>

                                <div class='col-sm-12 padding-left-0 taskBottomBox'>
                                    <!--task shared with-->
                                    <div class="col-sm-8 col-sm-offset-1 padding-left-0">
                                        <div class="task-shared-with line-height-40 " >
                                            <p class="font-small-2  cursor-pointer shared-with text-dim" ng-repeat="tag in task.Tags" ng-hide="tag.user_id == task.Task.created_by">
<!--                                                <span ng-bind="tag.name"  popover="{{tag.name}}" popover-trigger="mouseenter"></span> -->
                                                <span ng-bind="tag.name" popover-placement="top" popover-template="profiletemplate.templateUrl"  tabindex="0" popover-trigger="focus" popover-animation="true" class="no-border" style="line-height: 1;"></span>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--task comments--> 
                            <div class="row">
                                <comment-form id="{{task.Task.id}}" add="addComment(id,body,'other')" delete="deleteCommentModal(id,commentId,'other')" comments="task.Comment" loadmore="{{task.moreComments}}" load="deskService.getComment(id,lastId)" commentcount=task.Task.comment_count limitcount="2"></comment-form>
                            </div>
                        </div>
                        <!--other upcoming TASK End-->
                        <!--                        <div class="container-fluid">
                                                    <div class="row">
                                                        <p class='text-center color-black-alpha-low padding-tb-20'>You have no more tasks.</p>
                                                        <hr/>
                                                    </div>
                                                </div>-->
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-4">
            <ng-include src="'/scripts/View/Desk/sidebar_tasks.html'"></ng-include>
        </div>

        <!--TOUR AREA-->
        <span class="ng-cloak">
            <!--TOUR OVERLAY-->
            <div class="row tour-overlay" ng-show="tourOn"></div>
            <!--ADD TEAM AND PEOPLE INVITE-->

            <div class="col-sm-2 hidden-xs tourAnimation add-invite-tour-cover " ng-show="tourStep == 3 && tourOn" >
                <div class="timeline add-invite-tour">
                    <div class="direction-r">
                        <div class="flag-wrapper">
                            <div class="flag">
                                <p>Create teams and add people. Usual stuff, just quicker. Build is meant for teams with you at the center.</p>
                                <p class="pull-right cursor-pointer" ng-click="$root.tourOn = false"><span class="fa fa-check-circle fa-2x"></span></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!--PENDING / COMPLETED TOUR--> 
            <div class="hidden-xs sidebar-pending-cover tourAnimation" ng-show="tourStep == 2 && tourOn == true" >
                <div class="timelineL sidebar-task-tour">
                    <div class="direction-l">
                        <div class="flag-wrapper ">
                            <div class="flag">
                                <p>Map and manage priorities through pending and completed tasks. Stay on top of everything.</p>
                                <p class="pull-right cursor-pointer" ng-click="$root.tourStep = 3"><span class="fa fa-chevron-circle-right fa-2x"></span></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--             DONE TOUR  
                        <div class="tour-done" ng-show="tourOn">
                            <button class="btn btn-green" ng-click="$root.tourOn = false">Let's get started!</button>
                        </div>-->
            <!--CLOSE TOUR-->
            <div class="close-tour" ng-show="tourOn" ng-click='$root.tourOn = false'>
                <span class="fa fa-times color-black-alpha"></span>
            </div>
            <!--tour ends-->
            <!--TOUR AUTO START IN FIRST SESSION--> 
            <?php
            if (isset($AuthUser['FirstSession'])) {
                ?>
                <span ng-init="$root.tourOn = true;"></span>
                <?php
            }
            ?>
        </span>
    </div>
</div>
<style>
    .animatetextarea:focus {
        -webkit-transition: height 50ms ease-in-out;
        -moz-transition: height 50ms ease-in-out;
        -o-transition: height 50ms ease-in-out;
        transition: height 50ms ease-in-out;
    }
    body{
        overflow: hidden;
    }
</style>
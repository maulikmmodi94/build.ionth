<?php
$this->layout = 'layout_in';
$this->set('title_for_layout', 'Desk');
?>
<style>
    #loader-wrapper {
        position: fixed;
        top: 50px;
        left: 16.67%;
        width: 83.33%;
        height: 100%;
        z-index: 1000;
        background-color: white;
    }
    .loading-text{
        position: relative;
        left: 46%;
        top: 33%;
    }
    #loader {
        display: block;
        position: relative;
        left: 44%;
        top: 40%;
        width: 75px;
        height: 75px;
        margin: -75px 0 0 -75px;
        border-radius: 50%;
        border: 3px solid transparent;
        border-top-color: #3498db;
        -webkit-animation: spin 2s linear infinite; /* Chrome, Opera 15+, Safari 5+ */
        animation: spin 2s linear infinite; /* Chrome, Firefox 16+, IE 10+, Opera */
    }

    #loader:before {
        content: "";
        position: absolute;
        top: 5px;
        left: 5px;
        right: 5px;
        bottom: 5px;
        border-radius: 50%;
        border: 3px solid transparent;
        border-top-color: #e74c3c;
        -webkit-animation: spin 3s linear infinite; /* Chrome, Opera 15+, Safari 5+ */
        animation: spin 3s linear infinite; /* Chrome, Firefox 16+, IE 10+, Opera */
    }

    #loader:after {
        content: "";
        position: absolute;
        top: 15px;
        left: 15px;
        right: 15px;
        bottom: 15px;
        border-radius: 50%;
        border: 3px solid transparent;
        border-top-color: #f9c922;
        -webkit-animation: spin 1.5s linear infinite; /* Chrome, Opera 15+, Safari 5+ */
        animation: spin 1.5s linear infinite; /* Chrome, Firefox 16+, IE 10+, Opera */
    }

    @-webkit-keyframes spin {
        0%   {
            -webkit-transform: rotate(0deg);  /* Chrome, Opera 15+, Safari 3.1+ */
            -ms-transform: rotate(0deg);  /* IE 9 */
            transform: rotate(0deg);  /* Firefox 16+, IE 10+, Opera */
        }
        100% {
            -webkit-transform: rotate(360deg);  /* Chrome, Opera 15+, Safari 3.1+ */
            -ms-transform: rotate(360deg);  /* IE 9 */
            transform: rotate(360deg);  /* Firefox 16+, IE 10+, Opera */
        }
    }
    @keyframes spin {
        0%   {
            -webkit-transform: rotate(0deg);  /* Chrome, Opera 15+, Safari 3.1+ */
            -ms-transform: rotate(0deg);  /* IE 9 */
            transform: rotate(0deg);  /* Firefox 16+, IE 10+, Opera */
        }
        100% {
            -webkit-transform: rotate(360deg);  /* Chrome, Opera 15+, Safari 3.1+ */
            -ms-transform: rotate(360deg);  /* IE 9 */
            transform: rotate(360deg);  /* Firefox 16+, IE 10+, Opera */
        }
    }
</style>

<div class="container-fluid">
    <div class="row" ng-controller="deskController">
        <!--Loader--> 
        <div id="loader-wrapper" class="col-sm-12" ng-hide="true">
            <div id="loader"></div>
            <div class="loading-text text-bold font-big-6 color-black-alpha-high">Loading..</div>
        </div>

        <div class="" ng-class="{'col-sm-8 col-sm-offset-2':hideRightBar , 'col-sm-8' : !hideRightBar}">
            <div class="container-fluid">
                <!--NEW TASK FORM--> 

                <div class="tasks-input-box row">
                    <div class="padding-left-0" ng-class="{
                                'col-sm-10 col-sm-offset-1':searchTask.length == 0,  'col-sm-6':searchTask.length > 0}">
                        <div class="task-input">
                            <textarea class=""
                                      rows ="1"
                                      placeholder="Create a task. Ex: Meet @Stacy for coffee at 9am tomorrow." 
                                      ng-model="currentTask" 
                                      ng-change="checkPatterns(currentTask)"
                                      ng-enter="taskAddNlp(currentTask)"
                                      mentio
                                      mentio-select-not-found="true"
                                      mention-typed-term="currentMention"
                                      mentio-require-leading-space="true"
                                      mentio-id="'htmlContent'"
                                      ></textarea>
                            <p class="btnMakeTask text-center" ng-click="taskAddNlp(currentTask)" >+</p>

                            <mentio-menu
                                mentio-for="'htmlContent'"
                                mentio-trigger-char="'@'"
                                mentio-items="UserAndGroupTag"
                                mentio-template-url="/scripts/View/mentio.tpl.html"
                                mentio-select="getPeopleText(item)"
                                mentio-search="searchUserAndGroup(term, currentMention)"
                                ></mentio-menu>
                            <!--suggestion card--> 
                            <div class="container-fluid task-suggestion-card" ng-show="currentTask.length > 0">
                                <div class="row font-small-1">
                                    <!--Task--> 
                                    <div class="suggestion-card col-sm-6">
                                        <p class="font-small-2 suggestion-head">Task</p>
                                        <p ng-bind="currentTask"></p>
                                    </div>
                                    <!--shared with--> 
                                    <div class="col-sm-3 suggestion-card" ng-init="removeShared = ''">
                                        <p class="font-small-2 suggestion-head" >Shared with</p>
                                        <div ng-repeat="taskTag in tagsforTask" >
                                            <div ng-mouseenter="removeShared.id = taskTag.id" ng-mouseleave="removeShared = ''">
                                                <div ng-hide="removeShared.id === taskTag.id">
                                                    <g-profile-img type="user" path="{{taskTag.vga}}" name="{{taskTag.name}}" myclass="g-img-25 suggestion-user font-small-2 margin-right-10"></g-profile-img>       
                                                </div>
                                                <div ng-show="removeShared.id === taskTag.id"  >
                                                    <p  class="removeShared g-img-25 round-30 text-center cursor-pointer margin-right-10 pull-left" popover="{{taskTag.name}}" popover-trigger="mouseenter" >
                                                        <span class="fa fa-times"></span>
                                                    </p>   
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <!--deadline--> 
                                    <div class="suggestion-card col-sm-3">
                                        <p class="font-small-2 suggestion-head" >Deadline</p>

                                        <p class="font-small-3" ng-bind="FoundEndDate.toString('MMM dd, yyyy h:mm tt')"></p>
                                    </div>
                                </div>
                            </div>
                        </div>    
                    </div>
                    <!--                    <div class="padding-lr-0" ng-class="{'col-sm-2':searchTask.length == 0, 'col-sm-6':searchTask.length > 0}">
                                            SEARCH BAR 
                                            <div class="form-group search-task-box">
                                                <input class="form-control input-sm font-small-2" placeholder="Search task" ng-model="searchTask" ng-enter="taskSearch(searchTask)"/>
                                                <span ng-hide="searchTask.length > 0" class="fa fa-search"></span>
                                                <span ng-show="searchTask.length > 0" class=" fa fa-times" ng-click="clearSearch()" style="cursor: pointer"></span>
                                            </div>        
                                        </div>-->
                </div>

                <!--TASK FILTER--> 
                <div class=" row" ng-show="taskFilterOn">
                    <div class="bg-blue color-white filter-bar" ng-show="isSelectedUser">
                        <p>
                            <span>Showing tasks with </span>
                        <g-profile-img type="user" path="{{selectedUser.vga}}" name="{{selectedUser.name}}" myclass="g-img-25 margin-right-10"></g-profile-img> 
                        <span class="text-bold">{{selectedUser.name}}</span> 
                        <span class="pull-right text-bold cursor-pointer" ng-click="clearFilter()">&times;</span>
                        </p>
                    </div>
                    <div class="bg-blue color-white filter-bar" ng-hide="isSelectedUser">
                        <p><span class="text-bold">{{selectedGroup.name}}</span> <span class="pull-right text-bold cursor-pointer" ng-click="clearFilter()">&times;</span></p>
                    </div>
                </div>
                <!--SEARCH RESULT--> 
                <div class="singleTask container-fluid" ng-show="searchActive">
                    <!--                        <div class="task-date-head row">
                                            <p class="text-center">Today</p>
                                        </div>-->
                    <!--NO SEARCH RESULT--> 
                    <div ng-show="searchedTask.length <= 0">
                        <span class="text-muted">There are not such tasks</span>
                    </div>
                    <!--Task detail--> 
                    <div ng-repeat="task in searchedTask">
                        <div class="taskDetails row">
                            <div class="col-sm-1">
                                <g-profile-img type="user" myclass='g-desk-user-img' path="{{task.user_info.vga}}" name="{{task.user_info.name}}"></g-profile-img>
                            </div>
                            <div class="col-sm-8 padding-lr-0">
                                <div>
                                    <p class="text-bold"><span ng-bind="task.user_info.name" ng-click="showProfileofUser(task.user_info)" style="cursor:pointer"></span>, <span class="text-muted font-small-3" ng-bind="momentDate(task.Task.end_date)"></span></p>
                                    <p class="task-title" ng-bind="task.Task.title"></p>

                                </div>
                            </div>
                            <div class="col-sm-2 col-sm-offset-1">                              <li class="dropdown" dropdown>
                                    <p class="pull-right dropdown-toggle cursor-pointer" dropdown-toggle>
                                        <span class="fa fa-gear"></span>
                                    </p>
                                    <ul class="dropdown-menu task-settings-menu dropdown-menu-right text-center" role="menu">
                                        <li class="" ng-click="newTaskModal(task, 'today')"><span class="fa fa-gears"></span> Edit Task</li>
                                        <li class="" ng-click="taskDeleteModal(task.Task.id, 'today')"><span class="fa fa-trash-o"></span> Delete Task</li>

                                    </ul>
                                </li>
                                <span class="fa fa-bookmark color-red pull-right" ng-show="task.Task.priority == 1"></span>
                            </div>
                        </div>
                        <div class="taskShare row">
                            <div class="col-sm-7 col-sm-offset-1 padding-lr-0">
                                <p class="">
                                    <span ng-repeat="tag in task.Tags">
                                        <img class="round-30 shared-img" ng-src="{{tag.vga}}" popover="{{tag.name}}" popover-trigger="mouseenter" /> 
                                    </span>
                                    <span class="text-muted">+4 more</span>
                                </p>
                            </div>
                            <div class="col-sm-3 col-sm-offset-1">
                                <button class="btn-mark-complete btn btn-sm btn-green btn-block font-small-2" ng-click="markTaskComplete(task.Task.id, task.Task.is_completed, 'today')">Mark Complete</button>
                            </div>
                        </div>
                        <!--task comments--> 
                        <div class="row">
                            <comment-form id="{{task.Task.id}}" add="addComment(id,body,'today')" delete="deleteCommentModal(id,commentId,'today')" comments="task.Comment" loadmore="{{task.moreComments}}" load="deskService.getComment(id,lastId)" commentcount=task.Task.comment_count></comment-form>
                        </div>
                    </div>
                </div>

                <!--TASKS LSITING--> 
                <div class="tasks-list row" ng-hide="searchActive">

                    <div class="col-sm-12 padding-tb-20" ng-show="TaskData.Today.length <= 0 && TaskData.Tomorrow.length <= 0 && TaskData.This_week.length <= 0 && TaskData.other.length <= 0">
                        <p class="text-center font-small-2">
                            <img src="/img/empty-templates/empty_2.png" height="350" ng-click=""/>
                        <p class="text-center margin-top-30 text-dim">
                            Awesome! You have no upcoming tasks. <br/>Listen to a melody or create a task above.
                        </p>
                    </div>

                    <!--TODAY HEADING--> 
                    <div class="container-fluid" ng-show="TaskData.Today.length">
                        <div class="task-date-head row">
                            <div class="col-sm-5 date-lines"></div>
                            <div class="col-sm-2 padding-lr-0">
                                <p class="text-center font-small-1 date-heading">Today</p>
                            </div>
                            <div class="col-sm-5 date-lines"></div>

                        </div>
                    </div>

                    <!-- today's single task --> 
                    <div class="singleTask container-fluid" ng-repeat="task in TaskData.Today">
                        <!--Task detail--> 
                        <div class="taskDetails row">
                            <!--creator--> 
                            <div class="col-sm-1 padding-right-0">
                                <g-profile-img type="user" myclass='g-img-30' path="{{task.user_info.vga}}" name="{{task.user_info.name}}" ng-if="task.user_info"></g-profile-img>
                            </div>
                            <div class="col-sm-8 padding-lr-0">
                                <p class="text-bold line-height-16 color-black-alpha-high">
                                    <!--name--> 
                                    <span ng-bind="task.user_info.name" ng-click="showProfileofUser(task.user_info)" style="cursor:pointer"></span>,

                                </p>
                                <!--deadline-->
                                <p class="line-height-16 text-dim font-small-2 text-italic">
                                    <span class="" ng-bind="momentDate(task.Task.end_date)" ></span>,
                                    <span class="" ng-bind="dateFormat(task.Task.end_date, 'shortTime')"></span>
                                </p>
                            </div>
                            <div class="col-sm-offset-1 col-sm-1 padding-lr-0 ">
                                <p class="pull-right icon font-big-5 color-red icon-basic-bookmark" ng-show="task.Task.priority" style="margin-top: -15px" ></p>
                            </div>
                            <div class="col-sm-1">
                                <!--task settings-->
                                <li ng-show="task.Task.created_by == loggeduser" class="dropdown" dropdown>
                                    <a href="#"  class="dropdown-toggle cursor-pointer icon icon2-arrows-down color-black-alpha " dropdown-toggle>

                                    </a>
                                    <ul class="dropdown-menu task-settings-menu dropdown-menu-right" role="menu">
                                        <li class="container-fluid" ng-click="newTaskModal(task, 'today')">
                                            <a href="#" class="row">
                                                <div class="col-sm-2 padding-lr-0" style="padding-top:5px;">
                                                    <span class="icon icon-basic-gear font-big-2" style=""></span>
                                                </div>
                                                <div class="col-sm-10 padding-tb-5">
                                                    <p class="" >
                                                        <span>Edit task</span>
                                                    </p>    
                                                </div>
                                            </a> 
                                        </li>
                                        <li role="separator" class="divider"></li>
                                        <li class="container-fluid" ng-click="taskDeleteModal(task.Task.id, 'today')">
                                            <a href="#" class="row">
                                                <div class="col-sm-2 padding-lr-0" style="padding-top:5px;">
                                                    <span class="icon icon-basic-trashcan font-big-2" style=""></span>
                                                </div>
                                                <div class="col-sm-10 padding-tb-5">
                                                    <p class="" >
                                                        <span>Delete Task</span>
                                                    </p>    
                                                </div>
                                            </a>
                                        </li>

                                    </ul>
                                </li>
                            </div>
                            <span class="clearfix"></span>
                            <div class="col-sm-10 col-sm-offset-2 padding-lr-0">
                                <!--task title--> 
                                <div class="col-sm-12 padding-lr-0 padding-top-10">
                                    <p class="task-title" ng-bind="task.Task.title"></p>
                                </div>

                            </div>
                            <span class="clearfix"></span>
                            <div class="col-sm-offset-2 col-sm-8 margin-top-5">
                                <!--<hr class="margin-tb-0"/>-->
                            </div>
                            <div class="col-sm-2" style="position: relative;">
                                <div ng-click="markTaskComplete(task.Task.id, task.Task.is_completed, 'today')" class="btn-mark-complete pull-right icon icon2-arrows-check" >

                                </div>
                            </div>
                            <span class="clearfix"></span>

                            <div class='col-sm-12 padding-left-0 taskBottomBox'>
                                <!--task shared with-->
                                <div class="col-sm-8 col-sm-offset-1 padding-left-0">
                                    <div class=" pull-left margin-right-10" style="padding-top: 12px; min-height: 40px" >
                                        <p class="icon icon-basic-share" ng-show="task.Tags.length > 1">

                                        </p>
                                    </div>
                                    <div class="pull-left" ng-repeat="tag in task.Tags">
                                        <p class="font-small-2 btn-link cursor-pointer margin-right-5 line-height-40 "  popover="{{tag.name}}" ng-hide="tag.user_id == task.Task.created_by" >
                                            <span ng-bind="tag.name"></span>, 
                                        </p>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <!--task comments--> 
                        <div class="row">
                            <comment-form id="{{task.Task.id}}" add="addComment(id,body,'today')" delete="deleteCommentModal(id,commentId,'today')" comments="task.Comment" loadmore="{{task.moreComments}}" load="deskService.getComment(id,lastId)" commentcount=task.Task.comment_count limitcount="2"></comment-form>
                        </div>
                    </div>
                    <!--SINGLE TASK END--> 

                    <!--TOMORROW HEADING--> 
                    <div class='container-fluid' ng-show="TaskData.Tomorrow.length > 0">
                        <!--Date-->
                        <div class="task-date-head row">

                            <div class="col-sm-5 date-lines"></div>
                            <div class="col-sm-2 padding-lr-0 task-date-head">
                                <p class="text-center">Tomorrow</p>
                            </div>
                            <div class="col-sm-5 date-lines"></div>
                        </div>
                    </div>
                    <!--TOMORROW task start--> 
                    <div class="singleTask container-fluid" ng-show="TaskData.Tomorrow.length > 0" ng-repeat="task in TaskData.Tomorrow">
                        <!--Task detail--> 
                        <!--Task detail--> 
                        <div class="taskDetails row">
                            <!--creator--> 
                            <div class="col-sm-1 padding-right-0">
                                <g-profile-img type="user" myclass='g-img-30' path="{{task.user_info.vga}}" name="{{task.user_info.name}}" ng-if="task.user_info"></g-profile-img>
                            </div>
                            <div class="col-sm-8 padding-lr-0">
                                <p class="text-bold line-height-16 color-black-alpha-high">
                                    <!--name--> 
                                    <span ng-bind="task.user_info.name" ng-click="showProfileofUser(task.user_info)" style="cursor:pointer"></span>,

                                </p>
                                <!--deadline-->
                                <p class="line-height-16 text-dim font-small-2 text-italic">
                                    <span class="" ng-bind="momentDate(task.Task.end_date)" ></span>,
                                    <span class="" ng-bind="dateFormat(task.Task.end_date, 'shortTime')"></span>
                                </p>
                            </div>
                            <div class="col-sm-offset-1 col-sm-1 padding-lr-0 ">
                                <p class="pull-right icon font-big-5 color-red icon-basic-bookmark" ng-show="task.Task.priority" style="margin-top: -15px" ></p>
                            </div>
                            <div class="col-sm-1">
                                <!--task settings-->
                                <li ng-show="task.Task.created_by == loggeduser" class="dropdown" dropdown>
                                    <a href="#"  class="dropdown-toggle cursor-pointer icon icon2-arrows-down color-black-alpha " dropdown-toggle>

                                    </a>
                                    <ul class="dropdown-menu task-settings-menu dropdown-menu-right" role="menu">
                                        <li class="container-fluid" ng-click="newTaskModal(task, 'today')">
                                            <a href="#" class="row">
                                                <div class="col-sm-2 padding-lr-0" style="padding-top:5px;">
                                                    <span class="icon icon-basic-gear font-big-2" style=""></span>
                                                </div>
                                                <div class="col-sm-10 padding-tb-5">
                                                    <p class="" >
                                                        <span>Edit task</span>
                                                    </p>    
                                                </div>
                                            </a> 
                                        </li>
                                        <li role="separator" class="divider"></li>
                                        <li class="container-fluid" ng-click="taskDeleteModal(task.Task.id, 'today')">
                                            <a href="#" class="row">
                                                <div class="col-sm-2 padding-lr-0" style="padding-top:5px;">
                                                    <span class="icon icon-basic-trashcan font-big-2" style=""></span>
                                                </div>
                                                <div class="col-sm-10 padding-tb-5">
                                                    <p class="" >
                                                        <span>Delete Task</span>
                                                    </p>    
                                                </div>
                                            </a>
                                        </li>

                                    </ul>
                                </li>
                            </div>
                            <span class="clearfix"></span>
                            <div class="col-sm-10 col-sm-offset-2 padding-lr-0">
                                <!--task title--> 
                                <div class="col-sm-12 padding-lr-0 padding-top-10">
                                    <p class="task-title" ng-bind="task.Task.title"></p>
                                </div>

                            </div>
                            <span class="clearfix"></span>
                            <div class="col-sm-offset-2 col-sm-8 margin-top-5">
                                <!--<hr class="margin-tb-0"/>-->
                            </div>
                            <div class="col-sm-2" style="position: relative;">
                                <div ng-click="markTaskComplete(task.Task.id, task.Task.is_completed, 'tomorrow')" class="btn-mark-complete pull-right icon icon2-arrows-check" >

                                </div>
                            </div>
                            <span class="clearfix"></span>

                            <div class='col-sm-12 padding-left-0 taskBottomBox'>
                                <!--task shared with-->
                                <div class="col-sm-8 col-sm-offset-1 padding-left-0">
                                    <div class=" pull-left margin-right-10" style="padding-top: 12px; min-height: 40px" >
                                        <p class="icon icon-basic-share" ng-show="task.Tags.length > 1">

                                        </p>
                                    </div>
                                    <div class="pull-left" ng-repeat="tag in task.Tags">
                                        <p class="font-small-2 btn-link cursor-pointer margin-right-5 line-height-40 "  popover="{{tag.name}}" ng-hide="tag.user_id == task.Task.created_by" >
                                            <span ng-bind="tag.name"></span>, 
                                        </p>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <!--task comments--> 
                        <div class="row">
                            <comment-form id="{{task.Task.id}}" add="addComment(id,body,'today')" delete="deleteCommentModal(id,commentId,'today')" comments="task.Comment" loadmore="{{task.moreComments}}" load="deskService.getComment(id,lastId)" commentcount=task.Task.comment_count limitcount="2"></comment-form>
                        </div>

                    </div>
                    <!--Tomorrow TASK END--> 

                    <!--THIS WEEK HEADING--> 
                    <div class='container-fluid' ng-show="TaskData.This_week.length > 0">
                        <div class="task-date-head row">
                            <div class="col-sm-5 date-lines"></div>
                            <div class="col-sm-2 padding-lr-0">
                                <p class="text-center">This week</p>
                            </div>
                            <div class="col-sm-5 date-lines"></div>

                        </div>
                    </div>
                    <!--this week start--> 
                    <div class="singleTask container-fluid" ng-show="TaskData.This_week.length > 0" ng-repeat="task in TaskData.This_week"> 
                        <!--Task detail--> 
                        <div class="taskDetails row">
                            <div class="col-sm-1">
                                <g-profile-img type="user" myclass='g-img-25' path="{{task.user_info.vga}}" name="{{task.user_info.name}}" ng-if="task.user_info" ></g-profile-img>
                            </div>
                            <div class="col-sm-8 padding-lr-0">
                                <div>
                                    <p class="text-bold"><span ng-bind="task.user_info.name" ng-click="showProfileofUser(task.user_info)" style="cursor:pointer"></span>, <span class="text-muted font-small-3" ng-bind="momentDate(task.Task.end_date)"></span></p>
                                    <p class="task-title" ng-bind="task.Task.title"></p>


                                </div>
                            </div>
                            <div class="col-sm-2 col-sm-offset-1">                              <li class="dropdown pull-right margin-left-10" dropdown>
                                    <p class="pull-right dropdown-toggle cursor-pointer" dropdown-toggle>
                                        <span class="fa fa-gear"></span>
                                    </p>
                                    <ul class="dropdown-menu task-settings-menu dropdown-menu-right text-center" role="menu">
                                        <li class=""><span class="fa fa-gears"></span> Edit Task</li>
                                        <li class="" ng-click="taskDeleteModal(task.Task.id, 'this_week')"><span class="fa fa-trash-o"></span> Delete Task</li>

                                    </ul>
                                </li>
                                <span ng-show="task.Task.priority" class="fa fa-bookmark color-red pull-right"></span>
                            </div>
                        </div>
                        <div class="taskShare row">
                            <div class="col-sm-7 col-sm-offset-1 padding-lr-0">
                                <p class="">
                                    <span ng-repeat="tag in task.Tags">
                                        <g-profile-img type="user" myclass='g-img-25 margin-right-5 font-small-2' path="{{tag.vga}}" name="{{tag.name}}"></g-profile-img>
                                    </span> 
                                    </span>
                                </p>
                            </div>
                            <div class="col-sm-3 col-sm-offset-1">
                                <button class="btn-mark-complete btn btn-sm btn-green btn-block font-small-2" ng-click="markTaskComplete(task.Task.id, task.Task.is_completed, 'this_week')">Mark Complete</button>
                            </div>
                        </div>
                        <!--task comments--> 
                        <div class="row">
                            <comment-form id="{{task.Task.id}}" add="addComment(id,body,'this_week')" delete="deleteCommentModal(id,commentId,'this_week')" comments="task.Comment" loadmore="{{task.moreComments}}" load="deskService.getComment(id,lastId)" commentcount=task.Task.comment_count></comment-form>
                        </div>
                    </div>

                    <!--this week TASK END-->
                    <div class="container-fluid" ng-if='TaskData.other.length > 0'>
                        <div class="task-date-head row">
                            <div class="col-sm-5 date-lines"></div>
                            <div class="col-sm-2 padding-lr-0">
                                <p class="text-center">Upcoming</p>
                            </div>
                            <div class="col-sm-5 date-lines"></div>
                        </div>
                    </div>
                    <!--UPCOMING TASK start--> 
                    <div class="singleTask container-fluid" ng-repeat="task in TaskData.Other">
                        <!--Task detail--> 
                        <div class="taskDetails row">
                            <div class="col-sm-1">
                                <g-profile-img type="user" myclass='g-img-25' path="{{task.user_info.vga}}" name="{{task.user_info.name}}"></g-profile-img>
                            </div>
                            <div class="col-sm-8 padding-lr-0">
                                <div>
                                    <p class="text-bold"><span ng-bind="task.user_info.name" ng-click="showProfileofUser(task.user_info)" style="cursor:pointer"></span>, <span class="text-muted font-small-3" ng-bind="momentDate(task.Task.end_date)"></span></p>
                                    <p class="task-title" ng-bind="task.Task.title"></p>


                                </div>
                            </div>
                            <div class="col-sm-2 col-sm-offset-1">                              <li class="dropdown pull-right margin-left-10 " dropdown>
                                    <p class="pull-right dropdown-toggle cursor-pointer" dropdown-toggle>
                                        <span class="fa fa-gear"></span>
                                    </p>
                                    <ul class="dropdown-menu task-settings-menu dropdown-menu-right text-center" role="menu">
                                        <li class=""><span class="fa fa-gears"></span> Edit Task</li>
                                        <li class="" ng-click="taskDeleteModal(task.Task.id, 'other')"><span class="fa fa-trash-o"></span> Delete Task</li>

                                    </ul>
                                </li>
                                <span ng-show="task.Task.priority" class="fa fa-bookmark color-red pull-right"></span>
                            </div>
                        </div>
                        <div class="taskShare row">
                            <div class="col-sm-7 col-sm-offset-1 padding-lr-0">
                                <p class="">
                                    <span ng-repeat="tag in task.Tags">
                                        <img class="round-30 shared-img" ng-src="{{tag.vga}}" popover="{{tag.name}}" popover-trigger="mouseenter" /> 
                                    </span>
                                </p>
                            </div>
                            <div class="col-sm-3 col-sm-offset-1">
                                <button class="btn-mark-complete btn btn-sm btn-green btn-block font-small-2" ng-click="markTaskComplete(task.Task.id, task.Task.is_completed, 'other')">Mark Complete</button>
                            </div>
                        </div>
                        <!--task comments--> 
                        <div class="row">
                            <comment-form id="{{task.Task.id}}" add="addComment(id,body,'other')" delete="deleteCommentModal(id,commentId,'other')" comments="task.Comment" loadmore="{{task.moreComments}}" load="deskService.getComment(id,lastId)" commentcount=task.Task.comment_count></comment-form>
                        </div>
                    </div>
                    <!--other upcoming TASK End-->
                </div>
            </div>
        </div>
        <div class="col-sm-4">
            <?php echo $this->element('Task/sidebar_tasks') ?>  
        </div>
    </div>
</div>


<!--TODAY HEADING--> 
                        <div class="container-fluid" ng-show="TaskData.Today.length">
                            <div class="task-date-head row">
                                <div class="col-sm-5 date-lines"></div>
                                <div class="col-sm-2 padding-lr-0">
                                    <p class="text-center text-bold font-small-1 date-heading ">Today</p>
                                </div>
                                <div class="col-sm-5 date-lines"></div>

                            </div>
                        </div>

                        <!-- today's single task --> 
                        <div id="task{{task.Task.id}}" class="singleTask container-fluid" ng-repeat="task in TaskData.Today| orderBy:'Task.end_date'">
                            <!--Task detail--> 
                            <div class="taskDetails row">
                                <!--creator--> 
                                <div class="col-sm-1 padding-right-0">
                                    <g-profile-img type="user" myclass='g-img-30' path="{{task.user_info.vga}}" name="{{task.user_info.name}}" ng-if="task.user_info"></g-profile-img>
                                </div>
                                <div class="col-sm-8 padding-lr-0">
                                    <p class=" line-height-16 color-black-alpha">
                                        <!--name--> 
                                        <span ng-bind="task.user_info.name" ng-click="showProfileofUser(task.user_info)" style="cursor:pointer" class="text-bold"></span>, 

                                        <span class="text-dim font-small-2 text-italic" >    
                                            <span class="" ng-bind="momentDate(task.Task.end_date)" ></span>,
                                            <span class="" ng-bind="dateFormat(task.Task.end_date, 'shortTime')"></span>
                                        </span>

                                    </p>
                                    <p class="task-title" ng-bind="task.Task.title" style=""></p>
                                </div>
                                <div class="col-sm-offset-1 col-sm-1 padding-lr-0 ">
                                    <p class="pull-right icon font-big-5 color-red icon-basic-bookmark" ng-show="task.Task.priority" style="margin-top: -15px" ></p>
                                </div>
                                <div class="col-sm-1">
                                    <!--task settings-->
                                    <li ng-show="task.Task.created_by == loggeduser" class="dropdown text-center" dropdown>
                                        <a href="#"  class="dropdown-toggle cursor-pointer icon icon2-arrows-down color-black-alpha " dropdown-toggle>

                                        </a>
                                        <ul class="dropdown-menu dropdown-animation task-settings-menu dropdown-menu-right" role="menu">
                                            <li class="container-fluid" ng-click="newTaskModal(task, 'today')">
                                                <a href="#" class="row">
                                                    <div class="col-sm-2 padding-lr-0" style="padding-top:5px;">
                                                        <span class="icon icon-basic-gear font-big-2" style=""></span>
                                                    </div>
                                                    <div class="col-sm-10 padding-tb-5">
                                                        <p class="" >
                                                            <span>Edit task</span>
                                                        </p>    
                                                    </div>
                                                </a> 
                                            </li>
                                            <li role="separator" class="divider"></li>
                                            <li class="container-fluid" ng-click="taskDeleteModal(task.Task.id, 'today')">
                                                <a href="#" class="row">
                                                    <div class="col-sm-2 padding-lr-0" style="padding-top:5px;">
                                                        <span class="icon icon-basic-trashcan font-big-2" style=""></span>
                                                    </div>
                                                    <div class="col-sm-10 padding-tb-5">
                                                        <p class="" >
                                                            <span>Delete Task</span>
                                                        </p>    
                                                    </div>
                                                </a>
                                            </li>

                                        </ul>
                                    </li>
                                </div>
                                <span class="clearfix"></span>
                                <!--                            <div class="col-sm-10 col-sm-offset-1 padding-lr-0">
                                                                task title 
                                                                <div class="col-sm-11 padding-lr-0">
                                                                    <p class="task-title" ng-bind="task.Task.title" style=""></p>
                                                                </div>
                                
                                                            </div>
                                                            <span class="clearfix"></span>-->
                                <div class="col-sm-offset-2 col-sm-8 margin-top-5">
                                    <!--<hr class="margin-tb-0"/>-->
                                </div>
                                <div class="col-sm-2" style="position: relative;">
                                    <div ng-click="markTaskComplete(task.Task.id, task.Task.is_completed, 'today')" class="btn-mark-complete pull-right icon icon2-arrows-check" >

                                    </div>
                                </div>
                                <span class="clearfix"></span>

                                <div class='col-sm-12 padding-left-0 taskBottomBox'>
                                    <!--task shared with-->
                                    <div class="col-sm-8 col-sm-offset-1 padding-left-0">
                                        <div class="task-shared-with " >
                                            <p class="font-small-2 text-dim cursor-pointer shared-with" ng-repeat="tag in task.Tags" ng-hide="tag.user_id == task.Task.created_by">
                                                <span ng-bind="tag.name"  popover="{{tag.name}}" popover-trigger="mouseenter"></span> 
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--task comments--> 
                            <div class="row">
                                <comment-form id="{{task.Task.id}}" add="addComment(id,body,'today')" delete="deleteCommentModal(id,commentId,'today')" comments="task.Comment" loadmore="{{task.moreComments}}" load="deskService.getComment(id,lastId)" commentcount=task.Task.comment_count limitcount="2"></comment-form>
                            </div>
                        </div>
                        <!--SINGLE TASK END-->
<?php
$this->layout = 'gridle_in';
$this->set('title_for_layout', 'Desk');
?>
<div class="container-fluid" ng-controller="deskController" ng-cloak>
    <div id="g-desk-wrap" class="row" style="" >
        <div id="g-desk-group-user-wrap" class="g-desk-group-user-wrap col-sm-3 col-xs-3" style="">
            <!--<group-user-relation id="g-desk-svg" style="position: absolute; top:0;" ng-if="groups.length > 0" ></group-user-relation>-->

            <!--EMPTY TEMPLATES--> 
            <div ng-if="groups.length < 1" class="row">
                <div class='container-fluid'>
                    <div class='col-xs-9'>
                        <object data='/img/empty-templates/desk_empty.svg' class="pull-left"></object>
                    </div>
                </div>
            </div>

            <!--SEARCH A GROUP OR USER--> 
            <div id="desk-search-box" class='input-group'>
                <input class="form-control g-search-input input-lg input-circular" placeholder="Search for people or team" type="text" ng-model="searchInput">                    
                <div class='input-group-addon input-circular-settings g-task-input-settings'>
                    <span class='fa fa-search'></span>
                </div>
            </div>

            <div id="accordion_desk" class='g-desk-group-user-area panel-group row' >


                <!--                <div class="col-sm-3 hidden-xs">
                                    <div class="g-desk-user-img-div"  ng-if="groups.length > 0">
                                        <h4><span>Me</span>
                                            <span id="g-desk-me" ng-click="deskInitSelectedItem()" ><g-profile-img type="user" myclass='g-desk-user-img' height='50px' width='50px' path="{{users[findUser(loggeduser)]['UserInfo']['vga']}}" name="{{users[findUser(loggeduser)]['UserInfo']['name']}}" ></g-profile-img></span></h4>
                
                                    </div>
                                    <p id="btn-tour-desk" class='btn-link p-margin-0 font-small-1' style="margin-top:20px; margin-left:20px;" ng-show="groups.length > 0" >Take a tour</p>
                                </div>-->

                <!--    GROUPS LISTING              -->
                <div class="col-sm-12">

                    <div class="container-fluid">

                        <div id='desk-groups' class="g-desk-group-area row">
                            <div id="itemGroup{{$index}}" class="g-desk-tile panel g-desk-tile-grp container-fluid" ng-repeat="group in filt_groups = (groups| dataSearchFilter: searchInput : 'group')" ng-click="deskSelectitem(group.Group.id, 'group')" ng-class="{'g-desk-tile-highlight': group.isRelated == 1, 'g-desk-tile-selected':deskSelectedItem.value == group.Group.id && deskSelectedItem.type == 'group'}" >
                                <div class="g-desk-tile-item g-desk-tile-item-grp row" data-toggle="collapse" data-parent="#accordion_desk" href="#grp_collapse_{{$index}}" >
                                    <div class="g-desk-tile-name col-xs-12">
                                        <g-profile-img type="group" myclass="g-desk-tile-group-img font-small-1" name="{{group.Group.name}}"></g-profile-img>
                                        <p class="g-desk-tile-group-name" ng-bind="group.Group.name" ></p>
                                    </div>
                                </div>
                                <!--    collapsible actions group -->
                                <div id="grp_collapse_{{$index}}" class="g-desk-tile-actions text-center panel-collapse collapse row" ng-class="{'g-desk-tile-highlight': group.isRelated == 1, 'g-desk-tile-selected':deskSelectedItem.value == group.Group.id}">
                                    <div class="">
                                        <div class='g-desk-tile-action col-xs-3'>
                                            <p class="p-margin-0"  ng-click="groupOpenSettings(group.Group.id)" data-original-title="Manage team {{group.Group.name}}" data-placement="bottom"  rel="tooltip" tooltip ><span class="fa fa-gear" ></span></p>
                                        </div>
                                        <div class='g-desk-tile-action col-xs-3'>
                                            <p class="p-margin-0"  data-toggle="modal" data-target="#taskAddModal" ng-click="taskNewInit(); tagsCreate('group', group.Group.id);" title='Assign task to {{group.Group.name}}' rel="tooltip" tooltip data-placement="bottom"><span class="fa fa-tasks"></span></p>
                                        </div>
                                        <div class='g-desk-tile-action col-xs-3'>
                                            <p class="p-margin-0"  ng-click="convModalCreateGroup(); tagsCreate('group', group.Group.id); " title='Send a message to {{group.Group.name}}' rel="tooltip" tooltip data-placement="bottom"><span class="fa fa-comment-o"></span></p>
                                        </div>
                                        <div class='g-desk-tile-action col-xs-3'>
                                            <p class="p-margin-0"  ng-click="fileModalUpload(); tagsCreate('group', group.Group.id);" title='Share a file with {{group.Group.name}}' rel="tooltip" tooltip data-placement="bottom" ><span class="fa fa-file-text" ></span></p>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <button ng-if="groups.length > 0" class="btn-success btn btn-block text-emphasize" style="margin-top: 10px; min-height: 40px;" id='btn-add-team'  ng-click='groupOpenCreate();' >
                                ADD A NEW TEAM                               
                            </button>
                        </div>

                    </div>
                </div>
                <!--    USERS LISTING              -->
                <div class="col-sm-12 ">
                    <div class="container-fluid">
                        <div id='desk-users' class="g-desk-users-area row">
                            <div id="itemUser{{$index}}" class="g-desk-tile g-desk-tile-user container-fluid panel" ng-repeat="user in  filt_users_in_groups = (users_in_groups| dataSearchFilter: searchInput : 'user')" ng-click="deskSelectitem(user.UserInfo.user_id, 'user');" ng-class="{'g-desk-tile-highlight':user.isRelated == 1, 'g-desk-tile-selected':deskSelectedItem.value == user.UserInfo.user_id && deskSelectedItem.type == 'user'}">
                                <div class="g-desk-tile-item-user g-desk-tile-item row" ng-click="draw_links();" data-toggle="collapse" data-parent="#accordion_desk" href="#usr_collapse_{{$index}}" >
                                    <div class="g-desk-tile-name col-xs-12">
                                        <g-profile-img type="user"  path="{{user.UserInfo.vga}}" height="25px" width="25px" myclass="g-desk-tile-user-img font-small-1 " name="{{user.UserInfo.name}}"  ></g-profile-img>
                                        <p class="g-desk-tile-user-name p-margin-0" ng-bind="user.UserInfo.name" ></p>
                                    </div>
                                </div>
                                <!--    collapsible actions user -->
                                <div id="usr_collapse_{{$index}}" class='g-desk-tile-actions text-center panel-collapse collapse row'>
                                    <div class='g-desk-tile-action col-xs-3'>
                                        <p class="p-margin-0" ng-click="showUserInfoModal(user.UserInfo.user_id);" title="View {{user.UserInfo.name}}'s information" tooltip data-placement='bottom' ><span class='fa fa-user'></span></p>    
                                    </div>
                                    <div class='g-desk-tile-action col-xs-3'>
                                        <p class="p-margin-0" data-toggle="modal" data-target="#taskAddModal" ng-click="taskNewInit(); tagsCreate('user', user.UserInfo.user_id);" data-toggle="tooltip" data-placement='bottom' title="Assign a task to {{user.UserInfo.name}}" tooltip ><span class='fa fa-tasks'></span></p>
                                    </div>
                                    <div class='g-desk-tile-action col-xs-3'>
                                        <p class="p-margin-0" ng-click="convModalCreate(); tagsCreate('user', user.UserInfo.user_id); " data-target="#convModalCreate" title="Send a message to {{user.UserInfo.name}}" tooltip data-placement='bottom' ><span class='fa fa-comment-o'></span></p>

                                    </div>
                                    <div class='g-desk-tile-action col-xs-3'>
                                        <p class="p-margin-0 "ng-click="fileModalUpload(); tagsCreate('user', user.UserInfo.user_id);" title="Share a file with {{user.UserInfo.name}}" tooltip data-placement='bottom' ><span class='fa fa-file-text'></span></p>
                                    </div>
                                </div>
                            </div>
                        </div>  
                    </div>
                </div>
                </span>

            </div>
        </div>

        <!--        TASK AREA           -->
        <div  class="g-desk-tasks-wrap g-desk-tasks-border-normal col-sm-9 col-xs-9s " style="">

            <div class="g-desk-user-area" >
                <!-- new task and search-->
                <div class='g-desk-search-area col-sm-7 col-sm-offset-3 col-xs-10 padding-lr-0 ' class="">
                    <textarea class="form-control" 
                              rows="3" 
                              placeholder="Write a new task" 
                              ng-model="TaskSelected.Task.title" 
                              ng-change="checkPatterns(TaskSelected.Task.title)"
                              ng-enter="enterNewTaskActivate ? taskAdd(TaskSelected) : false "
                              mentio
                              mentio-select-not-found="true"
                              mention-typed-term="currentMention"
                              mentio-require-leading-space="true"
                              mentio-id="'htmlContent'"
                              ></textarea>
                    <mentio-menu
                        mentio-for="'htmlContent'"
                        mentio-trigger-char="'@'"
                        mentio-items="mentionSuggestions"
                        mentio-template-url="/js/mention/mentio-menu.tpl.html"
                        mentio-select="getPeopleText(item)"
                        mentio-search="searchPeople(term, currentMention)"
                        ></mentio-menu>

                    <!--    TASK SUGGESTION CARD        -->
                    <div class="col-sm-12 g-background-nutral-green task-suggestion-card" ng-show="TaskSelected.Task.title">
                        <div class="col-sm-6">
                            <p class="g-color-white">Task title</p>
                            <p ng-bind="TaskSelected.Task.title"></p>
                        </div>
                        <div class="col-sm-6 font-small-1">
<!--                            <p class="g-color-white">Start date</p>
                            <p ng-bind="FoundStartDate"></p>-->
                            <p class="g-color-white">End date</p>
                            <p ng-bind="dateFormat(FoundEndDate, 'medium')"></p>
                        </div>
                    </div>
                    <style>
                        .task-suggestion-card{

                        }
                    </style>

                </div>

                <!--                 new team, new task and search buttons
                                <div class="g-desk-add-grp-cover  col-xs-2">
                                    <button id="btn-desk-search" class="font-small-1  btn btn-default btn-sm g-desk-btn-grp" title='Create a new task' data-placement="right"  rel="tooltip" tooltip  ><span class='fa fa-tasks'></span></button>
                                    <button id="btn-new-task" class="font-small-1  btn btn-default btn-sm g-desk-btn-grp" title='Search team or members' data-placement="right"  rel="tooltip" tooltip ><span class='fa fa-search'></span></button>
                
                                </div>-->
            </div>
            <div id="tasks-listing" class="g-desk-tasks-area container-fluid">
                <div class="desk-tasks-filter row">
                    <div class="col-xs-12 padding-lr-0">
                        <div class="text-center">
                            <p class="btn-task-filter dropdown-toggle p-margin-0" data-toggle="dropdown">{{filterBy.name}} <span class="caret"></span></p>
                            <ul class="dropdown-menu dropdown-menu-right " role="menu">
                                <li ng-repeat="filterOption in TaskDeskfilterOptions" class="">
                                    <a href="#" ng-click="selectFilter(filterOption.id)" ng-bind-html="filterOption.name"></a>
                                </li>
                            </ul>    
                        </div>
                    </div>
                    <div class='col-xs-12 padding-lr-0'>
                        <div class='col-xs-offset-2 col-xs-8 tasks-separator'></div>
                    </div>
                    <div class="task-filter-msg-div text-center col-xs-12 padding-lr-0" ng-class="{'g-background-nutral-green ' : filterWith[0].type == 'group' || filterWith[0].type == 'user' }" >
                        <p class="p-margin-0" ng-hide="filterWith"><span class='fa fa-circle'></span>&nbsp;&nbsp; Showing all your tasks</p>
                        <p class="p-margin-0 " ng-show="filterWith"> Showing tasks with 
                            <span class="text-regular"  ng-repeat="tag in filterWith">
                                <span class="" ng-show="tag.type == 'user'" ng-bind-html="users[getUserIndex(tag.value)].UserInfo.name"></span>
                                <span ng-show="tag.type == 'group'" ng-bind-html="groups[findGroup(tag.value)].Group.name"></span>
                            </span>.            
                            <a href="#" ng-click="deskInitSelectedItem()"> Reset</a>
                        </p>
                    </div>
                    <div class='col-xs-12 padding-lr-0'>
                        <div class='col-xs-offset-2 col-xs-8 tasks-separator'></div>
                    </div>
                </div>

                <?php
                echo $this->element('Task/view');
                ?>
            </div>
        </div>        

    </div>

    <?php
    //Group Management Modal
    echo $this->element('Group/manage');
    //Group Create Modal
    echo $this->element('Group/new_grp');
    //new task modal
    echo $this->element('Task/new_task');
    echo $this->element('Task/single_task_view');
    echo $this->element('Conversation/create');
    echo $this->element('Conversation/manage');
    echo $this->element('Conversation/group_create');
    echo $this->element('Doc/upload');
    ?>
</div>
<style>
    .tour-highlight{

    }
</style>
<script>
                    $(document).ready(function() {
            var intro = introJs();
                    steps = [
                    {
                    intro: "<span class='text-regular'>Welcome!</span> We are excited to have you at Gridle.<br/><br/> From here, you will be able to manage tasks, send messages and share files across your teams.. We call this <span class='text-regular'>'Desk'</span>.<br/><br/>  Quickly, lets show you around. Go on, click on next or use <span class='text-regular'>arrow keys to navigate.</span>",
                    },
                    {
                    element:'#network-dropdown',
                            intro:"This is your network area, from <span class='text-regular'>Dashboard</span> you can <span class='text-regular'>add people into your network</span> and seee the statstics of network.",
                            highlightClass:"tour_highlight",
                            position:'right'
                    },
                    {
                    element: '#desk-groups',
                            intro: "All the teams that you are a part of will be listed here.<br/><br/> <span class='text-regular'>Click on the team</span> to view details, <span class='text-regular'>assign tasks, share files and send messages.</span>",
                            position: 'right'
                    },
                    {
                    element: '#desk-users',
                            intro: "All <span class='text-regular'>your team-mates</span> are here. Just like teams, you can click on them to interact.",
                            position: 'left'
                    },
                    {
                    element: '#tasks-listing',
                            intro: "All your <span class='text-regular'>tasks</span> are listed over here. You can click on team or team-mates on the left to view co-related tasks. <br/><br/>Also you can <span class='text-regular'>mark your tasks as done</span> from here.",
                            position: 'left'
                    },
                    {
                    element: '#new-task-box',
                            intro: "Great! <span class='text-regular'>Create a task</span> as your first step towards better work.",
                            position: 'bottom'
                    }
                    ];
                    intro.setOptions({
                    steps: steps,
                            tooltipClass: 'g-tour-tip',
                            nextLabel: 'Next',
                            prevLabel: 'Prev'
                    });
                    //Additional activity on each step of tour            
                    intro.onbeforechange(function (targetElement) {
                    $.each(steps, function (index, step) {
                    if ($(targetElement).is(step.element)) {
                    console.log("index", index);
                            switch (index) {
                    case 1:
                            $('#network-logo').parent().addClass('open');
                            break;
                            case 2:
                            $('#network-logo').parent().removeClass('open');
                            jQuery('.g-desk-tile-item-grp:first').click();
                            break;
                            case 3:
                            jQuery('.g-desk-tile-item-user:first').click();
                            break;
                    }
                    return false;
                    }
                    });
                    });
                    intro.onafterchange(function (targetElement) {
                    $.each(steps, function (index, step) {
                    if ($(targetElement).is(step.element)) {
                    switch (index) {
                    case 5:
                            jQuery('#input-new-task').focus();
                            break;
                    }
                    return false;
                    }
                    });
                    });
                    //Tour call on button click
                    $('#btn-tour-desk').click(function() {
            intro.start();
                    console.log("here")
            });
<?php
if (isset($AuthUser['FirstSession'])) {
    ?>
                //Auto start the tour if first session
                intro.start();
    <?php
}
?>
            });</script>
<script>
                    function opentaskmodel(evt) {
                    $('#newTaskBtn').trigger('click');
                    }
            function openteammodel(evt) {
            $('#groupModalCreate').modal('show');
            }
</script>
<script type="text/javascript" src="/js/date.js"></script>

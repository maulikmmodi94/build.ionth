<?php
$this->layout = 'gridle_in';
$this->set('title_for_layout', 'Activity Log');
?>
<div class="container-fluid" ng-controller="logController">
    <div class="log-page-wrap row">
        <div class="col-lg-offset-3 col-lg-6 col-md-offset-2 col-md-8 col-sm-8 col-sm-offset-1 col-xs-offset-1 col-xs-10">
            <!--    ACTIVITY LOG OPTIONS        -->
            <div class="activity-log-options container-fluid">
                <div class="row" >
                    <div class="col-lg-4 col-md-4 col-sm-5 col-xs-6">
                        <p class="font-big-4 text-muted text-uppercase" >Activity Log</p>
                    </div>
                    <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-6">
                        <div class="input-group input-group-sm">
                        <input class="form-control g-search-box" type="text" placeholder="Search any log" ng-model="searchtext.message">
                        <div class="input-group-addon input-group-addon-round">
                            <span class="fa fa-search"></span>
                        </div>
                        </div>
                            
                    </div>
                </div>
            </div>
            <!--        ACTIVITY LOG DETAILS            -->
            <div class="container-fluid" ng-repeat="log in (filteredLog = logs|filter:searchtext)" ng-init="logdate = log.time">
                <!--        DATE OF THE LOG             -->
                <div class="row" ng-if="$index==0 || (dateFormat(filteredLog[$index].time, 'shortDate') != dateFormat(filteredLog[$index - 1].time, 'shortDate'))">
                    <div class='log-date-heading'>
                        <p class="label label-warning font-big-1" ng-bind="momentDate(log.time)"  ></p>
                    </div>
                </div>
                <!--  LOG EXPANSION           -->
                <a href="{{log.link}}">
                    <div class="log-message-div row">
                        <div class="col-lg-1 col-md-1 col-sm-1 col-xs-2 padding-lr-0">
                            <p class='log-message-icon p-margin-0' ng-class="{'success-icon': log.type =='task' && log.activity=='taskcomplete'}">
                                <span class="fa" ng-class="{group:'fa-group',task:'fa-tasks',network:'fa-circle',file:'fa-file'}[log.type]"></span>
                            </p>    
                        </div>
                        <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10 padding-lr-0">
                            <p class='log-message text-muted p-margin-0'>
                                <span class=''  ng-bind-html="log.message"></span>
                            </p>
                        </div>
                    </div>
                </a>
            </div>
            <button ng-if="logsMoreOlder" ng-click="getLogsData(logsLoadedPage + 1)">Load Older</button>
        </div>
    </div>
</div>

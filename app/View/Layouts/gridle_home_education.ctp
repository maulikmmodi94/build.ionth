<!DOCTYPE html>
<html ng-app="GridleApp">
    <head>
        <?php echo $this->Html->charset(); ?>
        <title>
            <?php echo $title_for_layout; ?>
        </title>
        <!--    Favicon -->
        <link rel="shortcut icon" href="/favicon.ico?v=2" />
        <!--For responsive support--> 
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?php
        echo $this->Html->css('styles.css');
        echo $this->Html->script(JS_license);

        echo $this->fetch('meta');
        echo $this->fetch('css');
        echo $this->fetch('script');
        ?>
    </head>
    <body>
        <!-- HEADER -->
        <div id="gridle_header" class="one-edge-shadow container-fluid" >
            <!--    gridle logo -->
            <p class="g-logo-gridle-cover p-margin-0">
                <a id="g-logo-gridle" href="/">
                    <object data="/img/logos/gridle_logo.svg" type="image/svg+xml">
                        <img src="/img/logos/gridle_logo.png" alt="gridle logo" />
                    </object>
                </a>
            </p>

            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#g-header-left">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>
            <div id="g-header-left" class="navbar-collapse collapse"> 
                <?php if (!is_null(CakeSession::read('Auth.User.username'))) { ?>
                    <!--        if logged in            -->
                    <ul id="g-header-right" class="nav navbar-nav navbar-right">
                        <li><a href="/networks" class="btn-link" ><?php echo CakeSession::read('Auth.User.UserInfo.name'); ?></a></li>
                        <li><a href="/users/logout" ><button class="btn btn-default btn-sm">Log out</button></a></li>
                    </ul>
                <?php } else { ?>
                    <ul id="g-header-right" class="navbar navbar-nav navbar-right">
                        <!--        if logged out            -->
                        <li class="hidden-xs">Not on gridle yet ? </li>
                        <li><a href="/users/add" ><button class="btn btn-primary btn-sm content_highlight" style="color: white" >Sign up</button></a></li>
                        <li><a href="/users/login" class="btn-link">Login </a></li>
                    </ul>
                <?php } ?>

            </div>

        </div>

        <!--    HEADER ENDS             -->

        <div class="container-fluid home_wrap">
<!--            <p style="">
                <span class="pull-right"> <?php
            $flash = $this->Session->flash();
            if ($flash) {
                ?><?php echo $flash; ?><?php } ?> 
                </span>
            </p>-->
            <?php echo $this->fetch('content'); ?>

        </div>
        <?php echo $this->Js->writeBuffer(array('cache' => true)); ?>
        <!-- FOOTER-->
        <div id="gridle_footer" class="container-fluid" >
            <div class="row">
                <div class="footer-section col-lg-3 col-md-3 col-xs-6">
                    <p class="footer-section-heading">
                        GRIDLE MORE
                    </p>
                    <p>
                        <a class="footer-section-links" href="/pages/features">Features</a><br/>
                        <a class="footer-section-links" href="/pages/benefits">Benefits</a><br/>
                        <!--<a class="footer-section-links" href="">Clients</a><br/>-->
                        <!--<a class="footer-section-links" href="">Investors</a><br/>-->
                        <a class="footer-section-links" href="/pages/press">Press</a><br/>
                    </p>
                    <p class="footer-section-heading" style="margin-top: 20px;">
                        GRIDLE FOR EDUCATORS
                    </p>
                    <p>
                        <a class="footer-section-links" href="/pages/education">Education</a><br/>
                    </p>
                </div>
                <div class="footer-section col-lg-3 col-xs-6 col-md-3">
                    <p class="footer-section-heading">
                        GETTING STARTED
                    </p>
                    <p>
                        <a class="footer-section-links" href="/pages/pricing">Pricing</a><br/>
                        <!--<a class="footer-section-links" href="">Tutorial</a><br/>-->
                        <a class="footer-section-links" href="/pages/terms">Policies</a><br/>
                        <a class="footer-section-links" href="/pages/case-studies">Case studies</a><br/>
                    </p>
                </div>
                <div class="footer-section col-lg-3 col-md-3 col-xs-6">
                    <p class="footer-section-heading">
                        RESOURCES & SUPPORT
                    </p>
                    <p>
                        <a class="footer-section-links" href="/pages/support">Support</a><br/>
                        <!--<a class="footer-section-links" href="">Resources</a><br/>-->
                        <!--<a class="footer-section-links" href="">White-paper</a><br/>-->
                    </p>
                </div>
                <div class="footer-section col-lg-3 col-md-3 col-xs-6">
                    <p class="footer-section-heading">
                        CONTACT & INFO
                    </p>
                    <p>
                        <a class="footer-section-links" href="https://medium.com/@gridle" target="_blank">Blog</a><br/>
                        <a class="footer-section-links" href="/pages/about">About</a><br/>
                        <a class="footer-section-links" href="/pages/contact">Contact</a><br/>
                        <a class="footer-section-links" href="https://www.facebook.com/gridle.in" target='_blank'>Facebook</a><br/>
                        <a class="footer-section-links" href="https://twitter.com/gridle_io" target='_blank'>Twitter</a>
                    </p>
                </div>

            </div>
            <div id="copyright_sec" class="row">
                <p id="copyright_brand" class="pull-right">Copyright ©  2014,Pivoting Softwares Pvt. Ltd.</p>
            </div>

        </div>
        <div id="copyright_logos" >
            <p class="">
                <a href="http://global.sap.com/corporate-en/innovation/start-up-focus-program/index.epx" target="blank">
                    <img class="investor_logo" src="/img/investors/sap.png" alt="gridle, SAP startup focus member" height="40" alt="sap" />
                </a>
                <a href="http://hiracoventures.com" target="blank">
                    <img class="investor_logo" src="/img/investors/hiracoventures.png" alt="gridle investor Hiraco Ventures" height="40" alt="hiracoventures"/></a>
                <a href="http://ciieindia.org/" target="blank">
                    <img class="investor_logo" src="/img/investors/ciie_logo.png" alt="gridle investor CIIE" height="40" style="margin:0 30px;" alt="ciie"/>
                </a>
                <a href="http://www.microsoft.com/bizspark/" target="blank">
                    <img class="investor_logo" src="/img/investors/microsoft_bizspark.jpg" alt="gridle Microsoft Bizspark plus member" height="40" alt="bizspark" />
                </a>
            </p>
        </div>

        <!--    Javascript files         -->
        <?php echo $this->Html->script('bootstrap/collapse.js', false); ?>
        <script>
            app = angular.module('GridleApp', ['ngSanitize']);
        </script>       
    </body>
</html>
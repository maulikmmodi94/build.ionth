<!DOCTYPE html>
<html  ng-app="GridleApp">
    <head>
        <?php echo $this->Html->charset(); ?>
        <title>
            <?php echo $title_for_layout; ?>
        </title>
        <!--    Favicon -->
        <link rel="shortcut icon" href="/favicon.ico?v=2" />

        <!--For responsive support--> 
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <script>

            window.paceOptions = {
                //target: '.preloader',
                ajax: {
                    ignoreURLs: ['google-analytics', 'mouseflow', 'inspectlet', 'pollings', 'maps', 'amazonaws', 'cloudfront', 'userjoy', 'gstatic', 'maps.gstatic.com', 'fonts.gstatic']

                },
                restartOnRequestAfter: false

            };
        </script>
        <?php
        echo $this->Html->css('style.css');
        echo $this->fetch('meta');
        echo $this->fetch('css');
        echo $this->Html->script(JS_license);
//        echo $this->Html->script('pace.min.js');
        echo $this->fetch('script');
        ?>
        <script>

            Pace.on('done', function ()
            {
                $(Pace.options.target).css('display', 'none');
            });
            Pace.on('start', function ()
            {
                $(Pace.options.target).css('display', 'block');
            });
        </script>
    </head>
    <body class="container-fluid">
        <div class="preloader row"></div>

        <div class="g-page-wrap row">
            <?php
            echo $this->element('header_in');

            if (!$AuthUser['email_verified']) {
                echo $this->element('email_verification_stripe');
            }

            echo $this->fetch('content');

            echo $this->element('footer_in');

            if (Configure::read('server_mode') == 'development') {
                echo $this->element('sql_dump');
            }
            ?>
        </div>
        <?php
        echo $this->Html->script('modules/comments/comment.js');


        echo $this->Html->script(JS_gridle);

        //Intro.js for tour
        $message = strip_tags($this->Session->flash());

        if (!empty($message)) {
            ?>
            <script>
            $.bootstrapGrowl('<?= $message ?>', {
                ele: 'body', // which element to append to
                type: 'info', // (null, 'info', 'danger', 'success')
                offset: {from: 'top', amount: 50}, // 'top', or 'bottom'
                align: 'right', // ('left', 'right', or 'center')
                width: 250, // (integer, or 'auto')
                delay: 4000, // Time while the message will be displayed. It's not equivalent to the *demo* timeOut!
                allow_dismiss: true, // If true then will display a cross to close the popup.
                stackup_spacing: 10 // spacing between consecutively stacked growls.
            });
            </script>
        <?php } ?>

        <script>
            //Stop event bubbling to parent on scroll
            // It doesn't scroll the whole page on scrolling a div
            $('.g-scrollbar').on('DOMMouseScroll mousewheel', function (ev) {
                var $this = $(this),
                        scrollTop = this.scrollTop,
                        scrollHeight = this.scrollHeight,
                        height = $this.height(),
                        delta = (ev.type == 'DOMMouseScroll' ?
                                ev.originalEvent.detail * -40 :
                                ev.originalEvent.wheelDelta),
                        up = delta > 0;
                var prevent = function () {
                    ev.stopPropagation();
                    ev.preventDefault();
                    ev.returnValue = false;
                    return false;
                }

                if (!up && -delta > scrollHeight - height - scrollTop) {
                    // Scrolling down, but this will take us past the bottom.
                    $this.scrollTop(scrollHeight);
                    return prevent();
                } else if (up && delta > scrollTop) {
                    // Scrolling up, but this will take us past the top.
                    $this.scrollTop(0);
                    return prevent();
                }
            });
        </script>
        <?php
//Analytics 
        if (Configure::read('server_mode') == 'production') {
            echo $this->element('analytics/production');
        } else if (Configure::read('server_mode') == 'development') {
            echo $this->element('analytics/development');
        }
        ?>
    </body>
</html>

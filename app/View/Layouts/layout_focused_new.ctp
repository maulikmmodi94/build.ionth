<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title><?php echo $title_for_layout; ?></title>

        <!--    Favicon -->
        <link rel="shortcut icon" href="/favicon.ico?v=2" />


        <!--For responsive support--> 
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!--CSS--> 
         <?php
        echo $this->fetch('script');
        ?>
        <link rel="stylesheet" href="/css/gridle_vendor.css" />
        <link rel="stylesheet" href="/css/gridle.css" />
        <link rel="stylesheet" href="/css/linea.css" />
        <link rel="stylesheet" href="/css/arrows.css" />
        <link rel="stylesheet" href="/css/vendors/ngTags/ngTags.css" />
        <link rel="stylesheet" href="/css/vendors/angular-growl.css" />
        <link rel="stylesheet" href="/css/vendors/datepicker/datepicker.css" />
            <script type="text/javascript" src="/scripts/vendor.js"></script>
            <script type="text/javascript" src="/js/mention/mentio.min.js"></script>
            <script type="text/javascript" src="/js/mention/mentio.service.js"></script>
            <script type="text/javascript" src="/scripts/Vendor/min/datepicker.js"></script>
            <script type="text/javascript" src="/scripts/Comment/comment.js"></script>
            <script type="text/javascript" src="/scripts/Feed/angular.embedly.js"></script>
            <script type="text/javascript" src="/scripts/gridle_max.js"></script>
            <script src="/js/appear/appear.js"></script>
    </head>
    <body  ng-app="GridleApp">
        <div class="container-fluid">
        <div growl></div>
        <div class="row">
            
<!--            <div class="col-sm-12 padding-lr-0" style="position: relative; padding-top: 50px;">-->
<!--                <div class="row">-->
<!--                    <div class="col-sm-12">-->
                      <?php
                echo $this->element('header_focused_new');
                echo $this->element('notification_bar');
                    ?>
        </div>
<!--                </div>-->
                <div class="row">
<!--                    <div class="col-sm-12">-->
                <?php
//                echo $this->element('header_in_new');
                
                if (!$AuthUser['email_verified']) {

                    echo $this->element('email_verification_stripe');
                }
                
                echo $this->fetch('content');
                ?>
<!--                    </div>-->
                </div>
<!--            </div>-->

            <?php
            echo $this->element('footer_in_new');
            ?>
            
            <?php
            $message = strip_tags($this->Session->flash());

            if (!empty($message)) {
                ?>
                <div ng-init="growlNotify('<?= $message ?>')"></div>
            <?php } ?>

            <?php
//Analytics 
            if (Configure::read('server_mode') == 'production') {
                echo $this->element('analytics/production');
            } else if (Configure::read('server_mode') == 'development') {
                echo $this->element('analytics/development');
            }
            ?>
                <script src="//cdn.embedly.com/widgets/platform.js" async></script>

            <!--Our script-->        
<!--        </div>-->
        </div>
        <audio id="notifySound"><source src="https://s3-us-west-2.amazonaws.com/gridle-public/notify.mp3" type="audio/mpeg"></audio>
    </body>
</html>

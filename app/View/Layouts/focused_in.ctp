<!DOCTYPE html>
<html  ng-app="GridleApp">
    <head>
        <?php echo $this->Html->charset(); ?>
        <title>
            <?php echo $title_for_layout; ?>
        </title>
        <!--    Favicon -->
        <link rel="shortcut icon" href="/favicon.ico?v=2" />

        <?php
        echo $this->Html->css('style.css');
        echo $this->fetch('meta');
        echo $this->fetch('css');
        echo $this->Html->script('licensed');

        echo $this->fetch('script');
        ?>
    </head>
    <body class="container-fluid g-scrollbar">
        <div class="g-page-wrap row">
            <?php echo $this->element('header_focused_in'); ?>
            <?php
            if (!$AuthUser['email_verified']) {
                echo $this->element('email_verification_stripe');
            }
            ?>
            <?php echo $this->fetch('content'); ?>
            <?php echo $this->element('footer_in'); ?>
            
            <?php
            if(Configure::read('server_mode')== 'development'){
            echo $this->element('sql_dump');} 
            ?>
        
        </div>
        <?php
        echo $this->Html->script('gridle_180814');
        ?>

        <?php
        $message = strip_tags($this->Session->flash());

        if (!empty($message)) {
            ?>
            <script>
                $.bootstrapGrowl('<?= $message ?>', {
                    ele: 'body', // which element to append to
                    type: 'info', // (null, 'info', 'danger', 'success')
                    offset: {from: 'top', amount: 20}, // 'top', or 'bottom'
                    align: 'right', // ('left', 'right', or 'center')
                    width: 250, // (integer, or 'auto')
                    delay: 4000, // Time while the message will be displayed. It's not equivalent to the *demo* timeOut!
                    allow_dismiss: true, // If true then will display a cross to close the popup.
                    stackup_spacing: 10 // spacing between consecutively stacked growls.
                });

                $(document).ready(function() {
                    jQuery('.g-tooltip').tooltip('show');
                });

            </script>
        <?php } ?>
        <?php
        //echo $this->Html->script('bootstrap/collapse.js', false);
        ?>
    </body>
</html>

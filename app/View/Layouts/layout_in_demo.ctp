<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title><?php echo $title_for_layout; ?></title>

        <!--    Favicon -->
        <link rel="shortcut icon" href="/favicon.ico?v=2" />


        <!--For responsive support--> 
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!--CSS--> 
        <link rel="stylesheet" href="/css/gridle_vendor.css" />
        <link rel="stylesheet" href="/css/gridle.css" />

        <script type="text/javascript" src="/js/jquery.min.js" ></script>
        <script type="text/javascript" src="/js/angular.min.js"></script>
    </head>
    <body class="container-fluid" ng-app="GridleApp">
        <div class="row" ng-controller="toggleCtrl" >

            <!--LEFT SIDEBAR--> 
            <?php
            echo $this->element('sidebar_left_demo');
            ?>
            <div class="col-sm-10 col-sm-offset-2 padding-lr-0" style="position: relative; padding-top: 50px;">
                <?php
                echo $this->element('header_in_new_demo');
                echo $this->element('notification_bar_demo');
                if (!$AuthUser['email_verified']) {

                    echo $this->element('email_verification_stripe');
                }

                echo $this->fetch('content');
                ?>
            </div>

            <?php
            echo $this->element('footer_in_new');
            ?>


            <?php
            $message = strip_tags($this->Session->flash());

            if (!empty($message)) {
                ?>
                <script>
                            $.bootstrapGrowl('<?= $message ?>', {
                                ele: 'body', // which element to append to
                                type: 'info', // (null, 'info', 'danger', 'success')
                                offset: {from: 'top', amount: 50}, // 'top', or 'bottom'
                                align: 'right', // ('left', 'right', or 'center')
                                width: 250, // (integer, or 'auto')
                                delay: 4000, // Time while the message will be displayed. It's not equivalent to the *demo* timeOut!
                                allow_dismiss: true, // If true then will display a cross to close the popup.
                                stackup_spacing: 10 // spacing between consecutively stacked growls.
                            });
                </script>
            <?php } ?>

            <?php
//Analytics 
            if (Configure::read('server_mode') == 'production') {
                echo $this->element('analytics/production');
            } else if (Configure::read('server_mode') == 'development') {
                echo $this->element('analytics/development');
            }
            ?>

            <!--Vendors-->     
            <script type="text/javascript" src="/js/bootstrap-angular.js"></script>
            <script type="text/javascript" src="/js/angular-animate.min.js"></script>
            <script type="text/javascript" src="/js/underscore/underscore-min.js"></script>
            <script type="text/javascript" src="/js/mention/mentio.min.js"></script>
            <script src="/js/appear/appear.js"></script>
            <!--<script src="/js/kendo.all.min.js"></script>-->

            <!--Our script--> 
            <script type="text/javascript" src="/js/modules/comments/comment.js"></script>
            <script type="text/javascript" src="/js/app.js"></script>
            <!--KENDO--> 
<!--            <script src="/js/datetimepicker/kendo.core.min.js"></script>
            <script src="/js/datetimepicker/kendo.dom.min.js"></script>
            <script src="/js/datetimepicker/kendo.view.min.js"></script>
            <script src="/js/datetimepicker/kendo.window.min.js"></script>
            <script src="/js/datetimepicker/kendo.data.min.js"></script>
            <script src="/js/datetimepicker/kendo.touch.min.js"></script>
            <script src="/js/datetimepicker/kendo.selectable.min.js"></script>
            <script src="/js/datetimepicker/kendo.datetimepicker.min.js"></script>
            <script src="/js/datetimepicker/kendo.combobox.min.js"></script>
            <script src="/js/datetimepicker/kendo.angular.min.js"></script>-->
            
        </div>

    </body>
</html>

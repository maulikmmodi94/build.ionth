<!DOCTYPE html>
<html  ng-app="GridleApp">
    <head>
        <?php echo $this->Html->charset(); ?>
        <title>
            <?php echo $title_for_layout; ?>
        </title>
        <!--    Favicon -->
        <link rel="shortcut icon" href="/favicon.ico?v=2" />
        <script>

            window.paceOptions = {
                target: '.preloader',
                ajax: {
                    ignoreURLs: ['pollings', 'maps', 'amazonaws', 'gstatic','cloudfront','userjoy']
                },
                restartOnRequestAfter: false

            };
        </script>
        <?php
        //echo $this->Html->meta('icon');
        echo $this->Html->css('styles.css');
        echo $this->Html->script('licensed');
        //echo $this->Html->css('licensed');

        echo $this->fetch('meta');
        echo $this->fetch('css');
        echo $this->Html->script('pace.min.js');

        echo $this->fetch('script');
        ?>
        <script>

            Pace.on('done', function()
            {
                $(Pace.options.target).css('display', 'none');
            });
            Pace.on('start', function()
            {
                $(Pace.options.target).css('display', 'block');
            });
        </script>
    </head>
    <body class="container-fluid">
        <div class="preloader row"></div>

        <div class="row">
            <?php echo $this->element('header_focused_out'); ?>
            <?php echo $this->fetch('content'); ?>
        </div>
        <?php
        echo $this->Html->script('gridle_180814');
        ?>
        <?php
        $message = strip_tags($this->Session->flash());

        if (!empty($message)) {
            ?>
            <script>
                $.bootstrapGrowl('<?= $message ?>', {
                    ele: 'body', // which element to append to
                    type: 'info', // (null, 'info', 'danger', 'success')
                    offset: {from: 'top', amount: 20}, // 'top', or 'bottom'
                    align: 'right', // ('left', 'right', or 'center')
                    width: 250, // (integer, or 'auto')
                    delay: 4000, // Time while the message will be displayed. It's not equivalent to the *demo* timeOut!
                    allow_dismiss: true, // If true then will display a cross to close the popup.
                    stackup_spacing: 10 // spacing between consecutively stacked growls.
                });
            </script>
        <?php } ?>
        <script>
            app.controller('emptyFormsController', ['$scope', function($scope) {

                }]);
        </script>
    </body>
</html>

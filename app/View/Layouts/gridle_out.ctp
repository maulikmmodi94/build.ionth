<!DOCTYPE html>
<html  ng-app="GridleApp">
    <head>
        <?php echo $this->Html->charset(); ?>
        <title>
            <?php echo $title_for_layout; ?>
        </title>
        <?php
        //meta tags
        echo $this->Html->meta(array('name' => 'keywords', 'content' => 'project management, task management, collaboration tool, enterprise, teams, calendar, planner'));
        echo $this->Html->meta(array('name' => 'description', 'content' => 'Gridle is a complete cloud visual collaboration and communication tool for your time, task and project management for enterprises and teams.'));
//robot meta tag
        echo $this->Html->meta(array('name' => 'robots', 'content' => 'index, follow'));
//facebook open graph meta tags
        echo $this->Html->meta(array('property' => 'og:title', 'content' => 'gridle | visual cloud collaboration tool for enterprises and teams'));
        echo $this->Html->meta(array('property' => 'og:description', 'content' => 'Gridle is a complete cloud visual collaboration and communication tool for your time, task and project management for enterprises and teams.'));
        echo $this->Html->meta(array('property' => 'og:type', 'content' => 'website'));
        echo $this->Html->meta(array('property' => 'og:image', 'content' => 'http://gridle.io/img/logo/gridle_logo.png'));
        echo $this->Html->meta(array('property' => 'og:image:url', 'content' => 'http://gridle.io/img/logo/gridle_logo.png'));
        echo $this->Html->meta(array('property' => 'og:image:width', 'content' => '150'));
        echo $this->Html->meta(array('property' => 'og:image:height', 'content' => '50'));
        echo $this->Html->meta(array('property' => 'og:url', 'content' => 'http://gridle.io'));
        echo $this->Html->meta(array('property' => 'og:site_name', 'content' => 'Gridle'));
        echo $this->Html->meta(array('property' => 'fb:admins', 'content' => 'anupama.panchal'));
        echo $this->Html->meta(array('property' => 'fb:app_id', 'content' => '642120029214138'));
//twitter meta tags
        echo $this->Html->meta(array('name' => 'twitter:card', 'content' => 'summary'));
        echo $this->Html->meta(array('name' => 'twitter:site', 'content' => '@gridle_io'));
        echo $this->Html->meta(array('name' => 'twitter:image:src', 'content' => 'http://gridle.io/img/logo/gridle_logo.png'));
        ?>
        <!--    Favicon -->
        <link rel="shortcut icon" href="/favicon.ico?v=2" />
        <link rel="stylesheet" href="/css/vendors/angular-growl.css" />
        <link rel="stylesheet" href="/css/gridle_out.css" />
        <script type="text/javascript" src="/scripts/vendor.js"></script>
        <script type="text/javascript" src="/scripts/Vendor/min/angular-growl.js"></script>
        <script type="text/javascript" src="/scripts/gridle_out_max.js"></script>
        <script>

            window.paceOptions = {
                //target: '.preloader',
                ajax: {
                    ignoreURLs: ['google-analytics', 'mouseflow', 'inspectlet', 'pollings', 'maps', 'amazonaws', 'cloudfront', 'userjoy', 'fonts']
                },
                restartOnRequestAfter: false

            };
        </script>
        <?php
        echo $this->Html->css('styles.css');
        echo $this->Html->script(JS_license);

        echo $this->fetch('meta');
        echo $this->fetch('css');
        echo $this->Html->script('pace.min.js');

        echo $this->fetch('script');
        ?>
        <script>

            Pace.on('done', function ()
            {
                $(Pace.options.target).css('display', 'none');
            });
            Pace.on('start', function ()
            {
                $(Pace.options.target).css('display', 'block');
            });
        </script>
        <?php
        //meta tags
        echo $this->Html->meta(array('name' => 'keywords', 'content' => 'project management, task management, collaboration tool, enterprise, teams, calendar, planner'));
        echo $this->Html->meta(array('name' => 'description', 'content' => 'Gridle is a complete cloud visual collaboration and communication tool for your time, task and project management for enterprises and teams.'));
        //robot meta tag
        echo $this->Html->meta(array('name' => 'robots', 'content' => 'index, follow'));
        //facebook open graph meta tags
        echo $this->Html->meta(array('property' => 'og:title', 'content' => 'gridle | visual cloud collaboration tool for enterprises and teams'));
        echo $this->Html->meta(array('property' => 'og:description', 'content' => 'Gridle is a complete cloud visual collaboration and communication tool for your time, task and project management for enterprises and teams.'));
        echo $this->Html->meta(array('property' => 'og:type', 'content' => 'website'));
        echo $this->Html->meta(array('property' => 'og:image', 'content' => 'http://gridle.io/img/logo/gridle_logo.png'));
        echo $this->Html->meta(array('property' => 'og:image:url', 'content' => 'http://gridle.io/img/logo/gridle_logo.png'));
        echo $this->Html->meta(array('property' => 'og:image:width', 'content' => '150'));
        echo $this->Html->meta(array('property' => 'og:image:height', 'content' => '50'));
        echo $this->Html->meta(array('property' => 'og:url', 'content' => 'http://gridle.io'));
        echo $this->Html->meta(array('property' => 'og:site_name', 'content' => 'Gridle'));
        echo $this->Html->meta(array('property' => 'fb:admins', 'content' => 'anupama.panchal'));
        echo $this->Html->meta(array('property' => 'fb:app_id', 'content' => '642120029214138'));
        //twitter meta tags
        echo $this->Html->meta(array('name' => 'twitter:card', 'content' => 'summary'));
        echo $this->Html->meta(array('name' => 'twitter:site', 'content' => '@gridle_io'));
        echo $this->Html->meta(array('name' => 'twitter:image:src', 'content' => 'http://gridle.io/img/logo/gridle_logo.png'));
        ?>
    </head>
    <body class="container-fluid">
        <div class="preloader row"></div>

        <div class="row">
            <?php echo $this->element('header_out'); ?>
            <?php echo $this->fetch('content'); ?>
            <?php echo $this->element('footer_out_new'); ?>
        </div>
        <script type="text/javascript" src="/js/underscore/underscore-min.js"></script>

        <?php //echo $this->element('sql_dump');    ?>

        <?php
        echo $this->Html->script('modules/feeds/feed.js');
        echo $this->Html->script('modules/feeds/angular.embedly.js');
        echo $this->Html->script('modules/comments/comment.js');
        echo $this->Html->script(JS_gridle);
        ?>

        <?php
        $message = strip_tags($this->Session->flash());

        if (!empty($message)) {
            ?>
            <script>
            $.bootstrapGrowl('<?= $message ?>', {
                ele: 'body', // which element to append to
                type: 'info', // (null, 'info', 'danger', 'success')
                offset: {from: 'top', amount: 20}, // 'top', or 'bottom'
                align: 'right', // ('left', 'right', or 'center')
                width: 250, // (integer, or 'auto')
                delay: 4000, // Time while the message will be displayed. It's not equivalent to the *demo* timeOut!
                allow_dismiss: true, // If true then will display a cross to close the popup.
                stackup_spacing: 10 // spacing between consecutively stacked growls.
            });
            </script>
        <?php } ?>
        <?php
        echo $this->fetch('scriptBottom');
        ?>
        <script>
            app.controller('emptyFormsController', ['$scope', function ($scope) {
                }]);
        </script>
        <?php
        //Analytics 
        if (Configure::read('server_mode') == 'production') {
            echo $this->element('analytics/production');
        } else if (Configure::read('server_mode') == 'development') {
            echo $this->element('analytics/development');
        }
        ?>
        <script type="text/javascript" src="/js/mention/mentio.min.js"></script>
    </body>
</html>

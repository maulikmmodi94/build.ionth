<!DOCTYPE html>
<html  ng-app="GridleApp">
    <head>
        <?php echo $this->Html->charset(); ?>
        <title>
            <?php echo $title_for_layout; ?>
        </title>
        <?php
        //meta tags
        echo $this->Html->meta(array('name' => 'keywords', 'content' => 'project management, task management, collaboration tool, enterprise, teams, calendar, planner'));
        echo $this->Html->meta(array('name' => 'description', 'content' => 'Gridle is a complete cloud visual collaboration and communication tool for your time, task and project management for enterprises and teams.'));
//robot meta tag
        echo $this->Html->meta(array('name' => 'robots', 'content' => 'index, follow'));
//facebook open graph meta tags
        echo $this->Html->meta(array('property' => 'og:title', 'content' => 'gridle | visual cloud collaboration tool for enterprises and teams'));
        echo $this->Html->meta(array('property' => 'og:description', 'content' => 'Gridle is a complete cloud visual collaboration and communication tool for your time, task and project management for enterprises and teams.'));
        echo $this->Html->meta(array('property' => 'og:type', 'content' => 'website'));
        echo $this->Html->meta(array('property' => 'og:image', 'content' => 'http://gridle.io/img/logo/gridle_logo.png'));
        echo $this->Html->meta(array('property' => 'og:image:url', 'content' => 'http://gridle.io/img/logo/gridle_logo.png'));
        echo $this->Html->meta(array('property' => 'og:image:width', 'content' => '150'));
        echo $this->Html->meta(array('property' => 'og:image:height', 'content' => '50'));
        echo $this->Html->meta(array('property' => 'og:url', 'content' => 'http://gridle.io'));
        echo $this->Html->meta(array('property' => 'og:site_name', 'content' => 'Gridle'));
        echo $this->Html->meta(array('property' => 'fb:admins', 'content' => 'anupama.panchal'));
        echo $this->Html->meta(array('property' => 'fb:app_id', 'content' => '642120029214138'));
//twitter meta tags
        echo $this->Html->meta(array('name' => 'twitter:card', 'content' => 'summary'));
        echo $this->Html->meta(array('name' => 'twitter:site', 'content' => '@gridle_io'));
        echo $this->Html->meta(array('name' => 'twitter:image:src', 'content' => 'http://gridle.io/img/logo/gridle_logo.png'));
        ?>
        <meta content="width=device-width, initial-scale=1, maximum-scale=1" name="viewport">
        <!--    Favicon -->
        <link rel="shortcut icon" href="/favicon.ico?v=2" />
        <link rel="stylesheet" href="/css/vendors/angular-growl.css" />
        <link rel="stylesheet" href="/css/gridle_out.css" />
        <script type="text/javascript" src="/scripts/vendor.js"></script>
        <script type="text/javascript" src="/scripts/Vendor/min/angular-growl.js"></script>
        <script type="text/javascript" src="/scripts/gridle_out_max.js"></script>
        <script>

            window.paceOptions = {
                //target: '.preloader',
                ajax: {
                    ignoreURLs: ['google-analytics', 'mouseflow', 'inspectlet', 'pollings', 'maps', 'amazonaws', 'cloudfront', 'userjoy', 'fonts']
                },
                restartOnRequestAfter: false

            };
        </script>
        <?php
        echo $this->Html->css('styles.css');

        echo $this->fetch('meta');
        echo $this->fetch('css');

        echo $this->fetch('script');
        ?>

    </head>
    <body class="container-fluid" ng-controller="emptyFormsController">
        <div growl></div>
        <?php echo $this->element('header_out_new'); ?>
        <div class="preloader row"></div>

        <div class="row">

            <?php
            echo $this->fetch('content');
            echo $this->element('footer_out_new');
            ?>
        </div>
        <?php //echo $this->element('sql_dump');     ?>

        <?php
        $message = strip_tags($this->Session->flash());

        if (!empty($message)) {
            ?>
            <div ng-init="growlNotify('<?= $message ?>')"></div>
        <?php } ?>
        <?php
        echo $this->fetch('scriptBottom');
        ?>
        <?php
        //Analytics 
        if (Configure::read('server_mode') == 'production') {
            echo $this->element('analytics/production');
        } else if (Configure::read('server_mode') == 'development') {
            echo $this->element('analytics/development');
        }
        ?>
        <style>
            #authMessage{
                font-size:10px;
                color : #EB616E;
                margin-bottom:10px;
                margin-top:5px;
                font-family: 'mediumFont';
            }
            .form-error{
                font-size: 10px;
                color: #EB616E;
                font-family: 'mediumFont';
            }
        </style>
    </body>
</html>

<!DOCTYPE html>
<html  ng-app="GridleApp">
    <head>
        <?php echo $this->Html->charset(); ?>
        <title>
            <?php echo $title_for_layout; ?>
        </title>
        <!--    Favicon -->
        <link rel="shortcut icon" href="/favicon.ico?v=2" />
        <?php
        echo $this->Html->css('styles.css');
        echo $this->Html->script('licensed');
       
        echo $this->fetch('meta');
        echo $this->fetch('css');
        echo $this->fetch('script');
        ?>
        <style>
            .container-main{
                padding:100px;
            }
        </style>
    </head>
    <body class="container-fluid">
        <div class="row">
            <?php echo $this->element('header_out'); ?>
            <div class="container-main">
                <?php echo $this->fetch('content'); ?>
            </div>
            <?php echo $this->element('footer_out'); ?>
        </div>
        <?php echo $this->element('sql_dump'); ?>

        <?php
        echo $this->Html->script('gridle_180814');
        ?>

        <?php
        $message = strip_tags($this->Session->flash());

        if (!empty($message)) {
            ?>
            <script>
                $.bootstrapGrowl('<?= $message ?>', {
                    ele: 'body', // which element to append to
                    type: 'info', // (null, 'info', 'danger', 'success')
                    offset: {from: 'top', amount: 20}, // 'top', or 'bottom'
                    align: 'right', // ('left', 'right', or 'center')
                    width: 250, // (integer, or 'auto')
                    delay: 4000, // Time while the message will be displayed. It's not equivalent to the *demo* timeOut!
                    allow_dismiss: true, // If true then will display a cross to close the popup.
                    stackup_spacing: 10 // spacing between consecutively stacked growls.
                });
            </script>
        <?php } ?>
        <?php
        echo $this->fetch('scriptBottom');
//        <!--Analytics tool-->
       //echo $this->element('analytics');
        ?>
        <!---------analytics end--------------->

    </body>
</html>

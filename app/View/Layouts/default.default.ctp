<!DOCTYPE html>
<html  ng-app="GridleApp">
    <head>
        <?php echo $this->Html->charset(); ?>
        <title>
        </title>
        <?php
        echo $this->Html->meta('icon');

        echo $this->Html->script('licensed');
        echo $this->Html->script('gridle_180814');

        echo $this->Html->css('licensed');
        echo $this->Html->css('style');


        echo $this->fetch('meta');
        echo $this->fetch('css');
        echo $this->fetch('script');
        ?>
        <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
        <style>
            body {
                padding-top: 70px;
            }
            .navbar-default {
                background-color:rgba(255,255,255,0.7);
                border-color: #e7e7e7;
            }

            .navbar-brand
            {
                font-family: Ubuntu;
                font-weight: 700;
                font-size:1.6em;
            }

            body
            {
                font-family: Ubuntu;
            }

            .dropdown-menu
            {
                font-size: 14px;
                background-color: #fff;
                border: 1px solid #EDEDED;
                /* border: 1px solid rgba(0,0,0,.15); */
                border-radius: 4px;
                -webkit-box-shadow: 0 2px 1px rgba(0,0,0,.175);
                /* box-shadow: 0 6px 12px rgba(0,0,0,.175); */
                background-clip: padding-box;
            }
        </style>
    </head>
    <body>
        <!-- Fixed navbar -->
        <div class="navbar navbar-default navbar-fixed-top" role="navigation">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">GRIDLE</a>
                </div>
                <div class="navbar-collapse collapse">
                    <ul class="nav navbar-nav">
                        <li><a href="/desks">Desk</a></li>
                        <li><a href="#about">Message</a></li>
                        <li><a href="/pages/calendar">Task</a></li>
                        <li><a href="/docs">Files</a></li>
                        <li><a href="/logs">History</a></li>

                        <li><a notificationDropDown href="/notifications">Notifications<span class="badge notification-badge"></span></a></li>


                    </ul>
                    <ul class="nav navbar-nav navbar-right">
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><?= @$Networks[$CurrentNetwork]['name']; ?><span class="caret"></span></a>
                            <ul class="dropdown-menu" role="menu">
                                <?php foreach ($Networks as $key => $value) { ?>
                                    <li>
                                        <a href="/networks/switchNetwork/<?= $value['url']; ?>"><?= $value['name']; ?></a>  

                                    </li>
                                <?php } ?>
                                <li class="divider"></li>
                                <li><a href="/networks/view">View Members</a></li>
                                <li><a href="/networks">See All Networks</a></li>
                            </ul>
                        </li>

                        <li class="dropdown">

                            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><?= $AuthUser['UserInfo']['name']; ?> <span class="caret"></span></a>

                            <ul class="dropdown-menu" role="menu">
                                <li><a href="/users/edit">Edit Profile</a></li>
                                <li><a href="/users/change_password">Change Password</a></li>
                                <li><a href="/logout">Logout</a></li>
                            </ul>

                        </li>
                    </ul>
                </div><!--/.nav-collapse -->
            </div>
        </div>

        <div class="container">
            <?php echo $this->fetch('content'); ?>
            <div id="footer">
            </div>
        </div>
        <?php echo $this->element('sql_dump'); ?>
    </body>
    <?php
    $message = strip_tags($this->Session->flash());
    if (!empty($message)) {
        ?>
        <script>
            $.bootstrapGrowl('<?= $message ?>', {
                ele: 'body', // which element to append to
                type: 'info', // (null, 'info', 'danger', 'success')
                offset: {from: 'top', amount: 20}, // 'top', or 'bottom'
                align: 'right', // ('left', 'right', or 'center')
                width: 250, // (integer, or 'auto')
                delay: 4000, // Time while the message will be displayed. It's not equivalent to the *demo* timeOut!
                allow_dismiss: true, // If true then will display a cross to close the popup.
                stackup_spacing: 10 // spacing between consecutively stacked growls.
            });
        </script>
    <?php } ?>
    <script>
        $.get("/datas/getNotificationCount", function(data) {
            data = JSON.parse(data);
            if (data.status === 'success') {
                $(".notification-badge").html(data.data);
            }
        });
    </script>
</html>

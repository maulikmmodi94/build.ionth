<?php

$this->layout = 'gridle_in';
$this->set('title_for_layout', 'Conversations');
?>
<div class="container-fluid">
    <div id="g-chat-wrap" class="row" ng-controller='ChatController'>    

        <div id="g-chat-main" class="col-lg-6 col-lg-offset-2 col-md-7 col-md-offset-2 col-sm-9">
            <div class="container-fluid">
                <!--        conversation    options         -->
                <div class="g-chat-options row" ng-if="conversations">
                    <button id="conversation_button" class='g-btn-new-msg btn btn-success btn-sm pull-left' type="button" ng-click="convModalCreate()"><i class="fa fa-plus"></i> New Conversation</button>
                    <!--if creator of conversation-->
                    <!--Doubt: we just need button here no divs are required  -->
                    <div class="pull-right"  ng-if="convSelectedItem['Conversation']['mapping'] === '<?php echo Conversation_mapping_multiple; ?>'">
                        <button ng-if="convSelectedItem['Conversation']['user_id'] === loggeduser" class='g-btn-manage-chat btn btn-default btn-sm' type="button" ng-click="convModalManage()"><i class="fa fa-gear"></i>&nbsp;&nbsp;Manage</button>
                        <button ng-if="convSelectedItem['Conversation']['user_id'] !== loggeduser" class='g-btn-manage-chat btn btn-default btn-sm' ng-click="convModalManage()">View People</button>
                    </div>
                </div>
                
                <div class="g-chat-full-area row">
                    <!--    conversations area         -->
                    <div id='' class="g-chat-messages-wrap g-scrollbar " ng-if="convSelectedItem">
                        <!--    laod more button        -->
                        <div class="text-center" ng-if="!convSelectedItem['Message']['nomoremessage'] != false && convSelectedItem['Message'].length >= Messagestoload">                        
                            <button id="loadoldermessage" class="g-btn-load-more btn btn-default btn-sm" ng-click="convMessagesLoad(convSelectedItem['Conversation']['id'])" data-loading-text="Loading...">Load Older</button>
                        </div>
                        <!--    messages       -->
                        <div class="g-chat-message-area" ng-repeat="message in convSelectedItem['Message']| orderBy:'id'" convOnFinishRender conv-on-finish-render>
                            <div class="g-chat-msg-box g-msg-left-align" ng-class="(message.user_id == loggeduser ? 'pull-right':'')">
                                <p class="g-chat-msg-user" >
                                <g-profile-img type='user' myclass='g-chat-msg-user-img' path="{{users[findUser(message['user_id'])]['UserInfo']['vga']}}" name="{{users[findUser(message['user_id'])]['UserInfo']['name']}}" width="25px" height="25px" ></g-profile-img>
                                <span class="g-chat-msg-user-name g-color-txt-lightgrey" ng-bind="users[findUser(message['user_id'])]['UserInfo']['name'] | uppercase"></span>
                                </p>
                                <p class="g-chat-msg-content" ng-bind-html="message.body | msgSanitize"></p>
                                <p class="g-chat-msg-time time-ago g-color-txt-lightgrey" title="{{message.created}}"></p>
                            </div>
                        </div>
                    </div>                    
                    <!--      reply box       -->
                    <div class="g-chat-reply-area row" ng-if="convSelectedItem !== null">
                        <form class="g-form-chat-reply col-lg-7 col-lg-offset-2 col-md-7 col-md-offset-2 col-sm-9" name="convFormReply" ng-submit="convReply(convSelectedItem['Conversation']['id'], data.convMsgBody)">
                            <textarea class="g-textarea-msg form-control" rows="3" placeholder="Reply to the message" id="msgreply" name="msgreply"  conv-ng-enter="convReply(convSelectedItem['Conversation']['id'], data.convMsgBody)" maxlength="1000" required ng-model="data.convMsgBody"></textarea>
                            <!--<div class="pull-right">-->
                            <button class="g-btn-chat-reply btn btn-success btn-sm pull-right" data-loading-text="Sending..." type="submit">Send message</button>
                            <!--</div>-->
                            <div class="checkbox pull-left">
                                <label>
                                    <input ng-model="convReplyWithEnter" type="checkbox" ng-init="convReplyWithEnter = true"> Enter to send
                                </label>
                            </div>
                        </form>
                    </div>
                </div>
                <div class='container-fluid' ng-if="!conversations">
                    <div class='row center-block'>
                        <object data='/img/empty-templates/chatwindow_empty.svg'></object>
                    </div>
                </div>
            </div>
        </div>

        <!-- Conversations list -->
        <div id='' class="g-chat-conversations-list col-lg-3 col-lg-offset-1 col-md-3 col-sm-3" >
            <div class=' container-fluid'>
                <!--        Search area         -->
                <div id='' class='g-chat-search-area row'>
                    <input class="form-control g-search-box input-sm" type="text" placeholder="Search People or Group" ng-model='convSearchText'>
                </div>
                <!--        conversations       -->
                <div class="g-chat-conversations g-scrollbar row">
                    <div class='container-fluid' ng-if="!conversations">
                    <div class='row center-block'>
                        <object data='/img/empty-templates/chat_user_empty.svg'></object>
                    </div>
                </div>
                    <div class="g-chat-conversation-tile" ng-repeat='conversation in conversations| convSearch:convSearchText' ng-click="convSelectItem(conversation.Conversation.id)" ng-class="{'g-background-yellow': conversation.Conversation.id == convSelectedItem['Conversation']['id'], 'g-background-offwhite': convUnreadClass(conversation.UserConversation,loggeduser) }">
                        <!-- multiple user conversation tiles representation -->                
                        <div class="multiuserimage" ng-if="conversation.Conversation.mapping == '<?php echo Conversation_mapping_multiple; ?>'" >        
                            <div ng-switch on="{{conversation.UserConversation.length}}">
                                <!--if single user in conversation-->
                                <div ng-switch-when="1" class="multiuserimage">
                                    <g-profile-img type="user" myclass="multiuserimg pull-left" path="{{users[findUser(conversation.UserConversation[0].user_id)]['UserInfo']['vga']}}" name="{{users[findUser(conversation.UserConversation[0].user_id)]['UserInfo']['name']}}" height="20px" width="20px"></g-profile-img>
                                    <div class="multiuserimg pull-left color-swatch brand-info"></div>
                                    <div class="multiuserimg pull-left color-swatch brand-warning"></div>
                                    <div class="multiuserimg pull-left color-swatch brand-info"></div>
                                </div>
                                <!--if two users in conversation-->
                                <div ng-switch-when="2" class="multiuserimage">    
                                    <g-profile-img type="user" myclass="multiuserimg pull-left" path="{{users[findUser(conversation.UserConversation[0].user_id)]['UserInfo']['vga']}}" name="{{users[findUser(conversation.UserConversation[0].user_id)]['UserInfo']['name']}}" height="20px" width="20px"></g-profile-img>
                                    <div class="multiuserimg pull-left color-swatch g-background-yellow"></div>                            
                                    <div class="multiuserimg pull-left color-swatch g-background-offwhite"></div>
                                    <g-profile-img type="user" myclass="multiuserimg pull-left" path="{{users[findUser(conversation.UserConversation[1].user_id)]['UserInfo']['vga']}}" name="{{users[findUser(conversation.UserConversation[1].user_id)]['UserInfo']['name']}}" height="20px" width="20px"></g-profile-img>
                                </div>
                                <!--if three users in conversation-->
                                <div ng-switch-when="3" class="multiuserimage">
                                    <g-profile-img type="user" myclass="multiuserimg pull-left" path="{{users[findUser(conversation.UserConversation[0].user_id)]['UserInfo']['vga']}}" name="{{users[findUser(conversation.UserConversation[0].user_id)]['UserInfo']['name']}}" height="20px" width="20px"></g-profile-img>
                                    <g-profile-img type="user" myclass="multiuserimg pull-left" path="{{users[findUser(conversation.UserConversation[1].user_id)]['UserInfo']['vga']}}" name="{{users[findUser(conversation.UserConversation[1].user_id)]['UserInfo']['name']}}" height="20px" width="20px"></g-profile-img>
                                    <g-profile-img type="user" myclass="multiuserimg pull-left" path="{{users[findUser(conversation.UserConversation[2].user_id)]['UserInfo']['vga']}}" name="{{users[findUser(conversation.UserConversation[2].user_id)]['UserInfo']['name']}}" height="20px" width="20px"></g-profile-img>
                                    <div class="multiuserimg pull-left color-swatch g-background-offwhite"></div>
                                </div>
                                <!--if four users in conversation-->
                                <div ng-switch-default class="multiuserimage">
                                    <g-profile-img type="user" myclass="multiuserimg pull-left" path="{{users[findUser(conversation.UserConversation[0].user_id)]['UserInfo']['vga']}}" name="{{users[findUser(conversation.UserConversation[0].user_id)]['UserInfo']['name']}}" height="20px" width="20px"></g-profile-img>
                                    <g-profile-img type="user" myclass="multiuserimg pull-left" path="{{users[findUser(conversation.UserConversation[1].user_id)]['UserInfo']['vga']}}" name="{{users[findUser(conversation.UserConversation[1].user_id)]['UserInfo']['name']}}" height="20px" width="20px"></g-profile-img>
                                    <g-profile-img type="user" myclass="multiuserimg pull-left" path="{{users[findUser(conversation.UserConversation[2].user_id)]['UserInfo']['vga']}}" name="{{users[findUser(conversation.UserConversation[2].user_id)]['UserInfo']['name']}}" height="20px" width="20px"></g-profile-img>
                                    <g-profile-img type="user" myclass="multiuserimg pull-left" path="{{users[findUser(conversation.UserConversation[3].user_id)]['UserInfo']['vga']}}" name="{{users[findUser(conversation.UserConversation[3].user_id)]['UserInfo']['name']}}" height="20px" width="20px"></g-profile-img>
                                </div>
                            </div>
                        </div>
                        <!--group conversation-->
                        <div class="g-chat-tile-img-g g-img-thumb-grp groupletterimage" ng-if="conversation.Conversation.mapping == '<?php echo Conversation_mapping_group; ?>'">
                            <p class="g-img-thumb-grp g-background-blue g-color-white" ng-bind="groups[findGroup(conversation.Conversation.group_id)]['Group']['name'] | letterimage"></p>
                        </div> 
                        <!-- single user normal conversation -->
                        <div class="g-chat-tile-img-box" ng-if="conversation.Conversation.mapping == '<?php echo Conversation_mapping_single; ?>'">
                            <span ng-repeat='user in conversation.UserConversation| convRemoveCurrent:loggeduser' >
                            <!--<img class="g-chat-tile-img-single" ng-src="{{users[findUser(user.user_id)]['UserInfo']['vga']}}"  width='45' >-->
                                <g-profile-img type="user" myclass="g-chat-tile-img-single font-big-3" path="{{users[findUser(user.user_id)]['UserInfo']['vga']}}" name="{{users[findUser(user.user_id)]['UserInfo']['name']}}" width="45px" height="45px" ></g-profile-img>
                            </span>
                        </div>
                        <!--photo ends para-->

                        <div class="g-chat-tile-content">
                            <!--Name of ther person or group-->
                            <p class="g-chat-tile-user_id g-text-capitalcase" ng-if="conversation.Conversation.mapping != '<?php echo Conversation_mapping_group ?>'">
                                <span class='g-text-comma-trail'  ng-repeat='user in conversation.UserConversation| convRemoveCurrent:loggeduser' ng-bind="users[findUser(user['user_id'])]['UserInfo']['first_name']"></span>
                            </p>
                            <p class="g-chat-tile-user_id g-text-uppercase" ng-if="conversation.Conversation.mapping == '<?php echo Conversation_mapping_group ?>'" ng-bind="groups[findGroup(conversation.Conversation.group_id)]['Group']['name']"></p>
                            <!--Last message of the thread-->
                            <!-- <p class="g-chat-tile-msg lastmessage" ng-bind-html="conversation.Message.0.body | trustAsHtml"></p> -->
                            <span ng-repeat="message in conversation['Message']| orderBy:'id':true | limitTo :1">
                                <p class="g-chat-tile-msg lastmessage"  ng-bind-html="message.body | trustAsHtml"></p>
                                <p class="g-chat-tile-time g-color-txt-lightgrey time-ago" title="{{message.created}}"></p>
                            </span>                            
                        </div>                    
                    </div>
                    <!--LOAD MORE CONVERSATIONS-->
                    <div class="text-center" ng-show="convTotalPages > convPagesLoaded" >
                        <button class="g-btn-load-more btn btn-default btn-sm" ng-click="convConversationLoadOlder(convPagesLoaded + 1)">Load Older</button>
                    </div>
                </div>
                
            </div>

            <?php
            echo $this->element('Conversation/create');
            echo $this->element('Conversation/manage');
            ?>
            
        </div>

    </div>
</div>
<script>
    function openconversationmodel(evt) {
        $('#convModalCreate').modal('show');
    }
</script>
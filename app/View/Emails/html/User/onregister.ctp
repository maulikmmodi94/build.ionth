<?php
/**
 * Copyright 2010 - 2013, Cake Development Corporation (http://cakedc.com)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright 2010 - 2013, Cake Development Corporation (http://cakedc.com)
 * @license MIT License (http://www.opensource.org/licenses/mit-license.php)
 */

echo __d('users','Hello');
echo "<br/>";
echo "<br/>";

echo __d('users','<strong>%s</strong> ( email: <strong>%s</strong> ) has registered on gridle.',$data['name'],$data['email_to_notify']);
echo "<br/>";


echo '<p style="padding: 0px 0 0 0; color: #153643; font-family: Arial, sans-serif; font-size: 16px; line-height: 20px;text-align:justify"> Want to tell us something? Just hit reply to this mail or tweet to us on <a href="http://twitter.com/gridle_io">@gridle_io</a>. If you think this mail was not intended for you, please ignore.</p>';

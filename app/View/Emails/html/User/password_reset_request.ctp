<?php
/**
 * Copyright 2010 - 2013, Cake Development Corporation (http://cakedc.com)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright 2010 - 2013, Cake Development Corporation (http://cakedc.com)
 * @license MIT License (http://www.opensource.org/licenses/mit-license.php)
 */

echo __d('users', 'A request to reset your password was sent. To change your password click the link below.');
echo "<br>";

echo "<a target='_blank' style='margin:14px 0;color:#ffffff;text-decoration:none;display:inline-block;min-height:38px;line-height:39px;padding-top:0;padding-right:16px;padding-bottom:0;padding-left:16px;outline:0;background:#3eae5f;font-size:14px;text-align:center;font-style:normal;font-weight:400;border:0;vertical-align:bottom;white-space:nowrap;border-radius:0.2em' href='".$data['url']."'>Reset Password</a><script type='application/ld+json'>{'@context': 'http://schema.org','@type': 'EmailMessage','action': {'@type': 'ViewAction','url': 'https://watch-movies.com/watch?movieId=abc123','name': 'Watch movie'},'description': 'Watch the 'Avengers' movie online'}</script>";

<?php

/**
 * Copyright 2010 - 2013, Cake Development Corporation (http://cakedc.com)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright 2010 - 2013, Cake Development Corporation (http://cakedc.com)
 * @license MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
echo __d('tasks', 'Hi %s,', $data['fname']);
echo "<br/>";
echo "<br/>";

echo __d('tasks', '%s has changed details about a task. Here they are.', $data['edited_by']);

//echo __d('tasks', 'Originally, the task was <ul><li><strong>'.$data['Task']['title'].'</strong></li></ul> assigned to [Name of people it was assigned to] to be completed by [Deadline in format of 3 PM on 12th July]', $data['Task']['title']['new'], $data['edited_by']);
//echo "<br/>";
if ($data['Task']['priority']['old'] != $data['Task']['priority']['new']) {
    $i = 1;
}

echo "<br/>";
echo "<br/>";

$c = count($data['old_all_users']) - 1;
echo 'Originally, the task was <ul><li><strong>' . $data['Task']['title']['old'] . '</strong></li></ul> assigned to ';
if (!empty($data['old_all_users'])) {
    foreach ($data['old_all_users'] as $key => $value) {
        echo '<strong>' . $value . ' </strong>';
        if ($c != $key) {
            echo'<strong>, </strong>';
        }
    }
}


echo ' to be completed by ' . $data['old_enddate'];
if ($i == 1) {
    echo ' and marked as ' . $data['Task']['priority']['old'];
}
echo "<br/>";
echo "<br/>";


//echo __d('tasks', 'Now, it has been changed to <ul><li><strong>'.$data['Task']['title'].'</strong></li></ul> assigned to [Name of people it was assigned to] to be completed by [Deadline in format of 3 PM on 12th July]', $data['Task']['title']['new'], $data['edited_by']);
//echo "<br/>";
$c = count($data['all_user']) - 1;
echo 'Now, it has been changed to <ul><li><strong>' . $data['Task']['title']['new'] . '</strong></li></ul> assigned to ';
if (!empty($data['all_user'])) {
    foreach ($data['all_user'] as $key => $value) {
        echo '<strong>' . $value . '</strong>';
        if ($c != $key) {
            echo'<strong>, </strong>';
        }
    }
}
echo ' to be completed by ' . $data['new_enddate'];
if ($i == 1) {
    echo ' and marked as ' . $data['Task']['priority']['new'];
}
echo "<br/>";
echo "<br/>";




echo "<a target='_blank' style='margin:14px 0;color:#ffffff;text-decoration:none;display:inline-block;min-height:38px;line-height:39px;padding-top:0;padding-right:16px;padding-bottom:0;padding-left:16px;outline:0;background:#3eae5f;font-size:14px;text-align:center;font-style:normal;font-weight:400;border:0;vertical-align:bottom;white-space:nowrap;border-radius:0.2em' href='" . $data['url'] . "'>Take me to Build</a>";
echo "<br/>";

echo 'Also, in case you are facing any problems or have suggestions, just tweet to me <a href="https://twitter.com/ionthglobal">@bulid_ionth</a>. Contrary to what you might think, I reply blazingly fast.';
//echo 'If you think you are receiving a lot of my mails, just click on <a href = ' . $data['unsubscribe']. '> unsubscribe <a> and I won\'t bother you again. :)<br>';
//
//echo "<br/>";
//echo 'Yours,<br>Donna.';

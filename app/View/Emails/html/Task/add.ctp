<?php
/**
 * Copyright 2010 - 2013, Cake Development Corporation (http://cakedc.com)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright 2010 - 2013, Cake Development Corporation (http://cakedc.com)
 * @license MIT License (http://www.opensource.org/licenses/mit-license.php)
 */

echo __d('users','Hi %s,',$data['name']);
echo "<br/>";

echo __d('tasks','%s has shared tasks with you. Here it is.',$data['assigned_by']);
echo "<br/>";

//echo __d('tasks','<strong>%s</strong>',$data['Task']['title']);
echo "<br/>";

echo '<ul><li><strong>'.$data['Task']['title'].'</strong></li></ul>';

//echo "<table border='0' cellpadding='0' cellspacing='0' width='100%' class='mcnButtonBlock'>
//                        <tbody class='mcnButtonBlockOuter'>
//                            <tr>
//                                <td style='padding-top:18px; padding-right:18px; padding-bottom:18px; padding-left:18px;' valign='top' align='center' class='mcnButtonBlockInner'>
//                                    <table border='0' cellpadding='0' cellspacing='0' class='mcnButtonContentContainer' style='border-collapse: separate !important;border-radius: 5px;background-color: #138D90;'>
//                                        <tbody>
//                                            <tr>
//                                                <td align='center' valign='middle' class='mcnButtonContent' style='font-family: Arial; font-size: 16px; padding: 16px;'>
//                                                    <a class='mcnButton ' href='" . $data['url'] . "' target='_blank' style='font-weight: bold;letter-spacing: normal;line-height: 100%;text-align: center;text-decoration: none;color: #FFFFFF;'>Take me to Gridle</a>
//                                                </td>
//                                            </tr>
//                                        </tbody>
//                                    </table>
//                                </td>
//                            </tr>
//                        </tbody>
//                    </table>";


echo "<a target='_blank' style='margin:14px 0;color:#ffffff;text-decoration:none;display:inline-block;min-height:38px;line-height:39px;padding-top:0;padding-right:16px;padding-bottom:0;padding-left:16px;outline:0;background:#3eae5f;font-size:14px;text-align:center;font-style:normal;font-weight:400;border:0;vertical-align:bottom;white-space:nowrap;border-radius:0.2em' href='" . $data['url'] . "'>Take me to Build</a>";
echo "<br/>";

echo "Also, in case you are facing any problems or have suggestions, just tweet to me <a href='https://twitter.com/ionthglobal'>@build_ionth</a>. Contrary to what you might think, I reply blazingly fast.";
//echo 'If you think you are receiving a lot of my mails, just click on <a href = ' . $data['unsubscribe']. '> unsubscribe <a> and I won\'t bother you again. :)<br>';
//
//
//echo "<br/>";
//echo __d('tasks', 'Yours,<br>Donna.');


<?php
/**
 * Copyright 2010 - 2013, Cake Development Corporation (http://cakedc.com)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright 2010 - 2013, Cake Development Corporation (http://cakedc.com)
 * @license MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
echo __d('users', 'Hi,');
echo "<br/>";
echo "<br/>";
 
echo __d('networks', 'The user <strong>%s</strong> , the owner of Network <strong>%s</strong> wants to upgrade the existing network plan. ', $data['owner_name'],$data['network_name']);

echo "<br><br>";

echo __d('networks', 'User details are as follows: <ul><li>User Id : <strong>%s</strong></li><li>Name : <strong>%s</strong></li><li>Email : <strong>%s</strong></li><li>Network Id : <strong>%s</strong></li><li>Network Name : <strong>%s</strong></li></ul>  ', $data['owner_id'],$data['owner_name'],$data['owner_email'],$data['network_id'],$data['network_name']);

echo __('Reply back to the person as early as possible! ');

echo "<br><br>";

echo __('Also note that the terms <strong>Network</strong> and <strong>Workspace</strong> will be used interchangably.');

echo "<br><br>";



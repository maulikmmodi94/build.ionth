<?php $this->layout = 'gridle_in' ?>
<!--        MODAL WINDOW FOR NEW GROUP          -->
<div id='groupModalCreate' class="modal fade">
    <div class="g-modal modal-dialog" >
        <!--        CONTENT          -->
        <div class="g-modal-content modal-content">
            <!--        HEADER          -->
            <div class="g-modal-header modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="g-modal-title modal-title ">
                    <span class="g-modal-header-icon fa fa-group"></span> 
                    <span class="g-modal-header-title-text">Make a team</span>
                </h4>
            </div>
            <!--        BODY          -->
            <div class="g-modal-body modal-body">
            </div>
        </div>
    </div>
</div>
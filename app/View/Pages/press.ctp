<?php
$this->layout = 'gridle_out_new';
$this->set('title_for_layout', 'In the News | gridle ');
$brand = Configure::read('brand');
?>
<link rel="stylesheet" type="text/css" href="/css/terms.css">
<div class="position-relative">
	<div class="container-fluid terms-header">
		<div class="row">
				<div class= "col-sm-offset-2 col-sm-10">				
					<h1 class="terms-heading">Press</h1>
					<p class="terms-sub-heading">Gridle press features. Here are a few notable ones.</p>
				</div>
		</div>
	</div>
<div class="container-fluid background-white" >
	<div class="row" >
	<div class="col-sm-offset-2 col-sm-7">
	<div class="terms-content-use margin-tb-fifty">
		
         <!-- Economic Times     -->
        <div id="EconomicTimes">
            
            <div id="" class="article_style row">
                <div class="news_logo_div col-lg-4">
                    <p><img class="news_logo_img" src="/img/media/economic_times.png" alt='gridle on Economic Times' /></p>
                </div>
                <div class="news_article col-lg-8 ">
                    <h2 class="news_article_heading">Proverbial stepping stones</h2>
                    <p class="news_article_subheading">
                        <span class="news_article_date">July 26th 2015, </span> 
                        <span class="news_article_author">by Rajiv Singh</span>
                    </p>
                    <p class="news_article_content">With Gridle, companies and teams can sign up, invite colleagues and start managing their tasks, interacting with instant messangers, upload files, plan with a scheduler, view analytics and integrate tools like Skype and Dropbox.<a class="news_article_link" target="_blank" href="http://economictimes.indiatimes.com/small-biz/startups/ten-startup-artistes-who-are-using-their-failed-ventures-as-proverbial-stepping-stones/articleshow/48218102.cms?intenttarget=no">Continue reading.</a></p>
                </div>
            </div>       
        <hr class="terms-hr"/>
        </div>
		<!--	Business Standard 	-->
		<div id="BusinessStandard">
			<h1 class="terms-content-heading"></h1>
			<div id="" class="article_style row">
                <div class="news_logo_div col-lg-4">
                    <p><img class="news_logo_img" src="/img/media/business_standard.png" alt='gridle on business standard' /></p>
                </div>
                <div class="news_article col-lg-8 ">
                    <h2 class="news_article_heading">We plan to be on multiple platforms via native apps</h2>
                    <p class="news_article_subheading">
                        <span class="news_article_date">March 24th 2014, </span> 
                        <span class="news_article_author">by Vinay Umarji</span>
                    </p>
                    <p class="news_article_content">Ahmedabad-based start-up Gridle is finding acceptance for its cost-effective collaborative platform. Gridle plans to expand across verticals and geographies. In an interview with Business Standard, Gridle co-founders talk about the journey so far. <a class="news_article_link" target="_blank" href="http://www.business-standard.com/article/companies/we-plan-to-be-on-multiple-platforms-via-native-apps-anupama-panchal-114032400007_1.html">Continue reading.</a></p>
                </div>
            </div>
		<hr class="terms-hr"/>
		</div>
		<!--	The Times of India 	-->
		<div id="TimesofIndia">
			<h1 class="terms-content-heading"></h1>
			<div id="" class="article_style_alt row">
                <div class="news_logo_div col-lg-4">
                    <p><img class="news_logo_img" src="/img/media/timesofindia.png" alt='gridle on The Times of India' /></p>
                </div>
                <div class="news_article col-lg-8 ">
                    <h2 class="news_article_heading">Move over mail, IIM-A startup cuts communication load</h2>
                    <p class="news_article_subheading">
                        <span class="news_article_date">Feb 19th 2014, </span> 
                        <span class="news_article_author">by Chittra Unnithan</span>
                    </p>
                    <p class="news_article_content">Companies are looking at ways to improve productivity by saving time and maintain a high-performing staff. Gridle enables teams to coordinate well and work better together. 
                        <a class="news_article_link" target="_blank" href="http://epaper.timesofindia.com/Default/Scripting/ArticleWin.asp?From=Archive&Source=Page&Skin=TOINEW&BaseHref=TOIA%2F2014%2F02%2F19&PageLabel=3&EntityId=Ar00301&ViewMode=HTML">Continue reading.</a></p>
                </div>
            </div>
			<hr class="terms-hr"/>
		</div>
		<!--	Next Big What 	-->
		<div id="NextBigWhat">
			<h1 class="terms-content-heading"></h1>
			<div id="" class="article_style row">
                <div class="news_logo_div col-lg-4">
                    <p><img class="news_logo_img" src="/img/media/next_big_what.png" alt='gridle on NEXT BIG WHAT' /></p>
                </div>
                <div class="news_article col-lg-8 ">
                    <h2 class="news_article_heading">What Makes Gridle A One Stop Shop for All Communication Needs</h2>
                    <p class="news_article_subheading">
                        <span class="news_article_date">June 17th, 2014 </span> 
                        <span class="news_article_author">by Sanket Sheth</span>
                    </p>
                    <p class="news_article_content">Gridle comes handy to monitor, track and work better and it is happen more likely to the growing startups. What makes Gridle stand apart from the hoards of such products in the market is its pricing structure, cloud based services and its ability to integrate with all popular tools used in the market. 
                        <a class="news_article_link" target="_blank" href="http://www.nextbigwhat.com/makes-gridle-one-stop-shop-communication-needs-297/">Continue reading.</a></p>
                </div>
            </div>
			<hr class="terms-hr"/>
		</div>
		<!--        Tech Circle Blog      -->
		<div id="TechCircleBlog">
			<h1 class="terms-content-heading"></h1>
			<div id="" class="article_style row">
                <div class="news_logo_div col-lg-4">
                    <p><img class="news_logo_img" src="/img/media/tech_circle.jpeg" alt='gridle on Tech circle' /></p>
                </div>
                <div class="news_article col-lg-8 ">
                    <h2 class="news_article_heading">Excl: Cloud-based visual collaboration startup Gridle </h2>
                    <p class="news_article_subheading">
                        <span class="news_article_date">Nov 5 2013, </span> 
                        <span class="news_article_author">by Sainul Abudeen</span>
                    </p>
                    <p class="news_article_content">The platform can be used to collaborate on projects in teams, share files, assign tasks, chat and share in-house expertise. Additional features include inbox, planner, member dictionary and activity. 
                        <a class="news_article_link" target="_blank" href="http://techcircle.vccircle.com/2013/11/05/excl-cloud-based-visual-collaboration-platform-gridle-raises-funding-from-ian-others">Continue reading.</a></p>
                </div>
            </div>
			<hr class="terms-hr"/>
		</div>
		<!--        Business Standard      -->
		<div id="BusinessStandard-2013">
			<h1 class="terms-content-heading"></h1>
			<div id="" class="article_style_alt news_article_div row">
                <div class="news_logo_div col-lg-4">
                    <p><img class="news_logo_img" src="/img/media/business_standard.png" alt='gridle on business standard'/></p>
                </div>
                <div class="news_article col-lg-8 ">
                    <h2 class="news_article_heading">Collaboration made easy by gridle platform</h2>
                    <p class="news_article_subheading">
                        <span class="news_article_date">Dec 23th 2013, </span> 
                        <span class="news_article_author">by Vinay Umarji</span>
                    </p>
                    <p class="news_article_content">Gridle provides important tools for organisations to optimise workforce efficiency and eases project management. Its ability to self-manage and sync-team is of value, while data-visualisation in collaboration is unique.
                        <a class="news_article_link" target="_blank" href="http://www.business-standard.com/article/companies/collaboration-made-easy-by-gridle-platform-113122300056_1.html">Continue reading.</a></p>
                </div>
            </div>
			<hr class="terms-hr"/>
		</div>
		<!--        Your Story      -->
		<div id="YourStory">
			<h1 class="terms-content-heading"></h1>
			<div id="" class="article_style news_article_div row">
                <div class="news_logo_div col-lg-4">
                    <p><img class="news_logo_img" src="/img/media/yourstory.png" alt='gridle on yourstory' /></p>
                </div>
                <div class="news_article col-lg-8 ">
                    <h2 class="news_article_heading">Gridle, an affordable and easy collaborative tool targeted at SMBs</h2>
                    <p class="news_article_subheading">
                        <span class="news_article_date">June 4th 2013, </span> 
                        <span class="news_article_author">by Ganesh Nagarsekar</span>
                    </p>
                    <p class="news_article_content">With a sleek and clean UI, a secure cloud based service, and prices that are easy on the pocket, Gridle comes to the rescue of SMBs. Their services are charged at around 20% of what their US based competitors charge, making them a delight for medium sized enterprises. 
                        <a class="news_article_link" target="_blank" href="http://yourstory.com/2013/06/gridle-an-affordable-and-easy-collaborative-tool-targeted-at-smbs/">Continue reading.</a></p>
                </div>
            </div>
			<hr class="terms-hr"/>
		</div>
		<!--        Startup Central      -->
		<div id="StartupCentral">
			<h1 class="terms-content-heading"></h1>
			<div id="" class="article_style_alt news_article_div row">
                <div class="news_logo_div col-lg-4">
                    <p><img class="news_logo_img" src="/img/media/startup_central.png" alt='gridle on Startup central' /></p>
                </div>
                <div class="news_article col-lg-8 ">
                    <h2 class="news_article_heading">Cloud collaboration tool Gridle launches in public beta</h2>
                    <p class="news_article_subheading">
                        <span class="news_article_date">Feb 17th 2014, </span> 
                        <span class="news_article_author">by Snigdha Sendupta</span>
                    </p>
                    <p class="news_article_content">The company has ambitious plans for its future growth roadmap, even as it scales its product. Apart from rolling out Gridle in India, it plans to target overseas markets such as Southeast Asia and Australia. Put this company on your watch list of promising young technology startups out of India.
                        <a class="news_article_link" target="_blank" href="http://startupcentral.in/2014/02/gridle-seed-round-ciie-hiraco-ventures-16837/">Continue reading.</a></p>
                </div>
            </div>
			<hr class="terms-hr"/>
		</div>
		<!--        Tech Panda      -->
		<div id="TechPanda">
			<h1 class="terms-content-heading"></h1>
			<div id="" class="article_style news_article_div row">
                <div class="news_logo_div col-lg-4">
                    <p><img class="news_logo_img" src="/img/media/tech_panda.png" alt='gridle on tech panda' /></p>
                </div>
                <div class="news_article col-lg-8 ">
                    <h2 class="news_article_heading">Gridle – Visually Addictive and Pocket Friendly Cloud Collaboration tool for SMB</h2>
                    <p class="news_article_subheading">
                        <span class="news_article_date">Sept 25th 2013, </span> 
                        <span class="news_article_author">by Saraswathi Pulluru</span>
                    </p>
                    <p class="news_article_content">Gridle definitely fills in a gap in the existing SMB Collaboration Market, by offering a visually appealing tool. 
                        <a class="news_article_link" target="_blank" href="http://thetechpanda.com/2013/09/25/gridle-visually-addictive-pocket-friendly-collaborate-tool-smb/#.U3HQj4GSy_R">Continue reading.</a></p>
                </div>
            </div>
			<hr class="terms-hr"/>
		</div>
		<!--        Startup Central      -->
		<div id="StartupCentral-2013">
			<h1 class="terms-content-heading"></h1>
			<div id="" class="article_style_alt news_article_div row">
                <div class="news_logo_div col-lg-4">
                    <p><img class="news_logo_img" src="/img/media/startup_central.png" alt='gridle on Startup Central' /></p>
                </div>
                <div class="news_article col-lg-8 ">
                    <h2 class="news_article_heading">Ahmedabad startup Gridle in iAccelerator 2013 batch</h2>
                    <p class="news_article_subheading">
                        <span class="news_article_date">Aug 30th 2013, </span> 
                        <span class="news_article_author">by Snigdha Sendupta</span>
                    </p>
                    <p class="news_article_content">Gridle can be used to collaborate on projects in teams of 15-20, share files, assign tasks, share in-house expertise, train new recruits and more. The platform’s interface aims to be high on visual appeal.
                        <a class="news_article_link" target="_blank" href="http://startupcentral.in/2013/08/ahmedabad-startup-gridle-in-iaccelerator-2013-batch-31955/">Continue reading.</a></p>
                </div>
            </div>
			<hr class="terms-hr"/>
		</div>
		
	</div>
</div>
<div id="nav-list" class="col-sm-3 ">
	<div class="terms-autoscroll">
		<ul class="nav nav-pills nav-stacked" role="tablist">
            <li><a href="#EconomicTimes" class="">The Economic Times</a></li>
			<li><a href="#BusinessStandard" class="">Business Standard 2014</a></li>
			<li><a href="#TimesofIndia" class="">The Times of India</a></li>
			<li><a href="#NextBigWhat" class="">Next Big What</a></li>
			<li><a href="#TechCircleBlog" class="">Tech Circle Blog</a></li>
			<li><a href="#BusinessStandard-2013" class="">Business Standard 2013</a></li>
			<li><a href="#YourStory" class="">Your Story</a></li>
			<li><a href="#StartupCentral" class="">Startup Central 2014</a></li>
			<li><a href="#TechPanda" class="">Tech Panda</a></li>
			<li><a href="#StartupCentral" class="">Startup Central 2013</a></li>
			
		</ul>
	</div>
</div>
</div>
</div>
</div>
<?php
$this->layout = 'gridle_out';
$brand = Configure::read('brand');

$this->set('title_for_layout', __(' Build | cloud based visual collaboration tool | enterprises | teams ', $brand));
?>
<div class="g-page-wrap container-fluid" style="padding-bottom:0">
    <!--        section one (Main with the office/world svg)        -->
    <div id="section1" class="row">
        <p class="section_main_image col-xs-10 col-xs-offset-1" id="homesvgparent">
            <!--            <object class="center-block" data="/img/homepage/gridle_desk_view.svg" style="width: 100%;">
                            <img src="/img/homepage/gridle_desk_view.jpg" />
                        </object>-->
            <img src="/img/ionth/landing_img.jpg" width="100%" />
        </p>
        <div class="clearfix"></div>
        <h1 class="section1_text"><?php echo $brand ?> offers effortless real-time collaboration for your teams & enterprises.</h1>
        <div id="section1_bottom">
            <p><a class="btn btn-lg btn-primary btn-responsive homepage_btns text-semi-bold" href="/users/add" >Get started for free</a></p>
            <p><a href="/pages/policy" target='_blank'>Build's terms and conditions</a></p>
        </div>
    </div>
</div>
<div class="g-page-wrap container-fluid">
    <div class="row">
        <center>
            <iframe width="560" height="315" src="https://www.youtube.com/embed/tlpYIO0eEOw" frameborder="0" allowfullscreen></iframe>
        </center>

    </div>
</div>

<!--<div class="g-page-wrap container-fluid">
    <div class="row">
        <p class="font-big-6">Features</p>
        <div class="col-sm-3"></div>
        <div class="col-sm-3"></div>
        <div class="col-sm-3"></div>
        <div class="col-sm-3"></div>
    </div>
</div>-->
<style>

    .feature-box > div{
    }
    .feature-box > div > div > div{
        margin-top: 20px;
        margin-bottom: 20px;
        background: #F8F5EC;
        padding: 30px 10px;
        border-radius: 3px;
    }
    .feature-box{
        padding:10px 10%;
        margin-top: 100px;
    }

    .screenshots{

    }
    .screenshots-img{

    }
    .screenshots-head{
        margin-top: 10px;
        font-family: 'Roboto', 'sans-serif';
        color: black;
        padding: 0px 10px;
    }
    .screenshots-desc{

    }
    .feature-box{
        padding:10px 10%;
    }

    .features-head{
        /*font-family: 'Roboto', 'sans-serif';*/
        color: black;
    }
    .feature-description{

    }


</style>
<div class="g-page-wrap container-fluid">
    <div class="row text-center">
        <h1 class="section_heading" style="margin-bottom:100px; ">Features and Benefits</h1>
        <div class="col-sm-4 screenshots">
            <img class="screenshots-img" src="/img/ionth/screenshot_task.png" width="100%" />
            <p class="font-big-2 screenshots-head">Create new tasks instantly and have discussions on it with your team </p>
            <p class="screenshots-desc"></p>
        </div>

        <div class="col-sm-4 screenshots">
            <img class="screenshots-img" src="/img/ionth/screenshot_desk.png" width="100%" />
            <p class="font-big-2 screenshots-head">Share tasks, messages and files with your team-mates at one place</p>
            <p class="screenshots-desc"></p>
        </div>
        <div class="col-sm-4 screenshots">
            <img class="screenshots-img" src="/img/ionth/screenshot_files.png" width="100%" />
            <p class="font-big-2 screenshots-head">Share files and have contextual discussions</p>
            <p class="screenshots-desc"></p>
        </div>
    </div>
    <div class="row text-center feature-box">
        <p class="font-big-3" style="margin-bottom:100px;">Build takes care of all your work needs.</p>
        <div class="row feature-separation">
            <div class="col-sm-3">
                <div>
                    <p ><i class="fa fa-check-circle text-primary fa-5x"></i></p>
                    <p class="features-head font-big-2 text-emphasize text-muted">Task Management</p>
                    <p class="feature-description text-muted">Be focused on what you have to do and when. All with a visual interface.</p>
                </div>
            </div>
            <div class="col-sm-3">
                <div>
                    <p>
                        <i class="fa fa-cloud fa-5x text-primary"></i>
                    </p>
                    <p class="features-head font-big-2 text-emphasize text-muted">
                        File Management
                    </p>
                    <p class="feature-description text-muted">
                        Store your documents with ease in the cloud. Accessible from any device.
                    </p>
                </div>
            </div>
            <div class="col-sm-3">
                <div>
                    <p><i class="fa fa-refresh fa-5x text-primary"></i></p>

                    <p class="features-head font-big-2 text-emphasize text-muted">
                        Feeds
                    </p>
                    <p class="feature-description text-muted">
                        Interact with your colleagues with a social feed. Share while creating.
                    </p>
                </div>
            </div>
            <div class="col-sm-3">
                <div>
                    <p><i class="fa fa-comments-o fa-5x text-primary"></i></p>
                    <p class="features-head font-big-2 text-emphasize text-muted">
                        Instant Messaging
                    </p>
                    <p class="feature-description text-muted">
                        Stay in contact. Let your collaborators communicate with you in realtime.
                    </p>
                </div>
            </div>
        </div>
        <div class="row feature-separation">
            <div class="col-sm-3">
                <div>
                    <p><i class="fa fa-dashboard fa-5x text-primary"></i></p>
                    <p class="features-head font-big-2 text-emphasize text-muted">
                        User Management
                    </p>
                    <p class="feature-description text-muted">
                        Manage your team. Scale your user base as needed instantly.
                    </p>
                </div>
            </div>
            <div class="col-sm-3">
                <div>
                    <p><i class="fa fa-history fa-5x text-primary"></i></p>
                    <p class="features-head font-big-2 text-emphasize text-muted">
                        Activity Log
                    </p>
                    <p class="feature-description text-muted">
                        Be aware of who is doing what and when. Summarised as a project history.
                    </p>
                </div>
            </div>
            <div class="col-sm-3">
                <div>
                    <p><i class="fa fa-bell fa-5x text-primary"></i></p>
                    <p class="features-head font-big-2 text-emphasize text-muted">
                        Instant Notifications
                    </p>
                    <p class="feature-description text-muted">
                        Receive notifications right when they happen. Allowing you to optimise.
                    </p>
                </div>
            </div>
            <div class="col-sm-3">
                <div>
                    <p><i class="fa fa-clock-o fa-5x text-primary"></i></p>
                    <p class="features-head font-big-2 text-emphasize text-muted">
                        Deadline reminders
                    </p>
                    <p class="feature-description text-muted">
                        Know when your tasks are due. Never miss a deadline.
                    </p>
                </div>
            </div>
        </div>
        <div class="row feature-separation">
            <div class="col-sm-3">
                <div>
                    <p><i class="fa fa-group fa-5x text-primary"></i></p>
                    <p class="features-head font-big-2 text-emphasize text-muted">
                        Teams
                    </p>
                    <p class="feature-description text-muted">
                        Grow your company with the addition of new teams. Bringing your enterprise up to speed.

                    </p>
                </div>
            </div>
            <div class="col-sm-3">
                <div>
                    <p><i class="fa fa-share-alt text-primary fa-5x"></i></p>
                    <p class="features-head font-big-2 text-emphasize text-muted">
                        Sharing
                    </p>
                    <p class="feature-description text-muted">
                        Deliver your input while collaborating. Creating solid intellectual capital.
                    </p>
                </div>
            </div>
            <div class="col-sm-3">
                <div>
                    <p><i class="fa fa-pie-chart fa-5x text-primary"></i></p>
                    <p class="features-head font-big-2 text-emphasize text-muted">
                        Analytics
                    </p>
                    <p class="feature-description text-muted">
                        Get to know how you and team are doing. Analytics about how you spend your time.
                    </p>
                </div>
            </div>
            <div class="col-sm-3">
                <div>
                    <p><i class="fa fa-video-camera fa-5x text-primary"></i></p>
                    <p class="features-head font-big-2 text-emphasize text-muted">
                         video conference
                    </p>
                    <p class="feature-description text-muted">
                        have video conferencing with your team and even outside with a single click
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="g-page-wrap container-fluid">
    <!--    pricing plan section   -->
    <div id="pricing_plan_div" class="row">
        <h1 class="section_heading">Pricing & Plan</h1>

        <div class="colored_line_pattern_home">
            <p class="pattern_yellow_sec"></p><p class="pattern_red_sec"></p><p class="pattern_green_sec"></p>
        </div>
        <div class="clearfix"></div>

        <h3 class="section_subheading">Choose a plan and connect your team</h3>

        <div id="gridle_pricing-table" class="col-sm-10 col-sm-offset-1 col-xs-12">
            <div class="row">
                <!--Plan 1--> 
                <div class="plan plan1 col-sm-4 col-xs-12">
                    <div class="header">Early Stage
                        <p class="sub_header">Build</p>
                    </div>
                    <div class="price">$0</div>  
                    <div class="monthly">/ user / month</div>      
                    <ul>
                        <!--<li><span style="text-decoration: line-through;" class="text-danger" ><b>1 GB</b></span>  &nbsp;&nbsp;<b>2 GB</b> File space</li>-->
                        <li>Up to <b>5 </b> Users</li>
                    </ul>
                    <!--<a class="signup" href="/users/invite">Get Started</a>-->         
                </div>
                <!--Plan 2--> 
                <!--Plan 3--> 
                <div class="plan plan3 popular-plan col-sm-4 col-xs-12">
                    <div class="header">Scaling<p class="sub_header">Build Paid Edition</p></div>
                    <div class="price">$3<sup>+</sup></div>
                    <div class="monthly">/ user / month</div>
                    <ul>
                        <!--<li><b>50GB</b> File space</li>-->
                        <li><b>Unlimited</b> Users</li>
                    </ul>
                    <!--<a class="signup" href="/users/invite">Get Started</a>-->        
                </div>
                <div class="plan plan2 col-sm-4 col-xs-12 ">
                    <div class="header">Community

                        <p class="sub_header">Crowd</p></div>
                    <div class="price">$0</div>
                    <div class="monthly">/ user / month</div>  
                    <ul>
                        <!--<li><b>20GB</b> File space</li>-->
                        <li><b>Free</b> for life</li>	
                    </ul>
                    <!--<a class="signup" href="/users/invite">Get Started</a>-->            
                </div>
                <!--                <div class="plan plan4 col-sm-3 col-xs-12">
                                    <div class="header">Large
                                        <p class="sub_header">For Large Enterprises</p></div>
                                    <div class="price">Contact</div>
                                    <div class="monthly">hello@ionth.com</div>
                                    <ul>
                                        <li><b>100GB</b> File space</li>
                                        <li><b>Unlimited</b> People</li>
                                        <li><b>Unlimited</b> Teams</li>
                                    </ul>
                                    <p>for more than 100 people.</p>
                                    <a class="signup" href="/users/invite">Get Started</a>        
                                </div> 	-->
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <h1 class="font-big-3" style="line-height: 1.2em; padding-bottom: 50px;">We have special plans if you have more than <span class="text-emphasize">100 users</span>. Please contact <a href="mailto:hello@ionth.com">hello@ionth.com </a> for more details.</h1>
                </div>
            </div>
        </div>
        <div id="section1_bottom" >
            <p style=''><a class="btn btn-primary btn-lg homepage_btns" href="/signup">Sign up now for free</a></p>
        </div>  
    </div>
</div>

<div class="container-fluid">
    <div class="row" style="padding-top:50px; padding-bottom: 50px;">
        <div class="col-sm-3 col-sm-offset-1" style="padding-top:50px;">
            <p class="text-center font-big-6 text-muted">Contact us</p>
        </div>
        <div class="col-sm-3 ">
            <h1>Main Office</h1>
            <p>
                <span class="text-emphasize">Ionth Global Ltd.</span><br/>
                <span>2900 – 550 Burrard Street, Vancouver,<br/>
                    BC V6C 0A3, Canada<br/>
                    P: 1 (800) 460 3217<br/>
                </span>
            </p>
        </div>
        <div class="col-sm-3">
            <h1>Email</h1>
            <p><a href="mailto:anupama@blogtard.com?Subject=Contact_ionth">hello@ionth.com</a></p>
        </div>
    </div>
</div>
<!--Start of Zopim Live Chat Script-->
<script type="text/javascript">
    window.$zopim || (function (d, s) {
        var z = $zopim = function (c) {
            z._.push(c)
        }, $ = z.s =
                d.createElement(s), e = d.getElementsByTagName(s)[0];
        z.set = function (o) {
            z.set.
                    _.push(o)
        };
        z._ = [];
        z.set._ = [];
        $.async = !0;
        $.setAttribute('charset', 'utf-8');
        $.src = '//v2.zopim.com/?2zBCZywA2ATmYPo98GVNvRgFeipDDFQE';
        z.t = +new Date;
        $.
                type = 'text/javascript';
        e.parentNode.insertBefore($, e)
    })(document, 'script');
</script>
<!--End of Zopim Live Chat Script-->
<?php
$this->layout = 'gridle_in';
$this->set('title_for_layout', 'Activate your network');
?>
<div class="container-fluid">
    <div class="row" style="padding-bottom: 15%;background: #FDFDFD;">
        <div style="margin:2% 0;" class="text-center text-info"><h1>Activate your network</h1></div>
        <div class="text-center center-block" style="position: relative;">
            <img src="/img/inside/upgrade_lq.jpg" style="width:50%;">
            <p class="font-big-1 text-center p-margin-0" style="position: absolute; width: 100%; bottom: 0px">To activate your network please contact us  at <span class="text-emphasize">theteam[at]gridle.in</span></p>
        </div>
        <p class="text-center" style="margin-top: 5%;"><a href="<?php echo $_SERVER['HTTP_REFERER']; ?>"><i class="fa fa-chevron-circle-left"></i> Back</a></p>
        
    </div>
</div>
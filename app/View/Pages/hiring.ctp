<?php
$this->layout = 'gridle_out';
$this->set('title_for_layout', "We're hiring");
?>
<div class="g-page-wrap hiring-wrap container-fluid">
    <div class="row">
        <div class="col-lg-offset-3 col-lg-6 text-center">
            <div><p class="font-big-6" >Yes, We're hiring!</p></div>
        </div>
    </div>
</div>
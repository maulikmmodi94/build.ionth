<?php
$this->layout = 'layout_in_demo';
$this->set('title_for_layout', 'Demo');
?>
<link rel="stylesheet" href="/css/vendors/datetimepicker/kendo.common.min.css" />
<link rel="stylesheet" href="/css/vendors/datetimepicker/kendo.default.min.css" />
<!--<link rel="stylesheet" href="/css/vendors/datetimepicker/kendo.dataviz.min.css" />-->
<!--<link rel="stylesheet" href="/css/vendors/datetimepicker/kendo.dataviz.default.min.css" />-->
<div class="demo-section k-header col-sm-8" ng-controller="MyCtrl">
    <h4>Select date and time:</h4>
    <input kendo-date-time-picker
           ng-model="str"
           k-ng-model="obj"
           style="width: 100%;" />
    <pre>
  str: {{ str}}
  obj: {{ obj}}
  typeof obj: {{ getType(obj)}}
  obj instanceof Date: {{ isDate(obj)}}
    </pre>
</div>

<script>

</script>
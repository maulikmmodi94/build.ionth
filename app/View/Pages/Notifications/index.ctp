<?php
$this->layout = 'gridle_in';
$this->set('title_for_layout', 'Notifications');
?>
<div class="container-fluid" ng-controller="notificationController" ng-init="notificationInit();">
    <div class="log-page-wrap row">
        <div class="col-lg-offset-3 col-lg-6">
            <!--    ACTIVITY LOG OPTIONS        -->
            <div class="activity-log-options container-fluid">
                <div class="row" >
                    <div class="col-lg-8">
                        <p class="font-big-4 text-muted text-uppercase" >Notifications</p>
                    </div>
                    <div class="form-group col-lg-4">
                        <input class="form-control input-sm" type="text" placeholder="Search" ng-model="searchtext">    
                    </div>
                </div>
            </div>
            <!--       NOTIFICATION DETAILS            -->
            <div ng-repeat="notification in notifications">
                <!--        DATE OF THE NOTIFICATION             -->
                <div class="" ng-if="$index == 0 || (dateFormat(notifications[$index].time, 'shortDate') != dateFormat(notifications[$index - 1].time, 'shortDate'))">
                    <div class='log-date-heading'>
                        <p class="label label-warning font-big-1"  >{{momentDate(notification.time)}}</p>
                    </div>
                </div>
                <!--  NOTIFICATION EXPANSION           -->      
                <div class="log-message-div">

                    <p class='log-message-icon p-margin-0'>
                        <span class="fa" ng-class="{group:'fa-users', task:'fa-tasks', network:'fa-circle',file:'fa-file'}[notification.type]"></span>
                    </p>
                    <p class='log-message text-muted p-margin-0'>
                        <span class=''  ng-bind-html="notification.message"></span>
                    </p>
                    <p class='log-message-link btn-link p-margin-0'>
                        <a href='{{ notification.link }}'>View</a>
                    </p>
                </div>
            </div>
            <button ng-if="logsMoreOlder" ng-click="getLogsData(logsLoadedPage + 1)">Load Older</button>
        </div>
    </div>
</div>
<?php
$this->layout = 'gridle_out';
$this->set('title_for_layout', 'Features to collaborate | Desk | enterprise network | task management | gridle');

echo $this->Html->meta(array('name' => 'keywords', 'content' => 'project management, task management, collaboration tool, enterprise, teams, calendar, planner'));
echo $this->Html->meta(array('name' => 'description', 'content' => 'With desk, network, task-management, IMs, file-sharing and other integrations, gridle allows you to work together, with anyone, at anytime.'));
//robot meta tag
echo $this->Html->meta(array('name' => 'robots', 'content' => 'index, follow'));
//facebook open graph meta tags
echo $this->Html->meta(array('property' => 'og:title', 'content' => 'Features that make gridle the best project management tool'));
echo $this->Html->meta(array('property' => 'og:description', 'content' => 'With desk, network, task-management, IMs, file-sharing and other integrations, gridle allows you to work together, with anyone, at anytime.'));
echo $this->Html->meta(array('property' => 'og:type', 'content' => 'website'));
echo $this->Html->meta(array('property' => 'og:image', 'content' => 'http://gridle.io/img/homepage/gridle_features.jpg'));
echo $this->Html->meta(array('property' => 'og:image:url', 'content' => 'http://gridle.io/img/homepage/gridle_features.jpg'));
echo $this->Html->meta(array('property' => 'og:image:width', 'content' => '600'));
echo $this->Html->meta(array('property' => 'og:image:height', 'content' => '549'));
echo $this->Html->meta(array('property' => 'og:url', 'content' => 'http://gridle.io/pages/features'));
echo $this->Html->meta(array('property' => 'og:site_name', 'content' => 'Gridle'));
echo $this->Html->meta(array('property' => 'fb:admins', 'content' => 'anupama.panchal'));
echo $this->Html->meta(array('property' => 'fb:app_id', 'content' => '642120029214138'));
//twitter meta tags
echo $this->Html->meta(array('name' => 'twitter:card', 'content' => 'summary'));
echo $this->Html->meta(array('name' => 'twitter:site', 'content' => '@gridle.io'));
echo $this->Html->meta(array('name' => 'twitter:image:src', 'content' => 'http://gridle.io/img/homepage/gridle_features.jpg'));
?>
<div class="g-page-wrap container-fluid">
    <!--        home page wrapper       -->
    <div id="features_wrap" class="row">
        <h1 class="section_heading">Features</h1>
        <div class="colored_line_pattern_home">
            <p class="pattern_yellow_sec"></p><p class="pattern_red_sec"></p><p class="pattern_green_sec"></p>
        </div>
        <div class="clearfix"></div>
        <h3 class="section_subheading">And things that make gridle awesome!</h3>

        <!--     feature 1 DESK   -->
        <div id="" class="feature_div">
            <!-- small screen -->
            <div class="feature_div_top feature_heading visible-xs row">
                <h2 class='text-center'>We call it 'desk'</h2>
                <div class="colored_line_pattern_home">
                    <p class="pattern_yellow_sec"></p><p class="pattern_red_sec"></p><p class="pattern_green_sec"></p>
                </div>
            </div>
            <!--big screen--> 
            <div class="feature_div_top row hidden-xs">
                <div class="feature_heading col-lg-offset-1 col-lg-4 col-md-4 col-md-offset-1 col-sm-5 text-right" ><h2>We call it <br> 'desk'</h2></div>
                <div class="vertical_colored_pattern col-lg-1 col-md-1 col-sm-1 col-xs-1">
                    <p class="vertical_yellow"></p>
                    <p class="vertical_red"></p>
                    <p class="vertical_green"></p>
                </div>
                <div class="feature_subheading col-lg-6 col-md-6 col-sm-6"><h4>Because it is like your teams are sitting <br>with you around your desk. All. At once. </h4></div>      
            </div>
            <!--desk image--> 
            <div class="feature_explained row">
                <div class="feature_media col-sm-6 col-xs-12">
                    <p class="feature_svg">
                        <img class="center-block" src="/img/homepage/gridle_desk.png" alt="Gridle desk screenshot" width='100%' />
                    </p>
                </div>
                <div class="feature_description col-sm-6 col-xs-12">
                    <p>The desk gives you a <span class="content_highlight">bird eye view</span> of your teams/projects and team-members. Enterprise or not, you can know what your colleagues are upto, what is the stage of your project and perform actionable items like assigning tasks, sharing files or sending messages from <span class="content_highlight">one single shared screen space</span>. This is your team, <span class="content_highlight">visualized.</span></p>
                </div>
            </div>
        </div>

        <!--     feature 2 Network  -->
        <div id="" class="feature_div">
            <!-- small screen -->
            <div class="feature_div_top feature_heading row visible-xs">
                <h2 class='text-center'>Enterprise<br>Network</h2>
                <div class="colored_line_pattern_home">
                    <p class="pattern_yellow_sec"></p><p class="pattern_red_sec"></p><p class="pattern_green_sec"></p>
                </div>
            </div>
            <!--big screen--> 
            <div class="feature_div_top row hidden-xs">
                <div class="feature_heading col-lg-offset-1 col-lg-4 col-md-offset-1 col-md-4 col-sm-5 text-right " ><h2>Enterprise<br>Network</h2></div>
                <div class="vertical_colored_pattern col-lg-1 col-md-1 col-sm-1" style="height:5em;">
                    <p class="vertical_yellow"></p>
                    <p class="vertical_red"></p>
                    <p class="vertical_green"></p>
                </div>
                <div class="feature_subheading col-lg-6 col-md-1 col-sm-6" style="height:5em; margin-top:0px">
                    <h4>Your projects. Your teams. <br> Your rules.</h4>
                </div>      
            </div>

            <div class="feature_explained row">
                <div class="feature_media col-sm-6 col-xs-12">
                    <p class="feature_svg">
                        <img class="center-block" src="/img/homepage/gridle_enterprise_network" alt="Gridle enterprise network" height="" width="100%" style="margin:25px;" />
                    </p>
                </div>
                <div class="feature_description col-sm-6 col-xs-12">
                    <p>Own an enterprise? Want more controls? We have got you covered. With best in class <span class="content_highlight">user-management,</span> we make sure that gridle gives you everything you need including permissions, <span class="content_highlight">privacy and controls</span> just like you wished for. Rest assured with our intuitive control panel which tells you everything you need to know and do. Bend it according to your requirements, just like you want it.</p>
                </div>
            </div>
        </div>

        <!--     feature 3 task management   -->
        <div id="" class="feature_div">
            <!-- small screen -->
            <div class="feature_div_top feature_heading visible-xs row">
                <h2 class='text-center'>Time and Task management</h2>
                <div class="colored_line_pattern_home">
                    <p class="pattern_yellow_sec"></p><p class="pattern_red_sec"></p><p class="pattern_green_sec"></p>
                </div>
            </div>
            <!--big screen--> 
            <div class="feature_div_top row hidden-xs">
                <div class="feature_heading col-lg-offset-1 col-lg-4 col-md-4 col-md-offset-1 col-sm-5 text-right"><h2>Time and Task <br> management</h2></div>
                <div class="vertical_colored_pattern col-lg-1 col-md-1 col-sm-1" style="height:5em;">
                    <p class="vertical_yellow"></p>
                    <p class="vertical_red"></p>
                    <p class="vertical_green"></p>
                </div>
                <div class="feature_subheading col-lg-6 col-md-6 col-sm-6" style="height:5em;"><h4>Helps you visualize goals <br> and have accountability.</h4></div>      
            </div>
            <div class="feature_explained row">
                <div class="feature_media col-sm-6 col-xs-12">
                    <p class="feature_svg">
                        <object class="center-block" data="/img/homepage/gridle_task_management_screen.svg" type="image/svg+xml" width="100%">
                            <img src='/img/homepage/gridle_task_management_screen.jpg' alt='gridle task management screens' width="100%" />
                        </object>
                    </p>
                </div>
                <div class="feature_description col-sm-6 col-xs-12">
                    <p>We make sure that you are always on <span class="content_highlight">top of your priority lists</span> and in knowledge of your team's progress. With efficient creation, assignment and submission, you can cut down on meetings, status reports, and emails and can spend more time <span class="content_highlight">getting things done.</span> Our planner means informed decisions and less work about work.</p>
                </div>
            </div>
        </div>
        <!--     feature 4 Conversation   -->        
        <div id="" class="feature_div">
            <!-- small screen -->
            <div class="feature_div_top feature_heading visible-xs row">
                <h2 class='text-center'>Communicate effectively</h2>
                <div class="colored_line_pattern_home">
                    <p class="pattern_yellow_sec"></p><p class="pattern_red_sec"></p><p class="pattern_green_sec"></p>
                </div>
            </div>
            <!--big screen--> 
            <div class="feature_div_top row hidden-xs">
                <div class="feature_heading col-lg-offset-1 col-lg-4 col-md-4 col-md-offset-1 col-sm-5 text-right" ><h2>Communicate <br>effectively.</h2></div>
                <div class="vertical_colored_pattern col-lg-1 col-md-1 col-sm-1" style="height:5em;">
                    <p class="vertical_yellow"></p>
                    <p class="vertical_red"></p>
                    <p class="vertical_green"></p>
                </div>
                <div class="feature_subheading col-sm-6" style="height:5em;"><h4>Works across all devices <br>and all types of files.</h4></div>      
            </div>
            <div class="feature_explained row">
                <div class="feature_media col-sm-6 col-xs-12">
                    <p class="feature_svg">
                        <object class="center-block" data="/img/homepage/gridle_conversation_screen.svg" type="image/svg+xml" style="max-height: 350px" width="100%">
                            <img src='/img/homepage/gridle_conversation_screen.png' alt="girdle's conversation screen" width="100%"/>
                        </object>   
                    </p>
                </div>
                <div class="feature_description col-sm-6 col-xs-12">
                    <p>We know that you need to know and communicate on the go. With <span class="content_highlight">real-time</span> individual specific messaging and file-sharing, you can always be sure of easily sharing and retrieving your thoughts and assets. Our systems are <span class="content_highlight">secure,</span> have 99.9% up-time and with our diligent <span class="content_highlight">back up systems,</span> the only time you will lose your data would be during end of the world!</p>
                </div>
            </div>
        </div>
        <!--     feature 5 integrations   -->    
        <div id="" class="feature_div">
            <!-- small screen -->
            <div class="feature_div_top feature_heading visible-xs row">
                <h2 class='text-center'>Integrations and much more</h2>
                <div class="colored_line_pattern_home">
                    <p class="pattern_yellow_sec"></p><p class="pattern_red_sec"></p><p class="pattern_green_sec"></p>
                </div>
            </div>
            <div class="feature_div_top row hidden-xs">
                <div class="feature_heading col-lg-offset-1 col-lg-4 col-md-offset-1 col-md-4 col-sm-5 text-right" ><h2>Integrations <br>and much more</h2></div>
                <div class="vertical_colored_pattern col-lg-1 col-md-1 col-sm-1" style="height:5em;">
                    <p class="vertical_yellow"></p>
                    <p class="vertical_red"></p>
                    <p class="vertical_green"></p>
                </div>
                <div class="feature_subheading col-lg-6 col-md-6 col-sm-6" style="height:5em;"><h4>Access your skype and dropbox <br>from inside of gridle.</h4></div>      
            </div>
            <div class="feature_explained row">
                <div class="feature_media col-lg-4 col-lg-offset-1 col-md-6 col-sm-6 col-xs-12">
                    <p class="feature_svg" style="margin: 50px 0;">
                        <object class="center-block" data="/img/homepage/gridle.iotegrations.svg" type="image/svg+xml" width="100%">
                            <img src='/img/homepage/gridle_integrations.jpg' alt='Integrations of dropbox, skype and google on gridle' width="100%"/>
                        </object>
                    </p>
                </div>
                <div class="feature_media col-lg-4 col-lg-offset-1 col-md-6 col-sm-6 col-xs-12">
                    <p class="feature_svg" style="margin: 50px 0;">
                        <object class="center-block" data="/img/homepage/gridle_other_features.svg" type="image/svg+xml" width="100%">
                            <img src='/img/homepage/gridle_other_features.jpg' alt='Other features on gridle' width="100%"/>
                        </object>
                    </p>
                </div>
            </div>
        </div>
    </div>

    <!-- below signup section on each page  -->
    <div id="signup-btn-section" class="">
        <div class="section_column_testimonial row">
            <div class="testimonial_user_img_div col-lg-2 col-md-2 col-sm-2 col-xs-12">
                <img class="testimonial_user_img center-block" src="/img/homepage/gridle_kavina_patel_testimonial.jpg" alt="Gridle's testimonial by kavina patel, ceo quivocal pvt. ltd." style="width:100%;"/>
            </div>
            <div class="testimonial_text col-lg-8 col-md-8 col-sm-8 col-xs-12">
                <p class="testimonial_user_line"><span class="fa fa-quote-left"></span>&nbsp;&nbsp; The nature of storing files is superb. It is simple and I wonder that why dont other services do this! It is so intuitive.&nbsp;&nbsp; <span class="fa fa-quote-right"></span></p>
                <p class="testimonial_username">- Kavina Patel</p>
            </div>
        </div>
        <div class="signup_section home_section" >
            <p><a class="btn btn-lg btn-primary btn-responsive homepage_btns" href="/users/add" >Sign up now for free</a></p>
            <p><a class="signup_section_links" href="/pages/benefits">Know more</a></p>
        </div> 
    </div>
    <!--        signup section end          -->
</div>
<?php
$this->layout = 'gridle_out_new';
$this->set('title_for_layout', 'Careers');
$brand = Configure::read('brand');
?>
<div class="container-fluid">
    <!--    background  -->
    <div class="row">
        <div class="col-sm-12 position-relative backgroundcareers">
            <div class="set-opacity-careers"></div>
                <div class="row header">
                    <div class="col-xs-3 col-md-4 logo">
                        <a href="/ "><img src="/img/logos/logo@3x.png" height="30px"></img></a>
                    </div>
                    <div class="col-xs-offset-6 col-xs-3 col-md-4 col-md-offset-4 login-together">
                        <span class="hidden-xs" style="font-family:bookFont; font-size:13px;">
                            <a href="/pages/careers">
                                <font color="white">
                                We're hiring!
<!--                            Already a member?-->
                                </font>
                            </a>
                        </span>
                        <span><font color="white"> &nbsp;<a href="/login"><button type="button" class="btnLogins btn "> Login</button></a>
                            </font></span>
                    </div>
                </div>
                <div class="row middle line-height-2 margin-bottom-15-percent">
                    <div class="col-sm-12">
                        <p class="stop-the-madness">            
                            Work with us..
                        </p>
                        <center><p class="together-line">
                                It’s what we do together that sets us<br><i>apart!</i>
                            </p></center>
                    </div>
                </div>
                <div class="row buttons margin-bottom-2-percent" style="">
                    <div class="col-xs-offset-4 col-sm-offset-4 col-md-offset-4 col-xs-4 col-sm-4 col-md-4 signup">
                        <a href="#openings">
                            <button type="button" class="btn btnHomeSignup">See Openings</button>
                        </a>
                    </div>
                </div>
                <div class="row  margin-bottom-5-percent">
                        <span class="clearfix"></span>
                        <div class="col-md-6 col-md-offset-3 endtext-careers text-center">
                            <p>We are looking for people who like building great experiences through design, tech or whatever it is that you do.</p>
                        </div>
                        <span class="clearfix"></span>
                </div>         
                
        </div>
    </div>
    <!--    banner  -->
    <div class="row">
        <div id="openings" class="container-fluid terms-header">
            <div class="row">
                <div class= "col-sm-offset-2 col-sm-10">				
                    <h1 class="terms-heading">#RIP9to5</h1>
                    <p class="terms-sub-heading">Get in touch with us on contact[at]gridle[dot]io
                    </p>
                </div>
            </div>
        </div>
    </div>
    <!--    description     -->
    <div class="row">
        <div class="container-fluid background-white" >
            <div class="row" >
                <div class="col-sm-offset-2 col-sm-7">
                    <div class="terms-content-use margin-tb-fifty">

                        <div id="EconomicTimes">

                            <div id="" class="article_style row">
                                <div class="news_logo_div col-lg-4">
                                    <p>
                                        <img class="careers_img" src="/img/careers/icon_1-01.png" alt='Lead Developer' />
                                    </p>
                                </div>
                                <div class="news_article col-lg-8 ">
                                    <h2 class="news_article_heading">Lead Developer</h2><br/>
                                    <p class="news_article_content">Jack of all trades, this one is required to code, manage the tech team, work closely with the founders and set the product’s vision in motion. Apart from skills, knowledge and 2 Yrs+ experience, we are looking for you if you have the passion to build something meaningful. Needs to know PHP like the back of his/her hand. Nerd traits are a plus. 
                                    </p>
                                </div>
                            </div>       
                            <hr class="terms-hr"/>
                        </div>

                        <div id="BusinessStandard">
                            <h1 class="terms-content-heading"></h1>
                            <div id="" class="article_style row">
                                <div class="news_logo_div col-lg-4">
                                    <p><img class="careers_img" src="/img/careers/icon_2-01.png" alt='Sys Engineer' /></p>
                                </div>
                                <div class="news_article col-lg-8 ">
                                    <h2 class="news_article_heading">Sys Engineer</h2><br/>
                                    <p class="news_article_content">The one that makes all things possible. Knows that running a software is not magic but talent and sweat of hard-working coders pulling all-nighters so it works all the time the way it should and at a better-than-expected speed. Taking care of the back-end architecture, product security and management of resources are some of the things expected of you.</p>
                                </div>
                            </div>
                            <hr class="terms-hr"/>
                        </div>

                        <div id="TimesofIndia">
                            <h1 class="terms-content-heading"></h1>
                            <div id="" class="article_style_alt row">
                                <div class="news_logo_div col-lg-4">
                                    <p><img class="careers_img" src="/img/careers/icon_3-01.png" alt='UI/UX Designer' /></p>
                                </div>
                                <div class="news_article col-lg-8 ">
                                    <h2 class="news_article_heading">UI/UX Designer</h2><br/>
                                    <p class="news_article_content">It’s almost rude to build something and not build it beautiful. Look at our Manifesto! Design is at the core of Gridle. We are minimal. If your ambition is to say in ten sentences what others say in a book or if you measure length in pixels or if you say #333 instead of gray or if you prefer to doodle your idea instead of writing notes, join the bandwagon. We’ve been looking for you.</p>
                                </div>
                            </div>
                            <hr class="terms-hr"/>
                        </div>

                        <div id="NextBigWhat">
                            <h1 class="terms-content-heading"></h1>
                            <div id="" class="article_style row">
                                <div class="news_logo_div col-lg-4">
                                    <p><img class="careers_img" src="/img/careers/icon_4-01.png" alt='Front End Engineer' /></p>
                                </div>
                                <div class="news_article col-lg-8 ">
                                    <h2 class="news_article_heading">Front End Engineer</h2><br/>
                                    <p class="news_article_content">Bridges are one of the most important things for a city. Same is true for software teams. Bridging the pixel perfect carve of the interface design with robust back-end code is one of the core responsibilities for this job along with developing an understanding of user requirements and rapidly building prototypes. If your first language is CSS, give us a chance to work with you. Wont disappoint. Promise.</p>
                                </div>
                            </div>
                            <hr class="terms-hr"/>
                        </div>

                        <div id="TechCircleBlog">
                            <h1 class="terms-content-heading"></h1>
                            <div id="" class="article_style row">
                                <div class="news_logo_div col-lg-4">
                                    <p><img class="careers_img" src="/img/careers/icon_5-01.png" alt='VP of Biz Dev' /></p>
                                </div>
                                <div class="news_article col-lg-8 ">
                                    <h2 class="news_article_heading">VP of Biz-Dev</h2><br/>
                                    <p class="news_article_content">Fancy making deals that makes the world just a little bit more productive? This is much more than that. Ability to solve complex problems in the digital B2B space that are not well defined through an insatiable desire to learn new things and improve existing ones is the prime quality we are looking for. 3+ Yrs of work ex. to prove the ability is a definite plus!</p>
                                </div>
                            </div>
                            <hr class="terms-hr"/>
                        </div>
                        <!--        Business Standard      -->
                        <div id="BusinessStandard-2013">
                            <h1 class="terms-content-heading"></h1>
                            <div id="" class="article_style row">
                                <div class="news_logo_div col-lg-4">
                                    <p><img class="careers_img" src="/img/careers/icon_6-01.png" alt='Internships'/></p>
                                </div>
                                <div class="news_article col-lg-8 ">
                                    <h2 class="news_article_heading">Internships</h2><br/>
                                    <p class="news_article_content">Explore your interests and passion. We are always looking to work with smart people. Content writing, doodling, marketing, sales or tech; we have all the roles open for summer, winter, monsoon, spring or fall; at all times.</p>
                                </div>
                            </div>
                            <hr class="terms-hr"/>
                        </div>		
                    </div>
                </div>
                <div id="nav-list" class="col-sm-3 ">
                    <div class="terms-autoscroll-careers">
                        <ul class="nav nav-pills nav-stacked" role="tablist">
                            <li><a href="#EconomicTimes" class="">Lead Developer</a></li>
                            <li><a href="#BusinessStandard" class="">Sys Engineer</a></li>
                            <li><a href="#TimesofIndia" class="">UI/UX Designer</a></li>
                            <li><a href="#NextBigWhat" class="">Front End Engineer</a></li>
                            <li><a href="#TechCircleBlog" class="">VP of Biz-Dev</a></li>
                            <li><a href="#BusinessStandard-2013" class="">Internships</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>






<!--<div class="container-fluid position-relative">
    <div class="row backgroundcareers">
        
      
                    <div class="container-fluid">
                        <div class="row header">
                            <div class="col-xs-3 col-md-4 logo">
                                <a href="/ "><img src="/img/logos/logo@3x.png" height="30px"></img></a>
                            </div>
                            <div class="col-xs-offset-6 col-xs-3 col-md-4 col-md-offset-4 login-together">
                                <span class="hidden-xs" style="font-family:bookFont"><font color="white">
                                    Already a member?</font></span>
                                <span><font color="white"> &nbsp;<a href="/login"><button type="button" class="btnLogins btn "> Login</button></a>
                                    </font></span>
                            </div>
                        </div>
                        <div class="row middle line-height-2">
                            <div class="col-sm-12">
                            <p class="stop-the-madness">            
                                Work with us..
                            </p>
                            <center><p class="together-line">
                                    It’s what we do together that sets us<br><i>apart</i>
                                </p></center>
                            </div>
                        </div>
                        <div class="row buttons " style="">

                            <div class="col-xs-offset-4 col-sm-offset-4 col-md-offset-4 col-xs-4 col-sm-4 col-md-4 signup top-250">
                                <a href="#openings">
                                    <button type="button" class="btn btnCareers">See Openings</button>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="row ">
                        <span class="clearfix"></span>
                        <div class="col-md-6 col-md-offset-3 endtext-careers text-center top-250">
                            <p>We are looking for people who like building great experiences through design, tech or whatever it is that you do.</p>
                        </div>
                        <span class="clearfix"></span>
                    </div>
                
    </div>
    <div class="row position-relative">
        <div class="col-sm-12">
        <div class="set-opacity-careers"></div>
        </div>
    </div>
</div>

<div class="container-fluid"  >
    <div class="row">
        <div class="container-fluid">
            <div class="row header">
                <div class="col-xs-3 col-md-4 logo">
                    <a href="/ "><img src="/img/logos/logo@3x.png" height="30px"></img></a>
                </div>
                <div class="col-xs-offset-6 col-xs-3 col-md-4 col-md-offset-4 login-together">
                    <span class="hidden-xs" style="font-family:bookFont"><font color="white">
                        Already a member?</font></span>
                    <span><font color="white"> &nbsp;<a href="/login"><button type="button" class="btnLogins btn "> Login</button></a>
                        </font></span>
                </div>
            </div>
            <div class="row middle line-height-2">
                <p class="stop-the-madness">            
                    Work with us..
                </p>
                <center><p class="together-line">
                        It’s what we do together that sets us<br><i>apart</i>
                    </p></center>
            </div>
            <div class="row buttons " style="">

        <div class="col-xs-offset-4 col-sm-offset-4 col-md-offset-4 col-xs-4 col-sm-4 col-md-4 signup top-250">
            <a href="#openings">
<button type="button" class="btn btnCareers">See Openings</button>
            </a>
        </div>
            </div>
    </div>
    <div class="row ">
        <span class="clearfix"></span>
        <div class="col-md-6 col-md-offset-3 endtext-careers text-center top-250">
            <p>We are looking for people who like building great experiences through design, tech or whatever it is that you do.</p>
        </div>
        <span class="clearfix"></span>
    </div>
        </div>
    </div>
<div class="position-relative">
	<div id="openings" class="container-fluid terms-header">
		<div class="row">
				<div class= "col-sm-offset-2 col-sm-10">				
					<h1 class="terms-heading">#RIP9to5</h1>
					<p class="terms-sub-heading">Get in touch with us on contact[at]gridle[dot]io
</p>
				</div>
		</div>
	</div>
<div class="container-fluid background-white" >
	<div class="row" >
	<div class="col-sm-offset-2 col-sm-7">
	<div class="terms-content-use margin-tb-fifty">
		
        <div id="EconomicTimes">
            
            <div id="" class="article_style row">
                <div class="news_logo_div col-lg-4">
                    <p><img class="careers_img" src="/img/careers/icon_1-01.png" alt='Lead Developer' /></p>
                </div>
                <div class="news_article col-lg-8 ">
                    <h2 class="news_article_heading">Lead Developer</h2><br/>
                    <p class="news_article_content">Jack of all trades, this one is required to code, manage the tech team, work closely with the founders and set the product’s vision in motion. Apart from skills, knowledge and 2 Yrs+ experience, we are looking for you if you have the passion to build something meaningful. Needs to know PHP like the back of his/her hand. Nerd traits are a plus. 
</p>
                </div>
            </div>       
        <hr class="terms-hr"/>
        </div>
	
		<div id="BusinessStandard">
			<h1 class="terms-content-heading"></h1>
			<div id="" class="article_style row">
                <div class="news_logo_div col-lg-4">
                    <p><img class="careers_img" src="/img/careers/icon_2-01.png" alt='Sys Engineer' /></p>
                </div>
                <div class="news_article col-lg-8 ">
                    <h2 class="news_article_heading">Sys Engineer</h2><br/>
                    <p class="news_article_content">The one that makes all things possible. Knows that running a software is not magic but talent and sweat of hard-working coders pulling all-nighters so it works all the time the way it should and at a better-than-expected speed. Taking care of the back-end architecture, product security and management of resources are some of the things expected of you.</p>
                </div>
            </div>
		<hr class="terms-hr"/>
		</div>
	
		<div id="TimesofIndia">
			<h1 class="terms-content-heading"></h1>
			<div id="" class="article_style_alt row">
                <div class="news_logo_div col-lg-4">
                    <p><img class="careers_img" src="/img/careers/icon_3-01.png" alt='UI/UX Designer' /></p>
                </div>
                <div class="news_article col-lg-8 ">
                    <h2 class="news_article_heading">UI/UX Designer</h2><br/>
                    <p class="news_article_content">It’s almost rude to build something and not build it beautiful. Look at our Manifesto! Design is at the core of Gridle. We are minimal. If your ambition is to say in ten sentences what others say in a book or if you measure length in pixels or if you say #333 instead of gray or if you prefer to doodle your idea instead of writing notes, join the bandwagon. We’ve been looking for you.</p>
                </div>
            </div>
			<hr class="terms-hr"/>
		</div>
	
		<div id="NextBigWhat">
			<h1 class="terms-content-heading"></h1>
			<div id="" class="article_style row">
                <div class="news_logo_div col-lg-4">
                    <p><img class="careers_img" src="/img/careers/icon_4-01.png" alt='Front End Engineer' /></p>
                </div>
                <div class="news_article col-lg-8 ">
                    <h2 class="news_article_heading">Front End Engineer</h2><br/>
                    <p class="news_article_content">Bridges are one of the most important things for a city. Same is true for software teams. Bridging the pixel perfect carve of the interface design with robust back-end code is one of the core responsibilities for this job along with developing an understanding of user requirements and rapidly building prototypes. If your first language is CSS, give us a chance to work with you. Wont disappoint. Promise.</p>
                </div>
            </div>
			<hr class="terms-hr"/>
		</div>
		
		<div id="TechCircleBlog">
			<h1 class="terms-content-heading"></h1>
			<div id="" class="article_style row">
                <div class="news_logo_div col-lg-4">
                    <p><img class="careers_img" src="/img/careers/icon_5-01.png" alt='VP of Biz Dev' /></p>
                </div>
                <div class="news_article col-lg-8 ">
                    <h2 class="news_article_heading">VP of Biz-Dev</h2><br/>
                    <p class="news_article_content">Fancy making deals that makes the world just a little bit more productive? This is much more than that. Ability to solve complex problems in the digital B2B space that are not well defined through an insatiable desire to learn new things and improve existing ones is the prime quality we are looking for. 3+ Yrs of work ex. to prove the ability is a definite plus!</p>
                </div>
            </div>
			<hr class="terms-hr"/>
		</div>
		        Business Standard      
		<div id="BusinessStandard-2013">
			<h1 class="terms-content-heading"></h1>
			<div id="" class="article_style row">
                <div class="news_logo_div col-lg-4">
                    <p><img class="careers_img" src="/img/careers/icon_6-01.png" alt='Internships'/></p>
                </div>
                <div class="news_article col-lg-8 ">
                    <h2 class="news_article_heading">Internships</h2><br/>
                    <p class="news_article_content">Explore your interests and passion. We are always looking to work with smart people. Content writing, doodling, marketing, sales or tech; we have all the roles open for summer, winter, monsoon, spring or fall; at all times.</p>
                </div>
            </div>
			<hr class="terms-hr"/>
		</div>		
	</div>
</div>
<div id="nav-list" class="col-sm-3 ">
	<div class="terms-autoscroll-careers">
		<ul class="nav nav-pills nav-stacked" role="tablist">
                        <li><a href="#EconomicTimes" class="">Lead Developer</a></li>
			<li><a href="#BusinessStandard" class="">Sys Engineer</a></li>
			<li><a href="#TimesofIndia" class="">UI/UX Designer</a></li>
			<li><a href="#NextBigWhat" class="">Front End Engineer</a></li>
			<li><a href="#TechCircleBlog" class="">VP of Biz-Dev</a></li>
			<li><a href="#BusinessStandard-2013" class="">Internships</a></li>
		</ul>
	</div>
</div>
</div>
</div>
</div>-->
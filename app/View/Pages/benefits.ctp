<?php
$this->layout = 'gridle_out';
$this->set('title_for_layout', 'Benefits of collaboration | Visual data | Team efficiency increase | availability | gridle ');
echo $this->Html->meta(array('name' => 'keywords', 'content' => 'project management, task management, collaboration tool, enterprise, teams, calendar, planner'));
echo $this->Html->meta(array('name' => 'keywords', 'content' => 'collaboration tool, enterprise, teams, calendar, task management, planner'));
echo $this->Html->meta(array('name' => 'description', 'content' => 'With 24*7 availability and seamless sync across all devices, you can manage projects with an array of amazing features under scrutiny of advanced security.'));
//robot meta tag
echo $this->Html->meta(array('name' => 'robots', 'content' => 'index, follow'));
//facebook open graph meta tags
echo $this->Html->meta(array('property' => 'og:title', 'content' => 'Benefits of using gridle for your project management'));
echo $this->Html->meta(array('property' => 'og:description', 'content' => 'With 24*7 availability and seamless sync across all devices, you can manage projects with an array of amazing features under scrutiny of advanced security.'));
echo $this->Html->meta(array('property' => 'og:type', 'content' => 'website'));
echo $this->Html->meta(array('property' => 'og:image', 'content' => 'http://gridle.io/img/homepage/gridle_benefits.jpg'));
echo $this->Html->meta(array('property' => 'og:image:url', 'content' => 'http://gridle.io/img/homepage/gridle_benefits.jpg'));
echo $this->Html->meta(array('property' => 'og:image:width', 'content' => '600'));
echo $this->Html->meta(array('property' => 'og:image:height', 'content' => '549'));
echo $this->Html->meta(array('property' => 'og:url', 'content' => 'http://gridle.io/pages/benefits'));
echo $this->Html->meta(array('property' => 'og:site_name', 'content' => 'Gridle'));
echo $this->Html->meta(array('property' => 'fb:admins', 'content' => 'anupama.panchal'));
echo $this->Html->meta(array('property' => 'fb:app_id', 'content' => '642120029214138'));
//twitter meta tags
echo $this->Html->meta(array('name' => 'twitter:card', 'content' => 'summary'));
echo $this->Html->meta(array('name' => 'twitter:site', 'content' => '@gridle_io'));
?>
<div class="g-page-wrap container-fluid">
    <!--        home page wrapper       -->
    <div id="benefits_landing" class="row">
        <h1 class="section_heading">Why gridle?</h1>
        <div class="colored_line_pattern_home">
            <p class="pattern_yellow_sec"></p><p class="pattern_red_sec"></p><p class="pattern_green_sec"></p>
        </div>
        <div class="clearfix"></div>
        <h3 class="section_subheading">Still wondering? Check out the reasons below!</h3>
        <p id="benefits_description" class="col-lg-6 col-lg-offset-3">
            You know the drill. You have tried a lot of project management softwares online and have not been satisfied. You then have moved back to email. Emails are good for great many things but not for long running projects. Gridle is one platform <span class="content_highlight"> available cross devices </span> to <span class="content_highlight"> communicate tasks, share files and instantly message </span> so you can stay <span class="content_highlight">on top of priorities, store and retrieve assets</span> and stay in touch <span class="content_highlight">on the go</span>. Now, there are a lot of tools out there and decision making could be hard. So, we have prepared a list of benefits and a tour for you to check out how gridle helps.
        </p>

    </div>
    <div id="benefits_listed" class="">
        <!--    benefit first           -->
        <div class="benefits_div" style="">
            <!-- small screen -->
            <div class="feature_div_top feature_heading visible-xs row">
                <h2 class='text-center'>One platform</h2>
                <div class="colored_line_pattern_home">
                    <p class="pattern_yellow_sec"></p><p class="pattern_red_sec"></p><p class="pattern_green_sec"></p>
                </div>
            </div>
            <!--big screen-->
            <div class="feature_div_top row hidden-xs">
                <div class="feature_heading col-lg-offset-2 col-md-offset-2 col-lg-3 col-md-3 col-sm-5 text-right" ><h2>One <br> platform</h2></div>
                <div class="vertical_colored_pattern col-lg-1 col-md-1 col-sm-1">
                    <p class="vertical_yellow"></p>
                    <p class="vertical_red"></p>
                    <p class="vertical_green"></p>
                </div>
                <div class="feature_subheading col-lg-6 col-md-6 col-sm-6" ><h4 style="">Unifying your biggest resource..<br> <span class="content_highlight">< People ></span></h4></div>      
            </div>
            <div class="feature_explained row">
                <p class="col-sm-8 col-sm-offset-2 col-xs-12" style="text-align: center; padding: 30px 0">
                    <object class="center-block" data="/img/homepage/features_at_gridle.svg" width="100%">
                        <img src='/img/homepage/features_at_gridle.jpg' alt="Features at gridle" />
                    </object>
                </p>    
            </div>
        </div>
        <!--        data visualization         -->
        <div id="" class="benefits_div article_style_alt">
            <!-- small screen -->
            <div class="feature_div_top feature_heading visible-xs row">
                <h2 class='text-center'>Data visualization</h2>
                <div class="colored_line_pattern_home">
                    <p class="pattern_yellow_sec"></p><p class="pattern_red_sec"></p><p class="pattern_green_sec"></p>
                </div>
            </div>
            <!--big screen-->
            <div class="feature_div_top row hidden-xs" >
                <div class="feature_heading col-lg-offset-2 col-lg-3 col-md-offset-2 col-sm-5 col-xs-5 text-right"style="" ><h2>Also.. <br>datavisualization</h2></div>
                <div class="vertical_colored_pattern col-lg-1 col-md-5 col-sm-1 col-xs-1">
                    <p class="vertical_yellow"></p>
                    <p class="vertical_red"></p>
                    <p class="vertical_green"></p>
                </div>
                <div class="feature_subheading col-lg-6" style=""><h4 style="">That lets you do more<br> with less.</h4></div>      
            </div>
            <div class="feature_explained row">
                <div class="feature_media col-sm-5 col-sm-offset-1 col-xs-12" style="">
                    <p class="feature_svg">
                        <img src="/img/homepage/gridle_desk.png" alt="Gridle desk screenshot" width="80%"/>
                    </p>
                </div>
                <div class="feature_description col-sm-5 col-xs-12" style="">
                    <p>Get a bird-eye view of your teams, members, files, tasks and messages on <span class="content_highlight">one shared screen.</span> Delegate, track, communicate, visualize and comprehend everything without moving, scouting or asking around. This means, your work is about <span class="content_highlight">getting things done.</span></p>
                </div>
            </div>
        </div>
        <!--        Cloud         -->
        <div id="" class="benefits_div">
            <!-- small screen -->
            <div class="feature_div_top feature_heading visible-xs row">
                <h2 class='text-center'>24 X 7 Availaibility</h2>
                <div class="colored_line_pattern_home">
                    <p class="pattern_yellow_sec"></p><p class="pattern_red_sec"></p><p class="pattern_green_sec"></p>
                </div>
            </div>
            <!--big screen-->
            <div class="feature_div_top row hidden-xs">
                <div class="feature_heading col-lg-offset-1 col-lg-3 col-md-offset-1 col-md-3 col-sm-5 text-right" style="" ><h2>24 X 7<br>Availability</h2></div>
                <div class="vertical_colored_pattern col-lg-1 col-md-1 col-sm-1">
                    <p class="vertical_yellow"></p>
                    <p class="vertical_red"></p>
                    <p class="vertical_green"></p>
                </div>
                <div class="feature_subheading col-lg-6 col-md-6 col-sm-6" style=""><h4>Let people make business happen. <br>From anywhere, with anyone, at anytime.</h4></div>      
            </div>
            <div class="feature_explained row">
                <div class="feature_media col-sm-5 col-sm-offset-1 col-xs-12" style="">
                    <p class="feature_svg">
                        <object data="/img/homepage/gridle_on_multiple_devices.svg" width="100%">
                            <img src="/img/homepage/gridle_on_multiple_devices.jpg" alt="gridle on multiple devices" />
                        </object>
                    </p>
                </div>
                <div class="feature_description col-sm-5 col-xs-12" style="">
                    <p>Gridle is available on <span class="content_highlight">any device</span> with internet ability. There is nothing to download, save or run. There is nothing better than knowing that your work is going on smoothly when you are away from your desk. Think vacations, traveling, working from home.</p>
                </div>
            </div>
        </div>
        <!--        Security         -->
        <div id="" class="benefits_div">
            <!-- small screen -->
            <div class="feature_div_top feature_heading visible-xs row">
                <h2 class='text-center'>Data Security</h2>
                <div class="colored_line_pattern_home">
                    <p class="pattern_yellow_sec"></p><p class="pattern_red_sec"></p><p class="pattern_green_sec"></p>
                </div>
            </div>
            <!--big screen-->
            <div class="feature_div_top row hidden-xs">
                <div class="feature_heading  col-lg-4 col-md-4 col-sm-4 text-right" style=""><h2>Data<br>Security</h2></div>
                <div class="vertical_colored_pattern col-lg-1 col-md-1 col-sm-1">
                    <p class="vertical_yellow"></p>
                    <p class="vertical_red"></p>
                    <p class="vertical_green"></p>
                </div>
                <div class="feature_subheading col-lg-6 col-md-6 col-sm-6" style=""><h4 style="">Regular backups <br>and military grade security.</h4></div>      
            </div>
            <div class="feature_explained row">
                <div class="feature_media col-sm-4 col-sm-offset-1 col-xs-12" style="">
                    <p class="feature_svg" style="text-align:right;">
                        <object class="center-block" data="/img/homepage/gridle_secure_cloud.svg">
                            <img src="/img/homepage/gridle_secure_cloud.jpg" alt="gridle's secure cloud" />
                        </object>
                    </p>
                </div>
                <div class="feature_description col-sm-5 col-xs-12" style="">
                    <p>Gridle leverages <span class="content_highlight">multiple layers of defense </span>to protect key information and handle all critical facets of network and application security, including <span class="content_highlight">authentication, authorization and assurance.</span> We back up your data to our secure servers to prevent data-loss. Feel security, assurance and freedom.</p>
                </div>
            </div>
        </div>
    </div>


    <!-- below signup section on each page  -->
    <div  id="signup-btn-section" class="">
        <div class="section_column_testimonial row">
            <div class="testimonial_user_img_div col-lg-2 col-md-2 col-sm-12 col-xs-12">
                <img class="testimonial_user_img center-block" src="/img/homepage/gridle_shaival_gajjar_testimonial.jpg" alt="Gridle's testimonial by Shaival Gajjar, lead design of quivocal technologies pvt. ltd." style="width:100%;"/>
            </div>
            <div class="testimonial_text col-lg-8 col-md-8 col-sm-12 col-xs-12">
                <p class="testimonial_user_line"><span class="fa fa-quote-left"></span>&nbsp;&nbsp; It took us atleast 4 threads of mail to set up a meeting before! this reduced it to creating 1 activity.&nbsp;&nbsp;<span class="fa fa-quote-right"></span></p>
                <p class="testimonial_username">- Shaival Gajjar</p>
            </div>
        </div>
        <div class="signup_section home_section" >
            <p><a class="btn btn-lg btn-primary btn-responsive homepage_btns" href="/users/add" >Sign up now for free</a></p>
            <p><a class="signup_section_links" href="/pages/features">Take a tour</a> </p>
        </div> 
    </div>
    <!--        signup section end          -->
</div>
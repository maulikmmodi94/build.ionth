<?php
$this->layout = 'gridle_out_new';
$this->set('title_for_layout', 'ContactUs | Build');
$brand = Configure::read('brand');
?>
	<title>Contact Us</title>
	<meta content="width=device-width, initial-scale=1, maximum-scale=1" name="viewport">
	
        <div class="container-fluid backc">
            <div class="row">
                <div class="col-sm-4 col-xs-3 r"><div class="hr"></div>
                    <hr class="hrr">
                </div>
                <div class="col-sm-4 col-xs-6 buttonview">
                    Contact us
                </div>
                <div class="col-sm-4 col-xs-3 l"><div class="hr"></div>
                    <hr class="hrl">
                </div>
            </div>
            <!--                <div class="row">
                               <div class="col-sm-offset-4 col-sm-4 col-xs-6 buttonview-changed">
                                    Contact us
                                </div>
                            </div>		-->
            <div class="row">
                <div class="col-sm-offset-4 col-sm-4 col-xs-offset-2 col-xs-8">
                    <div class="loginbox">
                        <form class="horizontal" name="login" method="post" action="_blank">
                            <div class="col-sm-offset-1 col-sm-10">
                                <div class="form-group">
                                    <p class="fp">Questions, anyone? Drop us a line and we will get back real quick.</p>
                                </div>
                            </div>
                            <div class="col-sm-offset-1 col-sm-10">
                                <div class="form-group">
                                    <h6 class="be">Need quicker replies? Mentions on twitter is the fastest way to engage.</h6>
                                </div>
                            </div>
                            <div class="col-sm-offset-1 col-sm-10">
                                <div class="form-group">
                                    <p class="contact-email"><span style="opacity: 0.34;">Send us an email:</span><font color="#148eae"><a href="mailto:contact@gridle.io"> hello@ionth.com </a></font></p>
                                </div>
                            </div>
                            <!--					<div class="col-sm-offset-1 col-sm-10">
                                                                            <div class="form-group">
                                                                                <a href="https://twitter.com/gridle_io"><input type="text" class="form-control" name="email" id="email" placeholder="Twitter : @gridle.io" style="cursor: pointer;" OnClick="window.location='https://twitter.com/gridle_io'" disabled></a>
                                                                            </div>
                                                                    </div>-->
                            <!--                                        <div class="col-sm-offset-1 col-sm-10">
                                                                            <div class="form-group">
                                                                                    <h6 class="be">Needless to say, we are toll free.</h6>
                                                                            </div>
                                                                    </div>-->
                            <!--					<div class="col-sm-offset-1 col-sm-10">
                                                                            <div class="form-group">
                                                                                <input type="text" class="form-control" name="window" id="email" placeholder="Phone : +91-9377965301" style="cursor: default;" disabled>
                                                                            </div>
                                                                    </div>-->
                            <div class="col-sm-offset-1 col-sm-10" style="height:21px;">
                                <div class="form-group">
                                    <a href="https://twitter.com/ionthglobal" target="_blank"><button type="button" class="btn logins2"><font color="#005F3F">Contact us on twitter:<font color="white"> @ionthglobal</font></font></button></a>
                                </div>		
                            </div>
                            <div class="col-sm-offset-1 col-sm-10" style="padding-top: 25px;">
                                <div class="form-group">
                                    <h6 class="be"> This button will directly open the our twitter handle. If you choose to mail us personally you can always mail us at hello@ionth.com</h6>
                                </div>
                            </div>
                            <div class="col-sm-offset-1 col-sm-10">
                                <div class="form-group">
                                    <hr class="hrb">
                                </div>
                            </div>
                            <div class="row fontbottom">
                                <div class="col-sm-offset-1 col-sm-10">
                                    <a href="/users/add"><h6>Lets work together, take me to signup.</h6></a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

            </div>
        </div>
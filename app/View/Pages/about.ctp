<?php
$this->layout = 'gridle_out';
$this->set('title_for_layout', 'About | gridle.io');

echo $this->Html->meta(array('name' => 'keywords', 'content' => 'project management, task management, collaboration tool, enterprise, teams, calendar, planner'));
echo $this->Html->meta(array('name' => 'description', 'content' => 'Gridle is a complete cloud visual collaboration and communication tool for your time, task and project management for enterprises and teams.'));
//robot meta tag
echo $this->Html->meta(array('name' => 'robots', 'content' => 'index, follow'));
//facebook open graph meta tags
echo $this->Html->meta(array('property' => 'og:title', 'content' => 'gridle | visual cloud collaboration tool for enterprises and teams'));
echo $this->Html->meta(array('property' => 'og:description', 'content' => 'Gridle is a complete cloud visual collaboration and communication tool for your time, task and project management for enterprises and teams.'));
echo $this->Html->meta(array('property' => 'og:type', 'content' => 'website'));
echo $this->Html->meta(array('property' => 'og:image', 'content' => 'http://gridle.io/img/logo/gridle_logo.png'));
echo $this->Html->meta(array('property' => 'og:image:url', 'content' => 'http://gridle.io/img/logo/gridle_logo.png'));
echo $this->Html->meta(array('property' => 'og:image:width', 'content' => '150'));
echo $this->Html->meta(array('property' => 'og:image:height', 'content' => '50'));
echo $this->Html->meta(array('property' => 'og:url', 'content' => 'http://gridle.io/pages/about'));
echo $this->Html->meta(array('property' => 'og:site_name', 'content' => 'Gridle'));
echo $this->Html->meta(array('property' => 'fb:admins', 'content' => 'anupama.panchal'));
echo $this->Html->meta(array('property' => 'fb:app_id', 'content' => '642120029214138'));
//twitter meta tags
echo $this->Html->meta(array('name' => 'twitter:card', 'content' => 'summary'));
echo $this->Html->meta(array('name' => 'twitter:site', 'content' => '@gridle_io'));
echo $this->Html->meta(array('name' => 'twitter:image:src', 'content' => 'http://gridle.io/img/logo/gridle_logo.png'));
?>
<!--        home page wrapper       -->
<div class="g-page-wrap container-fluid">
    <div id="about_wrap" class="row" >
        <div id="aboutus_wrap" class="container-fluid">
            <h1 class="section_heading">About us</h1>
            <div class="colored_line_pattern_home">
                <p class="pattern_yellow_sec"></p><p class="pattern_red_sec"></p><p class="pattern_green_sec"></p>
            </div>
            <div class="clearfix"></div>
            <h3 class="section_subheading">Hackers, hustlers and normal people.</h3>

            <div class="row">
                <div id="myCarousel" class="carousel slide" data-ride="carousel" style="display: inline-block;">
                    <!-- Indicators -->
                    <ol class="carousel-indicators">
                        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                        <li data-target="#myCarousel" data-slide-to="1"></li>
                        <li data-target="#myCarousel" data-slide-to="2"></li>
                        <li data-target="#myCarousel" data-slide-to="3"></li>
                    </ol>
                    <div class="carousel-inner">
                        <div class="item active">
                            <img class="desaturate" src="/img/team/group/gridle_team_photo_office.jpg" alt="Gridle's team working at office" height="400px">

                        </div>
                        <div class="item">
                            <img class="desaturate" src="/img/team/group/gridle_team_photo_at_ciie.jpg" alt="Gridle's team at CIIE, IIM-A campus" height="400px">

                        </div>
                        <div class="item">
                            <img class="desaturate" src="/img/team/group/gridle_office_ciie.jpg" alt="Gridle's office outside premises" height="400px">

                        </div>
                        <div class="item">
                            <img class="desaturate" src="/img/team/group/gridle_team_photo_ciie_garden.jpg" alt="Gridle's team discussing at CIIE garden" height="400px">

                        </div>
                        <div class="item">
                            <img class="desaturate" src="/img/team/group/gridle_team_photo_gaming_night.jpg" alt="Gridle's team having a gaming night at CIIE, IIM-A campus" height="400px">

                        </div>
                    </div>
                </div><!-- /.carousel -->
                <div id="our_story_wrap">
                    <h1 class="section_heading">Our Story</h1>
                    <div class="colored_line_pattern_home">
                        <p class="pattern_yellow_sec"></p><p class="pattern_red_sec"></p><p class="pattern_green_sec"></p>
                    </div>
                    <div class="clearfix"></div>
                    <h3 class="section_subheading">Scratching our own itch.</h3>
                    <p class="our_story_para">So, we were three happy friends designing websites and free-lancing for pocket-money. Being in different locations, we shortly underwent the wrath of notorious emails when we tried to collaborate. With our assets landing in places harder to find than the National Treasure, we realized that there is an itch we need to scratch. There was no light-bulb moment for us. Everything happened over a period of time. We wanted to have something for our own use which was generic because we already had used specific tools to make it worse for us. The idea at gridle was not to build something cool or awesome; but to bring about a change that was necessary. In these times of statistics and matrices and user retention and innovation, we felt that a simple, generic and useful tool was missing. Making gridle was freeing - If we didn’t like something, we’d change it, and if we liked something, we’d chase it and try to get it perfect. We are two years old and getting better with age. Change in collaboration is all we seek and we are giving it everything. The deal is just getting started. Check out whats up with us.</p>
                </div>

                <div id="team_wrap">
                    <h1 class="section_heading">The team</h1>
                    <div class="colored_line_pattern_home">
                        <p class="pattern_yellow_sec"></p><p class="pattern_red_sec"></p><p class="pattern_green_sec"></p>
                    </div>
                    <div class="clearfix"></div>
                    <h3 class="section_subheading">Designed to sleep standing.</h3>

                    <div id="team_gallery">

                        <div class="gallery_sec">
                            <div class="team_photo_div">
                                <!--        YASH SHAH       -->
                                <div class="team_photo_sec" >
                                    <img  class="team_photo_img desaturate" src="/img/team/Yash_Shah.jpg" alt='Yash Shah, Co-founder and CEO, gridle' />
                                    <p class="team_social" style="">
                                        <a class="fa fa-facebook" target="_blank" href="https://www.facebook.com/yashparallel" alt="Yash Shah"></a>
                                        <a class="fa fa-twitter" target="_blank" href="https://twitter.com/yashparallel"></a>
                                        <a class="fa fa-linkedin" target="_blank" href="https://in.linkedin.com/in/yashparallel"></a>
                                        <a class="fa fa-globe" target="_blank" href="http://yashshah.in" ></a>
                                    </p>
                                </div>
                                <h2 class="team_photo_name">Yash Shah</h2>
                                <h3 class="team_photo_designation">Co-founder & CEO</h3>
                            </div>

                            <!--        ANUPAMA PANCHAL       -->
                            <div class="team_photo_div">
                                <div class="team_photo_sec" >
                                    <img class="team_photo_img desaturate" src="/img/team/Anupama_Panchal.jpg" alt='Anupama Panchal, Co-founder and CTO, gridle' />
                                    <p class="team_social" style="">
                                        <a class="fa fa-facebook" target="_blank" href="https://www.facebook.com/anupama.panchal" ></a>
                                        <a class="fa fa-twitter" target="_blank" href="https://twitter.com/anupamapanchal"></a>
                                        <a class="fa fa-linkedin" target="_blank" href="https://in.linkedin.com/in/anupamapanchal"></a>
                                        <a class="fa fa-globe" target="_blank" href="http://anupamapanchal.gridle.io"></a>
                                    </p>
                                </div>
                                <h2 class="team_photo_name">Anupama Panchal</h2>
                                <h3 class="team_photo_designation">Co-founder & CTO</h3>
                            </div>
                            <!--        ABHISHEK DOSHI      -->
                            <div class="team_photo_div">
                                <div class="team_photo_sec" >
                                    <img class="team_photo_img desaturate" src="/img/team/Abhishek_Doshi.jpg" alt='Abhishek Doshi, Co-founder and Creative Head, gridle' />
                                    <p class="team_social" style="">
                                        <a class="fa fa-facebook" target="_blank" href="https://www.facebook.com/abhishek.doshi.948" ></a>
                                        <a class="fa fa-twitter" target="_blank" href="https://twitter.com/Abhishek_gridle"></a>
                                        <a class="fa fa-linkedin" target="_blank" href="http://linkedin.com/pub/abhishek-doshi/22/443/b58"></a>
                                        <a class="fa fa-globe" target="_blank" href="http://abhishekdoshi.com" ></a>
                                    </p>
                                </div>
                                <h2 class="team_photo_name">Abhishek Doshi</h2>
                                <h3 class="team_photo_designation">Co-founder & Creative Head</h3>
                            </div>
                        </div>
                        <!--        NAIMISH SAKHPARA      -->
                        <div class="gallery_sec">
                            <div class="team_photo_div">
                                <div class="team_photo_sec">
                                    <img class="team_photo_img desaturate" src="/img/team/Naimish_Sakhpara.jpg" alt='Naimish Sakhpara, Product Manager, girdle' />
                                    <p class="team_social" style="">
                                        <a class="fa fa-facebook" target="_blank" href="https://www.facebook.com/profile.php?id=576595161" ></a>
                                        <a class="fa fa-twitter" target="_blank" href="https://twitter.com/NaimishSakhpara"></a>
                                        <a class="fa fa-linkedin" target="_blank" href="http://in.linkedin.com/in/naimishsakhpara"></a>
                                        <a class="fa fa-globe" target="_blank" href="http://naimishsakhpara.me/" ></a>
                                    </p>
                                </div>
                                <h2 class="team_photo_name">Naimish Sakhpara</h2>
                                <h3 class="team_photo_designation">Product Manager</h3>
                            </div>
                            <!--        HARDIK SONDAGAR      -->
                            <div class="team_photo_div">
                                <div class="team_photo_sec">
                                    <img  class="team_photo_img desaturate" src="/img/team/Hardik_Sondagar.jpg" alt='Hardik Sondagar, Lead Tech, gridle' />
                                    <p class="team_social" style="">
                                        <a class="fa fa-facebook" target="_blank" href="https://www.facebook.com/profile.php?id=100000513517317" ></a>
                                        <a class="fa fa-twitter" target="_blank" href="https://twitter.com/hardiksondagar"></a>
                                        <a class="fa fa-linkedin" target="_blank" href="http://in.linkedin.com/in/hardiksondagar"></a>
                                        <a class="fa fa-globe" target="_blank" href="http://hardiksondagar.me" ></a>
                                    </p>
                                </div>
                                <h2 class="team_photo_name">Hardik Sondagar</h2>
                                <h3 class="team_photo_designation">Lead Tech</h3>

                            </div>
                            <!--        SHOURYA JAIN      -->
                            <div class="team_photo_div">
                                <div class="team_photo_sec" >
                                    <img class="team_photo_img desaturate" src="/img/team/Shourya_Jain.jpg" alt='Shourya Jain, business admin, gridle' />
                                    <p class="team_social" style="">
                                        <a class="fa fa-facebook" target="_blank" href="https://www.facebook.com/profile.php?id=100000373511411" ></a>
                                        <a class="fa fa-twitter" target="_blank" href="https://twitter.com/shourya_orion"></a>
                                        <a class="fa fa-linkedin" target="_blank" href="https://in.linkedin.com/in/shouryaj"></a>
                                        <a class="fa fa-globe" target="_blank" href="" ></a>
                                    </p>
                                </div>
                                <h2 class="team_photo_name">Shourya Jain</h2>
                                <h3 class="team_photo_designation">Business Admin</h3>  
                            </div>
                        </div>

                    </div>
                </div>

                <div id="investor_wrap" class="">
                    <h1 class="section_heading">Angels</h1>
                    <div class="colored_line_pattern_home">
                        <p class="pattern_yellow_sec"></p><p class="pattern_red_sec"></p><p class="pattern_green_sec"></p>
                    </div>
                    <div class="clearfix"></div>
                    <h3 class="section_subheading">We <span class="fa fa-heart" style="color:#77CCCB;margin:5px;"></span> our investors</h3>
                    <div class="gallery_sec ">
                        <div class="team_photo_div">
                            <p class="team_photo_sec" ><a href="http://www.hiracoventures.com" target="_blank"> <img  class="team_photo_img desaturate" src="/img/investors/hiracoventures.png" alt="gridle's investor hiraco ventures" /> </a></p>
                            <p class="team_photo_name"></p>
                        </div>
                        <div class="team_photo_div">
                            <p class="team_photo_sec" > <a href="http://www.ciieindia.org" target="_blank"><img class="team_photo_img desaturate" src="/img/investors/ciie_logo.png" alt="gridle's investor CIIE" height="100px" /></a></p>
                            <p class="team_photo_name"></p>
                        </div>

                    </div>
                </div>

            </div> 
        </div>
    </div>
</div>
<script>
    $('.carousel').carousel({
        interval: 2000
    });
</script>

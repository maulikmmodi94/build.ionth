<?php
$this->layout = 'gridle_out';
$this->set('title_for_layout', 'Gridle | Terms of use and policies');

echo $this->Html->meta(array('name' => 'keywords', 'content' => 'project management, task management, collaboration tool, enterprise, teams, calendar, planner'));
echo $this->Html->meta(array('name' => 'description', 'content' => 'Gridle is a complete cloud visual collaboration and communication tool for your time, task and project management for enterprises and teams.'));
//robot meta tag
echo $this->Html->meta(array('name' => 'robots', 'content' => 'index, follow'));
//facebook open graph meta tags
echo $this->Html->meta(array('property' => 'og:title', 'content' => 'Gridle has simple policies, terms and conditions of use.'));
echo $this->Html->meta(array('property' => 'og:description', 'content' => 'Gridle is a complete cloud visual collaboration and communication tool for your time, task and project management for enterprises and teams.'));
echo $this->Html->meta(array('property' => 'og:type', 'content' => 'website'));
echo $this->Html->meta(array('property' => 'og:image', 'content' => 'http://gridle.io/img/logo/gridle_logo.png'));
echo $this->Html->meta(array('property' => 'og:image:url', 'content' => 'http://gridle.io/img/logo/gridle_logo.png'));
echo $this->Html->meta(array('property' => 'og:image:width', 'content' => '150'));
echo $this->Html->meta(array('property' => 'og:image:height', 'content' => '50'));
echo $this->Html->meta(array('property' => 'og:url', 'content' => 'http://gridle.io/pages/terms'));
echo $this->Html->meta(array('property' => 'og:site_name', 'content' => 'Gridle'));
echo $this->Html->meta(array('property' => 'fb:admins', 'content' => 'anupama.panchal'));
echo $this->Html->meta(array('property' => 'fb:app_id', 'content' => '642120029214138'));
//twitter meta tags
echo $this->Html->meta(array('name' => 'twitter:card', 'content' => 'summary'));
echo $this->Html->meta(array('name' => 'twitter:site', 'content' => '@gridle_io'));
echo $this->Html->meta(array('name' => 'twitter:image:src', 'content' => 'http://gridle.io/img/logo/gridle_logo.png'));
?>
<div class="g-page-wrap container-fluid">
<!--        terms of use           -->
<div class="row terms_wrap">
    <div class="col-lg-8 col-lg-offset-2">
        <h1 class="section_heading">Terms Of Use </h1>    
        <div class="colored_line_pattern_home">
            <p class="pattern_yellow_sec"></p>
            <p class="pattern_red_sec"></p>
            <p class="pattern_green_sec"></p>
        </div>
        <div class="clearfix"></div>
        <h3 class="section_subheading">Last updated on 1st Sept, 2014</h3> 
        <div class="policies_content">
            <p>This is an Agreement (hereinafter the Agreement, the Document), between Pivoting Softwares Pvt. 
                Ltd (hereafter referred to as Gridle, us, we, the Service and the Site) and you, the individual or legal 
                entity (hereinafter you, the User, member)</p>

            <p>By using Gridle, a Service of Pivoting Softwares Pvt. Ltd, you are agreeing to use the following Terms of 
                Use (Terms of Service). Please read them carefully.</p>

            <p>Pivoting Softwares Pvt. Ltd reserves the right to make amends in, update and change the Terms of Use 
                from time to time without prior notice. The amended Terms of Use shall be updated as and when amends 
                are made. This Agreement applies to all the users, whomsoever uses Gridle in either modes, paid and 
                otherwise.</p>

            <h2 class="policies_heading">USE OF OUR SERVICE</h2>

            <p>Gridle provides a platform for visual cloud collaboration, online.</p>

            <p>Gridle grants you the permission to use the Service it offers, provided you do not copy, distribute of disclose any part of it; you do not modify or reproduce the Service, in any manner and for any purpose and you comply with the Terms of Use.</p>

            <p> You will need to sign up and create a Member Account to use the Service. Your account gives you access to the services and functionality that we may establish and maintain from time to time and in our sole discretion. You shall never use another Member’s account without permission and for the sake of privacy, it is advisable to not allow any other person to use your account. You are required to provide all the information asked in an accurate and complete form. You are responsible for all the activity that happens through your account, hence we suggest you to keep your password secure and not share it with anyone under any circumstances. Gridle will not be liable for your losses caused by any unauthorized use of your account; you shall be liable for the losses of Gridle or others due to such unauthorized use.</p>

            <p>You agree not to use or launch any automated system, including without limitation, “robots,” “spiders,”, “offline readers,” etc., that accesses the Service in a manner that sends more request messages to the Gridle servers than a human can reasonably produce in the same period of time by using a conventional on-line web browser. You agree not to collect or harvest any personally identifiable information, including account names, from the Service nor to use the communication systems provided by the Service for any commercial solicitation purposes. You agree not to use any portion of the Service as a destination linked from any unsolicited bulk messages or unsolicited commercial messages.</p>

            <p>Gridle reserves the rights to temporarily or permanently suspend or otherwise refuse to permit you the use of the Service without notice and/or any liability. You are not allowed to interfere or compromise the system’s integrity or security. Uploading invalid data, viruses, bugs or any similar material through the Service is strictly forbidden. Also, upon termination for any reason you continue to be bound by this Agreement.</p>

            <h2 class="policies_heading">FEES+REFUND POLICY</h2>

            <p>By accepting the Terms of Use, you agree that Gridle may charge a fee for the use of any Service and 

            that, we will notify you of any such fee, before you incur it, and you agree to pay any fee, incurred by you. 

            In the event that you have elected to receive additional services for a fee, and you fail to pay such fee 

            within thirty (30) days, then in addition to all other remedies available to Gridle, Gridle may immediately 

            cease providing all such additional services. All fees are due and payable in advance with the first 

            monthly subscription fee due upon the registration of your account. Your service term will be renewed 

            automatically on a monthly, quarterly or annual basis depending upon whatever has been agreed by both 

            parties.</p>

            <p>
                All fees must be paid by credit card. By authorizing Gridle to charge a charge card or other credit card, 

            you are authorizing Gridle or a designated representative or agent to automatically continue charging 

            that card (or any replacement credit card account if the original card is renewed, lost, stolen, or changed 

            for any reason by the credit-issuing entity, and such entity informs Gridle of such new replacement card 

            account), financial account, or billing account for all fees and charges associated with the Services. If the 

            credit card information you provided is not valid, or if your credit card cannot be processed at the time 

            of the renewal charge, Gridle reserves the right to immediately terminate or suspend your access to the 

            Services, in the case of termination thereby terminating this Agreement and all of Gridle’s obligations 

            hereunder
            </p>

            <h2 class="policies_heading">CHANGING OR DISCONTINUING OUR SERVICE (FEATURES).</h2>

            <p>We are ever evolving and upgrading our features. We may add or remove them or stop providing them 

            altogether.</p>

            <p>You are free to stop using our service at anytime, however we’ll be sorry to see our valued user, leave.

            We are aware of the fact that you own your data and that preserving your access to the data is important. 

            So in case of discontinuation of a feature, wherever reasonably possible, we will provide you with a 

            reasonable advance notice and hence a chance to get information out of that feature.</p>

            <h2 class="policies_heading">PRIVACY AND SECURITY</h2>

            <p>We strive to protect your privacy and secure your information. Despite there being no such thing as 

            perfect online privacy/security, we will take all the reasonable steps to ensure the safety of your personal 

            information. However you understand and agree that these steps do not guarantee immunity from 

            unethical security breaches, viruses, security threats and other harmful vulnerabilities.</p>

            <h2 class="policies_heading">JURISDICTION</h2>

            <p>Gridle makes no guarantee that the content on the Site and/or the Service are appropriate or available or 

            appropriate for use in your country. Users or people, who visit and/or use the Site and/or the Service, do 

            it on their own free will and risk and are solely responsible for compliance with local laws. Gridle reserves 

            the right to, at anytime, limit or restrict the availability and accessibility of the Site and/or the Service to 

            any person, geographic area or jurisdiction and also to limit the quantities of any such feature that we 

            provide.</p>

            <h2 class="policies_heading">TERMINATION</h2>

            <p>This Agreement shall remain effective until terminated in accordance with its own terms and conditions. 

            You agree that Gridle, in its sole discretion, may terminate your password, account (in whole or in part), 

            or use of the Site or Services, and remove and discard any content within the Sites, at any time and for 

            any reason. You agree that any actions taken under this Section may be effective without prior notice 

            to you. YOU AGREE THAT WE WILL NOT BE LIABLE TO YOU OR ANY OTHER PARTY FOR ANY 

            TERMINATION OF YOUR ACCESS TO THE SITE OR SERVICES OR DELETION OF YOUR ACCOUNT 

            OR YOUR SUBMISSION MATERIALS. In the event of termination, however, those Sections in this 

            Agreement which, by their nature, survive termination of the Agreement shall so survive.</p>

            <h2 class="policies_heading">INFORMATION</h2>

            <p>This Agreement (including the Privacy Policy) constitutes the entire Agreement and understanding 

            between you and Gridle and governs your use of the Site and the Services, superseding any prior 

            Agreements between you and Gridle. This Agreement and the relationship between you and Gridle shall 

            be governed by and construed in accordance with the laws of India, without regard to its conflict of law 

            provisions. All disputes or jurisdictional matters are subject to jurisdiction of Ahmedabad, India. This 

            Agreement is not assignable, transferable or sub-licensable by you except with our prior written consent. 

            However, we may assign this Agreement to any third party whom we choose without your consent.</p>

            <h2 class="policies_heading">COMMUNICATIONS</h2>

            <p>Since our Service is on the internet, all the communication done between the user and Gridle will be 

            electronic. By agreeing to the Terms you consent to receive communications from us in an electronic form 

            and all the terms and conditions, Agreements, notices, Documents, disclosures and other information that 

            we provide to you electronically satisfy any legal requirements that such Communications would satisfy in 

            written form. Your consent to receive Communications and do business electronically, and our Agreement 

            to do so, applies to all of your interactions and transactions with us and this does not, in anyway, affect 

            your non-waivable rights. If you wish to stop receiving Communications electronically, you will have to, in 

            turn, stop using the Site and the Service. You may withdraw your consent by contacting us. Withdrawing 

            your consent will not affect the validity of the Communications, transactions and business done between 

            you and us, before your act of withdrawing the consent. You shall inform us of any effective change in 

            your e-mail address so that you continue to receive all Communications uninterrupted.</p>

            <h2 class="policies_heading">AGREEMENT MODIFICATIONS</h2>

            <p>All the modification and changes done to this Document will be visible on this page and you agree 

            to review the Agreement periodically to be aware of such changes. The Modified Agreement will be 

            indicated by “last updated on…” on the top of this page.</p>

            <h2 class="policies_heading">INDEMNIFICATION</h2>

            <p>In case any harm, claims, damages, losses, costs or expenses (including reasonable attorneys’ fees and 

            disbursements) arise directly or indirectly from your breach of this Agreements, any allegation that any 

            materials that you submit to Gridle, infringe or otherwise violate the copyright, trade secret, trademark 

            or other intellectual property rights, or any other rights of a third party, your access of the Site and/or 

            the Service, you agree to indemnify and defend Gridle and its subsidiaries, affiliates, officers, agents, 

            promoters, partners, employees, investors and representatives, harmless.</p>
        </div>

    </div>
</div>
</div>
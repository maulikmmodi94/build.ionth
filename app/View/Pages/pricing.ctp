<?php
$this->layout = 'gridle_out';
$this->set('title_for_layout', 'Build | Plans and pricing');

echo $this->Html->meta(array('name' => 'keywords', 'content' => 'project management, task management, collaboration tool, enterprise, teams, calendar, planner'));
echo $this->Html->meta(array('name' => 'description', 'content' => 'Instead of charging per user per month, unlike most project management tools, gridle has a very straight pricing plan. You pay for the amount of usage and can have unlimited users & teams.'));
//robot meta tag
echo $this->Html->meta(array('name' => 'robots', 'content' => 'index, follow'));
//facebook open graph meta tags
echo $this->Html->meta(array('property' => 'og:title', 'content' => 'Gridle has upfront and flat pricing. No hidden costs.'));
echo $this->Html->meta(array('property' => 'og:description', 'content' => 'Instead of charging per user per month, unlike most project management tools, gridle has a very straight pricing plan. You pay for the amount of usage and can have unlimited users & teams.'));
echo $this->Html->meta(array('property' => 'og:type', 'content' => 'website'));
echo $this->Html->meta(array('property' => 'og:image', 'content' => 'http://gridle.io/img/logo/gridle_logo.png'));
echo $this->Html->meta(array('property' => 'og:image:url', 'content' => 'http://gridle.io/img/logo/gridle_logo.png'));
echo $this->Html->meta(array('property' => 'og:image:width', 'content' => '150'));
echo $this->Html->meta(array('property' => 'og:image:height', 'content' => '50'));
echo $this->Html->meta(array('property' => 'og:url', 'content' => 'http://gridle.io/pages/pricing'));
echo $this->Html->meta(array('property' => 'og:site_name', 'content' => 'Gridle'));
echo $this->Html->meta(array('property' => 'fb:admins', 'content' => 'anupama.panchal'));
echo $this->Html->meta(array('property' => 'fb:app_id', 'content' => '642120029214138'));
//twitter meta tags
echo $this->Html->meta(array('name' => 'twitter:card', 'content' => 'summary'));
echo $this->Html->meta(array('name' => 'twitter:site', 'content' => '@gridle_io'));
echo $this->Html->meta(array('name' => 'twitter:image:src', 'content' => 'http://gridle.io/img/logo/gridle_logo.png'));
?>
<div class="g-page-wrap container-fluid">
    <!--    pricing plan section   -->
    <div id="pricing_plan_div" class="row">
        <h1 class="section_heading">Pricing & Plan</h1>

        <div class="colored_line_pattern_home">
            <p class="pattern_yellow_sec"></p><p class="pattern_red_sec"></p><p class="pattern_green_sec"></p>
        </div>
        <div class="clearfix"></div>

        <h3 class="section_subheading">Choose a plan and connect your team</h3>

        <div id="gridle_pricing-table" class="col-sm-10 col-sm-offset-1 col-xs-12">
            <div class="row">
                <div class="plan plan1 col-sm-3 col-xs-12">
                    <div class="header">Free
                        <p class="sub_header">For Individuals</p>
                    </div>
                    <div class="price">$0</div>  
                    <div class="monthly">per month</div>      
                    <ul>
                        <li><span><b>1 GB</b></span> </li>
                        <li><span><b>5 </b>People </span></li>
                        <li><b>Unlimited</b> Teams</li>
                    </ul>
                    <!--<a class="signup" href="/users/invite">Get Started</a>-->         
                </div>
                <div class="plan plan2 col-sm-3 col-xs-12 ">
                    <div class="header">Small

                        <p class="sub_header">For Small Enterprises</p></div>
                    <div class="price">$50</div>
                    <div class="monthly">per month</div>  
                    <ul>
                        <li><b>20GB</b> File space</li>
                        <li><b>Unlimited</b> People</li>	
                        <li><b>Unlimited</b> Teams</li>
                    </ul>
                    <!--<a class="signup" href="/users/invite">Get Started</a>-->            
                </div>
                <div class="plan plan3 popular-plan col-sm-3 col-xs-12">
                    <div class="header">Medium<p class="sub_header">For Medium Enterprises</p></div>
                    <div class="price">$80</div>
                    <div class="monthly">per month</div>
                    <ul>
                        <li><b>50GB</b> File space</li>
                        <li><b>Unlimited</b> People</li>
                        <li><b>Unlimited</b> Teams</li>
                    </ul>
                    <!--<a class="signup" href="/users/invite">Get Started</a>-->        
                </div>
                <div class="plan plan4 col-sm-3 col-xs-12">
                    <div class="header">Large
                        <p class="sub_header">For Large Enterprises</p></div>
                    <div class="price">$100</div>
                    <div class="monthly">per month</div>
                    <ul>
                        <li><b>100GB</b> File space</li>
                        <li><b>Unlimited</b> People</li>
                        <li><b>Unlimited</b> Teams</li>
                    </ul>
                    <!--<a class="signup" href="/users/invite">Get Started</a>-->        
                </div> 	
            </div>
        </div>
        <div id="section1_bottom" style=''>
            <p><a class="btn btn-primary btn-lg homepage_btns" href="/signup">Sign up now for free</a></p>
            <p><a>Take a tour</a> | <a href="/pages/benefits">Know more</a></p>
        </div>  
    </div>

    <div id="pricing_faq_div" class="row ">
        <div class="col-lg-8 col-lg-offset-2">
            <h1 class="section_heading">Have Questions?</h1>
            <div class="colored_line_pattern_home">
                <p class="pattern_yellow_sec"></p><p class="pattern_red_sec"></p><p class="pattern_green_sec"></p>
            </div>
            <div class="clearfix"></div>

            <h3 class="section_subheading">Still in doubt? Get in touch with us. Drop us a line on <a href="mailto:sales@gridle.io">sales@gridle.io</a></h3>

            <div id="faq_wrap">

                <div class="faq_div">
                    <h2 class="faq_question">Is credit card required to sign up?</h2>
                    <p class="faq_answer">Absolutely not. The first month is a no obligation trial. You can sign up and use the full set of features for the first month. You only pay us if you like it. You will also have access to our support. It will be just like you are paying for it.</p>
                </div>

                <div class="faq_div">
                    <h2 class="faq_question">Do you have an on-site version?</h2>
                    <p class="faq_answer">We are really passionate about the web. Our mission is to enable people to work from anywhere at anytime from any device. We provide a full set of security features, have a 99.9% up-time and back up your data daily so the only time you lose something on gridle would be during the end of the world.</p>
                </div>

                <div class="faq_div">
                    <h2 class="faq_question">What if I need more storage space?</h2>
                    <p class="faq_answer">You can get in touch with us here or drop us a line regarding your requirements. There is no 'IT guy' required to configure things, solve bugs or set things up and running. Our software is flexible enough to incorporate customizations and we love to hear and learn from our customers.</p>
                </div>

                <div class="faq_div">
                    <h2 class="faq_question">Are there any discounts available?</h2>
                    <p class="faq_answer">We offer discounts to not-for-profit. There is documentation required for that though. If you are not a non profit organization but still think that you are eligible for discount, please drop us aline here and we would be happy to talk to you.</p>
                </div>

            </div>
        </div>
    </div>


    <!-- below signup section on each page  -->
    <div id="signup-btn-section" class="row">
        <div class="section_column_testimonial row">
            <div class="testimonial_user_img_div col-sm-2 col-xs-12">
                <img class="testimonial_user_img center-block" src="/img/homepage/gridle_chiranjay_shah_testimonial.jpg" alt="gridle's testimonial by chiranjay shah of quivcoal technologies pvt. ltd." style="width:100%;"/>
            </div>
            <div class="testimonial_text col-sm-8 col-xs-12">
                <p class="testimonial_user_line">
                    <span class="fa fa-quote-left"></span>&nbsp;&nbsp;Since there were tours, there was no agenda or tutorial document to be read and understood. Using gridle was about getting things done from the start&nbsp;&nbsp; <span class="fa fa-quote-right"></span>
                </p>
                <p class="testimonial_username">- Chiranjay Shah</p>
            </div>
        </div>
        <div class="signup_section home_section" >
            <p><a class="btn btn-lg btn-primary btn-responsive homepage_btns" href="/users/add" >Sign up now for free</a></p>
            <p><a class="signup_section_links" href="/pages/features">Take a tour</a> &nbsp; <span class="signup_section_links">|</span> &nbsp; <a class="signup_section_links" href="/pages/benefits">Know more</a></p>
        </div> 
    </div>
    <!--        signup section end          -->
</div>
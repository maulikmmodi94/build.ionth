<?php
$this->layout = 'gridle_out';
$this->set('title_for_layout', 'Sign up | Gridle');
?>
<div class="g-page-wrap container-fluid"  ng-cloak >
    <div class="g-user-reg-wrap row">
        <div class="col-lg-6 col-lg-offset-3 col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1 col-xs-12  signup-section padding-lr-0 font-small-1" ng-controller="googleController">
            <div class="signup-heading">
                <h1 class="form-heading" >Get Started for Free</h1>
            </div>
            <div class="form-signup-area">
                <h4 class="form-subheading">Signup in 30 seconds. No credit card required.</h4>

                <!--    FORM        -->
                <?php
                echo $this->Form->create($model, array('type' => 'file', 'class' => 'form-signup form-form', 'novalidate', 'role' => 'form', 'name' => 'formSignUp', 'inputDefaults' => array('label' => false,)));
                ?>
                <div class="row">
                    <!--    FIRST NAME      -->
                    <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12 ">
                        <?php
                        echo $this->Form->hidden('User.timezone', array('hiddenField' => false, 'type' => 'number'));
                        $this->Form->unlockField('User.timezone');
                        echo $this->Form->input('UserInfo.first_name', array('type' => 'text', 'class' => 'form-control input-sm', 'placeholder' => 'First name', 'div' => FALSE, 'required', 'ng-minlength' => 2, 'ng-maxlength' => 30, 'ng-pattern' => '/^[a-z. ]*$/i', 'maxlength' => 30, 'ng-model' => 'data.UserInfo.first_name', 'autofocus'));
                        ?>
                        <div class="form-inline-error text-warning" ng-show="formSignUp['data[UserInfo][first_name]'].$dirty">
                            <p ng-show="formSignUp['data[UserInfo][first_name]'].$error.required" class="p-margin-0">Please fill your first name</p>
                            <p ng-show="formSignUp['data[UserInfo][first_name]'].$error.minlength" class="p-margin-0">Minimum 2 carachters are required.</p>
                            <p ng-show="formSignUp['data[UserInfo][first_name]'].$error.maxlength" class="p-margin-0">First name too long.</p>
                            <p ng-show="formSignUp['data[UserInfo][first_name]'].$error.pattern" class="p-margin-0">You can use only alphabets and spaces. </p>
                        </div>
                    </div>
                    <!--    LAST NAME      -->
                    <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <?php
                        echo $this->Form->input('UserInfo.last_name', array('type' => 'text', 'class' => 'form-control input-sm', 'placeholder' => 'Last name', 'div' => FALSE, 'required', 'ng-minlength' => 2, 'ng-maxlength' => 30, 'ng-pattern' => '/^[a-z. ]*$/i', 'maxlength' => 30, 'ng-model' => 'data.UserInfo.last_name'));
                        ?>
                        <div class="form-inline-error text-warning" ng-show="formSignUp['data[UserInfo][last_name]'].$dirty">
                            <p ng-show="formSignUp['data[UserInfo][last_name]'].$error.required" class="p-margin-0">Please fill your last name</p>
                            <p ng-show="formSignUp['data[UserInfo][last_name]'].$error.minlength" class="p-margin-0">Minimum 2 carachters are required.</p>
                            <p ng-show="formSignUp['data[UserInfo][last_name]'].$error.maxlength" class="p-margin-0">Last name too long.</p>
                            <p ng-show="formSignUp['data[UserInfo][last_name]'].$error.pattern" class="p-margin-0">You can use only alphabets and spaces. </p>
                        </div>
                    </div>
                </div>
                <?php
                $email = isset($this->request->data['User']['email']) ? $this->request->data['User']['email'] : null;
                ?>
                <!--    EMAIL   ADDRESS     -->
                <div class="form-group" ng-init="data.User.email = '<?= $email; ?>'">
                    <?php
                    if ($email) {

                        echo $this->Form->input('email', array('readonly', 'error' => array('isValid' => __d('users', 'Must be a valid email address'), 'isUnique' => __d('users', 'An account with that email already exists')), 'class' => 'form-control input-sm', 'placeholder' => 'Email address', 'div' => FALSE, 'ng-pattern' => '/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/', 'ng-model' => 'data.User.email', 'required'));
                    } else {
                        echo $this->Form->input('email', array('error' => array('isValid' => __d('users', 'Must be a valid email address'), 'isUnique' => __d('users', 'An account with that email already exists')), 'class' => 'form-control input-sm', 'placeholder' => 'Email address', 'div' => FALSE, 'ng-pattern' => '/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/', 'ng-model' => 'data.User.email', 'required'));
                    }
                    ?>
                    <div ng-show="formSignUp['data[User][email]'].$dirty" class="form-inline-error text-warning" >
                        <p ng-show="formSignUp['data[User][email]'].$error.required" class="p-margin-0" >Please fill your email address.</p>
                        <p ng-show="formSignUp['data[User][email]'].$error.pattern" class="p-margin-0">Write your email address</p>
                    </div>
                </div>
                <!--    PASSWORD        -->

                <div class="form-group">
                    <div class="input-group input-group-sm" ng-hide="showpassword">
                        <?php echo $this->Form->input('password', array('type' => 'password', 'class' => 'form-control', 'placeholder' => 'Password', 'div' => FALSE, 'required', 'ng-minlength' => 6, 'ng-model' => 'data.User.password', 'ng-hide' => 'showpassword')); ?>
                        <div class="input-group-addon font-small-1 hidden-xs ">
                            <label class="p-margin-0" style="font-weight:normal">
                                <input type="checkbox" ng-model="showpassword" ng-checked="false" >&nbsp;&nbsp;Show</label>
                        </div>
                    </div>
                    <div class="input-group input-group-sm" ng-show="showpassword">
                        <input class="form-control"  type="text" ng-model="data.User.password" ng-show="showpassword" placeholder="Password" />
                        <div class="input-group-addon font-small-1 " >
                            <label class="p-margin-0" style="font-weight:normal">
                                <input type="checkbox" ng-model="showpassword" ng-checked="false" >&nbsp;&nbsp;Show</label>
                        </div>
                    </div>
                    <div ng-show="formSignUp['data[User][password]'].$dirty" class="form-inline-error text-warning" >
                        <p ng-show="formSignUp['data[User][password]'].$error.required" class="p-margin-0" >Please fill your password.</p>
                        <p ng-show="formSignUp['data[User][password]'].$error.minlength" class="p-margin-0">Minimum 6 characters are required.</p>
                    </div>
                </div>
                <?php
                echo '<div class="row">';
                $tosLink = $this->Html->link(__d('users', 'Policy & Terms of Use'), array('controller' => 'pages', 'action' => 'terms', 'plugin' => null), array('target' => "_blank"));
                echo '<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">';
//                echo '<input type="button" class="btn btn-danger btn-block" ng-click="login()" value="Signup with Google+"/>';
//                echo '<img src="/img/google/google-button.png" alt="google" style="cursor:pointer"ng-click="login()" width="135px" height="42px">';
                echo '</div>';
                echo $this->Form->submit(__d('users', 'Sign me up'), array('class' => 'btn btn-primary btn-block', 'div' => array('class' => 'col-xs-12'), 'ng-disabled' => 'formSignUp.$invalid'));
                echo '</div>';
                echo '<div style="padding-top:15px;">';
                echo '<p class="font-big-1 text-center">OR</p>';
                echo '<input type="image" src="/img/google/google-button.png" alt="google" style="cursor:pointer;padding-left:48px;" ng-click="login()" id="googlebutton">';
                echo '</div>';
                ?>
                <?php
                echo $this->Form->end();
                ?> 
            </div>
            <div class="col-sm-offset-3">
                <p class="font-small-1 text-muted">By signing up, you agree to Gridle's 
                    <a href="/pages/terms" target="_blank">Policy &amp; Terms of Use</a>
                </p>
            </div>
            <div class="signup-sublinks">
                <p class='text-muted text-center font-small-1'>Already a member? <a href='/users/login'>Login here</a>.</p>
            </div>
        </div>
    </div>
    <!--<div class='row' style='margin-top: 50px;'>-->
        <!--<div class='col-lg-3 col-lg-offset-4'><img class='center-block'  src='/img/media/next_big_what.png' /></div>-->
        <!--<div class='col-lg-4'><p class='text-muted ' >Gridle is one stop shop for all communication needs of teams & enterprises.</p></div>-->
    <!--</div>-->
</div>
<script type="text/javascript">
//Gets User's Timezone
    var d = new Date();
    var offset = d.getTimezoneOffset();
    document.getElementById("UserTimezone").value = offset;
    $("#googlebutton").click(function(event)
  {
    event.preventDefault(); // cancel default behavior

    
  });
</script>
 
<script type="text/javascript">
      (function() {
       var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
       po.src = 'https://apis.google.com/js/client.js?onload=onLoadCallback';
       var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
     })();
</script>

<?php
$this->layout = 'gridle_out';
$this->set('title_for_layout', 'gridle | Priority Support');

echo $this->Html->meta(array('name' => 'keywords', 'content' => 'project management, task management, collaboration tool, enterprise, teams, calendar, planner'));
echo $this->Html->meta(array('name' => 'description', 'content' => 'Gridle is a complete cloud visual collaboration and communication tool for your time, task and project management for enterprises and teams.'));
//robot meta tag
echo $this->Html->meta(array('name' => 'robots', 'content' => 'index, follow'));
//facebook open graph meta tags
echo $this->Html->meta(array('property' => 'og:title', 'content' => 'Gridle extends priority support to all its customers'));
echo $this->Html->meta(array('property' => 'og:description', 'content' => 'We are here to serve. We serve the community of people who work together on managing projects and teams. In case of enquiries, suggestions or issues, ask us to serve and we wont let you down.'));
echo $this->Html->meta(array('property' => 'og:type', 'content' => 'website'));
echo $this->Html->meta(array('property' => 'og:image', 'content' => 'http://gridle.io/img/homepage/gridle_support.png'));
echo $this->Html->meta(array('property' => 'og:image:url', 'content' => 'http://gridle.io/img/homepage/gridle_support.png'));
echo $this->Html->meta(array('property' => 'og:image:width', 'content' => '100'));
echo $this->Html->meta(array('property' => 'og:image:height', 'content' => '200'));
echo $this->Html->meta(array('property' => 'og:url', 'content' => 'http://gridle.io/pages/support'));
echo $this->Html->meta(array('property' => 'og:site_name', 'content' => 'Gridle'));
echo $this->Html->meta(array('property' => 'fb:admins', 'content' => 'anupama.panchal'));
echo $this->Html->meta(array('property' => 'fb:app_id', 'content' => '642120029214138'));
//twitter meta tags
echo $this->Html->meta(array('name' => 'twitter:card', 'content' => 'summary'));
echo $this->Html->meta(array('name' => 'twitter:site', 'content' => '@gridle_io'));
echo $this->Html->meta(array('name' => 'twitter:image:src', 'content' => 'http://gridle.io/img/homepage/gridle_support.png'));
?>
<div id="support_wrap" class="g-page-wrap container-fluid">
    <div id="support_div" class="row">
        <h1 id="section_heading" class="section_heading">We are here to serve!</h1>
        <div class="colored_line_pattern_home">
            <p class="pattern_yellow_sec"></p><p class="pattern_red_sec"></p><p class="pattern_green_sec"></p>
        </div>
        <div class="clearfix"></div>
        <h3 id="section_subheading" class="section_subheading">Ask questions, discuss with us or discover ways to use Gridle like never before.</h3>    
        <div id="form_message" class="row"> 
            <p class="col-lg-6 col-lg-offset-3 col-md-offset-3 col-md-6">Ask questions, discuss with us or discover </p>
        </div>
        <div class="row support_form_parent">
            <div class="col-lg-2 col-lg-offset-1 col-md-3 ">
                <img src="/img/homepage/gridle_support.png" alt="gridle support" height="250" class="center-block"/>
            </div>
<!--            <div id="support_form_div" class="col-sm-6">
                <script type="text/javascript" src="https://s3.amazonaws.com/assets.freshdesk.com/widget/freshwidget.js"></script>
                <style type="text/css" media="screen, projection">
                    @import url(https://s3.amazonaws.com/assets.freshdesk.com/widget/freshwidget.css); 
                </style> 
                <iframe class="freshwidget-embedded-form" id="freshwidget-embedded-form" src="https://gridle.freshdesk.com/widgets/feedback_widget/new?&widgetType=embedded&screenshot=no&searchArea=no" scrolling="no" height="500px" width="100%" frameborder="0" >
                </iframe>
            </div>-->
        </div>
        <div class="clearfix"></div>
        <div id="support_bottom" class="row">
            <p>For <span class="content_highlight">blazing replies</span>, message us on <a target="_blank" href="https://www.facebook.com/gridle.in">facebook</a> or <a href="https://twitter.com/gridle_io" target="_blank">tweet us @gridle_io</a> </p>
            <p>
                <a href="https://twitter.com/intent/tweet?screen_name=gridle_io" class="twitter-mention-button" data-size="large" data-related="anupamapanchal">Tweet to @gridle_io</a>
                <script>!function(d, s, id) {
                        var js, fjs = d.getElementsByTagName(s)[0], p = /^http:/.test(d.location) ? 'http' : 'https';
                        if (!d.getElementById(id)) {
                            js = d.createElement(s);
                            js.id = id;
                            js.src = p + '://platform.twitter.com/widgets.js';
                            fjs.parentNode.insertBefore(js, fjs);
                        }
                    }(document, 'script', 'twitter-wjs');
                </script>
            </p>
        </div>
    </div>
</div>
<script>
    var loggedin = <?php echo ($this->Session->read('Auth.User') != NULL) ? 1 : 0; ?>;
</script>

<?php
echo $this->Html->script('jquery.validate.js', array('block' => 'scriptBottom'));
echo $this->Html->scriptBlock("
    $(\"#support_form\").validate({
        focusInvalid: true,
        submitHandler: function(form) {
            submit();
        },
        rules: {
            'data[Support][email_id]': {
                required: true,
                email: true
            },
            'data[Support][type]': {
                required: true,
                min: 0,
                max: 6
            },
            'data[Support][description]': {
                required: true,
                minlength: 1,
                maxlength: 999
            }
        },
        invalidHandler: function(event, validator) {
            var errors = validator.numberOfInvalids();
            if (errors) {
                var message = errors == 1
                        ? 'You missed 1 field. It has been highlighted'
                        : 'You missed ' + errors + ' fields. They have been highlighted';
                //console.log(message);
            } else {
                //$(\"div.error\").hide();
            }
        },
        messages: {
            'data[Support][email_id]': {
                required: \"We need your email address to contact you\",
                email: \"Your email address must be in the format of name@domain.com\"
            },
            'data[Support][type]': {
                required: \"Select an option\",
                min: \"Select an appropriate value\",
                max: \"Select an appropriate value\",
            },
            'data[Support][description]': {
                required: 'We need a description of problem/suggestion to help you',
                minlength: 'Minimum length of description is 1',
                maxlength: 'Maximum length of description is 999',
            }
        }
    });

    function submit() {
        var data = new Object();
if (!loggedin) {
            data['email_id'] = document.getElementById('support_email').value;
        }        
data['type'] = document.getElementById('support_type').value;
        data['description'] = document.getElementById('support_description').value;
        $('#support-btn').attr(\"disabled\", true);
        $('#support-btn').html('<i class=\"fa fa-spinner fa-spin\"></i> Sending');
        $.ajax({
            url: \"/supports/add/\",
            type: \"POST\",
            data: data,
            success: function(data) {
                data = JSON.parse(data);
                if (data.status === 'success')
                {
                    $('#support-btn').html('Submit');
                    $('#support-btn').removeAttr(\"disabled\");
                    $('#support_form').trigger('reset');
                    $('#form_message').css('opacity','1');
                    $('#form_message p').addClass('bg-success');
                    $('#form_message p').html(\"Your Ticket has been raised. We will respond to you within 2 business days.\");
                } else {
                    $('#support-btn').removeAttr(\"disabled\"); 
                    $('#support-btn').html(\"Solve\");
                    $('label.errorVal').css('display', 'inline-block');
                    $('label.errorVal').css('color', 'red');
                    $('label.errorVal').html(data.data);                    
                }
            },
            error: function() {
                $('#support-btn').removeAttr(\"disabled\"); 
                $('#support-btn').html(\"Solve\");
                $('label.errorVal').css('display', 'inline-block');
                $('label.errorVal').css('color', 'red');
                $('label.errorVal').html(\"You request could not be completed\");
            },
            complete: function() {
                
            }
        });
    }", array('block' => 'scriptBottom'));
?>

<?php
$this->layout = 'gridle_out_new';
$this->set('title_for_layout', 'Manifesto | gridle ');
$brand = Configure::read('brand');
?>

<link rel="stylesheet" type="text/css" href="/css/terms.css">
<div class="position-relative">
	<div class="container-fluid terms-header">
		<div class="row">
				<div class= "col-sm-offset-2 col-sm-10">				
					<h1 class="terms-heading">Manifesto</h1>
					<p class="terms-sub-heading">What are we doing here!</p>
				</div>
		</div>
	</div>
	<div class="container-fluid  background-white" >
	<div class="row" >
	<div class="col-sm-offset-2 col-sm-7">
	<div class="terms-content-manifesto margin-tb-fifty">
		<div id="">
			<h1 class="terms-content-heading"></h1>
		<p>
		The question is &quot;Why?&quot; All we wanted was to get work done but between Skype, Dropbox, Whatsapp and email, it was scattered, lost and not clear enough. Work should be about doing stuff and not about finding stuff to get done. 

So, we tried tools out there. Loud, heavy, could do everything but in process, became inevitably intimidating; distracting.

The first thing we did after 5 months of losing track of our work and deciding to build Gridle was drafting this page out. Our guiding principles, our Bible.

		</p>
<!--		<center><img src="img.jpg" width="600px" height="400px"/></center>-->
		<hr class="terms-hr"/>
		</div>
		<div id="Design_over_functionality">
			<h1 class="terms-content-heading">Design over functionality</h1>
			<p>
				If we are going to build something that is going to be the home for your work, it has to be elegant, simple and complete. Something like your favourite water-bottle. Right color, right weight and holds just enough water to quench your thirst after your morning jog.
			</p>
			<hr class="terms-hr"/>
		</div>
		<div id="Building_useful">
			<h1 class="terms-content-heading">Building useful</h1>
			<p>
				Usefulness is always better than innovation and personality comes from building something that people like to use, not from something that takes them by surprise one minute and is forgotten about, a week later. Having said that, we never understood how fashion shows work!
			</p>
			<hr class="terms-hr"/>
		</div>
		<div id="Relationships_are_paramount">
			<h1 class="terms-content-heading">Relationships are paramount</h1>
			<p>
				We don&#39;t want us to be the company and you to be our customers. We look at both of us as an ecosystem, a community that will grow together. A community is humane; it supports, fights, understands,loves and builds, together.
			</p>
			<hr class="terms-hr"/>
		</div>
		<div id="Respect_time">
			<h1 class="terms-content-heading">Respect time</h1>
			<p>
				We can always have more of everything. Friends, money, speed of our internet connections etc. but the one thing we can&#39;t have more of, is time. We will respect it. We don&#39;t do it enough. Responses, support, bug fixes and feedback will be done fast. Like, really fast.
			</p>
			<hr class="terms-hr"/>
		</div>
		<div id="">
			<h1 class="terms-content-heading"></h1>
			<p>
				<center>Everyday we will move towards building something that lasts. We want to feel good at the end of the day.</center>

			</p>
			<hr class="terms-hr"/>
		</div>
	</div>
	</div>
	<div class="col-sm-3 ">
		<div class="terms-autoscroll">
		<ul class="nav nav-pills nav-stacked" role="tablist">
			<li><a href="#Design_over_functionality" class="">Design over functionality</a></li>
			<li><a href="#Building_useful" class="">Building useful</a></li>
			<li><a href="#Relationships_are_paramount" class="">Relationships are paramount</a></li>
			<li><a href="#Respect_time" class="">Respect time</a></li>
			<li><a href="#" class=""></a></li>
		</ul>
	</div>
	</div>
	</div>
	</div>
</div>
<?php
$this->layout = 'gridle_out';
$this->set('title_for_layout', 'Case studies | Enterprises choose gridle over other tools');

echo $this->Html->meta(array('name' => 'keywords', 'content' => 'project management, task management, collaboration tool, enterprise, teams, calendar, planner'));
echo $this->Html->meta(array('name' => 'description', 'content' => 'Gridle is a complete cloud visual collaboration and communication tool for your time, task and project management for enterprises and teams.'));
//robot meta tag
echo $this->Html->meta(array('name' => 'robots', 'content' => 'index, follow'));
//facebook open graph meta tags
echo $this->Html->meta(array('property' => 'og:title', 'content' => 'gridle | visual cloud collaboration tool for enterprises and teams'));
echo $this->Html->meta(array('property' => 'og:description', 'content' => 'Gridle is a complete cloud visual collaboration and communication tool for your time, task and project management for enterprises and teams.'));
echo $this->Html->meta(array('property' => 'og:type', 'content' => 'website'));
echo $this->Html->meta(array('property' => 'og:image', 'content' => 'http://gridle.io/img/homepage/gridle_kavina_patel_testimonial.jpg'));
echo $this->Html->meta(array('property' => 'og:image:url', 'content' => 'http://gridle.io/img/homepage/gridle_kavina_patel_testimonial.jpg'));
echo $this->Html->meta(array('property' => 'og:image:width', 'content' => '280'));
echo $this->Html->meta(array('property' => 'og:image:height', 'content' => '280'));
echo $this->Html->meta(array('property' => 'og:url', 'content' => 'http://gridle.io/pages/case-studies'));
echo $this->Html->meta(array('property' => 'og:site_name', 'content' => 'Gridle'));
echo $this->Html->meta(array('property' => 'fb:admins', 'content' => 'anupama.panchal'));
echo $this->Html->meta(array('property' => 'fb:app_id', 'content' => '642120029214138'));
//twitter meta tags
echo $this->Html->meta(array('name' => 'twitter:card', 'content' => 'summary'));
echo $this->Html->meta(array('name' => 'twitter:site', 'content' => '@gridle_io'));
?>
<div class="g-page-wrap container-fluid">
<!--        home page wrapper       -->
<div id="landing_section" class="row">
    <div id="landing_div">
        <img id="landing_image" src="/img/homepage/gridle_quivocal_technologies_team.jpg" alt="Quivocal Technologies team on Gridle"/>
        <div id="landing_para">
            <p id="landing_para_content">Try using gridle with your colleagues. It is an intuitive <span class="content_highlight">project management</span> & <span class="content_highlight">time tracking</span> software for <span class="content_highlight">smart teams</span>. Check out how we used gridle to <span class="content_highlight">manage</span> our time, tasks, files and communication between and outside of our team.</p>
            <p id="landing_para_user">- Kavina, <span class="landing_para_user_highlight" style=""><i>CEO-Quivocal</i></span></p>
        </div>
        <div id="landing_photo_credits" class="hidden-xs">
            Photo courtesy: Quivocal Technologies
        </div>
    </div>
</div>


<!--        Section 1 (Kavina making team)           -->
<div id="testimonial_section1" class="home_section container section-offwhite">

    <h1 class="section_heading">
        Here's how Quivocal used gridle..
    </h1>

    <div class="colored_line_pattern_home">
        <p class="pattern_yellow_sec"></p><p class="pattern_red_sec"></p><p class="pattern_green_sec"></p>
    </div>
    <div class="clearfix"></div>
    <h3 class="section_subheading">Kavina Patel, their project lead, read about gridle in <a href="http://www.business-standard.com/article/companies/collaboration-made-easy-by-gridle-platform-113122300056_1.html" target="_blank">Business Standard.</a></h3>
    <div id="testimonial_section1_subsection" class="section_subsection row">
        <div id="testimonial_section1_left" class="col-lg-4 col-lg-offset-2 col-md-6 col-sm-6 col-xs-12">
            <div class="section_column_description">
                <p>She signed up and invited their core team of 4 on the platform. Initially, they used it to manage their personal time and official tasks. Their project happened over <span class="content_highlight">a period of 8 months, involved their team, field experts, advisors, govt. officials and free-lancers.</span> And all of them did not start using gridle at once. </p>
            </div>
        </div>
        <div id="testomonial_section1_right" class="col-lg-5 col-md-6 col-sm-6 col-xs-12">
            <p class="testimonial_screenshot" style="text-align: center; display: inline-block;">
                <img src="/img/homepage/gridle_add_team_screen.png" alt='Add team screen on gridle' width="70%" style="margin:5px;">
            </p>    
        </div>
    </div>
    <div class="section_column_testimonial row">
        <div class="testimonial_user_img_div col-lg-2 col-md-2 col-sm-2 col-xs-12">
            <img class="testimonial_user_img center-block" src="/img/homepage/gridle_chiranjay_shah_testimonial.jpg" alt="gridle's testimonial by chiranjay shah of quivcoal technologies pvt. ltd." style="width:100%;"/>
        </div>
        <div class="testimonial_text col-lg-8 col-md-9 col-sm-9 col-xs-12">
            <p class="testimonial_user_line"><span class="fa fa-quote-left"></span>&nbsp;&nbsp;Since there were tours, there was no agenda or tutorial document to be read and understood. Using gridle was about getting things done from the start&nbsp;&nbsp; <span class="fa fa-quote-right"></span></p>
            <p class="testimonial_username">- Chiranjay Shah</p>
        </div>
        <p></p>
    </div>
</div>
<!--        Section 2 (Task management + Shaival)           -->
<div id="testimonial_section2" class="container home_section section-white">  
    <h1 class="section_heading">
        They did not need a 'status email'..
    </h1>
    <div class="colored_line_pattern_home">
        <p class="pattern_yellow_sec"></p><p class="pattern_red_sec"></p><p class="pattern_green_sec"></p>
    </div>
    <div class="clearfix"></div>
    <h3 class="section_subheading">They could manage their personal and team tasks, time and everything in between with plannar.</h3>

    <div id="testimonial_section2_subsection" class="row section_subsection">

        <div id="testimonial_section2_left" class="col-lg-5 col-lg-offset-1 col-md-5 col-md-offset-1 col-sm-6 col-xs-12">
            <p>
                <object class="center-block" data="/img/homepage/gridle_task_management_screen.svg" type="image/svg+xml" width="100%">
                    <img src='/img/homepage/gridle_task_management_screen.jpg' alt='gridle task management screens'/>
                </object>
            </p>    
        </div>

        <div id="testimonial_section2_right" class="section_column col-lg-4 col-md-4 col-sm-6 col-xs-12" style=""> 
            <div class="section_column_description" style="">
                <p>They did not have to write any more update mails since the complete team was on the same page with <span class="content_highlight">interactive time-line and plannar.</span> They assigned tasks to each other and were in complete knowledge when assignee completed the tasks. They could sort their personal To-dos and got a bird-eye view and alerts on <span class="content_highlight">events, meetings and activities.</span></p>
            </div>

        </div>
    </div>
    <div class="section_column_testimonial row">
        <div class="testimonial_user_img_div col-lg-2 col-md-3 col-sm-2 col-xs-12">
            <img class="testimonial_user_img center-block" src="/img/homepage/gridle_shaival_gajjar_testimonial.jpg" alt="gridle's testimonial by Shaival Gajjar, lead design of quivocal technologies pvt. ltd." style="width:100%;"/>
        </div>
        <div class="testimonial_text col-lg-8 col-md-9 col-sm-9 col-xs-12">
            <p class="testimonial_user_line"><span class="fa fa-quote-left"></span>&nbsp;&nbsp; It took us atleast 4 threads of mail to set up a meeting before! this reduced it to creating 1 activity.&nbsp;&nbsp; <span class="fa fa-quote-right"></span></p>
            <p class="testimonial_username">-Shaival Gajjar</p>
        </div>
        <p></p>
    </div>
</div>

<!--        Section three  (instant messaging + jaydip )          -->
<div id="testimonial_section3" class="container home_section section-offwhite" >

    <h1 class="section_heading">
        They could chat from anywhere..
    </h1>

    <div class="colored_line_pattern_home">
        <p class="pattern_yellow_sec"></p><p class="pattern_red_sec"></p><p class="pattern_green_sec"></p>
    </div>
    <div class="clearfix"></div>
    <h3 class="section_subheading">They were available and together all the time for knowledge sharing and updates with recipients sorted and messaged instantly.</h3>
    <div id="testimonial_section3_subsection" class="section_subsection row">
        <div id="testimonial_section3_left" class="col-lg-4 col-lg-offset-2 col-md-4 col-md-offset-2 col-sm-6 col-xs-12">
            <div class="section_column_description">
                <p>While Kavina would talk to multiple people and make presentations, she was required to <span class="content_highlight">update the complete team</span> and discuss from time to time. They would chat, share interesting links and contact information. Jaydip meanwhile <span class="content_highlight">created his own team</span> of tech guys and used this to get updates and discuss things on hardware end.</p>
            </div>
        </div>

        <div id="testimonial_section3_right" class="section_column col-lg-5 col-md-5 col-sm-6 col-xs-12" >
            <p class="testimonial_screenshot" style="text-align: center; max-width: 200px;">
                <img class="center-block" src='/img/homepage/gridle_quivocal_using_conversation.png' alt='' width="100%"/>
            </p>    
        </div>
    </div>
    <div class="section_column_testimonial row">
        <div class="testimonial_user_img_div col-lg-2 col-md-2 col-sm-2 col-xs-12">
            <img class="testimonial_user_img center-block" src="/img/homepage/gridle_jaydip_biniwale_testimonial.jpg" alt="gridle's testimonial by Jaydip Biniwale, CTO of Quivocal technologies pvt. ltd." style="width:100%;"/>
        </div>
        <div class="testimonial_text col-lg-8 col-md-9 col-sm-10 col-xs-12">
            <p class="testimonial_user_line"><span class="fa fa-quote-left"></span>&nbsp;&nbsp; One cannot chat everything regarding hardware. Here, we could directly make skype calls and hold virtual meeting from inside of gridle. &nbsp;&nbsp; <span class="fa fa-quote-right"></p>
            <p class="testimonial_username">-Jaydip Biniwale</p>
        </div>
    </div>
</div>
<!--        Section four  (asset management)       -->
<div id="testimonial_section4" class="container home_section section-white">
    <h1 class="section_heading" >
        They created and shared files without worries
    </h1>

    <div class="colored_line_pattern_home">
        <p class="pattern_yellow_sec"></p><p class="pattern_red_sec"></p><p class="pattern_green_sec"></p>
    </div>
    <div class="clearfix"></div>
    <h3 class="section_subheading">All files were segregated, secure and backed up.</h3>

    <div id="testimonial_section4_subsection" class="section_subsection row">

        <div id="testimonial_section4_left" class="col-lg-5 col-lg-offset-1 col-md-offset-1 col-md-5 col-sm-5 col-xs-12">
            <p class="testimonial_screenshot" class="testimonial_screenshot" style="text-align: center">
                <img src="/img/homepage/gridle_file_sharing_screen.png" width="85%" class="center-block">
            </p>    
        </div>

        <div id="testimonial_section4_right" class="col-lg-4 col-md-4 col-sm-7 col-xs-12"> 
            <div class="section_column_description" >
                <p><span class="content_highlight">No, they did not shift.</span> They could access their dropbox from inside gridle. They shared their agendas, reports, presentations, circuit diagrams and charts with respective people inside and outside of company while managing permissions. They could upload <span class="content_highlight">private files or share them with people</span> and groups. While they were creating files and sharing, they were being maintained in an individual specific manner. They could click on a person and all files related to that persone were displayed for review and comments.</p>
            </div>
        </div>
    </div>
    <div class="section_column_testimonial row">
        <div class="testimonial_user_img_div col-lg-2 col-md-2 col-sm-2 col-xs-12">
            <img class="testimonial_user_img center-block" src="/img/homepage/gridle_kavina_patel_testimonial.jpg" alt="gridle's testimonial by Kavina Patel, CEO, quivocal technologies" style="width:100%;"/>
        </div>
        <div class="testimonial_text col-lg-8 col-md-9 col-sm-9 col-xs-12">
            <p class="testimonial_user_line"><span class="fa fa-quote-left"></span>&nbsp;&nbsp; The nature of storing files is superb. It is simple and I wonder that why dont other services do this! It is so intuitive.&nbsp;&nbsp; <span class="fa fa-quote-right"></p>
            <p class="testimonial_username">- Kavina Patel</p>
        </div>
    </div>
</div>
<!--        Section five  (DESK + jaydip )          -->
<div id="testimonial_section5" class="container home_section section-offwhite" >

    <h1 class="section_heading" >
        Data-visualization
    </h1>

    <div class="colored_line_pattern_home">
        <p class="pattern_yellow_sec"></p><p class="pattern_red_sec"></p><p class="pattern_green_sec"></p>
    </div>
    <div class="clearfix"></div>
    <h3 class="section_subheading">Everyone had a single shared screen space</h3>
    <div id="testimonial_section5_subsection" class="section_subsection row">

        <div id="testimonial_section5_left" class="col-lg-4 col-lg-offset-1 col-md-4 col-md-offset-1 col-sm-6 col-xs-12">
            <div class="section_column_description">
                <p>Apart from features which allowed them to communicate, they had a <span class="content_highlight">single shared screen space</span> which let them gain a bird-eye view of their project and team-mates. All at once. They would use it to know pending tasks from a specific person or unread messages; visit their planner or interacted with them <span class="content_highlight">without redirections. On the same screen.</span></p>
            </div>
        </div>
        <div id="testimonial_section5_right" class="section_column col-lg-5 col-md-5 col-sm-6 col-xs-12 " style="">
            <p class="testimonial_screenshot" style="">
                <img class="center-block" src="/img/homepage/gridle_desk.png" alt="gridle's desk's screenshot" width="100%">
            </p>    
        </div>
    </div>
    <div class="section_column_testimonial row">
        <div class="testimonial_user_img_div col-lg-2 col-md-2 col-sm-2 col-xs-12">
            <img class="testimonial_user_img center-block" src="/img/homepage/gridle_chiranjay_shah_testimonial.jpg" alt="gridle's testimonial by chiranjay shah, quivocal technologies" style="width:100%;"/>
        </div>
        <div class="testimonial_text col-lg-8 col-md-9 col-sm-9 col-xs-12">
            <p class="testimonial_user_line"><span class="fa fa-quote-left"></span>&nbsp;&nbsp; What glued me to gridle was not just functionalities but what it did with them. The visual way of working, the individual specific approach and the design made sure that I could not only manage but I could do my management faster. I could see our productivity increasing. &nbsp;&nbsp; <span class="fa fa-quote-right"></p>
            <p class="testimonial_username">- Chiranjay Shah</p>
        </div>
    </div>
</div>

<!-- section (signup again) -->
<div class="signup_section home_section" >
    <p><a class="btn btn-lg btn-primary btn-responsive homepage_btns" href="/users/add" >Sign up now for free</a></p>
    <p><a class="signup_section_links" href="/pages/features">Take a tour</a> &nbsp; <span class="signup_section_links">|</span> &nbsp; <a class="signup_section_links" href="/pages/benefits">Know more</a></p>
</div> 
</div>
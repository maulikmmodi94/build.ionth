<?php
$this->layout = 'gridle_out_new';
$this->set('title_for_layout', 'Making gridle ');
$brand = Configure::read('brand');
?>
<link rel="stylesheet" type="text/css" href="/css/terms.css">
<div class="position-relative">
<div class="container-fluid terms-header">
    <div class="row">
        <div class= "col-sm-offset-2 col-sm-10">				
            <h1 class="terms-heading">#futureofwork awaits!
            </h1>
            <p class="terms-sub-heading">Making Gridle work for your teams..
            </p>
        </div>
    </div>
</div>
<div class="container-fluid pageTerms background-white" >
    <div class="row" >
        <div class="col-sm-offset-2 col-sm-7">
            <div class="terms-content-manifesto margin-tb-fifty">
                <!--SECTION 1--> 
                <div id="welcomeToGridle">
                    <h1 class="terms-content-heading">Welcome to Gridle</h1>
                    <p class=>

                        <img class="imgShadow"class="imgShadow"src="/img/making-gridle/1.png" width="100%" />

                    </p>
                    <p>
                       Gridle is an easy and beautiful productivity suite for your teams. We believe that productivity comes from comfort and convenience of working the way you want. Gridle is an enabler that unifies different tools or applications you and your colleagues use to work together. Here’s couple of things you can do while setting up Gridle to make the most of your free forever account.
                    </p>
                    <hr class="terms-hr"/>
                </div>
                <!--SECTION 2--> 
                <div id="meetDonna">
                    <h1 class="terms-content-heading">Meet Donna</h1>
                    <p class=>

                        <img class="imgShadow"src="/img/making-gridle/2.png" width="100%" />

                    </p>
                    <p>
                        Donna is Gridle’s bot and is a delight to work with. She is fast, accurate, to the point and sometimes funny as well. All the things that you or your colleagues do on Gridle, Donna does the work. From reminding deadlines to sending workspace invitations to helping you retrieve your forgotten password; she takes care of you and your account.

                    </p>
                    <hr class="terms-hr"/>
                </div>

                <!--SECTION 3--> 
                <div id="piolet">
                    <h1 class="terms-content-heading">It all starts with a pilot</h1>
                    <p class=>

                        <img class="imgShadow"src="/img/making-gridle/3.png" width="100%" />

                    </p>
                    <p>Choose a specific week and a closely knit team in your company to start trying out Gridle and do not try to figure everything out from the first go. As a first step, just try using Gridle instead of writing emails (or IMs or Skype). Most of our user community finds great amount of value in design and ease of communication through Gridle. Look for indicators in your way of working. Are you able to get more done with less effort? If it’s a yes, it’s time to think about how are we going to make Gridle reach the rest of your organization for collective benefit.
                    </p>
                    <hr class="terms-hr"/>
                </div>
                <!--SECTION 4--> 
                <div id="workspaceAwesome">
                    <h1 class="terms-content-heading">Workspace. Much Awesome.</h1>
                    <p class=>

                        <img class="imgShadow"src="/img/making-gridle/4.png" width="100%" />

                    </p>
                    <p>
                        Workspace is a virtual version of your office, team or project. Basically, anything you want it to be. It can have from 1 to as many colleagues as you want and all your tasks, files, teams and apps. Entering into a workspace is basically like getting into your office; just that you can do it anywhere and anytime. Also, if you are one of those few like Nick Fury, who work with multiple teams or companies at once, you can have and manage multiple workspaces as well.
                    </p>
                    <hr class="terms-hr"/>
                </div>

                <!--SECTION 5--> 
                <div id="workingTasks">
                    <h1 class="terms-content-heading">Working with Tasks</h1>
                    <p class=>

                        <img class="imgShadow"src="/img/making-gridle/5.png" width="100%" />

                    </p>
                    <p>
                        Now we are talking! The approach towards task here is to make it as simple and as easy as possible. Try to get action items out of natural conversations instead of making people fill out long forms; after all, that’s what a software is all about. Making things easy. Just like you talk, you can type out “@John Doe: Call up Jane Doe for quarterly sales numbers by 3 PM next Monday” and that’s it. The task will be created, assigned and reminded.

                    </p>
                    <hr class="terms-hr"/>
                </div>
                <!--SECTION 6-->
                <div id="files">
                    <h1 class="terms-content-heading">Files</h1>
                    <p class=>

                        <img class="imgShadow"src="/img/making-gridle/6.png" width="100%" />

                    </p>
                    <p>
                        Yeah. So, with Gridle, you can upload, download, view, annotate and share files. Q: What’s more important than being able to do all that with a file? A: Being able to find a file shared with you a couple of months ago! Apart from search in files, clean design and sorting, we also allow filters through people. Because you may not remember name of the file, date when it was shared or subject of the mail but you will always know who shared it with you! Clicking on a person or team in the left pane, filters your file list. Simple as milk.
                    </p>
                    <hr class="terms-hr"/>
                </div>

                <!--SECTION 7-->
                <div id="search">
                    <h1 class="terms-content-heading">Search</h1>
                    <p style="margin-top:50px;">

                        <!--<img class="imgShadow"src="/img/making-gridle/7.png" width="100%" />-->

                    </p>
                    <p>
                        Or, call it advanced search. You be the judge of that. For us, it’s enough to know that it will do what you are expecting it to do without complicated sign language (like john + reports - monthly : San Francisco). Just type in what you think should work and we will land you exactly where you wanted to land. That’s the beauty and power of simplicity.

                    </p>
                    <hr class="terms-hr"/>
                </div>

            </div>
        </div>
        <div class="col-sm-3 ">
            <div class="terms-autoscroll">
                <ul class="nav nav-pills nav-stacked" role="tablist">
                    <li><a href="#welcomeToGridle" class="">Welcome to Gridle</a></li>
                    <li><a href="#meetDonna" class="">Meet Donna</a></li>
                    <li><a href="#piolet" class="">It all starts with a pilot</a></li>
                    <li><a href="#workspaceAwesome" class="">Workspace. Much Awesome.</a></li>
                    <li><a href="#workingTasks" class="">Working with Tasks</a></li>
                    <li><a href="#files" class="">Files</a></li>
                    <li><a href="#search" class="">Search</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>
</div>
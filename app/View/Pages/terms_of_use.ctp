<?php
$this->layout = 'gridle_out';
$this->set('title_for_layout', 'Terms_of_use | gridle ');
$brand = Configure::read('brand');
?>
<link rel="stylesheet" type="text/css" href="/css/terms.css">
<div class="position-relative" style="margin-top: 50px">
	<div class="container-fluid terms-header">
		<div class="row">
				<div class= "col-sm-offset-2 col-sm-10">				
					<h1 class="terms-heading">Terms of Use</h1>
					<p class="terms-sub-heading">Making Build work for your team</p>
				</div>
		</div>
	</div>
<div class="container-fluid background-white" >
	<div class="row" >
	<div class="col-sm-offset-2 col-sm-7">
	<div class="terms-content-use margin-tb-fifty">
		<div id="agreement">
			<h1 class="terms-content-heading">Agreement</h1>
		<p>
		This is an Agreement (hereinafter the Agreement, the Document), between Ionth Global Ltd (hereafter referred to as Build, us, we, the Service and the Site) and you, the individual or legal entity (hereinafter you, the User, member)<br/><br/>

		By using Build, a Service of Ionth Global Ltd, you are agreeing to use the following Terms of Use (Terms of Service). Please read them carefully.<br/><br/>

		Ionth Global Ltd reserves the right to make amends in, update and change the Terms of Use from time to time without prior notice. The amended Terms of Use shall be updated as and when amends are made. This Agreement applies to all the users, whomsoever uses Build in either modes, paid and otherwise.

		</p>
		<hr class="terms-hr"/>
		</div>
		<div id="UseOfOurService">
			<h1 class="terms-content-heading">Use of our service</h1>
		<p>
		Build provides a platform for visual cloud collaboration, online.<br/><br/>

		Build grants you the permission to use the Service it offers, provided you do not copy, distribute of disclose any part of it; you do not modify or reproduce the Service, in any manner and for any purpose and you comply with the Terms of Use.<br/><br/>

		You will need to sign up and create a Member Account to use the Service. Your account gives you access to the services and functionality that we may establish and maintain from time to time and in our sole discretion. You shall never use another Member&#39;s account without permission and for the sake of privacy, it is advisable to not allow any other person to use your account. You are required to provide all the information asked in an accurate and complete form. You are responsible for all the activity that happens through your account, hence we suggest you to keep your password secure and not share it with anyone under any circumstances. Build will not be liable for your losses caused by any unauthorized use of your account; you shall be liable for the losses of Build or others due to such unauthorized use.<br/><br/>

		You agree not to use or launch any automated system, including without limitation, &quot;robots&quot;, &quot;spiders&quot;, &quot;offline readers&quot;, etc., that accesses the Service in a manner that sends more request messages to the Build servers than a human can reasonably produce in the same period of time by using a conventional on-line web browser. You agree not to collect or harvest any personally identifiable information, including account names, from the Service nor to use the communication systems provided by the Service for any commercial solicitation purposes. You agree not to use any portion of the Service as a destination linked from any unsolicited bulk messages or unsolicited commercial messages.<br/><br/>

		Build reserves the rights to temporarily or permanently suspend or otherwise refuse to permit you the use of the Service without notice and/or any liability. You are not allowed to interfere or compromise the system&#39;s integrity or security. Uploading invalid data, viruses, bugs or any similar material through the Service is strictly forbidden. Also, upon termination for any reason you continue to be bound by this Agreement.

		</p>
		<hr class="terms-hr"/>
		</div>
		<div id="refundPolicy">
			<h1 class="terms-content-heading">Fees + Refund Policy</h1>
			<p>
				By accepting the Terms of Use, you agree that Build may charge a fee for the use of any Service and that, we will notify you of any such fee, before you incur it, and you agree to pay any fee, incurred by you. In the event that you have elected to receive additional services for a fee, and you fail to pay such fee within thirty (30) days, then in addition to all other remedies available to Build, Build may immediately cease providing all such additional services. All fees are due and payable in advance with the first monthly subscription fee due upon the registration of your account. Your service term will be renewed automatically on a monthly, quarterly or annual basis depending upon whatever has been agreed by both parties.<br/><br/>

				All fees must be paid by credit card. By authorizing Build to charge a charge card or other credit card, you are authorizing Build or a designated representative or agent to automatically continue charging that card (or any replacement credit card account if the original card is renewed, lost, stolen, or changed for any reason by the credit-issuing entity, and such entity informs Build of such new replacement card account), financial account, or billing account for all fees and charges associated with the Services. If the credit card information you provided is not valid, or if your credit card cannot be processed at the time of the renewal charge, Build reserves the right to immediately terminate or suspend your access to the Services, in the case of termination thereby terminating this Agreement and all of Build&#39;s obligations hereunder.
			</p>
			<hr class="terms-hr"/>
		</div>
		<div id="changing-service">
			<h1 class="terms-content-heading">Changing or discontinuing our service (Features)</h1>
			<p>
				We are ever evolving and upgrading our features. We may add or remove them or stop providing them altogether.<br/><br/>

				You are free to stop using our service at anytime, however we&#39;ll be sorry to see our valued user, leave. We are aware of the fact that you own your data and that preserving your access to the data is important. So in case of discontinuation of a feature, wherever reasonably possible, we will provide you with a reasonable advance notice and hence a chance to get information out of that feature.
			</p>
			<hr class="terms-hr"/>
		</div>
		<div id="privacy-and-security">
			<h1 class="terms-content-heading">Privacy and Security</h1>
			<p>
			We strive to protect your privacy and secure your information. Despite there being no such thing as perfect online privacy/security, we will take all the reasonable steps to ensure the safety of your personal information. However you understand and agree that these steps do not guarantee immunity from unethical security breaches, viruses, security threats and other harmful vulnerabilities.
			</p>
			<hr class="terms-hr"/>
		</div>
		<div id="Jurisdiction">
			<h1 class="terms-content-heading">Jurisdiction</h1>
			<p>
				Build makes no guarantee that the content on the Site and/or the Service are appropriate or available or appropriate for use in your country. Users or people, who visit and/or use the Site and/or the Service, do it on their own free will and risk and are solely responsible for compliance with local laws. Build reserves the right to, at anytime, limit or restrict the availability and accessibility of the Site and/or the Service to any person, geographic area or jurisdiction and also to limit the quantities of any such feature that we provide.
			</p>
			<hr class="terms-hr"/>
		</div>
		<div id="Termination">
			<h1 class="terms-content-heading">Termination</h1>
			<p>
			This Agreement shall remain effective until terminated in accordance with its own terms and conditions. You agree that Build, in its sole discretion, may terminate your password, account (in whole or in part), or use of the Site or Services, and remove and discard any content within the Sites, at any time and for any reason. You agree that any actions taken under this div may be effective without prior notice to you. YOU AGREE THAT WE WILL NOT BE LIABLE TO YOU OR ANY OTHER PARTY FOR ANY TERMINATION OF YOUR ACCESS TO THE SITE OR SERVICES OR DELETION OF YOUR ACCOUNT OR YOUR SUBMISSION MATERIALS. In the event of termination, however, those divs in this Agreement which, by their nature, survive termination of the Agreement shall so survive.
			</p>
			<hr class="terms-hr"/>
		</div>
		<div id="Information">
			<h1 class="terms-content-heading">Information</h1>
			<p>
				This Agreement (including the Privacy Policy) constitutes the entire Agreement and understanding between you and Build and governs your use of the Site and the Services, superseding any prior Agreements between you and Build. This Agreement and the relationship between you and Build shall be governed by and construed in accordance with the laws of India, without regard to its conflict of law provisions. All disputes or jurisdictional matters are subject to jurisdiction of Ahmedabad, India. This Agreement is not assignable, transferable or sub-licensable by you except with our prior written consent. However, we may assign this Agreement to any third party whom we choose without your consent.
			</p>
			<hr class="terms-hr"/>
		</div>
		<div id="Communications">
			<h1 class="terms-content-heading">Communications</h1>
			<p>
				Since our Service is on the internet, all the communication done between the user and Build will be electronic. By agreeing to the Terms you consent to receive communications from us in an electronic form and all the terms and conditions, Agreements, notices, Documents, disclosures and other information that we provide to you electronically satisfy any legal requirements that such Communications would satisfy in written form. Your consent to receive Communications and do business electronically, and our Agreement to do so, applies to all of your interactions and transactions with us and this does not, in anyway, affect your non-waivable rights. If you wish to stop receiving Communications electronically, you will have to, in turn, stop using the Site and the Service. You may withdraw your consent by contacting us. Withdrawing your consent will not affect the validity of the Communications, transactions and business done between you and us, before your act of withdrawing the consent. You shall inform us of any effective change in your e-mail address so that you continue to receive all Communications uninterrupted.
			</p>
			<hr class="terms-hr"/>
		</div>
		<div id="AgreementModifications">
			<h1 class="terms-content-heading">Agreement Modifications</h1>
			<p>
				All the modification and changes done to this Document will be visible on this page and you agree to review the Agreement periodically to be aware of such changes. The Modified Agreement will be indicated by &quot;last updated on&#46;&#46;&#46;&quot; on the top of this page.
			</p>
			<hr class="terms-hr"/>
		</div>
		<div id="Indemnification">
			<h1 class="terms-content-heading">Indemnification</h1>
			<p>
				In case any harm, claims, damages, losses, costs or expenses (including reasonable attorneys&#39; fees and disbursements) arise directly or indirectly from your breach of this Agreements, any allegation that any materials that you submit to Build, infringe or otherwise violate the copyright, trade secret, trademark or other intellectual property rights, or any other rights of a third party, your access of the Site and/or the Service, you agree to indemnify and defend Build and its subsidiaries, affiliates, officers, agents, promoters, partners, employees, investors and representatives, harmless.
			</p>
			<hr class="terms-hr"/>
		</div>
	</div>
</div>
<div id="nav-list" class="col-sm-3 ">
	<div class="terms-autoscroll">
		<ul class="nav nav-pills nav-stacked" role="tablist">
			<li><a href="#agreement" class="">Agreement</a></li>
			<li><a href="#UseOfOurService" class="">Use of our service</a></li>
			<li><a href="#refundPolicy" class="">Fees + Refund Policy</a></li>
			<li><a href="#changing-service" class="">Changing our service</a></li>
			<li><a href="#privacy-and-security" class="">Privacy and Security</a></li>
			<li><a href="#Jurisdiction" class="">Jurisdiction</a></li>
			<li><a href="#Termination" class="">Termination</a></li>
			<li><a href="#Information" class="">Information</a></li>
			<li><a href="#Communications" class="">Communications</a></li>
			<li><a href="#AgreementModifications" class="">Agreement Modifications</a></li>
			<li><a href="#Indemnification" class="">Indemnification</a></li>
		</ul>
	</div>
</div>
</div>
</div>
</div>
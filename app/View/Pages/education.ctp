<?php
$this->layout = 'gridle_home_education';
$this->set('title_for_layout', 'gridle | cloud based visual collaboration tool | enterprises | teams ');

echo $this->Html->meta(array('name' => 'keywords', 'content' => 'project management, task management, collaboration tool, enterprise, teams, calendar, planner'));
echo $this->Html->meta(array('name' => 'description', 'content' => 'Gridle is a complete cloud visual collaboration and communication tool for your time, task and project management for enterprises and teams.'));
//robot meta tag
echo $this->Html->meta(array('name' => 'robots', 'content' => 'index, follow'));
//facebook open graph meta tags
echo $this->Html->meta(array('property' => 'og:title', 'content' => 'gridle | visual cloud collaboration tool for enterprises and teams'));
echo $this->Html->meta(array('property' => 'og:description', 'content' => 'Gridle is a complete cloud visual collaboration and communication tool for your time, task and project management for enterprises and teams.'));
echo $this->Html->meta(array('property' => 'og:type', 'content' => 'website'));
echo $this->Html->meta(array('property' => 'og:image', 'content' => 'http://gridle.io/img/logo/gridle_logo.png'));
echo $this->Html->meta(array('property' => 'og:image:url', 'content' => 'http://gridle.io/img/logo/gridle_logo.png'));
echo $this->Html->meta(array('property' => 'og:image:width', 'content' => '150'));
echo $this->Html->meta(array('property' => 'og:image:height', 'content' => '50'));
echo $this->Html->meta(array('property' => 'og:url', 'content' => 'http://gridle.io'));
echo $this->Html->meta(array('property' => 'og:site_name', 'content' => 'Gridle'));
echo $this->Html->meta(array('property' => 'fb:admins', 'content' => 'anupama.panchal'));
echo $this->Html->meta(array('property' => 'fb:app_id', 'content' => '642120029214138'));
//twitter meta tags
echo $this->Html->meta(array('name' => 'twitter:card', 'content' => 'summary'));
echo $this->Html->meta(array('name' => 'twitter:site', 'content' => '@gridle_io'));
echo $this->Html->meta(array('name' => 'twitter:image:src', 'content' => 'http://gridle.io/img/logo/gridle_logo.png'));
?>
<div class="container-fluid">
    <!--        section one (Main with the office/world svg)        -->
    <div id="section1" class="row">
        <p class="education_landing_img">
            <object data="/img/homepage/education_gridle.svg" style="width: 100%;">
                <img src="/img/homepage/education_gridle.png" />
            </object>
        </p>
        <div class="clearfix"></div>
        <h1 class="section1_text">We firmly believe that educators shape the world.</h1>
        <h3 class="section1_subtext">Gridle helps shape it better with <span class="content_highlight">complete communication</span> and <span class="content_highlight">collaboration</span> for <span class="content_highlight">you</span> and <span class="content_highlight">your students</span>.</h3>
        <div id="section1_bottom">
            <p>
                <a class="btn btn-lg btn-primary btn-responsive homepage_btns" href="/users/add">Register now for free</a>
            </p>
            <!--<p><a href="/pages/features">Take a tour</a> | <a href="/pages/benefits">Know more</a></p>-->
        </div>    
    </div>
    <!--        EDUCATION BENEFITS           -->
    <div id="section3" class="home_section row"> 
        <div id="section3_left" class="col-lg-4 col-lg-offset-2 col-md-offset-1 col-md-5 col-sm-6 col-xs-12">
            <p>
                <object data="/img/homepage/education_benefits_gridle.svg" type="image/svg+xml">
                    <img src="/img/homepage/education_benefits_gridle.png" alt="gridle features" width="100%" />
                </object>
            </p>    
        </div>
        <div id="section3_right" class="col-lg-4 col-md-5 col-sm-6 col-xs-12"> 
            <div class="section_column_description" >
                <h2 class="content_highlight">Gridle for Educators</h2>
                <p>Share your <span class="content_highlight" >class-notes, presentations</span> or sheets. Schedule classes and <span class="content_highlight" >manage groups</span> for projects. With <span class="content_highlight" >skype and dropbox integration</span>, you always stay on top.</p>
                <br>
                <p class="text-center">
                    <!--<a href="/pages/features" class="btn btn_yellow btn-lg homepage_btns">Show me more features..</a>-->
                </p>
            </div>
        </div>
    </div>


    <!--        EDUCATION FEATURES           -->
    <div id="section2" class="home_section row section-white">
        <div id="section2_right" class="col-sm-3 col-sm-offset-1  col-sm-push-6 col-xs-12">
            <p>
                <img src="/img/homepage/education_features_gridle.png" alt="gridle features" width="70%"/>
            </p>    
        </div>
        <div id="section2_left" class="col-sm-4 col-sm-offset-2 col-sm-pull-4 col-xs-12" style="">
            <div class="section_column_description">
                <h2 class="content_highlight">Gridle for students</h2>

                <p>Stay in touch with <span class="content_highlight" >Instant Messages</span>, make groups and leverage <span class="content_highlight" >task management</span> system. Share <span class="content_highlight" >research</span>, links and achieve results faster and smarter.</p>
            </div>
        </div>


    </div>

    <!--    TESTIMONIAL         -->
    <div class="section_column_testimonial row">
        <div class="testimonial_user_img_div col-lg-2">
            <img class="testimonial_user_img" src="/img/homepage/gridle_radhika_kedia_testimonial.png" width="100" alt="chiranjay shah's testmonial about gridle"/>
        </div>
        <div class="testimonial_text col-lg-8">
            <p class="testimonial_user_line"><span class="fa fa-quote-left"></span>&nbsp;&nbsp; Gridle allows me to study and communicate directly at any time with my professors and project mates. Its awesome! &nbsp;&nbsp; <span class="fa fa-quote-right"></span></p>
            <p class="testimonial_username">- Radhika Kedia, Carnegie Mellon University</p>
        </div>
    </div>


    <!-- section (signup again) -->
    <div class="signup_section row" >
    </div>
    <!--   signup section   -->
    <div id="signup_section" class="row" >
        <div class="col-lg-4 col-lg-offset-2">
            <object data="/img/homepage/gridle_signup_type_writer.svg" type="image/svg+xml" width="100%" >
                <img src="/img/homepage/gridle_signup_typewriter.svg" alt="gridle free signup for teams"/>
            </object>
        </div>
        <div class="col-lg-4 signup_rightsection" style="margin-top: 0;">
            <h2 class="content_highlight text-center">Sign up now </h2>
            <h4 class="text-center">to modernize your courses because older ones just don't work</h4>
            <br>
            <p class="text-center"><a class="btn btn-lg btn-primary btn-responsive homepage_btns" href="/users/add" >Sign up now for free</a></p>
        </div>

    </div>
</div>
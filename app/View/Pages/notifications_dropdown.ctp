<?php $this->layout = 'ajax'; ?>   
<div class="notifications-wrap">
    <div class="notifcations-options text-center">
        <p class="notifications-heading text-muted font-big-1 p-margin-0" >Notifications</p>
    </div>
    <div class="notifications-content">
        <div class="notifications-loading" ng-if="!notifications">
            <h1 class="text-center">Loading ...</h1>
        </div>
        <div class="notifications-list container-fluid">
            <div class="notification-div row " ng-repeat="notification in notifications">
                <div class="notification-graphic col-lg-2">
                    <p class ="log-message-icon  p-margin-0" ><span class="fa" ng-class="{file:'fa-file',group:'fa-users', task:'fa-tasks', network:'fa-circle'}[notification.type]"></span></p>
                </div>
                <div class="notification-message col-lg-10">
                    <p class="p-margin-0"><span ng-bind-html="notification.message"></span>&nbsp;<a href="#">See</a></p>
                </div>
            </div>
        </div>
    </div>
    <div class="notifications-footer text-center" >
        <a href="/notifications" class=""><h4>See all</h4></a>
    </div>
</div>

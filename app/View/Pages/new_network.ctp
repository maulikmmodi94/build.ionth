<?php
$this->layout = 'gridle_in';
$this->set('title_for_layout', 'New Workspace');
?>
<div class='container-fluid'>
    <div class='g-add-network-wrap row' >
        <div class="add-network-rocket col-lg-2 padding-lr-0">
            <object data="/img/signup-flow/rocket.svg" ></object>
        </div>
        <div class="col-lg-8">
            <div class="g-add-network-content container-fluid">
                <div class="add-network-heading-div text-center row">
                    <h1 class="add-network-heading font-big-5 text-uppercase">You are about to rocket-fuel your teams with Gridle</h1>
                    <!--<h3>You are about to rocket-fuel your teams with Gridle</h3>-->
                </div>
                <div class="add-network-form row">
                    <div class="col-lg-offset-1 col-lg-7 padding-lr-0">
                        <div class="form-group">
                            <input class="form-control input-lg" placeholder="Give name to your network" />
                        </div>
                    </div>
                    <div class="col-lg-4 ">
                        <button class="btn btn-warning btn-block btn-lg" style="font-family: 'proxima-nova-extra-bold'; height: 46px;"><span class='font-big-1' >NEXT &nbsp;&nbsp;<span class="fa fa-chevron-circle-right"></span></span></button>
                    </div>
                    <div class="clearfix"></div>
                    <div class="add-network-below-content text-center">
                        <p class="font-big-1">Your workspace is going to be the complete set of people you work with.</p>
                        <p class='add-border-up' style='padding-top: 15px;'> It can be the <span class="text-emphasize">name of your company or project</span> and comes with a dashboard, basic analytics and a dedicated management panel.</p>
                    </div>
                </div>

            </div>
        </div>
        <div class="add-netwrk-moon col-lg-2">
            <p>
                <object data="/img/signup-flow/moon.svg" style="" ></object>
            </p>
        </div>
    </div>
</div>
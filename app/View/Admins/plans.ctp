<?php
$this->layout = 'gridle_in';
$this->set('title_for_layout', 'Plans');
?>
<div class='container'>
    <div class="row">

        <div class="col-lg-12">
            <h1>    
                Plans
            </h1>
            <hr>
            <ol class="breadcrumb">
                <li>
                    <?= $this->Html->link('Plans', array('controller' => 'admins', 'action' => 'plans'));
                    ?>
                </li>
                <li>
                    <?= $this->Html->link('Network', array('controller' => 'admins', 'action' => 'networks'));
                    ?>

                </li>
                <li class="active">
                    <?= $this->Html->link('Member', array('controller' => 'admins', 'action' => 'members'));
                    ?>
                </li>
            </ol>
            <div>
                <button type="button" class="btn btn-default" ng-click="showAddPlanForm = !showAddPlanForm">
                    Create Plan
                </button>
                <input type="hidden" ng-model="showAddPlanForm" <?php
                if (!empty($this->validationErrors['Plan'])) {
                    echo 'ng-init="showAddPlanForm=true"';
                }
                ?>/>
                <!--<input type="button" class="btn btn-primary" value="Actions"/>-->
                <hr>
            </div>


            <div>
                <table class="table table-condensed">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Description</th>
                            <th>User limit</th>
                            <th>File space</th>
                            <th>Duration</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr class="active"  ng-show="showAddPlanForm">
                            <td>
                                <?php
                                echo $this->Form->create('Plan', array('role' => 'form',
                                    'inputDefaults' => false));
                                ?>
                            </td>
                            <td>
                                <?php
                                echo $this->Form->input('name', array('label' => false, 'div' => false, 'class' => 'form-control'));
                                ?>
                            </td>
                            <td>
                                <?php
                                echo $this->Form->input('description', array('label' => false, 'div' => false, 'class' => 'form-control'));
                                ?>
                            </td>
                            <td>
                                <?php
                                echo $this->Form->input('user_limit', array('label' => false, 'div' => false, 'class' => 'form-control'));
                                ?>
                            </td>
                            <td>
                                <?php
                                echo $this->Form->input('storage_limit', array('type' => 'number', 'label' => false, 'div' => false, 'class' => 'form-control'));
                                ?>
                            </td>
                            <td>
                                <?php
                                echo $this->Form->input('time_limit', array('label' => false, 'div' => false, 'class' => 'form-control'));
                                ?>
                            </td>
                            <td>
                                <?php
                                echo $this->Form->submit(
                                        'Save Plan', array(
                                    'class' => 'btn btn-primary btn-sm',
                                    'title' => 'Add Plan',
                                    'div' => false,
                                        )
                                );



                                echo $this->Form->end();
                                echo '&nbsp;&nbsp;<button type="button" class="btn btn-sm"  ng-click="showAddPlanForm = !showAddPlanForm">Cancel</button>';
                                ?>
                            </td>
                        </tr>

                        <?php
                        foreach ($plans as $key => $plan) {

                            echo '<tr>';
                            echo '<td>' . ($key + 1) . '</td>';
                            echo '<td>' . $plan['Plan']['name'] . '</td>';
                            echo '<td>' . $plan['Plan']['description'] . '</td>';
                            echo '<td>' . $plan['Plan']['user_limit'] . '</td>';
                            echo '<td>' . $this->Number->toReadableSize($plan['Plan']['storage_limit']) . '</td>';

                            echo '<td>' . $plan['Plan']['time_limit'] . ' month</td>';
                            echo '<td>';
                            echo $this->Form->postLink(__('Delete'), array('action' => 'plans', $plan['Plan']['id']), array('class' => 'label label-danger', 'method' => 'delete'), __('Are you sure you want to delete "%s" ?', $plan['Plan']['name']));
                            echo '</td>';
                            echo '</tr>';
                        }
                        ?>

                    </tbody>
                </table>

                <p>
                    <?php
                    echo $this->Paginator->counter(array(
                        'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
                    ));
                    ?>
                </p>
                <ul class="pagination">
                    <?php
                    echo $this->Paginator->prev('&laquo;', array('tag' => 'li', 'escape' => false), '<a href="#">&laquo;</a>', array('class' => 'prev disabled', 'tag' => 'li', 'escape' => false));
                    echo $this->Paginator->numbers(array('separator' => '', 'tag' => 'li', 'currentLink' => true, 'currentClass' => 'active', 'currentTag' => 'a'));
                    echo $this->Paginator->next('&raquo;', array('tag' => 'li', 'escape' => false), '<a href="#">&raquo;</a>', array('class' => 'prev disabled', 'tag' => 'li', 'escape' => false));
                    ?>
                </ul>
            </div>


        </div>
    </div>
</div>
<?php echo $this->element('sql_dump'); ?>


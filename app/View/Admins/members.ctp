<?php
$this->layout = 'gridle_in';
$this->set('title_for_layout', 'Members | Admin');
?>
<div class='container'>
    <div class="row">

        <div class="col-lg-12">

            <h1>    
                Members
            </h1>
            <hr>
            <ol class="breadcrumb">
                <li>
                    <?= $this->Html->link('Plans', array('controller' => 'admins', 'action' => 'plans'));
                    ?>
                </li>
                <li>
                    <?= $this->Html->link('Network', array('controller' => 'admins', 'action' => 'networks'));
                    ?>

                </li>
                <li class="active">
                    <?= $this->Html->link('Member', array('controller' => 'admins', 'action' => 'members'));
                    ?>

                </li>
            </ol>
            <div>
                <button type="button" class="btn btn-default" ng-click="showAddForm = !showAddForm">
                    Add Member
                </button>
                <input type="hidden" ng-model="showAddForm" <?php
                if (!empty($this->validationErrors['UserNetwork'])) {
                    echo 'ng-init="showAddForm=true"';
                }
                ?>/>
                <!--<input type="button" class="btn btn-primary" value="Actions"/>-->
                <hr>
            </div>


            <div>
                <table class="table table-condensed">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th><?php echo $this->Paginator->sort('UserInfo.name','Name');
?></th>
                            <th><?php echo $this->Paginator->sort('Network.name','Network');
?></th>
                            <th><?php echo $this->Paginator->sort('UserNetwork.role','Role');
?></th>
                            <th><?php echo $this->Paginator->sort('UserNetwork.file_count','File Usage');
?></th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr class="active"  ng-show="showAddForm">
                            <td>
                                <?php
                                echo $this->Form->create('UserNetwork', array('role' => 'form',
                                    'inputDefaults' => false));
                                ?>
                            </td>
                            <td>
                                <?php
                                echo $this->Form->input('email', array('label' => false, 'div' => false, 'class' => 'form-control', 'placeholder' => 'Email'));
                                ?>
                            </td>                            
                            <td>

                                <?php
                                if (isset($default)) {
                                    $options[$default] = $networks[$default];
                                } else {
                                    $options = $networks;
                                }
                                echo $this->Form->input('network_id', array('options' => $networks, 'class' => 'form-control', 'label' => false, 'div' => FALSE, 'required'));
                                ?>
                            </td>
                            <td>

                                <?php
                                echo $this->Form->input('role', array('options' => $role, 'class' => 'form-control', 'label' => false, 'div' => FALSE, 'required'));
                                ?>
                            </td>
                            <td>
                                <?php
                                echo $this->Form->submit(
                                        'Add Member', array(
                                    'class' => 'btn btn-primary btn-sm',
                                    'title' => 'Add Member',
                                    'div' => false,
                                        )
                                );



                                echo $this->Form->end();
                                echo '&nbsp;&nbsp;<button type="button" class="btn btn-sm"  ng-click="showAddForm = !showAddForm">Cancel</button>';
                                ?>
                            </td>

                            <td></td>
                            <td></td>
                        </tr>

                        <?php
                        foreach ($members as $key => $member) {

                            echo '<tr>';
                            echo '<td>' . ($key + 1) . '</td>';
                            echo '<td>' . $member['UserInfo']['name'] . '</td>';
                            echo '<td>' . $networks[$member['UserNetwork']['network_id']] . '</td>';
                            echo '<td>' . $member['UserNetwork']['role'] . '</td>';
                            echo '<td>' . $this->Number->toReadableSize($member['UserNetwork']['file_count']). '</td>';
                            echo '<td>';
                            echo $this->Form->postLink(__('Remove'), array('action' => 'members', $member['UserNetwork']['id']), array('class' => 'label label-danger', 'method' => 'delete'), __('Are you sure you want to delete "%s" ?', $member['UserInfo']['name']));
                            echo '</td>';
                            echo '</tr>';
                        }
                        ?>

                    </tbody>
                </table>

                <p>
                    <?php
                    echo $this->Paginator->counter(array(
                        'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
                    ));
                    ?>
                </p>
                <ul class="pagination">
                    <?php
                    echo $this->Paginator->prev('&laquo;', array('tag' => 'li', 'escape' => false), '<a href="#">&laquo;</a>', array('class' => 'prev disabled', 'tag' => 'li', 'escape' => false));
                    echo $this->Paginator->numbers(array('separator' => '', 'tag' => 'li', 'currentLink' => true, 'currentClass' => 'active', 'currentTag' => 'a'));
                    echo $this->Paginator->next('&raquo;', array('tag' => 'li', 'escape' => false), '<a href="#">&raquo;</a>', array('class' => 'prev disabled', 'tag' => 'li', 'escape' => false));
                    ?>
                </ul>
            </div>


        </div>
    </div>
</div>
<?php echo $this->element('sql_dump'); ?>


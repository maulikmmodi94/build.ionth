<?php
$this->layout = 'gridle_in';
$this->set('title_for_layout', 'Networks | Admin');
echo $this->Html->css('bootstrap-datetimepicker');
?>
<div class='container'>
    <div class="row">
        
        <div class="col-lg-12">
            <h1>    
                Networks
                <label class="label label-info pull-right"><?= $this->Number->toReadableSize($network_usage) ?> / <?= $this->Number->toReadableSize($network_limit) ?></label>
            </h1>
            <hr>

            <ol class="breadcrumb">
                <li>
                    <?= $this->Html->link('Plans', array('controller' => 'admins', 'action' => 'plans')); ?>
                </li>
                <li>
                    <?= $this->Html->link('Network', array('controller' => 'admins', 'action' => 'networks')); ?>
                </li>
                <li class="active">
                    <?= $this->Html->link('Member', array('controller' => 'admins', 'action' => 'members')); ?>
                </li>
            </ol>
            <div>
                <button type="button" class="btn btn-default" ng-click="showAddNetworkForm = !showAddNetworkForm">
                    Create Network
                </button>
                <input type="hidden" ng-model="showAddNetworkForm" <?php
                if (!empty($this->validationErrors['Network'])) {
                    echo 'ng-init="showAddNetworkForm=true"';
                }
                ?>/>
                <!--<input type="button" class="btn btn-primary" value="Actions"/>-->
                <hr>
            </div>


            <div>
                <table class="table table-condensed">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th><?php echo $this->Paginator->sort('Network.name', 'Name'); ?></th>
                            <th><?php echo $this->Paginator->sort('Network.user_count', 'User Count'); ?></th>
                            <th><?php echo $this->Paginator->sort('Network.file_count', 'File Count'); ?></th>
                            <th><?php echo $this->Paginator->sort('Network.expires', 'Expiry Date'); ?></th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr class="active"  ng-show="showAddNetworkForm">
                            <td>
                                <?php
                                echo $this->Form->create('Network', array('role' => 'form',
                                    'inputDefaults' => false));
                                ?>
                            </td>
                            <td>
                                <?php
                                echo $this->Form->input('name', array('label' => false, 'div' => false, 'class' => 'form-control', 'placeholder' => 'Network name'));
                                ?>
                            </td>
                            <td>
                                <?php
                                echo $this->Form->input('NetworkPlan.0.plan_id', array('options' => $plans, 'class' => 'form-control', 'label' => false, 'div' => FALSE, 'required'));
                                ?>
                            </td>
                            <td>
                                <?php
                                echo $this->Form->input('self_add', array('default' => false, 'label' => false, 'div' => false, 'type' => 'hidden'));
                                echo $this->Form->submit(
                                        'Save Network', array(
                                    'class' => 'btn btn-primary btn-sm',
                                    'title' => 'Add Network',
                                    'div' => false,
                                        )
                                );
                                echo $this->Form->end();
                                echo '&nbsp;&nbsp;<button type="button" class="btn btn-sm"  ng-click="showAddNetworkForm = !showAddNetworkForm">Cancel</button>';
                                ?>
                            </td>
                            <td></td>
                            <td></td>
                        </tr>

                        <?php
                        foreach ($networks as $key => $network) {

                            echo '<tr>';
                            echo '<td>' . ($key + 1) . '</td>';
                            echo '<td>' . $this->Html->link($network['Network']['name'], array('controller' => 'admins', 'action' => 'members', 'network_id' => $network['Network']['id'])) . '</td>';
                            echo '<td>' . $network['Network']['user_count'] . ' / ' . $network['Network']['user_limit'] . '</td>';
                            echo '<td>' . $this->Number->toReadableSize($network['Network']['file_count']) . ' / ' . $this->Number->toReadableSize($network['Network']['file_limit']) . '</td>';
                            echo '<td>' . $this->Time->nice($network['Network']['expires']) . '</td>';
                            echo '<td>';
                            echo '<label class="label label-info" ng-init="network' . $network['Network']['id'] . '=false" ng-click="network' . $network['Network']['id'] . '=!network' . $network['Network']['id'] . '">Edit</label>&nbsp;';

                            echo $this->Form->postLink(__('Delete'), array('action' => 'networks', $network['Network']['id']), array('class' => 'label label-danger', 'method' => 'delete'), __('Are you sure you want to delete "%s" ?', $network['Network']['name']));
                            echo '</td>';
                            echo '</tr>';

                            echo '<tr ng-show="network' . $network['Network']['id'] . '" class="active">';
                            echo $this->Form->create('Network', array('role' => 'form',
                                'inputDefaults' => false, 'type' => 'PUT'));
                            echo '<td>';
                            echo $this->Form->input('id', array('type' => 'hidden', 'div' => false, 'class' => 'form-control', 'default' => $network['Network']['id']));
                            echo '</td>';
                            echo '<td>';
                            echo $this->Form->input('name', array('label' => false, 'div' => false, 'class' => 'form-control', 'placeholder' => 'Network name', 'default' => $network['Network']['name']));
                            echo '</td>';
                            echo '<td>';
                            echo $this->Form->input('user_limit', array('label' => false, 'div' => false, 'class' => 'form-control', 'placeholder' => 'User Limit', 'default' => $network['Network']['user_limit']));
                            echo '</td>';
                            echo '<td>';
                            echo $this->Form->input('file_limit', array('label' => false, 'div' => false, 'class' => 'form-control', 'placeholder' => 'File Limit', 'default' => $network['Network']['file_limit']));
                            echo '</td>';

                            echo '<td>';
                            echo $this->Form->input('expires', array('type' => 'text', 'label' => false, 'div' => false, 'class' => 'form-control datetimpicker', 'placeholder' => 'Expires In', 'default' => date("m/d/Y h:m A", strtotime($network['Network']['expires']))));
                            echo '</td>';

                            echo '<td>';
                            echo $this->Form->submit(
                                    'Edit Network', array(
                                'class' => 'btn btn-info btn-sm',
                                'title' => 'Edit Network',
                                'div' => false,
                                    )
                            );
                            echo '&nbsp;&nbsp;<button type="button" class="btn btn-sm"  ng-click="network' . $network['Network']['id'] . '=!network' . $network['Network']['id'] . '">Cancel</button>';
                            echo '</td>';
                            echo $this->Form->end();

                            echo '</tr>';
                        }
                        ?>

                    </tbody>
                </table>

                <p>
                    <?php
                    echo $this->Paginator->counter(array(
                        'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
                    ));
                    ?>
                </p>
                <ul class="pagination">
                    <?php
                    echo $this->Paginator->prev('&laquo;', array('tag' => 'li', 'escape' => false), '<a href="#">&laquo;</a>', array('class' => 'prev disabled', 'tag' => 'li', 'escape' => false));
                    echo $this->Paginator->numbers(array('separator' => '', 'tag' => 'li', 'currentLink' => true, 'currentClass' => 'active', 'currentTag' => 'a'));
                    echo $this->Paginator->next('&raquo;', array('tag' => 'li', 'escape' => false), '<a href="#">&raquo;</a>', array('class' => 'prev disabled', 'tag' => 'li', 'escape' => false));
                    ?>
                </ul>
            </div>


        </div>
    </div>
</div>
<?php echo $this->element('sql_dump'); ?>
<script>
    jQuery('.datetimpicker').datetimepicker({
        format: 'm/d/Y h:m A',
        //formatTime: 'g:i A',
        closeOnDateSelect: true,
        //minDate: '0',
        /*onShow: function(ct) {
         this.setOptions({
         maxDate: jQuery('#datetimepicker_end').val() ? jQuery('#datetimepicker_end').val() : false
         });
         }*/
        /*onChangeDateTime: function(dp, $input) {
         //alert($input.val());
         jQuery('#datetimepicker_end').datetimepicker().setOptions({
         minDate: '2'
         });
         }*/
    });
</script>


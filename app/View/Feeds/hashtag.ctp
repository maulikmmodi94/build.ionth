<?php
$this->layout = 'layout_focused_new';
$this->set('title_for_layout', 'HashtagSearch | Build');
?>
<?php
echo $this->Html->script('modules/feeds/twitter-text-min.js');
?>
<div class="container-fluid" ng-controller="SearchController" style="margin-top: 50px">
    <div class="g-feed-wrap row">
        <div class="col-sm-6 col-sm-offset-3">
            <!--Feed View List-->
            <div class=" feed-list row">
                <h1>Search results &nbsp;&nbsp;<a href="/feeds" class="font-normal"><span class="fa fa-arrow-left"></span>&nbsp;&nbsp;back to feeds</a></h1>
                <div ng-repeat="feed in searchService.feeds" class="feed-post container-fluid g-background-white">
                    <div class="row">
                        <div class="col-sm-12">
                            <span ng-click="feedDeleteModal(feed.Feed.id)" ng-if="feed.Feed.user_id === loggeduser" class="close text-emphasize pull-right">&times;</span>
                            <g-profile-img type='user' myclass='pull-left' path="{{users[findUser(feed.Feed.user_id)]['UserInfo']['vga']}}" width="25px" height="25px" name="{{users[findUser(feed.Feed.user_id)]['UserInfo']['name']}}" ng-if="users" ></g-profile-img>
                            <p class="feed-post-user-name"><span class=" text-emphasize g-doc-ppl-name" ng-bind="users[findUser(feed.Feed.user_id)]['UserInfo']['name']"  ></span>&nbsp;&nbsp;<span class="text-muted font-small-1" >shared</span>&nbsp;&nbsp;<span  title="{{feed.Feed.created}}" class="time-ago text-muted font-small-1"></span></p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12"><p class="p-margin-0" ng-bind-html="feed.Feed.message"></p></div>
                    </div>
                    <div ng-if="feed.Feed.preview_link" class="row">
                        <div class="col-sm-12">
                            <!--Add class embedly-card after angular is compiled-->
                            <a ng-class="{'embedly-card':1}" href="{{feed.Feed.preview_link}}" data-card-chrome="0" data-card-analytics="0"></a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <span class="pull-left text-muted feed-comment-count"><i class="fa fa-comments-o"></i>&nbsp;<span ng-show="feed.Feed.comment_count > 0" ng-bind="feed.Feed.comment_count"></span></span>

                            <span class="pull-left feed-comment-count text-muted">&nbsp;&nbsp;
                                <button class="btn-link" ng-click="feedLike(feed.Feed.id)" ng-hide="feed.Feed.is_liked_by_current_user">Like</button>
                                <button class="btn-link" ng-click="feedUnlike(feed.Feed.id)" ng-show="feed.Feed.is_liked_by_current_user">Unlike</button>

                                <button class="btn-link" ng-click="likesList(feed.Feed.id)" ng-if="feed.Feed.like_count > 0" ><span ng-show="feed.Feed.like_count > 0"  ng-bind="feed.Feed.like_count"></span>&nbsplike<span ng-if="feed.Feed.like_count > 1">s</span> on this</button>
                            </span>

                        </div>
                    </div>
                    <div class="row">
                        <comment-form id="{{feed.Feed.id}}" add="searchService.addComment(id,body)" delete="deleteCommentModal(id,commentId)" comments=feed.Comment loadmore="{{feed.Feed.moreComments}}" load="searchService.getComment(id,lastId)" commentcount=feed.comment_count></comment-form>
                    </div>

                </div>
                <button class="btn btn-sm btn-default center-block" ng-if="loadMoreTag" ng-click="feedGet(hashLoadedPage + 1)">Load More</button>
                <div ng-show="searchService.feeds.length <= 0">
                    <h4>
                        Entered Hashtag keyword doesnot exist
                    </h4>
                </div>
            </div>

        </div>
        <div class="col-sm-2 col-sm-offset-1" style="padding-top: 10px">
            <div class="input-group">
                <input class="form-control g-search-box" type="text" ng-model="hashsearch" placeholder='Search a Hashtag' ng-enter="boxsearch(hashsearch)">
                <div class='input-group-addon input-circular-settings-focused'><span class='fa fa-search'></span></div>
            </div>
        </div>
    </div>
</div>
<script src="/scripts/Vendor/min/twitter-text.min.js"></script>
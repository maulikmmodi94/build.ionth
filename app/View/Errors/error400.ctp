<?php
$this->layout = 'focused';
$this->set('title_for_layout', 'Page not found | Gridle');
?>
<style>
    .not-found-wrap{
        background: url('/img/patterns/halftone.png');
        padding-bottom: 150px;
    }
</style>
<div class="g-page-wrap container-fluid not-found-wrap">
    <div class="row">
        <div class="col-lg-6 col-lg-offset-3 forgot-pass-main-img-div">
            <p class="forgot-pass-main-img">
                <img src="/img/homepage/forgot_pass_gridle.png" width="100%" alt="forgot password gridle" />
            </p>
        </div>
        <div class="clearfix"></div>
        <div class="col-lg-6 col-lg-offset-3 form-forgot-pass-area">
            <h4 class="form-subheading" style="margin-bottom: 40px;" >The page you are looking for doesnt exist. If you are bored and were typing random web addresses, consider sharing us on twitter. </h4>
            <a href="https://twitter.com/intent/tweet?screen_name=gridle_io" class="twitter-mention-button center-block" data-size="large" data-related="anupamapanchal"  >Tweet to @gridle_io</a>
            <script>!function(d, s, id) {
                    var js, fjs = d.getElementsByTagName(s)[0], p = /^http:/.test(d.location) ? 'http' : 'https';
                    if (!d.getElementById(id)) {
                        js = d.createElement(s);
                        js.id = id;
                        js.src = p + '://platform.twitter.com/widgets.js';
                        fjs.parentNode.insertBefore(js, fjs);
                    }
                }(document, 'script', 'twitter-wjs');
            </script>
        </div>
    </div>
</div>

<?php
/**
 *
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Errors
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
?>
<?php /*
<h2><?php echo $message; ?></h2>
<p class="error">
	<strong><?php echo __d('cake', 'Error'); ?>: </strong>
	<?php printf(
		__d('cake', 'The requested address %s was not found on this server.'),
		"<strong>'{$url}'</strong>"
	); ?>
</p>
<?php
if (Configure::read('debug') > 0):
	echo $this->element('exception_stack_trace');
endif;*/
?>

<?php
$this->layout = 'gridle_in';
$this->set('title_for_layout', 'Group Page');
?>
<div ng-controller="groupController" class='container-fluid'    >
    <div class='group-page-wrap row'>
        <!--        GROUP INFOMRAITON           -->
        <div class="group-info-section container-fluid">
            <div class='g-grp-info row'>
                <!--    group image     -->
                <div class="g-grp-img-cover col-lg-1 col-md-1 col-sm-1 col-xs-1">
                    <p class='g-grp-img font-big-4'>G</p>
                </div>
                <!--    group name      -->
                <div class='g-grp-name col-lg-5 col-md-5 col-sm-5 col-xs-5'>
                    <p class='g-grp-name-text font-big-4 p-margin-0 text-capitalize' >{{ groups[groupCurrentIndex]["Group"].name }}</p>
                    <p class="g-grp-usr-count text-muted font-normal p-margin-0" >{{groups[groupCurrentIndex]["Group"].users_group_count}} Members &nbsp|&nbsp;&nbsp; 
                        <span class="btn-link text-muted" ng-click="groupOpenSettings(groups[groupCurrentIndex].Group.id)"> <i class="fa fa-gear"></i> My Profile</span>
                    </p>
                </div>
                <!--    search            -->
                <div class="g-grp-search-area col-lg-3 col-md-3 col-sm-3 col-xs-3">
                    <input class="g-search-box input-sm form-control" placeholder="Search for people" />
                </div>
                <!--    add members         -->
                <div class="g-grp-add-member-area col-lg-3 col-md-3 col-sm-3 col-xs-3 ">
                    <button class="btn btn-success btn-sm btn-block">Invite your team-mates.</button>
                </div>
            </div>
        </div>
        <!--            GROUP MEMBERS               -->
        <div class="container-fluid">
            <!--<div class="group-members-section-heading"><p class="text-muted">Members</p></div>-->
            <div class="group-members-section row">
                <div class="g-grp-user-tile-cover col-lg-2 col-md-2 col-sm-3 col-xs-4" ng-repeat="user in groups[groupCurrentIndex]['UsersGroup']">
                    <div class="g-grp-user-tile" >
                        <div class="g-grp-user-img-cover">
                            <p>
                            <g-profile-img type="user" path="{{ users[findUser(user.user_id)]['UserInfo']['vga']}}" name="{{ users[findUser(user.user_id)]['UserInfo']['name']}}" myclass="{{ user.role=='admin' ? 'g-grp-admin-img g-grp-user-img font-big-2' : 'g-grp-user-img font-big-2' }}" width='64px' height='64px' ></g-profile-img>
                            </p>
                        </div>
                        <div class="">
                            <p class="g-grp-user-name"><a href="#"> <span class="g-text-uppercase">{{ users[findUser(user.user_id)].UserInfo.name}}</span></a></p>
                            <p class="font-small-1" ng-show="{{user.role}} === admin ">Owner</p>
                            <p class="font-small-1" ng-show="{{user.role}} !== admin ">Member</p>
                        </div>

                    </div>
                </div>
            </div>
            <div class="group-members-section-footer">
                <p>
                    <span class="btn-link" ng-if="groups[groupCurrentIndex]['Group']['admin'] != loggeduser" ng-click="groupModalLeave(groups[groupCurrentIndex]['Group']['id'], groups[groupCurrentIndex]['Group']['name'])"><i class="fa fa-meh-o"></i> Leave group</span>
                    <span class="btn-link" ng-if="groups[groupCurrentIndex]['Group']['admin'] == loggeduser" ng-click="groupModalDelete(groups[groupCurrentIndex]['Group']['id'], groups[groupCurrentIndex]['Group']['name'])"><i class="fa fa-trash-o"></i> Delete group</span>
                </p>
            </div>
        </div>

    </div>
</div>
<?php
//Group Management Modal
echo $this->element('Group/manage');
?>
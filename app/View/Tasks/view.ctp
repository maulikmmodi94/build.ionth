<?php
$this->layout = 'gridle_in';
$this->set('title_for_layout', 'Task Page');
?>
<div ng-controller="taskController" class='container-fluid'    >
    <div class='task-page-wrap row'>
        <!--        FILE INFOMRAITON           -->
        <div class="task-info-section container-fluid">
            <div class='g-task-info row'>
                <!--    task image     -->
                <div class="g-task-img-cover col-xs-2">
                    <p class='g-task-icon-cover text-center p-margin-0 pull-right'>
                        <span class="g-task-icon fa fa-tasks"></span>
                    </p>
                </div>
                <!--    task name      -->
                <div class='g-task-description col-xs-8'>
                    <p class='g-task-title-text font-big-2 text-semi-bold' >{{ TaskData[0].Task.title}} </p>
                     
                     <!--Delete Task Button-->
                     <div ng-show="TaskAdmin"> 
                         <li class='btn-on-task pull-right' title="Delete Task" ng-click="taskDelete(TaskData[0].Task.id);$event.stopPropagation" tooltip data-placement="top"  ><span class="task-delete" ><i class="fa fa-trash-o"></i></span>
                        </li>  
                     </div>
                   
                     <!--Edit Task-->
                     <div ng-show="TaskAdmin">
                         <li class='btn-on-task pull-right' title="Edit Task" data-toggle="modal" data-target="#taskAddModal" ng-click="taskSelectItem(TaskData[0].Task.id);" tooltip data-placement="top" ><span class="fa fa-pencil"></span>
                        </li>
                     </div>
                    
                   
                    <!--Start and end info--> 
                    <p class='g-task-dates'>
                        <span class='text-muted'>Start : </span>{{ dateFormat(TaskData[0].Task.start_date, 'medium')}}&nbsp;&nbsp;&nbsp;
                        <span class='text-muted'>End : </span>{{ dateFormat(TaskData[0].Task.end_date, 'medium')}}
                        &nbsp;&nbsp;
                        <span ng-show='TaskData[0].Task.priority' class=' label badge-danger text-semi-bold ' >Important</span>
                        <span ng-show="!TaskData[0].Task.is_completed" class="label label-warning text-semi-bold ">Uncomplete</span>
                        <span ng-show="TaskData[0].Task.is_completed" class="label label-success text-semi-bold ">Complete</span>
                    </p>
      
                    <!--Created BY Info-->
                    <div class="">
                        <p class="g-task-user-name"> 
                        <g-profile-img ng-if="users" type="user" path="{{ users[findUser(TaskData[0].Task.created_by)].UserInfo.vga}}" name="{{ users[findUser(TaskData[0].Task.created_by)].UserInfo.name}}" width="25px" height="25px" myclass="g-task-user-img" ></g-profile-img>
                        <a href="#" ng-click="showUserInfoModal(TaskData[0].Task.created_by)"> <span class="g-text-capitalcase">{{ users[findUser(TaskData[0].Task.created_by)].UserInfo.name}}</span></a>
                        <span>&nbsp;created the task.</span>
                        </p>
                    </div>
                    <!--            FILE SHARED WITH         -->
                    <div class="" >
                        <p ng-if="TaskData[0].Tags.length > 1" class="text-muted font-small-1"><span>Shared with <span class="text-semi-bold" ng-bind="TaskData[0].Tags.length - 1"></span></span></p>
                        <div class="task-tile-ppl" ng-repeat="tags in TaskData[0].Tags" data-type="{{tags.type}}" ng-show="tags.role == 'user'" ng-class="{'badge-sucess': tags.type === 'group'}"  > 
                            <!--if group-->    
                            <p class=" pull-right task-tile-ppl-grp p-margin-0  text-capitalize" ng-if="tags.type === 'group'" title="Team :  {{groups[findGroup(tags.value)].Group.name}}" ng-bind='groups[findGroup(tags.value)].Group.name' tooltip data-placement="top" ></p>
                            <!--if user-->    
                            <span ng-if="tags.type === 'user'">
                                <g-profile-img ng-if="users"  type='user'  path="{{users[getUserIndex(tags.value)].UserInfo.vga}}" name="{{users[getUserIndex(tags.value)].UserInfo.name}}" myclass="task-tile-ppl-user p-margin-0 pull-right" width="30px" height="30px" tooltip title="{{users[getUserIndex(tags.value)].UserInfo.name}}" data-placement="top"></g-profile-img>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="container-fluid">

            <div class="task-comments-section row">

                <comment-form model='tasks' id='{{TaskData[0].Task.id}}' add="taskService.addComment(id,body)" delete="taskService.deleteCommentModal(id,commentId)" comments=TaskData[0].Comment loadmore="{{TaskData[0].moreComments}}" load="taskService.getComment(id,lastId)" commentcount=TaskData[0].Task.comment_count></comment-form>
<!--                <p>
                    <span class="btn-link"><i class="fa fa-trash-o"></i> Delete task</span>
                </p>-->
            </div>        
        </div>
    </div>
    
    <?php
    //new task modal
    echo $this->element('Task/new_task');
    //echo $this->element('Task/single_task_view');
    
    ?>
</div>

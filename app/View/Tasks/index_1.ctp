<?php
/* for calendar */

echo $this->Html->script('jquery-ui');
echo $this->Html->script('fullcalendar');
echo $this->Html->script('jquery.validate');
echo $this->Html->script('bootstrap-datetimepicker');
echo $this->Html->css('fullcalendar');
echo $this->Html->css('bootstrap-datetimepicker');
?>



<style>
    .task-assignee i
    {
        cursor: pointer;
    }

    .task-assignee i:hover:before
    {

        content: "\f057";
    }
    .task-delete,.task-edit,.task-new
    {
        cursor: pointer;
    }

    .task-action .fa
    {
        color:#a1a1a1;
        cursor:pointer;
        font-size:0.7em;
        transition: 0.2s all ease;
    }
    .task-action .fa:hover
    {
        color:#000000;
        transform:scale(1.3);
        -webkit-transform:scale(1.3);
    }

    .task-assignee
    {
        white-space: normal;
    }

    .highlight
    {
        background:rgb(228, 246, 254);
        background: #43aaa3;
        color: #fff;
    }



    .nganimation.ng-enter { 
        -webkit-animation: enter 600ms cubic-bezier(0.445, 0.050, 0.550, 0.950);
        animation: enter 600ms cubic-bezier(0.445, 0.050, 0.550, 0.950);
        display: block;
        position: relative;
    } 
    @-webkit-keyframes enter {
        from {
            opacity: 0;
            /*height: 0px;*/
            left: -70px;
        }
        75% {
            left: 15px;
        }
        to {
            opacity: 1;
            /*height: 30px;*/
            left: 0px;
        }
    }
    @keyframes enter {
        from {
            opacity: 0;
            /*height: 0px;*/
            left: -70px;
        }
        75% {
            left: 15px;
        }
        to {
            opacity: 1;
            /*height: 30px;*/
            left: 0px;
        }
    }

    .nganimation.ng-leave { 
        -webkit-animation: leave 600ms cubic-bezier(0.445, 0.050, 0.550, 0.950);
        animation: leave 600ms cubic-bezier(0.445, 0.050, 0.550, 0.950);
        display: block;
        position: relative;
    } 
    @-webkit-keyframes leave {
        to {
            opacity: 0;
            /*height: 0px;*/
            left: -70px;
        }
        25% {
            left: 15px;
        }
        from {
            opacity: 1;
            /*height: 30px;*/
            left: 0px;
        }
    }
    @keyframes leave {
        to {
            opacity: 0;
            /*height: 0px;*/
            left: -70px;
        }
        25% {
            left: 15px;
        }
        from {
            opacity: 1;
            /*height: 30px;*/
            left: 0px;
        }
    }

    .del
    {
        text-decoration: line-through;
    }
    .del .fa:before,.del .fa:after
    {
        text-decoration: line-through;
    }


</style>


<div class='container-fluid'>

<div class="row " ng-controller="taskController" style="padding-top:70px;">


    

    <div class="col-lg-8">
        <div class="col-lg-12">
            <div class="form-group col-lg-2">
                <input type="text" ng-model="search" class="form-control search-query" placeholder="Search Task"></div>
            <div class="form-group col-lg-4">
                <input type="text" class="form-control" ng-model="TaskSelected.Task.title" placeholder="Add Task" ng-enter="taskAdd(TaskSelected)">
                <i class="fa fa-gear" data-toggle="modal" data-target="#taskAddModal" ng-click="taskNewInit();" style="
                   position: absolute;
                   top: 0;
                   right: 25px;
                   line-height: 34px;
                   "></i>
            </div>
            <!--<p class="col-lg-6 label label-inf1o task-new label-success" data-toggle="modal" data-target="#taskAddModal" ng-click="taskNewInit();" title="Add New Task"> <i class="fa fa-plus"></i> Add Task</p>-->
            
            <div class="col-lg-3 btn-group">
                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                    {{filterBy.name}} <span class="caret"></span>
                </button>
                <ul class="dropdown-menu" role="menu">
                    <li><a href="#" ng-click="selectFilter('all')">All Task</a></li>
                    <li><a href="#" ng-click="selectFilter('assignedToMe')">Assigned to Me</a></li>
                    <li><a href="#" ng-click="selectFilter('assignedByMe')">Assigned By Me</a></li>
                    <li><a href="#" ng-click="selectFilter('BetweenMeAnd')">Between Me & </a></li>
                    <li><a href="#" ng-click="selectFilter('DeadlineMissed')">Deadline Missed</a></li>

                </ul>

            </div>
            <div class="col-lg-2">
                <tags-input ng-show="filterBy.id == 'BetweenMeAnd'" ng-model="filterWith" add-from-autocomplete-only="true"  display-property="name" placeholder="User and/or Group">
                    <auto-complete source="taskLoadTags($query)"></auto-complete>
                </tags-input>
            </div>

        </div>
        <div class="alert-success calAlert" ng-show="alertMessage != undefined && alertMessage != ''">
            <p>{{alertMessage}}</p>
        </div>
        <!--        <div class="btn-toolbar">
                    <div class="btn-group">
                        <button class="btn btn-success" ng-click="changeView('agendaDay', myCalendar1)">Day</button>
                        <button class="btn btn-success" ng-click="changeView('agendaWeek', myCalendar1)">Week</button>
                        <button class="btn btn-success" ng-click="changeView('month', myCalendar1)">Month</button>
                    </div>
                </div>-->
        <div class="calendar" ng-model="eventSources" calendar-watch-event="extraEventSignature" calendar="myCalendar1" config="uiConfig.calendar" ui-calendar="uiConfig.calendar"></div>
    </div>


    <div class="col-lg-4">


        <?php echo $this->element('Task/view')?>


    </div>








<?php echo $this->element('Task/new_task')?>


</div>



</div>
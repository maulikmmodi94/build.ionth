<?php
$this->layout = 'gridle_in';
$this->set('title_for_layout', 'Planner');

/* for calendar */

//echo $this->Html->script('jquery-ui');
//echo $this->Html->script('fullcalendar');
//echo $this->Html->script('jquery.validate');
//echo $this->Html->script('bootstrap-datetimepicker');

echo $this->Html->css('bootstrap-datetimepicker');
?>
<div class="container-fluid" ng-controller="taskController">
    <div class="g-planner-wrap row">
        <div class="col-lg-7 col-lg-offset-1 col-md-7 col-md-offset-1 col-sm-8 col-xs-8" >
            <!--    BETWEEN ME AND SOMEON       -->
 <!--<tags-input ng-show="filterBy.id == 'BetweenMeAnd'" ng-model="filterWith" add-from-autocomplete-only="true"  display-property="name" placeholder="User and/or Group">-->
            <!--<auto-complete source="taskLoadTags($query)"></auto-complete>-->
            <!--</tags-input>-->
            <!--    NEW TASK        -->
            <div class="new-task-area container-fluid">
                <div class='row'>
                    <div class='col-lg-3 col-md-3'>
                        <p class='new-task-area-heading text-muted p-margin-0'></p> 
                    </div>
                    <div class='col-lg-9 col-md-9'>
                        <div class="form-group">
                            <div class="input-group">
                                <input class="form-control input-sm g-search-box" type="text" placeholder="Write your task and hit enter to save it instantly" ng-model="TaskSelected.Task.title" ng-enter="taskAdd(TaskSelected)">
                                <div class="input-group-addon-round task-more-options input-group-addon btn" data-toggle="modal" data-target="#taskAddModal" ng-click="taskNewInit();" ><i class="fa fa-gear"></i> More options</div>
                            </div>
                        </div>        
                    </div>
                </div>
            </div>
            <!--    CALENDAR        -->
            <div class="calendar-wrap">
                <div class="calendar" ng-model="eventSources" calendar-watch-event="extraEventSignature" calendar="myCalendar1" config="uiConfig.calendar" ui-calendar="uiConfig.calendar"></div>
            </div>
        </div>

        <div class="planner-side-tasks-area col-lg-3 col-lg-offset-1 col-md-3 col-md-offset-1 col-sm-4 col-xs-4">
            <!-- task options       -->
            <div class="container-fluid g-desk-tasks-area" >
                <div class="row">
                    <div class="planner-search-box col-lg-8 col-md-8" >
                        <div class="form-group">
                            <div class="input-group">
                                <!--    SEARCH      -->
                                <input class="form-control input-sm g-search-box" type="text" ng-model="search" placeholder='search task'>
                                <div class="input-group-addon-round input-group-addon btn" ><span class='fa fa-search'></span></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4">
                        <div class="btn-group btn-group-sm">
                            <span type="button" class="btn-link dropdown-toggle" data-toggle="dropdown">{{filterBy.name}} <span class="caret"></span></span>
                            <ul class="dropdown-menu dropdown-menu-right" role="menu">
                                <li ng-repeat="filterOption in TaskfilterOptions">
                                    <a href="#" ng-click="selectFilter(filterOption.id)" ng-bind-html="filterOption.name"></a>
                                </li>
                            </ul>    
                        </div>
                    </div>
                </div>
                <div class="container-fluid">
                    <?php echo $this->element('Task/view') ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php echo $this->element('Task/new_task') ?>
      
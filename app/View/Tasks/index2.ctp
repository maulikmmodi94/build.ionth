<?php
$this->layout = 'gridle_in';
$this->set('title_for_layout', 'Planner');

/* for calendar */

echo $this->Html->script('jquery-ui');
echo $this->Html->script('jquery.validate');
echo $this->Html->script('bootstrap-datetimepicker');

echo $this->Html->css('bootstrap-datetimepicker');
?>
<div class="container" ng-controller="taskController">
    <div class="g-planner-wrap row ">

        <!--    NEW TASK        -->
        <div class="new-task-area container-fluid">
            <div class='row'>
                <div class='col-xs-6'>
                    <div class="form-group">
                        <div class="input-group">
                            <input class="form-control input-sm g-search-box" type="text" placeholder="Write your task and hit enter to save it instantly" ng-model="TaskSelected.Task.title" ng-enter="taskAdd(TaskSelected)" style="border-radius: 0;">
                            <div class="input-group-addon-round task-more-options input-group-addon btn"  style="border-radius: 0;" data-toggle="modal" data-target="#taskAddModal" ng-click="taskNewInit();" ><i class="fa fa-gear"></i> More options</div>
                        </div>
                    </div>        
                </div>
                <div class="planner-search-box col-xs-2" >
                    <div class="form-group">
                        <div class="input-group">
                            <!--    SEARCH      -->
                            <input class="form-control input-sm g-search-box"  style="border-radius: 0;" type="text" ng-model="search" placeholder='search task'>
                            <div class="input-group-addon-round input-group-addon btn" style="border-radius: 0;" ><span class='fa fa-search'></span></div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-2 text-center">
                    <div class="btn-group btn-group-sm">
                        <button type="button" class="btn btn-warning dropdown-toggle" data-toggle="dropdown">{{filterBy.name}} <span class="caret"></span></button>
                        <ul class="dropdown-menu dropdown-menu-right" role="menu">
                            <li ng-repeat="filterOption in TaskfilterOptions">
                                <a href="#" ng-click="selectFilter(filterOption.id)" ng-bind-html="filterOption.name"></a>
                            </li>
                        </ul>    
                    </div>
                   

                </div>
                <div class="col-xs-2 text-center">
                     <!--    BETWEEN ME AND SOMEON       -->
                    <tags-input ng-show="filterBy.id == 'BetweenMeAnd'" ng-model="filterWith" add-from-autocomplete-only="true"  display-property="name" placeholder="User and/or Group">
                        <auto-complete source="taskLoadTags($query)"></auto-complete>
                    </tags-input>
                </div>
                <div class="col-lg-12">
                    <?php echo $this->element('Task/view_1') ?>
                </div>
            </div>
        </div>


        <div class="planner-side-tasks-area col-lg-3 col-lg-offset-1 col-md-3 col-md-offset-1 col-sm-4 col-xs-4">
            <!-- task options       -->
            <div class="container-fluid" >
                <div class="row">


                </div>

            </div>
        </div>
    </div>
</div>
<?php echo $this->element('Task/new_task') ?>
      
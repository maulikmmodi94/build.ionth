<?php
/**
 * NetworkPlanFixture
 *
 */
class NetworkPlanFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 9, 'unsigned' => false, 'key' => 'primary'),
		'network_id' => array('type' => 'integer', 'null' => false, 'default' => '0', 'length' => 9, 'unsigned' => false, 'key' => 'index'),
		'plan_id' => array('type' => 'integer', 'null' => false, 'default' => '0', 'length' => 9, 'unsigned' => false, 'key' => 'index'),
		'start_date' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'end_date' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1),
			'BY_NETWORK_ID' => array('column' => 'network_id', 'unique' => 0),
			'BY_PLAN_ID' => array('column' => 'plan_id', 'unique' => 0)
		),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'network_id' => 1,
			'plan_id' => 1,
			'start_date' => '2014-08-11 17:41:50',
			'end_date' => '2014-08-11 17:41:50'
		),
	);

}

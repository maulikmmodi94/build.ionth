<?php
/**
 * PlanFixture
 *
 */
class PlanFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 9, 'unsigned' => false, 'key' => 'primary'),
		'price' => array('type' => 'integer', 'null' => false, 'default' => '0', 'length' => 9, 'unsigned' => false),
		'name' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 40, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'user_limit' => array('type' => 'integer', 'null' => false, 'default' => '0', 'length' => 9, 'unsigned' => false),
		'storage_limit' => array('type' => 'integer', 'null' => false, 'default' => '0', 'length' => 14, 'unsigned' => false),
		'time_limit' => array('type' => 'integer', 'null' => false, 'default' => '0', 'length' => 9, 'unsigned' => false),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'price' => 1,
			'name' => 'Lorem ipsum dolor sit amet',
			'user_limit' => 1,
			'storage_limit' => 1,
			'time_limit' => 1
		),
	);

}

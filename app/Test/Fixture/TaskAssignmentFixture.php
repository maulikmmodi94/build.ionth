<?php
/**
 * TaskAssignmentFixture
 *
 */
class TaskAssignmentFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 9, 'unsigned' => false, 'key' => 'primary'),
		'task_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 9, 'unsigned' => false),
		'is_completed' => array('type' => 'boolean', 'null' => false, 'default' => '0'),
		'user_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 9, 'unsigned' => false, 'key' => 'index'),
		'group_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 9, 'unsigned' => false),
		'network_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 9, 'unsigned' => false, 'key' => 'index'),
		'role' => array('type' => 'string', 'null' => false, 'default' => 'user', 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'is_submitted' => array('type' => 'boolean', 'null' => false, 'default' => '0'),
		'status' => array('type' => 'string', 'null' => false, 'default' => 'active', 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'isDeleted' => array('type' => 'boolean', 'null' => false, 'default' => '0'),
		'created' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'deleted' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'modified' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1),
			'BY_USER_ID' => array('column' => 'user_id', 'unique' => 0),
			'BY_NETWORK_ID' => array('column' => 'network_id', 'unique' => 0)
		),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'task_id' => 1,
			'is_completed' => 1,
			'user_id' => 1,
			'group_id' => 1,
			'network_id' => 1,
			'role' => 'Lorem ipsum dolor sit amet',
			'is_submitted' => 1,
			'status' => 'Lorem ipsum dolor sit amet',
			'isDeleted' => 1,
			'created' => '2014-07-07 12:28:36',
			'deleted' => '2014-07-07 12:28:36',
			'modified' => '2014-07-07 12:28:36'
		),
	);

}

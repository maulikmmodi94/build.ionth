<?php
App::uses('TaskAssignment', 'Model');

/**
 * TaskAssignment Test Case
 *
 */
class TaskAssignmentTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.task_assignment',
		'app.task',
		'app.user',
		'app.group',
		'app.network',
		'app.user_network',
		'app.user_info',
		'app.users_group'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->TaskAssignment = ClassRegistry::init('TaskAssignment');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->TaskAssignment);

		parent::tearDown();
	}

}

<?php
App::uses('NetworkPlan', 'Model');

/**
 * NetworkPlan Test Case
 *
 */
class NetworkPlanTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.network_plan',
		'app.network',
		'app.user_network',
		'app.user',
		'app.user_info',
		'app.group',
		'app.users_group',
		'app.plan'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->NetworkPlan = ClassRegistry::init('NetworkPlan');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->NetworkPlan);

		parent::tearDown();
	}

}

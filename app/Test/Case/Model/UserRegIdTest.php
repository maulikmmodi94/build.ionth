<?php
App::uses('UserRegId', 'Model');

/**
 * UserRegId Test Case
 *
 */
class UserRegIdTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.user_reg_id'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->UserRegId = ClassRegistry::init('UserRegId');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->UserRegId);

		parent::tearDown();
	}

}

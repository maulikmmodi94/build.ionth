<?php
App::uses('TasksController', 'Controller');

/**
 * TasksController Test Case
 *
 */
class TasksControllerTest extends ControllerTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.task',
		'app.user',
		'app.task_assignment',
		'app.group',
		'app.network',
		'app.user_network',
		'app.user_info',
		'app.users_group'
	);

}

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

app.factory('googleService',['$http','$rootScope','$httpParamSerializerJQLike',function($http,$rootScope,$httpParamSerializerJQLike){
        var factory={};
        
        factory.signupform = function(){
            
        };
        
        return factory;
}]);

app.controller('googleController', ['$rootScope', '$scope', '$http', function ($rootScope, $scope, $http) {
        $scope.googleForm = function (obj) {
            //if direct action doesnot work try this with DOM  
            //Google post here
            var d = new Date();
            var offset = d.getTimezoneOffset();
            $scope.data = {};
            $scope.data.UserInfo = {};
            $scope.data.User = {};
            $scope.data.UserInfo.first_name = obj[0];
            $scope.data.UserInfo.last_name = obj[1];
            $scope.data.User.email = obj[2];
            $scope.data.User.password = null;
            $scope.data.User.timezone = offset;
//            var form = document.getElementById("UserAddForm");
            
            $scope.$apply();
            //     form.submit();
            var formdata = $scope.data;
            var xsrf = $.param({data : formdata});
            
                $http({
                    method: 'POST',
                    url: '/users/add_google/',
                    data: xsrf
                }).success(function (data, header) {
                    if(data.redirect){
                        window.open(data.url,"_self");
                    }
                }).error(function (data, header) {
                    notify(data.message, header);
                });
            
        };

//        $scope.formsubmit = function () {
//            var form = document.getElementById('UserAddForm');
//            form.submit();
//        };

        $scope.login = function ()
        {
            console.log('hii')
            var myParams = {
                'clientid': '664463812333-sourro2d3gm5a790821e4r0tjqrb9be6.apps.googleusercontent.com',
                'cookiepolicy': 'single_host_origin',
                'callback': 'loginCallback',
                'approvalprompt': 'auto',
                'scope': 'https://www.googleapis.com/auth/plus.login https://www.googleapis.com/auth/plus.profile.emails.read https://www.googleapis.com/auth/plus.circles.read https://www.googleapis.com/auth/userinfo.email'
            };
            gapi.auth.signIn(myParams);
        };
        //all the logic after clicking and enetering data into google is inside

        loginCallback = function (result) {
            $scope.loginCallback(result);
        };

        $scope.loginCallback = function (result)
        {
            if (result['status']['signed_in'])
            {
                if(result['status']['method'] == 'PROMPT'){
                var request = gapi.client.plus.people.get(
                        {
                            'userId': 'me'
                        });
//                console.log(request);
                
//                    var request1 = gapi.client.plus.people.list({
//                        'userId': 'me',
//                        'collection': 'visible'
//                    });
//                    request1.execute(function (resp) {
//                        console.log(resp);
//                    });
                
                request.execute(function (resp)
                {
                    var obj = [];
                    obj[0] = resp['name']['givenName'];
                    obj[1] = resp['name']['familyName'];
                    obj[2] = resp['emails'][0].value;
                    $scope.googleForm(obj);
//                     console.log(resp);
                });
                }
            }
        };

        $scope.onLoadCallback = function () {
            gapi.client.setApiKey('AIzaSyCorSP1JpIaUGhVwbXNPKKzf8PT4FUERD8');
            gapi.client.load('plus', 'v1', function () {
            });
//            gapi.client.load('oauth2', 'v2', function() {
//                
//            });
        };

        onLoadCallback = function ()
        {
            $scope.onLoadCallback();
        }

    }]);

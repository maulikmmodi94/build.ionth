
app.factory('fileService',['$http','$q','$location','commentService' ,function ($http, $q, $location ,commentService) {
    
    var factory={};
    //Returns Index of group in $rootScope.groups
    factory.findFile = function (id) {
        for (var i in $rootScope.files) {
            if ($rootScope.files[i]['Doc']['id'] == id) {
                return i;
            }
        }
        return -1;
    };

    factory.getFile = function (id) {
        $http({
            method: 'Post',
            url: '/docs/getDoc/' + id + '.json'
        }).success(function (data, header) {
            $rootScope.files[0] = data.data;
            $rootScope.fileCurrentIndex = 0;
            if (typeof callback === "function") {
                callback();
            }
        })
    };
    
    factory.getFilesData = function () {
        var filesDataPromise;
        filesDataPromise = $http({
            method: 'GET',
            url: '/docs/getFiles.json'
        });
        return filesDataPromise;
    };
    
    factory.fileModalOpenManage = function () {
        $('#fileManageModal').modal('show');
    }
    factory.fileModalUpload = function () {
        $('#fileUploadModal').modal({backdrop: 'static'});
    }

    factory.fileEditBtn = function (id, name, oldname) {
        var data = {};
        if (name !== oldname)
        {
            data.name = name;
            data.doc_id = id;
            var xsrf = $.param({data: data});
            $http({
                method: 'POST',
                url: '/docs/edit.json',
                data: xsrf
            }).success(function (data, header) {
                $rootScope.fileDataEdit(id, data.data['Doc']);
                notify(data.message, header);
            }).error(function (data, header) {
                notify(data.message, header);
            });
        }
    };
    $rootScope.filesAddMembers = function (id, data) {
        if (data.userlist.length > 0) {
            data.doc_id = id;
            $rootScope.tags = [];
            $('#addmemberbtn').button('loading');
            var xsrf = $.param({data: data});
            $http({
                method: 'POST',
                url: '/docs/addSharedList.json',
                data: xsrf
            }).success(function (data, header) {
                $rootScope.fileDataReplace(id, data.data);
                $('#addmemberbtn').html('Add member');
                notify(data.message, header);
            }).error(function (data, header) {
                notify(data.message, header);
            }).then(function () {
                $('#addmemberbtn').button('reset');
            })
        }
    }

    $rootScope.filesRemoveMember = function (id, type, value) {
        var data = {};
        data.doc_id = id;
        data.value = value;
        data.type = type;
        var xsrf = $.param({data: data});
        $http({
            method: 'POST',
            url: '/docs/removeSharedList.json',
            data: xsrf
        }).success(function (data, header) {
            $rootScope.fileDataReplace(id, data.data);
            notify(data.message, header);
        }).error(function (data, header) {
            notify(data.message, header);
        }).then(function () {
        })
    }

    $rootScope.fileTagsAutocompleteLoad = function (id) {
        var fileTags = [];
        if (arguments.length > 0) {
            var users = $rootScope.files[$rootScope.findFile(id)]['DocAssignment'];
            for (var i in users) {
                if (users[i].user_id !== null)
                    fileTags.push({'value': users[i].user_id, 'type': 'user'});
                else if (users[i].group_id !== null)
                    fileTags.push({'value': users[i].group_id, 'type': 'group'});
            }
            var fileTags = $rootScope.getDifference($rootScope.taglist, fileTags);
            var deferred = $q.defer();
            deferred.resolve(fileTags);
            return deferred.promise;
        } else {
            var fileTags = $rootScope.getDifference($rootScope.taglist, fileTags);
            var deferred = $q.defer();
            deferred.resolve(fileTags);
            return deferred.promise;
        }
    }

    $rootScope.options = {
        // Required. Called when a user selects an item in the Chooser.
        success: function (files) {
            $rootScope.newFile = files[0];
            $rootScope.newFile.provider = 'dropbox';
            $rootScope.newFile.preview_url = $rootScope.newFile.link;
            $rootScope.newFile.size = $rootScope.newFile.bytes;
            $rootScope.newFile.url = $rootScope.newFile.link.replace(/(dl=)[^\&]+/, '$1' + 1);
            $rootScope.$apply();
        },
        // Optional. Called when the user closes the dialog without selecting a file
        // and does not include any parameters.
        cancel: function () {
            $rootScope.newFile = [];
            $rootScope.$apply();
            console.log('No file selected from dropbox')
        },
        // Optional. "preview" (default) is a preview link to the document for sharing,
        // "direct" is an expiring link to download the contents of the file. For more
        // information about link types, see Link types below.
        linkType: "preview", // or "direct"

        // Optional. A value of false (default) limits selection to a single file, while
        // true enables multiple file selection.
        multiselect: false, // or true

        // Optional. This is a list of file extensions. If specified, the user will
        // only be able to select files with these extensions. You may also specify
        // file types, such as "video" or "images" in the list. For more information,
        // see File types below. By default, all extensions are allowed.
        extensions: ['.pdf', '.doc', '.docx', '.jpeg', '.png', '.jpg', '.zip', '.svg'],
    };

    $rootScope.DropboxUpload = function ()
    {
        console.log('upload called');
        Dropbox.choose($rootScope.options);
    }

    $rootScope.Upload = function ()
    {
        var data = {};
        data.Doc = $rootScope.newFile;
        data.userlist = $rootScope.tags;
        var xsrf = $.param({data: data});
        $http({
            method: 'POST',
            url: '/docs/xUpload.json',
            data: xsrf
        }).success(function (data, header) {
            $rootScope.fileDataAdd(data.data);
            notify(data.message, header);
            $('#fileUploadModal').modal('hide');

        }).error(function (data, header) {
            notify(data.message, header);
        }).then(function ()
        {
            $rootScope.newFile = [];
            $rootScope.tags = [];
            if (typeof callback === "function") {
                callback();
            }
        });




    }

    $(document).ready(function () {
        // Prevent Dropzone from auto discovering this element:
        Dropzone.options.myAwesomeDropzone = false;
        // Disable auto discover for all elements:
        Dropzone.autoDiscover = false;
        var myDropzone = new Dropzone('#my-awesome-dropzone', {
            url: '/docs/upload.json',
            method: 'Post',
            autoProcessQueue: false,
            maxFilesize: 20, //MB
            addRemoveLinks: true, //cancel upload butotn
            maxFiles: 10, //maximum number of files to be allowed at once
            parallelUploads: 10,
            dictInvalidFileType: 'File type not supported',
            dictFileTooBig: "File size limit exceeds. Please upload files upto 1 MB.",
            accept: function (file, done) {
                if (file) {
                    $('#dropzone_backdrop').hide();
                    console.log("called");
                    done();
                }
            }
        });
        $('#fileUploadBtn').on('click', function (e) {
            e.preventDefault();
            if (myDropzone.getQueuedFiles().length != 0 && myDropzone.getUploadingFiles().length == 0) {
                $('#fileUploadShareInput').attr('hidden', 'hidden');
                $('#fileUploadShareDisplay').attr('hidden', false);
                $('#fileUploadBtn').button('loading');
                myDropzone.processQueue();
            }
        });
        myDropzone.on("sending", function (file, xhr, formData) {
            if (($rootScope.tags).length > 0)
                for (var i in $rootScope.tags) {
                    formData.append("userlist[" + i + "][value]", $rootScope.tags[i]['value']);
                    formData.append("userlist[" + i + "][name]", $rootScope.tags[i]['name']);
                    formData.append("userlist[" + i + "][type]", $rootScope.tags[i]['type']);
                }
        });
        myDropzone.on("complete", function (file) {
            var response = JSON.parse(file.xhr.response);
            var responsestatuscode = JSON.parse(file.xhr.status);
//            console.log(responsestatuscode);
            if (responsestatuscode >= 200 && responsestatuscode < 300) {
                $rootScope.fileDataAdd(response.data);
                myDropzone.removeFile(file);
                if (myDropzone.getQueuedFiles().length == 0 && myDropzone.getUploadingFiles().length == 0) {
                    $('#fileUploadShareInput').attr('hidden', false);
                    $('#fileUploadShareDisplay').attr('hidden', 'hidden');
                    fileModalUploadClose();
                }
                $rootScope.$apply();
                trackEvents.trackFileUpload('File-Upload', {file_name: response.data.Doc.name, type: response.data.Doc.file_extension, shared_count: response.data.DocAssignment.length - 1});

            } else {
                file.previewElement.classList.add("dz-error");
                var _ref = file.previewElement.querySelectorAll("[data-dz-errormessage]");
                var _results = [];
                for (_i = 0, _len = _ref.length; _i < _len; _i++) {
                    var node = _ref[_i];
                    _results.push(node.textContent = response.message);
                }
                return _results;
            }
            notify(response.message, {type: response.status});
        });
        myDropzone.on("queuecomplete", function () {
            $('#fileUploadBtn').button('reset');
            $('#fileUploadShareInput').attr('hidden', false);
            $('#fileUploadShareDisplay').attr('hidden', 'hidden');
            $rootScope.tags = [];
        });
        fileModalUploadClose = function () {
            if (myDropzone.getUploadingFiles().length == 0) {
                $('#fileUploadModal').modal('hide');
            }
        }


    });
    $rootScope.fileDownload = function (id) {
        if (id) {
            $http({
                method: 'POST',
                url: '/docs/download/' + id + '.json'
            }).success(function (data, header) {
                window.location.href = data.data;
            }).error(function (data, header) {
                notify(data.message, header);
            });
        }
    }

    $rootScope.fileDelete = function (id) {
        var data = {};
        var xsrf = $.param({data: data});
        $http({
            method: 'POST',
            url: '/docs/delete/' + id + '.json'
        }).success(function (data, header) {
            var index = $rootScope.findFile(id);
            $rootScope.files.splice(index, 1);
            $rootScope.fileInitSelectedItem();
            notify(data.message, header);
        }).error(function (data, header) {
            notify(data.message, header);
        });
    };
    /* Data Manipulation functions */
    $rootScope.fileDataAdd = function (data) {
        if ($rootScope.files)
            $rootScope.files.push(data);
    };
    $rootScope.fileDataEdit = function (id, data) {
        var index = $rootScope.findFile(id);
        $rootScope.files[index]['Doc'] = data;
    };
    $rootScope.fileDataEditUsers = function (id, data) {
        var index = $rootScope.findFile(id);
        $rootScope.files[index]['DocAssignment'] = data;
    };
    $rootScope.fileDataReplace = function (id, data) {
        var index = $rootScope.findFile(id);
        $rootScope.files[index] = data;
    };
    //Modal Windows
    $rootScope.fileModalDelete = function (id, name) {
        $('#fileManageModal').modal('hide');
        createDialog(null, {
            id: 'filedeletemodal',
            title: 'delete file',
            deleteleave: 0,
            string: 'Are you sure you want to delete file ' + '<span class=filenamemodal>' + name + '</span> ?',
            backdrop: true,
            success: {label: 'Yes, Delete', fn: function () {
                    $rootScope.fileDelete(id);
                }},
            cancel: {label: 'Cancel', fn: function () {
                    $('#fileManageModal').modal('show');
                }}
        });
    };
    $rootScope.fileModalMemberRemove = function (name, id, type, value, user_name) {
        createDialog(null, {
            id: 'filemodalmemberremove',
            title: 'Remove Member',
            deleteleave: 0,
            string: 'Are you sure you want to unshare file ' + '<span class=filenamemodal>' + name + '</span> with ' + user_name + '?',
            backdrop: true,
            success: {label: 'Yes, Delete', fn: function () {
                    $rootScope.filesRemoveMember(id, type, value);
                }},
            cancel: {label: 'Cancel', fn: function () {
                }}
        });
    };
    $rootScope.fileType = function (mime, files) {
        var res = mime.split("/");
        switch (res[0]) {
            case 'image':
                return 'image';
                break;
            case 'audio':
                return 'audio';
                break;
            case 'video':
                return 'image';
                break;
            case 'text':
                return 'text';
                break;
            default:
                return 'other';
                break;
        }
    };

    factory.addComment = function (id, body) {
        if (body && body.trim()) {
            commentService.add('docs', id, body)
                    .success(function (data, header) {
                        var index = $rootScope.findFile(id);
                        $rootScope.files[index]['Comment'] = ($rootScope.files[index]['Comment']).concat(data.data);
                        $rootScope.files[index]['Doc']['comment_count'] = $rootScope.files[index]['Doc']['comment_count'] + 1;
                    })
                    .error(function (data, header) {
                        notify(data.message, header);
                    })
        }
    }

    factory.deleteCommentModal = function (id, commentId) {
        createDialog(null, {
            id: 'filecommentdelete',
            title: 'Delete Comment',
            deleteleave: 0,
            string: 'Are you sure you want to delete comment?',
            backdrop: true,
            success: {label: 'Yes, Delete', fn: function () {
                    fact.deleteComment(id, commentId);
                }},
            cancel: {label: 'Cancel', fn: function () {
                }}
        });
    }

    factory.deleteComment = function (id, commentId) {
        if (commentId) {
            commentService.delete(commentId)
                    .success(function (data, header) {
                        var index = $rootScope.findFile(id);
                        $rootScope.files[index]['Comment'] = ($rootScope.files[index]['Comment']).filter(function (comment) {
                            return comment.id != commentId;
                        });
                        $rootScope.files[index]['Doc']['comment_count'] = $rootScope.files[index]['Doc']['comment_count'] - 1;
//                        notify(data.message, header);
                    })
                    .error(function (data, header) {
                        notify(data.message, header);
                    })
        }
    }

    factory.getComment = function (id, lastId) {
        if (angular.isUndefined(lastId))
            var lastId = null;

        //Check loadmore variable exists and is false
        var index = $rootScope.findFile(id);
        if (angular.isUndefined($rootScope.files[index]['moreComments']) || $rootScope.files[index]['moreComments'] !== false) {
            commentService.get('docs', id, lastId)
                    .success(function (data, header) {
                        $rootScope.files[index]['Comment'] = ($rootScope.files[index]['Comment']).concat(data.data.comments);
                        $rootScope.files[index]['moreComments'] = data.data['loadMore'];
                    })
                    .error(function (data, header) {
                        notify(data.message, header);
                    })
        }
    }

    factory.filePreviewUrl = null;
    factory.setPreviewUrl = function (doc) {
         $('#framebench-modal').modal('show');
        $http({
            method: 'GET',
            url: '/docs/preview/' + doc.id + '.json',
            cache: true
        }).success(function (data, header) {
            $rootScope.files[$rootScope.findFile(doc.id)].Doc.preview_url = data.data;
            fact.filePreviewUrl = data.data;
        }).error(function (data, header) {
            notify(data.message, header);
        });
    };

    factory.frameBenchMimes = function (mime) {
        var supported_mimes = ['image/gif', 'image/jpeg', 'image/jpg', 'image/png', 'image/tiff', 'image/vnd.adobe.photoshop', 'application/pdf', 'video/mp4', 'application/mp4', 'video/x-m4v', 'application/vnd.ms-powerpoint', 'application/vnd.openxmlformats-officedocument.presentationml.presentation', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document', 'application/msword', 'application/vnd.ms-excel', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'];
        if (supported_mimes.indexOf(mime) > -1)
            return true;
        else
            return false;
    };


    return factory;
}]);


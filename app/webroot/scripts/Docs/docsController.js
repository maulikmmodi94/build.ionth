app.controller('docsController', ['$rootScope', '$scope', '$http', 'docsService','$modal', function ($rootScope, $scope, $http, docsService,$modal) {
    
    $scope.files={};
    $scope.showSearch=false;
    
        $scope.getFilesData = function () {
        
        var filesDataPromise = docsService.getFilesData();
        filesDataPromise.success(function (data, header) {
            $scope.files = data.data['files'];
        });
        console.log($scope.files);
    };
      
      $scope.getFilesData();  
        
    //Modal Windows Start
    
    $scope.openModalShareDoc = function (size) {

        var modalInstance = $modal.open({
            templateUrl: '/js/modules/docs/shareDoc.html',
            controller: 'ModalInstanceCtrl',
            size: size,
            scope: $scope
        });

    };
    
    //Modal windows End
}]);



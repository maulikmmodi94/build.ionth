app = angular.module('GridleApp', ['ui.bootstrap','ngSanitize','angular-growl']);

app.config(['$compileProvider','growlProvider', function ($compileProvider,growlProvider){
        $compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|file|skype):/);
        growlProvider.globalDisableCountDown(true);
        growlProvider.globalDisableIcons(true);
        growlProvider.globalTimeToLive({success: 1000, error: 2000, warning: 3000, info: 3000});
}]);

app.run(['$http',function($http){
        $http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded';
        $http.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
        $http.defaults.headers.common['Accept'] = 'application/json';
}]);

app.controller('emptyFormsController', ['$scope','growl', function ($scope,growl) {
                 
                $scope.growlNotify = function (message){
                     growl.info(message);
                };
}]);

app.factory('googleService',['$http','$rootScope','$httpParamSerializerJQLike',function($http,$rootScope,$httpParamSerializerJQLike){
        var factory={};
        
        factory.signupform = function(obj){
            var googleSignupPromise;
            var d = new Date();
            var offset = d.getTimezoneOffset();
            var data={};
            data.UserInfo = {};
            data.User = {};
            data.UserInfo.first_name = obj[0];
            data.UserInfo.last_name = obj[1];
            data.User.email = obj[2];
            data.User.password = null;
            data.User.timezone = offset;
            var xsrf = $httpParamSerializerJQLike({data : data});
            googleSignupPromise = $http({
                    method: 'POST',
                    url: '/users/add_google/',
                    data: xsrf
                });
              return googleSignupPromise;  
        };
        
        return factory;
}]);

app.controller('googleController', ['$rootScope', '$scope', '$http','googleService', function ($rootScope, $scope, $http,googleService) {
        $scope.googleForm = function (obj) {
            //if direct action doesnot work try this with DOM  
            //Google post here
            var signupGooglePromise;
               signupGooglePromise= googleService.signupform(obj);
                signupGooglePromise.success(function (data, header) {
                    console.log(header);
                    if(header == 200){
                        window.open('/desks',"_self");
                    }
                }).error(function (data, header) {
                    growlNotify(data.message, header);
                });
        };

        $scope.login = function ()
        {
            var myParams = {
                'clientid': '715317634063-48ob0oicirgndvp4d3fh7c0ao0fgnt8c.apps.googleusercontent.com',
                'cookiepolicy': 'single_host_origin',
                'callback': 'loginCallback',
                'approvalprompt': 'auto',
                'scope': 'https://www.googleapis.com/auth/plus.login https://www.googleapis.com/auth/plus.profile.emails.read https://www.googleapis.com/auth/plus.circles.read https://www.googleapis.com/auth/userinfo.email'
            };
            gapi.auth.signIn(myParams);
        };
        //all the logic after clicking and enetering data into google is inside

        loginCallback = function (result) {
            $scope.loginCallback(result);
        };

        $scope.loginCallback = function (result)
        {
            if (result['status']['signed_in'])
            {
                if(result['status']['method'] == 'PROMPT'){
                var request = gapi.client.plus.people.get(
                        {
                            'userId': 'me'
                        });
//                console.log(request);
                
//                    var request1 = gapi.client.plus.people.list({
//                        'userId': 'me',
//                        'collection': 'visible'
//                    });
//                    request1.execute(function (resp) {
//                        console.log(resp);
//                    });
                
                request.execute(function (resp)
                {
                    var obj = [];
                    obj[0] = resp['name']['givenName'];
                    obj[1] = resp['name']['familyName'];
                    obj[2] = resp['emails'][0].value;
                    $scope.googleForm(obj);
//                     console.log(resp);
                });
                }
            }
        };

        $scope.onLoadCallback = function () {
            gapi.client.setApiKey('AIzaSyDMeHoBMUOAiqdg3OpI4KjiXdw-NiYeVBY');
            gapi.client.load('plus', 'v1', function () {
            });
//            gapi.client.load('oauth2', 'v2', function() {
//                
//            });
        };

        onLoadCallback = function ()
        {
            $scope.onLoadCallback();
        }

}]);

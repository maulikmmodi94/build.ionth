/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


app = angular.module('GridleApp', ['ui.bootstrap']);

app.run(['$rootScope', '$http', '$scope', function ($rootScope, $http , $scope) {

        $rootScope.users = [];
        $rootScope.groups = [];
        $rootScope.networks = [];
        $rootScope.networks_expired = [];
        $rootScope.currentnetwork;
        $rootScope.user_networks = [];
        $rootScope.network_users = [];
        /* Set Default Headers */
        $http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded';
        /* Set headers as Ajax  */
        $http.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
        $http.defaults.headers.common['Accept'] = 'application/json';
        $scope.runService = runService;
        
        //params will be inserted if required
        $rootScope.userPromise = $scope.runService.getUsersData();
                $rootScope.userPromise.success(function (data, header) {
                $rootScope.users = data.data['users'];
                $rootScope.loggeduser = data.data['loggedUser'];
                $rootScope.network_id = data.data['network_id'];
                
//                $rootScope.users = _.each($rootScope.users, function (singleUser) {
//                    return $rootScope.people.push({id: singleUser.UserInfo.user_id, name: singleUser.UserInfo.name, avatar: singleUser.UserInfo.vga, type: 'user'});
//                });

            }).error(function (data, header) {
                notify(data.message, header);

            });
            
          $rootScope.groupPromise = $scope.runService.getGroupsData();
             $rootScope.groupPromise.success(function (data, header) {
                $rootScope.groups = data.data['groups'];
            }).error(function(data,header){
                notify(data.message,header);
            });
        
//        $rootScope.getNetworksData = function (callback) {
//            $rootScope.networkPrmise = $http({
//                method: 'GET',
//                url: '/datas/getNetworks.json'
//            }).success(function (data, header) {
//                $rootScope.networks = data.data['networks'];
//                $rootScope.networks_expired = data.data['networks_expired'];
//                $rootScope.currentnetwork = data.data['currentnetwork'];
//                if (typeof callback === "function") {
//                    callback();
//                }
//
//            });
//        };
        
        $rootScope.userandNetworkPromise = $scope.runService.getUserandNetworkData();
                $rootScope.userandNetworkPromise.success(function (data) {
                $rootScope.user_networks = data.data['user_networks'];
                $rootScope.network_users[$rootScope.currentnetwork] = data.data['user_networks'];
            });

    }]);

app.controller('DropdownCtrl',['$scope' ,function ($scope) {
  
    $scope.status = {
        isopen: false
    };

    $scope.toggled = function (open) {
        
    };

    $scope.toggleDropdown = function ($event) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.status.isopen = !$scope.status.isopen;
    };
}]);

app.controller('toggleCtrl',['$scope',function($scope){
    
    $scope.showNotifications = false;
    
    $scope.toggleNotifications = function(){
        $scope.showNotifications = !$scope.showNotifications;
        console.log($scope.showNotifications);
    };
    
}]);


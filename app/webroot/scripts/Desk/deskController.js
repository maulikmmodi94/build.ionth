/* 
 * Desk Module
 */

angular.module('desk', ['comment'])
        .controller('deskController', ['$rootScope', '$scope', 'deskService','$q','createDialog','$timeout', '$modal','runService','trackEvents',function ($rootScope, $scope, deskService,$q,createDialog,$timeout,$modal,runService,trackEvents) {
                $scope.deskService = deskService;
                $rootScope.TaskData={};
                
                var options = {upcoming: 'other', completed: 'completed', pending: 'pending',tomorrow:'tomorrow',today:'today',next_week:'next_week',this_week:'this_week'};
                
                $scope.taskAdd = function (data) {
                    if (angular.equals({}, data))
                    {
                        return false;
                    }
                    if (data.Task.id)
                    {
                        $scope.taskEdit(data);
                    }
                        deskService.taskAdd(data).success(function (data,header) {
                            var result = $scope.addTasktoArray(data.data[0]);
                            notify(data.message, header);
                            trackEvents.trackTaskAdd('Task-Add', {title: data.data[0].Task.title, shared_count: data.data[0].Tags.length - 1});

                        }).error(function (data, header) {
                            notify(data.message, header);
                        });
                        return true;
                };
                
                $scope.addTasktoArray = function(data){
                    var end_date = $rootScope.momentDate(data.Task.end_date);
                    var end_week = moment(data.Task.end_date).week();
                    var this_week = moment().week();
                    if(this_week == end_week){
                     if(end_date == 'Today'){
                         $rootScope.TaskData.Today.push(data);
                     }else if(end_date == 'Tomorrow'){
                         $rootScope.TaskData.Tomorrow.push(data);
                     }else{
                         $rootScope.TaskData.This_week.push(data);
                     }   
                     return true;
                    }
                    else{
                     $rootScope.TaskData.Other.push(data);
                     return true;
                    }
                    return false;
                };
                

                $scope.taskEdit = function (data) {
                    if ($('#form-new-task').valid()) //find an angular way
                    {
                        if (typeof data.TaskAssignment == 'undefined' || (data.TaskAssignment.assignee_data).length == 0) {
                            delete data.TaskAssignment;
                        }
                        delete data.Comment;
                        var task_id = data.Task.id;
                        deskService.taskEdit(data).success(function (data, header) {
                            var index = $rootScope.getTaskById(task_id).index;
                            $rootScope.TaskData[index] = data.data[0];
                            $rootScope.taskNewInit();
                            $rootScope.taskModalClear();
                            notify(data.message, header);
                        }).error(function (data, header) {
                            notify(data.message, header);
                        });
                        return true;
                    }


                    return false;
                };

                $scope.isTaskAdmin = function (task, user_id)
                {
                    var returnStatus = false;
                    if(task.created_by == user_id){
                        returnStatus= true;
                    }
                    return returnStatus;
                };
                
                
                $scope.paginationInit = function(){
                  $scope.thiWeekTaskPage = 1;
                  $scope.otherTaskPage = 1;
                  $scope.completedtaskPage = 1;
                  $scope.pendingtaskPage = 1;
                  $scope.todayTaskPage = 1;
                  $scope.tomorrowTaskPage = 1;
                };
                
                 $rootScope.$on('user_Selected', function (event, args) {
                    $scope.paginationInit();
                    $scope.loadTaskData('user',args.user_id);
                });
                
                 $rootScope.$on('group_Selected', function (event, args) {
                    $scope.paginationInit();
                    $scope.loadTaskData('group',args.id);
                });
                
                 $rootScope.$on('clear_Filter', function (event, args) {
                    $scope.paginationInit();
                    $scope.loadTaskData('user');
                });
                
                $scope.getUpcomingTask = function(options,type,user_id){
                    var taskPage;
                    
                    if(type == 'today'){
                        taskPage = $scope.todayTaskPage;
                    }
                    else if(type == 'tomorrow'){
                        taskPage = $scope.tomorrowTaskPage;
                    }
                    else if(type == 'this_week'){
                        taskPage = $scope.thiWeekTaskPage;
                    }
                    else {
                        taskPage = $scope.otherTaskPage;
                    }
                   
                    var loadedTaskPromise = deskService.taskLoad(options,type,taskPage,user_id);
                    loadedTaskPromise.success(function (data,header) {
                        if (data.data != null)
                        {
                            if(data.type == 'today'){
                                $rootScope.TaskData.Today = data.data;
                                $scope.todayTaskPage++;
                            }
                            else if(data.type == 'tomorrow'){
                                $rootScope.TaskData.Tomorrow = data.data;
                                $scope.tomorrowTaskPage++;
                            }
                            else if(data.type == 'this_week'){
                                $rootScope.TaskData.This_week =data.data;
                                $scope.thiWeekTaskPage++;
                            }
                            else{
                                $rootScope.TaskData.Other =data.data;
                                $scope.otherTaskPage++;
                            }
                            
                            
                        }
                    }).error(function (data, header) {
                        //write a notifications
                    }); 
                };
                
                $scope.getCompletedTask = function(options,type,user_id){
                    if(angular.isUndefined(user_id)){
                    user_id='';
                    }
                   var loadedTaskPromise = deskService.taskLoad(options,type,$scope.completedtaskPage,user_id);
                    loadedTaskPromise.success(function (data,header) {
                        if (data.data != null)
                        {
                            $rootScope.TaskData.Completed = data.data;
                            $rootScope.TaskData.Completed.count = data.count;
                            $scope.completedtaskPage++;
                        }
                    }).error(function (data, header) {
                        //write a notifications
                    }); 
                };
                
                $scope.getPendingTask = function(options,type,user_id){
                    var loadedTaskPromise = deskService.taskLoad(options,type,$scope.pendingtaskPage,user_id);
                    loadedTaskPromise.success(function (data,header) {
                        if (data.data != null)
                        {
                            $rootScope.TaskData.Pending = data.data;
                            $rootScope.TaskData.Pending.count = data.count;
                            $scope.pendingtaskPage++;
                        }
                    }).error(function (data, header) {
                        //write a notifications
                    });
                };
                
                

                /*  load task data from server and store to $rootScope.TaskData */
                $scope.loadTaskData = function (type,id) {
                    $scope.getUpcomingTask(options.today,type,id);
                    $scope.getUpcomingTask(options.tomorrow,type,id);
                    $scope.getCompletedTask(options.completed,type,id);
                    $scope.getPendingTask(options.pending,type,id);
                    $scope.getUpcomingTask(options.this_week,type,id);
                    $scope.getUpcomingTask(options.next_week,type,id);
                };
                
                $scope.loadTaskData('user');
                
//                $scope.scrollcheck = function(){
//                    var type;
//                    var id;
//                    if($rootScope.isSelectedUser == true){
//                        type = 'user';
//                        id=$rootScope.selectedUser.id;
//                    }
//                    else{
//                        type = 'group';
//                        id=$rootScope.selectedGroup.id;
//                    }
//                  if(angular.isUndefined($rootScope.TaskData.Tomorrow)){
//                      $scope.getUpcomingTask(options.tomorrow,type,id);
//                  }
//                  else if(angular.isUndefined($rootScope.TaskData.Tomorrow)){
//                      $scope.getUpcomingTask(options.tomorrow,type,id);
//                  }
//                };
                
                $scope.deleteCommentModal = function (id, commentId,option) {
                    createDialog(null, {
                        id: 'taskcommentdelete',
                        title: 'Delete Comment',
                        deleteleave: 0,
                        string: 'Are you sure you want to delete comment?',
                        backdrop: true,
                        success: {label: 'Yes, Delete', fn: function () {
                                $scope.deleteComment(id, commentId,option);
                            }},
                        cancel: {label: 'Cancel', fn: function () {
                            }}
                    });
                };

                $scope.deleteComment = function (task_id, commentId,option) {
                    var deleteCommentPromise = deskService.deleteComment(task_id, commentId);
                    
                    deleteCommentPromise.success(function (data, header) {
                        var index = $rootScope.getTaskById(task_id,option).index;
                        if(option == 'today'){
                        $rootScope.TaskData.Today[index]['Task']['comment_count']--;
                        $rootScope.TaskData.Today[index]['Comment'] = ($rootScope.TaskData.Today[index]['Comment']).filter(function (comment) {
                            return comment.id != commentId;
                        });
                        }else if(option == 'tomorrow'){
                        $rootScope.TaskData.Tomorrow[index]['Task']['comment_count']--;
                        $rootScope.TaskData.Tomorrow[index]['Comment'] = ($rootScope.TaskData.Tomorrow[index]['Comment']).filter(function (comment) {
                            return comment.id != commentId;
                        });
                        }else if(option == 'this_week'){
                        $rootScope.TaskData.This_week[index]['Task']['comment_count']--;
                        $rootScope.TaskData.This_week[index]['Comment'] = ($rootScope.TaskData.This_week[index]['Comment']).filter(function (comment) {
                            return comment.id != commentId;
                        });
                        }else{
                        $rootScope.TaskData.Other[index]['Task']['comment_count']--;
                        $rootScope.TaskData.Other[index]['Comment'] = ($rootScope.TaskData.Other[index]['Comment']).filter(function (comment) {
                            return comment.id != commentId;
                        });
                        }
                    }).error(function (data, header) {
                                notify(data.message, header);
                      });
                };

                $scope.addComment = function (task_id, body,option) {
                    var addCommentPromise = deskService.addComment(task_id, body);
                    addCommentPromise.success(function (data, header) {
                        var index = $rootScope.getTaskById(task_id,option).index;
                        if(option == 'today'){
                        $rootScope.TaskData.Today[index]['Task']['comment_count']++;
                        $rootScope.TaskData.Today[index]['Comment'] = ($rootScope.TaskData.Today[index]['Comment']).concat(data.data);
                        }else if(option == 'tomorrow'){
                        $rootScope.TaskData.Tomorrow[index]['Task']['comment_count']++;
                        $rootScope.TaskData.Tomorrow[index]['Comment'] = ($rootScope.TaskData.Tomorrow[index]['Comment']).concat(data.data);
                        }else if(option == 'this_week'){
                        $rootScope.TaskData.This_week[index]['Task']['comment_count']++;
                        $rootScope.TaskData.This_week[index]['Comment'] = ($rootScope.TaskData.This_week[index]['Comment']).concat(data.data);
                        }else{
                        $rootScope.TaskData.Other[index]['Task']['comment_count']++;
                        $rootScope.TaskData.Other[index]['Comment'] = ($rootScope.TaskData.Other[index]['Comment']).concat(data.data);
                        }
                        
//                        $rootScope.taskSelectItem(id);
                    }).error(function (data, header) {
                                notify(data.message, header);
                            });
                };
                //get a task by id

                $rootScope.taskNewInit = function ()
                {

                    $rootScope.tags = [];
                    if (typeof $rootScope.TaskSelected.Task != "undefined")
                    {
                        var title = angular.copy($rootScope.TaskSelected.Task.title);
                        $rootScope.TaskSelected = {};
                        $rootScope.TaskSelected.Task = {};
                        $rootScope.TaskSelected.Task.title = title;
                    }
                    else
                    {
                        $rootScope.TaskSelected = {};
                        $rootScope.TaskSelected.Task = {};
                    }
                    
                    $('.task-add-modal').modal('hide');
                }

                $scope.taskRemoveAssignee = function (assignee, task_id) {
                    var remove_data = {};
                    remove_data.value = assignee.value;
                    remove_data.type = assignee.type;
                    var data = {};
                    data.TaskAssignment.remove_data = remove_data;
                    data.TaskAssignment.task_id = task_id;

                    var taskRemovePromise = deskService.removeassignee();
                    taskRemovePromise.success(function (data, header) {
                        var index = $rootScope.getTaskById(task_id);
                        $rootScope.TaskData[index] = data.data[0];
                        $rootScope.taskSelectItem(task_id);
                        notify(data.message, header);
                    }).error(function (data, header) {
                        notify(data.message, header);

                    });
                };

                $scope.taskRemoveAssigneeConfirm = function (assignee, task_id) {
                    var message;
                    if (assignee.type == 'group')
                    {
                        message = 'Are you sure you want to remove <strong>' + $rootScope.groups[$rootScope.findGroup(assignee.value)].Group.name + '</strong> ?';
                    } else
                    {
                        message = 'Are you sure you want to remove <strong>' + $rootScope.users[$rootScope.getUserIndex(assignee.value)].UserInfo.name + '</strong> ?';
                    }

                    createDialog(null, {
                        id: 'removeTaskAssignee',
                        title: 'Remove Assignee',
                        deleteleave: 0,
                        header: false,
                        footer: true,
                        string: message,
                        backdrop: true,
                        success: {label: 'Yes, Remove', fn: function () {
                                $scope.taskRemoveAssignee(assignee, task_id);
                            }},
                        cancel: {label: 'Cancel', fn: function () {

                            }}
                    });
                }

                $rootScope.taskSetCompleted = function (task_id, is_completed, $index) {

                    var markcompletePromise = deskService.markTaskasComplete(task_id, is_completed);

                    markcompletePromise.success(function (data,header) {

                        //Striking through animation on the element
                        if (!is_completed)
                        {
                            $rootScope.task_completing = $index;
                        } else if (is_completed) {
                            $rootScope.task_uncompleting = $index;
                            console.log($rootScope.task_uncompleting);
                        }
                        //Timeout for preventing it to disappear before strike
                        $timeout(function () {
                            var index = $rootScope.getTaskById(task_id).index;
                            $rootScope.TaskData[index] = data.data[0];
                            $rootScope.task_completing = null;
                            $rootScope.task_uncompleting = null;

                        }, 500);

                        notify(data.message, header);

                    }).error(function (data, header) {
                        notify(data.message, header);

                    });


                };
                
                $rootScope.markTaskComplete = function (task_id, is_completed,option) {

                    var markcompletePromise = deskService.markTaskasComplete(task_id, is_completed);

                    markcompletePromise.success(function (data,header) {
                        var checkSuccess = $scope.removeFromArray(task_id,option);
                        
                        notify(data.message, header);

                    }).error(function (data, header) {
                        notify(data.message, header);

                    });
                };
                
                $scope.removeFromArray = function(task_id,option){
                    if(option == 'today'){
                            var index = $rootScope.getTaskById(task_id,option).index;
                            $rootScope.TaskData.Today.splice(index,1);
                            return true;
                        }else if(option == 'tomorrow'){
                            var index = $rootScope.getTaskById(task_id,option).index;
                            $rootScope.TaskData.Tomorrow.splice(index,1);
                            return true;
                        }else if(option == 'this_week'){
                            var index = $rootScope.getTaskById(task_id,option).index;
                            $rootScope.TaskData.This_week.splice(index,1);
                            return true;
                        }else if(option == 'other'){
                            var index = $rootScope.getTaskById(task_id,option).index;
                            $rootScope.TaskData.Other.splice(index,1);
                            return true;
                        }else if(option == 'pending'){
                            var index = $rootScope.getTaskById(task_id,option).index;
                            $rootScope.TaskData.Pending.splice(index,1);
                            return true;
                        }else if(option == 'completed'){
                            var index = $rootScope.getTaskById(task_id,option).index;
                            $rootScope.TaskData.Completed.splice(index,1);
                            return true;
                        }else{
                            return false;
                        }
                };
                
                $scope.taskDelete = function(task_id,option){
                    
                    var taskDeltePromise = deskService.taskDelete(task_id);
                    
                    taskDeltePromise.success(function (data, header) {
                        
                    var checkSuccess = $scope.removeFromArray(task_id,option);
                     console.log(checkSuccess+'of removig task');   
                                }).error(function (data, header) {
                                    notify(data.message, header);

                                });
                };
                
                $rootScope.taskDeleteModal = function (task_id,options) {
                    createDialog(null, {
                        id: 'deleteTask',
                        title: 'Task Delete',
                        deleteleave: 0,
                        string: 'Are you sure you want to delete this Task ?',
                        backdrop: true,
                        success: {label: 'Yes, Delete', fn: function () {
                                $scope.taskDelete(task_id,options);
                            }},
                        cancel: {label: 'Cancel', fn: function () {

                            }}
                    });
                };

                $scope.getComment = function (id, lastId) {
                    var index = deskService.getTaskById(id).index;
                    if (angular.isUndefined($rootScope.TaskData[index]['moreComments']) || $rootScope.TaskData[index]['moreComments'] !== false) {
                        var getCommentPromise = deskService.getComment(id, lastId);
                        getCommentPromise.success(function (data) {
                            $rootScope.TaskData[index]['Comment'] = ($rootScope.TaskData[index]['Comment']).concat(data.data.comments);
                            $rootScope.TaskData[index]['moreComments'] = data.data['loadMore'];
                            $rootScope.taskSelectItem(id);
                        })
                                .error(function (data, header) {
                                    notify(data.message, header);
                                });
                    }
                };
                
                $rootScope.taskSelectItem = function (task_id) {
                    $rootScope.TaskSelected = angular.copy($rootScope.TaskData[$rootScope.getById(task_id)]);
//        $rootScope.TaskSelected = $rootScope.TaskData[$rootScope.getById(task_id)];
                    delete $rootScope.TaskSelected.TaskAssignment;
                    var temp = $rootScope.TaskSelected.Task;
                    delete $rootScope.TaskSelected.Task;
                    var start_date = new Date(moment(temp.start_date));
                    var end_date = new Date(moment(temp.end_date));
                    $rootScope.TaskSelected.Task = {};
                    $rootScope.TaskSelected.Task.id = temp.id;
                    $rootScope.TaskSelected.Task.title = temp.title.toString();
                    $rootScope.TaskSelected.Task.description = temp.description;
                    $rootScope.TaskSelected.Task.start_date = $filter('date')(new Date(moment(start_date)), "M/dd/yyyy h:mm a");
                    $rootScope.TaskSelected.Task.end_date = $filter('date')(new Date(moment(end_date)), "M/dd/yyyy h:mm a");
                    $rootScope.TaskSelected.Task.priority = (temp.priority == 1) ? true : false;
                    $rootScope.TaskSelected.Task.created_by = temp.created_by;
                    $rootScope.TaskSelected.Task.comment_count = temp.comment_count;
                    $rootScope.tags = [];
                    //setting dynamic height if modal window
                };

                //for notifications when task data wont be there

                $rootScope.getTaskById = function (id,type) {
                     var result;
                      if(type == 'today'){
                            result = deskService.findTaskinArray(id, $rootScope.TaskData.Today);
                            }
                         else if(type == 'tomorrow'){
                            result = deskService.findTaskinArray(id, $rootScope.TaskData.Tomorrow);
                            }
                            else if(type == 'this_week'){
                            result = deskService.findTaskinArray(id, $rootScope.TaskData.This_week);
                            }
                            else if(type == 'other'){
                             result = deskService.findTaskinArray(id, $rootScope.TaskData.Other);
                            }
                            else if(type == 'pending'){
                              result = deskService.findTaskinArray(id, $rootScope.TaskData.Pending);
                            }
                            else if(type == 'completed'){
                               result = deskService.findTaskinArray(id, $rootScope.TaskData.Completed);
                            }
                            else{
                                return;
                            }
                    
                    if (result == false) {
                        var getTaskPromise = deskService.getTaskById(id);
                        getTaskPromise.success(function (data, header) {
                        $scope.TaskDataById = data.data[0];
                        return $scope.TaskDataById;
                        });
                    }
                    else {
                        $scope.TaskDataById = result;
                        return $scope.TaskDataById;
                    }
                };

                $rootScope.taskCarousel = function (data, index, option) {
                    if (index > data.length || index < 0) {
                        return;
                    } else {
                        switch (option) {
                            case 'next':
                                if (index < data.length - 1)
                                    ++index;
                                break;
                            case 'previous':
                                if (index)
                                    --index;
                                break;
                            default:
                                return;
                        }
                        $rootScope.taskSelectItem(data[index]['Task']['id']);
                        $rootScope.selectedTask(index);
                        return;
                    }
                };

                

                /* For expanding the task tile's height on clicking it */
                $scope.selectedIndex = null;
                $scope.selectedTask = function ($index) {
                    if ($index !== $scope.selectedIndex) {
                        $scope.selectedIndex = $index;
                    } 
                };
                
        $scope.getTaskType = function (tags)
        {
            var no_group = 0;
            var no_user = -1;
            var type = 0;
            angular.forEach(tags, function (value, key) {

                if (value.type == "user")
                {
                    no_user++;
                }
                else if (value.type = "group")
                {
                    no_group++;
                }

            });
            no_user = (no_user > 0) ? 1 : 0;
            no_group = (no_group > 0) ? 1 : 0;
            type = no_user + 10 * no_group;
            switch (type) {
                case 00:
                    return 'self';
                    break;
                case 01:
                    return 'user';
                    break;
                case 10:
                    return 'group';
                    break;
                case 11:
                    return 'many';
                    break;
                default:
                    return 'undefined';
                    break;
            }
        };
                
                
               /* Nlp Task Add */
        $scope.currentTask = "";
        $scope.FoundStartDate = Date.today().setTimeToNow();
        $scope.FoundEndDate = (Date.today().setTimeToNow()).addHours(1);
        $scope.FoundEndDateExpression = "";
        $scope.FoundEndTimeExpression = "";
        $scope.FoundEndTime = $scope.FoundEndDate;
        $scope.currentMention = "";
        $scope.enterNewTaskActivate = true;
        $scope.tagsforTask=[];
                
            var days_array = [/today/, /tomorrow/, /yesterday/, /next monday/, /next tuesday/, /next wednesday/, /next thursday/, /next friday/, /next saturday/, /next sunday/,/next week/,/next month/];

            var time_array = [/((?:(?:[0-1][0-9])|(?:[2][0-3])|(?:[0-9])):(?:[0-5][0-9])(?::[0-5][0-9])?(?:\s?(?:am|AM|pm|PM)))/, /([01]\d|2[0-3]):([0-5]\d)/, /((?:(?:[0-1][0-2])|(?:[0-9]))?(?:\s?(?:am|AM|pm|PM)))/];
            
            // 1. HH::MM am/pm
            // 2. HH:MM (millitry time)
            // 3. 1-12 am/pm

        $scope.searchUserAndGroup = function(query , currentMention){
             $scope.enterNewTaskActivate = false;
             $scope.UserAndGroupTag;
             var usersearchPrmoise = runService.searchUserAndGroup(query);
             usersearchPrmoise.success(function(data){
             $scope.UserAndGroupTag = data.data;  
            });
            return $scope.UserAndGroupTag;
          };
       
       $scope.taskAddCallback = function(title){
           if($scope.enterNewTaskActivate == true){
           var data ={};
           data.Task = {};
           data.TaskAssignment = {};
           data.TaskAssignment.assignee_data={};
           var arraywithimg =[];
           data.Task.title = title;
           data.Task.start_date = $scope.FoundStartDate;
           data.Task.end_date = $scope.FoundEndDate;
           data.Task.priority = false;
           arraywithimg = deskService.maketaskAssignmentArray($scope.tagsforTask); 
           var i =0;
           for(i;i<arraywithimg.length;i++){
               data.TaskAssignment.assignee_data[i] = arraywithimg[i];
           }
           var result = $scope.taskAdd(data);
           if(result){
               $scope.currentTask='';
           }
           }
       };
       
        $scope.checkPatterns = function (taskTitle) {

            var day_temp = $scope.matchInArray(taskTitle, days_array);
            var time_temp = $scope.matchInArray(taskTitle, time_array);
            //date informaiton
            
            //if already found
            if (day_temp.toString() != $scope.FoundEndDateExpression) {

                day_temp = day_temp.toString();

                $scope.FoundEndDateExpression = day_temp;

                $scope.FoundEndDate = Date.parse($scope.FoundEndDateExpression);
                

                if (time_temp[0]) {

                    ($scope.FoundEndDate).setHours(($scope.FoundEndTime).getHours());

                    ($scope.FoundEndDate).setMinutes(($scope.FoundEndTime).getMinutes());
                    console.log($scope.FoundEndDate);
                }

            }

            //time information extraction

            if (time_temp[0] !== $scope.FoundEndTimeExpression) {

                $scope.FoundEndTimeExpression = time_temp[0];

                $scope.FoundEndTime = Date.parse(time_temp[0]);

                ($scope.FoundEndDate).setHours(($scope.FoundEndTime).getHours());

                ($scope.FoundEndDate).setMinutes(($scope.FoundEndTime).getMinutes());

            }
        };

        $scope.matchInArray = function (string, expressions) {

            var len = expressions.length,
                    i = 0;

            for (i; i < len; i++) {
                if (string.match(expressions[i])) {
                    var match = expressions[i].exec(string);
                    return match;
                }
            }

            return false;

        };
    
    $scope.getPeopleText = function (item) {
        
        $scope.tagsforTask.push(item.Tags);
        $scope.enterNewTaskActivate = true;
        // note item.label is sent when the typedText wasn't found
        return '@' + item.Tags.name;
    };

        /* Nlp task Complete*/
               
               $scope.paginationInit();
               
               /* Modal Window start */
               
            $scope.showProfileofUser = function (user) {
            $scope.ProfileofUser = user;
            var modalInstance = $modal.open({
            templateUrl: '/scripts/View/users/userProfile.html',
            controller: 'ModalInstanceCtrl',
            scope: $scope,
            
                });
            };
               
               /* Modal window End */
   }]);

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

angular.module('desk', ['comment'])
        .factory('deskService',["$http","commentService",'$httpParamSerializerJQLike',function ($http, commentService,$httpParamSerializerJQLike) {
         var factory={}; 
         var urlSeperator = '/';
         var controller = 'tasks';
         var actions = {view: 'view', add: 'add', edit: 'edit', del: 'delete',taskLoad:'getAssignedTask',getTask:'getTaskById',markcomplete:'setComplete',removeUser:'removeAssignee'};
         var service = {};
         
        factory.taskAdd = function(data){
            var postData = $httpParamSerializerJQLike({data :data});
            var url = urlSeperator + controller + urlSeperator + actions.add;
            var taskAddPromise = $http({
                            method: 'POST',
                            url: url,
                            data: postData
            });
            return taskAddPromise;
        };
        
        factory.taskEdit = function(data){
            var postData = $httpParamSerializerJQLike(data);
            var url = urlSeperator + controller + urlSeperator + actions.edit;
            var taskEditPromise = $http({
                            method: 'POST',
                            url: url,
                            data: postData
            });
            return taskEditPromise;
        };

        factory.taskDelete = function(task_id){
//            var postData = $httpParamSerializerJQLike(data);
            var url = urlSeperator + controller + urlSeperator + actions.del+urlSeperator+task_id;
            var taskDeltePromise = $http({
                            method: 'POST',
                            url: url
//                            data: postData
            });
            return taskDeltePromise;                          
        };
        
        //All or by user_id task loaded
        factory.taskLoad = function(options,type,page,user_id){
            var url;
            var taskLoadPromise;
            if(angular.isUndefined(user_id)){
                user_id='';
            }
            if(!page){
                page=1;
            }
             url = urlSeperator + controller + urlSeperator + actions.taskLoad+ urlSeperator+options+ urlSeperator+ type+ urlSeperator +page+ urlSeperator +user_id;
            taskLoadPromise = $http({
                            method: 'GET',
                            url: url
            });
            return taskLoadPromise;
        };
        
        factory.getTaskById = function(task_id){
            var url = urlSeperator + controller + urlSeperator + actions.getTask+ urlSeperator +task_id;
            var taskPromise = $http({
                            method: 'GET',
                            url: url
            });
            return taskPromise;
        };
        

        factory.deleteComment = function (id, commentId) {
            var delteCommentPromise;
            if (commentId) {
            delteCommentPromise = commentService.delete(commentId);
            }
            return delteCommentPromise;
        };

        factory.addComment = function (id, body) {
            var addCommentPromise;
            
            if (body && body.trim()) {
            addCommentPromise = commentService.add(controller, id, body);
            }
            
            return addCommentPromise;        
        };
        
          factory.getComment = function (id, lastId) {
            var getCommentPromise;
            getCommentPromise =  commentService.get(controller, id, lastId);
            return getCommentPromise; 
        };

        factory.markTaskasComplete = function(task_id, is_completed){
            var action = 'complete';
                    if (is_completed)
                    {
                        action = 'incomplete';
                    }
            var url = urlSeperator + controller + urlSeperator + actions.markcomplete+urlSeperator+task_id+urlSeperator+action;
            var taskPromise;
            taskPromise = $http({
                            method: 'POST',
                            url: url
            });
            return taskPromise;
        };
        
        factory.removeassignee = function(task_id,data){
            var postData = $httpParamSerializerJQLike(data);
            var url = urlSeperator + controller + urlSeperator + actions.removeUser;
                   
            var removeassigneePrmoise = $http({
                        method: 'POST',
                        url: url,
                        data: postData
                    });
            return removeassigneePrmoise;
        };

        factory.findTaskinArray = function(id,Tasks){
            
            var i=0;
            for (i;i<Tasks.length;i++){
                if (Tasks[i].Task.id == id){
                Tasks[i].index=i;
                return Tasks[i];
                }
            }
            return false;
        };
        
        //check if user is admin of task
        
        factory.isTaskAdmin = function (task_id,user_id) {
            var taskusers = service.getTaskById(task_id).TaskAssignment;
            var i;
            for (i = 0; i < taskusers.length; i++) {
                if (taskusers[i].user_id == user_id) {
                    if (taskusers[i].role == "admin") {
                        return true;
                    }
                }

            }
            return false;
        };
        //patch needs to be fixed
        factory.maketaskAssignmentArray = function(array){
            angular.forEach(array,function(value,key){               
                if(value.type == 'user'){
                    array[key].img = value.vga;
                }
            });
            return array;
        };
        
        return factory;
}]);

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

angular.module('desk', [])
        .controller('taskController', ['$rootScope', '$scope', 'deskService', function ($rootScope, $scope, deskService) {
//                deskService = deskService;
        $scope.taskAdd = function (data) {
            if (angular.equals({}, data))
            {
                return;
            }
            if ($('#form-new-task').valid()) //here add angular validations
            {
                deskService.taskAdd(data).success(function (data, header) {
                    $rootScope.TaskData.push(data.data[0]);
                    $rootScope.taskNewInit();
                    $rootScope.taskModalClear();
                    notify(data.message, header);
                    trackEvents.trackTaskAdd('Task-Add', {title: data.data[0].Task.title, shared_count: data.data[0].Tags.length - 1});
                }).error(function (data, header) {
                    notify(data.message, header);
                });
                return true;
            }

            return false;
        };
        $scope.deleteCommentModal = function (id, commentId, type) {
            createDialog(null, {
                id: 'taskcommentdelete',
                title: 'Delete Comment',
                deleteleave: 0,
                string: 'Are you sure you want to delete comment?',
                backdrop: true,
                success: {label: 'Yes, Delete', fn: function () {
                        $scope.deleteComment(id, commentId, type);
                    }},
                cancel: {label: 'Cancel', fn: function () {
                    }}
            });
        };

        $scope.deleteComment = function (id, commentId, type) {
            var deleteCommentPromise = deskService.deleteComment(id, commentId);
            deleteCommentPromise.success(function (data, header) {
                var index = $rootScope.getTaskById(id, type).index;
                $rootScope.TaskData.Today[index]['Task']['comment_count']--;
                $rootScope.TaskData.Today[index]['Comment'] = ($rootScope.TaskData[index]['Comment']).filter(function (comment) {
                    return comment.id != commentId;
                });
//                        $rootScope.taskSelectItem(id);
                notify(data.message, header);
            })
                    .error(function (data, header) {
                        notify(data.message, header);
                    });
        };

        $scope.addComment = function (task_id, body, option) {
            var addCommentPromise = deskService.addComment(task_id, body);

            addCommentPromise.success(function (data, header) {
                var index = $rootScope.getTaskById(task_id, option).index;

                if (option == 'today') {
                    $rootScope.TaskData.Today[index]['Task']['comment_count']++;
                    $rootScope.TaskData.Today[index]['Comment'] = ($rootScope.TaskData[index]['Comment']).concat(data.data);
                } else if (option == 'tomorrow') {
                    $rootScope.TaskData.Tomorrow[index]['Task']['comment_count']++;
                    $rootScope.TaskData.Tomorrow[index]['Comment'] = ($rootScope.TaskData[index]['Comment']).concat(data.data);
                } else if (option == 'this_week') {
                    $rootScope.TaskData.This_week[index]['Task']['comment_count']++;
                    $rootScope.TaskData.This_week[index]['Comment'] = ($rootScope.TaskData[index]['Comment']).concat(data.data);
                } else {
                    $rootScope.TaskData.Other[index]['Task']['comment_count']++;
                    $rootScope.TaskData.Oth[index]['Comment'] = ($rootScope.TaskData[index]['Comment']).concat(data.data);
                }

//                        $rootScope.taskSelectItem(id);
            })
                    .error(function (data, header) {
                        notify(data.message, header);
                    });
        };

        $scope.getComment = function (id, lastId) {
            var index = deskService.getTaskById(id).index;
            if (angular.isUndefined($rootScope.TaskData[index]['moreComments']) || $rootScope.TaskData[index]['moreComments'] !== false) {
                var getCommentPromise = deskService.getComment(id, lastId);
                getCommentPromise.success(function (data) {
                    $rootScope.TaskData[index]['Comment'] = ($rootScope.TaskData[index]['Comment']).concat(data.data.comments);
                    $rootScope.TaskData[index]['moreComments'] = data.data['loadMore'];
                    $rootScope.taskSelectItem(id);
                })
                        .error(function (data, header) {
                            notify(data.message, header);
                        });
            }
        };
    }]);

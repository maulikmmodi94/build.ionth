/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


app.directive('tooltip', function () {
    return {
        restrict: 'A',
        link: function (scope, element) {
            $(element).hover(function () {
                // on mouseenter
                $(element).tooltip('show');
            }, function () {
                // on mouseleave
                $(element).tooltip('hide');
            });

        }
    };
});

/*
 * Directive g-profile-img to be used for rendering images of groups and users when they are set or not set
 * Atrributes to be set 
 * 1. type (required) : 'user' or 'group' or 'network'
 * 2. path : path of the image
 * 3. myclass : class to be added
 * 4. width : width of image to be set
 * 5. height : height of image to be set
 * 6. name (required) : name for generating initials
 * 7. my-title : title for the tooltip or arrow hovers
 * <g-profile-img type="user" path='https//gridle/user/8.jpg' myclass='g-myclass' width='20px' height='20px' name="John Doe" my-title="User : John Doe" ></g-profile-img>
 */
app.directive('gProfileImg', function ($compile) {
    return{
        restrict: 'E',
        scope: {
            path: '@',
            name: '@',
            width: '@',
            height: '@',
            myclass: '@',
            type: '@',
        },
        link: function (scope, element, attrs) {


////               defining templates
            //if user image exist
            var user_img = '<img src="' + attrs.path + '" width="' + attrs.width + '" height="' + attrs.height + '" class=" g-user-initial-image p-margin-0 ' + attrs.myclass + '" alt="' + attrs.name + '"/>';
            //if no user image
            var user_initial = '<p width="' + attrs.width + '" height="' + attrs.height + '" class="g-user-initial-img  p-margin-0 center-block ' + attrs.myclass + '">' + intitials_generation(attrs.name) + '</p>';
            //group intitials image
            var group_initial = '<p width="' + attrs.width + '" height="' + attrs.height + '" class="g-group-initial-img p-margin-0 center-block ' + attrs.myclass + '">' + intitials_generation(attrs.name) + '</p>';
            //network initials image
            var network_initial = '<p width="' + attrs.width + '" height="' + attrs.height + '" class="g-group-initial-img p-margin-0 center-block ' + attrs.myclass + '" >' + intitials_generation(attrs.name) + '</p>';
            //selecting template
            var getTemplate = function (attrs) {
                var template = '';
                switch (attrs.type) {
                    case 'user' :
                        if (attrs.path.indexOf('/default') > -1 || attrs.paths)
                        {
                            template = user_initial;
                        } else {
                            template = user_img;
                        }
                        break;
                    case 'group' :
                        template = group_initial;
                        break;
                    case 'network' :
                        template = network_initial;
                        break;
                    default :
                        template = user_initial;
                }
                return template;
            };

            attrs.$observe("path", function (value) {
                element[0].innerHTML = " ";
                var e = angular.element(getTemplate(attrs));
                $compile(e.contents())(scope);
                element.append(e);
            });
            /*
             * element.append(getTemplate(attrs));
             * attrs.$observe();
             * element.replaceWith(getTemplate(attrs));
             * var e = angular.element(getTemplate(attrs));
             * $compile(e.contents())(scope);
             * element.replaceWith(e);
             */
        }
    };
});

/*
 This directive allows us to pass a function in on an enter key to do what we want.
 */
app.directive('ngEnter', function () {
    return function (scope, element, attrs) {
        element.bind("keydown keypress", function (event) {
            if (event.which === 13) {
//                scope.$apply(function() {
//                   scope.$eval(attrs.ngEnter);
//                });
                scope.$eval(attrs.ngEnter);
                event.preventDefault();
            }
        });
    };
});

app.directive('datechange', function ($filter) {
    return {
        require: 'ngModel',
        link: function (scope, el, attr, ngModel) {
            $(".date-task").on("dp.change", function (e) {
                var date = $(this).val();
                var id = $(this).attr('id');
                scope.$apply(function () {

                    if (ngModel.$name == id)
                    {
                        ngModel.$setViewValue(date);
                    }
                });
            });
        }
    };
});

app.directive("timeAgo", function ($q) {
    return {
        restrict: "C",
        scope: {
            title: '@'
        },
        link: function (scope, element, attrs) {
            // Using deferred to assert we only initialize timeago() once per
            // directive.
            var parsedDate = $q.defer();
            parsedDate.promise.then(function () {
                jQuery(element).timeago();
            });
            attrs.$observe('title', function (newValue) {
                parsedDate.resolve(newValue);
            });
        }
    };
});

app.directive('notificationDropDown', function ($rootScope, notification) {
    return{
        restrict: 'A',
//        templateUrl: '/pages/notifications_dropdown',
        template: '<div class="notifications-wrap">\n\
<div class="notifcations-options text-center">\n\
<p class="notifications-heading text-muted font-big-1 p-margin-0" >Notifications</p>\n\
</div>\n\
<div class="notifications-content g-scrollbar">\n\
<div class="notifications-loading" ng-if="!notifications">\n\
<h1 class="text-center">Loading ...</h1>\n\
</div>\n\
<div class="notifications-list container-fluid ">\n\
<div ng-if="notifications.length==0">It\'s lonely in here for now.. But when someone performs an action associated to you, you will be notified here..</div>\n\
<div class="notification-div row " ng-repeat="notification in notifications" ng-class="{ \'notification-unread\' : !notification.isSeen }">\n\
<a href="{{ notification.link }}">\n\
<div class="notification-graphic col-lg-2 col-md-2 col-sm-2 col-xs-2">\n\
<p class ="log-message-icon  p-margin-0" ng-class="{\'success-icon\': notification.type ==\'task\' && notification.activity==\'taskcomplete\'}" ><span class="fa" ng-class="{file:\'fa-file\',group:\'fa-users\', task:\'fa-tasks\', network:\'fa-circle\'}[notification.type]"></span></p>\n\
</div>\n\
<div class="notification-message col-lg-10 col-md-10 col-sm-10 col-xs-10">\n\
<p class="p-margin-0"><span ng-bind-html="notification.message"></span><br><span class="time-ago text-muted font-small-1" title="{{notification.time}}"></span></p>\n\
</div>\n\
</a>\n\
</div>\n\
</div>\n\
</div>\n\
<div class="notifications-footer text-right" ><a href="/notifications" class="">See all</a>\n\
</div>\n\
</div>'
    };
});

app.directive('frameBench', function (fileService, $sce) {
    return{
        template: '<div id="framebench-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"><div class="modal-dialog g-modal" style="width:80%;">\n\
    <div class="modal-content g-modal-content">\n\
        <div class="modal-body g-modal-body" style="padding:0px;">\n\
            <div ng-show="!filePreviewUrl" class="col-sm-12" style="height:600px">\n\
                <p class="text-center" style="line-height:600px; font-size:2em;">Waiting for the file...</p>\n\
            </div>\n\
            <iframe id="framebench-iframe" style="width: 100%; height:600px;" ng-src="{{filePreviewUrl}}" ng-show="filePreviewUrl"></iframe>\n\
            \n\
            </div>\n\
    </div>\n\
</div></div>',
        link: function (scope, element, attrs) {

            scope.iframeloading = false;

            scope.$watch(function () {
                return fileService.filePreviewUrl;
            }, function (newval) {
                if (newval && newval !== 'about:blank') {
                    //  $('#framebench-modal').modal('show');
                    scope.filePreviewUrl = $sce.trustAsResourceUrl(fileService.filePreviewUrl);
                    scope.iframeloading = true;
                }
            });

            $('#framebench-modal').on('hidden.bs.modal', function () {
                scope.filePreviewUrl = $sce.trustAsResourceUrl('about:blank');
                fileService.filePreviewUrl = 'about:blank';
                scope.$apply();
            });

//            $('#framebench-iframe').load(function () {
//                console.log('iframe loaded');
//                scope.iframeloading = false;
//                scope.$apply();
//            });

        }
    }
});

app.directive('hidebsmodal', function () {
    return {
        link: function (scope, element, attributes) {
            $('#taskAddModal').on('hidden.bs.modal', function (e) {
                // do something...
            })


            element.find('a').on('click', function () {
                //problematic line HERE
                $(element).trigger("myEvent.sample");
            });
        },
        //some other stuff
    };
});
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


var findUser = function (user_id, users) {
    var index = getUserIndex(user_id);
    if (index > -1) {
        return index;
    } else {
        if (!$rootScope.findUserInProgress && typeof (user_id) !== 'undefined') {
            $rootScope.findUserInProgress = true;
            $rootScope.getUserInfo(user_id);
        } else {
            return -1;
        }
    }
};

var getUserIndex = function (user_id, users) {
    for (var i in users) {
        if (users[i]['UserInfo']['user_id'] == user_id) {
            return i;
        }
    }
    return -1;
};

var findUserNetwork = function (user_id) {
    return getUserNetworkIndex(user_id);
};

var getUserNetworkIndex = function (user_id, user_networks) {
    for (var i in user_networks) {
        if (user_networks[i]['UserNetwork']['user_id'] == user_id) {
            return i;
        }
    }
    return -1;
};

$rootScope.getUserInfo = function (user_id) {
    $http({
        cache: true,
        method: 'GET',
        url: '/datas/getUserDetails/' + user_id + '.json'
    }).success(function (data, header) {
        data.data.user.UserInfo.last_name = ' (Removed)';
        data.data.user.UserInfo.name = data.data.user.UserInfo.name.split(" ")[0] + ' (Removed)';
        ($rootScope.users).push(data.data.user);
        $rootScope.findUserInProgress = false;

    });
};

//check to seee if a group exists in the groups array or not or not
var findGroup = function (id, groups) {
    for (var i in groups) {
        if (groups[i]['Group']['id'] == id) {
            return i;
        }
    }
    return -1;
};

var chekUserInGroup = function (user_id, group) {
    for (var j in group['UsersGroup']) {
        if (group['UsersGroup'][j].user_id == user_id) {
            return j;
        }
    }
    return -1;
};

function arrayUnique(array) {
    var a = array.concat();
    for (var i = 0; i < a.length; ++i) {
        for (var j = i + 1; j < a.length; ++j) {
            if (a[i] === a[j])
                a.splice(j--, 1);
        }
    }
    return a;
}
;

function notify(message, code, options)
{
    var options = [];
    if (message)
    {
        var defaultOption = {
            ele: 'body', // which element to append to
            type: 'info', // (null, 'info', 'danger', 'success')
            offset: {from: 'bottom', amount: 20}, // 'top', or 'bottom'
            align: 'left', // ('left', 'right', or 'center')
            width: 250, // (integer, or 'auto')
            delay: 4000, // Time hwile the message will be displayed. It's not equivalent to the *demo* timeOut!
            allow_dismiss: true, // If true then will display a cross to close the popup.
            stackup_spacing: 10 // spacing between consecutively stacked growls.
        };

        if (code >= 200 && code < 300)
        {
            options.type = "info";
        }
        else if (code >= 300 && code < 400)
        {
            window.location.href = message;
        }
        else if (code >= 400)
        {
            options.type = "danger";
        }

        $.extend(defaultOption, options);
        $.bootstrapGrowl(message, defaultOption);
    }
}

/* Not used */
function disableTagsInput() {
    document.getElementsByTagName("tags-input")[0].getElementsByTagName("input")[0].disabled = true;
    var tagsRemoveButtons = document.getElementsByTagName("tags-input")[0].getElementsByClassName("remove-button");
    for (var i in tagsRemoveButtons) {
        tagsRemoveButtons[i].hidden = true;
    }
}

function validateEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

function findInObjectArray(array, obj, key1, key2) {
    var item = null;
    for (var i = 0; i < array.length; i++) {
        // I'm aware of the internationalization issues regarding toLowerCase()
        // but I couldn't come up with a better solution right now
        if ((array[i][key1].toString().toLowerCase() === obj[key1].toString().toLowerCase()) && (array[i][key2].toLowerCase() === obj[key2].toLowerCase())) {
            item = array[i];
            break;
        }
    }
    return item;
};

$rootScope.showUserInfoModal = function (user_id)
        {
            var index = $rootScope.findUser(user_id);
            if (index > -1)
            {
                $rootScope.viewUser = $rootScope.users[index].UserInfo;
            }

            createDialog('User/view.html', {
                id: 'viewUser' + user_id,
                user_id: user_id,
                title: 'User information',
                footer: false,
                myModalClass: 'g-modal',
                deleteleave: 0,
                backdrop: true
            });
        };
        

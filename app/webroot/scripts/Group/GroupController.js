/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


app.controller('GroupController',['$rootScope','groupService','$scope','$modal','$http','$q',function($rootScope,groupService,$scope,$modal,$http,$q){
    /* Data Manipulation functions */
    $scope.tagsforGroup=[];
    $scope.grouptags = [];
    $scope.groupAdd = function (group) {
        var data = {};
        data.userlist = {};
        data.Group;
        data.Group.name = group.Group.name;
        var i = 0;
        for(i;i<$scope.tagsforGroup.length;i++){
        data.userlist[i] = $scope.tagsforGroup[i];    
        }
        var groupAddPromise =  groupService.groupCreate(data);
       groupAddPromise.success(function(data){
            $rootScope.groups.push(data.data);
        });
    };
    
    
    $scope.searchUserAutoLoad = function(query,current){
            var url = '/datas/search/'+query+'/'+'user';
            $scope.autoLoadedUsers; 
            var searchUserPromise = $http({
                            method: 'GET',
                            url: url
            });
            searchUserPromise.success(function(data){
             $scope.autoLoadedUsers = data.data;
            });
            return $scope.autoLoadedUsers;
    };
    
    $scope.getUsers = function (item) {
        $scope.tagsforGroup.push(item.Tags);
        return '@' + item.Tags.name;
    };
    
    $scope.groupDataEdit = function (id, data) {
        var index = $rootScope.findGroup(id);
        $rootScope.groups[index]['Group'] = data;
    };
    $scope.groupDataEditUsers = function (id, data) {
        var index = $rootScope.findGroup(id);
        $rootScope.groups[index]['UsersGroup'] = data;
    };
    $scope.groupDataReplace = function (id, data) {
        var index = $rootScope.findGroup(id);
        $rootScope.groups[index] = data;
    };
    //Modal Windows
    $scope.groupModalDelete = function (id, name) {
        
        createDialog(null, {
            id: 'groupdeletemodal',
            title: 'delete group',
            deleteleave: 0,
            string: 'Are you sure you want to delete group ' + '<span class=groupnamemodal>' + name + '</span> ?',
            backdrop: true,
            success: {label: 'Yes, Delete', fn: function () {
                    $scope.groupDelete(id);
                }},
            cancel: {label: 'Cancel', fn: function () {
                    $('#groupManageModal').modal('show');
                }}
        });
    };
    
    $scope.groupModalLeave = function (id, name) {
        
        createDialog(null, {
            id: 'leavegroupmodal',
            title: 'leave group',
            deleteleave: 1,
            string: 'Are you sure you want to leave group ' + '<span class=groupnamemodal>' + name + '</span> ?',
            backdrop: true,
            success: {label: 'Yes, Leave', fn: function () {
                    $scope.groupLeave(id);
                }},
            cancel: {label: 'Cancel', fn: function () {
                    $('#groupManageModal').modal('show');
                }}

        });
    };
    
    $scope.groupModalMemberRemove = function (group_id, group_name, user, user_name) {
        
        createDialog(null, {
            id: 'groupmodalmemberremove',
            title: 'Remove Member',
            deleteleave: 0,
            string: 'Are you sure you want to remove member ' + user_name + ' from group ' + group_name + '?',
            backdrop: true,
            success: {label: 'Yes, Delete', fn: function () {
                    $scope.groupRemoveMembers(user, group_id);
                }},
            cancel: {label: 'Cancel', fn: function () {
                }}
        });
    };
        
}]);
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


app.factory('groupService',['$rootScope','$http','$q','$httpParamSerializerJQLike', function ($rootScope, $http, $q,$httpParamSerializerJQLike) {

    var factory={};

    factory.getGroupObject = function (group_id) {
        
        var groupObject = $http({
            method: 'Post',
            url: '/groups/getGroup/' + group_id
        });
        return groupObject;
    };

    factory.groupCreate = function (data) {
        var groupCreatePromise;
        if (data) {
            var xsrf = $httpParamSerializerJQLike({data : data});
            groupCreatePromise = $http({
                method: 'Post',
                url: '/groups/add/',
                data: xsrf
            });
            return groupCreatePromise;
        }
        return false;
    };

    factory.groupEditBtn = function (id, name, oldname) {
        var data = {};
        var groupEditPromise;
        if (name !== oldname)
        {
            data.name = name;
            data.group_id = id;
            var xsrf = $httpParamSerializerJQLike(data);
            groupEditPromise = $http({
                method: 'POST',
                url: '/groups/edit/',
                data: xsrf
            });
            return groupEditPromise;
        }
        return false;
    };
    
    factory.groupAddMembers = function (id, users) {
        console.log('Hello this is log of Group Service add member');
        console.log(id);
        console.log(users);
        var addMembersPrmoise;
        if (users.userlist.length > 0) {
            var data = {};
            data.userlist = users.userlist;
            data.group_id = id;
            var xsrf = $httpParamSerializerJQLike(data);
            addMembersPrmoise = $http({
                method: 'POST',
                url: '/groups/addmember/',
                data: xsrf
            });
            return addMembersPrmoise;
        }
    };

    factory.groupRemoveMembers = function (user_id, id) {
        var data = {};
        var removeMembersPromise;
        data.group_id = id;
        data.user_id = user_id;
        var xsrf = $httpParamSerializerJQLike(data);
        removeMembersPromise = $http({
            method: 'Post',
            url: '/groups/removemember/',
            data: xsrf
        });
        
    };
    // delete group and leave group
    factory.groupDelete = function (id) {
        var data = {};
        data.group_id = id;
        var groupDeletePromise;
        var xsrf = $httpParamSerializerJQLike(data);
        groupDeletePromise = $http({
            method: 'POST',
            url: '/groups/delete/',
            data: xsrf
        });
        return groupDeletePromise;
    };
    
    factory.groupLeave = function (id) {
        var data = {};
        var groupLeavePromise;
        data.group_id = id;
        var xsrf = $httpParamSerializerJQLike(data);
        groupLeavePromise = $http({
            method: 'POST',
            url: '/groups/leave/',
            data: xsrf
        });
       return groupLeavePromise;
    };
    
    factory.checkEmail = function (email) {
            var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return re.test(email);
        };
    
    return factory;

}]);
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
angular.module('run', [])
        .factory('runService', ['$http',function ($http) {
                var factory={};
                var urlSeperator = '/';
                var controller = 'datas';
                var actions = {users: 'getUsers', groups: 'getGroups', usernetwork: 'getUserandNetwork', del: 'delete'};
                
                factory.getUsersData = function (page,loadmoreCount) {
                    if(!page){
                       page=1; 
                    }
                    if(!loadmoreCount){
                        loadmoreCount=0;
                    }
                    var url = urlSeperator + controller + urlSeperator + actions.users+ urlSeperator+page+urlSeperator+loadmoreCount;
                    var userDataPromise = $http({
                        method: 'GET',
                        url: url});
                    return userDataPromise;
                };

                factory.getGroupsData = function (page,loadmoreCount) {
                    if(!page){
                       page=1; 
                    }
                    if(!loadmoreCount){
                        loadmoreCount=0;
                    }
                    var url = urlSeperator + controller + urlSeperator + actions.groups +urlSeperator+page+urlSeperator+loadmoreCount;
                    var groupDataPromise = $http({
                        method: 'GET',
                        url: url});
                    return groupDataPromise;
                };

                factory.getUserandNetworkData = function () {
                    var url = urlSeperator + controller + urlSeperator + actions.usernetwork;
                    var getUserandNetworkDataPromise = $http({
                        method: 'GET',
                        url: url});
                    return getUserandNetworkDataPromise;
                };
                
                factory.orderByUserNetworkRole = function (usernetwork)
                {
                    var priority;
                    switch (usernetwork.UserNetwork.role) {
                        case 'owner':
                            priority = 1;
                            break;
                        case 'admin':
                            priority = 2;
                            break;
                        case 'user':
                            priority = 3;
                            break;
                        default:
                            priority = 4;
                            break;
                    }
                    return priority;
                };
                
                factory.searchUserAndGroup = function(query){
                 var url = '/datas/search/'+query;
                 var taskPromise = $http({
                            method: 'GET',
                            url: url
                 });
            return taskPromise;
          };
                
          return factory;
 }]);
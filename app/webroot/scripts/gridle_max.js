app= angular.module('GridleApp', ['ui.bootstrap', 'ngSanitize', 'comment', 'mentio', 'ngTagsInput', 'ngFileUpload', 'angular-growl', 'materialDatePicker', 'ngAnimate', 'monospaced.elastic','ngEmbedApp']);

app.config(['$compileProvider', 'growlProvider', function ($compileProvider, growlProvider) {
        $compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|file|skype):/);
        growlProvider.globalDisableCountDown(true);
        growlProvider.globalDisableIcons(true);
        growlProvider.globalTimeToLive({success: 1000, error: 2000, warning: 3000, info: 3000});
    }]);

app.factory('runService', ['$http', function ($http) {
        var factory = {};
        var urlSeperator = '/';
        var controller = 'datas';
        var actions = {users: 'getUsers', groups: 'getGroups', usernetwork: 'getUserandNetwork', del: 'delete'};

        factory.getUsersData = function (page, user_networkId) {
            if (!page) {
                page = 1;
            }
            if (!user_networkId) {
                user_networkId = 0;
            }
            var url = urlSeperator + controller + urlSeperator + actions.users + urlSeperator + page + urlSeperator + user_networkId;
            var userDataPromise = $http({
                method: 'GET',
                url: url});
            return userDataPromise;
        };

        factory.getGroupsData = function (page, user_group_id) {
            if (!page) {
                page = 1;
            }
            if (!user_group_id) {
                user_group_id = 0;
            }
            var url = urlSeperator + controller + urlSeperator + actions.groups + urlSeperator + page + urlSeperator + user_group_id;
            var groupDataPromise = $http({
                method: 'GET',
                url: url});
            return groupDataPromise;
        };

        factory.getUserandNetworkData = function () {
            var url = urlSeperator + controller + urlSeperator + actions.usernetwork;
            var getUserandNetworkDataPromise = $http({
                method: 'GET',
                url: url});
            return getUserandNetworkDataPromise;
        };

        factory.searchUserAndGroup = function (query, type) {
            var searchPromise;
            var url = '/datas/search/' + query;
            if (type) {
                url = '/datas/search/' + query + '/' + type;
            }
            searchPromise = $http({
                method: 'GET',
                cache: false,
                url: url
            });
            return searchPromise;
        };

        factory.extractEmails = function (string) {
            var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/ig;
            var result = string.match(re);
        };
        return factory;
    }]);


app.controller('headerController', ['$rootScope', '$scope', '$modal', '$http', function ($rootScope, $scope, $modal, $http) {
        $scope.currenturlPath = (window.location.pathname).split("/")[1];
        $scope.issearchTask;
        $scope.placeholder;
        $scope.searchTask;
        $scope.filequery;

        if ($scope.currenturlPath == 'desks') {
            $scope.issearchTask = true;
        }
        else if ($scope.currenturlPath == 'docs') {
            $scope.issearchTask = false;
        }
        $rootScope.navCollapsed = true;
        $rootScope.showNotifications = false;
        $rootScope.hideRightBar = false;

        $rootScope.toggleNotifications = function () {
            $rootScope.showNotifications = !$rootScope.showNotifications;
        };

        $scope.status = {
            isopen: false
        };

        $scope.toogleRightBar = function () {
            $rootScope.hideRightBar = !$rootScope.hideRightBar;
            if ($rootScope.showNotifications == true) {
                $rootScope.showNotifications = false;
            }
        };

        $scope.toggleDropdown = function ($event) {
            $event.preventDefault();
            $event.stopPropagation();
            $scope.status.isopen = !$scope.status.isopen;
        };

        $scope.taskSearchHeader = function (query) {
            if (query == null) {
                return false;
            } else {
                $rootScope.$broadcast('task_searched', query);
            }
        };

        $scope.checktaskSearch = function () {
            if ($scope.searchTask.length <= 0) {
                $scope.clearSearch();
            }
        };

        $scope.clearSearch = function () {
            if ($scope.issearchTask) {
                $rootScope.$broadcast('task_search_cleared');
                $scope.searchTask = '';
            }
            else if (!$scope.issearchTask) {
//                $rootScope.$broadcast('file_search_cleared');
                $scope.filequery = '';
            }
        };
        
        $rootScope.$on('clear_header_search', function (event, args) {
            $scope.clearSearch();
        });

        $scope.getFileSelected = function (file) {
            $scope.selectFile(file);
            $scope.filequery = file.Doc.name;
        };

        $scope.selectFile = function (file) {
            $rootScope.$broadcast('file_selected', file);
        };

        $scope.searchFile = function (query) {
            $rootScope.$broadcast('file_search_toggled', query);
            var url = '/docs/getFiles/' + query;
            var filePromise = $http({
                method: 'GET',
                url: url,
                cache: false
            }).success(function (data) {
                $scope.searchFileTags = data.data.files;
                return $scope.searchFileTags;
            });
        };

    }]);


app.run(['$rootScope', '$http', 'runService', '$filter', 'growl', function ($rootScope, $http, runService, $filter, growl) {

        $rootScope.users = [];
        $rootScope.groups = [];
        $rootScope.networks = [];
        $rootScope.networks_expired = [];
        $rootScope.user_networks = [];
        $rootScope.network_users = [];
        $rootScope.loadMoreUser;
        $rootScope.loadMoreGroup;
        $rootScope.LoadMoreCountUser;
        $rootScope.LoadMoreCountGroup;
        $rootScope.loadingUsers = true;
        $rootScope.loadingGroups = true;
        /* Set Default Headers */
        $http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded';
        /* Set headers as Ajax  */
        $http.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
        $http.defaults.headers.common['Accept'] = 'application/json';
//        runService = runService;


        $rootScope.userandNetworkPromise = runService.getUserandNetworkData();

        $rootScope.userandNetworkPromise.success(function (data) {
            $rootScope.user_networks = data.data['user_networks'];
            $rootScope.loggeduser = $rootScope.user_networks.UserInfo.user_id;
            $rootScope.loggeduser_name = $rootScope.user_networks.UserInfo.name;
            if ($rootScope.user_networks.UserNetwork) {
                $rootScope.user_role_network = $rootScope.user_networks.UserNetwork.role;
            }
            if (data.data['user_networks']['Network']) {
                $rootScope.currentNetwork = data.data['user_networks']['Network'].name;
                $rootScope.currentNetworkId = data.data['user_networks']['Network'].id;
                $rootScope.currentNetworkCount = data.data['user_networks']['Network'].user_count;
            }
        });
        

        //params will be inserted if required
        $rootScope.userPromise = runService.getUsersData();
        $rootScope.userPromise.success(function (data, header) {
            $rootScope.users = data.data['users'];
            $rootScope.loadMoreUser = data.LoadMore;
            $rootScope.LoadMoreCountUser = data.LoadMoreCount;
            $rootScope.loadingUsers = false;
        }).error(function (data, header) {
            notify(data.message, header);

        });

        $rootScope.groupPromise = runService.getGroupsData();
        $rootScope.groupPromise.success(function (data, header) {
            $rootScope.groups = data.data.groups;
            $rootScope.loadMoreGroup = data.LoadMore;
            $rootScope.LoadMoreCountGroup = data.LoadMoreCount;
            $rootScope.loadingGroups = true;
        }).error(function (data, header) {
            notify(data.message, header);
        });

        $rootScope.dateFormat = function (date, format) {
            var dateFormat = Date.parse(date);
            if (!format) {
                return (dateFormat.toString('MMM dd , yyyy h:mm tt'));
            }
            else if (format == 'date') {
                return (dateFormat.toString('MMM dd , yyyy'));
            }
            else if (format == 'time') {
                return (dateFormat.toString('h:mm tt'));
            } else {
                return $filter('date')(dateFormat, format);
            }
        };

        $rootScope.timeagoMoment = function (date) {
            return moment(date).from();
        };

        $rootScope.momentDate = function (date)
        {
            moment.locale('en', {
                calendar: {
                    lastDay: '[Yesterday]',
                    sameDay: '[Today]',
                    nextDay: '[Tomorrow]',
                    lastWeek: '[Last_Week]',
                    nextWeek: 'dddd DD,MMMM',
                    sameElse: 'dddd DD,MMMM'
                }
            });
            return moment(date).calendar();
        };

        $rootScope.growlNotify = function (message, code) {
            growl.info(message);

            if (code >= 200 && code < 300)
            {
//            options.type = "info";
            }
            else if (code >= 300 && code < 400)
            {
//            window.location.href = message;
            }
            else if (code >= 400)
            {
//            options.type = "danger";
            }

        };

    }]);

app.controller('sideTasksCtrl', ['$scope', '$rootScope', function ($scope, $rootScope) {

        $scope.tasksType = 'Pending';
        $rootScope.rightSideBar = true;
        $scope.changeTaskType = function (type) {

            switch (type) {

                case 'Pending' :
                    $scope.tasksType = "Pending";
                    break;

                case 'Completed' :
                    $scope.tasksType = "Completed";
                    break;

                default :
                    $scope.tasksType = 'Pending';

            }

        };

    }]);


app.factory('deskService', ["$http", "commentService", '$httpParamSerializerJQLike', function ($http, commentService, $httpParamSerializerJQLike) {
        var factory = {};
        var urlSeperator = '/';
        var controller = 'tasks';
        var actions = {view: 'view', add: 'add', edit: 'edit', del: 'delete', taskLoad: 'getAssignedTask', getTask: 'getTask', markcomplete: 'setComplete', removeUser: 'removeAssignee'};
        var service = {};

        factory.taskAdd = function (data) {
            var postData = $httpParamSerializerJQLike({data: data});
            var url = urlSeperator + controller + urlSeperator + actions.add;
            var taskAddPromise = $http({
                method: 'POST',
                url: url,
                data: postData
            });
            return taskAddPromise;
        };

        factory.taskEdit = function (data) {
            var postData = $httpParamSerializerJQLike({data: data});
            var url = urlSeperator + controller + urlSeperator + actions.edit + urlSeperator + data.Task.id;
            var taskEditPromise = $http({
                method: 'POST',
                url: url,
                data: postData
            });
            return taskEditPromise;
        };

        factory.taskDelete = function (task_id) {
//            var postData = $httpParamSerializerJQLike(data);
            var url = urlSeperator + controller + urlSeperator + actions.del + urlSeperator + task_id;
            var taskDeltePromise = $http({
                method: 'POST',
                url: url
//                            data: postData
            });
            return taskDeltePromise;
        };

        //All or by user_id task loaded
        factory.taskLoad = function (options, type, page, user_id) {
            var url;
            var taskLoadPromise;
            if (angular.isUndefined(user_id)) {
                user_id = '';
            }
            if (!page) {
                page = 1;
            }
            url = urlSeperator + controller + urlSeperator + actions.taskLoad + urlSeperator + options + urlSeperator + type + urlSeperator + page + urlSeperator + user_id;
            taskLoadPromise = $http({
                method: 'GET',
                url: url
            });
            return taskLoadPromise;
        };

        factory.getTaskById = function (task_id) {
            if (task_id) {
                var url = urlSeperator + controller + urlSeperator + actions.getTask + urlSeperator + task_id;
                var taskPromise = $http({
                    method: 'GET',
                    url: url
                });
                return taskPromise;
            } else {
                return false;
            }

        };

        factory.deleteComment = function (id, commentId) {
            var delteCommentPromise;
            if (commentId) {
                delteCommentPromise = commentService.delete(commentId);
            }
            return delteCommentPromise;
        };

        factory.addComment = function (id, body) {
            var addCommentPromise;

            if (body && body.trim()) {
                addCommentPromise = commentService.add(controller, id, body);
            }

            return addCommentPromise;
        };

        factory.getComment = function (id, lastId) {
            var getCommentPromise;
            getCommentPromise = commentService.get(controller, id, lastId);
            return getCommentPromise;
        };

        factory.markTaskasComplete = function (task_id, is_completed) {
            var action = 'complete';
            if (is_completed)
            {
                action = 'incomplete';
            }
            var url = urlSeperator + controller + urlSeperator + actions.markcomplete + urlSeperator + task_id + urlSeperator + action;
            var taskPromise;
            taskPromise = $http({
                method: 'POST',
                url: url
            });
            return taskPromise;
        };

        factory.removeassignee = function (task_id, data) {
            var postData = $httpParamSerializerJQLike(data);
            var url = urlSeperator + controller + urlSeperator + actions.removeUser + urlSeperator + task_id;

            var removeassigneePrmoise = $http({
                method: 'POST',
                url: url,
                data: postData
            });
            return removeassigneePrmoise;
        };

        factory.findTaskinArray = function (id, Tasks) {

            var i = 0;
            for (i; i < Tasks.length; i++) {
                if (Tasks[i].Task.id == id) {
                    Tasks[i].index = i;
                    return Tasks[i];
                }
            }
            return false;
        };

        //check if user is admin of task

        factory.isTaskAdmin = function (task_id, user_id) {
            var taskusers = service.getTaskById(task_id).TaskAssignment;
            var i;
            for (i = 0; i < taskusers.length; i++) {
                if (taskusers[i].user_id == user_id) {
                    if (taskusers[i].role == "admin") {
                        return true;
                    }
                }

            }
            return false;
        };
        //patch needs to be fixed
        factory.maketaskAssignmentArray = function (array) {
            angular.forEach(array, function (value, key) {
                if (value.type == 'user') {
                    array[key].img = value.vga;
                }
            });
            return array;
        };

        return factory;
    }]);


app.controller('deskController', ['$rootScope', '$scope', 'deskService', '$q', 'createDialog', '$sce', '$modal', 'runService', 'trackEvents', '$location', '$anchorScroll', function ($rootScope, $scope, deskService, $q, createDialog, $sce, $modal, runService, trackEvents, $location, $anchorScroll) {

        $scope.deskService = deskService;
        $scope.loadingTasks = true;
        $rootScope.tourOn = false;
        $rootScope.tourStep = 1;
        $rootScope.TaskData = {};
        $scope.latestTaskId = "";
        $scope.thiWeekTaskPage = 1;
        $scope.otherTaskPage = 1;
        $scope.completedtaskPage = 1;
        $scope.pendingtaskPage = 1;
        $scope.todayTaskPage = 1;
        $scope.tomorrowTaskPage = 1;

        $scope.scrollTo = function (element_id) {

            $location.hash(element_id);
            $anchorScroll();

        };
        var options = {upcoming: 'other', completed: 'completed', pending: 'pending', tomorrow: 'tomorrow', today: 'today', next_week: 'next_week', this_week: 'this_week'};

        $scope.taskAdd = function (data) {
            if (angular.equals({}, data))
            {
                return false;
            }
            deskService.taskAdd(data).success(function (data, header) {
                var result = $scope.addTasktoArray(data.data[0]);
                $scope.latestTaskId = data.data[0].Task.id;
                trackEvents.trackTaskAdd('Task-Add', {title: data.data[0].Task.title, shared_count: data.data[0].Tags.length - 1});
                notify(data.message, header);
            }).error(function (data, header) {
                notify(data.message, header);
            });
            return true;
        };

        $scope.addTasktoArray = function (data) {
            var end_date = $rootScope.momentDate(data.Task.end_date);
            var end_week = moment(data.Task.end_date).week();
            var this_week = moment().week();
            
            //needs to be solved
            if (end_date == 'Tomorrow') {
                    $rootScope.TaskData.Tomorrow.push(data);
                    return true;
                }
            
            if (this_week == end_week) {
                if (end_date == 'Today') {
                    $rootScope.TaskData.Today.push(data);
                } else if (end_date == 'Tomorrow') {
                    $rootScope.TaskData.Tomorrow.push(data);
                } else {
                    $rootScope.TaskData.This_week.push(data);
                }
                return true;
            }
            else if(this_week < end_week) {
                $rootScope.TaskData.Other.push(data);
                return true;
            }else if(this_week > end_week){
                $rootScope.TaskData.Pending.push(data);
                return true;
            }
            return false;
        };


        $scope.isTaskAdmin = function (task, user_id)
        {
            var returnStatus = false;
            if (task.created_by == user_id) {
                returnStatus = true;
            }
            return returnStatus;
        };


        $scope.paginationInit = function () {
            $scope.thiWeekTaskPage = 1;
            $scope.otherTaskPage = 1;
            $scope.completedtaskPage = 1;
            $scope.pendingtaskPage = 1;
            $scope.todayTaskPage = 1;
            $scope.tomorrowTaskPage = 1;
        };

        $rootScope.$on('user_Selected_task', function (event, args) {
            $scope.paginationInit();
            $scope.loadTaskData('user', args.user_id);
        });

        $rootScope.$on('group_Selected_task', function (event, args) {
            $scope.paginationInit();
            $scope.loadTaskData('group', args.id);
        });

        $rootScope.$on('clear_Filter_task', function (event, args) {
            $scope.paginationInit();
            $scope.loadTaskData('user');
        });

        $scope.getUpcomingTask = function (options, type, user_id) {
            var taskPage;

            if (type == 'today') {
                taskPage = $scope.todayTaskPage;
            }
            else if (type == 'tomorrow') {
                taskPage = $scope.tomorrowTaskPage;
            }
            else if (type == 'this_week') {
                taskPage = $scope.thiWeekTaskPage;
            }
            else {
                taskPage = $scope.otherTaskPage;
            }

            var loadedTaskPromise = deskService.taskLoad(options, type, taskPage, user_id);
            loadedTaskPromise.success(function (data, header) {
                if (data.data != null)
                {
                    if (data.type == 'today') {
                        $rootScope.TaskData.Today = data.data;
                        $scope.todayTaskPage++;
                    }
                    else if (data.type == 'tomorrow') {
                        $rootScope.TaskData.Tomorrow = data.data;
                        $scope.tomorrowTaskPage++;
                    }
                    else if (data.type == 'this_week') {
                        $rootScope.TaskData.This_week = data.data;
                        $scope.thiWeekTaskPage++;
                    }
                    else {
                        $rootScope.TaskData.Other = data.data;
                        $scope.otherTaskPage++;
                    }
                }
                $scope.laodingTasks = false;
            }).error(function (data, header) {
                //write a notifications
            });
        };

        $scope.getCompletedTask = function (options, type, user_id) {

            var loadedTaskPromise = deskService.taskLoad(options, type, $scope.completedtaskPage, user_id);
            loadedTaskPromise.success(function (data, header) {
                if (data.data != null)
                {
                    $rootScope.TaskData.Completed = data.data;
                    $rootScope.TaskData.Completed.count = data.count;
                    $scope.loadMoreComplete = data.loadMoreTag;
                    $scope.completedtaskPage++;
                }
            }).error(function (data, header) {
                //write a notifications
            });
        };

        $scope.getPendingTask = function (options, type, user_id) {
            var loadedTaskPromise = deskService.taskLoad(options, type, $scope.pendingtaskPage, user_id);
            loadedTaskPromise.success(function (data, header) {
                if (data.data != null)
                {
                    $rootScope.TaskData.Pending = data.data;
                    $rootScope.TaskData.Pending.count = data.count;
                    $scope.loadMorePending = data.loadMoreTag;
                    $scope.pendingtaskPage++;
                }
            }).error(function (data, header) {
                //write a notifications
            });
        };

        $scope.loadMorePendingTask = function () {
            var type, id;
            if ($rootScope.selectedUser) {
                type = 'user';
                id = $rootScope.selectedUser.user_id;
            } else if ($rootScope.selectedGroup) {
                type = 'group';
                id = $rootScope.selectedGroup.id;
            } else {
                type = 'user';
                id = $rootScope.loggeduser;
            }
            var loadedTaskPromise = deskService.taskLoad('pending', type, $scope.pendingtaskPage, id);
            loadedTaskPromise.success(function (data, header) {
                if (data.data != null)
                {
                    $scope.addtoPendingTask(data.data);
                    $rootScope.TaskData.Pending.count = data.count;
                    $scope.loadMorePending = data.loadMoreTag;
                    $scope.pendingtaskPage++;
                }
            }).error(function (data, header) {
                //write a notifications
            });
        };

        $scope.loadMoreCompletedTask = function () {
            var type, id;
            if ($rootScope.selectedUser) {
                type = 'user';
                id = $rootScope.selectedUser.user_id;
            } else if ($rootScope.selectedGroup) {
                type = 'group';
                id = $rootScope.selectedGroup.id;
            } else {
                type = 'user';
                id = $rootScope.loggeduser;
            }
            var loadedTaskPromise = deskService.taskLoad('completed', type, $scope.completedtaskPage, id);
            loadedTaskPromise.success(function (data, header) {
                if (data.data != null)
                {
                    $scope.addtoCompletedTask(data.data);
                    $rootScope.TaskData.Completed.count = data.count;
                    $scope.loadMoreComplete = data.loadMoreTag;
                    $scope.pendingtaskPage++;
                }
            }).error(function (data, header) {
                //write a notifications
            });
        };

        // Data manipulation

        $scope.addtoPendingTask = function (data) {
            for (var i in data) {
                $rootScope.TaskData.Pending.push(data[i]);
            }
        };

        $scope.addtoCompletedTask = function (data) {
            for (var i in data) {
                $rootScope.TaskData.Completed.push(data[i]);
            }
        };

        // Data manipulation End

        /*  load task data from server and store to $rootScope.TaskData */
        $scope.loadTaskData = function (type, id, callback) {
            $scope.getUpcomingTask(options.today, type, id);
            $scope.getUpcomingTask(options.tomorrow, type, id);
            $scope.getCompletedTask(options.completed, type, id);
            $scope.getPendingTask(options.pending, type, id);
            $scope.getUpcomingTask(options.this_week, type, id);
            $scope.getUpcomingTask(options.next_week, type, id);
        };

        $scope.loadTaskData('user');

        $scope.deleteCommentModal = function (id, commentId, option) {
            createDialog(null, {
                id: 'taskcommentdelete',
                title: 'Delete Comment',
                deleteleave: 0,
                string: 'Are you sure you want to delete comment?',
                backdrop: true,
                success: {label: 'Yes, Delete', fn: function () {
                        $scope.deleteComment(id, commentId, option);
                    }},
                cancel: {label: 'Cancel', fn: function () {
                    }}
            });
        };

        $scope.deleteComment = function (task_id, commentId, option) {
            var deleteCommentPromise = deskService.deleteComment(task_id, commentId);

            deleteCommentPromise.success(function (data, header) {
                var index = $rootScope.getTaskById(task_id, option).index;
                $scope.deleteCommentfromArray(index, option, commentId);
            }).error(function (data, header) {
                notify(data.message, header);
            });
        };

        $scope.findindexOfComemnt = function (comment_id, comments) {
            var index = 0;
            for (index in comments) {
                if (comment_id == comments[index].id) {
                    return index;
                }
            }
            return -1;
        };

        $scope.deleteCommentfromArray = function (index, option, commentId, comments) {
            if (option == 'today') {
                var indexofComment = $scope.findindexOfComemnt(commentId, $rootScope.TaskData.Today[index]['Comment']);
                $rootScope.TaskData.Today[index]['Task']['comment_count']--;
                $rootScope.TaskData.Today[index]['Comment'].splice(indexofComment, 1);
            } else if (option == 'tomorrow') {
                var indexofComment = $scope.findindexOfComemnt(commentId, $rootScope.TaskData.Tomorrow[index]['Comment']);
                $rootScope.TaskData.Tomorrow[index]['Task']['comment_count']--;
                $rootScope.TaskData.Tomorrow[index]['Comment'].splice(indexofComment, 1);
            } else if (option == 'this_week') {
                var indexofComment = $scope.findindexOfComemnt(commentId, $rootScope.TaskData.This_week[index]['Comment']);
                $rootScope.TaskData.This_week[index]['Task']['comment_count']--;
                $rootScope.TaskData.This_week[index]['Comment'].splice(indexofComment, 1);
            } else if (option == 'completed') {
                var indexofComment = $scope.findindexOfComemnt(commentId, $rootScope.TaskData.Completed[index]['Comment']);
                $rootScope.TaskData.Completed[index]['Task']['comment_count']--;
                $rootScope.TaskData.Completed[index]['Comment'].splice(indexofComment, 1);
            } else if (option == 'pending') {
                var indexofComment = $scope.findindexOfComemnt(commentId, $rootScope.TaskData.Pending[index]['Comment']);
                $rootScope.TaskData.Pending[index]['Task']['comment_count']--;
                $rootScope.TaskData.Pending[index]['Comment'].splice(indexofComment, 1);
            } else if (option == 'search') {
                var indexofComment = $scope.findindexOfComemnt(commentId, $scope.searchedTasks[index]['Comment']);
                $scope.searchedTasks[index]['Task']['comment_count']--;
                $scope.searchedTasks[index]['Comment'].splice(indexofComment, 1);
            } else {
                var indexofComment = $scope.findindexOfComemnt(commentId, $rootScope.TaskData.Other[index]['Comment']);
                $rootScope.TaskData.Other[index]['Task']['comment_count']--;
                $rootScope.TaskData.Other[index]['Comment'].splice(indexofComment, 1);
            }
        };

        $scope.addComment = function (task_id, body, option) {
            var addCommentPromise = deskService.addComment(task_id, body);
            addCommentPromise.success(function (data, header) {
                var index = $rootScope.getTaskById(task_id, option).index;
                $scope.addCommenttoArray(index, option, data);
                trackEvents.trackTaskAddComment('Task-Add-Comment', {body: data.data[0].body});
            }).error(function (data, header) {
                notify(data.message, header);
            });
        };

        $scope.addCommenttoArray = function (index, option, data) {
            if (option == 'today') {
                $rootScope.TaskData.Today[index]['Task']['comment_count']++;
                $rootScope.TaskData.Today[index]['Comment'].unshift(data.data[0]);
            } else if (option == 'tomorrow') {
                $rootScope.TaskData.Tomorrow[index]['Task']['comment_count']++;
                $rootScope.TaskData.Tomorrow[index]['Comment'].unshift(data.data[0]);
            } else if (option == 'this_week') {
                $rootScope.TaskData.This_week[index]['Task']['comment_count']++;
                $rootScope.TaskData.This_week[index]['Comment'].unshift(data.data[0]);
            } else if (option == 'completed') {
                $rootScope.TaskData.Completed[index]['Task']['comment_count']++;
                $rootScope.TaskData.Completed[index]['Comment'].unshift(data.data[0]);
            } else if (option == 'pending') {
                $rootScope.TaskData.Pending[index]['Task']['comment_count']++;
                $rootScope.TaskData.Pending[index]['Comment'].unshift(data.data[0]);
            } else if (option == 'search') {
                $scope.searchedTasks[index]['Task']['comment_count']++;
                $scope.searchedTasks[index]['Comment'].unshift(data.data[0]);
            } else {
                $rootScope.TaskData.Other[index]['Task']['comment_count']++;
                $rootScope.TaskData.Other[index]['Comment'].unshift(data.data[0]);
            }
        };

        $scope.markTaskComplete = function (task_id, is_completed, option) {

            var markcompletePromise = deskService.markTaskasComplete(task_id, is_completed);

            markcompletePromise.success(function (data, header) {
                $scope.markcompleteAction(data.data[0], task_id, option);
                trackEvents.trackTaskMarkComplete('Task-Mark-Complete', {body: data.data[0].body});
                notify(data.message, header);
            }).error(function (data, header) {
                notify(data.message, header);

            });
        };

        $scope.markcompleteAction = function (data, task_id, option) {
            var checkSuccess = $scope.removeFromArray(task_id, option);

            if (checkSuccess) {
                if (option == 'completed') {
                    $rootScope.TaskData.Pending.count++;
                    $scope.addTasktoArray(data);
                    $rootScope.TaskData.Completed.count--;
                } else if (option == 'pending') {
                    $rootScope.TaskData.Pending.count--;
                    $rootScope.TaskData.Completed.count++;
                    $rootScope.TaskData.Completed.unshift(data);
                } else {
                    $rootScope.TaskData.Completed.count++;
                    $rootScope.TaskData.Completed.unshift(data);
                }
            }
        };

        $scope.removeFromArray = function (task_id, option) {

            var index = $rootScope.getTaskById(task_id, option).index;
//            console.log(index);
            if (option == 'today') {
                $rootScope.TaskData.Today.splice(index, 1);
                return true;
            } else if (option == 'tomorrow') {
                $rootScope.TaskData.Tomorrow.splice(index, 1);
                return true;
            } else if (option == 'this_week') {
                $rootScope.TaskData.This_week.splice(index, 1);
                return true;
            } else if (option == 'other') {
                $rootScope.TaskData.Other.splice(index, 1);
                return true;
            } else if (option == 'pending') {
                $rootScope.TaskData.Pending.splice(index, 1);
                return true;
            } else if (option == 'completed') {
                $rootScope.TaskData.Completed.splice(index, 1);
                return true;
            } else if (option == 'search') {
                $scope.searchedTasks.splice(index, 1);
                return true;
            } else {
                return false;
            }
        };

        $scope.taskDelete = function (task_id, option) {
            var taskDeltePromise = deskService.taskDelete(task_id);

            taskDeltePromise.success(function (data, header) {
                var checkSuccess = $scope.removeFromArray(task_id, option);
                if (option == 'completed') {
                    $rootScope.TaskData.Completed.count--;
                } else if (option == 'pending') {
                    $rootScope.TaskData.Pending.count--;
                }
                trackEvents.trackTaskDelete('Task-Delete');
            }).error(function (data, header) {
                notify(data.message, header);
            });
        };

        $scope.taskDeleteModal = function (task_id, options) {
            createDialog(null, {
                id: 'deleteTask',
                title: 'Task Delete',
                deleteleave: 0,
                string: 'Are you sure you want to delete this Task ?',
                backdrop: true,
                success: {label: 'Yes, Delete', fn: function () {
                        $scope.taskDelete(task_id, options);
                    }},
                cancel: {label: 'Cancel', fn: function () {

                    }}
            });
        };

        $scope.getComment = function (id, lastId) {
            var index = deskService.getTaskById(id).index;
            if (angular.isUndefined($rootScope.TaskData[index]['moreComments']) || $rootScope.TaskData[index]['moreComments'] !== false) {
                var getCommentPromise = deskService.getComment(id, lastId);
                getCommentPromise.success(function (data) {
                    $rootScope.TaskData[index]['Comment'] = ($rootScope.TaskData[index]['Comment']).concat(data.data.comments);
                    $rootScope.TaskData[index]['moreComments'] = data.data['loadMore'];
                    $rootScope.taskSelectItem(id);
                })
                        .error(function (data, header) {
                            notify(data.message, header);
                        });
            }
        };
        //for notifications when task data wont be there

        $rootScope.getTaskById = function (id, type) {
            var result;
            if (type == 'today') {
                result = deskService.findTaskinArray(id, $rootScope.TaskData.Today);
            }
            else if (type == 'tomorrow') {
                result = deskService.findTaskinArray(id, $rootScope.TaskData.Tomorrow);
            }
            else if (type == 'this_week') {
                result = deskService.findTaskinArray(id, $rootScope.TaskData.This_week);
            }
            else if (type == 'other') {
                result = deskService.findTaskinArray(id, $rootScope.TaskData.Other);
            }
            else if (type == 'pending') {
                result = deskService.findTaskinArray(id, $rootScope.TaskData.Pending);
            }
            else if (type == 'completed') {
                result = deskService.findTaskinArray(id, $rootScope.TaskData.Completed);
            } else if (type == 'search') {
                result = deskService.findTaskinArray(id, $scope.searchedTasks);
            }

            else {
                return;
            }

            if (result == false) {
                var getTaskPromise = deskService.getTaskById(id);
                getTaskPromise.success(function (data, header) {
                    $scope.TaskDataById = data.data[0];
                    return $scope.TaskDataById;
                });
            }
            else {
                $scope.TaskDataById = result;
                return $scope.TaskDataById;
            }
        };

        //Search Task Module

        $rootScope.$on('task_searched', function (event, args) {
            $scope.taskSearch(args);
        });

        $rootScope.$on('task_search_cleared', function (event, args) {
            $scope.clearSearch();
        });

        $scope.searchActive = false;
        $scope.searchPage = 1;
        $scope.searchedTasks;
        $scope.searchTask = '';

        $scope.taskSearch = function (query, page) {
            $scope.searchActive = true;
            if (!page) {
                page = 1;
            }
            var searchTaskPromise;
            searchTaskPromise = deskService.taskLoad('all', query, $scope.searchPage);
            searchTaskPromise.success(function (data) {
                $scope.searchedTasks = data.data;
                if ($scope.searchTask == query) {
                    $scope.searchPage++;
                }
                trackEvents.trackTaskSearch('Task-Search', {query: query});
            });
        };

        $scope.clearSearch = function () {
            $scope.searchActive = false;
            $scope.searchPage = 1;
            $scope.searchedTasks = null;
            $scope.searchTask = '';
        };

        //Search Task End

        //Date and time picker
        $scope.defaultDatePicker;
        $scope.defaultTimePicker;
        $scope.arrowsDatePicker = {
            year: {
                left: '/img/datepicker/white_arrow_left.svg',
                right: '/img/datepicker/white_arrow_right.svg'
            },
            month: {
                left: '/img/datepicker/grey_arrow_left.svg',
                right: '/img/datepicker/grey_arrow_right.svg'
            }
        };

        $scope.headerOfDates = {
            monday: 'Mon',
            tuesday: 'Tue',
            wednesday: 'Wed',
            thursday: 'Thu',
            friday: 'Fri',
            saturday: 'Sat',
            sunday: 'Sun',
        };

        //Date and time picker End
        /* Nlp Task Add */
        $scope.currentTask = "";
        $scope.FoundStartDate = Date.today().setTimeToNow();
        $scope.FoundEndDate = (Date.today().setTimeToNow()).addHours(1);
        $scope.FoundEndDateOnly = ($scope.FoundEndDate).toString('MMMM dS, yyyy');
        $scope.FoundEndDateExpression = "";
        $scope.FoundEndTimeExpression = "";
        $scope.FoundEndTime = $scope.FoundEndDate;
        $scope.FoundEndTimeOnly = ((Date.today().setTimeToNow()).addHours(1)).toString('h:mm tt');
        $scope.currentMention = "";
        $scope.enterNewTaskActivate = true;
        $scope.tagsforTask = [];
        $scope.AddingTask = false;
        $scope.AddedTask = false;

        $scope.searchUserAndGroup = function (query, currentMention) {

            $scope.enterNewTaskActivate = false;
            $scope.UserAndGroupTag;
            var usersearchPrmoise = runService.searchUserAndGroup(query);
            usersearchPrmoise.success(function (data) {
                $scope.UserAndGroupTag = data.data.Tags;
            });
            return $scope.UserAndGroupTag;
        };

        $scope.getPeopleText = function (item) {
            $scope.tagsforTask.push(item);
            $scope.enterNewTaskActivate = true;
            $scope.UserAndGroupTag;
            return '@' + item.name;
//            return '<b>' + '@' + item.name + '</b>'
        };

        $scope.removetaskTag = function (index) {
            $scope.tagsforTask.splice(index, 1);
        };

        $scope.taskAddNlp = function (title) {
            if ($scope.enterNewTaskActivate == true || (angular.isUndefined($scope.UserAndGroupTag))) {
                if (title == '') {
                    return false;
                }
                var data = {};
                data.Task = {};
                data.TaskAssignment = {};
                data.TaskAssignment.assignee_data = {};
                data.Task.title = title;
                data.Task.start_date = $scope.FoundStartDate.toString('MM/dd/yyyy hh:mm tt');
                data.Task.end_date = $scope.FoundEndDate.toString('MM/dd/yyyy hh:mm tt');
                data.Task.priority = 0;
                if ($scope.checkforImp(title)) {
                    data.Task.priority = 1;
                }
                var i = 0;
                for (i; i < $scope.tagsforTask.length; i++) {
                    data.TaskAssignment.assignee_data[i] = $scope.tagsforTask[i];
                }
                var result = $scope.taskAdd(data);
                if (result) {
                    $scope.currentTask = '';
                    $scope.tagsforTask = [];
                    $scope.taskCallBackInit();
                    $scope.AddingTask = false;
                    $scope.AddedTask = true;
                    setTimeout(function () {
                        $scope.AddedTask = false;
                    }, 500);
                }
            }
        };

        $scope.taskCallBackInit = function () {
            $scope.FoundStartDate = Date.today().setTimeToNow();
            $scope.FoundEndDate = (Date.today().setTimeToNow()).addHours(1);
            $scope.FoundEndDateOnly = ($scope.FoundEndDate).toString('MMMM dS, yyyy');
            $scope.FoundEndDateExpression = "";
            $scope.FoundEndTimeExpression = "";
            $scope.FoundEndTime = $scope.FoundEndDate;
            $scope.FoundEndTimeOnly = ((Date.today().setTimeToNow()).addHours(1)).toString('h:mm tt');
        };

        $scope.checkforImp = function (title) {
            var match;
            var regex = /Important/i;
            if (title.match(regex)) {
                match = regex.exec(title);
                return match;
            }
            return false;
        };

        $scope.checkforTodayTask = function () {
            var now_time = Date();
            if ((moment($scope.FoundEndDateExpression).isAfter(now_time))) {
                return true;
            }
            else {
                return false;
            }
        };

        $scope.checkDateandTime = function (taskTitle) {

            var date = chrono.parse(taskTitle);
            if ((date.length > 0) && !($scope.FoundEndDateExpression)) {
                //check if task is of todays

                $scope.FoundEndDateExpression = date[0].start.date();

                if ($scope.checkforTodayTask()) {
                    $scope.FoundEndDate = $scope.FoundEndDateExpression;
                }
            } else if ((date.length > 0)) {
                if (date[0].end) {
                    $scope.FoundStartDate = date[0].start.date();
                    $scope.FoundEndDate = date[0].end.date();
                    $scope.FoundEndDateExpression = $scope.FoundEndDate;
                } else {
                    $scope.FoundEndDateExpression = date[0].start.date();
                    if ($scope.checkforTodayTask()) {
                        $scope.FoundEndDate = $scope.FoundEndDateExpression;
                    }
                }
            } else if ((date.length <= 0) && ($scope.FoundEndDateExpression)) {
                $scope.FoundEndDate = (Date.today().setTimeToNow()).addHours(1);
            }
        };

        /* Nlp task Complete*/

        /* Modal Window start */

        $scope.showProfileofUser = function (user) {
            $scope.ProfileofUser = user;
            var modalInstance = $modal.open({
                templateUrl: '/scripts/View/users/userProfile.html',
                controller: 'ModalInstanceCtrl',
                scope: $scope,
            });
        };

        //  rajvi's work 
        $scope.profiletemplate = {
            templateUrl: '/scripts/View/users/profileOnPopover.html'
        };
        //  -------------------------

        $scope.advTaskmodalInstance;
        $scope.editTask = false;
        $scope.NewTask = {};
        $scope.NewTask.type;

        $scope.taskAddModalwindow = function (Task, priority, date, time, tags) {
            var data = {};
            data.Task = {};
            data.TaskAssignment = {};
            data.TaskAssignment.assignee_data = {};
            data.Task.title = Task.Task.title;
            data.Task.priority = 0;
            if (priority) {
                data.Task.priority = true;
            }
            date = Date.parse(date);
            time = Date.parse(time);
            date.setHours(time.getHours());
            date.setMinutes(time.getMinutes());
            data.Task.end_date = date.toString('MM/dd/yyyy hh:mm tt');
            var i = 0;
            if (tags && tags.length > 0) {
                for (i = 0; i < $scope.tagsforTask.length; i++) {
                    $scope.tagsforTask[i] = tags[i];
                }
            }
            for (i = 0; i < $scope.tagsforTask.length; i++) {
                data.TaskAssignment.assignee_data[i] = $scope.tagsforTask[i];
            }
            if ($scope.editTask == true) {
                data.Task.id = Task.Task.id;
                data.Task.start_date = Task.Task.start_date;
                data.Task.created_by = Task.Task.created_by;
                data.Task.comment_count = Task.Task.comment_count;
                data.type = $scope.NewTask.type;
                var result = $scope.taskEdit(data);
                $scope.editTask = false;
            } else {
                var result = $scope.taskAdd(data);
            }
            $scope.NewTask = '';

            $scope.newTaskmodalInstance.close();
            $scope.tagsforTask = [];
            $scope.taskCallBackInit();
            if ($scope.currentTask) {
                $scope.currentTask = '';
            }
        };

        $scope.taskEdit = function (Task) {

            //send to service to edit and then at index set the response
            if (angular.equals({}, Task))
            {
                return false;
            }
            var taskType = Task.type;
            delete Task.type;
            delete Task.Comment;
            deskService.taskEdit(Task).success(function (data, header) {
                var index = $rootScope.getTaskById(Task.Task.id, taskType).index;
                if (taskType == 'today') {
                    $rootScope.TaskData.Today.splice(index, 1);
                    $rootScope.TaskData.Today.push(data.data[0]);
                }
                else if (taskType == 'tomorrow') {
                    $rootScope.TaskData.Tomorrow.splice(index, 1);
                    $rootScope.TaskData.Tomorrow.push(data.data[0]);
                }
                else if (taskType == 'this_week') {
                    $rootScope.TaskData.This_week.splice(index, 1);
                    $rootScope.TaskData.This_week.push(data.data[0]);
                } else if (taskType == 'pending') {
                    $rootScope.TaskData.Pending.splice(index, 1);
                    $rootScope.TaskData.Pending.push(data.data[0]);
                } else if (taskType == 'completed') {
                    $rootScope.TaskData.Completed.splice(index, 1);
                    $rootScope.TaskData.Completed.push(data.data[0]);
                }
                else {
                    $rootScope.TaskData.Other[index] = data.data[0];
                }
                trackEvents.trackTaskAdd('Task-Edit', {title: data.data[0].Task.title, shared_count: data.data[0].Tags.length - 1});
                notify(data.message, header);

            }).error(function (data, header) {
                notify(data.message, header);
            });
            return true;
        };

        $scope.taskRemoveAssignee = function (assignee, task_id, type) {
            var remove_data = {};
            remove_data.value = assignee.value;
            remove_data.type = assignee.type;
            var data = {};
            data.TaskAssignment = {};
            data.TaskAssignment.remove_data = remove_data;
            data.TaskAssignment.task_id = task_id;

            var taskRemovePromise = deskService.removeassignee(task_id, data);
            taskRemovePromise.success(function (data, header) {
                var index = $rootScope.getTaskById(task_id, type);//get task index
                $rootScope.TaskData[index] = data.data[0];//store the task at it's index
                $scope.NewTask = data.data[0];//set the NewTask as this new data
                notify(data.message, header);
            }).error(function (data, header) {
                notify(data.message, header);
            });
        };

        $scope.taskRemoveAssigneeConfirm = function (assignee, task_id) {
            var message;
            if (assignee.type == 'group')
            {
                message = 'Are you sure you want to remove <strong>' + assignee.name + '</strong> ?';
            } else
            {
                message = 'Are you sure you want to remove <strong>' + assignee.name + '</strong> ?';
            }
            createDialog(null, {
                id: 'removeTaskAssignee',
                title: 'Remove Assignee',
                deleteleave: 0,
                header: false,
                footer: true,
                string: message,
                backdrop: true,
                success: {label: 'Yes, Remove', fn: function () {
                        $scope.taskRemoveAssignee(assignee, task_id, $scope.NewTask.type);
                    }},
                cancel: {label: 'Cancel', fn: function () {

                    }}
            });
        };

        $scope.newTaskModal = function (Task, type) {
            $scope.defaultDatePicker = $scope.FoundEndDate.toString('MMM dd, yyyy');
            $scope.defaultTimePicker = $scope.FoundEndDate.toString('h:mm tt');

            if (Task.Task) {
                $scope.editTask = true;
                $scope.NewTask = Task;
                $scope.NewTask.type = type;
                $scope.defaultDatePicker = Date.parse(Task.Task.end_date).toString('MMM dd, yyyy');
                $scope.defaultTimePicker = Date.parse(Task.Task.end_date).toString('h:mm tt');
                trackEvents.trackTaskActivity('Task-Edit-Modal-Open');
            } else {
                $scope.editTask = false;
                $scope.NewTask.Task = {};
                $scope.NewTask.Task.title = Task;
                trackEvents.trackTaskActivity('Task-Advance-Modal-Open');
            }

            $scope.newTaskmodalInstance = $modal.open({
                templateUrl: '/scripts/View/Desk/taskFull.html?bust=' + Math.random().toString(36).slice(2),
                windowClass: 'task-full-modal',
                controller: 'ModalInstanceCtrl',
                scope: $scope,
                animation: true
            });



        };

        $scope.deleteTaskModal = function () {
            $scope.deleteTaskModalInstance = $modal.open({
                templateUrl: '/scripts/View/Desk/deleteTask.html?bust=' + Math.random().toString(36).slice(2),
                controller: 'ModalInstanceCtrl',
                scope: $scope
            });
        };

        /* Modal window End */

//        $scope.autoExpand = function (e) {
//            var element = typeof e === 'object' ? e.target : document.getElementById(e);
//            var scrollHeight = element.scrollHeight - 0; // replace 60 by the sum of padding-top and padding-bottom
//            element.style.height = scrollHeight + "px";
//        };

//        function expand() {
//            $scope.autoExpand('TextArea');
//        }

        $scope.resizeTextArea = function (stringLength) {
            $rootScope.$broadcast('elastic:adjust');
        };

    }]);

app.controller('navBarController', ['$rootScope', '$scope', 'runService', '$modal', '$http', '$httpParamSerializerJQLike', '$timeout', 'workspaceService', '$sce', 'trackEvents', function ($rootScope, $scope, runService, $modal, $http, $httpParamSerializerJQLike, $timeout, workspaceService, $sce, trackEvents) {

        $scope.currenturlPath = (window.location.pathname).split("/")[1];
        $scope.collapseusers = true;
        $scope.collapsegroups = true;
        $rootScope.selectedUser;
        $rootScope.selectedGroup;
        $scope.userPage = 2;
        $scope.groupPage = 2;
        $scope.loadMoreGroup = false;
        $scope.loadMoreUser = false;
        $rootScope.taskFilterOn = false;
        $rootScope.fileFilterOn = false;
        $scope.loadingUsers = false;
        $scope.UserAndGroupTag = null;

        $scope.toogleSelection = function (type) {
            if (type == 'user') {
                $scope.collapseusers = !($scope.collapseusers);
            } else if (type == 'group') {
                $scope.collapsegroups = !($scope.collapsegroups);
            }
        };

        //  Rajvi's Work | Check number of people
        $scope.checkPeople = $rootScope.users.length;
        //  Rajvi's Work ends

        //make a method to search

        $scope.selectUser = function (user) {
            if ($scope.currenturlPath == 'desks') {
                $rootScope.taskFilterOn = true;
                $rootScope.$broadcast('user_Selected_task', user);
            }
            else if ($scope.currenturlPath == 'docs') {
                $rootScope.fileFilterOn = true;
                $rootScope.$broadcast('user_Selected_file', user);
            }
            $rootScope.selectedGroup = "";
            $rootScope.selectedUser = user;
            $rootScope.isSelectedUser = true;
        };

        $scope.selectGroup = function (group) {
            if ($scope.currenturlPath == 'desks') {
                $rootScope.taskFilterOn = true;
                $rootScope.$broadcast('group_Selected_task', group);
            }
            else if ($scope.currenturlPath == 'docs') {
                $rootScope.fileFilterOn = true;
                $rootScope.$broadcast('group_Selected_file', group);
            }
            $rootScope.selectedUser = "";
            $rootScope.selectedGroup = group;
            $rootScope.isSelectedUser = false;
        };

        $rootScope.clearFilter = function (option) { //option for desk and files page
            $rootScope.selectedUser = null;
            $rootScope.selectedGroup = null;
            if ($scope.currenturlPath == 'desks') {
                $rootScope.taskFilterOn = false;
                $rootScope.$broadcast('clear_Filter_task', '');
            }
            else if ($scope.currenturlPath == 'docs') {
                $rootScope.fileFilterOn = false;
                $rootScope.$broadcast('clear_Filter_file', '');
            }
        };

        $scope.loadMoreUser = function () {
            var user_network_id = $rootScope.users[$rootScope.users.length - 1].UserInfo.user_network_id;

            $scope.userPromise = runService.getUsersData($scope.userPage, user_network_id);
            $scope.userPromise.success(function (data, header) {
                $scope.newusers = data.data['users'];
                $scope.i = 0;
                for ($scope.i; $scope.i < $scope.newusers.length; $scope.i++) {
                    $rootScope.users.push($scope.newusers[$scope.i]);
                }
                $rootScope.LoadMoreCountUser = data.LoadMoreCount;
                $rootScope.loadMoreUser = data.LoadMore;
                $scope.userPage++;
            }).error(function (data, header) {
                notify(data.message, header);
            });
        };

        $scope.loadMoreGroup = function () {
            var user_group_id = $rootScope.groups[$rootScope.groups.length - 1].Group.user_group_id;

            $scope.groupPromise = runService.getGroupsData($scope.groupPage, user_group_id);
            $scope.groupPromise.success(function (data, header) {
                $scope.newgroups = data.data['groups'];
                var i = 0;
                for (i; i < $scope.newgroups.length; i++) {
                    $rootScope.groups.push($scope.newgroups[i]);
                }
                $rootScope.LoadMoreCountGroup = data.LoadMoreCount;
                $rootScope.loadMoreGroup = data.LoadMore;
                $scope.groupPage++;
                trackEvents.trackTeamActivity('Group-Load-More');
            }).error(function (data, header) {
                notify(data.message, header);
            });
        };

        $scope.searchUserAndGroup = function (query) {
            $scope.UserAndGroupTag = null;
            var searchPromise = runService.searchUserAndGroup(query);
            searchPromise.success(function (data) {
                $scope.UserAndGroupTag = data.data.Tags;
                trackEvents.trackSearchUserGroup('Search-User-Group');
            });
            return $scope.UserAndGroupTag;
        };

        $scope.selectMatch = function (model, data) {
            if (model.type == 'user') {
                $scope.selectUser(model);
            } else if (model.type == 'group') {
                $scope.selectGroup(model);
            }
            $scope.customSelected = null;
        };

//         $scope.typeaheadOpts = {
//            inputFormatter: myInputFormatterFunction,
//            loading: myLoadingBoolean,
//            minLength: 3,
//            onSelect: myOnSelectFunction, // this will be run in addition to directive internals
//            templateUrl: '/path/to/my/template.html',
//            waitMs: 500,
//            allowsEditable: true
//        };

        $scope.extractEmails = function (emails) {
            var data = {};
            data.email = {};
            angular.forEach(emails, function (value, key) {
                data.email[key] = value.email;
            });
            return data;
        };

        $scope.invitenetworkModal;

        $scope.addToNetwork = function (emails) {
            var data = {};
            data = $scope.extractEmails(emails);
            data.User = {};
            data.User.return_to = 'invite_network';
            var url = '/users/admin_add';
            var xsrf = $httpParamSerializerJQLike({data: data});
            var addToNetworkPromise = $http({
                method: 'POST',
                url: url,
                data: xsrf
            });
            addToNetworkPromise.success(function (data) {
                $scope.addpeopleNetwork(data.data.info);
                $scope.invitenetworkModal.close();
                trackEvents.trackWorkspaceActivity('Workspace-Invite-People');
            });
        };

        $scope.addpeopleNetwork = function (data) {
            for (var i in data) {
                $rootScope.users[$rootScope.users.length] = {};
                $rootScope.users[$rootScope.users.length - 1].UserInfo = data[i];
            }
        };


        //team module
        $scope.addTeam = function () {

            $scope.addTeammodalInstance = $modal.open({
                templateUrl: '/scripts/View/teams/addTeam.html?bust=' + Math.random().toString(36).slice(2),
                controller: 'ModalInstanceCtrl',
//                animation: true,
                scope: $scope
            });
        };

        $rootScope.$on('group_close', function (event, args) {
            $scope.addTeammodalInstance.close();
        });

        //

        $scope.inviteToNetwork = function () {

            $scope.invitenetworkModal = $modal.open({
                templateUrl: '/scripts/View/Network/inviteToNetwork_1.html?bust=' + Math.random().toString(36).slice(2),
                controller: 'navBarController',
                scope: $scope,
                controller: 'ModalInstanceCtrl'
            });
        };


        $scope.teamSettingsmodalInstance;
        $rootScope.editTeamGroup = {};

        $rootScope.$on('group_edit_close', function (event, args) {
            $scope.teamSettingsmodalInstance.close();
        });

        $scope.editGroupModal = function (group) {
            $rootScope.editTeamGroup = group;

            $scope.teamSettingsmodalInstance = $modal.open({
                templateUrl: '/scripts/View/teams/teamSettings.html?bust=' + Math.random().toString(36).slice(2),
                controller: 'ModalInstanceCtrl'
//            windowClass: 'task-full-modal',
            });
        };

        //appear In module starts
        $scope.emailsforChat = [];
        $scope.roomName = "";
        $scope.chatmodalInstance;
        $scope.InvitemodalInstance;

        $scope.searchUserAutoLoadChat = function (query, current) {

            $scope.autoLoadedUsersChat;
            var searchUserPromise = runService.searchUserAndGroup(query, 'user');
            searchUserPromise.success(function (data) {
                $scope.autoLoadedUsersChat = data.data.Tags;
            });
            return $scope.autoLoadedUsersChat;
        };

        $scope.getUsersChat = function (item) {
            $scope.emailsforChat.push(item.email);
            return '@' + item.name;
        };

        $scope.sendChatInvite = function (inviteapearIn) {
            var url = '/users/sendMail';
            var data = {};
            data.link = $rootScope.AppearChatLink.toString();
            data.email = {};
            var i = 0;
            for (i; i < $scope.emailsforChat.length; i++) {
                data.email[i] = $scope.emailsforChat[i];
            }
            var xsrf = $httpParamSerializerJQLike({data: data});
            var sendInvitationPromise = $http({
                method: 'POST',
                url: url,
                data: xsrf
            });
            sendInvitationPromise.success(function (data) {
                $scope.InvitemodalInstance.close();
                $scope.appearInChat();
            });
        };

        $scope.openAppearInvite = function () {
            $scope.InvitemodalInstance = $modal.open({
                templateUrl: '/scripts/View/chat/appearInChatInvite.html?bust=' + Math.random().toString(36).slice(2),
                //windowClass: 'appearChatWindow',
                scope: $scope
            });
            trackEvents.trackAppearIn('Appear-Invit-Modal-Open');
        };

        var AppearIn = window.AppearIn;
        var appearin = new AppearIn();

        $scope.launchAppearChat = function () {

            // Returns true if browser can use WebRTC, false otherwise
            var isWebRtcCompatible = appearin.isWebRtcCompatible();

            if (!isWebRtcCompatible) {
                alert("Please upgrade your browser. WebRTC not supported.");
                return;
            }

            // Promise-based workflow (recommended)
            appearin.getRandomRoomName().then(function (roomName) {
                $scope.roomName = roomName;
                $rootScope.AppearChatLink = $sce.trustAsResourceUrl("https://appear.in" + roomName);
                // do something with the roomName
                var iframe = document.getElementById("appear-iframe");
                appearin.addRoomToIframe(iframe, $scope.roomName);
            });
            $scope.openAppearInvite();
            trackEvents.trackAppearIn('Appear-Chat-Modal-Open');
        };

        $scope.appearInChat = function () {

            $scope.chatmodalInstance = $modal.open({
                templateUrl: '/scripts/View/chat/appearInChat.html?bust=' + Math.random().toString(36).slice(2),
                controller: 'navBarController',
                windowClass: 'appearChatWindow',
                size: 'lg'
            });

        };

        //appear In module ends

        //Edit Network Module start
        $scope.EditWorkspaceModal;

        $scope.editNetwork = {};
        $rootScope.userandNetworkPromise.then(function () {
            $scope.editNetwork.name = $rootScope.currentNetwork;
        });

        $scope.editNetworkName = function (Network) {
            var editNetworkPromise;
            var data = {};
            data.Network = {};
            data.Network.name = Network.name;
            editNetworkPromise = workspaceService.editNetworkName(data);
            editNetworkPromise.success(function (data) {
                $rootScope.currentNetwork = data.data.Network.name;
                $scope.EditWorkspaceModal.close();
                trackEvents.trackWorkspaceActivity('Workspace-Rename', {old_name: Network.name, new_name: data.data.Network.name});
            });
        };



        $scope.openModalEditWorkspace = function () {
            $scope.EditWorkspaceModal = $modal.open({
                templateUrl: '/scripts/View/Network/renameWS.html?bust=' + Math.random().toString(36).slice(2),
                controller: 'ModalInstanceCtrl',
                scope: $scope
            });

        };

        //Upgrade the workspace plan
        $scope.upgradeNetworkPlan = function () {
            upgradeNetworkPromise = workspaceService.upgradeNetworkPlan();
            upgradeNetworkPromise.success(function (data) {
                $scope.UpgradeWorkspaceModal.close();
            });
        };

        $scope.UpgradeWorkspaceModal;
        $scope.openModalUpgradeWorkSpaceRequest = function () {
            $scope.UpgradeWorkspaceModal = $modal.open({
                templateUrl: '/scripts/View/Network/upgradeWS.html?bust=' + Math.random().toString(36).slice(2),
                controller: 'ModalInstanceCtrl',
                scope: $scope
            });
        };
        //Edit netwrok module add


    }]);

app.controller('ModalInstanceCtrl', ['$scope', '$modal', '$modalInstance', function ($scope, $modal, $modalInstance) {

        $scope.ok = function () {
            $modalInstance.close($scope.selected.item);
        };

        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };
    }]);

app.factory('groupService', ['$rootScope', '$http', '$q', '$httpParamSerializerJQLike', function ($rootScope, $http, $q, $httpParamSerializerJQLike) {

        var factory = {};

        factory.getGroupById = function (group_id) {

            var groupObject = $http({
                method: 'GET',
                url: '/groups/getGroup/' + group_id
            });
            return groupObject;
        };

        factory.groupCreate = function (data) {
            var groupCreatePromise;
            if (data) {
                var xsrf = $httpParamSerializerJQLike({data: data});
                groupCreatePromise = $http({
                    method: 'Post',
                    url: '/groups/add/',
                    data: xsrf
                });
                return groupCreatePromise;
            }
            return false;
        };

        factory.groupEditName = function (id, name, oldname) {
            var data = {};
            var groupEditPromise;
            if (name !== oldname)
            {
                data.name = name;
                data.group_id = id;
                var xsrf = $httpParamSerializerJQLike({data: data});
                groupEditPromise = $http({
                    method: 'POST',
                    url: '/groups/edit/',
                    data: xsrf
                });
                return groupEditPromise;
            }
            return false;
        };

        factory.groupAddMembers = function (id, users) {            
            var addMembersPrmoise;
            var data = {
                users : {
                    userlist : {} 
                },
                group_id : id
            };
            for (var i in users) {
                data.users.userlist[i] = users[i].user_id;
            }
            var xsrf = $httpParamSerializerJQLike({data: data});
            addMembersPrmoise = $http({
                method: 'POST',
                url: '/groups/addmember/',
                data: xsrf
            });
            return addMembersPrmoise;

        };

        factory.groupRemoveMembers = function (user_id, group_id) {
            var data = {};
            var removeMembersPromise;
            data.group_id = group_id;
            data.user_id = user_id;
            var xsrf = $httpParamSerializerJQLike({data: data});
            removeMembersPromise = $http({
                method: 'Post',
                url: '/groups/removemember/',
                data: xsrf
            });
            return removeMembersPromise;
        };
        // delete group and leave group
        factory.groupDelete = function (group_id) {
            var data = {};
            data.group_id = group_id;
            var groupDeletePromise;
            var xsrf = $httpParamSerializerJQLike({data: data});
            groupDeletePromise = $http({
                method: 'POST',
                url: '/groups/delete/',
                data: xsrf
            });
            return groupDeletePromise;
        };

        factory.groupLeave = function (group_id) {
            var data = {};
            var groupLeavePromise;
            data.group_id = group_id;
            var xsrf = $httpParamSerializerJQLike({data: data});
            groupLeavePromise = $http({
                method: 'POST',
                url: '/groups/leave/',
                data: xsrf
            });
            return groupLeavePromise;
        };

        factory.findGroup = function (id, groups) {
            for (var i in groups) {
                if (groups[i]['Group']['id'] == id) {
                    return i;
                }
            }
            return -1;
        };

        factory.checkEmail = function (email) {
            var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return re.test(email);
        };

        return factory;

    }]);

app.controller('GroupController', ['$rootScope', 'groupService', '$scope', 'runService', '$modal', 'createDialog', 'trackEvents', function ($rootScope, groupService, $scope, runService, $modal, createDialog, trackEvents) {
        /* Data Manipulation functions */
        $scope.tagsforGroup = [];
        $scope.teamRename = false;
        $scope.emailtagsforGroup = [];

        $scope.groupAdd = function (group) {
             var data = {
                users : {
                    userlist : {}
                },
                Group : {}
            };
            
            data.Group.name = group.Group.name;
            
            var i = 0;
            for (i; i < $scope.tagsforGroup.length; i++) {
                data.users.userlist[i] = $scope.tagsforGroup[i].user_id;
            }
            var groupAddPromise = groupService.groupCreate(data);
            groupAddPromise.success(function (data) {
                $scope.groupDataAdd(data.data);
                trackEvents.trackTeamActivity('Team-Add');
            });
            $rootScope.$broadcast('group_close', '');
        };

        $scope.addMember = function (group_id) {
            var addMembersPromise;
            addMembersPromise = groupService.groupAddMembers(group_id, $scope.tagsforGroup);
            addMembersPromise.success(function (data) {
                $scope.groupDataEdit(group_id, data.data);
                $scope.tagsforGroup = [];
                $scope.addToTeam='';
                trackEvents.trackTeamActivity('Team-Add-Member');
            });
        };

        $scope.editGroupName = function (group_id, name, oldname) {
            var editGroupPromise;
            editGroupPromise = groupService.groupEditName(group_id, name, oldname);
            editGroupPromise.success(function (data) {
                $scope.groupDataEdit(group_id, data.data);
                $scope.teamRename = false;
                trackEvents.trackTeamActivity('Team-Rename');
            });
        };

        $scope.groupRemoveMember = function (user_id, group_id) {
            var removeMemberPromise;
            removeMemberPromise = groupService.groupRemoveMembers(user_id, group_id);
            removeMemberPromise.success(function (data) {
                $scope.groupDataEdit(group_id, data.data);
                trackEvents.trackTeamActivity('Team-Remove-Member');
            });
        };

        $scope.deleteGroup = function (group_id) {
            var deleteGroupPromise;
            deleteGroupPromise = groupService.groupDelete(group_id);
            deleteGroupPromise.success(function (data) {
                $scope.groupDataDelete(group_id);
                $rootScope.$broadcast('group_edit_close', '');
                trackEvents.trackTeamActivity('Team-Delete');
            });
        };

        $scope.leaveGroup = function (group_id) {
            var leaveGroupPromise;
            leaveGroupPromise = groupService.groupLeave(group_id);
            leaveGroupPromise.success(function (data) {
                $scope.groupDataDelete(group_id);
                $rootScope.$broadcast('group_edit_close', '');
                trackEvents.trackTeamActivity('Team-Leave');
            });
        };

        $scope.groupDataAdd = function (data) {
            $rootScope.groups.push(data);
        };

        $scope.groupDataDelete = function (group_id) {
            var index = groupService.findGroup(group_id, $rootScope.groups);
            $rootScope.groups.splice(index, 1);
        };

        $scope.groupDataEdit = function (group_id, data) {
            var index = groupService.findGroup(group_id, $rootScope.groups);
            $rootScope.groups[index] = data;
            $rootScope.editTeamGroup = data;
        };

        $scope.groupDataReplace = function (group_id, data) {
            var index = groupService.findGroup(group_id, $rootScope.groups);
            $rootScope.groups[index] = data;
        };
        //Modal Windows
        $scope.groupModalDelete = function (group_id, name) {

            createDialog(null, {
                id: 'groupdeletemodal',
                title: 'delete group',
                deleteleave: 0,
                string: 'Are you sure you want to delete group ' + '<span class=groupnamemodal>' + name + '</span> ?',
                success: {label: 'Yes, Delete', fn: function () {
                        $scope.deleteGroup(group_id);
                    }},
                cancel: {label: 'Cancel', fn: function () {
                    }}
            });
        };

        $scope.groupModalLeave = function (group_id, name) {

            createDialog(null, {
                id: 'leavegroupmodal',
                title: 'leave group',
                deleteleave: 1,
                string: 'Are you sure you want to leave group ' + '<span class=groupnamemodal>' + name + '</span> ?',
                success: {label: 'Yes, Leave', fn: function () {
                        $scope.leaveGroup(group_id);
                    }},
                cancel: {label: 'Cancel', fn: function () {
                    }}

            });
        };

        $scope.groupModalMemberRemove = function (group_id, user_id, group_name, user_name) {

            createDialog(null, {
                id: 'groupmodalmemberremove',
                title: 'Remove Member',
                deleteleave: 0,
                string: 'Are you sure you want to remove member ' + user_name + ' from group ' + group_name + '?',
                success: {label: 'Yes, Delete', fn: function () {
                        $scope.groupRemoveMember(user_id, group_id);
                    }},
                cancel: {label: 'Cancel', fn: function () {
                    }}
            });
        };

        //autocomplete Start

        $scope.removegroupTag = function (index) {
            $scope.tagsforGroup.splice(index, 1);
        };

        $scope.searchUserAutoLoad = function (query, current) {
            $scope.autoLoadedUsers;
            var searchUserPromise = runService.searchUserAndGroup(query, 'user');
            searchUserPromise.success(function (data) {
                $scope.autoLoadedUsers = data.data.Tags;
            });
            return $scope.autoLoadedUsers;
        };

        $scope.getUsers = function (item) {
            $scope.tagsforGroup.push(item);
            return '@' + item.name;
        };

        //autocomplete End

    }]);

// File module starts


app.factory('docsService', ['$http', '$q', 'commentService', '$httpParamSerializerJQLike', function ($http, $q, commentService, $httpParamSerializerJQLike) {

        var factory = {};

        factory.getFilesData = function (type, id, page, sort) {
            var filesDataPromise;
            if (!sort) {
                sort = 'create1';
            }
            var url = '/docs/getFiles/' + type + '/' + id + '/' + page + '/' + sort;
            filesDataPromise = $http({
                method: 'GET',
                url: url
            });
            return filesDataPromise;
        };

        factory.fileRename = function (doc_id, newName) {
            var fileRenamePromise;
            var data = {};
            data.name = newName;
            data.doc_id = doc_id;
            var xsrf = $httpParamSerializerJQLike({data: data});
            fileRenamePromise = $http({
                method: 'POST',
                url: '/docs/edit',
                data: xsrf
            });
            return fileRenamePromise;
        };

        factory.getFileById = function (doc_id) {
            var filePromise;
            if (doc_id) {
                filePromise = $http({
                    method: 'GET',
                    url: '/docs/getDoc/' + doc_id
                });
                return filePromise;
            }
            else {
                return false;
            }
        };

        factory.fileUpload = function (data) {
            var fileUploadPromise;
            if (data) {
                var xsrf = $httpParamSerializerJQLike({data: data});
                fileUploadPromise = $http({
                    method: 'POST',
                    url: '/docs/xUpload.json',
                    data: xsrf
                });
                return fileUploadPromise;
            }
            else {
                return false;
            }
        };


        factory.fileDownload = function (id) {
            var filedownloadPromise;
            if (id) {
                filedownloadPromise = $http({
                    method: 'GET',
                    url: '/docs/download/' + id + '.json'
                });
                return filedownloadPromise;
            }
            else {
                return false;
            }
        };

        factory.fileDelete = function (id) {
            var fileDeletePromise;
            if (id) {
                fileDeletePromise = $http({
                    method: 'POST',
                    url: '/docs/delete/' + id
                });
                return fileDeletePromise;
            }
            else {
                return false;
            }
        };

        factory.shareFile = function (doc_id, users) {
            var shareFilePromise, url;
            url = '/docs/addSharedList';
            var data = {};
            data.userlist = {};
            data.doc_id = doc_id;
            var i = 0;
            for (i; i < users.length; i++) {
                data.userlist[i] = users[i];
            }
            if (data) {
                var xsrf = $httpParamSerializerJQLike({data: data});
                shareFilePromise = $http({
                    method: 'POST',
                    url: url,
                    data: xsrf
                });
                return shareFilePromise;
            }
            else {
                return false;
            }
        };

        factory.addComment = function (id, body) {
            var commentAddPromise;
            if (body && body.trim()) {
                commentAddPromise = commentService.add('docs', id, body);
                return commentAddPromise;
            }
            else {
                return false;
            }
        };

        factory.deleteComment = function (id, commentId) {
            var commentDeletePromise;
            if (commentId) {
                commentDeletePromise = commentService.delete(commentId);
                return commentDeletePromise;
            }
            else {
                return false;
            }
        };

        factory.getComment = function (id, lastId) {
            var getCommentPromise;
            if (id) {
                getCommentPromise = commentService.get('docs', id, lastId);
                return getCommentPromise;
            }
            else {
                return false;
            }
        };

        factory.filePreviewUrl = null;

        factory.frameBenchPreview = function (doc_id) {
            var framebenchPromise;
            framebenchPromise = $http({
                method: 'GET',
                url: '/docs/preview/' + doc_id + '.json',
                cache: true
            });
            return framebenchPromise;
        };

        factory.frameBenchMimes = function (mime) {
            var supported_mimes = ['image/gif', 'image/jpeg', 'image/jpg', 'image/png', 'image/tiff', 'image/vnd.adobe.photoshop', 'application/pdf', 'video/mp4', 'application/mp4', 'video/x-m4v', 'application/vnd.ms-powerpoint', 'application/vnd.openxmlformats-officedocument.presentationml.presentation', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document', 'application/msword', 'application/vnd.ms-excel', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'];
            if (supported_mimes.indexOf(mime) > -1)
                return true;
            else
                return false;
        };

        factory.findFile = function (id, array) {
            for (var i in array) {
                if (array[i]['Doc']['id'] == id) {
                    return i;
                }
            }
            return -1;
        };

        factory.fileDropbox = function (file, userlist) {
            var fileDropboxPromise;
            var url = '/docs/xUpload';
            var data = {};
            data.userlist = {};
            data.Doc = file;
            var i = 0;
            for (i; i < userlist.length; i++) {
                data.userlist[i] = userlist[i];
            }
            var xsrf = $httpParamSerializerJQLike({data: data});
            fileDropboxPromise = $http({
                method: 'POST',
                url: url,
                data: xsrf
            });
            return fileDropboxPromise;
        };

        return factory;
    }]);


app.controller('docsController', ['$rootScope', '$scope', '$http', 'docsService', '$modal', 'createDialog', 'runService', 'trackEvents', '$sce', 'Upload', 'trackEvents', function ($rootScope, $scope, $http, docsService, $modal, createDialog, runService, trackEvents, $sce, Upload, trackEvents) {

        $scope.files;
        $scope.showSearch = false;
        $scope.filePage = 1;
        $scope.selectedFile;
        $scope.loadMoreFiles;
        $scope.showFiles = true;
        $scope.searchSelectedFile;
        $scope.filequery;
        $scope.tagsforFile = [];
        $scope.filePreviewUrl;
        $scope.reshareDocModal;
        $scope.uploadFileModal;
        $scope.loadingFiles = true;
        $scope.renameFileModal;

        $scope.getFilesData = function (type, id, page, sort) {
            if (!type) {
                type = 'user';
            }
            if (!id) {
                id = $rootScope.loggeduser;
            }
            if (!page) {
                page = $scope.filePage;
            }
            if (!sort) {
                sort = 'create1';
            }
            var filesDataPromise = docsService.getFilesData(type, id, page, sort);

            filesDataPromise.success(function (data, header) {
                $scope.files = data.data.files;
                $scope.loadMoreFiles = data.loadMore;
                $scope.filePage++;
                $scope.selectFile($scope.files[0]);
            });
            return filesDataPromise;
        };

        $scope.loadMoreFile = function () {
            var type, id, page;
            page = $scope.filePage;
            if ($rootScope.selectedUser) {
                type = 'user';
                id = $rootScope.selectedUser.user_id;
            } else if ($rootScope.selectedGroup) {
                type = 'group';
                id = $rootScope.selectedGroup.id;
            } else {
                type = 'user';
                id = $rootScope.loggeduser;
            }
            var filesDataPromise = docsService.getFilesData(type, id, page);
            filesDataPromise.success(function (data, header) {
                if ($scope.files.length <= 0) {
                    $scope.files = data.data.files;
                } else {
                    $scope.fileDataAdd(data.data.files);
                }
                $scope.loadMoreFiles = data.loadMore;
                $scope.filePage++;
                $scope.selectFile($scope.files[0]);
            });
        };

        $scope.fileRename = function (doc_id, newName) {
            var fileRenamePromise;
            fileRenamePromise = docsService.fileRename(doc_id, newName);
            fileRenamePromise.success(function (data, header) {
                $scope.fileDataEdit(doc_id, data.data['Doc']);
                trackEvents.trackFileRename('File-Renamed');
                $scope.renameFileModal.close();
                notify(data.message, header);
            }).error(function (data, header) {
                notify(data.message, header);
            });
        };

        $scope.renameFileModal = function () {
            $scope.renameFileModal = $modal.open({
                templateUrl: '/scripts/View/docs/renameFile.html?bust=' + Math.random().toString(36).slice(2),
                controller: 'ModalInstanceCtrl',
                scope: $scope
            });
        };

        $scope.fileDeleteModal = function (id, name) {
            createDialog(null, {
                id: 'filedeletemodal',
                title: 'Delete file',
                deleteleave: 0,
                string: 'Are you sure you want to delete file ' + '<span class=filenamemodal>' + name + '</span> ?',
                backdrop: true,
                success: {label: 'Yes, Delete', fn: function () {
                        $scope.fileDelete(id);
//                        console.log(id);
                    }},
                cancel: {label: 'Cancel', fn: function () {

                    }}
            });
        };

        $scope.fileDelete = function (id) {
            var fileDetlePromise;
//            console.log(id);
            fileDetlePromise = docsService.fileDelete(id);
            fileDetlePromise.success(function (data, header) {
                var index = docsService.findFile(id, $scope.files);
                $scope.files.splice(index, 1);
//                console.log(index);
                if(!$scope.showFiles){
                  $scope.toggleSearch('');  
                }else{
                 $scope.selectFile($scope.files[index]);   
                }
                trackEvents.trackFileDelete('File-Delete');
                notify(data.message, header);
            }).error(function (data, header) {
                notify(data.message, header);
            });
        };

        $scope.shareFile = function (doc_id) {

            var shareFilePromise = docsService.shareFile(doc_id, $scope.tagsforFile);
            shareFilePromise.success(function (data) {
                $scope.reshareDocModal.close();
                $scope.fileDataEditUsers(doc_id, data.data.DocAssignment);
                $scope.tagsforFile = [];
                trackEvents.trackFileShare('File-Share');
            });
        };

        $scope.selectFile = function (file) {
//            $rootScope.hideRightBar = false;
            $scope.selectedFile = file;
        };

        $scope.checkforAdmin = function (id) {
            if ($rootScope.loggeduser == id) {
                return true;
            } else {
                return false;
            }
        };

        $scope.sortFiles = function (value) {
            $scope.paginationFileInit();
            if ($rootScope.selectedUser) {
                $scope.getFilesData('user', $rootScope.selectedUser.user_id, $scope.filePage, value);
            } else if ($rootScope.selectedGroup) {
                $scope.getFilesData('group', $rootScope.selectedGroup.id, $scope.filePage, value);
            } else {
                $scope.getFilesData('user', $rootScope.loggeduser, $scope.filePage, value);
            }
        };

        $scope.addComment = function (id, body) {
            var commentAddPromise;
            commentAddPromise = docsService.addComment(id, body);
            commentAddPromise.success(function (data, header) {
                var index = docsService.findFile(id, $scope.files);
                $scope.files[index]['Comment'].unshift(data.data[0]);
                if(!$scope.showFiles){
                    $scope.selectedFile['Comment'].unshift(data.data[0]);
                }
                $scope.files[index]['Doc']['comment_count'] = $scope.files[index]['Doc']['comment_count'] + 1;
                trackEvents.trackFileAddComment('FileAddComment');
            })
                    .error(function (data, header) {
                        notify(data.message, header);
                    });
        };

        $scope.deleteCommentModal = function (id, commentId) {
            createDialog(null, {
                id: 'filecommentdelete',
                title: 'Delete Comment',
                deleteleave: 0,
                string: 'Are you sure you want to delete comment?',
                backdrop: true,
                success: {label: 'Yes, Delete', fn: function () {
                        $scope.deleteComment(id, commentId);
                    }},
                cancel: {label: 'Cancel', fn: function () {
                    }}
            });
        };

        $scope.deleteComment = function (id, commentId) {
            var deleteCommentPromise;
            deleteCommentPromise = docsService.deleteComment(id, commentId);
            deleteCommentPromise.success(function (data, header) {
                var index = docsService.findFile(id, $scope.files);
                $scope.files[index]['Comment'] = ($scope.files[index]['Comment']).filter(function (comment) {
                    return comment.id != commentId;
                });
                if(!$scope.showFiles){
                    $scope.selectedFile['Comment']=($scope.selectedFile['Comment']).filter(function (comment) {
                    return comment.id != commentId;
                });
                }
                $scope.files[index]['Doc']['comment_count'] = $scope.files[index]['Doc']['comment_count'] - 1;
                notify(data.message, header);
            })
                    .error(function (data, header) {
                        notify(data.message, header);
                    });
        };

        $scope.getComment = function (id, lastId) {
            var getCommentPromise;
            if (angular.isUndefined(lastId)) {
                var lastId = null;
            }
            var index = docsService.findFile(id, $scope.files);
            if (angular.isUndefined($scope.files[index]['moreComments']) || $scope.files[index]['moreComments'] !== false) {
                getCommentPromise = docsService.getComment(id, lastId);
                getCommentPromise.success(function (data, header) {
                    $scope.files[index]['Comment'] = ($scope.files[index]['Comment']).concat(data.data.comments);
                    $scope.files[index]['moreComments'] = data.data['loadMore'];
                })
                        .error(function (data, header) {
                            notify(data.message, header);
                        });
            }
        };

        $scope.matchingfileMime = function (mime) {
            var check = docsService.frameBenchMimes(mime);
            return check;
        };

        // File manipulation
        $scope.fileDataAdd = function (data) {
            for (var i in data) {
                $scope.files.push(data[i]);
            }
        };

        $scope.filedataUnshift = function (data) {
            for (var i in data) {
                $scope.files.unshift(data[i]);
            }
        };

        $scope.fileDataEdit = function (id, data) {
            var index = docsService.findFile(id, $scope.files);
            $scope.files[index]['Doc'] = data;
        };
        $scope.fileDataEditUsers = function (id, data) {
            var index = docsService.findFile(id, $scope.files);
            $scope.files[index]['DocAssignment'] = data;
        };
        $scope.fileDataReplace = function (id, data) {
            var index = docsService.findFile(id, $scope.files);
            $scope.files[index] = data;
        };

        //File Manipulation

        $rootScope.$on('file_selected', function (event, args) {
            $scope.selectFile(args);
        });

        $rootScope.$on('file_search_toggled', function (event, args) {
            $scope.toggleSearch(args);
        });

        $scope.toggleSearch = function (query) {
            $scope.selectFile(null);
            $scope.showFiles = false;
            if (query == '') {
                $rootScope.$broadcast('clear_header_search');
                $scope.selectFile($scope.files[0]);
                $scope.showFiles = true;
            }
        };

        $rootScope.userandNetworkPromise.success(function () {
            $scope.getFilesData('user').success(function () {
                $scope.loadingFiles = false;
            });
        });

        $scope.searchUserAndGroup = function (query, currentMention) {
            $scope.UserAndGroupTag;
            var usersearchPrmoise = runService.searchUserAndGroup(query);
            usersearchPrmoise.success(function (data) {
                $scope.UserAndGroupTag = data.data.Tags;
            });
            return $scope.UserAndGroupTag;
        };
        $scope.getPeopleText = function (item) {
            $scope.tagsforFile.push(item);
            // note item.label is sent when the typedText wasn't found
            return '@' + item.name;
        };

        //upload module

        $scope.newFiles = [];
        $scope.newFileDropbox = {};
        $scope.completedFiles = [];
        $scope.progressBars = [];
        $scope.uploadFilePromise = [];
        $scope.uploadingAnimate = false;
 
        
        $scope.filterUploadEvent = function ($files, $file, $event, $rejectedFiles) {
            if ($files && $files.length > 0) {
                $scope.uploadEvent($files);
                $scope.openUploadFileModal();
            }
        };

        $scope.uploadEvent = function ($files) {
            if ($files.length > 0) {
                var start_length = $scope.newFiles.length;
                var end_length = (start_length + $files.length);
                $scope.savetoFileModel($files);
                for (var j = start_length; j < end_length; j++) {
                    $scope.uploadingAnimate = true;
                    (function (index) {
                        var file = $files[index];
                        $scope.uploadFilePromise[index] = $scope.uploadFile(file);
                        $scope.uploadFilePromise[index].progress(function (evt) {
                        $scope.progressBars[index] = $scope.calculateProgress(evt);
                        console.log($scope.progressBars[index]);
                        }).success(function (data, status, headers, config) {
                            $scope.files.unshift(data.data);
                            $scope.completedFiles.unshift(data.data);
                            $scope.removeFileModel(file);
                            $scope.progressBars[index] = 100;
                            $scope.selectFile(data.data);
                            $scope.uploadingAnimate = $scope.checkIfAlluploaded();
                            trackEvents.trackFileUpload('File-Upload', {file_name: data.data.Doc.name, type: data.data.Doc.file_extension, shared_count: data.data.DocAssignment.length - 1});
                        });
                    })(j);
                }
            }
        };
        
        $scope.uploadFile = function (file) {
            var fileUploadPromise;
            fileUploadPromise = Upload.upload({
                method: 'POST',
                url: '/docs/upload',
                file: file,
                sendFieldsAs: 'form'
            });
            return fileUploadPromise;
        };

        $scope.checkIfAlluploaded = function () {
            var result = true;
            var sum = 0;
            for (var i in $scope.progressBars) {
                sum += $scope.progressBars[i];
            }
            if ((($scope.progressBars.length * 100) / sum) == 1) {
                result = false;
            }
            return result;
        };

        $scope.calculateProgress = function (evt) {
            var progress = parseInt(100.0 * evt.loaded / evt.total) - 1;
            if (progress > 0) {
                return progress;
            } else {
                return 0;
            }
        };

        $scope.savetoFileModel = function ($files) {
            if($scope.newFiles.length <=0 ){
             for (var i in $files) {
                $scope.newFiles[i] = $files[i];
            }   
            }else{
                for (var i in $files) {
                $scope.newFiles.push($files[i]);
            }
            }
        };

        $scope.removeCompleted = function (index) {
             $scope.completedFiles.splice(index, 1);
        };
        
        $scope.removeFileModel = function (file) {
            var str = file.name.toLowerCase();
            for (var i in $scope.newFiles) {
                if ($scope.newFiles[i]['name'].toLowerCase() == str) {
                    $scope.newFiles.splice(i, 1);
                }
            }
        };

        $scope.fileDropbox = function (userlist) {
            var fileDropboxPromise = docsService.fileDropbox($scope.newFileDropbox, userlist);
            fileDropboxPromise.success(function (data, header) {
                $scope.completedFiles.push(data.data);
                if ($scope.files) {
                    $scope.files.unshift(data.data);
                } else {
                    $scope.files = data.data;
                }
//                notify(data.message, header);
            }).error(function (data, header) {
//                notify(data.message, header);
            });
        };

        $scope.optionsDropbox = {
            // Required. Called when a user selects an item in the Chooser.
            success: function (files) {
                $scope.newFileDropbox = files[0];
                $scope.newFileDropbox.provider = 'dropbox';
                $scope.newFileDropbox.preview_url = files[0].link;
                $scope.newFileDropbox.thumbnailLink = files[0].icon;
                $scope.newFileDropbox.size = files[0].bytes;
                $scope.newFileDropbox.url = files[0].link.replace(/(dl=)[^\&]+/, '$1' + 1);
                if (!$scope.uploadFileModal) {
                    $scope.openUploadFileModal();
                }
                $scope.fileDropbox([]);
            },
            cancel: function () {
                $scope.newFileDropbox = null;
                $scope.$apply();
            },
            linkType: "preview", // or "direct"
            multiselect: false, // or true
            extensions: ['.pdf', '.doc', '.docx', '.jpeg', '.png', '.jpg', '.zip', '.svg', '.txt', '.docx', '.xlsx', '.csv']
        };

        $scope.DropboxUpload = function () {
            Dropbox.choose($scope.optionsDropbox);
        };

        $scope.cancelUpload = function (index) {
            if ($scope.newFiles.length == 1) {
                $scope.uploadingAnimate = false;
            }
            $scope.newFiles.splice(index, 1);
            $scope.progressBars.splice(index, 1);
            $scope.uploadFilePromise[index].abort();
            $scope.uploadFilePromise.splice(index, 1);
        };

        $scope.removeFileTag = function (index) {
            $scope.tagsforFile.splice(index, 1);
        };

        //upload module End

        //Modal Windows Start

        $scope.openModalShareDoc = function () {

            $scope.reshareDocModal = $modal.open({
                templateUrl: '/scripts/View/docs/shareDoc.html?bust=' + Math.random().toString(36).slice(2),
                controller: 'ModalInstanceCtrl',
                scope: $scope,
                animation: true
            });

        };

        $scope.openUploadFileModal = function () {
            $scope.uploadFileModal = $modal.open({
                templateUrl: '/scripts/View/docs/uploadFull2.html?bust=' + Math.random().toString(36).slice(2),
                controller: 'ModalInstanceCtrl',
                scope: $scope,
                windowClass: 'upload-full-modal',
                animation: true
            });
        };



        //Modal windows End


        $rootScope.$on('user_Selected_file', function (event, args) {
            $scope.paginationFileInit();
            $scope.getFilesData('user', args.user_id);
        });

        $rootScope.$on('group_Selected_file', function (event, args) {
            $scope.paginationFileInit();
            $scope.getFilesData('group', args.id);
        });

        $rootScope.$on('clear_Filter_file', function (event, args) {
            $scope.paginationFileInit();
            $scope.getFilesData('user');
        });

        $scope.paginationFileInit = function () {
            $scope.filePage = 1;
        };


        $scope.framebenchPreview = function (doc_id, previewurl, provider) {
            if (provider == 'dropbox') {
                trackEvents.trackFilePreview('File-Dropbox-Preview');
                window.open(previewurl);
                return;
            }
            var frameBenchPromise;
            frameBenchPromise = docsService.frameBenchPreview(doc_id);
            $scope.openFramebenchModal();
            if (previewurl != null) {
                $scope.filePreviewUrl = $sce.trustAsResourceUrl(previewurl);
            } else {
                frameBenchPromise.success(function (data, header) {
                    var index = docsService.findFile(doc_id, $scope.files);
                    $scope.files[index].Doc.preview_url = data.data;
                    $scope.filePreviewUrl = $sce.trustAsResourceUrl(data.data);
                    trackEvents.trackTaskAdd('File-Framebench-Preview');
                }).error(function (data, header) {
//            notify(data.message, header);
                });
            }
        };

        $scope.openFramebenchModal = function () {

            var modalInstance = $modal.open({
                templateUrl: '/scripts/View/docs/frameBench.html?bust=' + Math.random().toString(36).slice(2),
                controller: 'ModalInstanceCtrl',
                windowClass: 'framebench-full-modal',
                scope: $scope,
                animation: true
            });

        };

        //when framebench closes write a function to set filepreviewurl about:blank
    }]);


app.factory('notificationService', ['$rootScope', '$http', function ($rootScope, $http) {

        var factory = {};

        factory.getNotificationsData = function (page,limit) {
            if(!page){
                page = 1;
            }
            if (!limit) {
                limit = 20;
            }
            var notificationsPromise;
            notificationsPromise = $http({
                method: 'GET',
                url: '/logs/getNotifications/'+ page + '/' + limit + '.json'
            });
            return notificationsPromise;
        };
        /*Used on only networks page*/
//        factory.getNotificationsUnseen = function (callback) {
//        $http({
//            method: 'GET',
//            cache: true,
//            url: '/logs/getUnseenNotifications.json'
//        }).success(function (data, header) {
//            $rootScope.notifications = data.data;
//        });
//    };

        /*Used on only networks page*/
        factory.getNotificationsUnseen = function (callback) {
            $http({
                method: 'GET',
                cache: true,
                url: '/logs/getUnseenNotifications.json'
            }).success(function (data, header) {
                $rootScope.notifications = data.data;
            });
        };

        factory.getNotificationUnseenCount = function () {
            var notificationCountPromise;
            notificationCountPromise = $http({
                method: 'GET',
                url: '/datas/getNotificationCount.json'
            });
            return notificationCountPromise;
        };

        factory.setNotificationSeen = function (latestNotificationId, latestNotificationLogId) {
            var setNotificationSeenPromise;
            setNotificationSeenPromise = $http({
                method: 'GET',
                url: '/notifications/setUnreadAll/' + latestNotificationId + '/' +latestNotificationLogId + '.json'
            });
            return setNotificationSeenPromise;
        };

        factory.findNotificationinArray = function (id, array) {
            var i = 0;
            for (i; i < array.length; i++) {
                if (id == array[i].notification_id) {
                    return i;
                }
            }
            return -1;
        };
        
        factory.readSingleNotificationById = function (notification_id) {
            var readSingleNotificationPromise;
            readSingleNotificationPromise = $http({
                method: 'GET',
                url: '/notifications/readNotification/' +notification_id + '.json'
            });
            return readSingleNotificationPromise;
        };
        
        factory.fetchUnseenNotification = function(latest_notification_id){
            var fetchUnseenNotificationPromise;
            fetchUnseenNotificationPromise = $http({
                method: 'GET',
                url: '/logs/getNewNotifications/' + latest_notification_id + '.json'
            });
            return fetchUnseenNotificationPromise;
        };

        return factory;

    }]);

app.controller('notificationController', ['$rootScope', '$scope', 'notificationService', '$interval', '$modal', 'deskService', 'docsService', 'createDialog', 'groupService', '$sce', function ($rootScope, $scope, notificationService, $interval, $modal, deskService, docsService, createDialog, groupService, $sce) {

        $scope.notifications;
        $scope.newNotifications;
        $scope.notificationsCount = 0;
        $scope.notificationPage = 2;
        $scope.notificationPageLimit = 20;
        $scope.loadMoreNotification = false;
        $scope.selectedFile;
        $scope.selectedTask;
        $scope.selectedNotification;
        $scope.fileModal;
        $scope.taskModal;
        $scope.groupModal;
        $scope.ringbell = false;
        $scope.selectedGroup;



        $scope.getNotificationsData = function () {
            var notificationPromise;
            notificationPromise = notificationService.getNotificationsData();
            notificationPromise.success(function (data, header) {
                $scope.notifications = data.data;
                $scope.loadMoreNotification = data.loadMore;
            });
        };
        
        $scope.loadMoreNotifications = function () {
            var loadMoreNotificationsPromise;
            loadMoreNotificationsPromise = notificationService.getNotificationsData($scope.notificationPage,$scope.notificationPageLimit);
            loadMoreNotificationsPromise.success(function (data, header) {
                $scope.newNotifications = data.data;
                $scope.i = 0;
                for ($scope.i; $scope.i < $scope.newNotifications.length; $scope.i++) {
                    $scope.notifications.push($scope.newNotifications[$scope.i]);
                }
//                $scope.notifications = data.data;
                $scope.loadMoreNotification = data.loadMore;
                $scope.notificationPage++;
            });
        };
        

        $scope.setNotificationRead = function () {
            var setNotificationSeenPromise;
            var latestNotificationId = $scope.notifications[0].notification_id;
            var latestNotificationLogId = $scope.notifications[0].log_id;
            setNotificationSeenPromise = notificationService.setNotificationSeen(latestNotificationId,latestNotificationLogId);
            setNotificationSeenPromise.success(function (data, header) {
            });
        };

        $scope.getNotificationCount = function () {
            var notificationcountPromise;
            notificationcountPromise = notificationService.getNotificationUnseenCount();
            notificationcountPromise.success(function (data, header) {
                if ($rootScope.notificationsCount < data.data) {
                    $scope.ringbell = true;
                    $scope.getNotificationsData();
                    $scope.notifySound();
                }
                $rootScope.notificationsCount = data.data;
//                $scope.getUnreadNotification();
            });
            return notificationcountPromise;
        };

//        $scope.getUnreadNotification = function(){
//            if($rootScope.notificationsCount > 0 && !angular.isUndefined($scope.notifications)){
//                    var unseenNotificationFetchPromise;
//                    var latestNotificationId = $scope.notifications[0].notification_id;
//                    unseenNotificationFetchPromise = notificationService.fetchUnseenNotification(latestNotificationId);
//                    unseenNotificationFetchPromise.success(function(data,header){
//                    if(!angular.isUndefined(data.data) && data.data != null){
//                        $scope.notifications = data.data;
//                    }
//                    });
//            }
//        };

        $scope.getNotificationsData();
        $scope.getNotificationCount();
        
        $rootScope.notificationClick = function () {
            $scope.notificationPage = 2;
            $scope.getNotificationsData();
            $scope.getNotificationCount();
//            $scope.setNotificationRead();
//            $rootScope.notificationsCount = 0;
        };
        
        $scope.markAllNotificationAsRead = function(){
            $scope.setNotificationRead();            
            $rootScope.notificationsCount = 0;
            $scope.readAllNotifications();
        };
        
        $scope.readAllNotifications = function(){
            var i = 0;
            for (i; i < $scope.notifications.length; i++) {
            var notification = $scope.notifications[i];
            notification.isSeen = true;
            }
        };
        
        //Modal Windows

        $scope.getFileById = function (doc_id) {
            $scope.selectedFile = {};
            var filePromise;
            filePromise = docsService.getFileById(doc_id);
            filePromise.success(function (data) {
                $scope.selectedFile = data.data;
            });
            return filePromise;
        };

        $scope.getTaskById = function (task_id) {
            $scope.selectedTask={};
            var taskPromise;
            taskPromise = deskService.getTaskById(task_id);
            taskPromise.success(function (data) {
                $scope.selectedTask = data.data[0];
            });
            return taskPromise;
        };

        $scope.getGroupById = function (group_id) {
            $scope.selectedGroup = {};
            var groupPromise;
            groupPromise = groupService.getGroupById(group_id);
            groupPromise.success(function (data) {
                $scope.selectedGroup = data.data;
            });
            return groupPromise;
        };
        
        $scope.readNotificationById = function(notification_id){
            var readnotificationPromis;
            readnotificationPromis = notificationService.readSingleNotificationById(notification_id);
            readnotificationPromis.success(function(data){
//            $rootScope.notificationsCount = data.count;
            });
            return readnotificationPromis;
        };
        

        $scope.setNoficationData = function (notification) {
            $scope.selectedNotification = notification;
            $scope.selectedNotification.time = Date.parse($scope.selectedNotification.time);
        };

        $scope.checkModalState = function (type) {
            
            if ($scope.taskModal && type != 'task') {
                $scope.taskModal.close();
            }
            if ($scope.fileModal && type != 'file') {
                $scope.fileModal.close();
            }
            if ($scope.groupModal && type != 'group') {
                $scope.groupModal.close();
            }
        };

        $scope.openNotificationmodal = function (notification, sameModaltype) {
            $scope.setNoficationData(notification);
             
            if ($scope.selectedNotification.type == "file") {
                var filePromise;
                filePromise = $scope.getFileById(notification.type_id);
                filePromise.success(function () {
                    if (!sameModaltype) {
                        $scope.openFileNotification();
                        $scope.checkModalState('file');
                    }
                }).error(function () {
                });
            }
            else if ($scope.selectedNotification.type == "task") {
                var taskPromise;
                taskPromise = $scope.getTaskById(notification.type_id);
                taskPromise.success(function () {
                    if (!sameModaltype) {
                        $scope.openTaskNotification();
                        $scope.checkModalState('task');
                    }
                }).error(function () {
                });
            }
            else if ($scope.selectedNotification.type == "group") {
                var groupPromise;
                groupPromise = $scope.getGroupById(notification.type_id);
                groupPromise.success(function () {
                    if (!sameModaltype) {
                        $scope.openTeamNotification();
                        $scope.checkModalState('group');
                    }
                }).error(function () {
                });
            }
            else if (notification.type == "network") {

            }

            if($scope.selectedNotification.isSeen == 0){
            var setSingleNotificationSeenPromise;
            setSingleNotificationSeenPromise = $scope.readNotificationById(notification.notification_id);
            setSingleNotificationSeenPromise.success(function (data, header) {
                $rootScope.notificationsCount = data.count;
                var notification_id  = $scope.selectedNotification.notification_id;
                $scope.markAsReadAction(notification_id);
            });
            }
        };
        
        $scope.markAsReadAction = function (notification_id){
            var index = $scope.findNotificationinArray(notification_id, $scope.notifications);
            var notification = $scope.notifications[index];
            notification.isSeen = true;
        };

        $scope.findNotificationinArray = function (id, array) {
            var index = notificationService.findNotificationinArray(id, array);
            return index;
        };

        $scope.checknotification = function (id) {
            var index = notificationService.findNotificationinArray(id, $scope.notifications);
            if (index == 0) {
                return 'first';
            } else if (index == ($scope.notifications.length - 1)) {
                return 'last';
            }
            return index;
        };

        $scope.nextNotification = function (id, current_type) {
            $scope.selectedNotification={};
            var index = $scope.findNotificationinArray(id, $scope.notifications);
            var notification = $scope.notifications[index + 1];
            var sameModaltype = false;
            if (current_type == notification.type) {
                sameModaltype = true;
            }
            $scope.openNotificationmodal(notification, sameModaltype);

            //and close the current one
        };

        $scope.previousNotification = function (id, current_type) {
            $scope.selectedNotification={};
            var index = $scope.findNotificationinArray(id, $scope.notifications);
            var notification = $scope.notifications[index - 1];
            var sameModaltype = false;
            if (current_type == notification.type) {
                sameModaltype = true;
            }
            $scope.openNotificationmodal(notification, sameModaltype);
        };

        $scope.openFileNotification = function () {

            $scope.fileModal = $modal.open({
                templateUrl: '/scripts/View/Notification/notificationFile.html',
                controller: 'ModalInstanceCtrl',
                animation: true,
                scope: $scope
            });
        };

        $scope.openTeamNotification = function () {

            $scope.groupModal = $modal.open({
                templateUrl: '/scripts/View/Notification/notificationTeam.html',
                controller: 'ModalInstanceCtrl',
                animation: true,
                scope: $scope
            });
        };

        $scope.openTaskNotification = function () {

            $scope.taskModal = $modal.open({
                templateUrl: '/scripts/View/Notification/notificationTask.html',
                controller: 'ModalInstanceCtrl',
                animation: true,
                scope: $scope
            });

        };

        //Modal Windows End

        //task methods start

        $scope.deleteCommentModal = function (id, commentId) {
            createDialog(null, {
                id: 'taskcommentdelete',
                title: 'Delete Comment',
                deleteleave: 0,
                string: 'Are you sure you want to delete comment?',
                backdrop: true,
                success: {label: 'Yes, Delete', fn: function () {
                        $scope.deleteComment(id, commentId);
                    }},
                cancel: {label: 'Cancel', fn: function () {
                    }}
            });
        };

        $scope.deleteComment = function (task_id, commentId) {
            var deleteCommentPromise = deskService.deleteComment(task_id, commentId);

            deleteCommentPromise.success(function (data, header) {
                $scope.selectedTask['Task']['comment_count']--;
                $scope.selectedTask['Comment'] = ($scope.selectedTask['Comment']).filter(function (comment) {
                    return comment.id != commentId;
                });
            }).error(function (data, header) {
                notify(data.message, header);
            });
        };

        $scope.addComment = function (task_id, body) {
            var addCommentPromise = deskService.addComment(task_id, body);
            addCommentPromise.success(function (data, header) {
                $scope.selectedTask['Task']['comment_count']++;
                $scope.selectedTask['Comment'].unshift(data.data[0]);
            }).error(function (data, header) {
                notify(data.message, header);
            });
        };

        $scope.taskDelete = function (task_id) {
            var taskDeltePromise = deskService.taskDelete(task_id);

            taskDeltePromise.success(function (data, header) {
                $scope.taskModal.close();
            }).error(function (data, header) {
                notify(data.message, header);
            });
        };

        $scope.taskDeleteModal = function (task_id) {
            createDialog(null, {
                id: 'deleteTask',
                title: 'Task Delete',
                deleteleave: 0,
                string: 'Are you sure you want to delete this Task ?',
                backdrop: true,
                success: {label: 'Yes, Delete', fn: function () {
                        $scope.taskDelete(task_id);
                    }},
                cancel: {label: 'Cancel', fn: function () {

                    }}
            });
        };


        $scope.markTaskComplete = function (task_id, is_completed) {

            var markcompletePromise = deskService.markTaskasComplete(task_id, is_completed);

            markcompletePromise.success(function (data, header) {
                $scope.taskModal.close();
                notify(data.message, header);
            }).error(function (data, header) {
                notify(data.message, header);

            });
        };

        //task method end

        $scope.notifySound = function(){
           var aud = angular.element(document.querySelector('#notifySound'))[0];
           aud.play();
           $scope.ringbell = false;
        };

        $scope.timer = $interval(function () {
            $scope.getNotificationCount();
        }, 15000);
    }]);


//File module Ends

app.factory('workspaceService', ['$rootScope', '$http', '$httpParamSerializerJQLike', function ($rootScope, $http, $httpParamSerializerJQLike) {
        var factory = {};

        factory.getAllnetworks = function () {
            var allNetoworksPromise, url;
            url = '/datas/getNetworks.json';
            allNetoworksPromise = $http({
                method: 'GET',
                url: url});
            return allNetoworksPromise;
        };

        factory.addNetwork = function (data) {
            var addNetworkPromise, url;
            url = '/networks/add.json';
            var xhrf = $httpParamSerializerJQLike({data: data});
            addNetworkPromise = $http({
                method: 'POST',
                url: url,
                data: xhrf});
            return addNetworkPromise;
        };

        factory.delteNetwork = function (id) {
            var deleteNetworkPromise, url;
            url = '/networks/delete.json';
            var xhrf = $httpParamSerializerJQLike({data: id});
            deleteNetworkPromise = $http({
                method: 'POST',
                url: url,
                data: xhrf});
            return deleteNetworkPromise;
        };

        factory.editNetworkName = function (data) {
            var editNetworkPromise, url;
            url = '/networks/edit.json';
            var xhrf = $httpParamSerializerJQLike({data: data});
            editNetworkPromise = $http({
                method: 'POST',
                url: url,
                data: xhrf});
            return editNetworkPromise;
        };
        
        factory.upgradeNetworkPlan = function () {
            var upgradeNetworkPromise, url;
            url = '/networks/upgrade_plan.json';
            upgradeNetworkPromise = $http({
                method: 'POST',
                url: url});
            return upgradeNetworkPromise;
        };

        return factory;
    }]);

app.controller('workspaceController', ['$scope', '$modal', 'workspaceService', '$rootScope', 'trackEvents', function ($scope, $modal, workspaceService, $rootScope, trackEvents) {

        $scope.networks;
        $scope.Network = {};
        $scope.loadingWorkspaces = true;

        $scope.getAllnetworks = function () {
            var getNetowrksPromise;
            getNetowrksPromise = workspaceService.getAllnetworks();
            getNetowrksPromise.success(function (data, header) {
                var networksObj = data.data['networks'];
                $scope.loadingWorkspaces = false;
                $scope.networks = Object.keys(networksObj).map(function (k) {
                    return networksObj[k]
                });
                $scope.networks_expired = data.data['networks_expired'];
            });
        };

        $scope.addNetwork = function (Network) {
            var addNetworkPromise;
            var data = {};
            data.Network = {};
            data.Network.name = Network.name;
            data.Network.return_to = 'model';
            addNetworkPromise = workspaceService.addNetwork(data);
            addNetworkPromise.success(function (data) {
                window.location.href = data.data;
            });
        };

        $scope.editNetworkName = function (Network) {
            var editNetworkPromise;
            var data = {};
            data.Network = {};
            data.Network.name = Network.name;
            editNetworkPromise = workspaceService.editNetworkName(data);
            editNetworkPromise.success(function (data) {

            });
        };

        $scope.openModalAddWorkspace = function () {

            var modalInstance = $modal.open({
                templateUrl: '/scripts/View/Network/addWorkspace.html?bust=' + Math.random().toString(36).slice(2),
                controller: 'ModalInstanceCtrl',
                scope: $scope
            });

        };

        $scope.getAllnetworks();

    }]);


app.factory('usersService', ['$rootScope', '$http', '$httpParamSerializerJQLike', function ($rootScope, $http, $httpParamSerializerJQLike) {
        var factory = {};

        factory.updateProfileInfo = function (data) {
            var updateProfilePromise, url;
            url = '/users/edit.json';
            var xhrf = $httpParamSerializerJQLike({data: data});
            updateProfilePromise = $http({
                method: 'POST',
                url: url,
                data: xhrf});
            return updateProfilePromise;
        };

        return factory;
    }]);

app.controller('usersController', ['$scope', 'usersService', '$rootScope', '$modal', '$http', 'Upload', 'trackEvents', function ($scope, usersService, $rootScope, $modal, $http, Upload, trackEvents) {

        $scope.profileModal;
        $scope.loadingLocations;
        $scope.profile_pic = [];
        $scope.data = {};
        $scope.data.UserInfo = {};
        $scope.savingData = false;

        $rootScope.userandNetworkPromise.then(function () {
            $scope.data.UserInfo.first_name = $rootScope.user_networks.UserInfo.first_name;
            $scope.data.UserInfo.last_name = $rootScope.user_networks.UserInfo.last_name;
            $scope.data.UserInfo.designation = $rootScope.user_networks.UserInfo.designation;
            $scope.data.UserInfo.location = $rootScope.user_networks.UserInfo.location;
        });

        $scope.OpenProfileSettings = function () {

            $scope.profileModal = $modal.open({
                templateUrl: '/scripts/View/users/editProfile.html?bust=' + Math.random().toString(36).slice(2),
                controller: 'ModalInstanceCtrl',
                scope: $scope,
                animation: true
            });
        };

        $scope.getLocation = function (val) {
            delete $http.defaults.headers.common['X-Requested-With'];

            return $http.get('https://maps.googleapis.com/maps/api/geocode/json?address=' + val + '&sensor=false')
                    .then(function (response) {
                        return response.data.results.map(function (item) {
                            return item.formatted_address;
                        });
                    });
//            $http.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
        };

        $scope.otherInfoUpdate = function (data) {
            var updateInfoPromise;
            updateInfoPromise = usersService.updateProfileInfo(data);
            updateInfoPromise.success(function () {
                $scope.profileModal.close();
                $scope.savingData = false;
                $rootScope.user_networks.UserInfo.name = data.UserInfo.name;
            });
        };

        $scope.updateUserInfo = function (data, photo) {
            $scope.savingData = true;
            if ((!photo.name) && (data)) {
                $scope.otherInfoUpdate(data);
                return;
            }
            if (!data) {
                data = {};
            }
            if (!data.UserInfo) {
                data.UserInfo = {};
            }
            data.UserInfo.photo = photo;
            if (photo.name) {
                Upload.upload({
                    method: 'POST',
                    url: '/users/edit',
                    file: photo,
                    data: data,
                    sendFieldsAs: 'form'
                }).progress(function (evt) {
                }).success(function (data) {
                    $scope.profileModal.close();
                    $scope.savingData = false;
                    $rootScope.user_networks.UserInfo.vga = data.UserInfo.vga;
                    $rootScope.user_networks.UserInfo.name = data.UserInfo.name;
                    trackEvents.trackUserActivity('User-Profile-Updated');
                });
            }
        };
    }]);

app.factory('dashboardService', ['$rootScope', '$http', '$httpParamSerializerJQLike', function ($rootScope, $http, $httpParamSerializerJQLike) {
        var factory = {};

        factory.getUsersofNetwork = function () {
            var userPromise;
            userPromise = $http({
                method: 'GET',
                url: '/datas/getAllNetworkUser'
            });
            return userPromise;
        };

        factory.extractEmails = function (emails) {
            var data = {};
            data.email = {};
            angular.forEach(emails, function (value, key) {
                data.email[key] = value.email;
            });
            return data;
        };

        factory.addToNetwork = function (emails) {
            var data = {};
            data = factory.extractEmails(emails);
            data.User = {};
            data.User.return_to = 'invite_network';
            var url = '/users/admin_add';
            var xsrf = $httpParamSerializerJQLike({data: data});
            var addToNetworkPromise = $http({
                method: 'POST',
                url: url,
                data: xsrf
            });
            return addToNetworkPromise;
        };

        factory.deleteNetwork = function (network_id) {
            var data = {};
            data.network_id = network_id;
            var url = '/networks/delete';
            var xsrf = $httpParamSerializerJQLike({data: data});
            var addToNetworkPromise = $http({
                method: 'POST',
                url: url,
                data: xsrf
            });
            return addToNetworkPromise;
        };

        factory.leaveNetwork = function (user_id) {
            var data = {};
            data.user_id = user_id;
            var url = '/networks/leave_network';
            var xsrf = $httpParamSerializerJQLike({data: data});
            var leaveNetworkPromise = $http({
                method: 'POST',
                url: url,
                data: xsrf
            });
            return leaveNetworkPromise;
        };

        factory.removeUserNetwork = function (user_id) {
            var data = {};
            data.user_id = user_id;
            var url = '/networks/remove';
            var xsrf = $httpParamSerializerJQLike({data: data});
            var removeUserPromise = $http({
                method: 'POST',
                url: url,
                data: xsrf
            });
            return removeUserPromise;
        };

        return factory;
    }]);

app.controller('dashboardController', ['$scope', '$modal', 'dashboardService', '$rootScope', 'trackEvents', function ($scope, $modal, dashboardService, $rootScope, trackEvents) {
        $scope.displayContent = 'people';
        $scope.invitenetworkModal;
        $scope.loadingNetworkUsers = true;
        $scope.deletetworkModal;
        $scope.removeUserModal;
        $scope.removeUserData = {};
        $scope.leaveUserData = {};
        $scope.leaveNetworkModal;

        $scope.changeselection = function (selection) {
            $scope.displayContent = selection;
        };

        $scope.getUsersofNetwork = function () {
            var userPromise = dashboardService.getUsersofNetwork();
            userPromise.success(function (data, header) {
                $scope.usersofNetwork = data.data.users;
                $scope.loadingNetworkUsers = false;
            }).error(function (data, header) {
                notify(data.message, header);
            });
        };

        $scope.addToNetwork = function (emails) {
            var addToNetworkPromise = dashboardService.addToNetwork(emails);
            addToNetworkPromise.success(function (data) {
                $scope.addpeopleNetwork(data.data.info);
                $scope.invitenetworkModal.close();
                trackEvents.trackWorkspaceActivity('Workspace-Invite-People-Dashboard');
            });
        };

        $scope.inviteToNetwork = function () {

            $scope.invitenetworkModal = $modal.open({
                templateUrl: '/scripts/View/Network/inviteToNetwork_1.html?bust=' + Math.random().toString(36).slice(2),
                scope: $scope,
                controller: 'ModalInstanceCtrl'
            });
        };

        $scope.deleteNetworkModal = function () {
            $scope.deletetworkModal = $modal.open({
                templateUrl: '/scripts/View/Network/deleteWorkspace.html?bust=' + Math.random().toString(36).slice(2),
                scope: $scope,
                controller: 'ModalInstanceCtrl'
            });
        };

        $scope.removeUserModalOpen = function (user, index) {
            $scope.removeUserData.user = user;
            $scope.removeUserData.index = index;

            $scope.removeUserModal = $modal.open({
                templateUrl: '/scripts/View/Network/removeUser.html?bust=' + Math.random().toString(36).slice(2),
                scope: $scope,
                controller: 'ModalInstanceCtrl'
            });
        };

        $scope.addpeopleNetwork = function (data) {
            for (var i in data) {
                $scope.usersofNetwork[$scope.usersofNetwork.length] = {};
                $scope.usersofNetwork[$scope.usersofNetwork.length - 1].UserInfo = data[i];
            }
        };

        $scope.deleteNetwork = function (network_id) {
            var networkdeletePromise;
            networkdeletePromise = dashboardService.deleteNetwork(network_id);
            networkdeletePromise.success(function (data) {
                trackEvents.trackWorkspaceActivity('Workspace-Delete');
                window.location.href = '/networks/index/1';
            });
        };

        $scope.leaveNetwork = function () {
            var leaveNetworkPromise;
            leaveNetworkPromise = dashboardService.leaveNetwork($rootScope.loggeduser);
            leaveNetworkPromise.success(function () {
                $scope.leaveUserData = {};
                $scope.leaveNetworkModal.close();
                trackEvents.trackWorkspaceActivity('Workspace-Leave');
                window.location.href = '/networks/index/1';
            });
        };

        $scope.leaveNetworkModalOpen = function () {

            $scope.leaveNetworkModal = $modal.open({
                templateUrl: '/scripts/View/Network/leaveWorkspace.html?bust=' + Math.random().toString(36).slice(2),
                scope: $scope,
                controller: 'ModalInstanceCtrl'
            });
        };

        $scope.removeUserNetwork = function (user_id, index) {
            var removeUserPromise;
            removeUserPromise = dashboardService.removeUserNetwork(user_id);
            removeUserPromise.success(function () {
                $scope.removeUserData = {};
                $scope.removeUserModal.close();
                $scope.usersofNetwork.splice(index, 1);
            });
        };

        $scope.getUsersofNetwork();
    }]);



app.factory('searchService', function ($http, commentService ,feedService) {

    var urlSeperator = '/';
    var controller = 'feeds';
    var actions = {view: 'view', add: 'add', edit: 'edit', del: 'delete' , serach: 'hashtag'};
    var factory = {};

    factory.feedstoload = 10;
    factory.feeds;

    factory.get = function (page) {
        var location = window.location.pathname.toString().split("/");
        var query = location[location.length-1];
        var url = urlSeperator + controller + urlSeperator + actions.serach + urlSeperator + query;
        if (angular.isNumber(page))
            var url = url + urlSeperator + query + page;
        return $http.post(url);
    };
   
    factory.delete = feedService.delete;

    factory.addComment = feedService.addComment;

    factory.deleteComment = feedService.deleteComment;

    factory.getComment = feedService.getComment;

    return factory;

});


app.controller('SearchController', ['$rootScope', '$scope', 'feedService', 'createDialog','searchService', function ($rootScope, scope, feedService, createDialog ,searchService) {


        scope.message = '';
        scope.searchService = searchService;
        scope.feedService = feedService;
        scope.feedsLoadedPage = 1;
        scope.feedDelete = function (id) {
            searchService.delete(id).success(function (data, header) {
                //Remove the feed from the list
                searchService.feeds.some(function (feed, index) {
                    if (feed.Feed.id == id)
                    {
                        searchService.feeds.splice(index, 1);
                        return true;
                    }
                });
            }).error(function (data, header) {
                notify(data.message, header);
            });
        };

        scope.feedDeleteModal = function (id) {
            createDialog(null, {
                id: 'deleteFeed',
                title: 'Delete Post',
                deleteleave: 0,
                string: 'Are you sure you want to Delete Post ?',
                backdrop: true,
                success: {label: 'Yes, Delete', fn: function () {
                        scope.feedDelete(id);
                    }},
                cancel: {label: 'Cancel', fn: function () {

                    }}
            });
        };
          
        scope.feedGet = function(page){
            searchService.get(page).success(function (data, header) {
                for (var i in data.data) {
                searchService.feeds.push(data.data[i]);
            }
            scope.feedsLoadedPage++;
        }).error(function (data, header) {
            notify(data.message, header);
        });
       };

        scope.boxsearch = function (hashsearch){
            var hash = twttr.txt.extractHashtags(hashsearch).toString();
            if(hash) {
                feedService.boxsearch(hash);
            }
            else{
                feedService.boxsearch(hashsearch);
            }
        }; 
        
        //Get all feeeds on page
        searchService.get().success(function (data, header) {
            
            searchService.feeds = data.data;
            scope.loadMoreTag = data.loadMoreTag;
        }).error(function (data, header) {
            notify(data.message, header);
        });

    }]);


app.factory('feedService',["$http","commentService","$httpParamSerializerJQLike",function ($http, commentService,$httpParamSerializerJQLike) {

    var urlSeperator = '/';
    var controller = 'feeds';
    var actions = {view: 'view', add: 'add', edit: 'edit', del: 'delete' , feedcount:'unreadFeedCount' , newfeed:'latestFeed' , serach: 'hashtag' , like: 'like_feed' , unlike: 'dislike_feed' , listLike: 'listLikes'};
    var factory = {};

    factory.feedstoload = 10;
    factory.feeds;

    factory.get = function (page) {
        var url = urlSeperator + controller + urlSeperator + actions.view;
        
        if (angular.isNumber(page))
            var url = url + urlSeperator + page;

        return $http.get(url);
    };
    factory.add = function (message, preview_link) {
        if (!message)
            return;
        var hashtags = twttr.txt.extractHashtags(message).toString();
        var url = urlSeperator + controller + urlSeperator + actions.add;
        var data = {message: message, preview_link: preview_link, hashtags: hashtags}; 
        var xsrf = $httpParamSerializerJQLike({data: data});
        return $http.post(url, xsrf);
    };

    factory.getUnseenFeedCount = function(){
        var url = urlSeperator + controller + urlSeperator + actions.feedcount;
        return $http.get(url);
    };
    
    factory.newFeedGet = function(){
        var url = urlSeperator + controller + urlSeperator + actions.newfeed;
        return $http.get(url);
    };
    
    factory.delete = function (id) {
        if (id) {
            var url = urlSeperator + controller + urlSeperator + actions.del + urlSeperator + id;
            return $http.post(url);
        }
        return;
    };
    
    factory.feedLike = function (id){
        if (id) {
            var url = urlSeperator + controller + urlSeperator + actions.like + urlSeperator + id;
            return $http.post(url);
        }
        return;
    };
    
    factory.feedUnlike = function (id){
        if (id) {
            var url = urlSeperator + controller + urlSeperator + actions.unlike + urlSeperator + id;
            return $http.post(url);
        }
        return;
    };
    
    factory.likesList = function (id , page){
        if (id) {
            var url = urlSeperator + controller + urlSeperator + actions.listLike + urlSeperator + id;
        }
        if(angular.isNumber(page)){
            url = url + urlSeperator + page;
        }
        return $http.post(url);
    };

    factory.addComment = function (id, body) {
        if (body && body.trim()) {
            commentService.add('feeds', id, body)
                    .success(function (data, header) {
                        factory.feeds.some(function (feed, index) {
                            if (feed.Feed.id == id) {
                                if(!(factory.feeds[index]['Comment'])){
                                    factory.feeds[index]['Comment'] = data.data;
                                    factory.feeds[index]['Feed']['comment_count']++;
                                }else{
                                 factory.feeds[index]['Comment'] = (factory.feeds[index]['Comment']).concat(data.data);
                                 factory.feeds[index]['Feed']['comment_count']++;   
                                }
                                return true;
                            }
                        });
                    })
                    .error(function (data, header) {
                        notify(data.message, header);
                    });
        }
    };

    factory.deleteComment = function (id, commentId) {
        if (commentId) {
            commentService.delete(commentId)
                    .success(function (data, header) {
                        factory.feeds.some(function (feed, index) {
                            if (feed.Feed.id == id) {
                                (factory.feeds[index]['Comment']).some(function (comment, ind) {
                                    if (comment.id == commentId) {
                                        factory.feeds[index]['Comment'].splice(ind, 1);
                                        factory.feeds[index]['Feed']['comment_count']--;
                                        return true;
                                    }
                                });
                                return true;
                            }
                        });
                        notify(data.message, header);
                    })
                    .error(function (data, header) {
                        notify(data.message, header);
                    });
        }
    };

    factory.getComment = function (id, lastId) {
        if (angular.isUndefined(lastId))
            var lastId = null;
        var id = parseInt(id);
        var lastId = parseInt(lastId);

        //Check loadmore variable exists and is false
        var index = factory.feeds.map(function (x) {
            return x.Feed.id;
        }).indexOf(id);
        if (angular.isUndefined(factory.feeds[index]['moreComments']) || factory.feeds[index]['moreComments'] !== false) {
            commentService.get('feeds', id, lastId)
                    .success(function (data, header) {
                        factory.feeds[index]['Comment'] = (factory.feeds[index]['Comment']).concat(data.data.comments);
                        factory.feeds[index]['moreComments'] = data.data['loadMore'];
                    })
                    .error(function (data, header) {
                        notify(data.message, header);
                    });
        }
    };

    factory.boxsearch = function(hash){
        console.log(hash);
            var url = urlSeperator + controller + urlSeperator + actions.serach + urlSeperator + hash;
            window.open(url,"_blank");
    };

    return factory;

}]);

app.controller('feedController', ['$rootScope', '$scope', 'feedService', 'createDialog', '$modal' , function ($rootScope, scope, feedService, createDialog,$modal) {

        scope.message = '';
        scope.feedService = feedService;

        scope.feedsLoadedPage=1;
        //scope.loadmoreFeedCount;

        scope.currentfeed;
        scope.feedsLoadedPage = 1;
        scope.likesLoadedPage=1;
        scope.likeslistModal;
        
        scope.feedAdd = function (message, preview_link) {
            
                scope.newFeedGet();
                scope.removePreview();
            
            feedService.add(message, preview_link).success(function (data, header) {
                scope.message = '';
                feedService.feeds.unshift(data.data);
            }).error(function (data, header) {
                notify(data.message, header);
            });
        };
            
        scope.feedDelete = function (id) {
            feedService.delete(id).success(function (data, header) {
                //Remove the feed from the list
                feedService.feeds.some(function (feed, index) {
                    if (feed.Feed.id === id)
                    {
                        feedService.feeds.splice(index, 1);
                        return true;
                    }
                });
            }).error(function (data, header) {
                notify(data.message, header);
            });
        };
            
        scope.boxsearch = function (hashsearch){
            var hash = twttr.txt.extractHashtags(hashsearch).toString();
            if(hash) {
                feedService.boxsearch(hash);
            }
            else{
                feedService.boxsearch(hashsearch);
            }
        };    

        scope.feedLike = function (id){
            feedService.feedLike(id).success(function (data, header){
                
                feedService.feeds.some(function (feed, index) {
                    if (feed.Feed.id === id)
                    {
                        feed.Feed.is_liked_by_current_user=true;
                        feed.Feed.like_count++;
                        return true;
                    }
                });
            }).error(function (data, header) {
                notify(data.message, header);
            });
        };
        
        scope.feedUnlike = function (id){
            feedService.feedUnlike(id).success(function (data, header){ 
            
                feedService.feeds.some(function (feed, index) {
                    if (feed.Feed.id === id)
                    {
                        feed.Feed.is_liked_by_current_user=false;
                        feed.Feed.like_count--;
                        return true;
                    }
                });
            }).error(function (data, header) {
                notify(data.message, header);
            });
        };
        
        scope.likesList = function(id , page){
            feedService.likesList(id , page).success(function (data, header){
              scope.currentfeed = data.likes;
              scope.loadMorelikes = data.loadMore;
              scope.openlistLikeModal();
            }).error(function (data, header) {
                notify(data.message, header);
            });
        };
        
        scope.feedDeleteModal = function (id) {
            createDialog(null, {
                id: 'deleteFeed',
                title: 'Delete Post',
                deleteleave: 0,
                string: 'Are you sure you want to Delete Post ?',
                backdrop: true,
                success: {label: 'Yes, Delete', fn: function () {
                        scope.feedDelete(id);
                    }},
                cancel: {label: 'Cancel', fn: function () {

                    }}
            });
        };


        scope.feedGet = function(page){
            feedService.get(page).success(function (data, header) {
            for (var i in data.data) {
                feedService.feeds.push(data.data[i]);
            }
            scope.feedsLoadedPage++;
            scope.loadmoreFeed = data.loadmoreFeed;
        }).error(function (data, header) {
            notify(data.message, header);
        });
        };
        
        scope.newFeedGet = function(){
         feedService.newFeedGet().success(function (data, header) {
             for (var i in data.data) {
                feedService.feeds.splice(i, 0, data.data[i]);
            }
            $rootScope.loadmoreFeedCount = 0;
            }).error(function (data, header) {
                notify(data.message, header);
            });
        };
        
        scope.deleteCommentModal = function (id, commentId) {
        createDialog(null, {
            id: 'feedcommentdelete',
            title: 'Delete Comment',
            deleteleave: 0,
            string: 'Are you sure you want to delete comment?',
            backdrop: true,
            success: {label: 'Yes, Delete', fn: function () {
                    feedService.deleteComment(id, commentId);
                }},
            cancel: {label: 'Cancel', fn: function () {
                }}
        });
        };
        
        scope.openlistLikeModal = function () {
            scope.likeslistModal = $modal.open({
                templateUrl: '/scripts/View/Feed/likeslist.html?bust=' + Math.random().toString(36).slice(2),
                scope: scope,
                controller: 'ModalInstanceCtrl'
            });
        };
        
        feedService.get().success(function (data, header) {
            feedService.feeds = data.data;
            scope.loadmoreFeed = data.loadmoreFeed;
            $rootScope.loadmoreFeedCount = 0;
        }).error(function (data, header) {
            notify(data.message, header);
        });
    }]);


app.directive('feedAdd',['$compile','$timeout',function ($compile, $timeout) {
    return{
        restrict: 'E',
        replace: true,
        templateUrl: '/scripts/View/Feed/add.html',
        link: function (scope, elements) {
            scope.preview_url = '';
            scope.body = '';

            var previewWithUrl = '<div id="preview" class="g-background-white link-preview-area">\n\
            <span class="pull-right close text-extra-bold" ng-click="removePreview()">&times;</span>\n\
            <a class="embedly-card" href="{{preview_url}}"  data-card-chrome="0" data-card-analytics="0" ></a>\n\
                        </div>';
            var preview = '<div id="preview"></div>';

            scope.checkUrl = function (body) {
                if (scope.body !== body) {
                    scope.body = body;
                }
                $timeout(function () {
                    if (scope.preview_url) {
                        return;
                    }
                    if (scope.body !== body) {
                        return;
                    }
                    //With the help of twitter text library
                    var preview_url = twttr.txt.extractUrls(body)[0];
                    if(preview_url){
                        scope.preview_url = preview_url;
                            var embedly = angular.element(previewWithUrl);
                            var e = $compile(embedly)(scope);
                            var division = elements[0].querySelector('#preview');
                            var $division = angular.element(division);
                            $division.replaceWith(e);
                            $compile($division)(scope);
                            return;
                    }
                }, 1500);
            };

            scope.removePreview = function () {
                scope.preview_url = '';
                var embedly = angular.element(preview);
                var e = $compile(embedly)(scope);
                var division = elements[0].querySelector('#preview');
                var $division = angular.element(division);
                $division.replaceWith(e);
            };

        }
    };
}]);

app.directive('myEmbedlyTest', function () {
    return {
        controller: function ($scope) {
            $scope.key = 'db66909faab54ad4b303cd66bd31dacc';
            $scope.query = {
                maxwidth: 300,
                maxheight: 300
            };
        }
    };
});


app.directive('gProfileImg', ['$compile', function ($compile) {
        var template;
        return{
            restrict: 'E',
            scope: {
                path: '=',
                name: '=',
                width: '@',
                height: '@',
                myclass: '@',
                type: '@'
            },
            link: function (scope, element, attrs) {
               
               var intitials_generation = function(name) {
                    var init = name.split(" ", 2);
                    if (init.length == 1)
                        return init[0].substring(0, 1).toUpperCase();
                    return init[0].charAt(0).toUpperCase();
//                    return init[0].charAt(0).toUpperCase() + init[1].charAt(0).toUpperCase();
                };
                
                switch (attrs.type) {
                        case 'user' :
                            if (scope.path.indexOf('/default') > -1 || scope.paths)
                            {
                                template = '<p width="' + attrs.width + '" height="' + attrs.height + '" class="g-img center-block ' + attrs.myclass + '">' + intitials_generation(scope.name) + '</p>';
                            } else {
                                template = '<img src="' + scope.path + '" width="' + attrs.width + '" height="' + attrs.height + '" class=" g-img ' + attrs.myclass + '" alt="' + scope.name + '"/>';
                            }
                            break;
                        case 'group' :
                            template = '<p width="' + attrs.width + '" height="' + attrs.height + '" class="g-img center-block ' + attrs.myclass + '">' + intitials_generation(scope.name) + '</p>';
                            break;
                        case 'network' :
                            template = '<p width="' + attrs.width + '" height="' + attrs.height + '" class="g-img center-block ' + attrs.myclass + '">' + intitials_generation(scope.name) + '</p>';
                            break;
                        default :
                            template = '<p width="' + attrs.width + '" height="' + attrs.height + '" class="g-img center-block ' + attrs.myclass + '">' + intitials_generation(scope.name) + '</p>';
                    }
                    element[0].innerHTML = " ";
                    var e = angular.element(template);
                    $compile(e.contents())(scope);
                    element.append(e);
                    template = element.html();
            },
            template : template
        };
    }]);

app.directive('gTask', ['$compile', function ($compile) {
        return{
            restrict: 'E',
            scope: {
                task:'=',
                name: '@',
                width: '@',
                height: '@',
                myclass: '@',
                type: '@'
            },
            link: function (scope, element, attrs) {

            }
        };
    }]);

app.filter('networkAlphabetOrder', function () {
    return function (items) {
        if (items == null)
            return false;
        var sortable = [];
        angular.forEach(items, function (value, key) {
            sortable.push([value, value.name.toString().toLowerCase()])
        });
        sortable.sort(function (a, b) {
            return a[1].localeCompare(b[1]);
        })
        var items = [];
        angular.forEach(sortable, function (value, key) {
            items.push(value[0])
        });
        return items;
    }
});

//app.filter('',function(){
//    
//});

app.filter('bytesConvert', function () {
    return function (num) {
        num = parseInt(num);
        if (num < 1024)
            return num + ' bytes';
        else if (num < 1048576)
            return (num / 1024).toFixed(1) + ' Kb';
        else if (num < 1073741824)
            return (num / 1048576).toFixed(2) + ' Mb';
        else
            return (num / 1073741824).toFixed(2) + ' Gb';
    }
});

app.factory('createDialog', ["$document", "$compile", "$rootScope", "$controller", "$timeout", function ($document, $compile, $rootScope, $controller, $timeout) {
        var defaults = {
            id: null,
            template: null,
            title: 'Default Title',
            backdrop: true,
            user_id: null,
            success: {label: 'OK', fn: null},
            cancel: {label: 'Close', fn: null},
            controller: null, //just like route controller declaration
            backdropClass: "modal-backdrop",
            backdropCancel: true,
            myModalClass: 'g-confirm',
            header: true,
            footer: true,
            templateUrl: null,
            modalClass: "modal",
            deleteleave: null /// 0 for delete and 1 for leave
        };
        /* Templates */
        var body = $document.find('body');
        return function Dialog(templateUrl/*optional*/, options, passedInLocals) {

            // Handle arguments if optional template isn't provided.
            if (angular.isObject(templateUrl)) {
                passedInLocals = options;
                options = templateUrl;
            } else {
                options.templateUrl = templateUrl;
            }

            options = angular.extend({}, defaults, options); //options defined in constructor

            var key;
            var idAttr = options.id ? ' id="' + options.id + '" ' : '';
            /* Default Templates */
            var defaultHeader = '<div class=" ' + options.myModalClass + '-header modal-header">\n\
            <button type="button" class="close" ng-click="$modalCancel()"></button>' + '<h4><span class="' + options.headerIcon + '"></span><span  id=>{{$title}}</span></h4></div>';
            var defaultBody = '<div class="' + options.myModalClass + '-body modal-body">' + options.string + '</div>';
            var defaultFooter = '<div class="' + options.myModalClass + '-footer modal-footer"><button class="btn deletebtn" ng-click="$modalSuccess()">{{$modalSuccessLabel}}</button>' + '<button class="btn" ng-click="$modalCancel()">{{$modalCancelLabel}}\n\</button></div>';
            /* Setting Header */
            /*    var headerTemplate = defaultHeader;
             if (options.header)
             {
             headerTemplate = '<div class="modal-footer deletemodalfooter"  ng-include="\'' + options.headerTemplate + '\'"></div>';
             }
             
             /* Setting Footer */
            /*  var footerTemplate = defaultFooter;
             if (options.footer)
             {
             footerTemplate = '<div class="modal-footer deletemodalfooter"  ng-include="\'' + options.footerTemplate + '\'"></div>';
             } */


            if (!options.footer) {
                defaultFooter = '';
            }


            /* Setting Modal Body */
            var modalBody = (function () {
                if (options.string) {
                    if (angular.isString(options.string)) {
                        // Simple string template
                        return defaultBody;
                    } else {
                        // jQuery/JQlite wrapped object
                        return '<div class="modal-body">' + options.template.html() + '</div>';
                    }
                } else {
                    // Template url
                    return '<div class=" ' + options.myModalClass + ' modal-body" ng-include="\'' + options.templateUrl + '\'"></div>'
                }
            })();
            //We don't have the scope we're gonna use yet, so just get a compile function for modal
            var modalEl = angular.element(
                    '<div class="' + options.modalClass + ' fade"' + idAttr + ' style="display: block;">' +
                    '  <div class=" ' + options.myModalClass + ' modal-dialog">' +
                    '    <div class="' + options.myModalClass + '-content modal-content">' +
                    defaultHeader +
                    modalBody +
                    defaultFooter +
                    '    </div>' +
                    '  </div>' +
                    '</div>');
            for (key in options.css) {
                modalEl.css(key, options.css[key]);
            }
            var divHTML = "<div ";
            if (options.backdropCancel) {
                divHTML += 'ng-click="$modalCancel()"';
            }
            divHTML += ">";
            var backdropEl = angular.element(divHTML);
            backdropEl.addClass(options.backdropClass);
            backdropEl.addClass('fade in');
            var handleEscPressed = function (event) {
                if (event.keyCode === 27) {
                    scope.$modalCancel();
                }
            };
            var closeFn = function () {
                body.unbind('keydown', handleEscPressed);
                modalEl.remove();
                if (options.backdrop) {
                    backdropEl.remove();
                }
            };
            body.bind('keydown', handleEscPressed);
            var ctrl, locals,
                    scope = options.scope || $rootScope.$new();
            scope.$title = options.title;
            scope.$modalClose = closeFn;
            scope.$modalCancel = function () {
                var callFn = options.cancel.fn || closeFn;
                callFn.call(this);
                scope.$modalClose();
            };
            scope.$modalSuccess = function () {
                var callFn = options.success.fn || closeFn;
                callFn.call(this);
                scope.$modalClose();
            };
            scope.$modalSuccessLabel = options.success.label;
            scope.$modalCancelLabel = options.cancel.label;
            if (options.controller) {
                locals = angular.extend({$scope: scope}, passedInLocals);
                ctrl = $controller(options.controller, locals);
                // Yes, ngControllerController is not a typo
                modalEl.contents().data('$ngControllerController', ctrl);
            }

            $compile(modalEl)(scope);
            $compile(backdropEl)(scope);
            body.append(modalEl);
            if (options.backdrop)
                body.append(backdropEl);
            $timeout(function () {
                modalEl.addClass('in');
            }, 200);
        };
    }]);

app.directive('ngEnter', function () {
    return function (scope, element, attrs) {
        element.bind("keydown keypress", function (event) {
            if (event.which === 13) {
//                scope.$apply(function() {
//                   scope.$eval(attrs.ngEnter);
//                });
                scope.$eval(attrs.ngEnter);
                event.preventDefault();
            }
        });
    };
});

app.directive('contenteditable', ['$sce', function($sce) {
        return {
            restrict: 'A',
            require: '?ngModel',
            link: function(scope, element, attrs, ngModel) {
                function read() {
                    var html = element.html();
                    if (attrs.stripBr && html === '<br>') {
                        html = '';
                    }
                    ngModel.$setViewValue(html);
                }

                if(!ngModel) return;

                ngModel.$render = function() {
                    if (ngModel.$viewValue !== element.html()) {
                        element.html($sce.getTrustedHtml(ngModel.$viewValue || ''));
                    }
                };

                element.on('blur keyup change', function() {
                    scope.$apply(read);
                });
                read();
            }
        };
    }]);

app.filter('tweetLinky',['$filter',
    function($filter) {
        return function(text, target) {
            if (!text) return text;

            var replacedText = $filter('linky')(text, target);
            var targetAttr = "";
            if (angular.isDefined(target)) {
                targetAttr = ' target="' + target + '"';
            }
            // replace #hashtags and send them to twitter
            var replacePattern1 = /(^|\s)#(\w*[a-zA-Z_]+\w*)/gim;
            replacedText = text.replace(replacePattern1, '$1<a href="https://twitter.com/search?q=%23$2"' + targetAttr + '>#$2</a>');
            // replace @mentions but keep them to our site
            var replacePattern2 = /(^|\s)\@(\w*[a-zA-Z_]+\w*)/gim;
            replacedText = replacedText.replace(replacePattern2, '$1<a href="https://twitter.com/$2"' + targetAttr + '>@$2</a>');
            return replacedText;
        };
    }
]);

app.service('trackEvents', function () {

    var isIntercom = false;

    //tracking events in intercom
    if (typeof Intercom !== 'undefined') {
        isIntercom = true;
    }

    this.trackNetworkAdd = function (custom_event, meta_data) {
        if (isIntercom) {
            Intercom('trackEvent', custom_event, meta_data);
        }

    };

    this.trackNetworkAddMember = function (custom_event, meta_data) {
        if (isIntercom) {
            Intercom('trackEvent', custom_event, meta_data);
        }
    };

    this.trackTaskAdd = function (custom_event, meta_data) {
        if (isIntercom) {
            Intercom('trackEvent', custom_event, meta_data);
        }
    };

    this.trackTaskAddComment = function (custom_event, meta_data) {
        if (isIntercom) {
            Intercom('trackEvent', custom_event, meta_data);
        }
    };
    this.trackTaskMarkComplete = function (custom_event, meta_data) {
        if (isIntercom) {
            Intercom('trackEvent', custom_event, meta_data);
        }
    };

    this.trackTaskDelete = function (custom_event, meta_data) {
        if (isIntercom) {
            Intercom('trackEvent', custom_event, meta_data);
        }
    };

    this.trackTaskSearch = function (custom_event, meta_data) {
        if (isIntercom) {
            Intercom('trackEvent', custom_event, meta_data);
        }
    };

    this.trackTaskActivity = function (custom_event, meta_data) {
        if (isIntercom) {
            Intercom('trackEvent', custom_event, meta_data);
        }
    };

    this.trackTeamAdd = function (custom_event, meta_data) {
        if (isIntercom) {
            Intercom('trackEvent', custom_event, meta_data);
        }
    };

    this.trackTeamMemberAdd = function (custom_event, meta_data) {
        if (isIntercom) {
            Intercom('trackEvent', custom_event, meta_data);
        }
    };

    this.trackFileUpload = function (custom_event, meta_data) {
        if (isIntercom) {
            Intercom('trackEvent', custom_event, meta_data);
        }
    };

    this.trackFileDownload = function (custom_event, meta_data) {
        if (isIntercom) {
            Intercom('trackEvent', custom_event, meta_data);
        }
    };

    this.trackFileRename = function (custom_event, meta_data) {
        if (isIntercom) {
            Intercom('trackEvent', custom_event, meta_data);
        }
    };
    this.trackFileDelete = function (custom_event, meta_data) {
        if (isIntercom) {
            Intercom('trackEvent', custom_event, meta_data);
        }
    };
    this.trackFileAddComment = function (custom_event, meta_data) {
        if (isIntercom) {
            Intercom('trackEvent', custom_event, meta_data);
        }
    };
    this.trackFileShare = function (custom_event, meta_data) {
        if (isIntercom) {
            Intercom('trackEvent', custom_event, meta_data);
        }
    };
    this.trackFilePreview = function (custom_event, meta_data) {
        if (isIntercom) {
            Intercom('trackEvent', custom_event, meta_data);
        }
    };
    this.trackTeamActivity = function (custom_event, meta_data) {
        if (isIntercom) {
            Intercom('trackEvent', custom_event, meta_data);
        }
    };
    this.trackWorkspaceActivity = function (custom_event, meta_data) {
        if (isIntercom) {
            Intercom('trackEvent', custom_event, meta_data);
        }
    };
    this.trackSearchUserGroup = function (custom_event, meta_data) {
        if (isIntercom) {
            Intercom('trackEvent', custom_event, meta_data);
        }
    };
    this.trackAppearIn = function (custom_event, meta_data) {
        if (isIntercom) {
            Intercom('trackEvent', custom_event, meta_data);
        }
    };
    this.trackUserActivity = function (custom_event, meta_data) {
        if (isIntercom) {
            Intercom('trackEvent', custom_event, meta_data);
        }
    };

});

function notify(data) {

}

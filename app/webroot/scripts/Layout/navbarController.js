/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
angular.module('navBar',[])
        .controller('navBarController',['$rootScope','$scope','runService','$modal','$http','$httpParamSerializerJQLike',function($rootScope,$scope,runService,$modal,$http,$httpParamSerializerJQLike){
        
          $scope.collapseusers = true;
          $scope.collapsegroups =true;
          $rootScope.selectedUser;
          $rootScope.selectedGroup;
          $scope.userPage=2;
          $scope.groupPage=2;
          $scope.loadMoreGroup=false;
          $scope.loadMoreUser=false;
          $rootScope.taskFilterOn=false;
//          runService=runService;
//          deskService = deskService;
//          
          $scope.toogleSelection = function(type){
              if(type == 'user'){
                  $scope.collapseusers = !($scope.collapseusers);
              }else if(type == 'group'){
                  $scope.collapsegroups = !($scope.collapsegroups);
              }
          };
          
          
          
          //make a method to search
          
          $scope.selectUser = function(user){
              $rootScope.taskFilterOn=true;
              $rootScope.selectedUser=user;
              $rootScope.isSelectedUser=true;
              $rootScope.$broadcast('user_Selected', user);
           };
          
          $scope.selectGroup = function(group){
              $rootScope.taskFilterOn=true;
              $rootScope.selectedGroup=group;
              $rootScope.isSelectedUser=false;
              $rootScope.$broadcast('group_Selected', group);
          };
          
          $rootScope.clearFilter = function(option){ //option for desk and files page
              $rootScope.taskFilterOn=false;
              $rootScope.selectedUser=null;
              $rootScope.selectedGroup=null;
              $rootScope.$broadcast('clear_Filter','');
          };
          
          $scope.loadMoreUser = function(){
             $scope.userPromise = runService.getUsersData($scope.userPage,$scope.LoadMoreCountUser);
                $scope.userPromise.success(function (data, header) {
                $scope.newusers=data.data['users'];
                $scope.i=0;
                for($scope.i;$scope.i<$scope.newusers.length;$scope.i++){
                $rootScope.users.push($scope.newusers[$scope.i]);    
                }
                $rootScope.LoadMoreCountUser = data.LoadMoreCount;
                $rootScope.loadMoreUser = data.LoadMore;
                $scope.userPage++;
            }).error(function (data, header) {
                notify(data.message, header);
            });
          };
          
          $scope.loadMoreGroup = function(){
             $scope.groupPromise = runService.getGroupsData($scope.groupPage,$scope.LoadMoreCountGroup);
                $scope.groupPromise.success(function (data, header) {
                $scope.newgroups=data.data['groups'];
                $scope.i=0;
                for($scope.i;i<$scope.newgroups.length;$scope.i++){
                $rootScope.groups.push($scope.newgroups[i]);    
                }
                $rootScope.LoadMoreCountGroup = data.LoadMoreCount;
                $rootScope.loadMoreGroup = data.LoadMore;
                $scope.groupPage++;
            }).error(function (data, header) {
                notify(data.message, header);
            });   
          };
          
         $scope.searchUserAndGroup = function(query){
             $scope.UserAndGroupTag;
             var url = '/datas/search/'+query;
             var taskPromise = $http({
                            method: 'GET',
                            url: url
            }).success(function(data){
                $scope.UserAndGroupTag = data.data;  
            });            
          };
         
         $scope.selectMatch = function(model,data){
             if(model.Tags.type == 'user'){
              $scope.selectUser(model.Tags);   
             }else if(model.Tags.type == 'group'){
              $scope.selectGroup(model.Tags);  
             }
             $scope.customSelected=null;
         };
         
//         $scope.typeaheadOpts = {
//            inputFormatter: myInputFormatterFunction,
//            loading: myLoadingBoolean,
//            minLength: 3,
//            onSelect: myOnSelectFunction, // this will be run in addition to directive internals
//            templateUrl: '/path/to/my/template.html',
//            waitMs: 500,
//            allowsEditable: true
//        };
             
         $scope.extractEmails = function(emails){
             var data ={};
             data.email={};
             angular.forEach(emails,function(value,key){
                 data.email[key] = value.email;
             });
             return data;
         };
         
         $scope.addToNetwork = function(emails){
             var data = {};
             data = $scope.extractEmails(emails);
             data.User={};
             data.User.return_to = 'invite_network';
             var url = '/users/admin_add';
             var xsrf = $httpParamSerializerJQLike({data :data});
             var addToNetworkPromise = $http({
                            method: 'POST',
                            url: url,
                            data: xsrf
            });
//            addToNetworkPromise.success(function(data){
//                //write logic to add them  
//                console.log(data);
//            });
         };
         
         $scope.addTeam = function () {

        var modalInstance = $modal.open({
            templateUrl: '/scripts/View/teams/addTeam.html?bust=' + Math.random().toString(36).slice(2),
            controller: 'GroupController',
            scope: $scope
        });
    };
    
        $scope.inviteToNetwork = function () {
        
        var modalInstance = $modal.open({
            templateUrl: '/scripts/View/networks/inviteToNetwork.html',
            controller: 'navBarController',
            scope: $scope
        });
    };
    
        $scope.openAppearModal = function () {

        var modalInstance = $modal.open({
            templateUrl: '/scripts/View/chat/appearChat.html',
            controller: 'navBarController',
            windowClass: 'appearChatWindow',
            size:'lg',
            scope: $scope
        });

    };
    
    $scope.AppearChatLink = "";
    
    var AppearIn = window.AppearIn || require('appearin-sdk');
    var appearin = new AppearIn();

    $scope.launchAppearChat = function () {

        // Returns true if browser can use WebRTC, false otherwise
        var isWebRtcCompatible = appearin.isWebRtcCompatible();

        if (!isWebRtcCompatible) {
            alert("Please upgrade your browser. WebRTC not supported.");
            return;
        }

        // Promise-based workflow (recommended)
        appearin.getRandomRoomName().then(function (roomName) {
            console.log(roomName);
            $scope.AppearChatLink = "https://appear.in/" + roomName;
            // do something with the roomName
            var iframe = document.getElementById("appear-iframe");
            appearin.addRoomToIframe(iframe, roomName);

        });
        $scope.openAppearModal();

    };
    
}]);


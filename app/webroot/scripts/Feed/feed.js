'use strict';
/**
 * Feed module
 */
var app = angular.module('feed', ['ngEmbedApp']);

app.factory('feedService', function ($http, commentService) {

    var urlSeperator = '/';
    var controller = 'feeds';
    var actions = {view: 'view', add: 'add', edit: 'edit', del: 'delete'};
    var factory = {};

    factory.feedstoload = 10;
    factory.feeds;

    factory.get = function (page) {

        var url = urlSeperator + controller + urlSeperator + actions.view;

        if (angular.isNumber(page))
            var url = url + urlSeperator + page;

        return $http.get(url);
    };
    factory.add = function (message, preview_link) {
        if (!message)
            return;
        var hashtags = twttr.txt.extractHashtags(message);
        var url = urlSeperator + controller + urlSeperator + actions.add;
        var data = $.param({message: message, preview_link: preview_link , tags : hashtags});
        return $http.post(url, data);
    }

    factory.delete = function (id) {
        if (id) {
            var url = urlSeperator + controller + urlSeperator + actions.del + urlSeperator + id;
            return $http.post(url);
        }
        return;
    }

    factory.addComment = function (id, body) {
        if (body && body.trim()) {
            commentService.add('feeds', id, body)
                    .success(function (data, header) {
                        factory.feeds.some(function (feed, index) {
                            if (feed.Feed.id == id) {
                                factory.feeds[index]['Comment'] = (factory.feeds[index]['Comment']).concat(data.data);
                                return true;
                            }
                        });
                    })
                    .error(function (data, header) {
                        notify(data.message, header);
                    })
        }
    }

    factory.deleteCommentModal = function (id, commentId) {
        createDialog(null, {
            id: 'feedcommentdelete',
            title: 'Delete Comment',
            deleteleave: 0,
            string: 'Are you sure you want to delete comment?',
            backdrop: true,
            success: {label: 'Yes, Delete', fn: function () {
                    fact.deleteComment(id, commentId);
                }},
            cancel: {label: 'Cancel', fn: function () {
                }}
        });
    }

    factory.deleteComment = function (id, commentId) {
        if (commentId) {
            commentService.delete(commentId)
                    .success(function (data, header) {
                        factory.feeds.some(function (feed, index) {
                            if (feed.Feed.id == id) {
                                (factory.feeds[index]['Comment']).some(function (comment, ind) {
                                    if (comment.id == commentId) {
                                        factory.feeds[index]['Comment'].splice(ind, 1);
                                        factory.feeds[index]['Feed']['comment_count']--;
                                        return true;
                                    }
                                });
                                return true;
                            }
                        });
                        notify(data.message, header);
                    })
                    .error(function (data, header) {
                        notify(data.message, header);
                    })
        }
    }

    factory.getComment = function (id, lastId) {
        if (angular.isUndefined(lastId))
            var lastId = null;
        var id=parseInt(id);var lastId=parseInt(lastId);
        
        //Check loadmore variable exists and is false
        var index = factory.feeds.map(function (x) {
            return x.Feed.id;
        }).indexOf(id);
        if (angular.isUndefined(factory.feeds[index]['moreComments']) || factory.feeds[index]['moreComments'] !== false) {
            commentService.get('feeds', id, lastId)
                    .success(function (data, header) {
                        factory.feeds[index]['Comment'] = (factory.feeds[index]['Comment']).concat(data.data.comments);
                        factory.feeds[index]['moreComments'] = data.data['loadMore'];
                    })
                    .error(function (data, header) {
                        notify(data.message, header);
                    })
        }
    }


    return factory;

});

app.controller('feedController', ['$rootScope', '$scope', 'feedService', function ($rootScope, scope, feedService) {

        scope.message = '';
        scope.feedService = feedService;

        scope.feedAdd = function (message, preview_link) {
            feedService.add(message, preview_link).success(function (data, header) {
                feedService.feeds.unshift(data.data);
                scope.message = '';
                scope.removePreview();
            }).error(function (data, header) {
                notify(data.message, header);
            });
        };

        scope.feedDelete = function (id) {
            feedService.delete(id).success(function (data, header) {
                //Remove the feed from the list
                feedService.feeds.some(function (feed, index) {
                    if (feed.Feed.id == id)
                    {
                        feedService.feeds.splice(index, 1);
                        return true;
                    }
                });
            }).error(function (data, header) {
                notify(data.message, header);
            });
        }

        //Get Users data
        $rootScope.getUsersData();
        //Get all feeeds on page
        feedService.get().success(function (data, header) {
            feedService.feeds = data.data;
        }).error(function (data, header) {
            notify(data.message, header);
        });

    }]);


app.directive('feedAdd', function ($compile, $timeout) {
    return{
        restrict: 'E',
        replace: true,
        templateUrl: '/js/modules/feeds/add.html',
        link: function (scope, elements, attrs) {

            scope.preview_url = '';
            scope.body = '';

            var previewWithUrl = '<div id="preview"><span class="pull-right" ng-click="removePreview()">&times;</span><a class="embedly-card" href="{{preview_url}}"></a><div>Preview for {{preview_url}}</div></div>';
            var preview = '<div id="preview"></div>'

            scope.checkUrl = function (body) {
                if (scope.body !== body) {
                    scope.body = body;
                }
                $timeout(function () {
                    if (scope.preview_url) {
                        return;
                    }
                    if (scope.body !== body) {
                        return;
                    }
                    var urlPattern = /[-a-zA-Z0-9@:%_\+.~#?&//=]{2,256}\.[a-z]{2,4}\b(\/[-a-zA-Z0-9@:%_\+.~#?&//=]*)?/gi;
                    var subs = body.split(" ");
                    subs.some(function (text) {
                        if (urlPattern.test(text)) {
                            scope.preview_url = text;
                            var embedly = angular.element(previewWithUrl);
                            var e = $compile(embedly)(scope);
                            var division = elements[0].querySelector('#preview');
                            var $division = angular.element(division);
                            $division.replaceWith(e);
                            $compile($division)(scope);
                            return;
                        }
                    });

                }, 1500);
            }

            scope.removePreview = function () {
                scope.preview_url = '';
                var embedly = angular.element(preview);
                var e = $compile(embedly)(scope);
                var division = elements[0].querySelector('#preview');
                var $division = angular.element(division);
                $division.replaceWith(e);
            }

        }
    }
});
app.directive('myEmbedlyTest', function () {
    return {
        controller: function ($scope, $rootScope) {
            $scope.key = 'db66909faab54ad4b303cd66bd31dacc';
            $scope.query = {
                maxwidth: 300,
                maxheight: 300,
            };
        }
    }
});

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
angular.module('comment', [])
        .directive('commentForm', ['commentService', function (commentService) {
        return{
            restrict: 'E',
            replace: true,
            scope: {
                id: '@',
                loadmore: '@',
                loggeduser: '@',
                add: '&',
                delete: '&',
                load: '&',
                comments: '=',
                commentcount: '=',
                limitcount: '@'
            },
            templateUrl: '/scripts/Comment/comment-form.html',
            link: function (scope, elements, attrs) {
                
                scope.commentstoload = commentService.commentstoload;
                scope.resetForm = function () {
                    scope.Comment.body = "";
                    scope.commentAddForm.$setPristine();
                }
                
                scope.changelimitcountto=function($event,num){
                    scope.limitcount=num;
                    angular.element($event.currentTarget).parent().html(''); 
                }
            }
        }
        ;
    }]);
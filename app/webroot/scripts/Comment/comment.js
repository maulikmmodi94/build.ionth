'use strict';
/**
 * Comment module
 */
var app = angular.module('comment', []);

app.factory('commentService',['$http','$httpParamSerializerJQLike',function ($http,$httpParamSerializerJQLike) {

    var urlSeperator = '/';
    var controller = 'comments';
    var actions = {load: 'commentLoad', add: 'commentAdd', delete: 'delete'};

    this.commentstoload = 10;

    this.get = function (modelName, modelId, lastComment) {

        var url = urlSeperator + modelName + urlSeperator + actions.load + urlSeperator + modelId;

        if (angular.isNumber(lastComment))
            var url = url + urlSeperator + lastComment;

        return $http.get(url);
    };
    this.add = function (modelName, modelId, commentBody) {
        var url = urlSeperator + modelName + urlSeperator + actions.add + urlSeperator + modelId;
        var data = $httpParamSerializerJQLike({body: commentBody});
        return $http.post(url, data);
    }

    this.delete = function (id) {
        if (id) {
            var url = urlSeperator + controller + urlSeperator + actions.delete + urlSeperator + id;
            return $http.post(url);
        }
        return;
    }

    return this;
}]);
app.directive('commentForm', ['commentService', function (commentService) {
        return{
            restrict: 'E',
            replace: true,
            scope: {
                id: '@',
                loadmore: '@',
                loggeduser: '@',
                add: '&',
                delete: '&',
                load: '&',
                comments: '=',
                commentcount: '=',
                limitcount: '@'
            },
            templateUrl: '/scripts/Comment/comment-form-demo.html',
            link: function (scope, elements, attrs) {
                
                scope.commentstoload = commentService.commentstoload;
                scope.resetForm = function () {
                    scope.Comment.body = "";
                    scope.commentAddForm.$setPristine();
                }
                
                scope.changelimitcountto=function($event,num){
                    scope.limitcount=num;
                    angular.element($event.currentTarget).parent().html(''); 
                }
            }
        }
        ;
    }]);


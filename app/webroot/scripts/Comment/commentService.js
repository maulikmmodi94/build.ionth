'use strict';
/**
 * Comment module
 */
angular.module('comment', [])
        .service('commentService',['$http', function ($http) {

    var urlSeperator = '/';
    var controller = 'comments';
    var actions = {load: 'commentLoad', add: 'commentAdd', delete: 'delete'};

    this.commentstoload = 10;

    this.get = function (modelName, modelId, lastComment) {

        var url = urlSeperator + modelName + urlSeperator + actions.load + urlSeperator + modelId;

        if (angular.isNumber(lastComment))
            var url = url + urlSeperator + lastComment;

        return $http.get(url);
    };
    this.add = function (modelName, modelId, commentBody) {
        var url = urlSeperator + modelName + urlSeperator + actions.add + urlSeperator + modelId;
        var data = $.param({body: commentBody});
        return $http.post(url, data);
    }

    this.delete = function (id) {
        if (id) {
            var url = urlSeperator + controller + urlSeperator + actions.delete + urlSeperator + id;
            return $http.post(url);
        }
        return;
    }
}]);


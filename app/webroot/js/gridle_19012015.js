/*  Tool tips to show on hover */

app = angular.module('GridleApp', ['ngTagsInput', 'ui.calendar', 'ngAnimate', 'ngSanitize', 'comment','tc.chartjs']);
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


function arrayUnique(array) {
    var a = array.concat();
    for (var i = 0; i < a.length; ++i) {
        for (var j = i + 1; j < a.length; ++j) {
            if (a[i] === a[j])
                a.splice(j--, 1);
        }
    }
    return a;
};

function notify(message, code, options)
{
    var options = [];
    if (message)
    {
        var defaultOption = {
            ele: 'body', // which element to append to
            type: 'info', // (null, 'info', 'danger', 'success')
            offset: {from: 'bottom', amount: 20}, // 'top', or 'bottom'
            align: 'left', // ('left', 'right', or 'center')
            width: 250, // (integer, or 'auto')
            delay: 4000, // Time hwile the message will be displayed. It's not equivalent to the *demo* timeOut!
            allow_dismiss: true, // If true then will display a cross to close the popup.
            stackup_spacing: 10 // spacing between consecutively stacked growls.
        };

        if (code >= 200 && code < 300)
        {
            options.type = "info";
        }
        else if (code >= 300 && code < 400)
        {
            window.location.href = message;
        }
        else if (code >= 400)
        {
            options.type = "danger";
        }

        $.extend(defaultOption, options);
        $.bootstrapGrowl(message, defaultOption);
    }
}

/* Not used */
function disableTagsInput() {
    document.getElementsByTagName("tags-input")[0].getElementsByTagName("input")[0].disabled = true;
    var tagsRemoveButtons = document.getElementsByTagName("tags-input")[0].getElementsByClassName("remove-button");
    for (var i in tagsRemoveButtons) {
        tagsRemoveButtons[i].hidden = true;
    }
}

function validateEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

function findInObjectArray(array, obj, key1, key2) {
    var item = null;
    for (var i = 0; i < array.length; i++) {
        // I'm aware of the internationalization issues regarding toLowerCase()
        // but I couldn't come up with a better solution right now
        if ((array[i][key1].toString().toLowerCase() === obj[key1].toString().toLowerCase()) && (array[i][key2].toLowerCase() === obj[key2].toLowerCase())) {
            item = array[i];
            break;
        }
    }
    return item;
}
;
/*Data calling and management functions*/
app.run(['$rootScope', '$http', '$filter', '$timeout', 'createDialog', 'trackEvents', function ($rootScope, $http, $filter, $timeout, createDialog, trackEvents) {

        $rootScope.users = [];
        $rootScope.groups = [];
        $rootScope.networks = [];
        $rootScope.networks_expired = [];
        $rootScope.currentnetwork;
        $rootScope.user_networks = [];
        $rootScope.network_users = [];
        /* Set Default Headers */
        $http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded';
        /* Set headers as Ajax  */
        $http.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
        $http.defaults.headers.common['Accept'] = 'application/json';

        $rootScope.getUsersData = function (callback) {
            $rootScope.userPromise = $http({
                method: 'GET',
                url: '/datas/getUsers.json'
            }).success(function (data, header) {
                $rootScope.users = data.data['users'];
                $rootScope.loggeduser = data.data['loggedUser'];
                $rootScope.network_id = data.data['network_id'];
                if (typeof callback === "function") {
                    callback();
                }

            }).error(function (data, header) {
                notify(data.message, header);

            })
        };
        /* function to get current time */
        $rootScope.getDatetime = function () {
            return new Date();
        };
        $rootScope.getNetworksData = function (callback) {
            $rootScope.networkPrmise = $http({
                method: 'GET',
                url: '/datas/getNetworks.json'
            }).success(function (data, header) {
                $rootScope.networks = data.data['networks'];
                $rootScope.networks_expired = data.data['networks_expired'];
                $rootScope.currentnetwork = data.data['currentnetwork'];
                if (typeof callback === "function") {
                    callback();
                }

            })
        };
        $rootScope.findUserInProgress = false; //To avoid infinite digest loop, Jugaad
        $rootScope.findUser = function (user_id) {
            var index = $rootScope.getUserIndex(user_id);
            if (index > -1) {
                return index;
            } else {
                if (!$rootScope.findUserInProgress && typeof (user_id) !== 'undefined') {
                    $rootScope.findUserInProgress = true;
                    $rootScope.getUserInfo(user_id);
                } else {
                    return -1;
                }
            }
        }

        $rootScope.getUserIndex = function (user_id) {
            for (var i in $rootScope.users) {
                if ($rootScope.users[i]['UserInfo']['user_id'] == user_id) {
                    return i;
                }
            }
            return -1;
        }
        $rootScope.findUserNetwork = function (user_id) {
            return $rootScope.getUserNetworkIndex(user_id);
        }

        $rootScope.getUserNetworkIndex = function (user_id) {
            for (var i in $rootScope.user_networks) {
                if ($rootScope.user_networks[i]['UserNetwork']['user_id'] == user_id) {
                    return i;
                }
            }
            return -1;
        }

        $rootScope.getUserInfo = function (user_id) {
           $http({
                cache: true,
                method: 'GET',
                url: '/datas/getUserDetails/' + user_id + '.json'
            }).success(function (data, header) {
                data.data.user.UserInfo.last_name = ' (Removed)';
                data.data.user.UserInfo.name = data.data.user.UserInfo.name.split(" ")[0] + ' (Removed)';
                ($rootScope.users).push(data.data.user);
                $rootScope.findUserInProgress = false;

            })
        }

        $rootScope.getGroupsData = function (callback) {
            $rootScope.groupPromise = $http({
                method: 'GET',
                url: '/datas/getGroups.json'
            }).success(function (data, header) {
                $rootScope.groups = data.data['groups'];
                if (typeof callback === "function") {
                    callback();
                }

            })
        };
        $rootScope.getUserNetworksData = function (callback) {
            url = '/datas/getUserNetworks.json';
            $rootScope.userNetworkPromise = $http({
                method: 'GET',
                url: url
            }).success(function (data, header) {
                $rootScope.user_networks = data.data['user_networks'];
                $rootScope.network_users[$rootScope.currentnetwork] = data.data['user_networks'];
                if (typeof callback === "function") {
                    callback();
                }

            })
        };
        /* Deprecated function */
        $rootScope.getOtherUserNetworksData = function (network_id, callback) {
            url = '/datas/getUserNetworks.json';
            if (network_id)
            {
                url = '/datas/getUserNetworks/' + network_id + '.json';
            }
            $http({
                method: 'GET',
                url: url
            }).success(function (data, header) {
                $rootScope.network_users[network_id] = data.data['user_networks'];
                if (typeof callback === "function") {
                    callback();
                }

            })
        };
        //Returns Index of group in $rootScope.groups
        $rootScope.findGroup = function (id) {
            for (var i in $rootScope.groups) {
                if ($rootScope.groups[i]['Group']['id'] == id) {
                    return i;
                }
            }
            return -1;
        }
        $rootScope.chekUserInGroup = function (user_id, id) {
            for (var i in $rootScope.groups) {
                if ($rootScope.groups[i]['Group']['id'] == id) {
                    for (var j in $rootScope.groups[i]['UsersGroup']) {
                        if ($rootScope.groups[i]['UsersGroup'][j].user_id == user_id) {
                            return j;
                        }
                    }
                }
            }
            return -1;
        }

        $rootScope.showUserInfoModal = function (user_id)
        {
            var index = $rootScope.findUser(user_id);
            if (index > -1)
            {
                $rootScope.viewUser = $rootScope.users[index].UserInfo;
            }

            createDialog('User/view.html', {
                id: 'viewUser' + user_id,
                user_id: user_id,
                title: 'User information',
                footer: false,
                myModalClass: 'g-modal',
                deleteleave: 0,
                backdrop: true,
            });
        }


        $rootScope.dateFormat = function (date, format)
        {

            return $filter('date')(new Date(moment(date)), format);
        }

        $rootScope.momentDate = function (date)
        {
            moment.lang('en', {
                calendar: {
                    lastDay: '[Yesterday]',
                    sameDay: '[Today]',
                    nextDay: '[Tomorrow]',
                    lastWeek: 'dddd DD,MMMM',
                    nextWeek: '[next] dddd',
                    sameElse: 'dddd DD,MMMM'
                }
            });
            return moment(date).calendar();
        }

        $rootScope.orderByUserNetworkRole = function (usernetwork)
        {
            var priority;
            switch (usernetwork.UserNetwork.role) {
                case 'owner':
                    priority = 1;
                    break;
                case 'admin':
                    priority = 2;
                    break;
                case 'user':
                    priority = 3;
                    break;
                default:
                    priority = 4;
                    break;
            }
            return priority;
        }
        /*
         $timeout(function() {
         $("[rel=tooltip]").tooltip();
         });
         */
    }]);

app.config(['$compileProvider', function ($compileProvider)
    {
        $compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|ftp|mailto|file|skype):/);
    }]);

app.filter('UsersRemoveLogged', function () {
    return function (items, user_id) {
        if (items == null) {
            return false;
        }
        var arrayToReturn = [];
        arrayToReturn = $.grep(items, function (n, i) {
            return (n['UserInfo']['user_id'] !== user_id);
        });
        return arrayToReturn;
    };
});
/*Data calling and management functions*/

/* Service for tags */
app.service('tags', function ($rootScope, $http, $q) {

    $rootScope.tags = [];
    $rootScope.current_tags = [];
    $rootScope.autoCompleteList = [];
    $rootScope.taglist = [];
    $rootScope.tagsLoadData = function ()
    {
        $rootScope.taglist = [];
        $.each($rootScope.users, function (key, value) {
            $rootScope.taglist.push({value: value.UserInfo.user_id, name: value.UserInfo.name, type: 'user', img: value.UserInfo.vga})
        });
        $.each($rootScope.groups, function (key, value) {
            $rootScope.taglist.push({value: value.Group.id, name: value.Group.name, type: 'group'})
        });
    }

    $rootScope.$watchCollection('groups', function () {
        $rootScope.tagsLoadData();
    });
    $rootScope.tagsCreate = function (type, value) {
        var findObj = {'type': type, 'value': value};
        var tag = findInObjectArray($rootScope.taglist, findObj, 'value', 'type');
        ($rootScope.tags).push(tag);
    }

    $rootScope.getDifference = function (array1, array2) {
        return array1.filter(function (item) {
            return !findInObjectArray(array2, item, 'value', 'type');
        });
    };
    $('.modal').on('hidden.bs.modal', function () {
        $rootScope.tags = [];
    })

});
/* Service for tags */

app.service('network', function ($rootScope, $http, $filter, createDialog) {

    $rootScope.network_selected = -1;


    $rootScope.selectNetwork = function (network_id)
    {
        $rootScope.network_selected = angular.copy(network_id);
    }


    $rootScope.assignRole = function (user_id, role, callback) {

        if (role == 'user')
        {
            createDialog(null, {
                id: 'removeUserNetwork',
                title: 'Remove Admin',
                deleteleave: 0,
                string: 'Are you sure you want to remove as Admin ?',
                backdrop: true,
                success: {label: 'Yes, Remove', fn: function () {
                        $http({
                            method: 'GET',
                            url: '/networks/assignRole/' + user_id + '/' + role
                        }).success(function (data, header) {
                            $rootScope.getUserNetworksData();
                            if (typeof callback === "function") {
                                callback();
                            }
                            notify(data.message, header);

                        }).error(function (data, header) {
                            notify(data.message, header);

                        })
                    }},
                cancel: {label: 'Cancel', fn: function () {

                    }}
            });
        }
        else
        {
            $http({
                method: 'GET',
                url: '/networks/assignRole/' + user_id + '/' + role
            }).success(function (data, header) {

                $rootScope.getUserNetworksData();
                if (typeof callback === "function") {
                    callback();
                }
                notify(data.message, header);
            }).error(function (data, header) {
                notify(data.message, header);
            })
        }

    };


    $rootScope.editNetwork = function (name, editMode, oldName) {
        if (editMode || name == oldName)
        {
            return;
        }
        var xsrf = $.param({'data[Network][name]': name});
        $http({
            method: 'POST',
            url: '/networks/edit.json',
            data: xsrf
        }).success(function (data, header) {
            $rootScope.networks = data.data['networks'];
            $rootScope.currentnetwork = data.data['currentnetwork'];
            notify(data.message, header);
            if (typeof callback === "function") {
                callback();
            }
        }).error(function (data, header) {
            $rootScope.networks[$rootScope.currentnetwork].name = oldName;
            notify(data.message, header);
        })
    };


    $rootScope.addNetwork = function (form_id) {

        var xsrf = $('#' + form_id).serialize();
        $http({
            method: 'POST',
            url: '/networks/add/',
            data: xsrf
        }).success(function (data, header) {
            $rootScope.getNetworksData();
            if (typeof callback === "function") {
                callback();
            }
            notify(data.message, header);

        }).error(function (data, header) {
            notify(data.message, header);
        });
    };
    $rootScope.getUserNetwork = function (user_id)
    {
        var task = $rootScope.TaskData[$rootScope.getById(task_id)];
        var returnStatus = false;
        angular.forEach(task.Tags, function (value, key) {
            if (value.role == role && value.value == assignee)
            {
                returnStatus = true;
                return;
            }

        });
        return returnStatus;
    }

    $rootScope.removeUserNetwork = function (user_id, callback) {
        $http({
            method: 'GET',
            url: '/networks/remove/' + user_id + '.json'
        }).success(function (data, header) {
            $rootScope.getUserNetworksData();
            if (typeof callback === "function") {
                callback();
            }
            notify(data.message, header);

        }).error(function (data, header) {
            notify(data.message, header);

        })
    };
    $rootScope.removeUserNetworkConfirm = function (user_id)
    {

        createDialog(null, {
            id: 'removeUserNetwork',
            title: 'Remove User',
            deleteleave: 0,
            string: 'Are you sure you want to remove user ?',
            backdrop: true,
            success: {label: 'Yes, Remove', fn: function () {
                    $rootScope.removeUserNetwork(user_id);
                }},
            cancel: {label: 'Cancel', fn: function () {

                }}
        });
    }



});
/* In place of orderBy because networks array has key as network.id */
app.filter('networkAlphabetOrder', function () {
    return function (items) {
        if (items == null)
            return false;
        var sortable = [];
        angular.forEach(items, function (value, key) {
            sortable.push([value, value.name.toString().toLowerCase()])
        });
        sortable.sort(function (a, b) {
            return a[1].localeCompare(b[1]);
        })
        var items = [];
        angular.forEach(sortable, function (value, key) {
            items.push(value[0])
        });
        return items;
    }
});
/* Network Module */
app.controller('networkController', ['$rootScope', '$scope', '$http', 'network', 'tags', 'group', 'notification', function ($rootScope, $scope, $http, network, tags, group, notification) {
        /*Initialize */
        //Get Groups data
        $rootScope.invitenetwork=true
        $rootScope.getGroupsData();
        $rootScope.getUserNetworksData();
        $rootScope.getUsersData();
        // $rootScope.getNotificationsUnseen();
        $scope.manageSelected = false;
        
        $scope.toogleSelection = function(type){
            if(type == 'manage'){
            $scope.manageSelected = true;
            $('.menu-collapse').removeClass('active');
            }
            else if(type == 'Analytics'){
            $scope.manageSelected = false;    
            }
            
        };
        
    }]);
/* Network Module */
app.controller('navigationController', ['$rootScope', '$scope', '$http', 'network', function ($rootScope, $scope, $http, network) {
        $rootScope.getNetworksData();
    }]);
/* Desk Module */
app.controller('deskController', ['$rootScope', '$scope', '$filter', '$http', 'desk', 'tags', 'taskService', 'group', 'conversation', 'fileService', function ($rootScope, $scope, $filter, $http, desk, tags, taskService, group) {
        $scope.taskService = taskService;
        /*Initialize */
        $rootScope.users_in_groups = [];
        $rootScope.TaskfilterConfig.hideCompleted = true;
        //Get Groups data

//        $rootScope.getGroupsData($rootScope.tagsLoadData);
//        $rootScope.getUsersData($rootScope.tagsLoadData);
//        $rootScope.loadTaskData();
        $rootScope.getUsersData(function () {
            $rootScope.getGroupsData(function () {
                $rootScope.tagsLoadData();
                $rootScope.loadTaskData();
            });
        });
        $rootScope.$watchCollection('groups', function (newvalue, oldvalue) {
            if (newvalue !== oldvalue) {
                $rootScope.users_in_groups = $filter('deskusers_in_groups')($rootScope.users);
            }
        });
//        $rootScope.deskInitSelectedItem();
//
    }]);
app.service('desk', function ($rootScope, $filter, d3Service) {
    $rootScope.deskInitSelectedItem = function () {
        $rootScope.deskSelectedItem = {'value': null, 'type': null};
        $rootScope.selectFilter('all');
        $rootScope.filterWith = [];

        //Remove all related
        for (var group in $rootScope.groups) {
            $rootScope.groups[group]['isRelated'] = 0;
        }
        for (var user in $rootScope.users) {
            $rootScope.users[user]['isRelated'] = 0;
        }
        d3Service.remove_highlighted_links();
    }

    $rootScope.deskSelectitem = function (value, type) {


        $rootScope.deskSelectedItem = {'value': value, 'type': type};
        if (type === 'group') {
            for (var user in $rootScope.users) {
                if ($rootScope.chekUserInGroup($rootScope.users[user]['UserInfo'].user_id, value) > -1) {
                    $rootScope.users[user]['isRelated'] = 1;
                } else {
                    $rootScope.users[user]['isRelated'] = 0;
                }
                for (var group in $rootScope.groups) {
                    $rootScope.groups[group]['isRelated'] = 0;
                }
            }
            //  $rootScope.users = $filter('deskOrderItemsList')($rootScope.users, $rootScope, 'user');
        }
        else if (type === 'user') {
            for (var group in $rootScope.groups) {
                if ($rootScope.chekUserInGroup(value, $rootScope.groups[group]['Group'].id) > -1) {
                    $rootScope.groups[group]['isRelated'] = 1;
                } else {
                    $rootScope.groups[group]['isRelated'] = 0;
                }
                for (var user in $rootScope.users) {
                    $rootScope.users[user]['isRelated'] = 0;
                }
            }
            //$rootScope.groups = $filter('deskOrderItemsList')($rootScope.groups, $rootScope, 'group');
        }
//        $rootScope.selectFilter('BetweenMeAnd');
        $rootScope.filterWith = [{value: value, type: type}];
        //$rootScope.search;
        d3Service.highlight_links(value, type);
    }
});
app.filter('deskusers_in_groups', function ($rootScope) {
    return function (items, $scope, type) {
        if (items == null) {
            return false;
        }
        var members = [];
        for (var i in $rootScope.groups) {
            for (var j in $rootScope.groups[i].UsersGroup) {
                members.push($rootScope.groups[i].UsersGroup[j].user_id);
            }
        }
        var returnArray = [];
        for (var i in items) {
            if (members.indexOf(items[i]['UserInfo']['user_id']) > -1 && items[i]['UserInfo']['user_id'] !== $rootScope.loggeduser) {
                returnArray.push(items[i]);
            }
        }
        return returnArray;
    }
});
app.filter('dataSearchFilter', function ($rootScope) {
    return function (items, searchName, type) {
        if (items == null) {
            return false;
        }
        if (!searchName)
        {
            return items;
        }
        searchName = searchName.toLowerCase();
        var returnArray = [];
        if (type === 'user') {
            for (var i in items) {
                if (((items[i]['UserInfo']['name']).toLowerCase()).indexOf(searchName) > -1) {
                    returnArray.push(items[i]);
                }
                continue;
            }
        } else if (type === 'group') {
            for (var i in items) {
                if (((items[i]['Group']['name']).toLowerCase()).indexOf(searchName) > -1) {
                    returnArray.push(items[i]);
                }
                continue;
            }
        }
        return returnArray;
    }
});
/*  Anupama Panchal editing         */
app.service('d3Service', ['$document', '$q', '$rootScope', function ($document, $q, $rootScope) {

        //calculating position of groups and users nodes
        this.calculatePos = function (groups, users) {
            var groupPos = [];
            var userPos = [];
            var links = [];
            var data = [];
            var ParentCords = $('#g-desk-svg').offset();
            var getMyCords = $('#g-desk-me').offset();
            var myCords = {x: getMyCords.left - ParentCords.left + $('#g-desk-me').width() / 2, y: getMyCords.top - ParentCords.top + $('#g-desk-me').height() / 2};
            //console.log("groups");console.log(groups);
            ////finding co-ordinates of users
            for (var i in users) {
                var user_id = users[i].UserInfo.user_id;
                var ele = $('#itemUser' + i);
                var childPos = ele.offset();
                var childOffset = {
                    x1: childPos.left - ParentCords.left,
                    y1: childPos.top - ParentCords.top + ele.height() / 2,
                    x2: childPos.left - ParentCords.left + ele.width(),
                    y2: childPos.top - ParentCords.top + ele.height() / 2
                };
                userPos[user_id] = childOffset;
            }
            //console.log(userPos);
            //forming links and group position array
            for (var i in groups) {
                var group_id = groups[i].Group.id;
                var groups_data = groups[i].UsersGroup;
                var ele = $('#itemGroup' + i);
                if (ele.length) {
                    var childPos = ele.offset();
                    groups_data.forEach(function (d) {
                        if (d.user_id !== $rootScope.loggeduser) {
                            if (d.user_id in userPos) {
                                links.push({group_id: d.group_id, user_id: d.user_id});
                            }
                        }
                    });
                    //retrieving co-ordinates of groups
                    var childOffset = {
                        x1: childPos.left - ParentCords.left,
                        y1: childPos.top - ParentCords.top + ele.height() / 2,
                        x2: childPos.left - ParentCords.left + ele.width(),
                        y2: childPos.top - ParentCords.top + ele.height() / 2
                    };
                    groupPos[group_id] = childOffset;
                }
            }
            //console.log(groups);


            data['links'] = links;
            data['groupPos'] = groupPos;
            data['userPos'] = userPos;
            data['myCords'] = myCords;
            return data;
        };
        this.relationLinks = function (ele, data, sel) {
            //empty the svg if data changes
            ele.empty();
            //No rendering if no data
            if (data.links.length === 0) {
                return;
            }

            //variable initialization
            var svg_width = $('#g-desk-group-user-wrap').width();
            var svg_height = $('#g-desk-group-user-wrap')[0].scrollHeight;
            var linksData = [];
            //group and user links data
            data.links.forEach(function (d, i) {
                var a = {x: data.groupPos[d.group_id].y2, y: data.groupPos[d.group_id].x2};
                var b = {x: data.userPos[d.user_id].y1, y: data.userPos[d.user_id].x1};
                linksData.push({source: a, target: b, type: 'grp-usr', start: d.group_id, end: d.user_id});
            });
            //you and group links data
            for (var index in data.groupPos) {
                var a = {x: data.myCords.y, y: data.myCords.x};
                var b = {x: data.groupPos[index].y1, y: data.groupPos[index].x1};
                linksData.push({source: a, target: b, type: 'me-grp', start: 'me', end: index});
            }
            ;
            //svg container
            var svg = d3.select(ele[0])
                    .append('svg')
                    .attr('id', 'svg-links')
                    .attr('width', svg_width)
                    .attr('height', svg_height - 10)
                    .attr('viewBox', "0 0 " + svg_width + " " + svg_height)
                    .attr("fill", 'black');
            svg.selectAll('*').remove();
            //function for diagonal projection of lines
            var diagonal = d3.svg.diagonal().projection(function (d) {
                return [d.y, d.x];
            });
            // links drawing
            var links = svg.selectAll('.g-links')
                    .data(linksData)
                    .enter()
                    .append("path")
                    .attr("class", "g-links g-stroke-lightgrey")
                    .attr("class", function (d) {
                        /*  if (sel.value !== null) {
                         if (d.start === sel.value || d.end === sel.value)
                         return "g-links g-stroke-blue g-stroke-lightgrey";
                         else
                         return "g-links g-stroke-lightgrey";
                         } */
                        return "g-links g-stroke-lightgrey";
                    })
                    .attr("d", diagonal);
            if (sel)
                this.highlight_links(sel.value, sel.type);
        };
        //highlighting the links
        this.highlight_links = function (value, type) {
            d3.selectAll('.g-links').classed("g-links", function (o) {
                var element = d3.select(this);
                if ((((o.end === value && o.type === 'me-grp') || o.start === value) && (type === 'group')) || (o.end === value && type === 'user' && o.type !== 'me-grp')) {
                    //add blue line
                    element.classed("g-stroke-nutral-green", true);
                    //bringing link to top
                    element.moveToFront();
                } else {
                    //remove blue line
                    element.classed("g-stroke-nutral-green", false);
                }
                return true;
            });
        };
//removing all the highlights on links
        this.remove_highlighted_links = function () {
            d3.selectAll('.g-stroke-nutral-green').classed("g-stroke-nutral-green", false);
        };
//moving the highlighted links to top
        d3.selection.prototype.moveToFront = function () {
            return this.each(function () {
                this.parentNode.appendChild(this);
            });
        };
    }]);
app.directive('groupUserRelation', function ($rootScope, $window, $timeout, d3Service) {
    return {
        restrict: 'E',
        replace: true,
        link: function (scope, element, attrs) {

            scope.draw_links = function () {
                $timeout(function () {
                    var selected_item = $rootScope.deskSelectedItem;
                    //console.log(selected_item);
                    //calculating positions of nodes and generating link data
                    if (scope.filt_groups.length || scope.filt_users_in_groups.length)
                    {
                        scope.data = d3Service.calculatePos(scope.filt_groups, scope.filt_users_in_groups);
                        //draw the relations
                        d3Service.relationLinks(element, scope.data, selected_item);
                    }
                });
            }

            //checking if there is any change in group user data
            scope.$watchCollection('filt_groups', function (newVal, oldVal) {
                scope.draw_links();
            });
            scope.$watchCollection('filt_users_in_groups', function (newVal, oldVal) {
                scope.draw_links();
            });
            $window.onresize = function () {
                scope.$apply();
            };
            // Watch for resize event
            scope.$watch(function () {
                return angular.element($window)[0].innerWidth;
            }, function () {
                scope.draw_links();
            });
        }
    };
});
/* Desk Module End*/


/* Group Module */
app.controller('groupController', ['$rootScope', '$scope', '$http', 'tags', 'group', function ($rootScope, $scope, $http, tags, group) {
        var url = (window.location.pathname).split("/");
        if (url[2] === 'view' && url.length > 3) {
            $rootScope.getUsersData(function () {
                $rootScope.getGroupObject(url[3]);
            });
        }
    }]);
app.service('group', function ($rootScope, $http, $q, createDialog, trackEvents) {

    $rootScope.groupInitSelectedGroup = function () {
        $rootScope.groupSelectedGroup = null;
    }
    $rootScope.groupOpenSettings = function (id) {
        $rootScope.groupSelectedGroup = id;
        $rootScope.groupCurrentIndex = $rootScope.findGroup(id);
        $('#groupManageModal').modal('show');
    }

    $rootScope.groupOpenCreate = function () {
        $('#groupModalCreate').modal('show');
    }

    $rootScope.getGroupObject = function (group_id) {
        $http({
            method: 'Post',
            url: '/groups/getGroup/' + group_id,
        }).success(function (data, header) {
            $rootScope.groups[0] = data.data[0];
            $rootScope.groupSelectedGroup = data.data[0].Group.id;
            $rootScope.groupCurrentIndex = $rootScope.findGroup($rootScope.groupSelectedGroup);
        }).error(function (data, header) {
        })
    }

    $rootScope.groupCreate = function (data) {
        if (data) {
            $('#creategroupbtn').button('loading');
            var xsrf = $.param({data: data});
            $http({
                method: 'Post',
                url: '/groups/add/',
                data: xsrf
            }).success(function (data, header) {
                $rootScope.groupDataAdd(data.data);
                $('#groupModalCreate').modal('hide');
                $rootScope.groupOpenSettings(data.data['Group']['id']);
                notify(data.message, header);

                trackEvents.trackTeamAdd('Team-Add', {group_name: data.data.Group.name, network_name: $rootScope.networks[$rootScope.currentnetwork].name});

            }).error(function (data, header) {
                notify(data.message, header);

            }).then(function () {
                $('#creategroupbtn').button('reset');
            })
        }
    }

    $rootScope.groupEditBtn = function (id, name, oldname) {
        var data = {};
//        var name = $.trim(angular.element(document.querySelector('#groupnametext' + id)).val());
//        var oldname = $.trim(angular.element(document.querySelector('#groupnametext' + id)).data("name"));
        if (name !== oldname)
        {
            data.name = name;
            data.group_id = id;
            var xsrf = $.param({data: data});
            $http({
                method: 'POST',
                url: '/groups/edit/',
                data: xsrf
            }).success(function (data, header) {
                $rootScope.groupDataEdit(id, data.data['Group']);
                notify(data.message, header);

            }).error(function (data, header) {
                notify(data.message, header);

            });
        }
    };
    $rootScope.groupAddMembers = function (id, users) {
        if (users.userlist.length > 0) {
            var data = {};
            data.userlist = users.userlist;
            data.group_id = id;
            $rootScope.tags = [];
            $('#addmemberbtn').button('loading');
            var xsrf = $.param({data: data});
            $http({
                method: 'POST',
                url: '/groups/addmember/',
                data: xsrf
            }).success(function (data, header) {
                $rootScope.groupDataReplace(id, data.data);
                $('#addmemberbtn').html('Add member');
                notify(data.message, header);

            }).error(function (data, header) {
                notify(data.message, header);

            }).then(function () {
                $('#addmemberbtn').button('reset');
            })
        }
    }

    $rootScope.groupRemoveMembers = function (user_id, id) {
        var data = {};
        data.group_id = id;
        data.user_id = user_id;
        var xsrf = $.param({data: data});
        $http({
            method: 'Post',
            url: '/groups/removemember/',
            data: xsrf
        }).success(function (data, header) {
            $rootScope.groupDataReplace(id, data.data);
            notify(data.message, header);

        }).error(function (data, header) {
            notify(data.message, header);

        })
    };
    // delete group and leave group
    $rootScope.groupDelete = function (id) {
        var data = {};
        data.group_id = id;
        var xsrf = $.param({data: data});
        $http({
            method: 'POST',
            url: '/groups/delete/',
            data: xsrf
        }).success(function (data, header) {
            var index = $rootScope.findGroup(id);
            $rootScope.groups.splice(index, 1);
            $rootScope.groupInitSelectedGroup();
            $rootScope.deskInitSelectedItem();
            notify(data.message, header);

        }).error(function (data, header) {
            notify(data.message, header);

        });
    };
    $rootScope.groupLeave = function (id) {
        var data = {};
        data.group_id = id;
        var xsrf = $.param({data: data});
        $http({
            method: 'POST',
            url: '/groups/leave/',
            data: xsrf
        }).success(function (data, header) {
            var index = $rootScope.findGroup(id);
            $rootScope.groups.splice(index, 1);
            $rootScope.groupInitSelectedGroup();
            $rootScope.deskInitSelectedItem();
            notify(data.message, header);

        }).error(function (data, header) {
            notify(data.message, header);
        });
    };
    /* Data Manipulation functions */
    $rootScope.groupDataAdd = function (data) {
        $rootScope.groups.push(data);
    };
    $rootScope.groupDataEdit = function (id, data) {
        var index = $rootScope.findGroup(id);
        $rootScope.groups[index]['Group'] = data;
    };
    $rootScope.groupDataEditUsers = function (id, data) {
        var index = $rootScope.findGroup(id);
        $rootScope.groups[index]['UsersGroup'] = data;
    };
    $rootScope.groupDataReplace = function (id, data) {
        var index = $rootScope.findGroup(id);
        $rootScope.groups[index] = data;
    };
    //Modal Windows
    $rootScope.groupModalDelete = function (id, name) {
        $('#groupManageModal').modal('hide');
        createDialog(null, {
            id: 'groupdeletemodal',
            title: 'delete group',
            deleteleave: 0,
            string: 'Are you sure you want to delete group ' + '<span class=groupnamemodal>' + name + '</span> ?',
            backdrop: true,
            success: {label: 'Yes, Delete', fn: function () {
                    $rootScope.groupDelete(id);
                }},
            cancel: {label: 'Cancel', fn: function () {
                    $('#groupManageModal').modal('show');
                }}
        });
    };
    $rootScope.groupModalLeave = function (id, name) {
        $('#groupManageModal').modal('hide');
        createDialog(null, {
            id: 'leavegroupmodal',
            title: 'leave group',
            deleteleave: 1,
            string: 'Are you sure you want to leave group ' + '<span class=groupnamemodal>' + name + '</span> ?',
            backdrop: true,
            success: {label: 'Yes, Leave', fn: function () {
                    $rootScope.groupLeave(id);
                }},
            cancel: {label: 'Cancel', fn: function () {
                    $('#groupManageModal').modal('show');
                }}

        });
    };
    $rootScope.groupModalMemberRemove = function (group_id, group_name, user, user_name) {
        createDialog(null, {
            id: 'groupmodalmemberremove',
            title: 'Remove Member',
            deleteleave: 0,
            string: 'Are you sure you want to remove member ' + user_name + ' from group ' + group_name + '?',
            backdrop: true,
            success: {label: 'Yes, Delete', fn: function () {
                    $rootScope.groupRemoveMembers(user, group_id);
                }},
            cancel: {label: 'Cancel', fn: function () {
                }}
        });
    };
    $rootScope.groupTagsAutocompleteLoad = function (id) {
        var groupTags = [];
        for (var i in $rootScope.taglist) {
            if ($rootScope.taglist[i]['type'] === 'group') {
                groupTags.push({'value': $rootScope.taglist[i].value, 'type': 'group'});
            }
        }
        if (arguments.length > 0) {
            var users = $rootScope.groups[$rootScope.findGroup(id)]['UsersGroup'];
            for (var i in users) {
                groupTags.push({'value': users[i].user_id, 'type': 'user'});
            }
            var groupTags = $rootScope.getDifference($rootScope.taglist, groupTags);
            var deferred = $q.defer();
            deferred.resolve(groupTags);
            return deferred.promise;
        } else {
            var groupTags = $rootScope.getDifference($rootScope.taglist, groupTags);
            var deferred = $q.defer();
            deferred.resolve(groupTags);
            return deferred.promise;
        }
    }

});
/* Group Module End*/

/*File Module */
app.factory('fileService', function ($rootScope, $http, $q, $location, createDialog, commentService) {
    $rootScope.files = [];
    $rootScope.newFile = [];
    $rootScope.fileInitSelectedItem = function () {
        $rootScope.fileSelectedItem = null;
        $rootScope.fileCurrentIndex = null;
    }
    $rootScope.fileSelectItem = function (doc_id) {
        $rootScope.fileSelectedItem = doc_id;
        $rootScope.fileCurrentIndex = $rootScope.findFile(doc_id);
    }
    $rootScope.fileInitFilterItem = function () {
        $rootScope.fileFilterItem = {};
    }
    $rootScope.fileSelectFilterItem = function (type, value) {
        $rootScope.fileFilterItem = {type: type, value: value};
    }
    //Returns Index of group in $rootScope.groups
    $rootScope.findFile = function (id) {
        for (var i in $rootScope.files) {
            if ($rootScope.files[i]['Doc']['id'] == id) {
                return i;
            }
        }
        return -1;
    };

    $rootScope.getFile = function (id) {
        $http({
            method: 'Post',
            url: '/docs/getDoc/' + id + '.json'
        }).success(function (data, header) {
            $rootScope.files[0] = data.data;
            $rootScope.fileCurrentIndex = 0;
            if (typeof callback === "function") {
                callback();
            }
        })
    };
    $rootScope.getFilesData = function (callback) {
        $http({
            method: 'GET',
            url: '/docs/getFiles.json'
        }).success(function (data, header) {
            $rootScope.files = data.data['files'];
            if (typeof callback === "function") {
                callback();
            }
        })
    };
    $rootScope.fileModalOpenManage = function () {
        $('#fileManageModal').modal('show');
    }
    $rootScope.fileModalUpload = function () {
        $('#fileUploadModal').modal({backdrop: 'static'});
    }

    $rootScope.fileEditBtn = function (id, name, oldname) {
        var data = {};
        if (name !== oldname)
        {
            data.name = name;
            data.doc_id = id;
            var xsrf = $.param({data: data});
            $http({
                method: 'POST',
                url: '/docs/edit.json',
                data: xsrf
            }).success(function (data, header) {
                $rootScope.fileDataEdit(id, data.data['Doc']);
                notify(data.message, header);
            }).error(function (data, header) {
                notify(data.message, header);
            });
        }
    };
    $rootScope.filesAddMembers = function (id, data) {
        if (data.userlist.length > 0) {
            data.doc_id = id;
            $rootScope.tags = [];
            $('#addmemberbtn').button('loading');
            var xsrf = $.param({data: data});
            $http({
                method: 'POST',
                url: '/docs/addSharedList.json',
                data: xsrf
            }).success(function (data, header) {
                $rootScope.fileDataReplace(id, data.data);
                $('#addmemberbtn').html('Add member');
                notify(data.message, header);
            }).error(function (data, header) {
                notify(data.message, header);
            }).then(function () {
                $('#addmemberbtn').button('reset');
            })
        }
    }

    $rootScope.filesRemoveMember = function (id, type, value) {
        var data = {};
        data.doc_id = id;
        data.value = value;
        data.type = type;
        var xsrf = $.param({data: data});
        $http({
            method: 'POST',
            url: '/docs/removeSharedList.json',
            data: xsrf
        }).success(function (data, header) {
            $rootScope.fileDataReplace(id, data.data);
            notify(data.message, header);
        }).error(function (data, header) {
            notify(data.message, header);
        }).then(function () {
        })
    }

    $rootScope.fileTagsAutocompleteLoad = function (id) {
        var fileTags = [];
        if (arguments.length > 0) {
            var users = $rootScope.files[$rootScope.findFile(id)]['DocAssignment'];
            for (var i in users) {
                if (users[i].user_id !== null)
                    fileTags.push({'value': users[i].user_id, 'type': 'user'});
                else if (users[i].group_id !== null)
                    fileTags.push({'value': users[i].group_id, 'type': 'group'});
            }
            var fileTags = $rootScope.getDifference($rootScope.taglist, fileTags);
            var deferred = $q.defer();
            deferred.resolve(fileTags);
            return deferred.promise;
        } else {
            var fileTags = $rootScope.getDifference($rootScope.taglist, fileTags);
            var deferred = $q.defer();
            deferred.resolve(fileTags);
            return deferred.promise;
        }
    }

    $rootScope.options = {
        // Required. Called when a user selects an item in the Chooser.
        success: function (files) {
            $rootScope.newFile = files[0];
            $rootScope.newFile.provider = 'dropbox';
            $rootScope.newFile.preview_url = $rootScope.newFile.link;
            $rootScope.newFile.size = $rootScope.newFile.bytes;
            $rootScope.newFile.url = $rootScope.newFile.link.replace(/(dl=)[^\&]+/, '$1' + 1);
            $rootScope.$apply();
        },
        // Optional. Called when the user closes the dialog without selecting a file
        // and does not include any parameters.
        cancel: function () {
            $rootScope.newFile = [];
            $rootScope.$apply();
            console.log('No file selected from dropbox')
        },
        // Optional. "preview" (default) is a preview link to the document for sharing,
        // "direct" is an expiring link to download the contents of the file. For more
        // information about link types, see Link types below.
        linkType: "preview", // or "direct"

        // Optional. A value of false (default) limits selection to a single file, while
        // true enables multiple file selection.
        multiselect: false, // or true

        // Optional. This is a list of file extensions. If specified, the user will
        // only be able to select files with these extensions. You may also specify
        // file types, such as "video" or "images" in the list. For more information,
        // see File types below. By default, all extensions are allowed.
        extensions: ['.pdf', '.doc', '.docx', '.jpeg', '.png', '.jpg', '.zip', '.svg'],
    };

    $rootScope.DropboxUpload = function ()
    {
        console.log('upload called');
        Dropbox.choose($rootScope.options);
    }

    $rootScope.Upload = function ()
    {
        var data = {};
        data.Doc = $rootScope.newFile;
        data.userlist = $rootScope.tags;
        var xsrf = $.param({data: data});
        $http({
            method: 'POST',
            url: '/docs/xUpload.json',
            data: xsrf
        }).success(function (data, header) {
            $rootScope.fileDataAdd(data.data);
            notify(data.message, header);
            $('#fileUploadModal').modal('hide');

        }).error(function (data, header) {
            notify(data.message, header);
        }).then(function ()
        {
            $rootScope.newFile = [];
            $rootScope.tags = [];
            if (typeof callback === "function") {
                callback();
            }
        });




    }

    $(document).ready(function () {
        // Prevent Dropzone from auto discovering this element:
        Dropzone.options.myAwesomeDropzone = false;
        // Disable auto discover for all elements:
        Dropzone.autoDiscover = false;
        var myDropzone = new Dropzone('#my-awesome-dropzone', {
            url: '/docs/upload.json',
            method: 'Post',
            autoProcessQueue: false,
            maxFilesize: 20, //MB
            addRemoveLinks: true, //cancel upload butotn
            maxFiles: 10, //maximum number of files to be allowed at once
            parallelUploads: 10,
            dictInvalidFileType: 'File type not supported',
            dictFileTooBig: "File size limit exceeds. Please upload files upto 1 MB.",
            accept: function (file, done) {
                if (file) {
                    $('#dropzone_backdrop').hide();
                    console.log("called");
                    done();
                }
            }
        });
        $('#fileUploadBtn').on('click', function (e) {
            e.preventDefault();
            if (myDropzone.getQueuedFiles().length != 0 && myDropzone.getUploadingFiles().length == 0) {
                $('#fileUploadShareInput').attr('hidden', 'hidden');
                $('#fileUploadShareDisplay').attr('hidden', false);
                $('#fileUploadBtn').button('loading');
                myDropzone.processQueue();
            }
        });
        myDropzone.on("sending", function (file, xhr, formData) {
            if (($rootScope.tags).length > 0)
                for (var i in $rootScope.tags) {
                    formData.append("userlist[" + i + "][value]", $rootScope.tags[i]['value']);
                    formData.append("userlist[" + i + "][name]", $rootScope.tags[i]['name']);
                    formData.append("userlist[" + i + "][type]", $rootScope.tags[i]['type']);
                }
        });
        myDropzone.on("complete", function (file) {
            var response = JSON.parse(file.xhr.response);
            var responsestatuscode = JSON.parse(file.xhr.status);
//            console.log(responsestatuscode);
            if (responsestatuscode >= 200 && responsestatuscode < 300) {
                $rootScope.fileDataAdd(response.data);
                myDropzone.removeFile(file);
                if (myDropzone.getQueuedFiles().length == 0 && myDropzone.getUploadingFiles().length == 0) {
                    $('#fileUploadShareInput').attr('hidden', false);
                    $('#fileUploadShareDisplay').attr('hidden', 'hidden');
                    fileModalUploadClose();
                }
                $rootScope.$apply();
                trackEvents.trackFileUpload('File-Upload', {file_name: response.data.Doc.name, type: response.data.Doc.file_extension, shared_count: response.data.DocAssignment.length - 1});

            } else {
                file.previewElement.classList.add("dz-error");
                var _ref = file.previewElement.querySelectorAll("[data-dz-errormessage]");
                var _results = [];
                for (_i = 0, _len = _ref.length; _i < _len; _i++) {
                    var node = _ref[_i];
                    _results.push(node.textContent = response.message);
                }
                return _results;
            }
            notify(response.message, {type: response.status});
        });
        myDropzone.on("queuecomplete", function () {
            $('#fileUploadBtn').button('reset');
            $('#fileUploadShareInput').attr('hidden', false);
            $('#fileUploadShareDisplay').attr('hidden', 'hidden');
            $rootScope.tags = [];
        });
        fileModalUploadClose = function () {
            if (myDropzone.getUploadingFiles().length == 0) {
                $('#fileUploadModal').modal('hide');
            }
        }


    });
    
    $rootScope.fileDownload = function (id) {
        if (id) {
            $http({
                method: 'POST',
                url: '/docs/download/' + id + '.json'
            }).success(function (data, header) {
                window.location.href = data.data;
            }).error(function (data, header) {
                notify(data.message, header);
            });
        }
    }

    $rootScope.fileDelete = function (id) {
        var data = {};
        var xsrf = $.param({data: data});
        $http({
            method: 'POST',
            url: '/docs/delete/' + id + '.json'
        }).success(function (data, header) {
            var index = $rootScope.findFile(id);
            $rootScope.files.splice(index, 1);
            $rootScope.fileInitSelectedItem();
            notify(data.message, header);
        }).error(function (data, header) {
            notify(data.message, header);
        });
    };
    /* Data Manipulation functions */
    $rootScope.fileDataAdd = function (data) {
        if ($rootScope.files)
            $rootScope.files.push(data);
    };
    $rootScope.fileDataEdit = function (id, data) {
        var index = $rootScope.findFile(id);
        $rootScope.files[index]['Doc'] = data;
    };
    $rootScope.fileDataEditUsers = function (id, data) {
        var index = $rootScope.findFile(id);
        $rootScope.files[index]['DocAssignment'] = data;
    };
    $rootScope.fileDataReplace = function (id, data) {
        var index = $rootScope.findFile(id);
        $rootScope.files[index] = data;
    };
    //Modal Windows
    $rootScope.fileModalDelete = function (id, name) {
        $('#fileManageModal').modal('hide');
        createDialog(null, {
            id: 'filedeletemodal',
            title: 'delete file',
            deleteleave: 0,
            string: 'Are you sure you want to delete file ' + '<span class=filenamemodal>' + name + '</span> ?',
            backdrop: true,
            success: {label: 'Yes, Delete', fn: function () {
                    $rootScope.fileDelete(id);
                }},
            cancel: {label: 'Cancel', fn: function () {
                    $('#fileManageModal').modal('show');
                }}
        });
    };
    $rootScope.fileModalMemberRemove = function (name, id, type, value, user_name) {
        createDialog(null, {
            id: 'filemodalmemberremove',
            title: 'Remove Member',
            deleteleave: 0,
            string: 'Are you sure you want to unshare file ' + '<span class=filenamemodal>' + name + '</span> with ' + user_name + '?',
            backdrop: true,
            success: {label: 'Yes, Delete', fn: function () {
                    $rootScope.filesRemoveMember(id, type, value);
                }},
            cancel: {label: 'Cancel', fn: function () {
                }}
        });
    };
    $rootScope.fileType = function (mime, files) {
        var res = mime.split("/");
        switch (res[0]) {
            case 'image':
                return 'image';
                break;
            case 'audio':
                return 'audio';
                break;
            case 'video':
                return 'image';
                break;
            case 'text':
                return 'text';
                break;
            default:
                return 'other';
                break;
        }
    };

    var fact = {};
    fact.addComment = function (id, body) {
        if (body && body.trim()) {
            commentService.add('docs', id, body)
                    .success(function (data, header) {
                        var index = $rootScope.findFile(id);
                        $rootScope.files[index]['Comment'] = ($rootScope.files[index]['Comment']).concat(data.data);
                        $rootScope.files[index]['Doc']['comment_count'] = $rootScope.files[index]['Doc']['comment_count'] + 1;
                    })
                    .error(function (data, header) {
                        notify(data.message, header);
                    })
        }
    }

    fact.deleteCommentModal = function (id, commentId) {
        createDialog(null, {
            id: 'filecommentdelete',
            title: 'Delete Comment',
            deleteleave: 0,
            string: 'Are you sure you want to delete comment?',
            backdrop: true,
            success: {label: 'Yes, Delete', fn: function () {
                    fact.deleteComment(id, commentId);
                }},
            cancel: {label: 'Cancel', fn: function () {
                }}
        });
    }

    fact.deleteComment = function (id, commentId) {
        if (commentId) {
            commentService.delete(commentId)
                    .success(function (data, header) {
                        var index = $rootScope.findFile(id);
                        $rootScope.files[index]['Comment'] = ($rootScope.files[index]['Comment']).filter(function (comment) {
                            return comment.id != commentId;
                        });
                        $rootScope.files[index]['Doc']['comment_count'] = $rootScope.files[index]['Doc']['comment_count'] - 1;
//                        notify(data.message, header);
                    })
                    .error(function (data, header) {
                        notify(data.message, header);
                    })
        }
    }

    fact.getComment = function (id, lastId) {
        if (angular.isUndefined(lastId))
            var lastId = null;

        //Check loadmore variable exists and is false
        var index = $rootScope.findFile(id);
        if (angular.isUndefined($rootScope.files[index]['moreComments']) || $rootScope.files[index]['moreComments'] !== false) {
            commentService.get('docs', id, lastId)
                    .success(function (data, header) {
                        $rootScope.files[index]['Comment'] = ($rootScope.files[index]['Comment']).concat(data.data.comments);
                        $rootScope.files[index]['moreComments'] = data.data['loadMore'];
                    })
                    .error(function (data, header) {
                        notify(data.message, header);
                    })
        }
    }

    fact.filePreviewUrl = null;
    fact.setPreviewUrl = function (doc) {
         $('#framebench-modal').modal('show');
        $http({
            method: 'GET',
            url: '/docs/preview/' + doc.id + '.json',
            cache: true
        }).success(function (data, header) {
            $rootScope.files[$rootScope.findFile(doc.id)].Doc.preview_url = data.data;
            fact.filePreviewUrl = data.data;
        }).error(function (data, header) {
            notify(data.message, header);
        });
    };

    fact.frameBenchMimes = function (mime) {
        var supported_mimes = ['image/gif', 'image/jpeg', 'image/jpg', 'image/png', 'image/tiff', 'image/vnd.adobe.photoshop', 'application/pdf', 'video/mp4', 'application/mp4', 'video/x-m4v', 'application/vnd.ms-powerpoint', 'application/vnd.openxmlformats-officedocument.presentationml.presentation', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document', 'application/msword', 'application/vnd.ms-excel', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'];
        if (supported_mimes.indexOf(mime) > -1)
            return true;
        else
            return false;
    };


    return fact;
});
app.filter('fileDisplayFilter', function () {
    return function (items, type, value) {
        var returnArray = [];
        if (!type || !value)
            return items;
        if (type === 'user') {
            for (var i in items) {
                if (items[i]['Doc']['user_id'] == value) {
                    returnArray.push(items[i]);
                } else {
                    for (var j in items[i]['DocAssignment']) {
                        if (items[i]['DocAssignment'][j]['user_id'] == value) {
                            returnArray.push(items[i]);
                            break;
                        }
                    }
                }
            }
        } else if (type === 'group') {
            for (var i in items) {
                for (var j in items[i]['DocAssignment']) {
                    if (items[i]['DocAssignment'][j]['group_id'] == value) {
                        returnArray.push(items[i]);
                        break;
                    }
                }
            }
        }
        return returnArray;
    }
});

app.directive('frameBench', function (fileService, $sce) {
    return{
        template: '<div id="framebench-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"><div class="modal-dialog g-modal" style="width:80%;">\n\
    <div class="modal-content g-modal-content">\n\
        <div class="modal-body g-modal-body" style="padding:0px;">\n\
            <div ng-show="!filePreviewUrl" class="col-sm-12" style="height:600px">\n\
                <p class="text-center" style="line-height:600px; font-size:2em;">Waiting for the file...</p>\n\
            </div>\n\
            <iframe id="framebench-iframe" style="width: 100%; height:600px;" ng-src="{{filePreviewUrl}}" ng-show="filePreviewUrl"></iframe>\n\
            \n\
            </div>\n\
    </div>\n\
</div></div>',
        link: function (scope, element, attrs) {

            scope.iframeloading = false;

            scope.$watch(function () {
                return fileService.filePreviewUrl;
            }, function (newval) {
                if (newval && newval !== 'about:blank') {
                  //  $('#framebench-modal').modal('show');
                    scope.filePreviewUrl = $sce.trustAsResourceUrl(fileService.filePreviewUrl);
                    scope.iframeloading = true;
                }
            });

            $('#framebench-modal').on('hidden.bs.modal', function () {
                scope.filePreviewUrl = $sce.trustAsResourceUrl('about:blank');
                fileService.filePreviewUrl = 'about:blank';
                scope.$apply();
            });

//            $('#framebench-iframe').load(function () {
//                console.log('iframe loaded');
//                scope.iframeloading = false;
//                scope.$apply();
//            });

        }
    }
});

app.controller('fileController', ['$rootScope', '$scope', '$http', 'tags', 'fileService', function ($rootScope, $scope, $http, tags, fileService) {

        $scope.fileService = fileService;
        $rootScope.selectedType = null;
        $rootScope.selectedValue = null;
        $rootScope.selectItem = function (type, value) {
            $rootScope.selectedType = type;
            $rootScope.selectedValue = value;
        }

        $rootScope.$watch('selectedValue', function () {
            if ($rootScope.selectedType == 'user') {
                if ($rootScope.selectedValue == $rootScope.loggeduser) {
                    $rootScope.showFilesRelation = 'All your files';
                } else {

                    $rootScope.showFilesRelation = 'With You and ' + $rootScope.users[$rootScope.findUser($rootScope.selectedValue)]['UserInfo']['name'];
                }
            } else if ($rootScope.selectedType == 'group') {
                $rootScope.showFilesRelation = 'With You and Group ' + $rootScope.groups[$rootScope.findGroup($rootScope.selectedValue)]['Group']['name'];
            }
        })

        /*Initialize */
        //Get Groups data
        $rootScope.getUsersData(function () {
            $rootScope.getGroupsData($rootScope.tagsLoadData);
            var url = (window.location.pathname).split("/");
            if (url[2] === 'view' && url.length > 3) {
                $rootScope.getFile(url[3]);
            } else {
                $rootScope.getFilesData(function () {
                    $rootScope.fileInitSelectedItem();
                    $rootScope.fileInitFilterItem();
                    $scope.selectedValue = $rootScope.loggeduser;
                    $scope.selectedType = 'user';
                });
            }
        });
    }]);
/*File Module End*/

app.controller('headerController', ['$rootScope', '$scope', '$http', 'notification', 'conversation', function ($rootScope, $scope, $http, notification, conversation) {

    }]);
/* Notification Module */
app.controller('notificationController', ['$rootScope', '$scope', '$http', 'notification', function ($rootScope, $scope, $http, notification) {
        $http({
            method: 'GET',
            url: '/logs/getNotifications/' + 499 + '.json'
        }).success(function (data, header) {
            $rootScope.notificationsPage = data.data;
        });
    }]);
app.service('notification', function ($rootScope, $http) {

    $rootScope.notificationInit = function () {
        $rootScope.getNotificationsData();
        if ($rootScope.notificationsUnseenCount && $rootScope.notificationsUnseenCount > 0) {
            $rootScope.setNotificationSeen();
        }
    }

    $rootScope.getNotificationsData = function (pageInit, limit, callback) {
        if (!limit) {
            limit = 20;
        }
        if (pageInit || ($rootScope.notificationsUnseenCount > 0)) {
            $http({
                method: 'GET',
                url: '/logs/getNotifications/' + limit + '.json'
            }).success(function (data, header) {
                $rootScope.notifications = data.data;
                if (typeof callback === "function") {
                    callback();
                }
            })
        }
    }

    /*Used on only networks page*/
    $rootScope.getNotificationsUnseen = function (callback) {
        $http({
            method: 'GET',
            cache: true,
            url: '/logs/getUnseenNotifications.json'
        }).success(function (data, header) {
            $rootScope.notifications = data.data;
        })
    }

    $rootScope.getNotificationUnseenCount = function (callback) {
        $http({
            method: 'GET',
            cache: true,
            url: '/datas/getNotificationCount.json'
        }).success(function (data, header) {
            $rootScope.notificationsUnseenCount = data.data;
            if ($rootScope.notificationsUnseenCount > 0)
            {
            }
        }).then()
        {
            notifyInit();
        }
    }

    function checkNotification()
    {
        $http({
            method: 'GET',
            cache: false,
            url: '/datas/getNotificationCount.json'
        }).success(function (data, header) {
            if ($rootScope.notificationsUnseenCount < data.data)
            {
//                   
                document.getElementById('notifySound').play();
            }
            $rootScope.notificationsUnseenCount = data.data;
        })
    }

    function notifyInit()
    {
        $('body').append("<audio id=\"notifySound\" class=\"notifySound\" controls><source src=\"https://s3-us-west-2.amazonaws.com/gridle-public/notify.mp3\" type=\"audio/mpeg\"></audio>");
        $('.notifySound').css('visibility', 'hidden');
        setInterval(checkNotification, 30000);
    }




    $rootScope.setNotificationSeen = function (callback) {
        if ($rootScope.notificationsUnseenCount) {
            $http({
                method: 'GET',
                url: '/notifications/setUnreadAll/'
            }).success(function (data, header) {
                $rootScope.notificationsUnseenCount = data.data;
            })
        }
    }

});
app.filter('notificationByNetwork', function () {
    return function (items, network_id) {
        arrayToReturn = [];
        if (items == null || items.length == 0) {
            return arrayToReturn;
        }
        arrayToReturn = items.filter(function (not) {
            return not.network_id == network_id;
        });
        return arrayToReturn;
    };
});
app.directive('notificationDropDown', function ($rootScope, notification) {
    return{
        restrict: 'A',
//        templateUrl: '/pages/notifications_dropdown',
        template: '<div class="notifications-wrap">\n\
<div class="notifcations-options text-center">\n\
<p class="notifications-heading text-muted font-big-1 p-margin-0" >Notifications</p>\n\
</div>\n\
<div class="notifications-content g-scrollbar">\n\
<div class="notifications-loading" ng-if="!notifications">\n\
<h1 class="text-center">Loading ...</h1>\n\
</div>\n\
<div class="notifications-list container-fluid ">\n\
<div ng-if="notifications.length==0">It\'s lonely in here for now.. But when someone performs an action associated to you, you will be notified here..</div>\n\
<div class="notification-div row " ng-repeat="notification in notifications" ng-class="{ \'notification-unread\' : !notification.isSeen }">\n\
<a href="{{ notification.link }}">\n\
<div class="notification-graphic col-lg-2 col-md-2 col-sm-2 col-xs-2">\n\
<p class ="log-message-icon  p-margin-0" ng-class="{\'success-icon\': notification.type ==\'task\' && notification.activity==\'taskcomplete\'}" ><span class="fa" ng-class="{file:\'fa-file\',group:\'fa-users\', task:\'fa-tasks\', network:\'fa-circle\'}[notification.type]"></span></p>\n\
</div>\n\
<div class="notification-message col-lg-10 col-md-10 col-sm-10 col-xs-10">\n\
<p class="p-margin-0"><span ng-bind-html="notification.message"></span><br><span class="time-ago text-muted font-small-1" title="{{notification.time}}"></span></p>\n\
</div>\n\
</a>\n\
</div>\n\
</div>\n\
</div>\n\
<div class="notifications-footer text-right" ><a href="/notifications" class="">See all</a>\n\
</div>\n\
</div>'
    };
});
/* Log Module */

app.controller('logController', ['$rootScope', '$scope', '$http', 'log', function ($rootScope, $scope, $http, log) {
        $rootScope.getLogsData($rootScope.logsLoadedPage + 1);
    }])

app.service('log', function ($rootScope, $http) {
    $rootScope.logsLoadedPage = 0;
    $rootScope.logsMoreOlder = false;
    $rootScope.logs = [];
    $rootScope.getLogsData = function (page, callback) {
        $http({
            method: 'GET',
            url: '/logs/getLogs/' + page + '.json'
        }).success(function (data, header) {
            for (var i in data.data) {
                $rootScope.logs.push(data.data[i]);
            }
            $rootScope.logsMoreOlder = data.moreOlder;
            $rootScope.logsLoadedPage++;
            if (typeof callback === "function") {
                callback();
            }
        })
    };
    $rootScope.getLogsGroup = function (group_id, page, callback) {
        if (group_id && page) {
            $http({
                method: 'GET',
                url: '/logs/getLogsGroup/' + group_id + '/' + page + '.json'
            }).success(function (data, header) {
                $rootScope.logs = data.data;
                $rootScope.logsLoadedPage++;
                if (typeof callback === "function") {
                    callback();
                }
            })
        }
    };
    $rootScope.getLogsFile = function (doc_id, page, callback) {
        if (group_id && page) {
            $http({
                method: 'GET',
                url: '/logs/getLogsDoc/' + doc_id + '/' + page + '.json'
            }).success(function (data, header) {
                $rootScope.logs = data.data;
                $rootScope.logsLoadedPage++;
                if (typeof callback === "function") {
                    callback();
                }
            })
        }
    };
})

/* Log Module End*/

/* Conversation Module */


app.controller('ChatController', ['$rootScope', '$scope', '$sce', '$filter', '$http', 'conversation', 'tags', function ($rootScope, $scope, $sce, $filter, $http, conversation, tags) {
        /* Initialize Data */
        $rootScope.getUsersData(function () {
            $rootScope.getGroupsData(function () {
                $rootScope.tagsLoadData();
                var url = (window.location.pathname).split("/");
                if (url[2] === 'index' && url.length > 3) {
                    $rootScope.convGetData($rootScope.polling, url[3]);
                } else {
                    $rootScope.convGetData($rootScope.polling);
                }
            });
        });
        /* Initialize Data */
    }]);
app.service('conversation', function ($rootScope, $http, $q, createDialog, $timeout, trackEvents) {

    $rootScope.isDataLoading = true;
    $rootScope.convPagesLoaded = 1; //total number of sets of conversation loaded
    $rootScope.tobeopened = null; //conversation to be opened
    $rootScope.convUnreadCount = null;
    //For current conversation
    $rootScope.isAdmin = false;
    $rootScope.convtype = -1;
    //Conversation End
    $rootScope.convSelectedItem = null;
    $rootScope.convSelectedItemIndex = null;
    $rootScope.convData;
    $rootScope.Messagestoload = 20;
    UserConversationsetting_isUnread_true = 0;
    $rootScope.convGetData = function (callback, conversation_id) {
        var url = '/datas/getConversations.json';
        if (conversation_id) {
            url = url + '/' + conversation_id;
            $rootScope.tobeopened = conversation_id;
        }
        $http({
            method: 'GET',
            url: url
        }).success(function (data, header) {
            $rootScope.convLatestMessage = data.data['latestmessage'];
            $rootScope.convTotalPages = data.data['totalsets'];
            $rootScope.conversations = data.data['conversations'];
            if ($rootScope.conversations) {
                $rootScope.convSelectItem($rootScope.conversations[0]['Conversation']['id']);
            }
            $rootScope.convOpen();
            $rootScope.convDataLoaded = true;
            if (typeof callback === "function") {
                callback();
            }
        })
    }

    //Returns Index of group in $rootScope.groups
    $rootScope.convFind = function (conversation_id) {
        for (var i in $rootScope.conversations) {
            if ($rootScope.conversations[i]['Conversation']['id'] == conversation_id) {
                return i;
            }
        }
        return -1;
    }

    $rootScope.convModalCreate = function () {
        $('#convModalCreate').modal('show');
    }

    $rootScope.convModalManage = function () {
        $('#convModalManage').modal('show');
    }
    $rootScope.convModalCreateGroup = function () {
        $('#convModalCreateGroup').modal('show');
    }

    $rootScope.convCreate = function (data) {
        if (data.userlist.length > 0 && data.body != '') {
            var xsrf = $.param({body: data.body, userlist: data.userlist});
            var userlist_size = data.userlist.length;
            $http({
                method: 'POST',
                url: '/conversations/createconversation.json',
                data: xsrf
            }).success(function (data, header) {
                $rootScope.tobeopened = data.data;
                $('#convModalCreate').modal('hide');
                notify(data.message, header);

                trackEvents.trackNewChat('New-Chat', {share_count: userlist_size});

            }).error(function (data, header) {
                notify(data.message, header);
            }).then(function () {
            })
        }
    }

    $rootScope.groupReply = function (data, group_id) {
        if (data.userlist.length > 0 && data.body != '') {
            var xsrf = $.param({body: data.body, userlist: data.userlist});
            $http({
                method: 'POST',
                url: '/messages/groupreply/' + group_id + '.json',
                data: xsrf
            }).success(function (data, header) {
                notify(data.message, header);
            }).error(function (data, header) {
                notify(data.message, header);
            }).then(function () {
                $('#convModalCreateGroup').modal('hide');
            })
        }
    }


    $rootScope.convReply = function (conversation_id, msg) {
        //Check if msg is not empty or undefined and has atleast one character
        if (msg && String(msg).trim().length > 0) {
            $('#sendmessage').button('loading');
            $('#msgreply').attr('readonly', 'true');
            var xsrf = $.param({body: msg});
            $http({
                method: 'POST',
                url: '/messages/reply/' + conversation_id + '.json',
                data: xsrf
            }).success(function (data, header) {
            }).then(function () {
                angular.element(document.querySelector('#msgreply')).val('');
                $rootScope.convData = {};
                $('#msgreply').removeAttr('readonly');
                $('#sendmessage').button('reset');
            });
        }
    }

    $rootScope.convMessagesLoad = function (conversation_id) {
        $('#loadoldermessage').button('loading');
        if (!$rootScope.convSelectedItem['Message']['nomoremessage']) {
            $http({
                method: 'GET',
                url: '/messages/loadmessages/' + conversation_id + '/' + $rootScope.convSelectedItem['Message'][0]['id'] + '/old/' + parseInt($rootScope.Messagestoload + 1)
            }).success(function (data, header) {
                $('#loadoldermessage').button('reset');
                $rootScope.convDataMessageAdd(data.data, conversation_id, 'old');
            });
        }
    };
    $rootScope.convConversationLoadOlder = function (set) {
        $http({
            method: 'Get',
            url: '/conversations/loadmoreconversations/' + set + '.json'
        }).success(function (data, header) {
            $rootScope.convDataAddConversations(data.data, 'old');
            $rootScope.convPagesLoaded++;
        })
    };
    $rootScope.convMemberAdd = function (conversation_id, data) {
        if (data.userlist.length > 0) {
            $rootScope.tags = [];
            $('#convMemberAddbtn').button('loading');
            ;
            var xsrf = $.param({userlist: data.userlist, conversation_id: conversation_id});
            $http({
                method: 'POST',
                url: '/conversations/addparticipants.json',
                data: xsrf
            }).success(function (data, header) {
                $rootScope.convDataModifyUserConversation(data.data, conversation_id);
            }).then(function () {
                $rootScope.convData = {};
                $('#convMemberAddbtn').button('reset');
            })
        }
    };
    $rootScope.convRemoveMember = function (conversation_id, user) {
        var xsrf = $.param({user: user, conversation_id: conversation_id});
        $http({
            method: 'POST',
            url: '/conversations/removeparticipants.json',
            data: xsrf
        }).success(function (data, header) {
            $rootScope.convDataModifyUserConversation(data.data, conversation_id);
        }).then(function () {
        })
    }

    $rootScope.convTagsAutocompleteLoad = function (conversation_id) {
        var convTags = [];
        for (var i in $rootScope.taglist) {
            if ($rootScope.taglist[i]['type'] === 'group') {
                convTags.push({'value': $rootScope.taglist[i].value, 'type': 'group'});
            }
        }
        if (arguments.length > 0) {
            var users = $rootScope.conversations[$rootScope.convFind(conversation_id)]['UserConversation'];
            //To be removed
            for (var i in users) {
                convTags.push({'value': users[i].user_id, 'type': 'user'});
            }
            convTags = $rootScope.getDifference($rootScope.taglist, convTags);
            var deferred = $q.defer();
            deferred.resolve(convTags);
            return deferred.promise;
        } else {
            convTags = $rootScope.getDifference($rootScope.taglist, convTags);
            var deferred = $q.defer();
            deferred.resolve(convTags);
            return deferred.promise;
        }
    }

    $rootScope.convSelectItem = function (conversation_id) {
        var index = $rootScope.convFind(conversation_id);
        if ($rootScope.conversations[index].Message.loaded === null) {
            //Remove messages till now
            ($rootScope.conversations[index].Message).splice(0, 1);
            //Initiate last message
            $rootScope.conversations[index].Message.lastmessage = $rootScope.convLatestMessage;
            //Load messages
            $rootScope.loadmessages(conversation_id, 'old', $rootScope.convLatestMessage, $rootScope.Messagestoload + 1);
            //Set loaded to true
            $rootScope.conversations[index].Message.loaded = true;
        }
        //Check unread
        for (var i in $rootScope.conversations[index].UserConversation) {
            if (($rootScope.conversations[index].UserConversation[i].user_id == $rootScope.loggeduser) && ($rootScope.conversations[index].UserConversation[i].isUnread == UserConversationsetting_isUnread_true)) {
                $rootScope.convSetRead(conversation_id);
            }
        }
        //Set current conversation to this id
        $rootScope.convSelectedItemIndex = index;
        $rootScope.convSelectedItem = $rootScope.conversations[index];
    }

    $rootScope.convSetRead = function (conversation_id) {
        url = '/conversations/setread/' + conversation_id;
        $http({
            method: 'POST',
            url: url
        }).success(function (data, header) {
            $rootScope.convDataModifyUserConversation(data.data, conversation_id);
        })
    };
    $rootScope.polling = function () {
        $http({
            method: 'GET',
            url: '/pollings/notifymessages/' + $rootScope.convLatestMessage
        }).success(function (data, header) {
            //Check status code for No-Content
            if (header !== 204) {
                $rootScope.convLatestMessage = data['latestmessageid'];
                for (var i in data.data) {
                    $rootScope.convDataAddConversations(data.data[i]['data'], 'new');
                }
                //If new create conversation, open it after it is received
                $rootScope.convOpen();
            }
        }).error(function (data, header) {
            notify('Could not load new messages. Please Reload the page.', header);

        }).then(function (data) {
            $timeout(function () {
                $rootScope.polling()
            }, 2000);
        })
    }

    $rootScope.convOpen = function () {
        if ($rootScope.tobeopened == null)
            return false;
        $rootScope.convSelectItem($rootScope.tobeopened);
        $rootScope.tobeopened = null;
    };
    /*Data Management function*/
    //Add new messages.
    $rootScope.convDataMessageAdd = function (data, conversationid, direction) {
        var index = $rootScope.convFind(conversationid);
        if (direction === 'old') {
            //Check for more messages
            if (data.length > $rootScope.Messagestoload) {
                //Remove the additional message
                data.splice(0, 1);
                $rootScope.conversations[index]['Message']['nomoremessage'] = false;
            } else {
                $rootScope.conversations[index]['Message']['nomoremessage'] = true;
            }
            $.each(data, function (key, value) {
                ($rootScope.conversations[index]['Message']).unshift(value);
            })
//            $rootScope.conversations[index]['Message']['lastmessage'] = data[data.length-1]['id'];
        } else if (direction === 'new') {
            $.each(data, function (key, value) {
                ($rootScope.conversations[index]['Message']).push(value);
            })
        }
    };
    $rootScope.convDataAddConversations = function (data, direction) {
        if ($rootScope.conversations == null) {
            $rootScope.conversations = [];
            location.reload();
        }
        if (direction === 'old') {
            $.each(data, function (key, value) {
                ($rootScope.conversations).push(value);
            });
        } else if (direction === 'new') {
            $.each(data, function (key, value) {
                //Check if conversation already exists
                var index = $rootScope.convFind(value.Conversation.id);
                if (index > -1) {
                    $rootScope.conversations[index]['Conversation'] = value.Conversation;
                    $rootScope.conversations[index]['UserConversation'] = value.UserConversation;
                    $rootScope.convDataMessageAdd(value.Message, value.Conversation.id, 'new');
                    $rootScope.conversations.splice(0, 0, $rootScope.conversations.splice(index, 1)[0]);
                } else {
                    ($rootScope.conversations).unshift(value);
                }
            });
        }
    };
    $rootScope.convDataModifyUserConversation = function (data, conversationid) {
        var index = $rootScope.convFind(conversationid);
        $rootScope.conversations[index]['UserConversation'] = data;
    };
    $rootScope.convModalMemberRemove = function (conversation_id, user, user_name) {
        createDialog(null, {
            id: 'convmodalmemberremove',
            title: 'Remove Member',
            deleteleave: 0,
            string: 'Are you sure you want to remove member ' + user_name + ' from conversation ?',
            backdrop: true,
            success: {label: 'Yes, Delete', fn: function () {
                    $rootScope.convRemoveMember(conversation_id, user);
                }},
            cancel: {label: 'Cancel', fn: function () {
                }}
        });
    };
    $rootScope.getConversationUnreadCount = function () {
        $http({
            method: 'GET',
            cache: true,
            url: '/datas/getConversationUnreadCount/'
        }).success(function (data, header) {
            $rootScope.convUnreadCount = data.data;
        })
    }


    $rootScope.convUnreadClass = function (userconversations, user_id) {
        var unread = false;
        angular.forEach(userconversations, function (value, key) {
            if (value.user_id == user_id) {
                if ((value.isUnread ? 1 : 0) == UserConversationsetting_isUnread_true) {
                    unread = true;
                    return;
                }
            }
        })
        return unread;
    }
});
app.filter('reverse', function () {
    return function (items) {
        return items.slice().reverse();
    };
});
app.directive('convNgEnter', function () {
    return function (scope, element, attrs) {
        element.bind("keydown keypress", function (event) {
            if (event.which === 13 && scope.convReplyWithEnter) {
                scope.$apply(function () {
                    scope.$eval(attrs.convNgEnter, {'event': event});
                });
                event.preventDefault();
            }
        });
    };
});
app.filter('convRemoveCurrent', function () {
    return function (items, user_id) {
        if (items == null) {
            return false;
        }
        var arrayToReturn = [];
        var user_idindex;
        for (var i = 0; i < items.length; i++) {
            if (items[i].user_id != user_id) {
                arrayToReturn.push(items[i]);
            } else {
                user_idindex = i;
            }
        }
        if (arrayToReturn.length == 0) {
            arrayToReturn.push(items[user_idindex]);
        }
        return arrayToReturn;
    };
});
app.directive("timeAgo", function ($q) {
    return {
        restrict: "C",
        scope: {
            title: '@'
        },
        link: function (scope, element, attrs) {
            // Using deferred to assert we only initialize timeago() once per
            // directive.
            var parsedDate = $q.defer();
            parsedDate.promise.then(function () {
                jQuery(element).timeago();
            });
            attrs.$observe('title', function (newValue) {
                parsedDate.resolve(newValue);
            });
        }
    };
});
app.filter('convSearch', function ($rootScope) {

    return function (items, searchName) {
        if (items == null) {
            return false;
        }
        if (!searchName)
        {
            return items;
        }
        searchName = searchName.toLowerCase();
        var arrayToReturn = [];
        for (var i = 0; i < items.length; i++) {
            if (items[i].Conversation.mapping === 'group') {
                var name = ($rootScope.groups[$rootScope.findGroup(items[i].Conversation.group_id)]['Group'].name).toLowerCase();
                if (name.indexOf(searchName) != -1)
                {
                    arrayToReturn.push(items[i]);
                    break;
                }
            } else {
                for (var j = 0; j < items[i].UserConversation.length; j++)
                {
                    var name = $rootScope.users[$rootScope.findUser(items[i].UserConversation[j].user_id)]['UserInfo']['name'].toLowerCase();
                    if (name.indexOf(searchName) != -1)
                    {
                        arrayToReturn.push(items[i]);
                        break;
                    }
                }
            }
        }

        return arrayToReturn;
    };
});
app.directive('convOnFinishRender', function ($timeout) {
    return {
        restrict: 'A',
        link: function (scope, element, attr, controller) {
            if (scope.$last === true) {
                $timeout(function () {
                    var dom = $(element[0].parentNode);
                    dom.scrollTop(dom.prop("scrollHeight"));
                });
            }
        }
    }
});
/* Conversation Module End */


//http://fundoo-solutions.github.io/angularjs-modal-service/
//http://www.befundoo.com/blog/angularjs-popup-dialog/
app.factory('createDialog', ["$document", "$compile", "$rootScope", "$controller", "$timeout", "$templateCache", function ($document, $compile, $rootScope, $controller, $timeout, $templateCache) {
        var defaults = {
            id: null,
            template: null,
            title: 'Default Title',
            backdrop: true,
            user_id: null,
            success: {label: 'OK', fn: null},
            cancel: {label: 'Close', fn: null},
            controller: null, //just like route controller declaration
            backdropClass: "modal-backdrop",
            backdropCancel: true,
            myModalClass: 'g-confirm',
            header: true,
            footer: true,
            templateUrl: null,
            modalClass: "modal",
            deleteleave: null /// 0 for delete and 1 for leave
        };
        /* Templates */

        $templateCache.put('User/view.html',
                '<div class="user-info-content text-center">\n\
            <p class="text-center">\n\
            <g-profile-img type="user" path="{{viewUser.vga}}" width="64px" height="64px" myclass="user-info-img font-big-3" name="{{viewUser.name}}" ></g-profile-img>\n\
            </p>\n\
            <h2 class="user-info-name text-center" >{{viewUser.name}}</h2>\n\
            <p ng-show="viewUser.email" > <i class=" text-muted fa fa-envelope-square"></i> &nbsp;&nbsp; {{viewUser.email}}</p>\n\
            <p ng-show="viewUser.skype" ><a class="btn btn-default" href="skype:{{viewUser.skype}}?call"><i class=" text-muted fa fa-skype"></i> {{viewUser.skype}}</a></p>\n\
            <p><span class="" ng-show="viewUser.designation" ><i class="fa fa-briefcase text-muted" ></i>&nbsp;&nbsp;{{viewUser.designation}}</span>\n\
            <span class="" ng-show="viewUser.company" >{{viewUser.company}}</span></p>\n\
            <p class="" ng-show="viewUser.location" ><i class="fa fa-map-marker text-muted"></i>&nbsp;&nbsp;{{viewUser.location}}</p>\n\
            </div>'
                );
        var body = $document.find('body');
        return function Dialog(templateUrl/*optional*/, options, passedInLocals) {

            // Handle arguments if optional template isn't provided.
            if (angular.isObject(templateUrl)) {
                passedInLocals = options;
                options = templateUrl;
            } else {
                options.templateUrl = templateUrl;
            }

            options = angular.extend({}, defaults, options); //options defined in constructor

            var key;
            var idAttr = options.id ? ' id="' + options.id + '" ' : '';
            /* Default Templates */
            var defaultHeader = '<div class=" ' + options.myModalClass + '-header modal-header">\n\
            <button type="button" class="close" ng-click="$modalCancel()">Ã—</button>' + '<h4><span class="' + options.headerIcon + '"></span>Â Â <span  id=>{{$title}}</span></h4></div>';
            var defaultBody = '<div class="' + options.myModalClass + '-body modal-body">' + options.string + '</div>';
            var defaultFooter = '<div class="' + options.myModalClass + '-footer modal-footer"><button class="btn deletebtn" ng-click="$modalSuccess()">{{$modalSuccessLabel}}</button>' + '<button class="btn" ng-click="$modalCancel()">{{$modalCancelLabel}}\n\</button></div>';
            /* Setting Header */
            /*    var headerTemplate = defaultHeader;
             if (options.header)
             {
             headerTemplate = '<div class="modal-footer deletemodalfooter"  ng-include="\'' + options.headerTemplate + '\'"></div>';
             }
             
             /* Setting Footer */
            /*  var footerTemplate = defaultFooter;
             if (options.footer)
             {
             footerTemplate = '<div class="modal-footer deletemodalfooter"  ng-include="\'' + options.footerTemplate + '\'"></div>';
             } */


            if (!options.footer) {
                defaultFooter = '';
            }


            /* Setting Modal Body */
            var modalBody = (function () {
                if (options.string) {
                    if (angular.isString(options.string)) {
                        // Simple string template
                        return defaultBody;
                    } else {
                        // jQuery/JQlite wrapped object
                        return '<div class="modal-body">' + options.template.html() + '</div>';
                    }
                } else {
                    // Template url
                    return '<div class=" ' + options.myModalClass + ' modal-body" ng-include="\'' + options.templateUrl + '\'"></div>'
                }
            })();
            //We don't have the scope we're gonna use yet, so just get a compile function for modal
            var modalEl = angular.element(
                    '<div class="' + options.modalClass + ' fade"' + idAttr + ' style="display: block;">' +
                    '  <div class=" ' + options.myModalClass + ' modal-dialog">' +
                    '    <div class="' + options.myModalClass + '-content modal-content">' +
                    defaultHeader +
                    modalBody +
                    defaultFooter +
                    '    </div>' +
                    '  </div>' +
                    '</div>');
            for (key in options.css) {
                modalEl.css(key, options.css[key]);
            }
            var divHTML = "<div ";
            if (options.backdropCancel) {
                divHTML += 'ng-click="$modalCancel()"';
            }
            divHTML += ">";
            var backdropEl = angular.element(divHTML);
            backdropEl.addClass(options.backdropClass);
            backdropEl.addClass('fade in');
            var handleEscPressed = function (event) {
                if (event.keyCode === 27) {
                    scope.$modalCancel();
                }
            };
            var closeFn = function () {
                body.unbind('keydown', handleEscPressed);
                modalEl.remove();
                if (options.backdrop) {
                    backdropEl.remove();
                }
            };
            body.bind('keydown', handleEscPressed);
            var ctrl, locals,
                    scope = options.scope || $rootScope.$new();
            scope.$title = options.title;
            scope.$modalClose = closeFn;
            scope.$modalCancel = function () {
                var callFn = options.cancel.fn || closeFn;
                callFn.call(this);
                scope.$modalClose();
            };
            scope.$modalSuccess = function () {
                var callFn = options.success.fn || closeFn;
                callFn.call(this);
                scope.$modalClose();
            };
            scope.$modalSuccessLabel = options.success.label;
            scope.$modalCancelLabel = options.cancel.label;
            if (options.controller) {
                locals = angular.extend({$scope: scope}, passedInLocals);
                ctrl = $controller(options.controller, locals);
                // Yes, ngControllerController is not a typo
                modalEl.contents().data('$ngControllerController', ctrl);
            }

            $compile(modalEl)(scope);
            $compile(backdropEl)(scope);
            body.append(modalEl);
            if (options.backdrop)
                body.append(backdropEl);
            $timeout(function () {
                modalEl.addClass('in');
            }, 200);
        };
    }]);
/* Task Module*/
app.factory('taskService', function ($rootScope, $http, $q, $filter, $templateCache, $timeout, createDialog, commentService, trackEvents) {
    /* Task data will stored here */
    $rootScope.TaskData = [];
    $rootScope.TaskfilterOptions = [
        {'name': 'All Task', 'id': 'all'},
        {'name': 'Assigned To Me', 'id': 'assignedToMe'},
        {'name': 'Assigned By Me', 'id': 'assignedByMe'},
//        {'name': 'Betweeen Me and ', 'id': 'BetweenMeAnd'},
        {'name': 'Deadline Missed', 'id': 'DeadlineMissed'},
        {'name': 'Only Completed', 'id': 'Completed'},
        {'name': 'Today\'s tasks', 'id': 'TodaysTask'},
        {'name': 'Only Important', 'id': 'OnlyImportant'},
    ];
    $rootScope.TaskDeskfilterOptions = [
        {'name': 'All Task', 'id': 'all'},
//        {'name': 'Assigned To Me', 'id': 'assignedToMe'},
//        {'name': 'Assigned By Me', 'id': 'assignedByMe'},
//        {'name': 'Betweeen Me and ', 'id': 'BetweenMeAnd'},
        {'name': 'Deadline Missed', 'id': 'DeadlineMissed'},
        {'name': 'Only Completed', 'id': 'Completed'},
        {'name': 'Today\'s tasks', 'id': 'TodaysTask'},
        {'name': 'Only Important', 'id': 'OnlyImportant'},
    ];

    $rootScope.filterBy = $rootScope.TaskfilterOptions[0];
    $rootScope.filterWith = [];
    $rootScope.TaskSelected = {};
    $rootScope.TaskfilterConfig = {
        'hideCompleted': false
    };

    $rootScope.hideCompleted = false;

    $('.task-add-modal').on('hidden.bs.modal', function (e) {

        $("#form-new-task .form-control").each(function () {
            $(this).val(null);
        });
        $rootScope.tags = [];
        $rootScope.TaskSelected = {};
//        $rootScope.taskNewInit();
        $rootScope.$apply();
    });

    $('#singleTaskView').on('hide.bs.modal', function (e) {
        $rootScope.tags = [];
        $rootScope.TaskSelected = {};
        $rootScope.$apply();
    });

    /* Function to get Task Assignment type */
    $rootScope.getTaskType = function (tags)
    {
        var no_group = 0;
        var no_user = -1;
        var type = 0;
        angular.forEach(tags, function (value, key) {

            if (value.type == "user")
            {
                no_user++;
            }
            else if (value.type = "group")
            {
                no_group++;
            }

        });
        no_user = (no_user > 0) ? 1 : 0;
        no_group = (no_group > 0) ? 1 : 0;
        type = no_user + 10 * no_group;
        switch (type) {
            case 00:
                return 'self';
                break;
            case 01:
                return 'user';
                break;
            case 10:
                return 'group';
                break;
            case 11:
                return 'many';
                break;
            default:
                return 'undefined';
                break;
        }
    }
    $rootScope.is_deadline_missed = function (end_date) {
        deadline = new Date(end_date);
        today = new Date();
        if (today > deadline)
            return true;
        else
            return false;
    };
    /* Template Task View*/
    $templateCache.put('Task/view.html',
            "<div class=\"autocomplete\" ng-show=\"suggestionList.visible\"><ul class=\"suggestion-list\"><li class=\"suggestion-item\" ng-repeat=\"item in suggestionList.items track by track(item)\" ng-class=\"{selected: item == suggestionList.selected}\" ng-click=\"addSuggestion()\" ng-mouseenter=\"suggestionList.select($index)\" ng-bind-html=\"highlight(item)\"></li></ul></div>"
            );
    /* function to get current time */
    $rootScope.selectFilter = function (id) {

        switch (id) {
            case 'all':
                $rootScope.filterBy = {'name': 'All Task', 'id': 'all'}
                break;
            case 'assignedToMe':
                $rootScope.filterBy = {'name': 'Assigned To Me', 'id': 'assignedToMe'}
                break;
            case 'assignedByMe':
                $rootScope.filterBy = {'name': 'Assigned By Me', 'id': 'assignedByMe'}
                break;
            case 'BetweenMeAnd':
                $rootScope.filterBy = {'name': '', 'id': 'BetweenMeAnd'}
                break;
            case 'DeadlineMissed':
                $rootScope.filterBy = {'name': 'Deadline Missed', 'id': 'DeadlineMissed'}
                break;
            case 'Completed':
                $rootScope.filterBy = {'name': 'Only Completed', 'id': 'Completed'}
                break;
            case 'TodaysTask':
                $rootScope.filterBy = {'name': 'Today\'s tasks', 'id': 'TodaysTask'}

                break;
            case 'OnlyImportant':
                $rootScope.filterBy = {'name': 'Only Important', 'id': 'OnlyImportant'}

                break;
            default:
                $rootScope.filterBy = {'name': 'All Task', 'id': 'all'}
                break;

        }
    };
    $rootScope.isAssigee = function (task_id, assignee, role)
    {
        var task = $rootScope.TaskData[$rootScope.getById(task_id)];
        var returnStatus = false;
        angular.forEach(task.Tags, function (value, key) {
            if (value.role == role && value.value == assignee)
            {
                returnStatus = true;
                return;
            }
        });
        return returnStatus;
    }

    /*  load task data from server and store to $rootScope.TaskData */
    $rootScope.loadTaskData = function (callback)
    {
        /* Ajax request*/
        $http({
            method: 'POST',
            url: '/tasks/getAssignedTask.json'
        }).success(function (data, header) {
            /* On success set data to TaskData*/
            if (data.data != null)
            {
                $rootScope.TaskData = data.data;
            }
        }).error(function (data, header) {
        })

        if (typeof callback === "function") {
            callback();
        }

    }

    $rootScope.getById = function (id)
    {
        var returnVal = -1;
        $.each($rootScope.TaskData, function (key, value) {

            if (value.Task.id == id)
            {
                returnVal = key;
                return;
            }
        });
        return returnVal;
    }

    $rootScope.taskModalClear = function ()
    {
        $rootScope.TaskSelected = {};
        $rootScope.TaskSelected.Task = {};
        $('.task-add-modal').modal('hide');
        $("#form-new-task .form-control").each(function () {
            $(this).val(null);
        });
    }


    $rootScope.taskNewInit = function ()
    {

        $rootScope.tags = [];
        if (typeof $rootScope.TaskSelected.Task != "undefined")
        {
            var title = angular.copy($rootScope.TaskSelected.Task.title);
            $rootScope.TaskSelected = {};
            $rootScope.TaskSelected.Task = {};
            $rootScope.TaskSelected.Task.title = title;
        }
        else
        {
            $rootScope.TaskSelected = {};
            $rootScope.TaskSelected.Task = {};
        }

//        $rootScope.$apply();
        $('.task-add-modal').modal('hide');
    }

    $rootScope.taskAdd = function (data) {
        if (angular.equals({}, data))
        {
            return;
        }
        if (data.Task.id != null)
        {
            return $rootScope.taskEdit(data);
        }
        if ($('#form-new-task').valid())
        {

            if (typeof data.TaskAssignment == 'undefined' || (data.TaskAssignment.assignee_data).length == 0) {
                delete data.TaskAssignment;
            }
            var data = $.param({data: data});
            $http({
                method: 'POST',
                url: '/tasks/add.json',
                data: data
            }).success(function (data, header) {
                $rootScope.TaskData.push(data.data[0]);
                $rootScope.taskNewInit();
                $rootScope.taskModalClear();
                $rootScope.taskOpData = null;
                notify(data.message, header);
                trackEvents.trackTaskAdd('Task-Add', {title: data.data[0].Task.title, shared_count: data.data[0].Tags.length - 1});

            }).error(function (data, header) {
                notify(data.message, header);
            });
            return true;
        }

        return false;
    };
    
//    Task created by nlp method start
    
    $rootScope.tasknlpAdd = function (data) {
        if (angular.equals({}, data))
        {
            return;
        }
         
          var data = $.param({data: data});
            $http({
                method: 'POST',
                url: '/tasks/add_nlp.json',
                data: data
            }).success(function (data, header) {
                $rootScope.TaskData.push(data.data[0]);
                $rootScope.taskNewInit();
                $rootScope.taskModalClear();
                $rootScope.taskOpData = null;
                notify(data.message, header);
                trackEvents.trackTaskAdd('Task-Add', {title: data.data[0].Task.title, shared_count: data.data[0].Tags.length - 1});
            }).error(function (data, header) {
                notify(data.message, header);
            });
            return true;
        

        return false;
    }

//    Task created by nlp method End

    $rootScope.taskEdit = function (data) {
        if ($('#form-new-task').valid())
        {


            if (typeof data.TaskAssignment == 'undefined' || (data.TaskAssignment.assignee_data).length == 0) {
                delete data.TaskAssignment;
            }
            delete data.Comment;
            task_id = data.Task.id;
            var data = $.param({data: data});
            $http({
                method: 'POST',
                url: '/tasks/edit/' + task_id + '.json',
                data: data
            }).success(function (data, header) {

                index = $rootScope.getById(task_id);
                $rootScope.TaskData[index] = data.data[0];
                $rootScope.taskNewInit();
                $rootScope.taskModalClear();

                notify(data.message, header);
            }).error(function (data, header) {
                notify(data.message, header);
            });
            return true;
        }


        return false;
    }

    $rootScope.taskRemoveAssignee = function (assignee, task_id) {

        var remove_data = {};
        remove_data.value = assignee.value;
        remove_data.type = assignee.type;
        var data = $.param({'data[TaskAssignment][remove_data]': remove_data, 'data[TaskAssignment][task_id]': task_id});
        $http({
            method: 'POST',
            url: '/tasks/removeAssignee/' + task_id + '.json',
            data: data
        }).success(function (data, header) {
            index = $rootScope.getById(task_id);
            $rootScope.TaskData[index] = data.data[0];
            $rootScope.taskSelectItem(task_id);
            notify(data.message, header);

        }).error(function (data, header) {
            notify(data.message, header);

        })
    }

    $rootScope.taskRemoveAssigneeConfirm = function (assignee, task_id) {
        if (assignee.type == 'group')
        {
            message = 'Are you sure you want to remove <strong>' + $rootScope.groups[$rootScope.findGroup(assignee.value)].Group.name + '</strong> ?';
        } else
        {
            message = 'Are you sure you want to remove <strong>' + $rootScope.users[$rootScope.getUserIndex(assignee.value)].UserInfo.name + '</strong> ?';
        }

        createDialog(null, {
            id: 'removeTaskAssignee',
            title: 'Remove Assignee',
            deleteleave: 0,
            header: false,
            footer: true,
            string: message,
            backdrop: true,
            success: {label: 'Yes, Remove', fn: function () {
                    $rootScope.taskRemoveAssignee(assignee, task_id);
                }},
            cancel: {label: 'Cancel', fn: function () {

                }}
        });
    }




    $rootScope.task_completing = null;
    $rootScope.task_uncompleting = null;

    $rootScope.taskSetCompleted = function (task_id, is_completed, $index) {

        var action = 'complete';
        if (is_completed)
        {
            action = 'incomplete';
        }

        url = '/tasks/setComplete/' + task_id + '/' + action + '.json';
        $http({
            method: 'POST',
            url: url
        }).success(function (data, header) {

            //Striking through animation on the element
            if (!is_completed)
            {
                $rootScope.task_completing = $index;
            } else if (is_completed) {
                $rootScope.task_uncompleting = $index;
                console.log($rootScope.task_uncompleting);
            }
            //Timeout for preventing it to disappear before strike
            $timeout(function () {
                index = $rootScope.getById(task_id);
                $rootScope.TaskData[index] = data.data[0];
                $rootScope.task_completing = null;
                $rootScope.task_uncompleting = null;

            }, 500);

            notify(data.message, header);

        }).error(function (data, header) {
            notify(data.message, header);

        })


    }






    $rootScope.taskDelete = function (task_id) {


        createDialog(null, {
            id: 'deleteTask',
            title: 'Task Delete',
            deleteleave: 0,
            string: 'Are you sure you want to delete this Task ?',
            backdrop: true,
            success: {label: 'Yes, Delete', fn: function () {
                    $http({
                        method: 'POST',
                        url: '/tasks/delete/' + task_id + '.json',
                    }).success(function (data, header) {

                        index = $rootScope.getById(task_id);
                        $rootScope.TaskData.splice(index, 1);

                    }).error(function (data, header) {
                        notify(data.message, header);

                    })
                }},
            cancel: {label: 'Cancel', fn: function () {

                }}
        });
    };
    $rootScope.taskLoadTags = function (query) {
    $rootScope.autoCompleteList = $rootScope.taglist;
        var currentTags = [];
        if (typeof $rootScope.TaskSelected.Tags !== 'undefined')
        {


            currentTags = angular.copy($rootScope.TaskSelected.Tags);
            angular.forEach(currentTags, function (value, key) {
                if (value.role === 'admin')
                {
                    currentTags.splice(key, 1);
                }
            });
        }
        $rootScope.autoCompleteList = $rootScope.getDifference($rootScope.taglist, currentTags);
        var deferred = $q.defer();
        deferred.resolve($rootScope.autoCompleteList);
        return deferred.promise;
    };
    $rootScope.taskSelectItem = function (task_id) {
        $rootScope.TaskSelected = angular.copy($rootScope.TaskData[$rootScope.getById(task_id)]);
//        $rootScope.TaskSelected = $rootScope.TaskData[$rootScope.getById(task_id)];
        delete $rootScope.TaskSelected.TaskAssignment;
        var temp = $rootScope.TaskSelected.Task;
        delete $rootScope.TaskSelected.Task;
        var start_date = new Date(moment(temp.start_date));
        var end_date = new Date(moment(temp.end_date));
        $rootScope.TaskSelected.Task = {};
        $rootScope.TaskSelected.Task.id = temp.id;
        $rootScope.TaskSelected.Task.title = temp.title.toString();
        $rootScope.TaskSelected.Task.description = temp.description;
        $rootScope.TaskSelected.Task.start_date = $filter('date')(new Date(moment(start_date)), "M/dd/yyyy h:mm a");
        $rootScope.TaskSelected.Task.end_date = $filter('date')(new Date(moment(end_date)), "M/dd/yyyy h:mm a");
        $rootScope.TaskSelected.Task.priority = (temp.priority == 1) ? true : false;
        $rootScope.TaskSelected.Task.created_by = temp.created_by;
        $rootScope.TaskSelected.Task.comment_count = temp.comment_count;
        $rootScope.tags = [];
        //setting dynamic height if modal window
    }

    $rootScope.getTask = function (id) {
        $http({
            method: 'Post',
            url: '/tasks/getTask/' + id + '.json'
        }).success(function (data, header) {
            $rootScope.TaskData[0] = data.data[0];
            if (typeof callback === "function") {
                callback();
            }
         });
    }

    var fact = {};
    fact.deleteCommentModal = function (id, commentId) {
        createDialog(null, {
            id: 'taskcommentdelete',
            title: 'Delete Comment',
            deleteleave: 0,
            string: 'Are you sure you want to delete comment?',
            backdrop: true,
            success: {label: 'Yes, Delete', fn: function () {
                    fact.deleteComment(id, commentId);
                }},
            cancel: {label: 'Cancel', fn: function () {
                }}
        });
    }

    fact.deleteComment = function (id, commentId) {
        if (commentId) {
            commentService.delete(commentId)
                    .success(function (data, header) {
                        var index = $rootScope.getById(id);
                        $rootScope.TaskData[index]['Task']['comment_count']--;
                        $rootScope.TaskData[index]['Comment'] = ($rootScope.TaskData[index]['Comment']).filter(function (comment) {
                            return comment.id != commentId;
                        });
                        $rootScope.taskSelectItem(id);
//                        notify(data.message, header);
                    })
                    .error(function (data, header) {
                        notify(data.message, header);
                    })
        }
    }

    fact.addComment = function (id, body) {
        if (body && body.trim()) {
            commentService.add('tasks', id, body)
                    .success(function (data, header) {
                        var index = $rootScope.getById(id);
                        $rootScope.TaskData[index]['Task']['comment_count']++;
                        $rootScope.TaskData[index]['Comment'] = ($rootScope.TaskData[index]['Comment']).concat(data.data);
                        $rootScope.taskSelectItem(id);
                    })
                    .error(function (data, header) {
                        notify(data.message, header);
                    })
        }
    }

    fact.getComment = function (id, lastId) {
        if (angular.isUndefined(lastId))
            var lastId = null;

        //Check loadmore variable exists and is false
        var index = $rootScope.getById(id);
        if (angular.isUndefined($rootScope.TaskData[index]['moreComments']) || $rootScope.TaskData[index]['moreComments'] !== false) {
            commentService.get('tasks', id, lastId)
                    .success(function (data, header) {
                        $rootScope.TaskData[index]['Comment'] = ($rootScope.TaskData[index]['Comment']).concat(data.data.comments);
                        $rootScope.TaskData[index]['moreComments'] = data.data['loadMore'];
                        $rootScope.taskSelectItem(id);
                    })
                    .error(function (data, header) {
                        notify(data.message, header);
                    })
        }
    }

    fact.taskCarousel = function (data, index, option) {
        if (index > data.length || index < 0) {
            return;
        } else {
            switch (option) {
                case 'next':
                    if (index < data.length - 1)
                        ++index;
                    break;
                case 'previous':
                    if (index)
                        --index;
                    break;
                default:
                    return;
            }
            $rootScope.taskSelectItem(data[index]['Task']['id']);
            $rootScope.selectedTask(index);
            return;
        }
    }

    /* For expanding the task tile's height on clicking it */
    $rootScope.selectedIndex = null;
    $rootScope.selectedTask = function ($index) {
        if ($index !== $rootScope.selectedIndex) {
            $rootScope.selectedIndex = $index;
        } else {
            //   $rootScope.selectedIndex = null;
        }

    };
    /*---------------------------------------------------*/

    return fact;


});

//signup controller start

app.controller('googleController', ['$rootScope', '$scope', '$http', function ($rootScope, $scope, $http) {
        $scope.googleForm = function (obj) {
            //if direct action doesnot work try this with DOM  
            //Google post here
            var d = new Date();
            var offset = d.getTimezoneOffset();
            var data={};
            data.UserInfo = {};
            data.User = {};
            data.UserInfo.first_name = obj[0];
            data.UserInfo.last_name = obj[1];
            data.User.email = obj[2];
            data.User.password = null;
            data.User.timezone = offset;
//            var form = document.getElementById("UserAddForm");
            
//            $scope.$apply();
            //     form.submit();
//            var formdata = data;
            var xsrf = $.param({data : data});
            
                $http({
                    method: 'POST',
                    url: '/users/add_google/',
                    data: xsrf
                }).success(function (data, header) {
                    if(data.redirect){
                        window.open(data.url,"_self");
                    }
                }).error(function (data, header) {
                    notify(data.message, header);
                });
            
        };

//        $scope.formsubmit = function () {
//            var form = document.getElementById('UserAddForm');
//            form.submit();
//        };

        $scope.login = function ()
        {
            console.log('hii')
            var myParams = {
                'clientid': '664463812333-sourro2d3gm5a790821e4r0tjqrb9be6.apps.googleusercontent.com',
                'cookiepolicy': 'single_host_origin',
                'callback': 'loginCallback',
                'approvalprompt': 'auto',
                'scope': 'https://www.googleapis.com/auth/plus.login https://www.googleapis.com/auth/plus.profile.emails.read https://www.googleapis.com/auth/plus.circles.read https://www.googleapis.com/auth/userinfo.email'
            };
            gapi.auth.signIn(myParams);
        };
        //all the logic after clicking and enetering data into google is inside

        loginCallback = function (result) {
            $scope.loginCallback(result);
        };

        $scope.loginCallback = function (result)
        {
            if (result['status']['signed_in'])
            {
                if(result['status']['method'] == 'PROMPT'){
                var request = gapi.client.plus.people.get(
                        {
                            'userId': 'me'
                        });
//                console.log(request);
                
//                    var request1 = gapi.client.plus.people.list({
//                        'userId': 'me',
//                        'collection': 'visible'
//                    });
//                    request1.execute(function (resp) {
//                        console.log(resp);
//                    });
                
                request.execute(function (resp)
                {
                    var obj = [];
                    obj[0] = resp['name']['givenName'];
                    obj[1] = resp['name']['familyName'];
                    obj[2] = resp['emails'][0].value;
                    $scope.googleForm(obj);
//                     console.log(resp);
                });
                }
            }
        };

        $scope.onLoadCallback = function () {
            gapi.client.setApiKey('AIzaSyCorSP1JpIaUGhVwbXNPKKzf8PT4FUERD8');
            gapi.client.load('plus', 'v1', function () {
            });
//            gapi.client.load('oauth2', 'v2', function() {
//                
//            });
        };

        onLoadCallback = function ()
        {
            $scope.onLoadCallback();
        }

    }]);

//Signup controller end


//change password controller start

app.controller('changepassword', ['$scope','$http' , function($scope,$http){
                $http({
                    method: 'GET',
                    url: '/users/token_check/'
                }).success(function (data, header) {
                    $scope.setPassword = data.token_exists;
                }).error(function (data, header) {
                    notify(data.message, header);
                });
}]);

//change password controller start

/* Task Controller */
app.controller('taskController', ['$rootScope', '$scope', '$http', 'taskService', 'tags', '$filter', function ($rootScope, $scope, $http, taskService, tags, $filter) {
        /*Initialize */
//Get Groups data
        $scope.taskService = taskService;
        var url = (window.location.pathname).split("/");
        if (url[2] === 'view' && url.length > 3) {
            $rootScope.getTask(url[3]);
        } else {
            $rootScope.loadTaskData();
        }
        $rootScope.getGroupsData($rootScope.tagsLoadData);
        $rootScope.getUsersData($rootScope.tagsLoadData);
        var date = new Date();
        var d = date.getDate();
        var m = date.getMonth();
        var y = date.getFullYear();
        /* event source that pulls from google.com */
        $scope.isTaskAdmin = function(id){
            var taskusers = $rootScope.TaskData[$rootScope.getById(id)].TaskAssignment;
            var i;
            for(i=0;i<taskusers.length;i++){
                if(taskusers[i].user_id == $rootScope.loggeduser){
                    if(taskusers[i].role == "admin"){
                        return true;
                    }
                }
                
            }
            return false;
        };
        
        $scope.eventSource = {
            url: "http://www.google.com/calendar/feeds/usa__en%40holiday.calendar.google.com/public/basic",
            className: 'gcal-event', // an option!
            currentTimezone: 'America/Chicago' // an option!
        };
        /* event source that contains custom events on the scope */
//        $scope.events = [
//            {title: 'All Day Event', start: new Date(y, m, 1)},
//            {title: 'Long Event', start: new Date(y, m, d - 5), end: new Date(y, m, d - 2)},
//            {id: 999, title: 'Repeating Event', start: new Date(y, m, d - 3, 16, 0), allDay: false},
//            {id: 999, title: 'Repeating Event', start: new Date(y, m, d + 4, 16, 0), allDay: false},
//            {title: 'Birthday Party', start: new Date(y, m, d + 1, 19, 0), end: new Date(y, m, d + 1, 22, 30), allDay: false},
//            {title: 'Click for Google', start: new Date(y, m, 28), end: new Date(y, m, 29), url: 'http://google.com/'}
//        ];
        $scope.events = [];
        /* event source that calls a function on every view switch */
        $scope.eventsF = function (start, end, callback) {
            var s = new Date(start).getTime() / 1000;
            var e = new Date(end).getTime() / 1000;
            var m = new Date(start).getMonth();
            var events = [{title: 'Feed Me ' + m, start: s + (50000), end: s + (100000), allDay: false, className: ['customFeed']}];
            callback(events);
        };
        $scope.calEventsExt = {
            color: '#f00',
            textColor: 'yellow',
            events: [
                {type: 'party', title: 'Lunch', start: new Date(y, m, d, 12, 0), end: new Date(y, m, d, 14, 0), allDay: false},
                {type: 'party', title: 'Lunch 2', start: new Date(y, m, d, 12, 0), end: new Date(y, m, d, 14, 0), allDay: false},
                {type: 'party', title: 'Click for Google', start: new Date(y, m, 28), end: new Date(y, m, 29), url: 'http://google.com/'}
            ]
        };
        /* alert on eventClick */
        $scope.alertOnEventClick = function (event, allDay, jsEvent, view) {
//            $scope.alertMessage = (event.title + ' was clicked ');
//            console.log(event);
            $rootScope.taskSelectItem(event.id);
            $('.task-add-modal').modal('show');
        };
        //with this you can handle the events that generated by clicking the day(empty spot) in the calendar
        $scope.dayClick = function (date, allDay, jsEvent, view) {
            $scope.$apply(function () {
                $scope.newTaskInit(date);
                $('.task-add-modal').modal('show');
            });
        };

        //for initiallizing the start and end date as well as time slot of 1 hours starting from hours next to current
        $scope.newTaskInit = function (date) {

            //setting start date and end date as the  
            var today = new Date();
            var end_date = new Date();
            var current_time = today.getHours();
            var start_time = 0;
            var end_time = 0;
            //if date is provided then set the date otherwise  take today
            if (date) {
                start_date = date;
                end_date = date;
            } else {
                start_date = end_date = today;
            }
            //before 11pm set next hours
            if (current_time < 23) {

                start_time = current_time + 1;
                end_time = start_time + 1;
                start_date = start_date.setHours(start_time);
                end_date = end_date.setHours(end_time);

            } else {
                //else between 11 to 0;
                start_time = current_time;
                end_time = current_time;

                start_date = start_date.setHours(start_time);
                end_date = end_date.setHours(end_time, 59);

            }

            //setting starting and ending date time

            $rootScope.taskNewInit();
            $rootScope.TaskSelected.Task.start_date = $filter('date')(start_date, "M/dd/yyyy h:mm a");
            $rootScope.TaskSelected.Task.end_date = $filter('date')(end_date, "M/dd/yyyy h:mm a");

        };
        /* alert on Drop */
        $scope.alertOnDrop = function (event, dayDelta, minuteDelta, allDay, revertFunc, jsEvent, ui, view) {
            $scope.alertMessage = ('Event Droped to make dayDelta ' + dayDelta);
            console.log(event);
        };
        /* alert on Resize */
        $scope.alertOnResize = function (event, dayDelta, minuteDelta, revertFunc, jsEvent, ui, view) {
            $scope.alertMessage = ('Event Resized to make dayDelta ' + minuteDelta);
        };
        /* add and removes an event source of choice */
        $scope.addRemoveEventSource = function (sources, source) {
            var canAdd = 0;
            angular.forEach(sources, function (value, key) {
                if (sources[key] === source) {
                    sources.splice(key, 1);
                    canAdd = 1;
                }
            });
            if (canAdd === 0) {
                sources.push(source);
            }
        };
        /* add custom event*/
        $scope.addEvent = function (event) {
            $scope.events.push({
                title: event.Task.title.toString(),
                description: event.Task.description,
                id: event.Task.id,
                start: event.Task.start_date,
                end: event.Task.end_date,
                allDay: false


            });
        };
        /* remove event */
        $scope.remove = function (index) {
            $scope.events.splice(index, 1);
        };
        /* Change View */
        $scope.changeView = function (view, calendar) {
            calendar.fullCalendar('changeView', view);
        };
        /* Change View */
        $scope.renderCalender = function (calendar) {
            if (calendar) {
                calendar.fullCalendar('render');
            }
        };
        /* config object */
        $scope.uiConfig = {
            calendar: {
                height: 450,
                editable: false,
                header: {
                    left: 'title ,prev,next',
                    center: '',
                    right: 'today ,month,agendaWeek,agendaDay'
                },
                buttonIcons: {
                    prev: 'fa fa-check',
                    next: 'fa fa-check',
                },
                theme: false,
                eventClick: $scope.alertOnEventClick,
//                eventDrop: $scope.alertOnDrop,
                eventResize: $scope.alertOnResize,
                dayClick: $scope.dayClick,
            }
        };
        $rootScope.$watchCollection('TaskData', function (v) {

            $scope.events.splice(0, $scope.events.length);
            angular.forEach($rootScope.TaskData, function (value, key) {
                $scope.addEvent(value);
            });
            $scope.$apply;
        });
        $scope.extraEventSignature = function (event) {
            return "" + event.description;
        };
        /* event sources array*/
        $scope.eventSources = [$scope.events]; //, $scope.eventSource, $scope.eventsF];
        //$scope.eventSources = [$rootScope.TaskData]; //, $scope.eventSource, $scope.eventsF];

        //$scope.eventSources2 = [$scope.events];
        if (url[2] === 'view' && url.length > 3) {
        $rootScope.userPromise.then(function(){
            $scope.TaskAdmin = $scope.isTaskAdmin($rootScope.TaskData[0].Task.id);
        });
        }
    }]);

//Data analytics controller

app.controller('analyticscontroller' , ['$rootScope','$scope','$http','$timeout','$q',function($rootScope,$scope,$http,$timeout,$q){
       
        $scope.selected=1; //for group,user,network
        $scope.admingroups=[];
        $scope.networkdata = [];
        $scope.total_task;
        $scope.complete_task;
        $scope.da_imp;
        $scope.dm_imp;
        $scope.da_nimp;
        $scope.dm_nimp;
        $scope.avg_delay;
        $scope.productivity;
        $scope.grp_total_task;
        $scope.grp_complete_task;
        $scope.grp_da_imp;
        $scope.grp_dm_imp;
        $scope.grp_da_nimp;
        $scope.grp_dm_nimp;
        $scope.grp_avg_delay;
        $scope.grp_productivity;
        $scope.network_total_task;
        $scope.network_complete_task;
        $scope.network_da_imp;
        $scope.network_dm_imp;
        $scope.network_da_nimp;
        $scope.network_dm_nimp;
        $scope.network_avg_delay;
        $scope.network_productivity;
        $scope.usernetwork_total_task;
        $scope.usernetwork_complete_task;
        $scope.usernetwork_da_imp;
        $scope.usernetwork_dm_imp;
        $scope.usernetwork_da_nimp;
        $scope.usernetwork_dm_nimp;
        $scope.usernetwork_avg_delay;
        $scope.usernetwork_productivity;
        $scope.networkusers = [];
        
        $scope.generateTopUsers = function(){
            $scope.user1 = $rootScope.users[$rootScope.findUser($scope.topusers[0].User_id)];
            $scope.user2 = $rootScope.users[$rootScope.findUser($scope.topusers[1].User_id)];
            $scope.user3 = $rootScope.users[$rootScope.findUser($scope.topusers[2].User_id)];
            $scope.user_productivity1 = $scope.topusers[0].data.productivity;
            $scope.user_productivity2 = $scope.topusers[1].data.productivity;
            $scope.user_productivity3 = $scope.topusers[2].data.productivity;
        };
        
        $scope.generateTopGroup = function(groups){
            //patch good solution must be found
            if(groups.length>2){
            $scope.group1 = $rootScope.groups[$rootScope.findGroup(groups[0].Group_id)];
            $scope.group2 = $rootScope.groups[$rootScope.findGroup(groups[1].Group_id)];
            $scope.group3 = $rootScope.groups[$rootScope.findGroup(groups[2].Group_id)];
            $scope.group_productivity1 = groups[0].data.productivity;
            $scope.group_productivity2 = groups[1].data.productivity;
            $scope.group_productivity3 = groups[2].data.productivity;
            }
            else if(groups.length>1){
            $scope.group1 = $rootScope.groups[$rootScope.findGroup(groups[0].Group_id)];
            $scope.group2 = $rootScope.groups[$rootScope.findGroup(groups[1].Group_id)];
            $scope.group_productivity1 = groups[0].data.productivity;
            $scope.group_productivity2 = groups[1].data.productivity;
            }
            else{
                $scope.group1 = $rootScope.groups[$rootScope.findGroup(groups[0].Group_id)];
                $scope.group_productivity1 = groups[0].data.productivity;
            }
            $scope.topgrouplength = groups.length;
            
        };
        
        $scope.selectpersonal = function(){
            $scope.selected = 1 ;
            $scope.generateUserchart();
        };
        
        $scope.selectnetowrk = function(){
            $scope.selected = 3;
            $scope.generateNetworkchart();
        };
        
        $scope.selectgroup = function(){
          $scope.selected = 2;
          $scope.initializechart();
        };
        
        $scope.selectindividual = function(){
          $scope.selected = 4;  
        };
       
        $scope.getUserstats = function (){
//             Url method to send id
            var url = '/networks/user_stats';
            var id = $rootScope.loggeduser; 
            url = url+'/'+id;    
            $http({
                method: 'GET',
                url: url
            }).success(function (data, header) {
               $scope.total_task = data.task_total;
               $scope.task_assigned = data.task_assigned;
               $scope.complete_task = data.data.task_complete;
               $scope.incomplete = data.data.task_notcomplete;
               $scope.da_imp = data.data.task_acomplish_imp;
               $scope.dm_imp = data.data.task_missed_imp;
               $scope.da_nimp = data.data.task_acomplish_notimp;
               $scope.dm_nimp = data.data.task_missed_notimp;
               $scope.avg_delay = data.data.Avg_delay;
               $scope.productivity = data.data.productivity;
               $scope.generateUserchart();
               $scope.getNetworkstats();
              // $timeout($scope.generateTopUsers,200);
               //$timeout(function(){$scope.generateUserNetwork($scope.topusers)},1000);
        }).error(function(data,header){
                notify(data.message, header);
            });
            
        };
        //should be called at page loading
        $scope.findGroupDetails = function(){
            $scope.admingroups=[];
            var i;
            var group = null;
            for( i = 0 ; i < $rootScope.groups.length ; i++){
                group = $rootScope.groups[i];
                if(group.Group.admin == $rootScope.loggeduser){
                    $scope.admingroups.push(group);
                }
            }
        };
        
        $scope.getGroupstats = function(id,name){
            $scope.currentgroup = name;
            $scope.selected = 2 ;
            $scope.findGroupDetails();
            var url ='/networks/group_stats/'+id;
            $http({
                method: 'GET',
                url: url
            }).success(function (data, header) {
               $scope.grp_total_task = data.current_Group.total_task;
               $scope.grp_incomplete = data.current_Group.data.task_notcomplete;
               $scope.grp_complete_task = data.current_Group.data.task_complete;
               $scope.grp_da_imp = data.current_Group.data.task_acomplish_imp;
               $scope.grp_dm_imp = data.current_Group.data.task_missed_imp;
               $scope.grp_da_nimp = data.current_Group.data.task_acomplish_notimp;
               $scope.grp_dm_nimp = data.current_Group.data.task_missed_notimp;
               $scope.grp_avg_delay = data.current_Group.data.Avg_delay;
               $scope.grp_productivity = data.current_Group.data.productivity;
               $scope.top_groups = data.Top_groups;
               $scope.generateGroupchart();
               $scope.generateTopGroup($scope.top_groups);
            }).error(function(data,header){
                notify(data.message, header);
            });
        };
                
        $scope.getNetworkstats = function(){
            var url ='/networks/network_stats';
            $http({
                method: 'GET',
                url: url,
            }).success(function (data, header) { 
               $scope.network_total_task = data.current_Network.total_task;
               $scope.network_incomplete = data.current_Network.task_notcomplete;
               $scope.network_complete_task = data.current_Network.task_complete;
               $scope.network_da_imp = data.current_Network.task_acomplish_imp;
               $scope.network_dm_imp = data.current_Network.task_missed_imp;
               $scope.network_da_nimp = data.current_Network.task_acomplish_notimp;
               $scope.network_dm_nimp = data.current_Network.task_missed_notimp;
               $scope.network_avg_delay = data.current_Network.Avg_delay;
               $scope.network_productivity = data.current_Network.productivity;
               $scope.topusers = data.Top_User_Network;
        }).error(function(data,header){
                notify(data.message, header);
            }).then(function(){
             $scope.generateTopUsers();   
            });
            
        };
        
       
        $scope.optionsBar = {
            responsive: true,
            scaleBeginAtZero: true,
            scaleShowGridLines: true,
            scaleGridLineColor: "rgba(0,0,0,.05)",
            scaleGridLineWidth: 1,
            barShowStroke: true,
            barStrokeWidth: 2,
            barValueSpacing: 20,
            barDatasetSpacing: 1,
            legendTemplate: ''
        };
        
        $scope.dataBar = {
            labels: ['Completed', 'Imp deadline achieved','Important deadline missed','Completed non important' ,'Deadline Missed'],
            datasets: [
                {
                    fillColor: "rgba(151,187,205,0.5)",
                    strokeColor: "rgba(151,187,205,0.8)",
                    highlightFill: "rgba(151,187,205,0.75)",
                    highlightStroke: "rgba(151,187,205,1)",
                    data: []
                }
            ]
        };
        
        $scope.usernetworkdataBar = {
            labels: ['Completed', 'Imp deadline achieved','Important deadline missed','Completed non important' ,'Deadline Missed'],
            datasets: [
                {
                    fillColor: "rgba(151,187,205,0.5)",
                    strokeColor: "rgba(151,187,205,0.8)",
                    highlightFill: "rgba(151,187,205,0.75)",
                    highlightStroke: "rgba(151,187,205,1)",
                    data: []
                }
            ]
        };
        
        $scope.optionsDoughnut ={
            responsive: true,
            segmentShowStroke: true,
            segmentStrokeColor: '#fff',
            segmentStrokeWidth: 2,
            percentageInnerCutout: 50, // This is 0 for Pie charts
            animationSteps: 100,
            animationEasing: 'easeOutBounce',
            animateRotate: true,
            animateScale: false,
            legendTemplate: ''  
        };
       
        $scope.dataDoughnut= [
            {
                value: 0,
                color: '#F7464A',
                highlight: '#FF5A5E',
                label: 'Incomplete'
            },
            {
                value: 0,
                color: '#46BFBD',
                highlight: '#5AD3D1',
                label: 'Complete'
            }
        ];
        
        $scope.usernetworkdataDoughnut= [
            {
                value: 0,
                color: '#F7464A',
                highlight: '#FF5A5E',
                label: 'Incomplete'
            },
            {
                value: 0,
                color: '#46BFBD',
                highlight: '#5AD3D1',
                label: 'Complete'
            }
        ];
        $rootScope.makeUserarray = function(){
         var usersArray=[];
         var i;
         for(i=0; i<$rootScope.users.length;i++){
             usersArray.push($rootScope.users[i].UserInfo);
         }
         return usersArray;
        };
        
        $scope.loadanalyticsTags = function(query){
         var defer = $q.defer();
         $scope.currentUsers = $rootScope.makeUserarray();
         defer.resolve($scope.currentUsers);
         return defer.promise;
        };
        
        $scope.findAnalyticById = function (id){
            var i;
            for(i=0;i<$scope.topusers.length;i++){
                if($scope.topusers[i].User_id === id){
                    return $scope.topusers[i];
                }
            }
        };
       
        $scope.userNetworkAutoComplete = function(str){
            $scope.usernetworkplaceholder = str[0].first_name;
            $scope.showusernetworkcharts = true
            var datauserchart=[];
            datauserchart = $scope.findAnalyticById(str[0].user_id);
            $scope.usernetwork_total_task = datauserchart.task_total;
            $scope.usernetwork_task_assigned = datauserchart.task_assigned;
            $scope.usernetwork_complete_task = datauserchart.data.task_complete;
            $scope.usernetwork_incomplete = datauserchart.data.task_notcomplete;
            $scope.usernetwork_da_imp = datauserchart.data.task_acomplish_imp;
            $scope.usernetwork_dm_imp = datauserchart.data.task_missed_imp;
            $scope.usernetwork_da_nimp = datauserchart.data.task_acomplish_notimp;
            $scope.usernetwork_dm_nimp = datauserchart.data.task_missed_notimp;
            $scope.usernetwork_avg_delay = datauserchart.data.Avg_delay;
            $scope.usernetwork_productivity = datauserchart.data.productivity;
            $scope.generateuserNetworkchart();
        };
        
        $scope.initializechart = function(){
            $scope.dataDoughnut[0].value = 0;
            $scope.dataDoughnut[1].value = 0;
            $scope.dataBar.datasets[0].data = [];
        };
        
        $scope.initializeusernetworkchart = function () {
            $scope.usernetworkdataDoughnut[0].value = 0;
            $scope.usernetworkdataDoughnut[1].value = 0;
            $scope.usernetworkdataBar.datasets[0].data = [];
            $scope.usernetwork_productivity = null;
            $scope.usernetwork_avg_delay = null;
        };
        
        $scope.generateUserchart = function(){
            $scope.dataDoughnut[0].value = $scope.incomplete;
            $scope.dataDoughnut[1].value = $scope.complete_task;
            $scope.dataBar.datasets[0].data = [$scope.complete_task,$scope.da_imp,$scope.dm_imp,$scope.da_nimp,$scope.dm_nimp];
        };
        
        $scope.generateGroupchart = function(){
            $scope.dataDoughnut[0].value = $scope.grp_incomplete;
            $scope.dataDoughnut[1].value = $scope.grp_complete_task ;
            $scope.dataBar.datasets[0].data = [$scope.grp_complete_task,$scope.grp_da_imp,$scope.grp_dm_imp,$scope.grp_da_nimp,$scope.grp_dm_nimp];
        };
        
        $scope.generateNetworkchart = function(){
            $scope.dataDoughnut[0].value = $scope.network_incomplete;
            $scope.dataDoughnut[1].value = $scope.network_complete_task ;
            $scope.dataBar.datasets[0].data = [$scope.network_complete_task,$scope.network_da_imp,$scope.network_dm_imp,$scope.network_da_nimp,$scope.network_dm_nimp];
        };
        
        $scope.generateuserNetworkchart = function(){
            $scope.usernetworkdataDoughnut[0].value = $scope.usernetwork_incomplete;
            $scope.usernetworkdataDoughnut[1].value = $scope.usernetwork_total_task ;
            $scope.usernetworkdataBar.datasets[0].data = [$scope.usernetwork_complete_task,$scope.usernetwork_da_imp,$scope.usernetwork_dm_imp,$scope.usernetwork_da_nimp,$scope.usernetwork_dm_nimp];
        };
        $rootScope.userPromise.then(function(){
        $scope.getUserstats();
        $scope.findGroupDetails();
        });
        
}]);

//Data analytics controller End

app.filter('taskSearch', function ($rootScope) {
    return function (items, searchText) {

        $('.task-listing-area').removeHighlight();
        if (items == null) {
            return false;
        }
        if (!searchText)
        {
            return items;
        }

        searchText = searchText.toLowerCase();
        var returnArray = [];
        returnArray.splice(0, returnArray.length);
        angular.forEach(items, function (value, key) {
            /* Uncomment to enable Search for assignee */
//            angular.forEach(value.Tags, function(tag, keyInner) {
//                if(tag.type=='user')
//                {
//                    value.Tags[keyInner].name = $rootScope.users[$rootScope.getUserIndex(tag.value)].UserInfo.name;
//                }
//            });
            if (search(value.Task.title, searchText))
            {
                returnArray.push(items[key]);
            }

        });
        $('.task-listing-area').highlight(searchText);
        return returnArray;
    }



});


app.filter('filterBy', function ($rootScope, $filter) {
    return function (items) {



        if (items == null) {
            return false;
        }
        if ($rootScope.filterBy.id == 'all')
        {

            return items;
        }
        else if ($rootScope.filterBy.id == 'assignedByMe' || $rootScope.filterBy.id == 'assignedToMe')
        {
            var role = 'user';
            if ($rootScope.filterBy.id == 'assignedByMe')
            {
                role = 'admin';
            }
            var returnArray = [];
            angular.forEach(items, function (value, key) {


                if ($rootScope.isAssigee(value.Task.id, $rootScope.loggeduser, role))
                {
                    returnArray.push(items[key]);
                }

            });
            return returnArray;
        }
        /*
         * Shifted to seperate filter 'FilterByAssginee'
         * 
         else if ($rootScope.filterBy.id == 'BetweenMeAnd')
         {
         var returnArray = [];
         var tempKey;
         angular.forEach(items, function (value, key) {
         
         angular.forEach($rootScope.filterWith, function (tag, keyInner) {
         
         if (angular.isObject(findInObjectArray(value.Tags, tag, 'value', 'type')))
         {
         
         if (tempKey != key)
         {
         returnArray.push(items[key]);
         }
         tempKey = key;
         return;
         }
         });
         
         });
         return returnArray;
         return returnArray.uniqueObjects();
         }*/
        else if ($rootScope.filterBy.id == 'DeadlineMissed')
        {
            var returnArray = [];
            var date = new Date();
            angular.forEach(items, function (value, key) {


                if (date > new Date(value.Task.end_date) && value.Task.is_completed == 0)
                {
                    returnArray.push(items[key]);
                }

            });
            return returnArray;
        }
        else if ($rootScope.filterBy.id == 'Completed')
        {
            var returnArray = [];
            angular.forEach(items, function (value, key) {


                if (value.Task.is_completed)
                {
                    returnArray.push(items[key]);
                }

            });
            return returnArray;
        }
        else if ($rootScope.filterBy.id == 'TodaysTask')
        {
            var returnArray = [];
            angular.forEach(items, function (value, key) {

                if ($rootScope.momentDate(value.Task.end_date) == 'Today')
                {
                    returnArray.push(items[key]);
                }


            });
            return returnArray;
        } else if ($rootScope.filterBy.id == 'OnlyImportant')
        {
            var returnArray = [];
            angular.forEach(items, function (value, key) {

                if (value.Task.priority == 1)
                {
                    returnArray.push(items[key]);
                }


            });
            return returnArray;
        }
        else
        {
            return items;
        }

        return false;
    }




});

app.filter('filterIncompleteTasks', function ($rootScope) {
    return function (items) {

        if (!items)
            return false;

        if ($rootScope.filterBy.id != 'Completed')
        {
            var returnArray = [];
            angular.forEach(items, function (value, key) {


                if (!value.Task.is_completed)
                {
                    returnArray.push(items[key]);
                }

            });
            return returnArray;
        }
        else
        {
            return items;
        }

    };
});

app.filter('filterWithAssignee', function ($rootScope, $filter) {
    return function (items) {


        if ($rootScope.filterWith.length == 0)
        {
            return items;
        }

        var returnArray = [];
        var tempKey;
        angular.forEach(items, function (value, key) {

            angular.forEach($rootScope.filterWith, function (tag, keyInner) {

                if (angular.isObject(findInObjectArray(value.Tags, tag, 'value', 'type')))
                {

                    if (tempKey != key)
                    {
                        returnArray.push(items[key]);
                    }
                    tempKey = key;
                    return;
                }
            });

        });
        return returnArray;


    }




});
/* Task Module End*/

app.filter('msgSanitize', function ($sce) {
    return function (msg) {
        return String(msg).replace(/\\r\\n/g, '<br/>');
    }
})

app.filter('trustAsHtml', function ($sce) {
    return function (html) {
        return $sce.trustAsHtml((html).toString());
    }
})

/* Directives */
/* Directives*/
app.directive('datechange', function ($filter) {
    return {
        require: 'ngModel',
        link: function (scope, el, attr, ngModel) {
            $(".date-task").on("dp.change", function (e) {
                var date = $(this).val();
                var id = $(this).attr('id');
                scope.$apply(function () {

                    if (ngModel.$name == id)
                    {
                        ngModel.$setViewValue(date);
                    }
                });
            });
        }
    };
});
/* Angular Custom Listener to bind Modal Hide event*/
app.directive('hidebsmodal', function () {
    return {
        link: function (scope, element, attributes) {
            $('#taskAddModal').on('hidden.bs.modal', function (e) {
                // do something...
            })


            element.find('a').on('click', function () {
                //problematic line HERE
                $(element).trigger("myEvent.sample");
            });
        },
        //some other stuff
    };
});
/*
 This directive allows us to pass a function in on an enter key to do what we want.
 */
app.directive('ngEnter', function () {
    return function (scope, element, attrs) {
        element.bind("keydown keypress", function (event) {
            if (event.which === 13) {
//                scope.$apply(function() {
//                   scope.$eval(attrs.ngEnter);
//                });
                scope.$eval(attrs.ngEnter);
                event.preventDefault();
            }
        });
    };
});
/* Seach in Object */

var comparator = function (obj, text) {
    if (obj && text && typeof obj === 'object' && typeof text === 'object') {
        for (var objKey in obj) {
            if (objKey.charAt(0) !== '$' && hasOwnProperty.call(obj, objKey) &&
                    comparator(obj[objKey], text[objKey])) {
                return true;
            }
        }
        return false;
    }
    text = ('' + text).toLowerCase();
    return ('' + obj).toLowerCase().indexOf(text) > -1;
};
var search = function (obj, text) {
    if (typeof text == 'string' && text.charAt(0) === '!') {
        return !search(obj, text.substr(1));
    }
    switch (typeof obj) {
        case "boolean":
        case "number":
        case "string":
            return comparator(obj, text);
        case "object":
            switch (typeof text) {
                case "object":
                    return comparator(obj, text);
                default:
                    for (var objKey in obj) {
                        if (objKey.charAt(0) !== '$' && search(obj[objKey], text)) {
                            return true;
                        }
                    }
                    break;
            }
            return false;
        case "array":
            for (var i = 0; i < obj.length; i++) {
                if (search(obj[i], text)) {
                    return true;
                }
            }
            return false;
        default:
            return false;
    }
};
/* Highlight */
jQuery.fn.highlight = function (pat) {
    function innerHighlight(node, pat) {
        var skip = 0;
        if (node.nodeType == 3) {
            var pos = node.data.toUpperCase().indexOf(pat);
            if (pos >= 0) {
                var spannode = document.createElement('span');
                spannode.className = 'highlight';
                var middlebit = node.splitText(pos);
                var endbit = middlebit.splitText(pat.length);
                var middleclone = middlebit.cloneNode(true);
                spannode.appendChild(middleclone);
                middlebit.parentNode.replaceChild(spannode, middlebit);
                skip = 1;
            }
        }
        else if (node.nodeType == 1 && node.childNodes && !/(script|style)/i.test(node.tagName)) {
            for (var i = 0; i < node.childNodes.length; ++i) {
                i += innerHighlight(node.childNodes[i], pat);
            }
        }
        return skip;
    }
    return this.length && pat && pat.length ? this.each(function () {
        innerHighlight(this, pat.toUpperCase());
    }) : this;
};
jQuery.fn.removeHighlight = function () {
    return this.find("span.highlight").each(function () {
        this.parentNode.firstChild.nodeName;
        with (this.parentNode) {
            replaceChild(this.firstChild, this);
            normalize();
        }
    }).end();
};
app.filter('bytesConvert', function () {
    return function (num) {
        num = parseInt(num);
        if (num < 1024)
            return num + ' bytes';
        else if (num < 1048576)
            return (num / 1024).toFixed(1) + ' Kb';
        else if (num < 1073741824)
            return (num / 1048576).toFixed(2) + ' Mb';
        else
            return (num / 1073741824).toFixed(2) + ' Gb';
    }
});
app.filter('letterimage', function () {
    return function (text) {
        return intitials_generation(text);
    }
});
// for creating the initials of the user's name and group name from full name
function intitials_generation(name) {
    var init = name.split(" ", 2);
    if (init.length == 1)
        return init[0].substring(0, 1).toUpperCase();
    return init[0].charAt(0).toUpperCase() + init[1].charAt(0).toUpperCase();
};


/*
 * Directive g-profile-img to be used for rendering images of groups and users when they are set or not set
 * Atrributes to be set 
 * 1. type (required) : 'user' or 'group' or 'network'
 * 2. path : path of the image
 * 3. myclass : class to be added
 * 4. width : width of image to be set
 * 5. height : height of image to be set
 * 6. name (required) : name for generating initials
 * 7. my-title : title for the tooltip or arrow hovers
 * <g-profile-img type="user" path='https//gridle/user/8.jpg' myclass='g-myclass' width='20px' height='20px' name="John Doe" my-title="User : John Doe" ></g-profile-img>
 */
app.directive('gProfileImg', function ($compile) {
    return{
        restrict: 'E',
        scope: {
            path: '@',
            name: '@',
            width: '@',
            height: '@',
            myclass: '@',
            type: '@',
        },
        link: function (scope, element, attrs) {


////               defining templates
            //if user image exist
            var user_img = '<img src="' + attrs.path + '" width="' + attrs.width + '" height="' + attrs.height + '" class=" g-user-initial-image p-margin-0 ' + attrs.myclass + '" alt="' + attrs.name + '"/>';
            //if no user image
            var user_initial = '<p width="' + attrs.width + '" height="' + attrs.height + '" class="g-user-initial-img  p-margin-0 center-block ' + attrs.myclass + '">' + intitials_generation(attrs.name) + '</p>';
            //group intitials image
            var group_initial = '<p width="' + attrs.width + '" height="' + attrs.height + '" class="g-group-initial-img p-margin-0 center-block ' + attrs.myclass + '">' + intitials_generation(attrs.name) + '</p>';
            //network initials image
            var network_initial = '<p width="' + attrs.width + '" height="' + attrs.height + '" class="g-group-initial-img p-margin-0 center-block ' + attrs.myclass + '" >' + intitials_generation(attrs.name) + '</p>';
            //selecting template
            var getTemplate = function (attrs) {
                var template = '';
                switch (attrs.type) {
                    case 'user' :
                        if (attrs.path.indexOf('/default') > -1 || attrs.paths)
                        {
                            template = user_initial;
                        } else {
                            template = user_img;
                        }
                        break;
                    case 'group' :
                        template = group_initial;
                        break;
                    case 'network' :
                        template = network_initial;
                        break;
                    default :
                        template = user_initial;
                }
                return template;
            };

            attrs.$observe("path", function (value) {
                element[0].innerHTML = " ";
                var e = angular.element(getTemplate(attrs));
                $compile(e.contents())(scope);
                element.append(e);
            });
            /*
             * element.append(getTemplate(attrs));
             * attrs.$observe();
             * element.replaceWith(getTemplate(attrs));
             * var e = angular.element(getTemplate(attrs));
             * $compile(e.contents())(scope);
             * element.replaceWith(e);
             */
        }
    };
});


app.directive('tooltip', function () {
    return {
        restrict: 'A',
        link: function (scope, element) {
            $(element).hover(function () {
                // on mouseenter
                $(element).tooltip('show');
            }, function () {
                // on mouseleave
                $(element).tooltip('hide');
            });

        }
    };
});

app.service('trackEvents', function ($rootScope) {

    var ms = this;

    var isIntercom = false;

    //tracking events in intercom
    if (typeof Intercom !== 'undefined') {
        isIntercom = true;
    }
    //when a network is created
    $('#btn-network-add').click(function () {
        var network_name = document.getElementById('networkname').value;
        ms.trackNetworkAdd('Network-add', {network_name: network_name});
    });

    //when members are invited to network
    $('#btn-network-add-member').click(function () {
        ms.trackNetworkAddMember('Network-Invite', {network_name: $rootScope.networks[$rootScope.currentnetwork].name, new_member_count: $('#formNetworksInvite .tag-item').size()});
    });

    //clicking on tour
    $('#btn-tour-desk').click(function () {
        ms.trackTour('Tour-desk');
    });



    this.trackNetworkAdd = function (custom_event, meta_data) {
        if (isIntercom) {
            Intercom('trackEvent', custom_event, meta_data);
        }

    };

    this.trackNetworkAddMember = function (custom_event, meta_data) {
        if (isIntercom) {
            Intercom('trackEvent', custom_event, meta_data);
        }
    };

    this.trackTaskAdd = function (custom_event, meta_data) {
        if (isIntercom) {
            Intercom('trackEvent', custom_event, meta_data);
        }
    };

    this.trackTeamAdd = function (custom_event, meta_data) {
        if (isIntercom) {
            Intercom('trackEvent', custom_event, meta_data);
        }
    };

    this.trackTeamMemberAdd = function (custom_event, meta_data) {
        if (isIntercom) {
            Intercom('trackEvent', custom_event, meta_data);
        }
    };

    this.trackFileUpload = function (custom_event, meta_data) {
        if (isIntercom) {
            Intercom('trackEvent', custom_event, meta_data);
        }
    };

    this.trackNewChat = function (custom_event, meta_data) {
        if (isIntercom) {
            Intercom('trackEvent', custom_event, meta_data);
        }
    };

    this.trackTour = function (custom_event) {
        if (isIntercom) {
            Intercom('trackEvent', custom_event);
        }
    };

});
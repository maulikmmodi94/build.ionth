<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

App::uses('ModelBehavior', 'Model');
/**
 * Short description for class.
 *
 * @package		plugins.comments
 * @subpackage	models.behaviors
 */
CakePlugin::load('Utils');

class BlackHoleException extends Exception {
    
}

class NoActionException extends Exception {
    
}

class CommentableBehavior extends ModelBehavior {

    /**
     * Settings array
     *
     * @var array
     */
    public $settings = array();

    /**
     * Default settings
     *
     * @var array
     */
    public $defaults = array(
        'commentModel' => 'Comments.Comment',
    );

    /**
     * Setup
     *
     * @param Model $model
     * @param array $settings
     */
    public function setup(Model $model, $settings = array()) {
        if (!isset($this->settings[$model->alias])) {
            $this->settings[$model->alias] = $this->defaults;
        }

        if (!is_array($settings)) {
            $settings = (array) $settings;
        }

        $this->settings[$model->alias] = array_merge($this->settings[$model->alias], $settings);
        $this->bindCommentModel($model);
    }

    /**
     * Binds the comment and the current model 
     *
     * @param Model $model
     * @return void
     */
    public function bindCommentModel(Model $model) {
        $config = $this->settings[$model->alias];

        if (!empty($config['commentModel']) && is_array($config['commentModel'])) {
            $model->bindModel(
                    array(
                'hasMany' => array(
                    'Comment' => $config['commentModel']
                )
                    ), false
            );
        } else {
            $model->bindModel(
                    array(
                'hasMany' => array(
                    'Comment' => array(
                        'className' => $config['commentModel'],
                        'foreignKey' => 'entity_id',
                        'conditions' => array('Comment.isDeleted' => NOT_DELETED),
                        'fields' => '',
                        'dependent' => true,
                        'order' => 'id DESC',
                        'limit' => Comments_to_load,
                        'offset' => '',
                        'exclusive' => '',
                        'finderQuery' => '',
                        'counterQuery' => ''
                    )
                )
                    ), false
            );
        }

        $model->Comment->bindModel(array(
            'belongsTo' => array(
                $model->alias => array(
                    'className' => $model->name,
                    'foreignKey' => 'entity_id',
                    'conditions' => array('Comment.isDeleted' => NOT_DELETED),
                    'fields' => '',
                    'counterCache' => true,
                    'dependent' => false)
            )
                ), false
        );
    }

    /**
     * Prepare models association to before fetch data
     *
     * @param Model $model
     * @param array $options
     * @return boolean
     */
    public function commentBeforeFind(Model $model, $options) {
        extract($options);
        $model->Behaviors->disable('Containable');
        $model->Comment->Behaviors->disable('Containable');
        $unbind = array();

        foreach (array('belongsTo', 'hasOne', 'hasMany', 'hasAndBelongsToMany') as $assocType) {
            if (!empty($model->Comment->{$assocType})) {
                $unbind[$assocType] = array();
                foreach ($model->Comment->{$assocType} as $key => $assocConfig) {
                    $keep = false;
                    if (!empty($options['keep']) && in_array($key, $options['keep'])) {
                        $keep = true;
                    }
//                    if (!in_array($key, array($userModel, $model->alias)) && !$keep) {
                    if (!in_array($key, array($model->alias)) && !$keep) {
                        $unbind[$assocType][] = $key;
                    }
                }
            }
        }
        if (!empty($unbind)) {
            $model->Comment->unbindModel($unbind, false);
        }

        $model->Comment->belongsTo[$model->alias]['fields'] = array($model->primaryKey);
        if (isset($id)) {
//            $conditions[$model->alias . '.' . $model->primaryKey] = $id;
            $conditions[$model->Comment->alias . '.entity_id'] = $id;
            $conditions[$model->Comment->alias . '.model'] = $model->alias;
        }
        $model->Comment->recursive = 0;

        $model->Behaviors->enable('Containable');
        $model->Comment->Behaviors->enable('Containable');

        return array('conditions' => $conditions);
    }

    /**
     * Increment or decrement the comment count cache on the associated model
     *
     * @param Model 		$Model     Model to change count of
     * @param mixed         $id        The id to change count of
     * @param string        $direction 'up' or 'down'
     * @return null
     */
    public function changeCommentCount(Model $Model, $id = null, $direction = 'up') {
        if ($Model->hasField('comments')) {
            if ($direction == 'up') {
                $direction = '+ 1';
            } elseif ($direction == 'down') {
                $direction = '- 1';
            } else {
                $direction = null;
            }

            $Model->id = $id;
            if (!is_null($direction) && $Model->exists(true)) {
                return $Model->updateAll(
                                array($Model->alias . '.comments' => $Model->alias . '.comments ' . $direction), array($Model->alias . '.id' => $id));
            }
        }
        return false;
    }

    /**
     * Delete comment
     *
     * @param Model $Model
     * @param string $commentId
     * @return boolean
     */
    public function commentDelete(Model $Model, $commentId = null) {
        return $Model->Comment->delete($commentId);
    }

    /**
     * Load comments
     *
     * @param Model $Model
     * @param array $modelName  name of the model
     * @param array $entityId   Id of the model
     * @param array $options    extra information and comment statistics
     * @return mixed            fetched Comments
     */
    public function commentLoad(Model $Model, $modelName, $entityId, $options = array()) {
        return $Model->Comment->load($modelName, $entityId, $options);
    }

    /**
     * Get comments
     *
     * @param Model $Model
     * @param mixed $id single Id or array of Ids
     * @return mixed fetched Comments
     */
    public function commentGet(Model $Model, $id = null) {
        return $Model->Comment->get($id);
    }

    /**
     * Handle adding comments
     *
     * @param Model $Model     Object of the related model class
     * @param mixed $commentId parent comment id, 0 for none
     * @param array $options   extra information and comment statistics
     * @throws BlackHoleException
     * @return boolean
     */
    public function commentAdd(Model $Model, $options = array()) {
//        $this->log("in commentadd");
//        $this->log($options);
        extract($options);
        $Model->Comment->recursive = -1;
        if (!empty($data)) {
            $data['Comment']['user_id'] = $userId;
            $data['Comment']['model'] = $modelName;
            if (!isset($data['Comment']['entity_id'])) {
                $data['Comment']['entity_id'] = $modelId;
            }
            $data['Comment']['network_id'] = $networkId;

            $event = new CakeEvent('Behavior.Commentable.beforeCreateComment', $Model, $data);
            CakeEventManager::instance()->dispatch($event);
            if ($event->isStopped() && !$event->result) {
                return false;
            } elseif ($event->result) {
                $data = $event->result;
            }
            $Model->Comment->create($data);

            if ($Model->Comment->save()) {
                $id = $Model->Comment->id;
                $data['Comment']['id'] = $id;
                $Model->Comment->data[$Model->Comment->alias]['id'] = $id;
                $this->changeCommentCount($Model, $modelId);                
                $event = new CakeEvent('Behavior.Commentable.afterCreateComment', $Model, $data);
                CakeEventManager::instance()->dispatch($event);
                if ($event->isStopped() && !$event->result) {
                    return false;
                }

                return $id;
            } else {
                return false;
            }
        }
        return null;
    }

}

<?php

/**
 * Comments
 *
 * Copyright 2014, Pivoting Softwares Pvt Ltd
 * @package   plugins.comments
 */

/**
 * Short description for class.
 *
 * @package		plugins.comments
 * @subpackage	plugins.comments.config.schema
 */
class CommentSchema extends CakeSchema {

    /**
     * Name
     *
     * @var string
     */
    public $name = 'Comments';

    /**
     * Before callback
     *
     * @param string Event
     * @return boolean
     */
    public function before($event = array()) {
        return true;
    }

    /**
     * After callback
     *
     * @param string Event
     * @return boolean
     */
    public function after($event = array()) {
        return true;
    }

    /**
     * Schema for taggeds table
     *
     * @var array
     */
    public $comments = array(
        'id' => array('type' => 'integer', 'null' => false, 'length' => 9, 'key' => 'primary'),
        'user_id' => array('type' => 'integer', 'null' => false, 'length' => 9, 'key' => 'index'),
        'model' => array('type' => 'string', 'null' => false, 'default' => NULL),
        'entity_id' => array('type' => 'integer', 'null' => false, 'length' => 9),
        'network_id' => array('type' => 'integer', 'null' => false, 'length' => 9),
        'body' => array('type' => 'text', 'null' => true, 'default' => NULL),
        'isDeleted' => array('type' => 'boolean', 'null' => false, 'default' => 0),
        'deleted' => array('type' => 'datetime', 'null' => true, 'default' => NULL),
        'created' => array('type' => 'datetime', 'null' => false),
        'indexes' => array(
            'PRIMARY' => array('column' => 'id', 'unique' => 1),
            'BY_MODEL' => array('column' => 'model', 'unique' => 0),
            'BY_ENTITY_ID' => array('column' => 'entity_id', 'unique' => 0)
        )
    );

}

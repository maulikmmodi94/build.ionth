<?php

/**
 * Copyright 2009 - 2013, Cake Development Corporation (http://cakedc.com)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright 2009 - 2013, Cake Development Corporation (http://cakedc.com)
 * @license MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
App::uses('CommentsAppController', 'Comments.Controller');

/**
 * Comments Controller
 *
 * @package comments
 * @subpackage comments.controllers
 *
 * @property Comment $Comment
 * @property PrgComponent $Prg
 * @property SessionComponent  $Session
 * @property RequestHandlerComponent $RequestHandler
 * @property CommentsComponent  $Comments
 */
class CommentsController extends CommentsAppController {

    /**
     * Name
     *
     * @var string
     */
    public $name = 'Comments';

    /**
     * Components
     *
     * @var array
     */
    public $components = array(
        'Paginator',
        'Comments.Comments' => array(
            'active' => false,
        ),
    );

    /**
     * Uses
     *
     * @var array
     */
    public $uses = array(
        'Comments.Comment'
    );

    /**
     * Delete action
     *
     * @param string UUID
     */
    public function delete($id = null) {
        $this->request->onlyAllow('post', 'delete');
        $this->viewClass = 'Json';
        $this->Comment->id = $id;
        $this->Comment->recursive = -1;
        $user_id = $this->Auth->user('id');
        $comment = $this->Comment->findById($id, array('user_id', 'model'));
        if (empty($comment) || !$comment['Comment']['user_id'] || $comment['Comment']['user_id'] !== $user_id) {
            throw new Exception('Invalid Comment to delete', 403);
        } elseif ($this->Comment->deleteComment($id, false)) {
            $this->Comments->flash(__d('comments', 'Comment deleted'));
            $returnObject = array('message' => 'Comment deleted');
            $this->set('returnObject', $returnObject);
            return;
        }
        throw new Exception('Unable to delete comment.Please try again later.', 403);
    }

}

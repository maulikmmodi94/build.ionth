<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

App::uses('Component', 'Controller');

class CommentsComponent extends Component {

    /**
     * Components
     *
     * @var array $components
     */
    public $components = array(
        'Paginator',
        'Auth',
        'Session'
    );

    /**
     * Name
     *
     * @var string
     */
    public $name = 'Comments';

    /**
     * Uses
     *
     * @var array
     */
    public $uses = array(
        'Comments.Comment'
    );

    /**
     * Name of actions this component uses
     *
     * @var array $commentActions
     */
    protected $commentActions = array(
        'commentAdd', 'commentDelete', 'commentLoad'
    );

    /**
     * Name of actions this component should use
     *
     * Customizable in beforeFilter()
     *
     * @var array $actionNames
     */
    public $actionNames = array(
        'view', 'comments'
    );

    /**
     * Name of action to be performed for response after action :
     * 
     * 'afterAdd' - on successful comment add
     * 
     * Customizable in beforeFilter()
     *
     * @var array $responseActions
     */
    public $responseActions = array();

    /**
     * Actions used for deleting of some model record, which doesn't use SoftDelete
     * (so we want comments delete directly)
     *
     * Causes than Comment association will NOT be automatically unbind()ed,
     * independently on $this->unbindAssoc
     *
     * Customizable in beforeFilter()
     *
     * @var array $deleteActions
     */
    public $deleteActions = array();

    /**
     * Name of 'commentable' model
     *
     * Customizable in beforeFilter(), or default controller's model name is used
     *
     * @var string Model name
     */
    public $modelName = null;

    /**
     * Name of association for comments
     *
     * Customizable in beforeFilter()
     *
     * @var string Association name
     */
    public $assocName = 'Comment';

    /**
     * Flag if this component should permanently unbind association to Comment model in order to not
     * query model for not necessary data in Controller::view() action
     *
     * Customizable in beforeFilter()
     *
     * @var boolean
     */
    public $unbindAssoc = false;

    /**
     * Parameters passed to view
     *
     * @var array
     */
    public $commentParams = array();

    /**
     * Name of view variable which contains model data for view() action
     *
     * Needed just for PK value available in it
     *
     * Customizable in beforeFilter(), or default Inflector::variable($this->modelName)
     *
     * @var string
     */
    public $viewVariable = 'returnObject';

    /**
     * Enabled
     *
     * @var boolean $enabled
     */
    public $active = true;

    /**
     * Constructor.
     *
     * @param ComponentCollection $collection
     * @param array               $settings
     * @return CommentsComponent
     */
    public function __construct(ComponentCollection $collection, $settings = array()) {
        parent::__construct($collection, $settings);
        foreach ($settings as $setting => $value) {
            if (isset($this->{$setting})) {
                $this->{$setting} = $value;
            }
        }
    }

    /**
     * Initialize Callback
     *
     * @param Controller $controller
     * @return void
     */
    public function initialize(Controller $controller) {
        $this->Controller = $controller;
        $this->modelName = $controller->modelClass;
        $this->modelAlias = $controller->{$this->modelName}->alias;
        $controller->helpers = array_merge($controller->helpers, array('Comments.Cleaner'));
        if (!$controller->{$this->modelName}->Behaviors->attached('Commentable')) {
            $controller->{$this->modelName}->Behaviors->attach('Comments.Commentable');
        }
    }

    /**
     * Callback
     *
     * @param Controller $controller
     * @return void
     */
    public function startup(Controller $controller) {
        $controller = $this->Controller;
        if (in_array($controller->action, $this->deleteActions)) {
            $controller->{$this->modelName}->{$this->assocName}->softDelete(false);
        } elseif ($this->unbindAssoc) {
            foreach (array('hasMany', 'hasOne') as $assocType) {
                if (array_key_exists($this->assocName, $controller->{$this->modelName}->{$assocType})) {
                    $controller->{$this->modelName}->unbindModel(array($assocType => array($this->assocName)), false);
                    break;
                }
            }
        }
        if ($this->active && in_array($this->Controller->request->params['action'], $this->commentActions)) {
            $type = $this->commentActions();
            $this->_sendResponse();
        }
    }

    /**
     * Callback
     *
     * @param Controller $controller
     * @return void
     */
    public function beforeRender(Controller $controller) {
        try {
            
        } catch (BlackHoleException $exception) {
            return $this->Controller->blackHole($exception->getMessage());
        } catch (NoActionException $exception) {
            
        }
    }

    /**
     * Deletes comments
     *
     * @param string $modelId
     * @param string $commentId
     * @return void
     */
    public function callback_delete($modelId, $commentId) {
        if ($this->Controller->{$this->modelName}->commentDelete($commentId)) {
            $this->flash(__d('comments', 'The Comment has been deleted.'));
        } else {
            $this->flash(__d('comments', 'Error appear during comment deleting. Try later.'));
        }
        $this->redirect();
    }

    /**
     * Flat representation of the comment data.
     *
     * @param array $options
     * @return array
     */
    public function callback_fetchDataFlat($options) {
        $paginate = $this->_prepareModel($options);
        $overloadPaginate = !empty($this->Controller->paginate['Comment']) ? $this->Controller->paginate['Comment'] : array();
        $this->Controller->Paginator->settings['Comment'] = array_merge($paginate, $overloadPaginate);
        $data = $this->Controller->Paginator->paginate($this->Controller->{$this->modelName}->Comment);
        return $data;
    }

    /**
     * Handles controllers actions like list/add related comments
     *
     * @param string $displayType
     * @param bool   $processActions
     * @throws RuntimeException
     * @return void
     */
    public function callback_view() {
        if (!isset($this->Controller->{$this->modelName}) ||
                (!array_key_exists($this->assocName, array_merge($this->Controller->{$this->modelName}->hasOne, $this->Controller->{$this->modelName}->hasMany)))) {
            throw new RuntimeException('CommentsComponent: model ' . $this->modelName . ' or association ' . $this->assocName . ' doesn\'t exist');
        }

        $primaryKey = $this->Controller->{$this->modelName}->primaryKey;

        if (empty($this->Controller->viewVars[$this->viewVariable][$this->Controller->{$this->modelName}->alias][$primaryKey])) {
            throw new RuntimeException('CommentsComponent: missing view variable ' . $this->viewVariable . ' or value for primary key ' . $primaryKey . ' of model ' . $this->modelName);
        }
        $id = $this->Controller->viewVars[$this->viewVariable][$this->Controller->{$this->modelName}->alias][$primaryKey];
    }

    public function commentActions() {
        if (empty($this->Controller->params['pass'])) {
            throw new RuntimeException('CommentsComponent: missing parameter id of model ' . $this->modelName);
        }
        $id = $this->Controller->params->pass[0];        
        $network_id = $this->Session->read('CurrentNetwork');
        $displayType = 'flat';
        $user = $this->_getUser();
        switch ($this->Controller->params['action']) {
            case 'commentAdd':
                $options = compact('id', 'user','network_id');
                if ($this->_call('isUserPart', array($options))) {
                    $this->_call('add', array($id, $displayType));
                }
                break;
            case 'commentLoad':
                $data['lastComment'] = isset($this->Controller->params->pass[1]) ? @$this->Controller->params->pass[1] : null;
                $options = compact('id', 'user','network_id');
                if ($this->_call('isUserPart', array($options))) {
                    $this->_call('load', array($id, $data));
                }
                break;
            default:
                break;
        }
    }

    /**
     * Prepare model association to fetch data
     *
     * @param array $options
     * @return boolean
     */
    protected function _prepareModel($options) {
        $params = array(
//            'isAdmin' => $this->Auth->user('is_admin') == true,
//            'userModel' => $this->userModel,
//            'userData' => $this->Auth->user()
        );
        return $this->Controller->{$this->modelName}->commentBeforeFind(array_merge($params, $options));
    }

    /**
     * Determine used type of display (flat/threaded/tree)
     *
     * @return string Type of comment display
     */
    public function callback_initType() {
        return 'flat';
    }

    /**
     * Call action from component or overridden action from controller.
     *
     * @param string $method
     * @param array  $args
     * @throws BadMethodCallException
     * @return mixed
     */
    protected function _call($method, $args = array()) {
        $methodName = 'callback_comments' . Inflector::camelize(Inflector::underscore($method));
        $localMethodName = 'callback_' . $method;
        if (method_exists($this->Controller, $methodName)) {
            return call_user_func_array(array($this->Controller, $methodName), $args);
        } elseif (method_exists($this, $localMethodName)) {
            return call_user_func_array(array($this, $localMethodName), $args);
        } else {
            throw new BadMethodCallException();
        }
    }

    /**
     * Handle adding comments
     *
     * @param integer $modelId
     * @param integer $commentId Parent comment id
     * @param string  $displayType
     * @param array   $data
     */
    public function callback_add($modelId, $displayType, $data = array()) {
        if (!is_null($this->Controller->data['body'])) {
            $data['Comment']['body'] = $this->cleanHtml($this->Controller->data['body']);
            $modelName = $this->Controller->{$this->modelName}->alias;
            if (!empty($this->Controller->{$this->modelName}->fullName)) {
                $modelName = $this->Controller->{$this->modelName}->fullName;
            }
            $options = array(
                'userId' => $this->Auth->user('id'),
                'modelId' => $modelId,
                'modelName' => $modelName, //		
                'data' => $data,
                'networkId' => $this->Session->read('CurrentNetwork')
            );
            $result = $this->Controller->{$this->modelName}->commentAdd($options);
            if (!is_null($result)) {
                if ($result) {
                    try {
                        $options['commentId'] = $result;
                        $this->_call('afterAdd', array($options));
                    } catch (BadMethodCallException $exception) {
                        
                    }
                } else {
                    throw new RuntimeException('The Comment could not be saved. Please, try again.');
                }
            }
        } else {
            throw new RuntimeException('The Comments data is missing in controller.');
        }
    }

    public function callback_load($modelId, $options = array()) {
        $modelName = $this->Controller->{$this->modelName}->alias;
        if (!empty($this->Controller->{$this->modelName}->fullName)) {
            $modelName = $this->Controller->{$this->modelName}->fullName;
        }
        $result = $this->Controller->{$this->modelName}->commentLoad($modelName, $modelId, $options);
        if ($result) {
            $this->_setResponse('Comments loaded successfully.', $result);
        } else {
            $this->flash(__d('comments', 'The Comment could not be saved. Please, try again.'));
        }
    }

    /**
     * Non view action process method
     *
     * @param array
     * @return boolean
     */
    protected function _processActions($options) {
        extract($options);
        if (isset($this->Controller->passedArgs['comment'])) {
            if ($this->allowAnonymousComment || $this->Auth->user()) {
                
            } else {
                $this->Controller->Session->write('Auth.redirect', $this->Controller->request['url']);
                $this->Controller->redirect($this->Controller->Auth->loginAction);
            }
        }
    }

    /**
     * Wrapping method to clean incoming html contents
     *
     * @deprecated This is going to be removed in the near future
     * @param string $text
     * @param string $settings
     * @return string
     */
    public function cleanHtml($text, $settings = 'full') {
        App::import('Helper', 'Comments.Cleaner');
        $cleaner = & new CleanerHelper(new View($this->Controller));
        return $cleaner->clean($text, $settings);
    }

    /**
     * Flash message - for ajax queries, sets 'messageTxt' view vairable,
     * otherwise uses the Session component and values from CommentsComponent::$flash.
     *
     * @param string $message The message to set.
     * @return void
     */
    public function flash($message) {
        $isAjax = TRUE;
        if ($isAjax) {
            $this->_setResponse($message);
        } else {
            $this->Controller->Session->setFlash($message, $this->flash['element'], $this->flash['params'], $this->flash['key']);
        }
    }

    /**
     * Redirect
     * Redirects the user to the wanted action by persisting passed args excepted
     * the ones used internally by the component
     *
     * @param array $urlBase
     * @return void
     */
    public function redirect($urlBase = array()) {
//        $isAjax = isset($this->Controller->params['isAjax']) ? $this->Controller->params['isAjax'] : false;
        $isAjax = true;
        $url = array_merge(array_diff_key($this->Controller->passedArgs, array_flip($this->_supportNamedParams)), $urlBase);
        if ($isAjax) {
            $this->Controller->set('redirect', $url);
        } else {
            $this->Controller->redirect($url);
        }
        if ($isAjax) {
            $this->ajaxMode = true;
            $this->Controller->set('ajaxMode', true);
        }
    }

    /**
     * Prepare passed parameters.
     *
     * @return void
     */
    public function callback_prepareParams() {
        $this->commentParams = array_merge($this->commentParams, array(
            'viewComments' => $this->viewComments,
            'modelName' => $this->modelAlias
        ));
        $allowedParams = array('comment', 'comment_action', 'quote');
        foreach ($allowedParams as $param) {
            if (isset($this->Controller->passedArgs[$param])) {
                $this->commentParams[$param] = $this->Controller->passedArgs[
                        $param];
            }
        }
    }

    public function callback_afterAdd($options) {
        extract($options);
        if (!empty($this->responseActions['afterAdd'])) {
            $response = $this->Controller->{$this->modelName}->{$this->responseActions['afterAdd']}($modelId);
        } else {
            $response = $this->Controller->{$this->modelName}->commentGet($commentId);
        }
        $this->UserInfo = ClassRegistry::init('UserInfo');
        $response[0]['user_info'] = $this->UserInfo->findByUser_id($response[0]['user_id']);
        $response[0]['user_info'] = $response[0]['user_info']['UserInfo'];
        
        $this->_setResponse("Comment Added Successfully", $response);
    }

    public function callback_isUserPart($options) {
        if (!isset($this->responseActions['isUserPart']))
            return true;
        if (!$this->Controller->{$this->modelName}->{$this->responseActions['isUserPart']}($options)) {
            $this->flash(__d('comments', 'You are not authorized for this action.'));
            return false;
        }
        return true;
    }

    protected function _setResponse($message = null, $data = null) {
        $returnObject = array(
            'message' => $message,
            'data' => $data
        );
        $this->Controller->set($this->viewVariable, $returnObject);
    }

    protected function _sendResponse() {
        $returnObject = $this->Controller->viewVars[$this->viewVariable];
        $this->Controller->response->body(json_encode($returnObject, JSON_NUMERIC_CHECK));
        $this->Controller->response->send();
        $this->_stop();
        $this->response->send();
    }

    protected function _getUser() {
        return $this->Auth->user('id');
    }

}

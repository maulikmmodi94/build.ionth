<?php
/**
 * Like Model
 *
 */
class Like extends AppModel {
	
	public $validate = array(
        'user_id' => array(
            'rule1' => array(
                'rule' => 'notEmpty',
                'message' => 'User Id can not be empty'
            ),
            'rule2' => array(
                'rule' => 'numeric',
                'message' => 'User Id can only be numeric'
            )
        ),
        'network_id' => array(
            'rule1' => array(
                'rule' => 'notEmpty',
                'message' => 'Feed must belong to a Network and Network Id can not be empty'
            ),
            'rule2' => array(
                'rule' => 'numeric',
                'message' => 'Network Id can only be numeric'
            )
        ),                
        'created' => array(
            'rule1' => array(
                'rule' => 'notEmpty',
                'created time can not be empty'
            ),
            'rule2' => array(
                'rule' => 'datetime',
                'message' => 'Please give a valid date and time.'
            )
        ),
    );
    
    
    public function __construct( $id = false, $table = NULL, $ds = NULL ){
		$this->belongsTo['User'] = array(
			'className' => Configure::read('Plugin.Like.user.model'),
			'foreignKey'=> 'user_id',
		);
		parent::__construct($id,$table,$ds);
	}
}

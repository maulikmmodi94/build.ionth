<?php
$this->layout = 'gridle_in';
$this->set('title_for_layout', 'Profile Settings');
echo $this->Html->script('http://maps.googleapis.com/maps/api/js?sensor=false&amp;libraries=places');
echo $this->Html->script('jquery.geocomplete');

echo $this->Html->script('bootstrap-datetimepicker');
echo $this->Html->css('bootstrap-datetimepicker');

if (isset($UserInfo['UserInfo']['dob'])) {
    $UserInfo['UserInfo']['dob'] = date("m/d/Y", strtotime($UserInfo['UserInfo']['dob']));
     
}
?>
<div class="container-fluid">
    <div class="g-settings-wrap row">
        <!-- SETTINGS PAGE MENU  -->
        <div class="dashboard-menu-setting settings-nav col-lg-offset-1 col-lg-3 col-md-offset-1 col-md-3 col-sm-4 col-xs-4">
            <a href="/users/edit" class="profle-settings-btn">
                <div class='dashboard-menu-item-setting active'>
                    <p class='p-margin-0'>Edit profile</p>
                </div>    
            </a>
            <a href="/users/change_password" class="change-pass-btn">
                <div class='dashboard-menu-item-setting'>
                    <p class='p-margin-0'>Change password</p>
                </div>
            </a>
        </div>
        <div class="dashboard-setting col-lg-4 col-lg-offset-1 col-md-4 col-md-offset-1 col-sm-6 col-xs-8">
            <div class="settings-edit-profile-area container-fluid">
                <p class='text-muted text-center font-big-2'>Update your profile information</p
                <!--  PREVIEW DISPLAY PICTURE     -->
                <p id='profile-image-preview' class="edit-profile-img-preview center-block " style="background-image: url('<?php echo $UserInfo['UserInfo']['xvga']; ?>')"></p>
                <p id='btn-upload-photo' class="text-muted text-center btn-link  ">Click here to upload photo</p>
                <!--    EDIT PROFILE FORM   -->
                <?php
                echo $this->Form->create($model, array('type' => 'file', 'class' => 'form-edit-profile form-form',
                    'name' => 'formEditProfile',
                    'role' => 'form',
                    'novalidate',
                ));
                ?>
                <?php
                //profile photo
                echo $this->Form->input('UserInfo.photo', array('type' => 'file', 'required' => false, 'label' => FALSE, 'class' => 'upload-photo-input', 'onchange' => 'imagePreview(this)'));
                ?>
                <div class='form-group'>
                    <div class="col-lg-5 padding-lr-0 col-md-5 ">
                        <div class='form-group'>
                            <!--    FIRST NAME      -->
                            <?php
                            echo $this->Form->input('UserInfo.first_name', array('value' => $UserInfo['UserInfo']['first_name'], 'class' => 'input-sm form-control', 'label' => array('class' => 'g-form-label text-muted'), 'div' => FALSE,'placeholder'=>'First name', 'required', 'ng-minlength' => 2, 'ng-maxlength' => 30, 'ng-pattern' => '/^[a-z ]*$/i', 'maxlength' => 30));
                            ?>
                            <div class="form-inline-error text-warning" ng-show="formEditProfile['data[UserInfo][first_name]'].$dirty">
                                <p ng-show="formEditProfile['data[UserInfo][first_name]'].$error.required" class="p-margin-0">Please fill your first name</p>
                                <p ng-show="formEditProfile['data[UserInfo][first_name]'].$error.minlength" class="p-margin-0">Minimum 2 carachters are required.</p>
                                <p ng-show="formEditProfile['data[UserInfo][first_name]'].$error.maxlength" class="p-margin-0">First name too long.</p>
                                <p ng-show="formEditProfile['data[UserInfo][first_name]'].$error.pattern" class="p-margin-0">You can use only alphabets and spaces. </p>
                            </div>
                        </div>
                    </div>
                    <!--    LAST NAME      -->
                    <div class="form-group col-lg-offset-1 col-lg-6 col-md-offset-1 col-md-6  padding-lr-0 ">
                        <?php echo $this->Form->input('UserInfo.last_name', array('value' => $UserInfo['UserInfo']['last_name'], 'class' => 'input-sm form-control', 'label' => array('class' => 'g-form-label text-muted'), 'div' => FALSE)); ?>

                    </div>
                </div>
                <?php
                //Email
                echo $this->Form->input('UserInfo.email', array('value' => $UserInfo['UserInfo']['email'], 'disabled' => true, 'class' => 'input-sm form-control', 'required' => false, 'label' => array('class' => 'g-form-label text-muted'), 'div' => array('class' => 'form-group')));
                //Skype
                echo $this->Form->input('UserInfo.skype', array('value' => $UserInfo['UserInfo']['skype'],'class' => 'input-sm form-control', 'required' => false, 'label' => array('class' => 'g-form-label text-muted'), 'div' => array('class' => 'form-group')));
                //LOCATION
                echo $this->Form->input('UserInfo.location', array('id' => 'geocomplete', 'value' => $UserInfo['UserInfo']['location'], 'class' => 'input-sm form-control', 'required' => false, 'label' => array('class' => 'g-form-label text-muted'), 'div' => array('class' => 'form-group')));
                //DESIGNATION
                echo $this->Form->input('UserInfo.designation', array('value' => $UserInfo['UserInfo']['designation'], 'class' => 'input-sm form-control', 'required' => FALSE, 'label' => array('class' => 'g-form-label text-muted'), 'div' => array('class' => 'form-group')));
                //COMPANY
                echo $this->Form->input('UserInfo.company', array('value' => $UserInfo['UserInfo']['company'], 'class' => 'input-sm form-control', 'required' => FALSE, 'label' => array('class' => 'g-form-label text-muted'), 'div' => array('class' => 'form-group')));
                //DOB
//                echo $this->Form->input('UserInfo.dob', array(
//                    'dateFormat' => 'DMY',
//                    'minYear' => date('Y') - 100,
//                    'maxYear' => date('Y') - 13,
//                    'selected' => $UserInfo['UserInfo']['dob'],
//                    'class' => 'col-lg-2 form-control',
//                    'label' => array('text' => 'Date of birth', 'class' => 'g-form-label text-muted'),
//                    'div' => array('class' => 'form-group')
//                ));
//
//                echo $this->Form->input('UserInfo.dob', array(
//                    'type' => 'text',
//                    'id' => 'dob',
//                    'value' => $UserInfo['UserInfo']['dob'],
//                    'class' => 'col-lg-2 col-md-2 form-control',
//                    'label' => array('text' => 'Date of birth', 'class' => 'g-form-label text-muted'),
//                    'div' => array('class' => 'form-group')
//                ));


                //SAVE BUTTON
                echo '<div class="row"><div class="col-lg-offset-4 col-lg-4 col-md-offset-4 col-md-4 padding-lr-0">';
                echo $this->Form->submit(__d('users', 'Save'), array('class' => 'btn btn-success btn-sm btn-block', 'div' => array('class' => 'btn-profile-save form-group')));
                echo '</div></div>';
                echo $this->Form->end();
                ?>
            </div>
        </div>
    </div>
</div>
<script>
    //browsing photo on clicking photo
    $('#btn-upload-photo').click(function() {
        $('#UserInfoPhoto').click();
    });

    //image preview 
    function imagePreview(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                document.getElementById('profile-image-preview').style.backgroundImage = "url('" + e.target.result + "')";
            }
            reader.readAsDataURL(input.files[0]);
        }
    }


    $(function() {

        // Geo Complete
        $("#geocomplete").geocomplete()
                .bind("geocode:result", function(event, result)
        {
            console.log("Result: " + result.formatted_address);
        })
                .
                bind("geocode:error", function(event, status)
        {
            console.log("ERROR: " + status);
        })
                .
                bind("geocode:multiple", function(event, results)
        {
            console.log("Multiple: " + results.length + " results found");
        });

        $("#find").click(function()
        {
            $("#geocomplete").trigger("geocode");
        });


        $("#examples a").click(function()
        {
            $("#geocomplete").val($(this).text()).trigger("geocode");
            return false;
        });

    });

    $(function() {
        $('#dob').datetimepicker({
            pickTime: false
        });
    });

</script>

<?php
$this->layout = 'gridle_in';
$this->set('title_for_layout', 'Password Settings');
?>
<div class="container-fluid">
    <div class="g-settings-wrap row">
        <!-- SETTINGS PAGE MENU  -->
        <div class="dashboard-menu-setting settings-nav col-lg-offset-1 col-lg-3 col-md-offset-1 col-md-3 col-sm-4 col-xs-4">
            <a href="/users/edit" class="profle-settings-btn">
                <div class='dashboard-menu-item-setting'>
                    <p class='p-margin-0'>Edit profile</p>
                </div>    
            </a>
            <a href="/users/change_password" class="change-pass-btn">
                <div class='dashboard-menu-item-setting active'>
                    <p class='p-margin-0'>Change password</p>
                </div>
            </a>
        </div>
        <div class="dashboard-setting g-settings-expand col-lg-3 col-lg-offset-1 col-md-5 col-md-offset-1 col-sm-6 col-xs-8">
            <div class="settings-edit-profile-area container-fluid">
                <div class="users form change-pass-form-area">
                    <p class='text-muted text-center font-big-2'>Set a new password for your account.</p>
                    <?php
                    echo $this->Form->create($model, array('action' => 'change_password', 'class' => 'form-form form-change-pass', 'role' => 'form'));
                    echo '<div ng-hide = "setPassword">';
                    echo $this->Form->input('old_password', array(
                        'label' => array('text' => 'Current password', 'class' => 'g-form-label text-muted'),'type' => 'password' ,'class' => 'form-control input-sm', 'div' => array('class' => 'form-group')));
                    echo '</div>';
                    echo $this->Form->input('new_password', array('label' => array('text' => 'New password', 'class' => 'g-form-label text-muted'),'type' => 'password','class' => 'form-control input-sm', 'div' => array('class' => 'form-group')));
                    echo $this->Form->input('confirm_password', array(
                        'label' => array('text' => 'Confirm new password', 'class' => 'g-form-label text-muted'),'type' => 'password','class' => 'form-control input-sm', 'div' => array('class' => 'form-group')));
                    echo '<div class="row"><div class=" col-lg-offset-2 col-lg-8 col-md-offset-2 col-md-8">';
                    echo $this->Form->submit(__d('users', 'Set new password'), array('class' => 'btn btn-success btn-block btn-sm ', 'div' => array('class' => 'form-group')));
                    echo'</div></div>';
                    echo $this->Form->end();
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
$this->layout = 'gridle_out_new';
$this->set('title_for_layout', 'Reset Password | Build');
$brand = Configure::read('brand');
?>
<meta content="width=device-width, initial-scale=1, maximum-scale=1" name="viewport">

<div class="container-fluid backc">
    <div class="row header">
<div class="container-fluid backc">
    <div class="row">
        <div class="col-sm-4 col-xs-3 r"><div class="hr"></div>
            <hr class="hrr">
        </div>
        <div class="col-sm-4 col-xs-6 buttonview">
            Reset Password
        </div>
        <div class="col-sm-4 col-xs-3 l"><div class="hr"></div>
            <hr class="hrl">
        </div>
    </div>

    <div class="row">
        <div class="col-sm-offset-4 col-sm-4 col-xs-offset-2 col-xs-8">
            <div class="loginbox">
                <?php
            echo $this->Form->create($model, array(
                'name'=>'resetPassword',
                'url' => array(
                    'action' => 'reset_password',$token),
                    'class'=>'form-reset-pass',
                    'role'=>'form','inputDefaults'=>array('label'=>false)
                    ));?>
                <div class="col-sm-offset-1 col-sm-10">
                    <div class="form-group">
                <p class="fp" style="line-height: 1.42857143; margin-top:10px !important;">We got your back! Here, just set your new password</p> 
                    </div>
                </div>
                <div class="col-sm-offset-1 col-sm-10">
               <div class="form-group">
               <h6 class="be">If this doesn't solve your problem, please mail us at hello@ionth.com</h6>
              </div>
                </div>
                <div class="col-sm-offset-1 col-sm-10">
                <div class="form-group">
<!--              <input name="data[User][email]" class="form-control input-sm" placeholder="Your registered email address here" autofocus="autofocus" maxlength="320" id="UserEmail" required="required" type="email"/>-->
                   <?php echo $this->Form->input('new_password', array('type' => 'password', 
                'class'=>'form-control','placeholder'=>'Write your new password here.',
                'div'=>'form-group','autofocus'));
                   ?>
                </div>
                </div>
                <div class="col-sm-offset-1 col-sm-10 form-group">
                  <a href="/login">
<!--                      <button type="submit" class="btn logins2" ng-disabled="myForm.email.$dirty && myForm.email.$invalid"><font color="white">Reset Password</font></button>-->
<?php
echo $this->Form->submit(__d('users', 'Reset password'), array('class'=>'btn logins2'));
?>
                  </a>
              </div>
                <?php
                echo $this->Form->end();
                ?>

<!--                <script>
                    var app = angular.module('myApp', []);
                    app.controller('validateCtrl', function ($scope) {
                        $scope.user = 'John Doe';
                        $scope.email = 'john.doe@gmail.com';
                    });
                </script>-->

                <div class="col-sm-offset-1 col-sm-10">
                    <div class="form-group">
                        <h6 class="be">You will be directed to the login page where you've to enter the new password</h6>
                    </div>
                </div>
                <div class="col-sm-offset-1 col-sm-10">
                    <div class="form group">
                        <hr class="hrb">
                    </div>
                </div>
                <div class="row fontbottom">
                    <div class="col-sm-offset-1 col-sm-10">
                        <a href="/login"><h6>No I remember it, take me to the login page</h6></a>
                    </div>
                </div>
                
            </div>
        </div>

    </div>
</div>
    </div>
</div>
<?php $this->layout='ajax';?>
<div class="users form">
    <?php echo $this->Form->create($model); 
        $this->Form->unlockField('email');
    ?>
    <fieldset>
        <tags-input ng-model="tags" display-property="email" placeholder="Add Member" allowed-tags-pattern='^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$'>
        </tags-input>
        <legend><?php echo __d('users', 'Add Users'); ?></legend>
        <div class="input text required" ng-repeat="tag in tags">
            <label for="email{{$index+1}}"></label>
            <input name="data[email][{{$index+1}}]" type="hidden" id="email{{$index+1}}" ng-value="tag.email" required="required"></input>
        </div>
    </fieldset>
    <?php echo $this->Form->end('Submit'); ?>
</div>
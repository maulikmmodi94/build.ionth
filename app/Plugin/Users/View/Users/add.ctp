<?php
$this->layout = 'gridle_out_new';
$this->set('title_for_layout', 'Signup | Build ');
$brand = Configure::read('brand');
?>
<meta content="width=device-width, initial-scale=1, maximum-scale=1" name="viewport">
<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet" type="text/css">
<script src="https://apis.google.com/js/client:platform.js?onload=render" async defer></script>
<script>
//    function render() {
//        gapi.signin.render('customBtn', {
//            //'callback': 'signinCallback',
//            'clientid': '841077041629.apps.googleusercontent.com',
//            'cookiepolicy': 'single_host_origin',
//            'requestvisibleactions': 'http://schema.org/AddAction',
//            'scope': 'https://www.googleapis.com/auth/plus.login'
//        });
//    }
</script>
<style type="text/css">
    #customBtn {
        display: inline-block;
        background: #dd4b39;
        color: white;
        width: 110px;
        height:40px;
        border-radius: 5px;
        white-space: nowrap;
    }
    #customBtn:hover {
        background: #e74b37;
        cursor:pointer;
    }
    span.label {
        font-weight: bold;
    }
    span.icon {
        background: url('/img/google/googleplus.jpg');
        display: inline-block;
        vertical-align: middle;
        width: 40px;
        height: 40px;
        border-right: #bb3f30 1px solid;
        border-top-left-radius: 5px;
        border-bottom-left-radius: 5px;
    }
    span.buttonText {
        display: inline-block;
        text-align: center;
        padding-left: 11px;
        padding-right: 35px;
        font-size: 14px;
        font-weight: bold;
        /* Use the Roboto font that is loaded in the <head> */
        font-family: 'Roboto',arial,sans-serif;
    }

</style>
<div class="container-fluid backc">
    <div class="row">
        <div class="col-sm-4 col-xs-3 r">
            <hr class="hrr"/>
        </div>
        <div class="col-sm-4 col-xs-6 buttonview-loginpage">
            Give Build a try today!
        </div>
        <div class="col-sm-4 col-xs-3 l">
            <hr class="hrl"/>
        </div>
    </div>


    <!--From create-->
    <!--<form class="horizontal" name="login" method="post" action="_blank">-->
    <?php
    echo $this->Form->create($model, array('type' => 'file',
        'class' => 'horizontal',
        'novalidate',
        'role' => 'form',
        'name' => 'formSignUp',
        'inputDefaults' => array('label' => false,)));
    
    echo $this->Form->hidden('User.timezone', array('hiddenField' => false, 'type' => 'number'));
    $this->Form->unlockField('User.timezone');
    echo $this->Form->hidden('User.type', array('type' => 'text','value'=>'gridle'));
    ?>
    <div class="row">
        <div class="col-sm-offset-4 col-sm-4 col-xs-offset-2 col-xs-8" ng-controller="googleController">
            <div class="loginbox">
                <div class="col-sm-offset-1 col-sm-10">
                    <div class="margin-bottom-ten">
                        <p class="fp">Do more things in less time and enjoy the finer things in life.</p>
                    </div>
                </div>

                <!--First Name-->
                <div class="col-sm-offset-1 col-sm-10">
                    <div class="margin-bottom-ten">
                        <h6 class="be">Something that your colleagues call you.</h6>
                        <div class="row">
                            <div class="col-sm-6">

                                <?php

                                echo $this->Form->input('UserInfo.first_name', array('type' => 'text',
                                    'class' => 'form-control',
                                    'placeholder' => 'First name',
                                    'div' => FALSE,
                                    'required',
                                    'ng-minlength' => 2,
                                    'ng-maxlength' => 30,
                                    'ng-pattern' => '/^[a-z. ]*$/i',
                                    'maxlength' => 30,
                                    'ng-model' => 'data.UserInfo.first_name',
                                    'autofocus'));
                                ?>

                                <div class="text-align-left show-error-message font-small-4" ng-show="formSignUp['data[UserInfo][first_name]'].$dirty">
                                    <p ng-show="formSignUp['data[UserInfo][first_name]'].$error.required" class=" p-margin-0">First name please</p>
                                    <p ng-show="formSignUp['data[UserInfo][first_name]'].$error.minlength" class=" p-margin-0">Min. 2 characters</p>
                                    <p ng-show="formSignUp['data[UserInfo][first_name]'].$error.maxlength" class=" p-margin-0">First name too long.</p>
                                    <p ng-show="formSignUp['data[UserInfo][first_name]'].$error.pattern" class=" p-margin-0">Alphabets only</p>
                                </div>

                            </div>
                            <div class="col-sm-6">

                                <?php
                                echo $this->Form->input('UserInfo.last_name', array('type' => 'text',
                                    'class' => 'form-control',
                                    'placeholder' => 'Last name',
                                    'div' => FALSE,
                                    'required',
                                    'ng-minlength' => 2,
                                    'ng-maxlength' => 30,
                                    'ng-pattern' => '/^[a-z. ]*$/i',
                                    'maxlength' => 30,
                                    'ng-model' => 'data.UserInfo.last_name'));
                                ?>

                                <div class="text-align-left show-error-message font-small-4" ng-show="formSignUp['data[UserInfo][last_name]'].$dirty">
                                    <p ng-show="formSignUp['data[UserInfo][last_name]'].$error.required" class=" p-margin-0">Last name please</p>
                                    <p ng-show="formSignUp['data[UserInfo][last_name]'].$error.minlength" class=" p-margin-0">Min. 2 characters</p>
                                    <p ng-show="formSignUp['data[UserInfo][last_name]'].$error.maxlength" class=" p-margin-0">Last name too long.</p>
                                    <p ng-show="formSignUp['data[UserInfo][last_name]'].$error.pattern" class=" p-margin-0">Alphabets only</p>
                                </div>  

                            </div>
                        </div>
                    </div>
                </div>				
                <div class="col-sm-offset-1 col-sm-10">
                    <div class="margin-bottom-ten">
                        <h6 class="be">Something to get in touch with.</h6>
                        <?php
                        $email = isset($this->request->data['User']['email']) ? $this->request->data['User']['email'] : null;
                        ?>
                        <!--    EMAIL   ADDRESS     -->

                        <div class="margin-bottom-ten" ng-init="data.User.email = '<?= $email; ?>'">
        <!--                                <input type="email" class="form-control" name="email" id="email" placeholder="Your Email" ng-model="email" required>-->
                            <?php
                            if ($email) {
                                echo $this->Form->input('User.email', array('readonly', 'type' => 'email', 'error' => array('isValid' => __d('users', 'Must be a valid email address'), 'isUnique' => __d('users', 'An account with that email already exists')), 'class' => 'form-control', 'placeholder' => 'Email address', 'div' => FALSE, 'ng-pattern' => '/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/', 'ng-model' => 'data.User.email', 'required'));
                            } else {
                                echo $this->Form->input('User.email', array('type' => 'email', 'error' => array('isValid' => __d('users', 'Must be a valid email address'), 'isUnique' => __d('users', 'An account with that email already exists')), 'class' => 'form-control', 'placeholder' => 'Email address', 'div' => FALSE, 'ng-pattern' => '/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/', 'ng-model' => 'data.User.email', 'required'));
                            }
                            ?>

                            <div ng-show="formSignUp['data[User][email]'].$dirty" class="text-align-left show-error-message font-small-4" >
                                <p ng-show="formSignUp['data[User][email]'].$error.required" class=" p-margin-0" >Please fill your email address.</p>
                                <p ng-show="formSignUp['data[User][email]'].$error.pattern" class=" p-margin-0">Write your email address</p>
                            </div>

                        </div>

                    </div>

                </div>

                <!--Password-->

                <div class="col-sm-offset-1 col-sm-10">

                    <div class="margin-bottom-ten">
                        <h6 class="be">Easy to remember, hard to guess.</h6>


                        <!--                            <div class="margin-bottom-ten">
                                                       <input type="password" class="form-control" name="password" id="password" placeholder="Password" ng-model="password" required>-->
                        <?php echo $this->Form->input('password', array('type' => 'password', 'class' => 'form-control', 'placeholder' => 'Password', 'div' => FALSE, 'required', 'ng-minlength' => 6, 'ng-model' => 'data.User.password', 'ng-hide' => 'showpassword')); ?>

                        <div ng-show="formSignUp['data[User][password]'].$dirty" class="text-align-left show-error-message font-small-4" >
                            <p ng-show="formSignUp['data[User][password]'].$error.required" class=" p-margin-0" >Enter a combination of atleast 6 numbers, letters and punctuation marks.</p>
                            <p ng-show="formSignUp['data[User][password]'].$error.minlength" class=" p-margin-0">Min. 6 characters</p>
                        </div>

                        <!--                           <div class="input-group-addon font-small-1 hidden-xs ">
                                                        <label class="p-margin-0" style="font-weight:normal">
                                                            <input type="checkbox" ng-model="showpassword" ng-checked="false" >&nbsp;&nbsp;Show</label>
                                                    </div>
                                                    <div class="input-group input-group-sm" ng-show="showpassword">
                                                            <input class="form-control"  type="text" ng-model="data.User.password" ng-show="showpassword" placeholder="Password" />
                                                            <div class="input-group-addon font-small-1 " >
                                                                <label class="p-margin-0" style="font-weight:normal">
                                                                    <input type="checkbox" ng-model="showpassword" ng-checked="false" >&nbsp;&nbsp;Show</label>
                                                            </div>
                                                        </div>-->
                                    <!--                                <span style="color:red" ng-show="myForm.password.$dirty && myForm.password.$invalid">
                                                                        <span ng-show="myForm.password.$error.required">Password is required.</span>
                                                                    </span>
                                                        <br>-->
                    </div>

                </div>

                <!--                        <div class="col-sm-offset-1 col-sm-10">
                                            <div class="form group">
                                                <input type="text" class="form-control" name="window" id="window"><br>
                                            </div>
                                        </div>	-->
                <div class="col-sm-offset-1 col-sm-10">
                    <div class="row">
                        <div class="col-sm-5">
                            <div class="margin-bottom-ten">
                                <div id="gSignInWrapper">
                                    <span class="label"></span>
                                    <div id="customBtn" class="customGPlusSignIn">
                                        <!-- <?php
                                        echo '<input type="image" src="/img/google/googleplus.jpg" alt="google"  ng-click="login()" id="googlebutton" >';
//style="cursor:pointer;padding-left:48px;"
                                        ?>-->           <div ng-click="login()" id="googlebutton">
                                            <span class="icon"></span>
                                            <span class="buttonText">Sign in</span>
                                        </div>
                                    </div>
                                </div>
                                                                                        <!--<input type="image" class="form-control" name="googlelogo" id="googlelogo" src="gplusicon.png">-->
                            </div>
                        </div>
                        <div class="col-sm-offset-1 col-sm-6">
                            <!--                                    <a href="/pages/signup"><button type="submit" class="btn logins1" ng-disabled="myForm.password.$dirty && myForm.password.$invalid || myForm.email.$dirty && myForm.email.$invalid || myForm.firstname.$dirty && myForm.firstname.$invalid || myForm.lastname.$dirty && myForm.lastname.$invalid"><font color="white">Signup</button></font></a>-->
                            <?php echo $this->Form->submit(__d('users', 'Signup'), 
                                    array('class' => 'btn logins1',
                                        'ng-disabled' => 'formSignUp.$invalid')); ?>


                        </div>


                        <!--</form>-->

<!--                        <script>
                            var app = angular.module('myApp', []);
                            app.controller('validateCtrl', function ($scope) {
                                $scope.password = 'John Doe';
                                $scope.email = 'john.doe@gmail.com';
                                $scope.firstname = 'John';
                                $scope.lastname = 'Doe';
                            });
                        </script>-->

                        <div class="col-sm-offset-1 col-sm-10">
                            <div class="margin-bottom-ten">
                                <hr class="hrb" style="margin-top: 0px; margin-bottom: 0px;"/>
                            </div>
                        </div>
                        <?php
                        echo $this->Form->end();
                        ?> 
                    </div>
                </div>
                <div class="row fontbottom">
                    <div class="col-sm-offset-1 col-sm-10">
                        <a href="/users/login"><h6>Already a member?&nbsp;Login!</h6></a>
                    </div>
                </div>
            </div>

        </div>

    </div>
    <style>
        .btn-success{
            background-color: #008858;
        }
    </style>
    <script type="text/javascript">
        //Gets User's Timezone
        var d = new Date();
        var offset = d.getTimezoneOffset();
        document.getElementById("UserTimezone").value = offset; 
    </script>

    <script type="text/javascript">
        (function () {
            var po = document.createElement('script');
            po.type = 'text/javascript';
            po.async = true;
            po.src = 'https://apis.google.com/js/client.js?onload=onLoadCallback';
            var s = document.getElementsByTagName('script')[0];
            s.parentNode.insertBefore(po, s);
        })();
    </script>  
</div>
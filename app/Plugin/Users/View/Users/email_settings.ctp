<?php
$this->layout = 'gridle_in';
$this->set('title_for_layout', 'Email Settings');
?>
<div class="container-fluid">
    <div class="g-settings-wrap row">
        <!-- SETTINGS PAGE MENU  -->
        <div class="dashboard-menu-setting settings-nav col-lg-offset-1 col-lg-3 col-md-offset-1 col-md-3 col-sm-4 col-xs-4">
            <a href="/users/edit" class="profle-settings-btn">
                <div class='dashboard-menu-item-setting active'>
                    <p class='p-margin-0'>Email Settings</p>
                </div>    
            </a>

        </div>
        <div class="dashboard-setting col-lg-4 col-lg-offset-1 col-md-4 col-md-offset-1 col-sm-6 col-xs-8">
            <div class="settings-edit-profile-area container-fluid">
                <?php
                echo $this->Form->create('EmailSetting', array(
                    'role' => 'form',
                ));

                $fields_to_remove = array('id', 'user_id', 'email', 'created', 'modified','USER_INVITATION','NETWORK_INVITATION','NETWORK_ROLE_ADMIN','NETWORK_ACTIVATED');
                foreach ($fields_to_remove as $value) {
                    unset($EmailSetting['EmailSetting'][$value]);
                }
                
                $EmailSettingValue= Configure::read('EmailSetting');
                
                foreach ($EmailSetting['EmailSetting'] as $key => $value) {

                    
                    echo '<div class="dashboard-menu-item active">'.(isset($EmailSettingValue[$key])?$EmailSettingValue[$key]:$key).'  ';
                    echo $this->Form->checkbox($key, array('class'=>'pull-right','checked'=>$value));
                    echo '</div>';
                }




                echo '<br>';

                echo $this->Form->submit(__d('email_settings', 'Save Preferences'), array('class' => 'btn btn-success btn-sm pull-right', 'div' => false));

                echo $this->Form->end();
                ?>
            </div>
        </div>
    </div>
</div>

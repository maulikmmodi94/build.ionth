<?php
$this->layout = 'gridle_out_new';
$this->set('title_for_layout', 'Resend Verification | Build');
?>
<div class="container-fluid backc">
    <div class="row">
        <div class="col-sm-4 col-xs-3 r">
            <hr class="hrr">
        </div>
        <div class="col-sm-4 col-xs-6 buttonview">
            Verify my mail
        </div>
        <div class="col-sm-4 col-xs-3 l">
            <hr class="hrl">
        </div>
    </div>

    <div class="row">
        <div class="col-sm-offset-4 col-sm-4 col-xs-offset-2 col-xs-8">
            <div class="loginbox">
                <!--<form class="horizontal" name="login" method="post" action="/users/resend_verification">-->
                    <?php
                echo $this->Form->create($model, array(
                    'url' => array(
                        'admin' => false,
                        'action' => 'resend_verification'), 'class' => 'horizontal', 'role' => 'form'));
                ?>
                
                    <div class="col-sm-offset-1 col-sm-10">
                        <div class="form-group">
                            <p class="fp" style="line-height: 1.42857143; margin-top:10px !important;">Let's get this done quickly. Just give us your email.</p>
                        </div>
                    </div>
                    <div class="col-sm-offset-1 col-sm-10">
                        <div class="form-group">
                            <h6 class="be" style="line-height: 1.42857143; margin-top:10px !important;">Put in your mail, get the magic link right in your email inbox and click. You are done.</h6>
<!--                            <input type="text" class="form-control" name="email" id="email" placeholder="Your Email"><br>-->
                            <?php
                    if (isset($AuthUser)) {
                    echo $this->Form->input('email', array(
                         'value'=>$AuthUser['email'], 'class' => 'form-control','label' => false, 'placeholder' => 'Your Email','readonly'));
                } else {
                    echo $this->Form->input('email', array(
                         'class' => 'form-control','label' => false, 'placeholder' => 'Your Email','autofocus'));
                }
                ?>
                        </div>
                    </div>							
                    <!--					<div class="col-sm-offset-1 col-sm-10">
                                                                    <div class="form group">
                                                                            <input type="text" class="form-control" name="window" id="window"><br>
                                                                    </div>
                                                            </div>-->
                    <div class="col-sm-offset-1 col-sm-10" style="height: 50px;">
                        <div class="form-group">
<!--                            <a href=""><button type="button" class="btn logins2"><font color="white">Resend me the verification link</font></button></a>-->
           <?php                 echo $this->Form->submit(__d('users', 'Resend me the verification link'), array('class' => 'btn logins2'));?>
                        </div>
                    </div>
                    <div class="col-sm-offset-1 col-sm-10">
                        <div class="form-group">
                            <h6 class="be">We are sorry that you have to do this. But in order to make Build more secure and user friendly, we need to know that you are real. Don't worry, your data is safe with us.</h6>
                        </div>
                    </div>
                    <div class="col-sm-offset-1 col-sm-10">
                        <div class="form-group">
                            <hr class="hrb">
                        </div>
                    </div>
                    <div class="row fontbottom">
                        <div class="col-sm-offset-1 col-sm-10">
                            <a href="/users/login"><h6>My account is fine, take me to the login page</h6></a>
                        </div>
                    </div>
                <!--</form>-->
                <?php
                
                echo $this->Form->end();
                ?>
            </div>
        </div>
    </div>
</div>
</div>
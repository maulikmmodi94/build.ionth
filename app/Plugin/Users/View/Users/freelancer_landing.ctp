<?php
$this->layout = 'landing';
$this->set('title_for_layout', 'Gridle');
?>
<div class="g-land-wrap container-fluid">
    <div class="g-user-land-wrap row">
            <div class="col-sm-7 font-coustard text-left">
                <p class="land-header-title">Expresso-shot for your productivity!</p>
                <div class="land-content col-sm-offset-1">
                <!--Bullet 1-->
                <div class="row bullet-point">
                    <div class="col-sm-1">
                        <p><i class="fa-land fa-check-square-o fa-1x text-success"></i></p>
                    </div>
                    <div class="col-sm-10 padding-lr-0">
                        <p class="p-margin-0 font-big-2">
                            Stay on top with individual centric Tasks, IMs & Files.        
                        </p>
                        <p class="">
                            People make business happen, we believe!    
                        </p>
                    </div>
                </div>
                <!--Bullet 2-->
                <div class="row bullet-point">
                    <div class="col-sm-1">
                        <p><i class="fa-land fa-check-square-o fa-1x text-success"></i></p>
                    </div>
                    <div class="col-sm-10 padding-lr-0">
                        <p class="p-margin-0 font-big-2">
                            Dead simple. You don't need to learn Gridle.         
                        </p>
                        <p class="">
                            Gridle will align itself to the way you work!    
                        </p>
                    </div>    
                </div>
                <!--Bullet 3-->
                <div class="row bullet-point">
                    <div class="col-sm-1">
                        <p><i class="fa-land fa-check-square-o fa-1x text-success"></i></p>
                    </div>
                    <div class="col-sm-10 padding-lr-0">
                        <p class="p-margin-0 font-big-2">
                            No pay-per-user bullsh*t. Add as many people as you want.         
                        </p>
                        <p class="">
                            We want your business to grow as much as you do!
                        </p>
                    </div>
                </div>
                
                <!--Bullet 4-->
                <div class="row bullet-point">
                    <div class="col-sm-1">
                        <p><i class="fa-land fa-check-square-o fa-1x text-success"></i></p>
                    </div>
                    <div class="col-sm-10 padding-lr-0">
                        <p class="p-margin-0 font-big-2">
                            216,823 Man-hours saved already
                        </p>
                        <p class="">
                            For remote-ok entrepreneurs, designers & free-lancers    
                        </p>
                    </div>
                </div>
                </div>
            </div>
            <div class="col-sm-4 land-signup-section col-sm-offset-1 padding-lr-0" ng-controller="emptyFormsController">
                <div>
                    <p class="form-land-heading p-margin-0" >Get started in 30 Secs for free</p>
                </div>
                <div class="land-form-signup-area">
                    <p class="p-margin-0">No credit card required. Ofcourse, all features are accessible.</p>

                    <!--    FORM        -->
                    <?php
                    echo $this->Form->create($model, array('type' => 'file', 'class' => 'land-form-signup form-form', 'novalidate', 'role' => 'form', 'name' => 'formSignUp', 'inputDefaults' => array('label' => false,)));
                    ?>
                    <div class="row">
                        <!--    FIRST NAME      -->
                        <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12 ">
                            <?php
                            echo $this->Form->hidden('User.timezone', array('hiddenField' => false, 'type' => 'number'));
                            $this->Form->unlockField('User.timezone');
                            echo $this->Form->input('UserInfo.first_name', array('type' => 'text', 'class' => 'form-control input-sm', 'placeholder' => 'First name', 'div' => FALSE, 'required', 'ng-minlength' => 2, 'ng-maxlength' => 30, 'ng-pattern' => '/^[a-z. ]*$/i', 'maxlength' => 30, 'ng-model' => 'data.UserInfo.first_name', 'autofocus'));
                            ?>
                            <div class="form-inline-error text-warning" ng-show="formSignUp['data[UserInfo][first_name]'].$dirty">
                                <p ng-show="formSignUp['data[UserInfo][first_name]'].$error.required" class="p-margin-0">Please fill your first name</p>
                                <p ng-show="formSignUp['data[UserInfo][first_name]'].$error.minlength" class="p-margin-0">Minimum 2 carachters are required.</p>
                                <p ng-show="formSignUp['data[UserInfo][first_name]'].$error.maxlength" class="p-margin-0">First name too long.</p>
                                <p ng-show="formSignUp['data[UserInfo][first_name]'].$error.pattern" class="p-margin-0">You can use only alphabets and spaces. </p>
                            </div>
                        </div>
                        <!--    LAST NAME      -->
                        <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <?php
                            echo $this->Form->input('UserInfo.last_name', array('type' => 'text', 'class' => 'form-control input-sm', 'placeholder' => 'Last name', 'div' => FALSE, 'required', 'ng-minlength' => 2, 'ng-maxlength' => 30, 'ng-pattern' => '/^[a-z. ]*$/i', 'maxlength' => 30, 'ng-model' => 'data.UserInfo.last_name'));
                            ?>
                            <div class="form-inline-error text-warning" ng-show="formSignUp['data[UserInfo][last_name]'].$dirty">
                                <p ng-show="formSignUp['data[UserInfo][last_name]'].$error.required" class="p-margin-0">Please fill your last name</p>
                                <p ng-show="formSignUp['data[UserInfo][last_name]'].$error.minlength" class="p-margin-0">Minimum 2 characters are required.</p>
                                <p ng-show="formSignUp['data[UserInfo][last_name]'].$error.maxlength" class="p-margin-0">Last name too long.</p>
                                <p ng-show="formSignUp['data[UserInfo][last_name]'].$error.pattern" class="p-margin-0">You can use only alphabets and spaces. </p>
                            </div>
                        </div>
                    </div>
                    <?php
                    $email = isset($this->request->data['User']['email']) ? $this->request->data['User']['email'] : null;
                    ?>
                    <!--    EMAIL   ADDRESS     -->
                    <div class="form-group" ng-init="data.User.email = '<?= $email; ?>'">
                        <?php
                        if ($email) {

                            echo $this->Form->input('email', array('readonly', 'error' => array('isValid' => __d('users', 'Must be a valid email address'), 'isUnique' => __d('users', 'An account with that email already exists')), 'class' => 'form-control input-sm', 'placeholder' => 'Email address', 'div' => FALSE, 'ng-pattern' => '/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/', 'ng-model' => 'data.User.email', 'required'));
                        } else {
                            echo $this->Form->input('email', array('error' => array('isValid' => __d('users', 'Must be a valid email address'), 'isUnique' => __d('users', 'An account with that email already exists')), 'class' => 'form-control input-sm', 'placeholder' => 'Email address', 'div' => FALSE, 'ng-pattern' => '/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/', 'ng-model' => 'data.User.email', 'required'));
                        }
                        ?>
                        <div ng-show="formSignUp['data[User][email]'].$dirty" class="form-inline-error text-warning" >
                            <p ng-show="formSignUp['data[User][email]'].$error.required" class="p-margin-0" >Please fill your email address.</p>
                            <p ng-show="formSignUp['data[User][email]'].$error.pattern" class="p-margin-0">Write your email address</p>
                        </div>
                    </div>
                    <!--    PASSWORD        -->

                    <div class="form-group">
                        <div class="input-group input-group-sm" ng-hide="showpassword">
                            <?php echo $this->Form->input('password', array('type' => 'password', 'class' => 'form-control', 'placeholder' => 'Password', 'div' => FALSE, 'required', 'ng-minlength' => 6, 'ng-model' => 'data.User.password', 'ng-hide' => 'showpassword')); ?>
                            <div class="input-group-addon font-small-1 hidden-xs ">
                                <label class="p-margin-0" style="font-weight:normal">
                                    <input type="checkbox" ng-model="showpassword" ng-checked="false" >&nbsp;&nbsp;Show</label>
                            </div>
                        </div>
                        <div class="input-group input-group-sm" ng-show="showpassword">
                            <input class="form-control"  type="text" ng-model="data.User.password" ng-show="showpassword" placeholder="Password" />
                            <div class="input-group-addon font-small-1 " >
                                <label class="p-margin-0" style="font-weight:normal">
                                    <input type="checkbox" ng-model="showpassword" ng-checked="false" >&nbsp;&nbsp;Show</label>
                            </div>
                        </div>
                        <div ng-show="formSignUp['data[User][password]'].$dirty" class="form-inline-error text-warning" >
                            <p ng-show="formSignUp['data[User][password]'].$error.required" class="p-margin-0" >Please fill your password.</p>
                            <p ng-show="formSignUp['data[User][password]'].$error.minlength" class="p-margin-0">Minimum 6 characters are required.</p>
                        </div>
                    </div>
                    <?php
                    echo '<div class="row">';
                    $tosLink = $this->Html->link(__d('users', 'Policy & Terms of Use'), array('controller' => 'pages', 'action' => 'terms', 'plugin' => null), array('target' => "_blank"));
                    echo '<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12"><p class="font-small-1 text-muted">By signing up, you agree to Gridle\'s ' . $tosLink . ' </p></div>';
                    echo $this->Form->submit(__d('users', 'Sign me up'), array('class' => 'btn btn-primary btn-block', 'div' => array('class' => 'col-lg-6 col-md-6 col-sm-6 col-xs-12 text-semi-bold'), 'ng-disabled' => 'formSignUp.$invalid'));
                    echo '</div>';
                    ?>
                    <?php
                    echo $this->Form->end();
                    ?> 
                </div>
                <div class="signup-sublinks">
                    <p class='text-muted text-center font-small-1'>Already a member? <a href='/users/login'>Login here</a>.</p>
                </div>
            </div>
    </div>
    <div class="testimonial_margin">
        <div class="col-sm-6">
            <div class="testimonial_user_img_div col-sm-2 col-xs-12">
                <img class="testimonial_user_img center-block" src="/img/toi.jpg" width="100" alt="chiranjay shah's testmonial about gridle"/>
            </div>
            <div class="testimonial_text col-sm-6 col-xs-12">
                <p class="testimonial_user_line"><span class="fa fa-quote-left"></span>&nbsp;&nbsp; "Move over mail Gridle cuts communication load" &nbsp;&nbsp; <span class="fa fa-quote-right"></span></p><p class="testimonial_username">- The Times of India</p>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="testimonial_user_img_div col-sm-2 col-xs-12">
                <img class="testimonial_user_img center-block" src="/img/landing/Ian.png" alt="Ian Tyner's testmonial about gridle"/>
            </div>
            <div class="testimonial_text col-sm-6 col-xs-12">
                <p class="testimonial_user_line"><span class="fa fa-quote-left"></span>&nbsp;&nbsp; Gridle is Fantastic &nbsp;&nbsp; <span class="fa fa-quote-right"></span></p>
                <p class="testimonial_username">- Ian Tyner, Ionth Global Ltd.</p>
            </div>
        </div>
    </div>
</div>
    <!--<div class='row' style='margin-top: 50px;'>-->
        <!--<div class='col-lg-3 col-lg-offset-4'><img class='center-block'  src='/img/media/next_big_what.png' /></div>-->
        <!--<div class='col-lg-4'><p class='text-muted ' >Gridle is one stop shop for all communication needs of teams & enterprises.</p></div>-->
    <!--</div>-->
<script type="text/javascript">
//Gets User's Timezone
    var d = new Date();
    var offset = d.getTimezoneOffset();
    document.getElementById("UserTimezone").value = offset;
</script>
<style>

    .main-text{
        font-weight: normal;
        letter-spacing: 1px;
        font-size: 16px;      
    }    

    .sub-text{
        font-weight: normal;
        letter-spacing: 1px;
        font-size: 14px;       
    }    

    .land-header-title{
        font-family: 'Coustard', serif;
        color : brown;
        font-size: 36px;
        padding-bottom: 10px;
    }

    .font-coustard{
        font-family: 'Coustard', serif;
    }

    .land-header-subtitle{
        margin-bottom: 5% ;
    }

    .g-user-land-wrap{
        font-family : 'proxima-nova-light';
        background-image : url('/img/landing/landing_freelancer.jpg');
        background-repeat: no-repeat;
        background-size:cover;
        min-height: 600px;
        padding: 20px;
        padding-right: 30px;
        padding-top: 100px;
    }

    .content-section{
        background:white;
        background:rgba(255,255,255,0.7);
        border-radius:5px;
        padding-bottom:20px;
    }

    .land-form-signup{
        margin-top: 10px;
        padding: 0% 15%;
        padding-top: 20px;
    }

    .g-land-wrap {
        padding-top: 50px;
        /* padding-bottom: 50px; */
        border-bottom: 1px solid #f7f5eb;
    }

    .form-land-heading{
        text-align: center;
        line-height: 36px;
        font-size: 24px;
        font-weight: 600;
        color: #E79367;
        background-size: 100% 100%;
    }

    .land-back{
        background: white;
        background: rgba(255, 255, 255, 0.4);
        padding-bottom : 15px;
    }

    .land-signup-section{
        background: white;
        background: rgba(255, 255, 255, 0.85);
        border-radius: 5px;
        padding: 20px;
        
        margin-top: 10px;
    }

    .land-form-signup-area{
        border-radius: 5px;
        padding: 0 0 5% 0;
    }
    .bullet-point{
        padding: 5px 0;
    }
    
    .land-content{
        margin-top: 35px;
    }
    
    .testimonial_margin{
        margin-top: 15px;
        margin-bottom: 15px;
    }
    
   .fa-land {
        display: inline-block;
        font-family: FontAwesome;
        font-style: normal;
        font-weight: normal;
        line-height: 1;
        margin-top: 4px;
        -webkit-font-smoothing: antialiased;
        -moz-osx-font-smoothing: grayscale;
}
    
</style>
<link href='https://fonts.googleapis.com/css?family=Coustard' rel='stylesheet' type='text/css'>
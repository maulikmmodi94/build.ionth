<?php
$this->layout = 'gridle_out_new';
$this->set('title_for_layout', 'Build | cloud based visual collaboration tool | enterprises | teams ');

?>

<div class="container-fluid backc" ng-controller="googleController">
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet" type="text/css">
    <script type="text/javascript">
                (function () {
                    var po = document.createElement('script');
                    po.type = 'text/javascript';
                    po.async = true;
                    po.src = 'https://apis.google.com/js/client.js?onload=onLoadCallback';
                    var s = document.getElementsByTagName('script')[0];
                    s.parentNode.insertBefore(po, s);
                })();
    </script>

    <style type="text/css">
        #customBtn {
            display: inline-block;
            background: #dd4b39;
            color: white;
            width: 110px;
            height:40px;
            border-radius: 5px;
            white-space: nowrap;
        }
        #customBtn:hover {
            background: #e74b37;
            cursor:pointer;
        }
        span.label {
            font-weight: bold;
        }
        span.icon {
            background: url('/img/google/googleplus.jpg');
            display: inline-block;
            vertical-align: middle;
            width: 40px;
            height: 40px;
            border-right: #bb3f30 1px solid;
            border-top-left-radius: 5px;
            border-bottom-left-radius: 5px;
        }
        span.buttonText {
            display: inline-block;
            text-align: center;
            padding-left: 11px;
            padding-right: 35px;
            font-size: 14px;
            font-weight: bold;
            /* Use the Roboto font that is loaded in the <head> */
            font-family: 'Roboto',arial,sans-serif;
        }
    </style>
    <div class="row">
        <div class="col-sm-4 col-xs-3 r">
            <hr class="hrr">
        </div>
        <div class="col-sm-4 col-xs-6 buttonview">
            Welcome back!
        </div>
        <div class="col-sm-4 col-xs-3 l">
            <hr class="hrl">
        </div>
    </div>

    <div class="row">
        <div class="col-sm-offset-4 col-sm-4 col-xs-offset-2 col-xs-8" style="margin-bottom:65px;">
            <div class="loginbox" style="line-height: 1.42857143;">
                <!--<form class="horizontal" name="login" method="post" action="_blank">-->
                <?php
                echo $this->Form->create($model, array(
                    'action' => 'login',
                    'id' => 'myForm',
                    'name' => 'myForm',
                    'autocomplete' => 'on',
                    'class' => 'horizontal',
                    'role' => 'form',
                    'inputDefaults' => array('label' => false,),
                    'novalidate'));
//                echo $this->Form->hidden('User.return_to', array('value' => $return_to));
                echo $this->Form->hidden('User.timezone', array('hiddenField' => false, 'type' => 'number'));
                echo $this->Form->hidden('User.remember_me', array('hiddenField' => false, 'type' => 'number'));
                echo $this->Form->hidden('User.type', array('type' => 'text','value'=>'gridle'));
                $this->Form->unlockField('User.timezone');
                ?>
                <div class="col-sm-offset-1 col-sm-10">
                    <div class="form-group">
                        <p class="fp">Please enter your login credentials</p>
                    </div>
                </div>
                <div class="col-sm-offset-1 col-sm-10">
                    <div class="form-group">
                        <h6 class="be"></h6>
                    </div>
                </div>
                <!--Email Address-->
                <div class="col-sm-offset-1 col-sm-10">
                    <div class="form-group">
                        <?php
                        echo $this->Form->input('email', array('class' => 'form-control',
                            'type' => 'text',
                            'placeholder' => 'Email',
                            'div' => FALSE,
                            'ng-pattern' => '/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/',
                            'ng-model' => 'data.User.email',
                            'required',
                            'autofocus'));
                        ?>


<!--                                <span style="color:red" ng-show="myForm.email.$dirty && myForm.data[User][email].$invalid">
              <span ng-show="myForm.data[User][email].$error.required">Email is required.</span>
              <span ng-show="myForm.data[User][email].$error.email">Invalid email address.</span>
          </span>-->
                    </div>
                </div>							
                <!--                        <div class="col-sm-offset-1 col-sm-10">
                                            <div class="form-group">
                                                <h6 class="be"></h6>
                                            </div>
                                        </div>				-->
                <!--password-->
                <div class="col-sm-offset-1 col-sm-10">
                    <div class="">
                        <?php
                        echo $this->Form->input('password', array('type' => 'password',
                            'class' => 'form-control',
                            'placeholder' => 'Password',
                            'div' => FALSE,
                            'required',
                            'ng-minlength' => 6,
                            'ng-model' => 'data.User.password',
                            'ng-init' => 'password_visibility=password',
                            'ng-hide' => 'showpassword'));
                        ?>

<!--                                <span style="color:red" ng-show="myForm.data[User][password].$dirty && myForm.data[User][password].$invalid">
    <span ng-show="myForm.data[User][password].$error.required">Password is required.</span>
</span>-->
                    </div>
                </div>					
                <span class="clearfix"></span>
                <div class="col-sm-offset-1 col-sm-10">
                    <?php echo $this->Session->flash('auth'); ?>
                </div>
                <div class="col-sm-offset-1 col-sm-10" style="margin-top: 15px;">
                    <div class="row">
                        <div class="col-sm-5">
                            <div class="form group" ng-click="login()">
                                <div id="gSignInWrapper">
                                    <span class="label"></span>
                                    <div id="customBtn" class="customGPlusSignIn">
                                        <span class="icon"></span>
                                        <span class="buttonText">Sign in</span>
                                    </div>
                                </div>														
                            </div>
                        </div>
                        <div class="col-sm-offset-1 col-sm-6">
                            <!--<a href="/users/login">-->
                            <!--                                    <button type="submit" class="btn logins1" ng-disabled="myForm.password.$dirty && myForm.password.$invalid || myForm.data[User][email].$dirty && myForm.data[User][email].$invalid"><font color="white">Get in</font></button>-->
                            <?php
                            echo $this->Form->submit(__d('users', 'Login'), array('class' => 'logins1 btn',
                                'div' => array('class' => 'col-lg-6 col-md-6'), //'ng-disabled' => 'formLogin.$invalid'
                            ));

                            echo $this->Form->end();
                            ?>
                            <!--</a>-->
                        </div>
                    </div>
                </div>
                <!--</form>-->
                <?php
//                        echo "</form>\n";
                echo $this->Form->end();
                ?>
<!--                        <script>
                    var app = angular.module('myApp', []);
                    app.controller('validateCtrl', function ($scope) {
                        $scope.password = 'Password';
                        $scope.email = 'Your Email';
                    });
                </script>-->
                <div class="col-sm-offset-1 col-sm-10">
                    <div class="form-group">
                        <hr class="hrb">
                    </div>
                </div>
                <div class="row fontbottom">
                    <div class="col-sm-offset-1 col-sm-10">
                        <h6><a href="/users/add">Not on Build yet?&nbsp;Signup </a><br></h6><h6><a href="/users/reset_password">Forgot password&nbsp;</a>&nbsp;|&nbsp;<a href="/users/resend_verification">Resend Verification&nbsp;</a></h6>
                    </div>
                </div>
                </form>
            </div>

        </div>

    </div>
    <script type="text/javascript">
//Gets User's Timezone
        var d = new Date();
        var offset = d.getTimezoneOffset();
        document.getElementById("UserTimezone").value = offset;
    </script>
</div>

<?php $this->layout = 'gridle_out'; ?>
<div class="g-page-wrap container-fluid">
    <div class="g-resend-verification-wrap row ">
        <div class="col-lg-4 col-lg-offset-1 resend-verification-main-img">
            <p>
                <object data="/img/homepage/gridle_signup_type_writer.svg" type="image/svg+xml" class="center-block">
                    <img src="/img/homepage/gridle_signup_type_writer.svg" />
                </object>
            </p>
        </div>
        <div class="col-lg-4 col-lg-offset-1 form-resend-verification-area">
            <h2 class="form-heading">Resend Invitation</h2>
            <!--<h5 class="form-subheading">Tagline</h5>-->
            <div class="users form">
                <?php
                echo $this->Form->create($model, array(
                    'url' => array(
                        'admin' => false,
                        'action' => 'resend_invitation'), 'class' => 'form-resend-verifcation form-form', 'role' => 'form'));
                echo '<div class="form-group">';
                if (isset($AuthUser)) {
                    echo $this->Form->input('email', array(
                        'label' => __d('users', 'User\'s Email'), 'class' => 'form-control', 'placeholder' => 'Email address'));
                } else {
                    echo $this->Form->input('email', array(
                        'label' => __d('users', 'Your Email'), 'class' => 'form-control', 'placeholder' => 'Email address','autofocus'));
                }
                echo '</div>';
                echo $this->Form->submit(__d('users', 'Send again'), array('class' => 'btn btn-success'));
                echo $this->Form->end();
                ?>
            </div>
        </div>
    </div>
</div>

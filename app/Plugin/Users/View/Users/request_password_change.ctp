<?php
$this->layout = 'gridle_out_new';
$this->set('title_for_layout', 'Forgot Password | Build');
?>
<meta content="width=device-width, initial-scale=1, maximum-scale=1" name="viewport">

<div class="container-fluid backc">
    <div class="row header">
    <div class="row">
        <div class="col-sm-4 col-xs-3 r">
            <hr class="hrr">
        </div>
        <div class="col-sm-4 col-xs-6 buttonview">
            Forgot Password
        </div>
        <div class="col-sm-4 col-xs-3 l">
            <hr class="hrl">
        </div>
    </div>
    <div class="row">
        <div class="col-sm-offset-4 col-sm-4 col-xs-offset-2 col-xs-8">
            <div class="loginbox loginbox-password">                           

                <?php
                echo $this->Form->create($model, array(
                    'name'=>'forgotPassword',
                    'url' => array('admin' => false, 'action' => 'reset_password'),
                    'class' => 'form-reset-pass',
                    'role' => 'form',
                    'inputDefaults' => array(
                    'label' => false,
                )));
                ?> 
                
                <div class="col-sm-offset-1 col-sm-10">
                    <div class="form-group">
                        <p class="fp">We got your back! Here, just give us your email.</p>
                    </div>
                </div>
                <div class="col-sm-offset-1 col-sm-10">
                    <div class="form-group">
                        <h6 class="be">If you don't receive a link, be sure to check your spam folder.</h6>
                    </div>
                </div>
                <div class="col-sm-offset-1 col-sm-10">
                    <div class="form-group">
                        

<!--                        <input name="data[User][email]" class="form-control" placeholder="Your Email" autofocus="autofocus" maxlength="320" id="email" required="required" type="email">-->
                         <?php
                         echo $this->Form->input('email', array('class' => 'form-control', 'placeholder' => 'Your registered email address here', 'div' => array('class' => 'form-group'), 'autofocus'));
                               ?>
                        
                    </div>
                </div>
                <div class="col-sm-offset-1 col-sm-10">
                    
                    <div ng-show="forgotPassword['data[User][email]'].$dirty" class="form-group text-warning">
                        <div class="window form-group form-control">
<!--                        <input type="text" class="form-control" name="window" id="window" disabled>-->
                            <span ng-show="forgotPassword.data[User][email].$dirty && forgotPassword.data[User][email].$invalid">
                                <span ng-show="forgotPassword.email.$error.required">Email is required.</span>
                                <span ng-show="forgotPassword.email.$error.email">Invalid email address.</span> 
                            </span>
                        </div>
                    </div>
                </div>
               	<div class="col-sm-offset-1 col-sm-10">

<!--                    <a href="/pages/login">
                        
                        <button type="submit" class="btn logins2" ng-disabled="forgotPassword.email.$dirty && myForm.email.$invalid"><font color="white">Send me the link on my email.</font></button></a>-->
                    <div class="form-group" ng-disabled="forgotPassword.email.$dirty && myForm.email.$invalid" style="color: #ffffff; height: 50px;">
            <?php              echo $this->Form->submit(__d('users', 'Send me the link on my email'), array('class' => 'btn logins2'));
            ?>
                    </div>


                </div>
                <?php
                echo $this->Form->end();
                ?>
<!-- <script>
var app = angular.module('myApp', []);
app.controller('validateCtrl', function($scope) {
$scope.user = 'John Doe';
$scope.email = 'john.doe@gmail.com';
});
</script>-->

                <div class="col-sm-offset-1 col-sm-10">
                    <div class="form-group">
                        <h6 class="be">You will receive a mail with a secure link. Click on that and set your new password. Yeap, that's pretty much it.</h6>
                    </div>
                </div>
                <div class="col-sm-offset-1 col-sm-10">
                    <div class="form-group">
                        <hr class="hrb-password">
                    </div>
                </div>
                <div class="row fontbottom">
                    <div class="col-sm-offset-1 col-sm-10">
                        <a href="/users/login"><h6>Oh yes, got it! Take me back to login.</h6></a>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
</div>
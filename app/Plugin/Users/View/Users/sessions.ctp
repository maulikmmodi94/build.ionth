<?php
$this->layout = 'gridle_in';
$this->set('title_for_layout', 'Sessions');
?>
<style>
    .list-group-item
    {
        margin-bottom:10px;
    }
</style>
<div class="container-fluid">
    <div class="g-settings-wrap row">
        <!-- SETTINGS PAGE MENU  -->
        <div class="list-group dashboard col-lg-offset-3 col-lg-6 col-md-offset-3 col-md-6 col-sm-12 col-xs-12">

            <?php
            foreach ($sessions as $key => $session) {
              //  $browser = get_browser($session['UserSession']['user_agent'], true);
               // $brower_name = $browser['browser'];
                echo ' <div href="#" class="list-group-item">';
               // echo ' <h4>Session from <strong>' .$browser['parent'].'</strong> at IP address '. $session['UserSession']['ip'] . '</span></h4><hr>';
                echo ' <h4>Session from IP address '. $session['UserSession']['ip'] . '</span></h4><hr>';
                echo ' <p>Last accessed <br><strong>' . $this->Time->timeAgoInWords($session['UserSession']['created']) . '</strong></p>';
                echo ' <p>User Agent: <br> <small>' . $session['UserSession']['user_agent'] . '</small></p>';

                echo $this->Form->postLink(__('End Session'), array('action' => 'sessions', $session['UserSession']['id']), array('class' => 'btn btn-danger btn-sm'), __('Are you sure you want to delete Session from %s ?', $session['UserSession']['ip']));
                echo '</div>';
            }
            ?>




        </div>

    </div>
</div>
<?php
/**
 * Copyright 2010 - 2013, Cake Development Corporation (http://cakedc.com)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright 2010 - 2013, Cake Development Corporation (http://cakedc.com)
 * @license MIT License (http://www.opensource.org/licenses/mit-license.php)
 */

echo __d('users','Hi %s,',$data['mail_name']['UserInfo']['first_name']);
echo "<br/>";
echo "<br/>";

echo __d('users','%s has asked your presence for having a video call right now. Click on the button below to go the the meeting room.',$data['name']);
echo "<br/>";


echo "<a target='_blank' style='margin:14px 0;color:#ffffff;text-decoration:none;display:inline-block;min-height:38px;line-height:39px;padding-top:0;padding-right:16px;padding-bottom:0;padding-left:16px;outline:0;background:#3eae5f;font-size:14px;text-align:center;font-style:normal;font-weight:400;border:0;vertical-align:bottom;white-space:nowrap;border-radius:0.2em' href='".$data['url']."'>Join Video Call</a>";
echo "<br/>";
echo "<br/>";

echo __d('users','Just click on the above button. Will work seamlessly on Phone, tablet, PC or any internet enabled device and no download or sign up will be required. Hassle free.');
echo "<br/>";
echo "<br/>";

echo 'Also, in case you are facing any problems or have suggestions, just hit reply or tweet to me <a href="https://twitter.com/ionthglobal">@build_ionth</a>. Contrary to what you might think, I reply blazingly fast.';
//echo 'If you think you are receiving a lot of my mails, just click on <a href = ' . $data['unsubscribe']. '> unsubscribe <a> and I won\'t bother you again. :)<br>';
//
//echo "<br/>";
//echo __d('tasks', 'Yours,<br>Donna.');

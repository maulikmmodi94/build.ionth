<?php
/**
 * Copyright 2010 - 2013, Cake Development Corporation (http://cakedc.com)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright 2010 - 2013, Cake Development Corporation (http://cakedc.com)
 * @license MIT License (http://www.opensource.org/licenses/mit-license.php)
 */

echo __d('users','Hello');
echo "<br/>";
echo "<br/>";

echo __d('users','User with email <strong>%s</strong> have been invited to work with <strong>%s</strong> on <strong>%s</strong> workspace at Build.',$data['email_to_notify'],$data['name'],$data['network_name']);
echo "<br/>";


echo 'Want to tell us something? Just hit reply to this mail or tweet to us on <a href="https://twitter.com/ionthglobal">@build_ionth</a>. If you think this mail was not intended for you, please ignore.';
//echo 'If you think you are receiving a lot of my mails, just click on <a href = ' . $data['unsubscribe']. '> unsubscribe <a> and I won\'t bother you again. :)<br>';
//echo "<br/>";
//echo 'Yours,<br>Donna';
<?php
/**
 * Copyright 2010 - 2013, Cake Development Corporation (http://cakedc.com)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright 2010 - 2013, Cake Development Corporation (http://cakedc.com)
 * @license MIT License (http://www.opensource.org/licenses/mit-license.php)
 */

echo __d('users','Hello %s',$data['name']);
echo "<br/>";
echo "<br/>";

echo __d('users',' Please click on the button below to complete setting up your account.');
echo "<br/>";


echo "<a target='_blank' style='margin:14px 0;color:#ffffff;text-decoration:none;display:inline-block;min-height:38px;line-height:39px;padding-top:0;padding-right:16px;padding-bottom:0;padding-left:16px;outline:0;background:#3eae5f;font-size:14px;text-align:center;font-style:normal;font-weight:400;border:0;vertical-align:bottom;white-space:nowrap;border-radius:0.2em' href='".$data['url']."'>Accept Invitation</a>";

echo ' Want to tell us something? Just hit reply to this mail or tweet to us on <a href="https://twitter.com/ionthglobal">@build_ionth</a>. If you think this mail was not intended for you, please ignore.';
//echo 'If you think you are receiving a lot of my mails, just click on <a href = ' . $data['unsubscribe']. '> unsubscribe <a> and I won\'t bother you again. :)<br>';
//echo "<br/>";
//echo 'Yours,<br>Donna';
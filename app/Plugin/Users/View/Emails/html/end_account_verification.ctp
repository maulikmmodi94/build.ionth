<?php

/**
 * Copyright 2010 - 2013, Cake Development Corporation (http://cakedc.com)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright 2010 - 2013, Cake Development Corporation (http://cakedc.com)
 * @license MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
echo __d('users', 'Hi <strong>%s</strong>,', $data['fname']);
echo "<br/><br/>";
echo __d('users', 'The verification link I sent the other day after you signed up on Build had expired. These things happen and I understand that you might have been caught up with something important. ');
echo "<br/><br/>";
echo __d('users', 'For security reasons I disabled your previous verification link. Don’t worry, I’ve got you covered. Click on the button below and you will be able to fetch another verification link.');
echo "<br>";
echo "<a target='_blank' style='margin:14px 0;color:#ffffff;text-decoration:none;display:inline-block;min-height:38px;line-height:39px;padding-top:0;padding-right:16px;padding-bottom:0;padding-left:16px;outline:0;background:#3eae5f;font-size:14px;text-align:center;font-style:normal;font-weight:400;border:0;vertical-align:bottom;white-space:nowrap;border-radius:0.2em' href='" . $data['url'] . "'>Get my Build account back</a>";
echo "<br/>";

echo __d('users', 'Its great to have you back. :)');
echo "<br/>";

echo 'Also, in case you are facing any problems or have suggestions, just hit reply or tweet to me <a href="https://twitter.com/ionthglobal">@build_ionth</a>. Contrary to what you might think, I reply blazingly fast.';
//echo 'If you think you are receiving a lot of my mails, just click on <a href = ' . $data['unsubscribe']. '> unsubscribe <a> and I won\'t bother you again. :)<br>';
//
//echo "<br/>";
//echo 'Yours,<br>Donna';

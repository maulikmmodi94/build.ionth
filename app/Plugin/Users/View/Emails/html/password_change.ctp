<?php
/**
 * Copyright 2010 - 2013, Cake Development Corporation (http://cakedc.com)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright 2010 - 2013, Cake Development Corporation (http://cakedc.com)
 * @license MIT License (http://www.opensource.org/licenses/mit-license.php)
 */

echo __d('users','Hi %s,',$data['fname']);
echo "<br/>";
echo "<br/>";

echo __d('users','Your password has been changed successfully.');
echo "<br/>";
echo "<br/>";

echo __d('users','If this request was not made by you. Do let me know by replying to this email ASAP.');
echo "<br/>";

echo 'Also, in case you are facing any problems or have suggestions, just tweet to me <a href="https://twitter.com/ionthglobal">@bulid_ionth</a>. Contrary to what you might think, I reply blazingly fast.';
//echo 'If you think you are receiving a lot of my mails, just click on <a href = ' . $data['unsubscribe']. '> unsubscribe <a> and I won\'t bother you again. :)<br>';
//echo "<br/>";
//echo 'Yours,<br>Donna';

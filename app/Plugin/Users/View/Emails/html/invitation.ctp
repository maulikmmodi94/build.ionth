<?php
/**
 * Copyright 2010 - 2013, Cake Development Corporation (http://cakedc.com)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright 2010 - 2013, Cake Development Corporation (http://cakedc.com)
 * @license MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
echo __d('users', 'Hi,');
echo "<br/>";
echo "<br/>";

echo __d('users','We are using Build to work together. It’s great in managing tasks, files and discussions. I am inviting you to join <strong>%s</strong> on Build. Just click on the button below.',$data['network_name']);
echo "<br/>";


echo "<a target='_blank' style='margin:14px 0;color:#ffffff;text-decoration:none;display:inline-block;min-height:38px;line-height:39px;padding-top:0;padding-right:16px;padding-bottom:0;padding-left:16px;outline:0;background:#3eae5f;font-size:14px;text-align:center;font-style:normal;font-weight:400;border:0;vertical-align:bottom;white-space:nowrap;border-radius:0.2em' href='".$data['url']."'>Join workspace</a>";
echo "<br/>";
echo 'Also, in case you are facing any problems or have suggestions, just hit reply. It will directly send out mail to Build. You can also tweet to them <a href="https://twitter.com/ionthglobal">@build_ionth</a>.';
//echo 'If you think you are receiving a lot of my mails, just click on <a href = ' . $data['unsubscribe']. '> unsubscribe <a> and I won\'t bother you again. :)<br>';
//
//echo "<br/>";
//echo __d('networks', 'Thanks,<br>%s',$data['invited_by']);

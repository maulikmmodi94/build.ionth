<?php

/**
 * Copyright 2010 - 2013, Cake Development Corporation (http://cakedc.com)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright 2010 - 2013, Cake Development Corporation (http://cakedc.com)
 * @license MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
App::uses('CakeEmail', 'Network/Email');
App::uses('UsersAppController', 'Users.Controller');
App::uses('CakeEvent', 'Event');
App::import('Vendor', 'S3', array('file' => 'S3' . DS . 'S3.php'));

/**
 * Users Users Controller
 *
 * @package       Users
 * @subpackage    Users.Controller
 * @property	  AuthComponent $Auth
 * @property	  CookieComponent $Cookie
 * @property	  PaginatorComponent $Paginator
 * @property	  SecurityComponent $Security
 * @property	  SessionComponent $Session
 * @property	  User $User
 * @property	  RememberMeComponent $RememberMe
 */
class UsersController extends AppController {

    /**
     * Controller name
     *
     * @var string
     */
    public $name = 'Users';

    /**
     * If the controller is a plugin controller set the plugin name
     *
     * @var mixed
     */
    public $plugin = null;

    /**
     * Helpers
     *
     * @var array
     */
    public $helpers = array(
        'Html',
        'Form',
        'Session',
        'Time',
        'Text'
    );

    /**
     * Components
     *
     * @var array
     */
    public $components = array(
        'Auth',
        'Session',
        'Cookie',
        'Paginator',
        'Security',
        'Search.Prg',
        'Users.RememberMe',
    );
    public $emailTokenExpirationTime = 604800;
    public $cookieLifeTime = '+1 year';

    /**
     * Preset vars
     *
     * @var array $presetVars
     * @link https://github.com/CakeDC/search
     */
    public $presetVars = true;

    /**
     * Constructor
     *
     * @param CakeRequest $request Request object for this controller. Can be null for testing,
     *  but expect that features that use the request parameters will not work.
     * @param CakeResponse $response Response object for this controller.
     */
    public function __construct($request, $response) {
        $this->_setupComponents();
        parent::__construct($request, $response);
        $this->_reInitControllerName();
    }

    /**
     * Providing backward compatibility to a fix that was just made recently to the core
     * for users that want to upgrade the plugin but not the core
     *
     * @link http://cakephp.lighthouseapp.com/projects/42648-cakephp/tickets/3550-inherited-controllers-get-wrong-property-names
     * @return void
     */
    protected function _reInitControllerName() {
        $name = substr(get_class($this), 0, -10);
        if ($this->name === null) {
            $this->name = $name;
        } elseif ($name !== $this->name) {
            $this->name = $name;
        }
    }

    /**
     * Returns $this->plugin with a dot, used for plugin loading using the dot notation
     *
     * @return mixed string|null
     */
    protected function _pluginDot() {
        if (is_string($this->plugin)) {
            return $this->plugin . '.';
        }
        return $this->plugin;
    }

    /**
     * Wrapper for CakePlugin::loaded()
     *
     * @param string $plugin
     * @return boolean
     */
    protected function _pluginLoaded($plugin, $exception = true) {
        $result = CakePlugin::loaded($plugin);
        if ($exception === true && $result === false) {
            throw new MissingPluginException(array('plugin' => $plugin));
        }
        return $result;
    }

    /**
     * Setup components based on plugin availability
     *
     * @return void
     * @link https://github.com/CakeDC/search
     */
    protected function _setupComponents() {
        if ($this->_pluginLoaded('Search', false)) {
            $this->components[] = 'Search.Prg';
        }
    }

    /**
     * beforeFilter callback
     *
     * @return void
     */
    public function beforeFilter() {
        parent::beforeFilter();
        $this->_setupAuth();
        $this->_setupPagination();

        if ($this->action == 'add_google') {
            $this->Security->unlockedActions = 'add_google';
        }
        if ($this->action == 'api_login') {
            $this->Security->unlockedActions = 'api_login';
        }
        if ($this->action == 'apiGoogleLogin') {
            $this->Security->unlockedActions = 'apiGoogleLogin';
        }
        if ($this->action == 'registerDevice') {
            $this->Security->unlockedActions = 'registerDevice';
        }
        if ($this->action == 'admin_add') {
            $this->Security->unlockedActions = 'admin_add';
        }if ($this->action == 'edit') {
            $this->Security->unlockedActions = 'edit';
        }if ($this->action == "sendMail") {
            $this->Security->unlockedActions = 'sendMail';
        }
        if ($this->action == "api_add") {
            $this->Security->unlockedActions = 'api_add';
        }

        $this->set('model', $this->modelClass);

        if (!Configure::read('App.defaultEmail')) {
            Configure::write('App.defaultEmail', 'noreply@' . env('HTTP_HOST'));
        }

        // set cookie options
        $this->Cookie->key = 'qSI232qs*&sXOw!adre@34SAv!@*(XSL#$%)asGb$@11~_+!@#HKis~#^';
        $this->Cookie->httpOnly = true;
        $this->Cookie->time = 18144000;
        if (!$this->Auth->loggedIn() && $this->Cookie->check('rememberMe')) {
            $cookie = $this->Cookie->read('rememberMe');

            $user = $this->User->find('first', array(
                'conditions' => array(
                    'User.email' => @$cookie['User']['email'],
                    'User.password' => @$cookie['User']['password']
                ),
                'contain' => array('UserInfo')
            ));


            if (isset($user['User']) && $this->Auth->login($user['User'])) {
                $Event = new CakeEvent(
                        'Controller.User.afterLogin', $this, array('request' => $this->request, 'user_id' => $this->Auth->user('id'), 'session_id' => CakeSession::id())
                );
                $this->getEventManager()->dispatch($Event);
                $this->Session->write('Auth.User.UserInfo', $user['UserInfo']);
                $this->User->setTimeZone($cookie['User']['timezone']);
                $this->redirect('/');
            } else {
                $this->_distroyCookie();
                $this->redirect('/users/logout'); // destroy session & cookie
            }
        }
    }

    /**
     * Sets the default pagination settings up
     *
     * Override this method or the index action directly if you want to change
     * pagination settings.
     *
     * @return void
     */
    protected function _setupPagination() {
        $this->Paginator->settings = array(
            'limit' => 12,
            'conditions' => array(
                $this->modelClass . '.active' => 1,
                $this->modelClass . '.email_verified' => 1
            )
        );
    }

    /**
     * Sets the default pagination settings up
     *
     * Override this method or the index() action directly if you want to change
     * pagination settings. admin_index()
     *
     * @return void
     */
    protected function _setupAdminPagination() {
        $this->Paginator->settings = array(
            'limit' => 20,
            'order' => array(
                $this->modelClass . '.created' => 'desc'
            )
        );
    }

    /**
     * Setup Authentication Component
     *
     * @return void
     */
    protected function _setupAuth() {
        if (Configure::read('Users.disableDefaultAuth') === true) {
            return;
        }


        $this->Auth->allow('add', 'reset', 'verify', 'logout', 'view', 'reset_password', 'login', 'resend_verification', 'unsubscribe', 'add_google', 'token_check', 'freelancer_landing', 'api_login', 'registerDevice', 'apiGoogleLogin', 'admin_add', 'sendMail','api_add');

        if (!is_null(Configure::read('Users.allowRegistration')) && !Configure::read('Users.allowRegistration')) {
            $this->Auth->deny('add');
        }


        if ($this->request->action == 'register') {
            $this->Components->disable('Auth');
        }

        $this->Auth->authenticate = array(
            'Form' => array(
                'fields' => array(
                    'username' => 'email',
                    'password' => 'password'),
                'userModel' => $this->_pluginDot() . $this->modelClass,
                'scope' => array(
                    'OR' => array(
                        array('AND' => array(
                                array($this->modelClass . '.active' => 1),
                                array($this->modelClass . '.email_verified' => 0),
                                array($this->modelClass . '.created >' => date('Y-m-d H:i:s', time() - $this->emailTokenExpirationTime)),
                            )),
                        array(
                            'AND' => array(
                                array($this->modelClass . '.active' => 1),
                                array($this->modelClass . '.email_verified' => 1))
        )))));

        $this->Auth->loginRedirect = array('controller' => 'networks', 'action' => 'index');
        $this->Auth->logoutRedirect = array('plugin' => Inflector::underscore($this->plugin), 'controller' => 'users', 'action' => 'login');
        $this->Auth->loginAction = array('admin' => false, 'plugin' => Inflector::underscore($this->plugin), 'controller' => 'users', 'action' => 'login');
    }

    /**
     * Shows a users profile
     *
     * @param string $slug User Slug
     * @return void
     */
    public function view($slug = null) {


        try {
            $this->set('user', $this->{$this->modelClass}->view($slug));
        } catch (Exception $e) {
            $this->Session->setFlash($e->getMessage());
            $this->redirect('/');
        }
    }

    /**
     * Edit
     *
     * @param string $id User ID
     * @return void
     */
    public function edit() {
        $this->viewClass = 'Json';
        if ($this->request->is('Post') && !empty($this->request->data)) {
            #check if there is photo in edit request or not 
            if (isset($_FILES['file'])) {
                $this->request->data['UserInfo']['photo'] = $_FILES['file'];
            }
            /* Email cannot be edited */
            unset($this->request->data['UserInfo']['email']);
            $this->log($this->request->data['UserInfo']);
            if (isset($this->request->data['UserInfo']['photo']) && $this->request->data['UserInfo']['photo']['error'] == UPLOAD_ERR_OK) {
                $this->log("no upload error");
                $photo_name = $this->User->UserInfo->field('photo', array('user_id' => $this->Auth->User('id')));
                $this->log($photo_name);
                $extension = pathinfo($this->request->data['UserInfo']['photo']['name'], PATHINFO_EXTENSION);
                $this->request->data['UserInfo']['photo']['name'] = md5($this->request->data['UserInfo']['photo']['name'] . time() . mt_rand()) . "." . $extension;
            }

            /* Saving UserInfo */
            if ($this->_addUserInfo($this->Auth->user('id'))) {
                /* Delete Old files */
                $thumbnails = array('vga', 'xvga', 'thumb');
                $s3 = new S3(_aws_access_key, _aws_secret_key);
                if (isset($photo_name) && $photo_name) {
                    foreach ($thumbnails as $key => $value) {
                        $path = S3_Picture_Profile_Path . $this->Auth->User('id') . "/" . $value . "_" . $photo_name;
                        $s3->deleteObject($this->User->UserInfo->s3_bucket_name, $path);
                        $this->log("deleting files");
                    }
                }
//                $this->Session->setFlash('Profile has been updated.');
                $UserInfo = $this->User->UserInfo->findByUserId($this->Auth->user('id'));
                $this->set(array(
                    'users' => $UserInfo,
                    '_serialize' => 'users'
                ));
//                return $this->redirect(array('action' => 'settings'));
                return;
            } else {
                $this->Session->setFlash('Something went wrong, we will try and get it fixed ASAP');
            }
        }

        $UserInfo = $this->User->UserInfo->findByUserId($this->Auth->user('id'));
        $this->Session->write('Auth.User.UserInfo', $UserInfo['UserInfo']);
        $this->set('UserInfo', $UserInfo);
        // @todo replace this with something better than the user details that were removed
    }

    protected function _checkUploadError() {
        if (!isset($this->request->data['UserInfo']['photo']['error']) || $this->request->data['UserInfo']['photo']['error'] == UPLOAD_ERR_NO_FILE) {
            unset($this->request->data['UserInfo']['photo']);
        }
    }

    /**
     * Admin add
     *
     * @return void
     */
    public function admin_add() {
        $this->request->allowMethod('Post');
        if ($this->request->is('post')) {
            /* Set NetworkList to Session */
            $Networks = $this->_setNetworkList();
            $CurrentNetwork = $this->Session->read('CurrentNetwork');
            $authUser = $this->Auth->user('id');
            if (empty($Networks[$CurrentNetwork])) {
                $this->Session->setFlash('Network not found');
                $this->redirect('/networks/view');
            }

            $this->loadModel('Network');
//            if (!$this->Network->UserNetwork->isAdmin($authUser, $CurrentNetwork)) {
//                $this->Session->setFlash('Only Admins can Invite');
//                if (isset($this->request->data['User']['return_to']) && !empty($this->request->data['User']['return_to'])) {
//                return $this->redirect($this->request->data['User']['return_to']);
//            } else {
//                return $this->redirect('/networks/view/' . $Networks[$CurrentNetwork]['url']);
//            }
//                $this->redirect('/networks/view');
//            }

            $network_object = $this->Network->find('first', array(
                'conditions' => array('Network.id' => $CurrentNetwork),
                'fields' => array('Network.remaining_users', 'Network.file_limit')
            ));
            if ($network_object['Network']['remaining_users'] < count($this->request->data['email'])) {
                $this->Session->setFlash(__('You cannot add more members. Please upgrade your network. '));
//            return $this->redirect($this->referer());
                $this->viewClass = 'Json';
                $this->set(array(
                    'users' => 'False',
                    '_serialize' => 'users'
                ));
                return;
            }
            $this->log('$this->request->data-----------');
            $this->log($this->request->data);
            $request_emails = $this->request->data['email'];
            foreach ($request_emails as $key => $value) {
                $request_emails[$key] = strtolower($value);
            }

            $emails = array_unique($request_emails);

            /* Seperate Valid and Invalid Emails */
            $validate_emails = $this->User->validateEmails($emails);
            $valid_emails = $validate_emails['valid'];
            $invalid_emails = $validate_emails['invalid'];
            /* Seperate Registed Emails */
            $registered_users = $this->User->find('list', array(
                'conditions' => array(
                    'User.email' => $valid_emails
                ),
                'fields' => array(
                    'User.id',
                    'User.email'
                )
            ));

            $un_registered_users = array_diff($valid_emails, $registered_users);
            $this->loadModel('UserNetwork');
            $this->log($registered_users);
            $this->log($CurrentNetwork);
            $network_members = $this->User->UserNetwork->find('list', array(
                'conditions' => array(
                    'UserNetwork.user_id' => array_keys($registered_users),
                    'UserNetwork.network_id' => $CurrentNetwork
                ),
                'fields' => array(
                    'UserNetwork.user_id',
                    'UserNetwork.status'
            )));
        $this->log('$network_members-------');
            $this->log($network_members);
            $network_members_removed = array_diff($network_members, array('active'));


            $network_members_active = array_diff($network_members, $network_members_removed);


            $non_network_members = array_diff(array_keys($registered_users), array_keys($network_members));
            $non_network_members_master = $non_network_members;

            $new_users = array();

            $this->User->UserInfo->validator()->remove('last_name');
            $this->User->UserInfo->validator()->remove('first_name');
            foreach ($un_registered_users as $key => $value) {

                $email_explode = explode("@", $value);
                $slug = preg_replace("/[^0-9a-zA-Z ]/m", "", $email_explode[0]);
                $slug = preg_replace("/ /", "-", $slug);

                $username = $this->User->makeUniqueSlug(strtolower($slug));
                $this->request->data['User']['username'] = $username;
                $this->request->data['User']['email'] = $value;
                $this->request->data['User']['password'] = $this->User->generatePassword();
                $this->request->data['User']['tos'] = true;
                $this->request->data['User']['added_by'] = $this->Auth->user('id');
                $this->request->data['User']['active'] = false;
                $this->request->data['User']['invite_token'] = $this->User->generateToken();



                $user = $this->User->add($this->request->data);
                $this->request->data['UserInfo']['first_name'] = ucfirst($slug);
                $this->request->data['UserInfo']['last_name'] = '';
                $this->request->data['UserInfo']['email'] = $value;
                $this->_addUserInfo($user['User']['id']);

                $this->User->EmailSetting->create();
                $this->request->data['EmailSetting']['email'] = $value;
                $this->request->data['EmailSetting']['user_id'] = $user['User']['id'];
                $this->User->EmailSetting->save($this->request->data);


                array_push($new_users, $user);
                $email_data = array(
                    'name' => $this->Auth->user('UserInfo.name'),
                    'email' => $value,
                    'network_name' => $Networks[$CurrentNetwork]['name'],
                    'network_url' => $Networks[$CurrentNetwork]['url'],
                    'invited_by' => $this->Auth->user('UserInfo.name'),
                    'url' => Router::url(array('admin' => false, 'plugin' => 'users', 'controller' => 'users', 'action' => 'add', $this->request->data['User']['invite_token']), true)
                );
                /* first time invitation mail */
                CakeResque::enqueue('default', 'MailShell', array('sendInvitationEmail', $email_data));

                /* if not signed up for 3 days after invitation */
                CakeResque::enqueueIn(VARIFICATION_EXPIRATION, 'default', 'UserShell', array('AfterInvite', $email_data), true);
//              CakeResque::enqueue('default', 'UserShell', array('AfterInvite', $email_data), true);
            }
            $new_users = Hash::combine($new_users, '{n}.User.id', '{n}.User.email');

            $non_network_members = array_merge($non_network_members, array_keys($new_users));


            $network_object = $this->Network->find('first', array(
                'conditions' => array('Network.id' => $CurrentNetwork),
                'fields' => array('Network.remaining_users', 'Network.file_limit')
            ));
            if ($network_object['Network']['remaining_users'] >= count($non_network_members)) {
                $data = array();

                foreach ($non_network_members as $key => $value) {
                    $temp = array();
                    $temp['UserNetwork']['network_id'] = $Networks[$CurrentNetwork]['id'];
                    $temp['UserNetwork']['user_id'] = $value;
                    $temp['UserNetwork']['role'] = 'admin';
                    $temp['UserNetwork']['status'] = 'active';
                    $temp['UserNetwork']['added_by'] = $this->Auth->user('id');
                    $temp['UserNetwork']['file_limit'] = $network_object['Network']['file_limit'];
                    $temp['UserNetwork']['info'] = $this->User->UserInfo->findByUserId($value);
                    array_push($data, $temp);
                }

                if (!empty($data) && $this->User->UserNetwork->saveMany($data)) {
                    if (preg_match('/add_member/', $this->referer())) {
                        $group_data['admin'] = $this->Auth->user('id');
                        $group_data['network_id'] = $CurrentNetwork;
                        $group_data['userlist'] = $non_network_members;
                        $group_data['group_name'] = "Team " . $Networks[$CurrentNetwork]['name'];

                        $Event = new CakeEvent(
                                'Controller.Group.afterNetworkCreate', $this, $group_data
                        );
                        $this->getEventManager()->dispatch($Event);
                    }

                    $info = array(
                        'Network' => $Networks[$CurrentNetwork],
                        'network_name' => $Networks[$CurrentNetwork]['name'],
                        'UserNetwork' => $non_network_members_master,
                        'invited_by' => $this->Auth->user('UserInfo.name'),
                        'email' => $this->request->data['email'][0]
                    );
                    /* check whether user is signed up or not */
                    $u = $this->User->findByEmail($info['email'], array('User.email', 'User.email_token'));
                    if (empty($u['User']['invite_token']) || is_null($u['User']['invite_token'])) {
                        $info['url'] = Router::url(array('admin' => false, 'plugin' => FALSE, 'controller' => 'networks', 'action' => 'switchNetwork', $Networks[$CurrentNetwork]['url'] . '/desks'), TRUE);
                    } else {
                        $info['url'] = Router::url(array('admin' => false, 'plugin' => 'users', 'controller' => 'users', 'action' => 'add', $u['User']['invite_token']), true);
                    }
                    #invite user to current network
                    CakeResque::enqueue('default', 'NetworkShell', array('invite', $info));

                    /* if not signed up for 3 days after invitation */
                    CakeResque::enqueueIn(VARIFICATION_EXPIRATION, 'default', 'UserShell', array('AfterInvite', $info), true);
//                    CakeResque::enqueue('default', 'UserShell', array('AfterInvite', $info), true);
//                    $this->Session->setFlash('Invitations have been sent successfully');
                    if (isset($this->request->data['User']['return_to'])) {
                        if ($this->request->data['User']['return_to'] == 'invite_network') {
                            $this->viewClass = 'Json';
                            $user_pro = Hash::extract($data, '{n}.UserNetwork.info.UserInfo');
                            $uu = $this->User->UserNetwork->findByUserIdAndNetworkId($data[0]['UserNetwork']['user_id'], $data[0]['UserNetwork']['network_id'], array('UserNetwork.id', 'UserNetwork.user_id'));
                            $user_pro[0]['user_network_id'] = $uu['UserNetwork']['id'];
                            $returnObject = array(
                                'message' => 'success',
                                'data' => array('email' => $this->request->data['email'],
                                    'info' => $user_pro),
                            );
                            $this->set(array(
                                'users' => $returnObject,
                                '_serialize' => 'users'
                            ));
                            return $returnObject;
                        } else {
                            return $this->redirect($this->request->data['User']['return_to']);
                        }
                    } else {
                        /* return path unset */
//                        return $this->redirect('/networks/view/' . $Networks[$CurrentNetwork]['url']);
                    }
                } else {
                    $this->Session->setFlash('Something went wrong, we will try and get it fixed ASAP.');
                    return $this->redirect('/networks/view/' . $Networks[$CurrentNetwork]['url']);
                }
            } else {
                $this->Session->setFlash("You cannot add more members. Please upgrade your network. ");
                $this->redirect('/networks/view/' . $Networks[$CurrentNetwork]['url']);
            }
            return $this->redirect('/networks/view/' . $Networks[$CurrentNetwork]['url']);
        }
    }

    /**
     * it send the appear.in link to its user which is specified in request data.
     * 
     * @param type $mail_id
     * @param type $link
     */
    public function sendMail() {
        $this->autoRender = false;
        $this->viewClass = 'Json';
        if ($this->request->is('post')) {
            if (isset($this->request->data)) {
//        $Network_id = $this->Session->read('CurrentNetwork');
                $mail_id = $this->request->data['email'];

                #there is no check of current network or user's data
                foreach ($mail_id as $value) {
//            $this->log('$value');
//            $this->log($value);
                    $email_data = array(
                        'name' => $this->Auth->user('UserInfo.first_name'),
                        'email' => $value,
                        'url' => $this->request->data['link'],
                        'mail_name' => $this->User->UserInfo->findByEmail($value, array('UserInfo.first_name'))
                    );
                    CakeResque::enqueue('default', 'MailShell', array('sendInvitationLink', $email_data));
                }
            }
        }
    }

    /**
     * User register action
     * @author Maulik M Modi
     * @return void
     */
    public function freelancer_landing() {
        $this->add();
    }

    public function registerDevice() {
        $this->viewClass = 'Json';


        $user_id = $this->request->query['user'];
        $reg_id = $this->request->query['reg_id'];

        $user = $this->User->findById($user_id, array('User.username', 'User.email'));
        $datas = array(
            'registration_id' => $reg_id,
            'user_id' => $user_id,
            'username' => $user['User']['username'],
            'email' => $user['User']['email']
        );
        $reg = $this->UserRegId->check_regId($reg_id, $user_id);
        $returnObject['data'] = 'ALREADY_REGISTERED';
        if (empty($reg)) {
            $user = $this->UserRegId->store_regId($datas);
            $returnObject['data'] = 'DEVICE_REGISTERED';
        }
        $returnObject['status'] = 'success';

        $this->set(array(
            'data' => $returnObject,
            '_serialize' => 'data'
        ));
    }
    
    /**
     * @discription-> this function is called in signup/login with google
     * and then it redirect function to its appropriate page.
     * @author maulik m modi
     */
    public function add_google() {
        $this->viewClass = 'Json';
        $this->User->validator()->remove('email');
        $this->User->validator()->remove('password');
        /* check whether user is login or not */
        if($this->_isUserLoggedIn()){
            $this->Session->setFlash(__d('users', 'You are already registered and logged in.'));
            $this->redirect('/');
        }
        if ($this->request->is('post') && !empty($this->request->data)) {

                $this->request->data['User']['email'] = strtolower($this->request->data['User']['email']);
                $emails = $this->User->fetchEmail($this->request->data['User']['email']);
                $this->request->data['User']['type'] = 'google';
                
                if (!empty($emails)) {
                    $val = $this->login();
                } else {
                    $this->request->data['User']['password'] = $this->User->generatePassword();
                    $this->request->data['User']['password_token'] = $this->User->generateToken();

                    $val = $this->add();
                }
        }
        $returnObject = array(
            'redirect' => 'true',
            'url' => $val
        );
        $this->set(array(
            'users' => $returnObject,
            '_serialize' => 'users'
        ));
    }

    protected function _setDefaultValueOfUser($current_user_data = null){
        $this->request->data['User']['email'] = strtolower($this->request->data['User']['email']);
        
        if (isset($current_user_data['User']['invite_token'])) {
            $this->request->data['User']['invite_token'] = $current_user_data['User']['invite_token'];
            }

        /**
         * ex for: maulik!123/#@gmail.com
         * it gives 
         * $slug = maulik123
         */    
        $parts = explode('@', $this->request->data['User']['email']);
        $slug = preg_replace("/[^0-9a-zA-Z ]/m", "", $parts[0]);
        $slug = preg_replace("/ /", "-", $slug);
        $this->request->data['User']['username'] = $this->User->makeUniqueSlug($slug);
        $this->request->data['UserInfo']['email'] = $this->request->data['User']['email'];
        $this->User->UserInfo->data = $this->request->data;
        return;
    }
    
    protected function _setDefaultValueForGoogleSignup($registered_user_info){
        $set_default_value_of_user = array(
                'id' => $registered_user_info['User']['id'],
                'email_token' => NULL,
                'email_verified' => SET,
                'email_token_expires' => NULL,
                'last_login_type' => GOOGLE
            );
        $this->User->save($set_default_value_of_user);
        
        $registered_user_info['User']['email_token'] = NULL;  
        return;
    }
    
    protected function _setDefaultValueForGridleSignup(){
        $this->User->saveField('last_login_type', 'gridle');
        return;
    }
    
    protected function _setEventAfterRegistration($registered_user_info){
        
        $registered_user_info['User']['name'] = $this->request->data['UserInfo']['first_name'] . " " . $this->request->data['UserInfo']['last_name'];
        $date = date_create($registered_user_info['User']['email_token_expires']);
        $registered_user_info['User']['expiration'] = date_format($date, 'jS F'); //g:ia \o\n l jS F Y
        
        #create an event after reg. for email settings and mail
        $Event = new CakeEvent(
                'Controller.User.afterRegistration', $this, $registered_user_info);

        $this->getEventManager()->dispatch($Event);

        if ($Event->isStopped()) {
                $this->redirect(array('action' => 'login'));
            }
            return;
    }
    
    protected function _autoLoginUser(){
        
        $AuthUser = $this->User->find('first', array('contain' => array('UserInfo'), 'conditions' => array('User.id' => $this->User->id)));

        if (isset($AuthUser) && $this->Auth->login($AuthUser['User'])) {
            $Event = new CakeEvent(
                    'Controller.User.afterLogin', $this, array('request' => $this->request, 'user_id' => $this->Auth->user('id'), 'session_id' => CakeSession::id())
                    );
            $this->getEventManager()->dispatch($Event);
        
        $this->Session->write('Auth.User.UserInfo', $AuthUser['UserInfo']);
        $this->Session->write('Auth.User.FirstSession', TRUE);
        $this->setTimeZone($this->request->data['User']['timezone']);
        
        return $AuthUser;
        }
        return FALSE;
    }

    /**
     * signup method for fresh and invited user
     * @param type $invite_token
     */
    public function add($invite_token = -1){
        
        if($this->_isUserLoggedIn()){
            $this->Session->setFlash(__d('users', 'You are already registered and logged in.'));
            $this->redirect('/');
        }
        
        $current_user_data = $this->User->findByInviteTokenAndActive($invite_token, 0, array('email', 'invite_token'));
        
        if ($this->request->is('post') && !empty($this->request->data)) {
            $this->_setDefaultValueOfUser($current_user_data);
            
             if ($this->User->UserInfo->validates()) {
                 $registered_user_info = $this->User->register($this->request->data);
                 
                 if (empty($registered_user_info)) {
                        $this->Session->setFlash(__d('users', 'This email id is already registered.'));
                        $this->redirect(array('action' => 'add'));
                    }
                $signup_type = $this->request->data['User']['type'];    
                
                switch ($signup_type){
                    
                    case GOOGLE:
                        $this->_setDefaultValueForGoogleSignup($registered_user_info);
                        break;
                    
                    case GRIDLE:
                        $this->_setDefaultValueForGridleSignup();
                        break;
                    
                    default :
                        break;
                        
                    }    
                    
             } else {
                    $this->Session->setFlash(__d('users', 'The user could not be saved. Please try again.'));
                    $this->redirect(array('action' => 'add'));
             }
            if (isset($registered_user_info) && $registered_user_info !== false) {
                
                $this->_setEventAfterRegistration($registered_user_info);
                /* Saving UserInfo */
                $this->_addUserInfo($registered_user_info['User']['id']);
                $AuthUser = $this->_autoLoginUser();
                
                if ($this->User->UserNetwork->getUserNetworkCount($AuthUser['User']['id']) == 0) {
                    $this->_setDefaultNetwork();
                }else{
                    $this->_setInvitationNetwork();
                }
                
            }else {
                 unset($this->request->data[$this->modelClass]['password']);
                $this->Session->setFlash(__d('users', 'Your account could not be created. Please, try again.'), 'default', array('class' => 'message warning'));
            }    
        }
         $this->request->data = $current_user_data;
    }
    
    /**
     * api add function for ionth
     */
    public function api_add(){
        $this->add();
    }


    public function add_old($invite_token = -1) {
        $this->log('in add users----------');
        $this->log($this->request->data);
        /* check whether user is login or not */
//        $this->log($this->Auth->user());
//        $this->log($this->Auth->loggedIn());
//        if ($this->Auth->user()) {
//            $this->Session->setFlash(__d('users', 'You are already registered and logged in.'));
//            $this->redirect('/');
//        }
        if($this->_isUserLoggedIn()){
            $this->Session->setFlash(__d('users', 'You are already registered and logged in.'));
            $this->redirect('/');
        }
        $User = $this->User->findByInviteTokenAndActive($invite_token, 0, array('email', 'invite_token'));
        /* when user perform self signup operation then this process is perform */
        if ($this->request->is('post') && !empty($this->request->data)) {
//            if () {
                $this->request->data['User']['email'] = strtolower($this->request->data['User']['email']);
                if (isset($User['User']['invite_token'])) {
                    $this->request->data['User']['invite_token'] = $User['User']['invite_token'];
                }

                $parts = explode('@', $this->request->data['User']['email']);
                $slug = preg_replace("/[^0-9a-zA-Z ]/m", "", $parts[0]);
                $slug = preg_replace("/ /", "-", $slug);
                $this->request->data['User']['username'] = $this->User->makeUniqueSlug($slug);
                if (!isset($this->request->data['type'])) {
                    $this->request->data['type'] = null;
                }
                $this->request->data['UserInfo']['email'] = $this->request->data['User']['email'];
                $this->User->UserInfo->data = $this->request->data;
                
                
                if ($this->User->UserInfo->validates()) {
                    $user = $this->{$this->modelClass}->register($this->request->data);
                    if (empty($user)) {
                        $this->Session->setFlash(__d('users', 'This email id is already registered.'));
                        $this->redirect(array('action' => 'add'));
                    }
                    if ($this->request->data['type'] == 'google') {
                        
                        $set_default_value_of_user = array(
                            'id' => $user['User']['id'],
                            'email_token' => NULL,
                            'email_verified' => SET,
                            'email_token_expires' => NULL,
                            'last_login_type' => GOOGLE
                        );
                        $this->User->save($set_default_value_of_user);
//                        $this->User->saveField('email_token', NULL);
//                        $this->User->saveField('email_verified', 1);
//                        $this->User->saveField('email_token_expires', NULL);
//                        $this->User->saveField('last_login_type', 'google');
                        $user['User']['email_token'] = NULL;
                    } else {
                        $this->User->saveField('last_login_type', 'gridle');
                    }
                } else {
                    $this->Session->setFlash(__d('users', 'The user could not be saved. Please try again.'));
                    $this->redirect(array('action' => 'add'));
                }

                $this->log('$user------------out-----------');
                $this->log($user);
                
                if (isset($user) && $user !== false) {

                    $user['User']['name'] = $this->request->data['UserInfo']['first_name'] . " " . $this->request->data['UserInfo']['last_name'];
                    $date = date_create($user['User']['email_token_expires']);
                    $user['User']['expiration'] = date_format($date, 'jS F'); //g:ia \o\n l jS F Y
                    /* create an event after reg. for email settings and mail */
                    $Event = new CakeEvent(
                            'Controller.User.afterRegistration', $this, $user);

                    $this->getEventManager()->dispatch($Event);

                    if ($Event->isStopped()) {
                        $this->redirect(array('action' => 'login'));
                    }

                    /* Saving UserInfo */
                    $this->_addUserInfo($user[$this->modelClass]['id']);

                    /* Auto Login User */
                    $AuthUser = $this->User->find('first', array('contain' => array('UserInfo'), 'conditions' => array('User.id' => $this->User->id)));

                    if ($this->Auth->login($AuthUser['User'])) {
                        $Event = new CakeEvent(
                                'Controller.User.afterLogin', $this, array('request' => $this->request, 'user_id' => $this->Auth->user('id'), 'session_id' => CakeSession::id())
                        );
                        $this->getEventManager()->dispatch($Event);
                    }

                    $this->Session->write('Auth.User.UserInfo', $AuthUser['UserInfo']);
                    $this->Session->write('Auth.User.FirstSession', TRUE);
                    $this->setTimeZone($this->request->data['User']['timezone']);

                    /* it redirect the user to network add page */
                    if ($this->User->UserNetwork->getUserNetworkCount($AuthUser['User']['id']) == 0) {
//                        if ($this->request->data['type'] == 'google') {
//                            return '/networks/add/user:fresh';
//                        }
//                        $this->redirect(array('plugin' => false, 'controller' => 'networks', 'action' => 'add', 'user' => 'fresh'));
//                        $request =  new CakeRequest(Router::url(array('plugin' => false, 'controller' => 'networks', 'action' => 'add')));
//                        $fresh = array(
//                            'name' => $this->Session->read('Auth.User.UserInfo.first_name') . '\'s Workspace',
//                            'return_to' => "/desks");
//                        $request->data('Network',$fresh);
//                        $response = new CakeResponse();
//                        $d = new Dispatcher();
//                        $d->dispatch($request, $response);
                        $this->_setDefaultNetwork();
                    } else {
                        #find invitation network
//                        $net_id = $this->User->UserNetwork->findNetwork($this->User->id);
//                        $this->loadModel('Network');
//                        $this->Session->write('CurrentNetwork', $net_id);
//                        $net_name = $this->Network->getNetworkField($net_id,'url');
//                        
//                        #if invitation network exists then redirect it otherwise give error
//                        if(!empty($net_name)){
//                            return $this->redirect('/desks');
////                        return $this->redirect('/networks/switchNetwork/' . $net_name . '/desks');
//                        }else{
//                            throw new Exception('Requested Network is not found.', 403);
//                        }
                        $this->_setInvitationNetwork();
                    }
                } else {
                    unset($this->request->data[$this->modelClass]['password']);
                    $this->Session->setFlash(__d('users', 'Your account could not be created. Please, try again.'), 'default', array('class' => 'message warning'));
                }
//            }
        }

        $this->request->data = $User;
    }
    
    /**
     * it sets the default network for fresh user
     * @return type
     */
    protected function _setDefaultNetwork(){
        
        $request =  new CakeRequest(Router::url(array('plugin' => false, 'controller' => 'networks', 'action' => 'add')));
        $fresh = array(
                'name' => $this->Session->read('Auth.User.UserInfo.first_name') . '\'s Workspace',
                'return_to' => "/desks");
        $request->data('Network',$fresh);
        $response = new CakeResponse();
        $d = new Dispatcher();
        return $d->dispatch($request, $response);
    }
    /**
     * it sets invitation network for invity
     * @return type
     */
    protected function _setInvitationNetwork(){
         #find invitation network
                        $net_id = $this->User->UserNetwork->findNetwork($this->User->id);
                        $this->loadModel('Network');
                        $this->Session->write('CurrentNetwork', $net_id);
                        $net_name = $this->Network->getNetworkField($net_id,'url');
                        
                        #if invitation network exists then redirect it otherwise give error
                        if(!empty($net_name)){
                            return $this->redirect('/desks');
//                        return $this->redirect('/networks/switchNetwork/' . $net_name . '/desks');
                        }else{
                            return $this->_setDefaultNetwork();
                        }
    }
    
    /* old add method */

//    public function add1($invite_token = -1) {
////         if ($this->request->is('get')) {
////         
////             
////         }
//         
////        if ($this->request->is('post')) {
//        if ($this->Auth->user()) {
//            $this->Session->setFlash(__d('users', 'You are already registered and logged in.'));
//            $this->redirect('/');
//        }
//        $this->log('hiii');
//        $User = $this->User->findByInviteTokenAndActive($invite_token, 0, array('email', 'invite_token'));
//        $this->log($User);
//        $this->log($this->request->data);
//        if (!empty($this->request->data)) {
//            /*check user is already loged in or not*/
////            $this->log($this->request->data);
////            $this->log($this->request->data['User']['email']);
////            $this->log(strtolower($this->request->data['User']['email']));
//            $this->request->data['User']['email'] = strtolower($this->request->data['User']['email']);
////            $this->log($this->request->data['User']['email']);
//            $emails = $this->User->fetchEmail($this->request->data['User']['email']);    
////            $this->log('hiii');
////            $this->log($emails);
////            $this->log(is_null($emails));
//            //check whether email id is exists or not.  
////            if($emails){
////                $this->log('in condition');
//////                $this->redirect($this->referer());
////                $this->redirect(array('action' => 'add'));
//////                $this->Session->setFlash('This email id is already registered.');
////                $returnObject['status'] = 'This email id is already registered.';
////        $this->set(array(
////            'data' => $returnObject,
////            '_serialize' => 'data'
////        ));
////                return;
////            }
////            $this->log('regular condition');
//            
//            if (isset($User['User']['invite_token'])) {
//                $this->request->data['User']['invite_token'] = $User['User']['invite_token'];
//            }
//
//            $parts = explode('@', $this->request->data['User']['email']);
//            $slug = preg_replace("/[^0-9a-zA-Z ]/m", "", $parts[0]);
//            $slug = preg_replace("/ /", "-", $slug);
//            $this->request->data['User']['username'] = $this->User->makeUniqueSlug($slug);
//
//
//            $this->request->data['UserInfo']['email'] = $this->request->data['User']['email'];
//            $this->User->UserInfo->data = $this->request->data;
//            if ($this->User->UserInfo->validates()) {
//                $user = $this->{$this->modelClass}->register($this->request->data);
//            } else {
//                $this->Session->setFlash(__d('users', 'The user could not be saved. Please try again.'));
//                $this->redirect(array('action' => 'add'));
//            }
//            if (isset($user) && $user !== false) {
//$this->log($user);
//                $Event = new CakeEvent(
//                        'Controller.User.afterRegistration', $this, array(
//                    'data' => $user,
//                        )
//                );
//
//                $this->getEventManager()->dispatch($Event);
//
//
//                if ($Event->isStopped()) {
//                    $this->redirect(array('action' => 'login'));
//                }
//
//                /* Saving UserInfo */
//                $this->_addUserInfo($user[$this->modelClass]['id']);
//
//                /*if sign up with google then redirect to password.ctp*/
//                $password = $this->request->data[$this->modelClass]['password'];
////                $this->log($user);
////                $this->log($user['User']['id']);
//                if(!$password){
//                    $this->log($password);
//                    $this->log('in password');
////                    $this->log($user);
////                    $this->set_password();
//                    unset($this->User->validate['password']['too_short']);
//                    $this->redirect(array('action' => 'set_password', $user['User']['id'], $user['User']['timezone']));
////                    Router::connect('/users', array('action' => 'set_password'/*, $user['User']['id'], $user['User']['timezone']*/));
////                    $this->redirect('/users/set_password/'.$user['User']['id'].'/'.$user['User']['timezone'].'');
//                    return;
//                    
//                }
//                /* Sending verification mail */
//
//                //$this->_sendVerificationEmail($this->{$this->modelClass}->data);
//
//                if (!isset($User['User']['invite_token'])) {
//                    /* Sending Verification Email Start */
//                    $email_data['name'] = $this->request->data['UserInfo']['first_name'] . " " . $this->request->data['UserInfo']['last_name'];
//                    $email_data['email_token'] = $this->{$this->modelClass}->data['User']['email_token'];
//                    $email_data['email'] = $this->request->data['User']['email'];
//                    $email_data['url'] = Router::url(array('admin' => false, 'plugin' => 'users', 'controller' => 'users', 'action' => 'verify', 'email', $email_data['email_token']), true);
//
//                    /* Replacing Send Mail via CakeResque queue system */
//                    CakeResque::enqueue('default', 'MailShell', array('sendVerificationEmail', $email_data));
//                    /* Sending Email End */
//                }
//                $this->Session->setFlash(__d('users', 'Your account has been created. You should receive an e-mail shortly to authenticate your account. Once validated you will be able to login.'));
//
//                /* Auto Login User */
//                $AuthUser = $this->User->find('first', array('contain' => array('UserInfo'), 'conditions' => array('User.id' => $this->User->id)));
//                if ($this->Auth->login($AuthUser['User'])) {
//                    $Event = new CakeEvent(
//                            'Controller.User.afterLogin', $this, array('request' => $this->request, 'user_id' => $this->Auth->user('id'), 'session_id' => CakeSession::id())
//                    );
//                    $this->getEventManager()->dispatch($Event);
//                }
//
//                
//                $this->Session->write('Auth.User.UserInfo', $AuthUser['UserInfo']);
//                $this->Session->write('Auth.User.FirstSession', TRUE);
//                $this->setTimeZone($this->request->data['User']['timezone']);
//
//                if ($this->User->UserNetwork->getUserNetworkCount($AuthUser['User']['id']) == 0) {
//                    $this->redirect(array('plugin' => false, 'controller' => 'networks', 'action' => 'add'));
//                } else {
//                    $this->redirect(array('plugin' => false, 'controller' => 'networks', 'action' => 'index'));
//                }
//            }
//            else {
//                unset($this->request->data[$this->modelClass]['password']);
//                unset($this->request->data[$this->modelClass]['temppassword']);
//                $this->Session->setFlash(__d('users', 'Your account could not be created. Please, try again.'), 'default', array('class' => 'message warning'));
//            }
//        }
//        $this->request->data = $User;
////    }
////    }
//    }


    public function _addUserInfo($id = null) {
        if (!empty($this->request->data)) {

            $this->_checkUploadError();
//
//            if (isset($this->request->data['UserInfo']['dob']) && $this->request->data['UserInfo']['dob']) {
//                $this->request->data['UserInfo']['dob'] = new DateTime($this->request->data['UserInfo']['dob']);
//                $this->request->data['UserInfo']['dob'] = $this->request->data['UserInfo']['dob']->format('Y-m-d');
//            } else {
//                unset($this->request->data['UserInfo']['dob']);
//            }



            if (!is_null($id)) {

                /* Set Primary Key */
                $this->User->UserInfo->primaryKey = 'user_id';
                $this->User->UserInfo->id = $id;
                $this->request->data['UserInfo']['user_id'] = $id;
            }
            if ($this->User->UserInfo->save($this->request->data)) {
                $this->User->UserInfo->primaryKey = 'id';
                return true;
            } else {
                $this->User->UserInfo->primaryKey = 'id';
                return false;
            }
        }
        return false;
    }

    /**
     * Common login action
     *
     * @return void
     */
//    public function login_old() {
//        $this->log('in auth component--------login-');
//            $this->log($this->request->data);
//        if ($this->Auth->user()) {
//            return $this->redirect('/');
//        }
//        $user_id = NULL;
//        if ($this->request->is('post') && !empty($this->request->data)) {
//            #$this->User->validates(array('fieldList' => array('email', 'password')))
//            /* Changing Auth Component to Login via Username and Email both */
////            if (filter_var($this->request->data['User']['username'], FILTER_VALIDATE_EMAIL)) {
////                $this->request->data['User']['email'] = $this->request->data['User']['username'];
////                $this->Auth->authenticate = array(
////                    'Form' => array(
////                        'fields' => array(
////                            'username' => 'email',
////                            'password' => 'password'),
////                        'userModel' => $this->_pluginDot() . $this->modelClass,
////                        'scope' => array(
////                            $this->modelClass . '.active' => 1,
////                            $this->modelClass . '.email_verified' => 1)));
////            }
//            if (!isset($this->request->data['type'])) {
//                $this->request->data['type'] = null;
//            }
//
//            if ($this->request->data['type'] == 'google') {
////                $this->log('$this->request->data-------------');
////                $this->log($this->request->data);
//                $user_id = $this->User->findByEmail($this->request->data['User']['email']);
//                $user_id = $user_id['User']['id'];
//                /* Auto Login User */
////                $this->log($user_id);
//                $AuthUser = $this->User->find('first', array('contain' => array('UserInfo'), 'conditions' => array('User.id' => $user_id)));
////                $this->log('$AuthUser------------------');
////                $this->log($AuthUser);
//                /* login with google */
//
//                if ($this->Auth->login($AuthUser['User'])){
////                    $this->log('in user login--------------google----');
//                    $Event = new CakeEvent(
//                            'Controller.User.afterLogin', $this, array(
//                        'request' => $this->request,
//                        'user_id' => $user_id,
//                        'session_id' => CakeSession::id()
//                            )
//                    );
//
//                    $this->getEventManager()->dispatch($Event);
//
//
//                    $this->User->id = $user_id;
//                    $this->setTimeZone($this->request->data['User']['timezone']);
////                    $this->User->saveField('last_login', date('Y-m-d H:i:s'));
////                    /** serious doubts : why multiple time saving in just one row     * */
////                    $this->User->saveField('last_login_type', 'google');
////                    $this->User->saveField('email_token', NULL);
////                    $this->User->saveField('email_verified', 1);
////                    $this->User->saveField('email_token_expires', NULL);
//                    $set_default_value_of_user = array(
//                        'id' => $this->User->id,
//                        'last_login' => date('Y-m-d H:i:s'),
//                        'last_login_type' => 'google',
//                        'email_token' =>  NULL,
//                        'email_verified' => SET,
//                        'email_token_expires' => NULL
//                );
//                    $this->User->save($set_default_value_of_user, FALSE);
////                    $this->_setNetworkList($user_id);
//                    
//                    $this->Session->write('Auth.User.UserInfo', $AuthUser['UserInfo']);
////                    if ($this->here == $this->Auth->loginRedirect) {
////                        $this->Auth->loginRedirect = '/';
////                    }
//                }
//                return '/networks/index/1';
//            }
//
//            /* conventional login, when user clicks the button */
//
//            if ($this->Auth->login()) {
//
//                $Event = new CakeEvent(
//                        'Controller.User.afterLogin', $this, array(
//                    'request' => $this->request,
//                    'user_id' => $this->Auth->user('id'),
//                    'session_id' => CakeSession::id()
//                        )
//                );
//
//                $this->getEventManager()->dispatch($Event);
//                //Set the timezone of user
//                $this->setTimeZone($this->request->data['User']['timezone']);
//                $this->User->id = $this->Auth->user('id');
//                $set_default_value_of_user = array(
//                        'id' => $this->User->id,
//                        'last_login' => date('Y-m-d H:i:s'),
//                        'last_login_type' => 'gridle',
//                );
//                $this->User->save($set_default_value_of_user, FALSE);
////                $this->User->saveField('last_login', date('Y-m-d H:i:s'));
////                $this->User->saveField('last_login_type', 'gridle');
////                $this->log($this->_setNetworkList());
////                $this->log('$this->Auth->loginRedirect-------------');
////                $this->log($this->Auth->loginRedirect);
////                $this->log($this->here);
////                if ($this->here == $this->Auth->loginRedirect) {
////                    $this->Auth->loginRedirect = '/';
////                }
//
//                #for remember me(cookie)
////              $this->_setUserCookie();
//
//                if (empty($data[$this->modelClass]['return_to'])) {
//                    $data[$this->modelClass]['return_to'] = null;
//                }
//                $this->_getLastVisitedNetwork();
////                return $this->redirect('/networks/index/1');
//            } else {
//                $this->Auth->flash(__d('users', 'Invalid e-mail / password combination.  Please try again'));
//            }
//        }
////        $this->log($this->request->params['named']);
////        if (isset($this->request->params['named']['return_to'])) {
////            $this->set('return_to', urldecode($this->request->params['named']['return_to']));
////        } else {
////            $this->set('return_to', false);
////        }
////        $allowRegistration = Configure::read('Users.allowRegistration');
////        $this->log('$allowRegistration------------');
////        $this->log($allowRegistration);
////        $this->set('allowRegistration', (is_null($allowRegistration) ? true : $allowRegistration));
//    }

    /**
     * login method for users.
     * @return type
     */
    public function login(){
        
       if($this->_isUserLoggedIn()){
            $this->redirect('/');
        }
        
        if ($this->request->is('post') && !empty($this->request->data) && isset($this->request->data['User']['type'])) {
            $login_type = strtolower($this->request->data['User']['type']);
            
            switch($login_type){
           
                case GOOGLE:
                    $this->_loginWithGoogle();
                    break;
                
                case GRIDLE:
                    $this->_loginWithGridle(); 
                    break;
                
                default :
                    break;
            }
        }
    }
    
    /**
     * method used for login with google
     */
    protected function _loginWithGoogle(){
        
         $user_data = $this->User->findByEmail($this->request->data['User']['email']);
        
         if(isset($user_data['User']['id'])){
             $user_id = $user_data['User']['id'];
             $AuthUser = $this->User->find('first', array('conditions' => array('User.id' => $user_id)));
             
             if ($this->Auth->login($AuthUser['User'])){
                 
                 $set_default_value_of_user = array(
                        'id' => $this->Auth->user('id'),
                        'last_login' => date('Y-m-d H:i:s'),
                        'last_login_type' => 'google',
                        'email_token' =>  NULL,
                        'email_verified' => SET,
                        'email_token_expires' => NULL
                );
             
                $this->_insertDefaultValue($set_default_value_of_user);
             }
         }      
         return;
    }
    /**
     * method used for login with gridle
     */
    protected function _loginWithGridle(){
        if ($this->Auth->login()) {
            
            $set_default_value_of_user = array(
                    'id' => $this->Auth->user('id'),
                    'last_login' => date('Y-m-d H:i:s'),
                    'last_login_type' => 'gridle',
                );
            
                $this->_insertDefaultValue($set_default_value_of_user);
        } else {
            $this->Auth->flash(__d('users', 'Invalid e-mail / password combination.  Please try again'));
        }
        
        return;
    }
    /**
     * it stores default values into databse and call listener
     * @param type $set_default_value_of_user -> it is a set of userrs data that will store into database
     */
    protected function _insertDefaultValue($set_default_value_of_user = null){
        
        $this->User->save($set_default_value_of_user, FALSE);
        $this->setTimeZone($this->request->data['User']['timezone']);
        
        $Event = new CakeEvent(
                        'Controller.User.afterLogin', $this, array(
                    'request' => $this->request,
                    'user_id' => $this->Auth->user('id'),
                    'session_id' => CakeSession::id()
                        )
                );
        $this->getEventManager()->dispatch($Event);
        
        $User_info = $this->User->UserInfo->findByUserId($this->Auth->user('id'));
        $this->Session->write('Auth.User.UserInfo', $User_info['UserInfo']);
        
        $this->_getLastVisitedNetwork();
        return;
    }

    /**
     * #it checks whether user has previous network_id saved.
     * @return type
     */
    protected function _getLastVisitedNetwork(){
        
        $network_id = Hash::extract($this->User->find('first', array(
                            'conditions' => array('User.id' => $this->Auth->user('id')),
                            'fields' => array('User.last_network')
                        )), 'User.last_network');
        $this->loadModel('Network');
        $url = $this->Network->getNetworkField($network_id[0],'url');
        
        if(!empty($url)){
            return $this->redirect('/networks/switchNetwork/' . $url . '/desks');
        }else{
            return $this->redirect('/networks/index');
            }
    }
    
    /**
     * if remember me is set
     */
    protected function _setUserCookie(){
        $data = $this->request->data[$this->modelClass];
        
        if (!$this->request->data[$this->modelClass]['remember_me']) {
            //$this->RememberMe->destroyCookie();
            $this->_distroyCookie();
        } else {
            $this->request->data['User']['password'] = $this->Auth->password($this->request->data['User']['password']);
            $this->_setCookie();
        }
    }
    /**
     * Search - Requires the CakeDC Search plugin to work
     *
     * @throws MissingPluginException
     * @return void
     * @link https://github.com/CakeDC/search
     */
    protected function search() {

        $this->_pluginLoaded('Search');
        $searchTerm = '';
        $this->Prg->commonProcess($this->modelClass);

        $by = null;
        if (!empty($this->request->params['named']['search'])) {
            $searchTerm = $this->request->params['named']['search'];
            $by = 'any';
        }
        if (!empty($this->request->params['named']['username'])) {
            $searchTerm = $this->request->params['named']['username'];
            $by = 'username';
        }
        if (!empty($this->request->params['named']['email'])) {
            $searchTerm = $this->request->params['named']['email'];
            $by = 'email';
        }
        $this->request->data[$this->modelClass]['search'] = $searchTerm;

        $this->Paginator->settings = array(
            'search',
            'limit' => 12,
            'by' => $by,
            'search' => $searchTerm,
            'conditions' => array(
                'AND' => array(
                    $this->modelClass . '.active' => 1,
                    $this->modelClass . '.email_verified' => 1)));

        $this->set('users', $this->Paginator->paginate($this->modelClass));
        $this->set('searchTerm', $searchTerm);
    }

    /**
     * Common logout action
     *
     * @return void
     */
    public function logout() {

        $user = $this->Auth->user();
        $session_id_to_destroy = CakeSession::id();
        $this->_deleteSession($session_id_to_destroy);
        $this->Session->destroy();
        if (isset($_COOKIE[$this->Cookie->name])) {
            $this->Cookie->destroy();
        }
        $this->_distroyCookie();
//        //$this->RememberMe->destroyCookie();
//        $this->Session->setFlash(sprintf(__d('users', '%s you have successfully logged out'), $user['UserInfo']['first_name']));
        $this->redirect($this->Auth->logout());
    }

    protected function _deleteSession($session_id_to_destroy = null) {
        $this->loadModel('UserSession');
        $user_session = $this->UserSession->findBySessionId($session_id_to_destroy);
        if (isset($user_session['UserSession']['session_id'])) {
            return $this->UserSession->delete($user_session['UserSession']['id']);
        }
        return false;
    }

    /**
     * Checks if an email is already verified and if not renews the expiration time
     *
     * @return void
     */
    public function resend_verification() {
        if ($this->request->is('post')) {
            try {
                if ($this->{$this->modelClass}->checkEmailVerification($this->request->data)) {
                    $user = $this->User->findByEmail($this->request->data[$this->modelClass]['email']);


                    $email_data['url'] = Router::url(array('admin' => false, 'plugin' => 'users', 'controller' => 'users', 'action' => 'verify', 'email', $user[$this->modelClass]['email_token']), true);

                    /* Sending Verification Email Start */
                    $email_data = array(
                        'email' => $this->request->data[$this->modelClass]['email'],
                        'name' => $user['UserInfo']['name'],
                        'resend' => true,
                        'email_token' => $user[$this->modelClass]['email_token'],
                        'url' => Router::url(array('admin' => false, 'plugin' => 'users', 'controller' => 'users', 'action' => 'verify', 'email', $user[$this->modelClass]['email_token']), true)
                    );
                    /* Replacing Send Mail via CakeResque queue system */
                    CakeResque::enqueue('default', 'MailShell', array('sendVerificationEmail', $email_data));
                    /* Sending Email End */
                    //$this->_sendVerificationEmail($this->{$this->modelClass}->data);
                    $this->Session->setFlash(__d('users', 'The email has been sent again. Please check your inbox.'));
                    //$this->redirect('login');
                } else {
                    $this->Session->setFlash(__d('users', 'The email could not be sent. Something went wrong.'));
                }
            } catch (Exception $e) {
                $this->Session->setFlash($e->getMessage());
            }
        }
    }

    public function resend_invitation() {

        if ($this->request->is('post')) {

            $User = $this->User->find('first', array('conditions' => array('User.email' => $this->request->data['User']['email'], 'User.active' => 0)));

            if (count($User) > 0) {
                $this->loadModel('UserNetwork');

                $Networks = $this->Session->read('Networks');
                $authUser = $this->Auth->user('id');
                $network_id = $this->Session->read('CurrentNetwork');
                $role = $this->UserNetwork->getUserNetworkField($authUser, $network_id, 'role');

                /* Allowed roles to remove user */
                $allowed_role = array(ROLE_OWNER, ROLE_ADMIN);
                $usernetwork_id = $this->UserNetwork->getUserNetworkField($User['User']['id'], $network_id, 'id');

                if (in_array($role, $allowed_role) && $usernetwork_id > 0) {
                    $email_data = array(
                        'name' => $this->Auth->user('UserInfo.name'),
                        'email' => $User['User']['email'],
                        'network_name' => $Networks[$network_id]['name'],
                        'url' => Router::url(
                                array
                            (
                            'admin' => false,
                            'plugin' => 'users',
                            'controller' => 'users',
                            'action' => 'add', $User['User']['invite_token']
                                ), true
                        )
                    );

                    CakeResque::enqueue('default', 'MailShell', array('sendInvitationEmail', $email_data));
                    $this->Session->setFlash('User ReInvited');
                } else {
                    $this->Session->setFlash('User not part of network');
                }
            } else {
                $this->Session->setFlash('User not found');
            }
        }
    }

    /**
     * Confirm email action
     *
     * @param string $type Type, deprecated, will be removed. Its just still there for a smooth transistion.
     * @param string $token Token
     * @return void
     */
    public function verify($type = 'email', $token = null) {
        if ($type == 'reset') {
            // Backward compatiblity
            $this->request_new_password($token);
        }
        try {
            $this->{$this->modelClass}->verifyEmail($token);
            $this->Session->setFlash(__d('users', 'Congratulations, you are good to go.'));
            return $this->redirect('/');
        } catch (RuntimeException $e) {
            $this->Session->setFlash($e->getMessage());
            return $this->redirect('/');
        }
    }

    /**
     * This method will send a new password to the user
     *
     * @param string $token Token
     * @throws NotFoundException
     * @return void
     */
    public function request_new_password($token = null) {
        if (Configure::read('Users.sendPassword') !== true) {
            throw new NotFoundException();
        }

        $data = $this->{$this->modelClass}->verifyEmail($token);

        if (!$data) {
            $this->Session->setFlash(__d('users', 'The link has expired. Please request for new password again.'));
            return $this->redirect('/');
        }

        if ($this->{$this->modelClass}->save($data, array('validate' => false))) {
            $this->_sendNewPassword($data);
            $this->Session->setFlash(__d('users', 'Password reset instructions have been sent to your email.'));
            $this->redirect(array('action' => 'login'));
        }

        //When does it get fired : Anupama
        $this->Session->setFlash(__d('users', 'There was an error verifying your account. Please check the email you were sent, and retry the verification link.'));
        $this->redirect('/');
    }

    /**
     * Sends the password reset email
     *
     * @param array
     * @return void
     */
    protected function _sendNewPassword($userData) {

        $Email = $this->_getMailInstance();
        $Email->from(Configure::read('App.defaultEmail'))
                ->to($userData[$this->modelClass]['email'])
                ->replyTo(Configure::read('App.defaultEmail'))
                ->return(Configure::read('App.defaultEmail'))
                ->subject(env('HTTP_HOST') . ' ' . __d('users', 'Password Reset'))
                ->template($this->_pluginDot() . 'new_password')
                ->viewVars(array(
                    'model' => $this->modelClass,
                    'userData' => $userData))
                ->send();
    }

    /**
     * Allows the user to enter a new password, it needs to be confirmed by entering the old password
     *
     * @return void
     */
    public function change_password() {
        if ($this->request->is('post')) {
            $this->request->data[$this->modelClass]['id'] = $this->Auth->user('id');
            $token = $this->User->findById($this->request->data[$this->modelClass]['id'], array('User.password_token'));
            if (isset($token['User']['password_token'])) {
                $this->token_check($token['User']['password_token']);
//            $this->redirect(array('plugin' => 'users', 'controller' => 'users', 'action' => 'token_check', $token['User']['password_token']));
//            (array('admin' => false, 'plugin' => 'users', 'controller' => 'users', 'action' => 'reset_password', $this->{$this->modelClass}->data[$this->modelClass]['password_token']));
            }
            if ($this->{$this->modelClass}->changePassword($this->request->data)) {
                $this->Session->setFlash(__d('users', 'Your password has been changed successfully.'));
                /* Notify user for password change */
                CakeResque::enqueue('default', 'UserShell', array('onPasswordChange', array(
                        'id' => $this->Auth->user('id'),
                        'url' => Router::url(array('admin' => false, 'plugin' => 'users', 'controller' => 'users', 'action' => 'login'), true)
                )));
                /* Sending Email End */

                // we don't want to keep the cookie with the old password around
                //$this->RememberMe->destroyCookie();
                $this->_distroyCookie();
                //$this->redirect('/');
            }
        }
    }

    public function token_check($token = null) {
        $this->viewClass = 'Json';
//        $this->log('in token_check');
        $user_id = $this->Auth->user('id');
//        $this->log($user_id);
        $network_id = $this->Session->read('CurrentNetwork');
//        $this->log($token);
        if (isset($token)) {
//            $this->log($this->request->data);
//            $this->log('in set token');
            $this->_resetPassword($token);
            $this->Session->setFlash(__d('users', 'Your password has been set successfully.'));
        }

        $token = $this->User->findById($user_id, array('User.password_token'));
//        $this->log('on first time token check');
//        $this->log($token);
        if (isset($token['User']['password_token'])) {
            $returnObject['token_exists'] = 'true';
//            $returnObject['token'] = $token;
        } else {
            $returnObject['token_exists'] = 'false';
//            $returnObject['token'] = $token;
        }
        $this->set(array(
            'user' => $returnObject,
            '_serialize' => 'user'
        ));
    }

    /**
     * Reset Password Action
     *
     * Handles the trigger of the reset, also takes the token, validates it and let the user enter
     * a new password.
     *
     * @param string $token Token
     * @param string $user User Data
     * @return void
     */
    public function reset_password($token = null, $user = null) {
        if (empty($token)) {
            $admin = false;
            if ($user) {
                $this->request->data = $user;
                $admin = true;
            }
            $this->_sendPasswordReset($admin);
        } else {
            $this->_resetPassword($token);
        }
    }

    /**
     * Sets a list of languages to the view which can be used in selects
     *
     * @deprecated No fallback provided, use the Utils plugin in your app directly
     * @param string $viewVar View variable name, default is languages
     * @throws MissingPluginException
     * @return void
     * @link https://github.com/CakeDC/utils
     */
    protected function _setLanguages($viewVar = 'languages') {
        $this->_pluginLoaded('Utils');

        $Languages = new Languages();
        $this->set($viewVar, $Languages->lists('locale'));
    }

    /**
     * Checks if the email is in the system and authenticated, if yes create the token
     * save it and send the user an email
     *
     * @param boolean $admin Admin boolean
     * @param array $options Options
     * @return void
     */
    protected function _sendPasswordReset($admin = null, $options = array()) {
        if (!empty($this->request->data)) {
            $user = $this->{$this->modelClass}->passwordReset($this->request->data);
            if (!empty($user)) {
                /* Sending Password Reset Email Start */
                $email_data = array(
                    'email' => $user[$this->modelClass]['email'],
                    'url' => Router::url(array('admin' => false, 'plugin' => 'users', 'controller' => 'users', 'action' => 'reset_password', $this->{$this->modelClass}->data[$this->modelClass]['password_token']), true),
                );
                /* Replacing Send Mail via CakeResque queue system */
                CakeResque::enqueue('default', 'MailShell', array('sendPasswordReset', $email_data));
                /* Sending Email End */
                if ($admin) {
                    $this->Session->setFlash(sprintf(
                                    __d('users', '%s has been sent an email with instruction to reset their password.'), $user[$this->modelClass]['email']));
                    $this->redirect(array('action' => 'index', 'admin' => true));
                } else {
                    $this->Session->setFlash(__d('users', 'You should receive an email with further instructions shortly'));
                    $this->redirect(array('action' => 'login'));
                }
            } else {
                $this->Session->setFlash(__d('users', 'No user was found with that email.'));
//                $this->redirect($this->referer('/'));
            }
        }
        $this->render('request_password_change');
    }

    /**
     * Sets the cookie to remember the user
     *
     * @param array RememberMe (Cookie) component properties as array, like array('domain' => 'yourdomain.com')
     * @param string Cookie data keyname for the userdata, its default is "User". This is set to User and NOT using the model alias to make sure it works with different apps with different user models across different (sub)domains.
     * @return void
     * @link http://book.cakephp.org/2.0/en/core-libraries/components/cookie.html
     */
    protected function _setCookie($options = array(), $cookieKey = 'rememberMe') {
//        $this->RememberMe->settings['cookieKey'] = $cookieKey;
//        $this->RememberMe->configureCookie($options);
//        $this->RememberMe->setCookie();

        $this->Cookie->write($cookieKey, $this->request->data, true, $this->cookieLifeTime);
    }

    protected function _distroyCookie($options = array(), $cookieKey = 'rememberMe') {
        $this->Cookie->delete($cookieKey);
    }

    protected function _cookieIsSet($cookieKey = 'rememberMe') {
        $cookie = $this->Cookie->read($cookieKey);
        return (!empty($cookie));
    }

    /**
     * This method allows the user to change his password if the reset token is correct
     *
     * @param string $token Token
     * @return void
     */
    protected function _resetPassword($token) {
        if (empty($this->request->data['User']['old_password'])) {
            $user = $this->{$this->modelClass}->checkPasswordTokengoogle($token);
        } else {
            $user = $this->{$this->modelClass}->checkPasswordToken($token);
        }
        if (empty($user)) {
            $this->Session->setFlash(__d('users', 'Invalid password reset token, try again.'));
            $this->redirect(array('action' => 'reset_password'));
        }
        if (!empty($this->request->data) && $this->{$this->modelClass}->resetPassword(Set::merge($user, $this->request->data))) {
            /* Notify user for password change */
            CakeResque::enqueue('default', 'UserShell', array('onPasswordChange', array(
                    'id' => $user['User']['id'],
                    'url' => Router::url(array('admin' => false, 'plugin' => 'users', 'controller' => 'users', 'action' => 'login'))
            )));
            /* Sending Email End */
            if ($this->_cookieIsSet()) {
                $this->Session->setFlash(__d('users', 'Your password has been changed successfully.'));
                $this->_setCookie();
            } else {
                $this->Session->setFlash(__d('users', 'Your password was changed. Please login with new password.'));
                $this->redirect($this->Auth->loginAction);
            }
        }

        $this->set('token', $token);
    }

    /**
     * Returns a CakeEmail object
     *
     * @return object CakeEmail instance
     * @link http://book.cakephp.org/2.0/en/core-utility-libraries/email.html
     */
    protected function _getMailInstance() {
        $emailConfig = Configure::read('Users.emailConfig');
        if ($emailConfig) {
            return new CakeEmail($emailConfig);
        } else {
            return new CakeEmail('default');
        }
    }

    /**
     * Default isAuthorized method
     *
     * This is called to see if a user (when logged in) is able to access an action
     *
     * @param array $user
     * @return boolean True if allowed
     * @link http://book.cakephp.org/2.0/en/core-libraries/components/authentication.html#using-controllerauthorize
     */
    public function isAuthorized($user = null) {
        return parent::isAuthorized($user);
    }

    public function email_settings() {

        $email_settings = $this->User->EmailSetting->findByUserId($this->Auth->user('id'));

        if ($this->request->is('post')) {
            $this->User->EmailSetting->id = $email_settings['EmailSetting']['id'];
            $this->request->data['EmailSetting']['email'] = $this->Auth->user('email');
            if ($this->User->EmailSetting->save($this->request->data)) {
                $this->Session->setFlash(__d('users', 'Your email preferences has been changed successfully.'));
            } else {
                $this->Session->setFlash(__d('users', 'Some error occured'));
            }
            return $this->redirect($this->referer());
        }

        if (count($email_settings) > 0) {
            $this->set('EmailSetting', $email_settings);
        } else {
            $this->User->EmailSetting->create();
            $this->User->EmailSetting->set('user_id', $this->Auth->user('id'));
            $this->User->EmailSetting->set('email', $this->Auth->user('email'));
            $this->User->EmailSetting->save();
            $this->set('EmailSetting', $this->User->EmailSetting->findByUserId($this->Auth->user('id')));
        }
    }

    public function sessions($user_session_id = null) {
        $this->loadModel('UserSession');
        /* To get Browser information , php_browser.ini should be included in php.ini */
        // $browser = get_browser(null, true);

        if (isset($user_session_id)) {


            $UserSession = $this->UserSession->findByIdAndUserId($user_session_id, $this->Auth->user('id'));
            if (isset($UserSession['UserSession']['session_id'])) {


                $session_id_to_destroy = $UserSession['UserSession']['session_id'];
                // 1. commit session if it's started.
                if (session_id()) {
                    session_commit();
                }

                // 2. store current session id
                session_start();
                $current_session_id = session_id();
                session_commit();

                // 3. hijack then destroy session specified.
                session_id($session_id_to_destroy);
                session_start();
                session_destroy();
                session_commit();

                // 4. restore current session id. If don't restore it, your current session will refer to the session you just destroyed!
                if ($current_session_id != $session_id_to_destroy) {
                    session_id($current_session_id);
                    session_start();
                    session_commit();
                }

                if ($this->UserSession->delete($user_session_id)) {
                    if ($current_session_id != $session_id_to_destroy) {
                        return $this->redirect('/users/sessions');
                    } else {
                        return $this->redirect('/users/sessions');
                        // return $this->redirect('/users/logout');
                    }
                }
            }
        }

        $this->set('sessions', $this->UserSession->findAllByUserId($this->Auth->user('id')));
    }

    public function unsubscribe($email_encode_url_safe, $signature) {

        $email_encode = strtr($email_encode_url_safe, "-_", "+/");

        $key = Configure::read('mcrypt_key');

        $email = rtrim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, md5($key), base64_decode($email_encode), MCRYPT_MODE_CBC, md5(md5($key))), "\0");

        $email_encode = base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($key), $email, MCRYPT_MODE_CBC, md5(md5($key))));
        $signature_to_check = HASH_HMAC('md5', $email_encode . $email, Configure::read('secret_key'));


        $email_setting = $this->User->EmailSetting->findByEmail($email);
        $fields_to_remove = array('id', 'user_id', 'email', 'created', 'modified');
        $fields = array_keys($this->User->EmailSetting->getColumnTypes());
        $fields_to_set = array_diff($fields, $fields_to_remove);
        if (empty($email_setting)) {
            $user = $this->User->findByEmail($email);
            $this->User->EmailSetting->create();

            $this->User->EmailSetting->set('email', $email);
            $this->User->EmailSetting->set('user_id', $user['User']['id']);
        } else {
            $this->User->EmailSetting->id = $email_setting['EmailSetting']['id'];
        }
        foreach ($fields_to_set as $value) {
            $this->User->EmailSetting->set($value, false);
        }

        if ($this->User->EmailSetting->save()) {
            $this->Session->setFlash(__d('users', 'You have been unsubscribe successfully'));
        } else {
            $this->Session->setFlash(__d('users', 'Some error occured!'));
        }
        return $this->redirect($this->referer());
    }

    /*     * **** MOBILE APP FUNCTIONS   ******************** */

    public function api_login() {
        $this->viewClass = 'Json';
        $this->request->onlyAllow('post');

        $data = $this->request->input();
        $data = json_decode($data, true);

        //make a user object from request data
        $user = array();
        $this->request->data['User']['email'] = $data['email'];
        if (array_key_exists('password', $data)) {
            $this->request->data['User']['password'] = $data['password'];
        }
        //check for the password
//        $this->log("Checking Login");
        if ($this->Auth->login()) {
//            $this->log("Checking Login Success");
            $user = $this->Auth->user();
//            $this->log("Checking Login Auth Success");
            //unset unnecessary user details
            unset($user['username']);
            unset($user['gender']);
            unset($user['contact_no']);
            unset($user['last_action']);
            unset($user['role']);
            unset($user['email_token']);
            unset($user['email_token_expires']);
            unset($user['invite_token']);
            unset($user['password_token']);
            unset($user['created']);
            unset($user['modified']);
            // //unset unnecessary user info
            unset($user['UserInfo']['id']);

            unset($user['UserInfo']['dob']);
            unset($user['UserInfo']['isDeleted']);
            unset($user['UserInfo']['created']);
            unset($user['UserInfo']['modified']);
            unset($user['UserInfo']['deleted']);
//            unset email settigs
            unset($user['EmailSetting']);

//            $Event = new CakeEvent(
//                        'Controller.User.afterLogin', $this, array('request' => $this->request, 'user_id' => $this->Auth->user('id'), 'session_id' => CakeSession::id()));
//            $this->getEventManager()->dispatch($Event);
//
            $returnObject['status'] = 'success';
//            $returnObject['data'] = $this->request->data['email'];
            $returnObject['data'] = $user;
        } else {
            $returnObject['status'] = 'error';
            $returnObject['message'] = 'Unable to login! Try Again Later.';
            //$returnObject['data'] = $this->request->data['email'];
        }

        //      send the response
//        $returnObject['status'] = 'success';
//        $returnObject['data'] = $this->request->data['email'];

        $this->set(array(
            'data' => $returnObject,
            '_serialize' => 'data'
        ));
    }

    /**
     * Function to login/sign-up using Google in Android Mobile App
     */
    public function apiGoogleLogin() {
        $this->viewClass = 'Json';

        $this->User->validator()->remove('password');

        $this->request->allowMethod('post');

        if ($this->request->is('post')) {

            $data = $this->request->input();
            $data = json_decode($data, true);

            if (!empty($data)) {

                $email = strtolower($data['email']);
                $first_name = $data['first_name'];
                $last_name = $data['last_name'];

                if (!empty($this->User->fetchEmail($email))) {


                    $this->authUser = $this->User->findByEmail($email);

                    if ($this->authUser) {

                        $returnObject['status'] = 'success';

                        $returnObject['data'] = $this->authUser;
                        $returnObject['data']['id'] = $this->authUser['User']['id'];
                        $returnObject['data']['email'] = $this->authUser['User']['email'];
                        $this->set(array(
                            'data' => $returnObject,
                            '_serialize' => 'data'
                        ));
                    }

                    return;
                } else {
                    //User is new user, trying to register for the first time
                    $this->log("NEW REGISTRATION FROM GOOGLE LOGIN");
                    $data['User']['password'] = $this->User->generatePassword();
                    $parts = explode('@', $email);
                    $slug = preg_replace("/[^0-9a-zA-Z]/m", "", $parts[0]);
//                    $this->log("1");
//                    $this->log($slug);
                    $slug = preg_replace("/ /", "-", $slug);
//                    $this->log("2");
//                    $this->log($slug);
                    $data['User']['username'] = $this->User->makeUniqueSlug($slug);
//                    $this->log("3");
//                    $this->log($data['User']['username']);
//                    $data['User']['last_login_type'] = 'google';
                    $data['User']['email'] = $email;
                    $data['User']['timezone'] = 0;
                    $current_date = new DateTime();
                    $data['User']['created'] = $current_date->format('Y-m-d H:i:00');
//                    $data['User']['modified'] = $current_date->format('Y-m-d H:i:00');
//                    $data['User']['email_token'] = null;
//                    $data['User']['email_verified'] = 1;
//                    $data['User']['email_token_expires'] = null;

                    $data['UserInfo']['first_name'] = $first_name;
                    $data['UserInfo']['last_name'] = $last_name;
                    $data['UserInfo']['email'] = $email;
//                    $this->log("DATA LOG FROM NEW REGISTARTION");
//                    $this->log($data);
                    if ($this->User->UserInfo->validates()) {
//                        $this->log("Inside Validate");
                        $user = $this->{$this->modelClass}->register($data); //, false);//array('emailVerification' => 'false'));    //Save the data                    
                        $this->User->saveField('email_token', NULL);
                        $this->User->saveField('email_verified', 1);
                        $this->User->saveField('email_token_expires', NULL);
                        $this->User->saveField('last_login_type', 'google');
                        $user['User']['email_token'] = NULL;
//                        $this->log("User->regis() Over");
//                        $this->log($user);
                    } else {
                        $returnObject['status'] = 'error';
                        $returnObject['message'] = 'The user could not be saved. Please try again.';
                        $this->set(array(
                            'data' => $returnObject,
                            '_serialize' => 'data'
                        ));
                        return;
                    }

                    if (isset($user) && $user !== false) {
                        $user['User']['name'] = $data['UserInfo']['first_name'] . " " . $data['UserInfo']['last_name'];
                        /* create an event after reg. for email settings and mail */
                        $Event = new CakeEvent(
                                'Controller.User.afterRegistration', $this, $user);
                        $this->getEventManager()->dispatch($Event);
                        if ($Event->isStopped()) {
                            $returnObject['status'] = 'error';
                            $returnObject['message'] = 'The user could not be saved. Please try again.';
                            $this->set(array(
                                'data' => $returnObject,
                                '_serialize' => 'data'
                            ));
                            return;
                        }

                        $this->request->data = $data;
                        $this->_addUserInfo($user[$this->modelClass]['id']);
//                        $this->log("UserInfo added");
                        App::import('Controller', 'Networks');
                        $networks = new NetworksController();
                        $result = $networks->createNetworkMobile($user[$this->modelClass]['id']);
//                        $this->log($result);
                        if ($result) {
//                            $this->log("Register success, now login");
//                            $this->getEventManager()->dispatch($Event);
//                            $returnObject['status'] = 'success';
//                            $returnObject['message'] = 'Network Created Successfully.';
//                            $this->set(array(
//                                    'data' => $returnObject,
//                                    '_serialize' => 'data'
//                                ));
                            $this->api_login();
                        } else {
//                            $this->log("Network not created");
                            $returnObject['status'] = 'error';
                            $returnObject['message'] = 'Failed to create network.';
                            $this->set(array(
                                'data' => $returnObject,
                                '_serialize' => 'data'
                            ));
                            return;
                        }
                    } else {
                        $returnObject['status'] = 'error';
                        $returnObject['message'] = 'Your account could not be created. Please, try again.';
                        $this->set(array(
                            'data' => $returnObject,
                            '_serialize' => 'data'
                        ));
                        return;
                    }
                }
            }
        }
    }

}

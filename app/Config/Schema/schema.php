<?php

/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Config.Schema
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
/**
 *
 * Using the Schema command line utility
 *
 * Use it to configure database for Gridle
 *
 */
/*
 * During schema update with new table if Cakephp throws an error of Database missing in datasource
 * And Cache='Redis'
 * Then, go to terminal and  type :
 * redis-cli --raw keys "{{$PREFIX}}cake_model*" | xargs redis-cli DEL
 * where $PREFIX = variable in core.php
 */

App::uses('ClassRegistry', 'Utility');
App::uses('Plan', 'Model');
App::uses('EmailSetting', 'Model');

class masterSchema extends CakeSchema {

    public $name = 'master';

    public function before($event = array()) {
        $db = ConnectionManager::getDataSource($this->connection);
        $db->cacheSources = false;
        return true;
    }

    public function after($event = array()) {
        if (isset($event['create'])) {
            switch ($event['create']) {
                case "plans":
                    $this->InsertDefaultPlans();
                case "email_settings":
//                    $this->InsertDefaultEmailSettings();
            }
        }
    }

    /*
     * Schema for users table
     */

    public $users = array(
        'id' => array('type' => 'integer', 'null' => false, 'length' => 9, 'key' => 'primary'),
        'username' => array('type' => 'string', 'null' => false, 'length' => 20, 'key' => 'index'),
        'email' => array('type' => 'string', 'null' => false, 'length' => 320, 'key' => 'index'),
        'email_verified' => array('type' => 'boolean', 'null' => false, 'default' => '0'),
        'email_token' => array('type' => 'string', 'null' => false, 'length' => 128),
        'email_token_expires' => array('type' => 'datetime', 'null' => false),
        'invite_token' => array('type' => 'string', 'null' => true, 'length' => 128),
        'primary_network_id' => array('type' => 'integer', 'null' => true, 'length' => 9),
        'password' => array('type' => 'string', 'null' => false, 'length' => 128),
        'password_token' => array('type' => 'string', 'null' => true, 'length' => 128),
        'last_login' => array('type' => 'datetime', 'null' => true, 'default' => NULL),
        'last_network' => array('type' => 'integer', 'null' => true, 'length' => 9),
//        'last_action' => array('type' => 'datetime', 'null' => true, 'default' => NULL),
        'last_login_type' => array('type' => 'string', 'null' => false, 'default' => null),
        'active' => array('type' => 'boolean', 'null' => false, 'default' => '0'),
        'role' => array('type' => 'string', 'null' => false, 'default' => ROLE_USER),
        'created' => array('type' => 'datetime', 'null' => true, 'default' => NULL),
        'modified' => array('type' => 'datetime', 'null' => true, 'default' => NULL),
        'indexes' => array('PRIMARY' => array('column' => 'id', 'unique' => 1), 'BY_USERNAME' => array('column' => array('username'), 'unique' => 1), 'BY_EMAIL' => array('column' => array('email'), 'unique' => 1))
    );
    
//    public $user_regids = array(
//        'id' => array('type' => 'integer', 'null' => false, 'length' => 9, 'key' => 'primary'),
//        'registration_id' => array('type' => 'string', 'null' => false, 'key' => 'index'),
//        'user_id' => array('type' => 'integer', 'null' => false, 'length' => 9, 'key' => 'index'),
//        'username' => array('type' => 'string', 'null' => false, 'length' => 20, 'key' => 'index'),
//        'email' => array('type' => 'string', 'null' => false, 'length' => 320, 'key' => 'index')
//    );

    
    public $networks = array(
        'id' => array('type' => 'integer', 'null' => false, 'length' => 9, 'key' => 'primary'),
        'url' => array('type' => 'string', 'null' => false, 'length' => 20, 'key' => 'index'),
        'name' => array('type' => 'string', 'null' => false, 'length' => 30, 'key' => 'index'),
        'user_count' => array('type' => 'integer', 'null' => false, 'length' => 9),
        'group_count' => array('type' => 'integer', 'null' => false, 'length' => 9),
        'file_count' => array('type' => 'biginteger', 'null' => false, 'length' => 18),
        'user_limit' => array('type' => 'integer', 'null' => false, 'length' => 9),
        'file_limit' => array('type' => 'biginteger', 'null' => false, 'length' => 18),
        'created_by' => array('type' => 'integer', 'null' => false, 'length' => 9, 'key' => 'index'),
        'ip_check' => array('type' => 'boolean', 'null' => true, 'default' => NULL),
        'status' => array('type' => 'string', 'null' => false),
        'isDeleted' => array('type' => 'boolean', 'null' => false, 'default' => 0),
        'expires' => array('type' => 'datetime', 'null' => false),
        'created' => array('type' => 'datetime', 'null' => false),
        'deleted' => array('type' => 'datetime', 'null' => true, 'default' => NULL),
        'modified' => array('type' => 'datetime', 'null' => false),
        'indexes' => array('PRIMARY' => array('column' => 'id', 'unique' => 1), 'BY_URL' => array('column' => array('url'), 'unique' => 1), 'BY_CREATED_BY' => array('column' => array('created_by'), 'unique' => 0))
    );
    
    public $user_networks = array(
        'id' => array('type' => 'integer', 'null' => false, 'length' => 9, 'key' => 'primary'),
        'user_id' => array('type' => 'integer', 'null' => false, 'length' => 9, 'key' => 'index', 'comments' => 'user of the user and a foreign key'),
        'network_id' => array('type' => 'integer', 'null' => false, 'length' => 9, 'key' => 'index'),
        'file_count' => array('type' => 'biginteger', 'null' => false, 'length' => 18),
        'file_limit' => array('type' => 'biginteger', 'null' => false, 'length' => 18),
        'unseen_notification' => array('type' => 'integer', 'null' => false, 'length' => 9),
        'lastseen_feed' => array('type' => 'integer', 'null' => false, 'length' => 9),
        'role' => array('type' => 'string', 'null' => false),
        'status' => array('type' => 'string', 'null' => false),
        'added_by' => array('type' => 'integer', 'null' => false, 'length' => 9),
        'removed_by' => array('type' => 'integer', 'null' => false, 'length' => 9),
        'isDeleted' => array('type' => 'boolean', 'null' => false, 'default' => 0),
        'created' => array('type' => 'datetime', 'null' => false),
        'deleted' => array('type' => 'datetime', 'null' => true, 'default' => NULL),
        'modified' => array('type' => 'datetime', 'null' => false),
        'indexes' => array('PRIMARY' => array('column' => 'id', 'unique' => 1), 'BY_USER_ID' => array('column' => 'user_id', 'unique' => 0), 'BY_NETWORK_ID' => array('column' => 'network_id', 'unique' => 0))
    );
    public $network_access = array(
        'id' => array('type' => 'integer', 'null' => false, 'length' => 9, 'key' => 'primary'),
        'name' => array('type' => 'string', 'null' => false),
        'network_id' => array('type' => 'integer', 'null' => false, 'length' => 9),
        'ip_address' => array('type' => 'string', 'null' => true),
        'role' => array('type' => 'string', 'null' => false),
        'user_id' => array('type' => 'integer', 'null' => true, 'length' => 9),
        'isDeleted' => array('type' => 'boolean', 'null' => false, 'default' => 0),
        'deleted' => array('type' => 'datetime', 'null' => true, 'default' => NULL),
        'created' => array('type' => 'datetime', 'null' => false),
        'indexes' => array(
            'PRIMARY' => array('column' => 'id', 'unique' => 1),
            'BY_NETWORK_ID' => array('column' => 'network_id', 'unique' => 0)
        )
    );
    public $user_infos = array(
        'id' => array('type' => 'integer', 'null' => false, 'length' => 9, 'key' => 'primary'),
        'user_id' => array('type' => 'integer', 'null' => false, 'length' => 9, 'key' => 'index'),
        'first_name' => array('type' => 'string', 'null' => true, 'default' => NULL, 'length' => 35),
        'last_name' => array('type' => 'string', 'null' => true, 'default' => NULL, 'length' => 35),
        'email' => array('type' => 'string', 'null' => false, 'length' => 320),
        'skype' => array('type' => 'string', 'null' => true, 'length' => 60),
        'gender' => array('type' => 'boolean', 'null' => true, 'default' => 0),
        'designation' => array('type' => 'string', 'null' => true, 'default' => NULL, 'length' => 200),
        'company' => array('type' => 'string', 'null' => true, 'default' => NULL, 'length' => 200),
        'photo' => array('type' => 'string', 'null' => false, 'default' => 'default.jpg', 'length' => 200),
        'photo_dir' => array('type' => 'string', 'null' => false, 'default' => 'default', 'length' => 200),
        'dob' => array('type' => 'date', 'null' => true, 'default' => NULL),
        'location' => array('type' => 'string', 'null' => true, 'default' => NULL, 'length' => 100),
        'contact_no' => array('type' => 'string', 'null' => true, 'default' => NULL, 'length' => 22),
        'isDeleted' => array('type' => 'boolean', 'null' => false, 'default' => 0),
        'created' => array('type' => 'datetime', 'null' => false),
        'deleted' => array('type' => 'datetime', 'null' => true, 'default' => NULL),
        'modified' => array('type' => 'datetime', 'null' => false),
        'indexes' => array('PRIMARY' => array('column' => 'id', 'unique' => 1), 'BY_USER_ID' => array('column' => 'user_id', 'unique' => 0))
    );
    public $user_sessions = array(
        'id' => array('type' => 'integer', 'null' => false, 'length' => 9, 'key' => 'primary'),
        'user_id' => array('type' => 'integer', 'null' => false, 'length' => 9, 'key' => 'index'),
        'ip' => array('type' => 'string', 'null' => true, 'default' => NULL, 'length' => 30),
        'user_agent' => array('type' => 'string', 'null' => true, 'default' => NULL, 'length' => 400),
        'session_id' => array('type' => 'string', 'null' => true, 'default' => NULL, 'length' => 400),
        'valid' => array('type' => 'boolean', 'null' => false, 'default' => 0),
        'created' => array('type' => 'datetime', 'null' => false),
        'indexes' => array('PRIMARY' => array('column' => 'id', 'unique' => 1), 'BY_USER_ID' => array('column' => 'user_id', 'unique' => 0))
    );
    public $groups = array(
        'id' => array('type' => 'integer', 'null' => false, 'length' => 9, 'key' => 'primary'),
        'network_id' => array('type' => 'integer', 'null' => false, 'length' => 9, 'key' => 'index'),
        'name' => array('type' => 'string', 'null' => false, 'length' => 30),
        'description' => array('type' => 'string', 'null' => true, 'default' => NULL, 'length' => 100),
        'admin' => array('type' => 'integer', 'null' => false, 'length' => 9, 'key' => 'index'),
        'users_group_count' => array('type' => 'integer', 'null' => false, 'length' => 5),
        'status' => array('type' => 'string', 'null' => false),
        'isDeleted' => array('type' => 'boolean', 'null' => false, 'default' => 0),
        'created' => array('type' => 'datetime', 'null' => false),
        'deleted' => array('type' => 'datetime', 'null' => true, 'default' => NULL),
        'modified' => array('type' => 'datetime', 'null' => false),
        'indexes' => array('PRIMARY' => array('column' => 'id', 'unique' => 1), 'BY_NETWORK_ID' => array('column' => 'network_id', 'unique' => 0), 'BY_ADMIN' => array('column' => 'admin', 'unique' => 0))
    );
    public $user_groups = array(
        'id' => array('type' => 'integer', 'null' => false, 'length' => 9, 'key' => 'primary'),
        'user_id' => array('type' => 'integer', 'null' => false, 'length' => 9, 'key' => 'index'),
        'group_id' => array('type' => 'integer', 'null' => false, 'length' => 9, 'key' => 'index'),
        'network_id' => array('type' => 'integer', 'null' => false, 'length' => 9, 'key' => 'index'),
        'added_by' => array('type' => 'integer', 'null' => false, 'length' => 9, 'key' => 'index'),
        'removed_by' => array('type' => 'integer', 'null' => false, 'length' => 9, 'key' => 'index'),
        'role' => array('type' => 'string', 'null' => false),
        'status' => array('type' => 'string', 'null' => false),
        'isDeleted' => array('type' => 'boolean', 'null' => false, 'default' => 0),
        'created' => array('type' => 'datetime', 'null' => false),
        'deleted' => array('type' => 'datetime', 'null' => true, 'default' => NULL),
        'modified' => array('type' => 'datetime', 'null' => false),
        'indexes' => array('PRIMARY' => array('column' => 'id', 'unique' => 1), 'BY_USER_ID' => array('column' => 'user_id', 'unique' => 0), 'BY_GROUP_ID' => array('column' => 'group_id', 'unique' => 0), 'BY_NETWORK_ID' => array('column' => 'network_id', 'unique' => 0), 'BY_ADDED_BY' => array('column' => 'added_by', 'unique' => 0), 'BY_REMOVED_BY' => array('column' => 'removed_by', 'unique' => 0))
    );
    public $messages = array(
        'id' => array('type' => 'integer', 'null' => false, 'length' => 9, 'key' => 'primary'),
        'conversation_id' => array('type' => 'integer', 'null' => false, 'length' => 9, 'key' => 'index'),
        'user_id' => array('type' => 'integer', 'null' => false, 'length' => 9, 'key' => 'index'),
        'body' => array('type' => 'string', 'null' => false, 'length' => 1000),
        'isDeleted' => array('type' => 'boolean', 'null' => false, 'default' => 0),
        'created' => array('type' => 'datetime', 'null' => false),
        'deleted' => array('type' => 'datetime', 'null' => true, 'default' => NULL),
        'indexes' => array('PRIMARY' => array('column' => 'id', 'unique' => 1), 'BY_USER_ID' => array('column' => 'user_id', 'unique' => 0), 'BY_CONVERSATION_ID' => array('column' => 'conversation_id', 'unique' => 0))
    );
    public $conversations = array(
        'id' => array('type' => 'integer', 'null' => false, 'length' => 9, 'key' => 'primary'),
        'network_id' => array('type' => 'integer', 'null' => false, 'length' => 9, 'key' => 'index'),
        'title' => array('type' => 'string', 'null' => true, 'default' => NULL, 'length' => 30),
        'user_id' => array('type' => 'integer', 'null' => false, 'length' => 9, 'key' => 'index'),
        'group_id' => array('type' => 'integer', 'null' => true, 'default' => NULL, 'length' => 9, 'key' => 'index'),
        'mapping' => array('type' => 'string', 'null' => false, 'length' => 20),
        'status' => array('type' => 'string', 'null' => false, 'length' => 20),
        'user_conversation_count' => array('type' => 'integer', 'null' => false, 'length' => 9),
        'message_count' => array('type' => 'integer', 'null' => false, 'length' => 9),
        'isDeleted' => array('type' => 'boolean', 'null' => false, 'default' => 0),
        'created' => array('type' => 'datetime', 'null' => false),
        'deleted' => array('type' => 'datetime', 'null' => true, 'default' => NULL),
        'modified' => array('type' => 'datetime', 'null' => false),
        'indexes' => array('PRIMARY' => array('column' => 'id', 'unique' => 1), 'BY_USER_ID' => array('column' => 'user_id', 'unique' => 0), 'BY_GROUP_ID' => array('column' => 'group_id', 'unique' => 0), 'BY_NETWORK_ID' => array('column' => 'network_id', 'unique' => 0))
    );
    public $user_conversations = array(
        'id' => array('type' => 'integer', 'null' => false, 'length' => 9, 'key' => 'primary'),
        'conversation_id' => array('type' => 'integer', 'null' => false, 'length' => 9, 'key' => 'index'),
        'network_id' => array('type' => 'integer', 'null' => false, 'length' => 9, 'key' => 'index'),
        'user_id' => array('type' => 'integer', 'null' => true, 'default' => NULL, 'length' => 9, 'key' => 'index'),
        'group_id' => array('type' => 'integer', 'null' => true, 'default' => NULL, 'length' => 9, 'key' => 'index'),
        'role' => array('type' => 'string', 'null' => false, 'length' => 20),
        'status' => array('type' => 'string', 'null' => false, 'length' => 20),
        'isUnread' => array('type' => 'boolean', 'null' => false, 'default' => 0),
        'isDeleted' => array('type' => 'boolean', 'null' => false, 'default' => 0),
        'created' => array('type' => 'datetime', 'null' => false),
        'deleted' => array('type' => 'datetime', 'null' => true, 'default' => NULL),
        'modified' => array('type' => 'datetime', 'null' => false),
        'indexes' => array('PRIMARY' => array('column' => 'id', 'unique' => 1), 'BY_USER_ID' => array('column' => 'user_id', 'unique' => 0), 'BY_GROUP_ID' => array('column' => 'group_id', 'unique' => 0), 'BY_NETWORK_ID' => array('column' => 'network_id', 'unique' => 0), 'BY_CONVERSATION_ID' => array('column' => 'conversation_id', 'unique' => 0))
    );
    public $docs = array(
        'id' => array('type' => 'integer', 'null' => false, 'length' => 9, 'key' => 'primary'),
        'name' => array('type' => 'string', 'null' => false, 'length' => 255),
        'hash_name' => array('type' => 'string', 'null' => false, 'length' => 128, 'key' => 'index'),
        'file_size' => array('type' => 'integer', 'null' => false, 'length' => 9),
        'file_extension' => array('type' => 'string', 'null' => false, 'length' => 15),
        'file_mime' => array('type' => 'string', 'null' => true, 'default' => NULL, 'length' => 128),
        'user_id' => array('type' => 'integer', 'null' => false, 'length' => 9, 'key' => 'index'),
        'network_id' => array('type' => 'integer', 'null' => false, 'length' => 9, 'key' => 'index'),
        'bucket_path' => array('type' => 'string', 'null' => false, 'length' => 255),
        'preview_url'=>array('type'=>'text','null'=>true),
        'thumbnailLink' => array('type' => 'string', 'null' => true, 'length' => 255),
        'doc_assignment_count' => array('type' => 'integer', 'null' => false, 'length' => 9, 'default' => 0),
        'provider' => array('type' => 'string', 'null' => false, 'length' => 30, 'default' => 's3'),
        'comment_count' => array('type' => 'integer', 'null' => false, 'default' => 0, 'length' => 9),
        'isDeleted' => array('type' => 'boolean', 'null' => false, 'default' => 0),
        'created' => array('type' => 'datetime', 'null' => false),
        'deleted' => array('type' => 'datetime', 'null' => true, 'default' => NULL),
        'modified' => array('type' => 'datetime', 'null' => false),
        'indexes' => array('PRIMARY' => array('column' => 'id', 'unique' => 1), 'BY_HASH_NAME' => array('column' => 'hash_name', 'unique' => 1), 'BY_USER_ID' => array('column' => 'user_id', 'unique' => 0), 'BY_NETWORK_ID' => array('column' => 'network_id', 'unique' => 0))
    );
    public $docs_assignment = array(
        'id' => array('type' => 'integer', 'null' => false, 'length' => 9, 'key' => 'primary'),
        'doc_id' => array('type' => 'integer', 'null' => false, 'length' => 9, 'key' => 'index'),
        'group_id' => array('type' => 'integer', 'null' => true, 'default' => NULL, 'length' => 9, 'key' => 'index'),
        'user_id' => array('type' => 'integer', 'null' => true, 'default' => NULL, 'length' => 9, 'key' => 'index'),
        'shared_by' => array('type' => 'integer', 'null' => false, 'length' => 9, 'key' => 'index'),
        'removed_by' => array('type' => 'integer', 'null' => true, 'default' => NULL, 'length' => 9, 'key' => 'index'),
        'network_id' => array('type' => 'integer', 'null' => false, 'length' => 9, 'key' => 'index'),
        'status' => array('type' => 'string', 'null' => false, 'length' => 20),
        'comment_count' => array('type' => 'integer', 'null' => false, 'default' => 0, 'length' => 9),
        'isDeleted' => array('type' => 'boolean', 'null' => false, 'default' => 0),
        'created' => array('type' => 'datetime', 'null' => false),
        'deleted' => array('type' => 'datetime', 'null' => true, 'default' => NULL),
        'modified' => array('type' => 'datetime', 'null' => false),
        'indexes' => array('PRIMARY' => array('column' => 'id', 'unique' => 1), 'BY_DOC_ID' => array('column' => 'doc_id', 'unique' => 0), 'BY_GROUP_ID' => array('column' => 'group_id', 'unique' => 0), 'BY_USER_ID' => array('column' => 'user_id', 'unique' => 0), 'BY_SHARED_BY' => array('column' => 'shared_by', 'unique' => 0), 'BY_REMOVED_BY' => array('column' => 'removed_by', 'unique' => 0), 'BY_NETWORK_ID' => array('column' => 'network_id', 'unique' => 0))
    );
    public $tasks = array(
        'id' => array('type' => 'integer', 'null' => false, 'length' => 9, 'key' => 'primary'),
        'title' => array('type' => 'string', 'null' => false, 'length' => 500),
        'description' => array('type' => 'string', 'null' => false, 'length' => 255),
        'start_date' => array('type' => 'datetime', 'null' => false),
        'end_date' => array('type' => 'datetime', 'null' => false),
        'priority' => array('type' => 'integer', 'null' => false, 'length' => 2, 'default' => 0),
        'is_completed' => array('type' => 'boolean', 'null' => false, 'default' => false),
        'completed_by' => array('type' => 'integer', 'null' => false, 'length' => 9),
        'network_id' => array('type' => 'integer', 'null' => false, 'length' => 9, 'key' => 'index'),
        'taskassignment_count' => array('type' => 'integer', 'null' => false, 'length' => 9, 'default' => 0),
        'comment_count' => array('type' => 'integer', 'null' => false, 'default' => 0, 'length' => 9),
        'created_by' => array('type' => 'integer', 'null' => false, 'length' => 9, 'key' => 'index'),
        'deleted_by' => array('type' => 'integer', 'null' => false, 'length' => 9, 'key' => 'index'),
        'isDeleted' => array('type' => 'boolean', 'null' => false, 'default' => 0),
        'created' => array('type' => 'datetime', 'null' => false),
        'completed' => array('type' => 'datetime', 'null' => true),
        'deleted' => array('type' => 'datetime', 'null' => true, 'default' => NULL),
        'modified' => array('type' => 'datetime', 'null' => false),
        'indexes' => array('PRIMARY' => array('column' => 'id', 'unique' => 1), 'BY_USER_ID' => array('column' => 'created_by', 'unique' => 0), 'BY_NETWORK_ID' => array('column' => 'network_id', 'unique' => 0))
    );
    public $task_assignments = array(
        'id' => array('type' => 'integer', 'null' => false, 'length' => 9, 'key' => 'primary'),
        'task_id' => array('type' => 'integer', 'null' => false, 'length' => 9),
        'user_id' => array('type' => 'integer', 'null' => false, 'length' => 9, 'key' => 'index'),
        'group_id' => array('type' => 'integer', 'null' => false, 'length' => 9, 'key' => 'index'),
        'network_id' => array('type' => 'integer', 'null' => false, 'length' => 9, 'key' => 'index'),
        'role' => array('type' => 'string', 'null' => false, 'default' => ROLE_USER),
        'is_submitted' => array('type' => 'boolean', 'null' => false, 'default' => 0),
        'status' => array('type' => 'string', 'null' => false, 'default' => STATUS_ACTIVE),
        'added_by' => array('type' => 'integer', 'null' => false, 'length' => 9),
        'removed_by' => array('type' => 'integer', 'null' => false, 'length' => 9),
        'isDeleted' => array('type' => 'boolean', 'null' => false, 'default' => 0),
        'created' => array('type' => 'datetime', 'null' => false),
        'deleted' => array('type' => 'datetime', 'null' => true, 'default' => NULL),
        'modified' => array('type' => 'datetime', 'null' => false),
        'indexes' => array('PRIMARY' => array('column' => 'id', 'unique' => 1), 'BY_USER_ID' => array('column' => 'user_id', 'unique' => 0), 'BY_NETWORK_ID' => array('column' => 'network_id', 'unique' => 0)));
    public $logs = array(
        'id' => array('type' => 'integer', 'null' => false, 'length' => 9, 'key' => 'primary'),
        'user_id' => array('type' => 'integer', 'null' => false, 'length' => 9, 'key' => 'index'),
        'network_id' => array('type' => 'integer', 'null' => false, 'length' => 9, 'key' => 'index'),
        'type' => array('type' => 'string', 'null' => true, 'default' => NULL, 'length' => 35),
        'object_id' => array('type' => 'integer', 'null' => false, 'length' => 9),
        'activity' => array('type' => 'string', 'null' => true, 'default' => NULL, 'length' => 35),
        'isDeleted' => array('type' => 'boolean', 'null' => false, 'default' => 0),
        'info' => array('type' => 'string', 'null' => true, 'default' => NULL, 'length' => 100),
        'created' => array('type' => 'datetime', 'null' => false),
        'deleted' => array('type' => 'datetime', 'null' => true, 'default' => NULL),
        'indexes' => array('PRIMARY' => array('column' => 'id', 'unique' => 1), 'BY_USER_ID' => array('column' => 'user_id', 'unique' => 0), 'BY_NETWORK_ID' => array('column' => 'network_id', 'unique' => 0))
    );
    public $log_participants = array(
        'id' => array('type' => 'integer', 'null' => false, 'length' => 9, 'key' => 'primary'),
        'log_id' => array('type' => 'integer', 'null' => false, 'length' => 9, 'key' => 'index'),
        'network_id' => array('type' => 'integer', 'null' => false, 'length' => 9, 'key' => 'index'),
        'type' => array('type' => 'string', 'null' => true, 'default' => NULL, 'length' => 35),
        'participant_id' => array('type' => 'integer', 'null' => false, 'length' => 9),
        'isDeleted' => array('type' => 'boolean', 'null' => false, 'default' => 0),
        'deleted' => array('type' => 'datetime', 'null' => true, 'default' => NULL),
        'created' => array('type' => 'datetime', 'null' => false),
        'indexes' => array('PRIMARY' => array('column' => 'id', 'unique' => 1), 'BY_LOG_ID' => array('column' => 'log_id', 'unique' => 0), 'BY_NETWORK_ID' => array('column' => 'network_id', 'unique' => 0))
    );
    public $notifications = array(
        'id' => array('type' => 'integer', 'null' => false, 'length' => 9, 'key' => 'primary'),
        'user_id' => array('type' => 'integer', 'null' => false, 'length' => 9, 'key' => 'index'),
        'network_id' => array('type' => 'integer', 'null' => false, 'length' => 9, 'key' => 'index'),
        'isSeen' => array('type' => 'boolean', 'null' => false, 'default' => 0),
        'log_id' => array('type' => 'integer', 'null' => false, 'length' => 9, 'key' => 'index'),
        'isDeleted' => array('type' => 'boolean', 'null' => false, 'default' => 0),
        'deleted' => array('type' => 'datetime', 'null' => true, 'default' => NULL),
        'created' => array('type' => 'datetime', 'null' => false),
        'indexes' => array('PRIMARY' => array('column' => 'id', 'unique' => 1), 'BY_USER_ID' => array('column' => 'user_id', 'unique' => 0), 'BY_NETWORK_ID' => array('column' => 'network_id', 'unique' => 0), 'BY_LOG_ID' => array('column' => 'log_id', 'unique' => 0))
    );
    public $plans = array(
        'id' => array('type' => 'integer', 'null' => false, 'length' => 9, 'key' => 'primary'),
        'name' => array('type' => 'string', 'null' => false, 'length' => 40),
        'price' => array('type' => 'integer', 'null' => false, 'length' => 9, 'default' => 0),
        'description' => array('type' => 'string', 'null' => true, 'length' => 1000),
        'user_limit' => array('type' => 'integer', 'null' => false, 'length' => 9, 'default' => 0),
        'storage_limit' => array('type' => 'biginteger', 'null' => false, 'length' => 14, 'default' => 0),
        'time_limit' => array('type' => 'integer', 'null' => false, 'length' => 9, 'default' => 0),
        'indexes' => array('PRIMARY' => array('column' => 'id', 'unique' => 1))
    );
    public $network_plans = array(
        'id' => array('type' => 'integer', 'null' => false, 'length' => 9, 'key' => 'primary'),
        'network_id' => array('type' => 'integer', 'null' => false, 'length' => 9, 'default' => 0),
        'plan_id' => array('type' => 'integer', 'null' => false, 'length' => 9, 'default' => 0),
        'start' => array('type' => 'datetime', 'null' => false),
        'end' => array('type' => 'datetime', 'null' => false),
        'isDeleted' => array('type' => 'boolean', 'null' => false, 'default' => 0),
        'deleted' => array('type' => 'datetime', 'null' => true, 'default' => NULL),
        'indexes' => array(
            'PRIMARY' => array('column' => 'id', 'unique' => 1),
            'BY_NETWORK_ID' => array('column' => 'network_id', 'unique' => 0),
            'BY_PLAN_ID' => array('column' => 'plan_id', 'unique' => 0)
        )
    );
    public $email_settings = array(
        'id' => array('type' => 'integer', 'null' => false, 'length' => 9, 'key' => 'primary'),
        'user_id' => array('type' => 'integer', 'null' => false, 'length' => 9, 'key' => 'unique'),
        'email' => array('type' => 'string', 'null' => false, 'length' => 320),
//        'network_id' => array('type' => 'integer', 'null' => false, 'length' => 9, 'default' => 0),
        'TASK_SHARED' => array('type' => 'boolean', 'null' => false, 'default' => 1),
        'TASK_EDIT' => array('type' => 'boolean', 'null' => false, 'default' => 1),
        'TASK_SUBMIT' => array('type' => 'boolean', 'null' => false, 'default' => 1),
        'TASK_DEADLINE' => array('type' => 'boolean', 'null' => false, 'default' => 1),
        'NETWORK_MONTHLY_ANALYTICS' => array('type' => 'boolean', 'null' => false, 'default' => 1),
        'NETWORK_INVITATION' => array('type' => 'boolean', 'null' => false, 'default' => 1),
        'NETWORK_ROLE_ASSIGN' => array('type' => 'boolean', 'null' => false, 'default' => 1),
        'NETWORK_ACTIVATED' => array('type' => 'boolean', 'null' => false, 'default' => 1),
        'USER_INVITATION' => array('type' => 'boolean', 'null' => false, 'default' => 1),
        'DOC_SHARED' => array('type' => 'boolean', 'null' => false, 'default' => 1),
        'created' => array('type' => 'datetime', 'null' => false),
        'modified' => array('type' => 'datetime', 'null' => false),
        'indexes' => array(
            'PRIMARY' => array('column' => 'id', 'unique' => 1),
            'UNIQUE' => array('column' => 'user_id', 'unique' => 1),
//            'BY_NETWORK_ID' => array('column' => 'network_id', 'unique' => 0),
        )
    );
    public $cronjobs = array(
        'id' => array('type' => 'integer', 'null' => false, 'length' => 9, 'key' => 'primary'),
        'job_id' => array('type' => 'string', 'null' => false, 'length' => 500),
        'name' => array('type' => 'string', 'null' => false, 'length' => 255),
        'schedule_date' => array('type' => 'datetime', 'null' => false),
        'status' => array('type' => 'string', 'null' => false, 'length' => 10),
        'created' => array('type' => 'datetime', 'null' => false),
        'deleted' => array('type' => 'datetime', 'null' => true, 'default' => NULL),
        'modified' => array('type' => 'datetime', 'null' => false),
        'indexes' => array(
            'PRIMARY' => array(
                'column' => 'id',
                'unique' => 1
            ),
        )
    );
    public $feeds = array(
        'id' => array('type' => 'integer', 'null' => false, 'length' => 9, 'key' => 'primary'),
        'message' => array('type' => 'string', 'null' => false, 'length' => 1000),
        'preview_link' => array('type' => 'text', 'null' => false),
        'user_id' => array('type' => 'integer', 'null' => false, 'length' => 9),
        'network_id' => array('type' => 'integer', 'null' => false, 'length' => 9),
        'comment_count' => array('type' => 'integer', 'null' => false, 'default' => 0, 'length' => 9),
        'like_count' => array('type' => 'integer', 'null' => false, 'default' => 0, 'length' => 9),
        'created' => array('type' => 'datetime', 'null' => true, 'default' => NULL),
        'is_Deleted' => array('type' => 'boolean', 'null' => false, 'default' => 0),
        'deleted' => array('type' => 'datetime', 'null' => true, 'default' => NULL),
        'indexes' => array('PRIMARY' => array('column' => 'id', 'unique' => 1), 'BY_NETWORK_ID' => array('column' => 'network_id', 'unique' => 0), 'BY_USER_ID' => array('column' => 'user_id', 'unique' => 0))
    );

    /**
     * Schema for taggeds table
     *
     * @var array
     */
    
    public $tagged = array(
        'id' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 36, 'key' => 'primary'),
        'foreign_key' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 36),
        'tag_id' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 36),
        'model' => array('type' => 'string', 'null' => false, 'default' => null, 'key' => 'index'),
        'language' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 6, 'key' => 'index'),
        'times_tagged' => array('type' => 'integer', 'null' => false, 'default' => '1'),
        'created' => array('type' => 'datetime', 'null' => true, 'default' => null),
        'modified' => array('type' => 'datetime', 'null' => true, 'default' => null),
        'indexes' => array(
            'PRIMARY' => array('column' => 'id', 'unique' => 1),
            'UNIQUE_TAGGING' => array('column' => array('model', 'foreign_key', 'tag_id', 'language'), 'unique' => 1),
            'INDEX_TAGGED' => array('column' => 'model', 'unique' => 0),
            'INDEX_LANGUAGE' => array('column' => 'language', 'unique' => 0)
        )
    );

    /**
     * Schema for tags table
     *
     * @var array
     * @access public
     */
    public $tags = array(
        'id' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 36, 'key' => 'primary'),
        'identifier' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 30, 'key' => 'index'),
        'name' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 30),
        'keyname' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 30),
        'created' => array('type' => 'datetime', 'null' => true, 'default' => null),
        'modified' => array('type' => 'datetime', 'null' => true, 'default' => null),
        'indexes' => array(
            'PRIMARY' => array('column' => 'id', 'unique' => 1),
            'UNIQUE_TAG' => array('column' => array('identifier', 'keyname'), 'unique' => 1)
        )
    );
    
    /**
     * Schema for Likes Table
     */
    public $likes = array(
        'id' => array('type' => 'integer', 'null' => false, 'default' => NULL, 'key' => 'primary'),
        'model' => array('type' => 'string', 'null' => false, 'default' => NULL, 'length' => 50, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
        'foreign_id' => array('type' => 'integer', 'null' => false, 'default' => NULL),
        'network_id' => array('type' => 'integer', 'null' => false, 'length' => 9),
        'user_id' => array('type' => 'integer', 'null' => false, 'default' => NULL),
        'created' => array('type' => 'datetime', 'null' => false, 'default' => NULL),
        'indexes' => array(
            'PRIMARY' => array('column' => 'id', 'unique' => 1),
            'BY_MODEL' => array('column' => 'model', 'unique' => 0),
        ),
        'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
    );
    
    public $comments = array(
        'id' => array('type' => 'integer', 'null' => false, 'length' => 9, 'key' => 'primary'),
        'user_id' => array('type' => 'integer', 'null' => false, 'length' => 9, 'key' => 'index'),
        'model' => array('type' => 'string', 'null' => false, 'default' => NULL),
        'entity_id' => array('type' => 'integer', 'null' => false, 'length' => 9),
        'network_id' => array('type' => 'integer', 'null' => false, 'length' => 9),
        'body' => array('type' => 'text', 'null' => true, 'default' => NULL),
        'isDeleted' => array('type' => 'boolean', 'null' => false, 'default' => 0),
        'deleted' => array('type' => 'datetime', 'null' => true, 'default' => NULL),
        'created' => array('type' => 'datetime', 'null' => false),
        'indexes' => array(
            'PRIMARY' => array('column' => 'id', 'unique' => 1),
            'BY_MODEL' => array('column' => 'model', 'unique' => 0),
            'BY_ENTITY_ID' => array('column' => 'entity_id', 'unique' => 0)
        )
    );

    public function InsertDefaultPlans() {
        $Plan = ClassRegistry::init("Plan");
        $records = array(
            array(
                "Plan" => array(
                    "id" => 1,
                    "name" => "Free",
                    "price" => 0,
                    "description" => "2 Gb for 20 Users for lifetime",
                    "user_limit" => 20,
                    "storage_limit" => 2000000000,
                    "time_limit" => 999
                )
            ),
            array(
                "Plan" => array(
                    "id" => 2,
                    "name" => "Small",
                    "price" => 50,
                    "description" => "50 $ 2Gb For 1 Month",
                    "user_limit" => 999,
                    "storage_limit" => 2000000000,
                    "time_limit" => 1
                )
            ),
            array(
                "Plan" => array(
                    "id" => 3,
                    "name" => "Medium",
                    "price" => 80,
                    "description" => "80$ 8Gb For 1 Month",
                    "user_limit" => 999,
                    "storage_limit" => 8000000000,
                    "time_limit" => 1
                )
            ),
            array(
                "Plan" => array(
                    "id" => 4,
                    "name" => "Large",
                    "price" => 100,
                    "description" => "100 $ 10Gb For 1 Month",
                    "user_limit" => 999,
                    "storage_limit" => 10000000000,
                    "time_limit" => 1
                )
            ),
        );
        $Plan->saveAll($records);
    }

    public function InsertDefaultEmailSettings() {

        $User = ClassRegistry::init("User");
        $this->EmailSetting = ClassRegistry::init("EmailSetting");

        $records = array();

        $user_settings = $User->find('list', array(
            'fields' => array(
                'UserSetting.id',
                'UserSetting.user_id'
        )));

        $users = $User->find('list', array(
            'fields' => array(
                'User.id',
                'User.email'
            ),
            'conditions' => array(
                'User.id !=' => $user_settings
            )
        ));

        if (empty($users)) {
            return;
        }

        foreach ($users as $key => $value) {
            array_push($records, array(
                'EmailSetting' => array(
                    'user_id' => $key,
                    'email' => $value
                )
            ));
        }

        $this->EmailSetting->saveAll($records);
    }

}

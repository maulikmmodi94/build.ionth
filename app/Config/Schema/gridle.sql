

DROP TABLE IF EXISTS `gridle_master`.`users`;
DROP TABLE IF EXISTS `gridle_master`.`networks`;
DROP TABLE IF EXISTS `gridle_master`.`user_networks`;
DROP TABLE IF EXISTS `gridle_master`.`user_infos`;
DROP TABLE IF EXISTS `gridle_master`.`user_sessions`;
DROP TABLE IF EXISTS `gridle_master`.`groups`;
DROP TABLE IF EXISTS `gridle_master`.`user_groups`;
DROP TABLE IF EXISTS `gridle_master`.`messages`;
DROP TABLE IF EXISTS `gridle_master`.`conversations`;
DROP TABLE IF EXISTS `gridle_master`.`user_conversations`;
DROP TABLE IF EXISTS `gridle_master`.`docs`;
DROP TABLE IF EXISTS `gridle_master`.`docs_assignment`;
DROP TABLE IF EXISTS `gridle_master`.`tasks`;
DROP TABLE IF EXISTS `gridle_master`.`task_assignments`;
DROP TABLE IF EXISTS `gridle_master`.`logs`;
DROP TABLE IF EXISTS `gridle_master`.`log_participants`;
DROP TABLE IF EXISTS `gridle_master`.`notifications`;
DROP TABLE IF EXISTS `gridle_master`.`plans`;
DROP TABLE IF EXISTS `gridle_master`.`network_plans`;


CREATE TABLE `gridle_master`.`users` (
	`id` int(9) NOT NULL AUTO_INCREMENT,
	`username` varchar(20) NOT NULL,
	`email` varchar(320) NOT NULL,
	`email_verified` tinyint(1) DEFAULT '0' NOT NULL,
	`email_token` varchar(128) NOT NULL,
	`email_token_expires` datetime NOT NULL,
	`primary_network_id` int(9) DEFAULT NULL,
	`password` varchar(128) NOT NULL,
	`password_token` varchar(128) DEFAULT NULL,
	`last_login` datetime DEFAULT NULL,
	`last_action` datetime DEFAULT NULL,
	`active` tinyint(1) DEFAULT '0' NOT NULL,
	`created` datetime DEFAULT NULL,
	`modified` datetime DEFAULT NULL,	PRIMARY KEY  (`id`),
	UNIQUE KEY `BY_USERNAME` (`username`),
	UNIQUE KEY `BY_EMAIL` (`email`)) 	;

CREATE TABLE `gridle_master`.`networks` (
	`id` int(9) NOT NULL AUTO_INCREMENT,
	`url` varchar(20) NOT NULL,
	`name` varchar(30) NOT NULL,
	`user_count` int(9) NOT NULL,
	`group_count` int(9) NOT NULL,
	`file_count` bigint(18) NOT NULL,
	`user_limit` int(9) NOT NULL,
	`file_limit` bigint(18) NOT NULL,
	`created_by` int(9) NOT NULL,
	`status` varchar(255) NOT NULL,
	`isDeleted` tinyint(1) DEFAULT '0' NOT NULL,
	`expires` datetime NOT NULL,
	`created` datetime NOT NULL,
	`deleted` datetime DEFAULT NULL,
	`modified` datetime NOT NULL,	PRIMARY KEY  (`id`),
	UNIQUE KEY `BY_URL` (`url`),
	KEY `BY_CREATED_BY` (`created_by`)) 	;

CREATE TABLE `gridle_master`.`user_networks` (
	`id` int(9) NOT NULL AUTO_INCREMENT,
	`user_id` int(9) NOT NULL,
	`network_id` int(9) NOT NULL,
	`file_count` bigint(18) NOT NULL,
	`file_limit` bigint(18) NOT NULL,
	`role` varchar(255) NOT NULL,
	`status` varchar(255) NOT NULL,
	`added_by` int(9) NOT NULL,
	`removed_by` int(9) NOT NULL,
	`isDeleted` tinyint(1) DEFAULT '0' NOT NULL,
	`created` datetime NOT NULL,
	`deleted` datetime DEFAULT NULL,
	`modified` datetime NOT NULL,	PRIMARY KEY  (`id`),
	KEY `BY_USER_ID` (`user_id`),
	KEY `BY_NETWORK_ID` (`network_id`)) 	;

CREATE TABLE `gridle_master`.`user_infos` (
	`id` int(9) NOT NULL AUTO_INCREMENT,
	`user_id` int(9) NOT NULL,
	`first_name` varchar(35) DEFAULT NULL,
	`last_name` varchar(35) DEFAULT NULL,
	`email` varchar(320) NOT NULL,
	`gender` tinyint(1) DEFAULT '0',
	`designation` varchar(200) DEFAULT NULL,
	`company` varchar(200) DEFAULT NULL,
	`photo` varchar(200) DEFAULT 'default.jpg' NOT NULL,
	`photo_dir` varchar(200) DEFAULT 'default' NOT NULL,
	`dob` date DEFAULT NULL,
	`location` varchar(100) DEFAULT NULL,
	`contact_no` varchar(22) DEFAULT NULL,
	`isDeleted` tinyint(1) DEFAULT '0' NOT NULL,
	`created` datetime NOT NULL,
	`deleted` datetime DEFAULT NULL,
	`modified` datetime NOT NULL,	PRIMARY KEY  (`id`),
	KEY `BY_USER_ID` (`user_id`)) 	;

CREATE TABLE `gridle_master`.`user_sessions` (
	`id` int(9) NOT NULL AUTO_INCREMENT,
	`user_id` int(9) NOT NULL,
	`ip` varchar(30) DEFAULT NULL,
	`user_agent` varchar(400) DEFAULT NULL,
	`session_id` varchar(400) DEFAULT NULL,
	`valid` tinyint(1) DEFAULT '0' NOT NULL,
	`created` datetime NOT NULL,	PRIMARY KEY  (`id`),
	KEY `BY_USER_ID` (`user_id`)) 	;

CREATE TABLE `gridle_master`.`groups` (
	`id` int(9) NOT NULL AUTO_INCREMENT,
	`network_id` int(9) NOT NULL,
	`name` varchar(30) NOT NULL,
	`description` varchar(100) DEFAULT NULL,
	`admin` int(9) NOT NULL,
	`users_group_count` int(5) NOT NULL,
	`status` varchar(255) NOT NULL,
	`isDeleted` tinyint(1) DEFAULT '0' NOT NULL,
	`created` datetime NOT NULL,
	`deleted` datetime DEFAULT NULL,
	`modified` datetime NOT NULL,	PRIMARY KEY  (`id`),
	KEY `BY_NETWORK_ID` (`network_id`),
	KEY `BY_ADMIN` (`admin`)) 	;

CREATE TABLE `gridle_master`.`user_groups` (
	`id` int(9) NOT NULL AUTO_INCREMENT,
	`user_id` int(9) NOT NULL,
	`group_id` int(9) NOT NULL,
	`network_id` int(9) NOT NULL,
	`added_by` int(9) NOT NULL,
	`removed_by` int(9) NOT NULL,
	`role` varchar(255) NOT NULL,
	`status` varchar(255) NOT NULL,
	`isDeleted` tinyint(1) DEFAULT '0' NOT NULL,
	`created` datetime NOT NULL,
	`deleted` datetime DEFAULT NULL,
	`modified` datetime NOT NULL,	PRIMARY KEY  (`id`),
	KEY `BY_USER_ID` (`user_id`),
	KEY `BY_GROUP_ID` (`group_id`),
	KEY `BY_NETWORK_ID` (`network_id`),
	KEY `BY_ADDED_BY` (`added_by`),
	KEY `BY_REMOVED_BY` (`removed_by`)) 	;

CREATE TABLE `gridle_master`.`messages` (
	`id` int(9) NOT NULL AUTO_INCREMENT,
	`conversation_id` int(9) NOT NULL,
	`user_id` int(9) NOT NULL,
	`body` varchar(1000) NOT NULL,
	`isDeleted` tinyint(1) DEFAULT '0' NOT NULL,
	`created` datetime NOT NULL,
	`deleted` datetime DEFAULT NULL,	PRIMARY KEY  (`id`),
	KEY `BY_USER_ID` (`user_id`),
	KEY `BY_CONVERSATION_ID` (`conversation_id`)) 	;

CREATE TABLE `gridle_master`.`conversations` (
	`id` int(9) NOT NULL AUTO_INCREMENT,
	`network_id` int(9) NOT NULL,
	`title` varchar(30) DEFAULT NULL,
	`user_id` int(9) NOT NULL,
	`group_id` int(9) DEFAULT NULL,
	`mapping` varchar(20) NOT NULL,
	`status` varchar(20) NOT NULL,
	`user_conversation_count` int(9) NOT NULL,
	`message_count` int(9) NOT NULL,
	`isDeleted` tinyint(1) DEFAULT '0' NOT NULL,
	`created` datetime NOT NULL,
	`deleted` datetime DEFAULT NULL,
	`modified` datetime NOT NULL,	PRIMARY KEY  (`id`),
	KEY `BY_USER_ID` (`user_id`),
	KEY `BY_GROUP_ID` (`group_id`),
	KEY `BY_NETWORK_ID` (`network_id`)) 	;

CREATE TABLE `gridle_master`.`user_conversations` (
	`id` int(9) NOT NULL AUTO_INCREMENT,
	`conversation_id` int(9) NOT NULL,
	`network_id` int(9) NOT NULL,
	`user_id` int(9) DEFAULT NULL,
	`group_id` int(9) DEFAULT NULL,
	`role` varchar(20) NOT NULL,
	`status` varchar(20) NOT NULL,
	`isUnread` tinyint(1) DEFAULT '0' NOT NULL,
	`isDeleted` tinyint(1) DEFAULT '0' NOT NULL,
	`created` datetime NOT NULL,
	`deleted` datetime DEFAULT NULL,
	`modified` datetime NOT NULL,	PRIMARY KEY  (`id`),
	KEY `BY_USER_ID` (`user_id`),
	KEY `BY_GROUP_ID` (`group_id`),
	KEY `BY_NETWORK_ID` (`network_id`),
	KEY `BY_CONVERSATION_ID` (`conversation_id`)) 	;

CREATE TABLE `gridle_master`.`docs` (
	`id` int(9) NOT NULL AUTO_INCREMENT,
	`name` varchar(255) NOT NULL,
	`hash_name` varchar(128) NOT NULL,
	`file_size` int(9) NOT NULL,
	`file_extension` varchar(15) NOT NULL,
	`file_mime` varchar(128) DEFAULT NULL,
	`user_id` int(9) NOT NULL,
	`network_id` int(9) NOT NULL,
	`bucket_path` varchar(150) NOT NULL,
	`doc_assignment_count` int(9) DEFAULT 0 NOT NULL,
	`isDeleted` tinyint(1) DEFAULT '0' NOT NULL,
	`created` datetime NOT NULL,
	`deleted` datetime DEFAULT NULL,
	`modified` datetime NOT NULL,	PRIMARY KEY  (`id`),
	UNIQUE KEY `BY_HASH_NAME` (`hash_name`),
	KEY `BY_USER_ID` (`user_id`),
	KEY `BY_NETWORK_ID` (`network_id`)) 	;

CREATE TABLE `gridle_master`.`docs_assignment` (
	`id` int(9) NOT NULL AUTO_INCREMENT,
	`doc_id` int(9) NOT NULL,
	`group_id` int(9) DEFAULT NULL,
	`user_id` int(9) DEFAULT NULL,
	`shared_by` int(9) NOT NULL,
	`removed_by` int(9) DEFAULT NULL,
	`network_id` int(9) NOT NULL,
	`status` varchar(20) NOT NULL,
	`isDeleted` tinyint(1) DEFAULT '0' NOT NULL,
	`created` datetime NOT NULL,
	`deleted` datetime DEFAULT NULL,
	`modified` datetime NOT NULL,	PRIMARY KEY  (`id`),
	KEY `BY_DOC_ID` (`doc_id`),
	KEY `BY_GROUP_ID` (`group_id`),
	KEY `BY_USER_ID` (`user_id`),
	KEY `BY_SHARED_BY` (`shared_by`),
	KEY `BY_REMOVED_BY` (`removed_by`),
	KEY `BY_NETWORK_ID` (`network_id`)) 	;

CREATE TABLE `gridle_master`.`tasks` (
	`id` int(9) NOT NULL AUTO_INCREMENT,
	`title` varchar(500) NOT NULL,
	`description` varchar(255) NOT NULL,
	`start_date` datetime NOT NULL,
	`end_date` datetime NOT NULL,
	`priority` int(2) DEFAULT 0 NOT NULL,
	`is_completed` tinyint(1) DEFAULT '0' NOT NULL,
	`network_id` int(9) NOT NULL,
	`taskassignment_count` int(9) DEFAULT 0 NOT NULL,
	`created_by` int(9) NOT NULL,
	`deleted_by` int(9) NOT NULL,
	`isDeleted` tinyint(1) DEFAULT '0' NOT NULL,
	`created` datetime NOT NULL,
	`deleted` datetime DEFAULT NULL,
	`modified` datetime NOT NULL,	PRIMARY KEY  (`id`),
	KEY `BY_USER_ID` (`created_by`),
	KEY `BY_NETWORK_ID` (`network_id`)) 	;

CREATE TABLE `gridle_master`.`task_assignments` (
	`id` int(9) NOT NULL AUTO_INCREMENT,
	`task_id` int(9) NOT NULL,
	`user_id` int(9) NOT NULL,
	`group_id` int(9) NOT NULL,
	`network_id` int(9) NOT NULL,
	`role` varchar(255) DEFAULT 'user' NOT NULL,
	`is_submitted` tinyint(1) DEFAULT '0' NOT NULL,
	`status` varchar(255) DEFAULT 'active' NOT NULL,
	`added_by` int(9) NOT NULL,
	`removed_by` int(9) NOT NULL,
	`isDeleted` tinyint(1) DEFAULT '0' NOT NULL,
	`created` datetime NOT NULL,
	`deleted` datetime DEFAULT NULL,
	`modified` datetime NOT NULL,	PRIMARY KEY  (`id`),
	KEY `BY_USER_ID` (`user_id`),
	KEY `BY_NETWORK_ID` (`network_id`)) 	;

CREATE TABLE `gridle_master`.`logs` (
	`id` int(9) NOT NULL AUTO_INCREMENT,
	`user_id` int(9) NOT NULL,
	`network_id` int(9) NOT NULL,
	`type` varchar(35) DEFAULT NULL,
	`object_id` int(9) NOT NULL,
	`activity` varchar(35) DEFAULT NULL,
	`info` varchar(100) DEFAULT NULL,
	`created` datetime NOT NULL,	PRIMARY KEY  (`id`),
	KEY `BY_USER_ID` (`user_id`),
	KEY `BY_NETWORK_ID` (`network_id`)) 	;

CREATE TABLE `gridle_master`.`log_participants` (
	`id` int(9) NOT NULL AUTO_INCREMENT,
	`log_id` int(9) NOT NULL,
	`network_id` int(9) NOT NULL,
	`type` varchar(35) DEFAULT NULL,
	`participant_id` int(9) NOT NULL,
	`created` datetime NOT NULL,	PRIMARY KEY  (`id`),
	KEY `BY_LOG_ID` (`log_id`),
	KEY `BY_NETWORK_ID` (`network_id`)) 	;

CREATE TABLE `gridle_master`.`notifications` (
	`id` int(9) NOT NULL AUTO_INCREMENT,
	`user_id` int(9) NOT NULL,
	`network_id` int(9) NOT NULL,
	`isSeen` tinyint(1) DEFAULT '0' NOT NULL,
	`log_id` int(9) NOT NULL,	PRIMARY KEY  (`id`),
	KEY `BY_USER_ID` (`user_id`),
	KEY `BY_NETWORK_ID` (`network_id`),
	KEY `BY_LOG_ID` (`log_id`)) 	;

CREATE TABLE `gridle_master`.`plans` (
	`id` int(9) NOT NULL AUTO_INCREMENT,
	`name` varchar(40) NOT NULL,
	`price` int(9) DEFAULT 0 NOT NULL,
	`user_limit` int(9) DEFAULT 0 NOT NULL,
	`storage_limit` bigint(14) DEFAULT 0 NOT NULL,
	`time_limit` int(9) DEFAULT 0 NOT NULL,	PRIMARY KEY  (`id`)) 	;

CREATE TABLE `gridle_master`.`network_plans` (
	`id` int(9) NOT NULL AUTO_INCREMENT,
	`network_id` int(9) DEFAULT 0 NOT NULL,
	`plan_id` int(9) DEFAULT 0 NOT NULL,
	`start` datetime NOT NULL,
	`end` datetime NOT NULL,	PRIMARY KEY  (`id`),
	KEY `BY_NETWORK_ID` (`network_id`),
	KEY `BY_PLAN_ID` (`plan_id`)) 	;


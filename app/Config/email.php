<?php

/**
 *
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Config
 * @since         CakePHP(tm) v 2.0.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

/**
 * This is email configuration file.
 *
 * Use it to configure email transports of CakePHP.
 *
 * Email configuration class.
 * You can specify multiple configurations for production, development and testing.
 *
 * transport => The name of a supported transport; valid options are as follows:
 *  Mail - Send using PHP mail function
 *  Smtp - Send using SMTP
 *  Debug - Do not send the email, just return the result
 *
 * You can add custom transports (or override existing transports) by adding the
 * appropriate file to app/Network/Email. Transports should be named 'YourTransport.php',
 * where 'Your' is the name of the transport.
 *
 * from =>
 * The origin email. See CakeEmail::from() about the valid values
 *
 */
class EmailConfig {

    public $smtp = [
        'transport' => 'Mail',
        'from' => 'you@localhost',
            //'charset' => 'utf-8',
            //'headerCharset' => 'utf-8',
    ];
    public $default = [
        'transport' => 'Smtp',
        'from' => array('theteam@gridle.io' => 'The Team'),
//this needs to validated with SES. More details here: http://viorel.sfetea.ro/blog/en/2011/10/31/sending-e-mails-the-efficient-way-using-amazon-ses-and-cakephp/
        'host' => 'ssl://email-smtp.us-west-2.amazonaws.com',
        'port' => 465,
        'timeout' => 30,
        //AKIAIDDP42IY743Q4RPQ
        //AstD/F7dLnMeR9VmMJjddMMnNyVND8Ga2Y0qsvN1gNR4
        'username' => 'AKIAIDDP42IY743Q4RPQ',
        'password' => 'AstD/F7dLnMeR9VmMJjddMMnNyVND8Ga2Y0qsvN1gNR4',
        'client' => null,
        'log' => false,
        'charset' => 'utf-8',
        'headerCharset' => 'utf-8',
            // 'ssl' => 'tls',
    ];
    public $default_gmail = [
        'transport' => 'Smtp',
        'host' => 'ssl://smtp.gmail.com',
        'port' => 465,
        'timeout' => 30,
        'username' => 'gridle.in@gmail.com',
        'password' => 'gridle123',
        'client' => null,
        'log' => false,
        'charset' => 'utf-8',
        'headerCharset' => 'utf-8',
        'ssl' => 'tls',
    ];
    public $default_webmail = [
        'host' => 'md-35.webhostbox.net',
        'port' => 465,
        'timeout' => 30,
        'username' => 'theteam@gridle.io',
        'password' => 'olemsaint',
        'client' => null,
        'log' => false,
        'charset' => 'utf-8',
        'headerCharset' => 'utf-8',
        'tls' => true,
    ];
    public $fast = [
        'from' => 'you@localhost',
        'sender' => null,
        'to' => null,
        'cc' => null,
        'bcc' => null,
        'replyTo' => null,
        'readReceipt' => null,
        'returnPath' => null,
        'messageId' => true,
        'subject' => null,
        'message' => null,
        'headers' => null,
        'viewRender' => null,
        'template' => false,
        'layout' => false,
        'viewVars' => null,
        'attachments' => null,
        'emailFormat' => null,
        'transport' => 'Smtp',
        'host' => 'localhost',
        'port' => 25,
        'timeout' => 30,
        'username' => 'user',
        'password' => 'secret',
        'client' => null,
        'log' => true,
            //'charset' => 'utf-8',
            //'headerCharset' => 'utf-8',
    ];

}

<?php

/**
 * This file is loaded automatically by the app/webroot/index.php file after core.php
 *
 * This file should load/create any application wide configuration settings, such as
 * Caching, Logging, loading additional configuration files.
 *
 * You should also use this file to include any files that provide global functions/constants
 * that your application uses.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Config
 * @since         CakePHP(tm) v 0.10.8.2117
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
// Setup a 'default' cache configuration for use in the application.
Cache::config('default', array('engine' => 'File'));
// Cache::config('default', array('engine' => 'Redis'));

/**
 * The settings below can be used to set additional paths to models, views and controllers.
 *
 * App::build(array(
 *     'Model'                     => array('/path/to/models/', '/next/path/to/models/'),
 *     'Model/Behavior'            => array('/path/to/behaviors/', '/next/path/to/behaviors/'),
 *     'Model/Datasource'          => array('/path/to/datasources/', '/next/path/to/datasources/'),
 *     'Model/Datasource/Database' => array('/path/to/databases/', '/next/path/to/database/'),
 *     'Model/Datasource/Session'  => array('/path/to/sessions/', '/next/path/to/sessions/'),
 *     'Controller'                => array('/path/to/controllers/', '/next/path/to/controllers/'),
 *     'Controller/Component'      => array('/path/to/components/', '/next/path/to/components/'),
 *     'Controller/Component/Auth' => array('/path/to/auths/', '/next/path/to/auths/'),
 *     'Controller/Component/Acl'  => array('/path/to/acls/', '/next/path/to/acls/'),
 *     'View'                      => array('/path/to/views/', '/next/path/to/views/'),
 *     'View/Helper'               => array('/path/to/helpers/', '/next/path/to/helpers/'),
 *     'Console'                   => array('/path/to/consoles/', '/next/path/to/consoles/'),
 *     'Console/Command'           => array('/path/to/commands/', '/next/path/to/commands/'),
 *     'Console/Command/Task'      => array('/path/to/tasks/', '/next/path/to/tasks/'),
 *     'Lib'                       => array('/path/to/libs/', '/next/path/to/libs/'),
 *     'Locale'                    => array('/path/to/locales/', '/next/path/to/locales/'),
 *     'Vendor'                    => array('/path/to/vendors/', '/next/path/to/vendors/'),
 *     'Plugin'                    => array('/path/to/plugins/', '/next/path/to/plugins/'),
 * ));
 *
 */
/**
 * Custom Inflector rules can be set to correctly pluralize or singularize table, model, controller names or whatever other
 * string is passed to the inflection functions
 *
 * Inflector::rules('singular', array('rules' => array(), 'irregular' => array(), 'uninflected' => array()));
 * Inflector::rules('plural', array('rules' => array(), 'irregular' => array(), 'uninflected' => array()));
 *
 */
/**
 * Plugins need to be loaded manually, you can either load them one by one or all of them in a single call
 * Uncomment one of the lines below, as you need. Make sure you read the documentation on CakePlugin to use more
 * advanced ways of loading plugins
 *
 * CakePlugin::loadAll(); // Loads all plugins at once
 * CakePlugin::load('DebugKit'); //Loads a single plugin named DebugKit
 *
 */
/**
 * You can attach event listeners to the request lifecycle as Dispatcher Filter. By default CakePHP bundles two filters:
 *
 * - AssetDispatcher filter will serve your asset files (css, images, js, etc) from your themes and plugins
 * - CacheDispatcher filter will read the Cache.check configure variable and try to serve cached content generated from controllers
 *
 * Feel free to remove or add filters as you see fit for your application. A few examples:
 *
 * Configure::write('Dispatcher.filters', array(
 * 		'MyCacheFilter', //  will use MyCacheFilter class from the Routing/Filter package in your app.
 * 		'MyCacheFilter' => array('prefix' => 'my_cache_'), //  will use MyCacheFilter class from the Routing/Filter package in your app with settings array.
 * 		'MyPlugin.MyFilter', // will use MyFilter class from the Routing/Filter package in MyPlugin plugin.
 * 		array('callable' => $aFunction, 'on' => 'before', 'priority' => 9), // A valid PHP callback type to be called on beforeDispatch
 * 		array('callable' => $anotherMethod, 'on' => 'after'), // A valid PHP callback type to be called on afterDispatch
 *
 * ));
 */
Configure::write('Dispatcher.filters', array(
    'AssetDispatcher',
    'CacheDispatcher'
));

/**
 * Configures default file logging options
 */
App::uses('CakeLog', 'Log');
CakeLog::config('debug', array(
    'engine' => 'File',
    'types' => array('notice', 'info', 'debug'),
    'file' => 'debug',
));
CakeLog::config('error', array(
    'engine' => 'File',
    'types' => array('warning', 'error', 'critical', 'alert', 'emergency'),
    'file' => 'error',
));

CakeLog::config('cronjob', array(
    'engine' => 'File',
    'types' => array('cronjob'),
    'file' => 'cronjob',
));

CakeLog::config('email', array(
    'engine' => 'File',
    'types' => array('email'),
    'file' => 'email',
));

CakePlugin::loadAll(array(
    'Like' => array('routes' => true)
));

CakePlugin::load(array(# or CakePlugin::loadAll(array(
    'CakeResque' => array(
        'bootstrap' => true,
        'Scheduler' => array(
            'enabled' => true
        )
    )

));

Configure::write('App.defaultEmail', 'hello@ionth.com');
Configure::write('file_limit', '10000000000000000');


Configure::write('mcrypt_key', 'VIP of twattled strong spiv.');
Configure::write('secret_key', '677269646C65');

Configure::write('file_limit', '100000000000000');



Configure::write('EmailSetting', array(
    'TASK_SHARED' => 'Task Share',
    'TASK_EDIT' => 'Task Edit',
    'TASK_SUBMIT' => 'Task Submit',
    'TASK_DEADLINE' => 'Task Deadline Reminder',
    'DOC_SHARED'=>'Doc Share',
    'NETWORK_MONTHLY_ANALYTICS'=>'Monthly Analytics Report',
    'NETWORK_ROLE_ASSIGN'=>'Network Role Assign'
        )
);


/* On Registration and Invite Notify Email */
define('NOTIFY_EMAIL', 'anupama@blogtard.com');

App::uses('CakeEventListener', 'Event');
App::uses('CakeEventManager', 'Event');
App::uses('UserListener', 'Event');
CakeEventManager::instance()->attach(new UserListener());
App::uses('GroupListener', 'Event');
CakeEventManager::instance()->attach(new GroupListener());
App::uses('DocListener', 'Event');
CakeEventManager::instance()->attach(new DocListener());
App::uses('TaskListener', 'Event');
CakeEventManager::instance()->attach(new TaskListener());
App::uses('NetworkListener', 'Event');
CakeEventManager::instance()->attach(new NetworkListener());
App::uses('CommentListener', 'Event');
CakeEventManager::instance()->attach(new CommentListener());

define('NOT_DELETED', 0);
define('DELETED', 1);

/* Status Start */
define('STATUS_ACTIVE', 'active');
define('STATUS_INVITED', 'invited');
define('STATUS_REMOVED', 'removed');
define('STATUS_LEFT', 'left');
/* Status End */

/* Role Start */
define('ROLE_SPECIAL', 'special');
define('ROLE_ADMIN', 'admin');
define('ROLE_OWNER', 'owner');
define('ROLE_USER', 'user');
/* Role End */

/* Role Limits Start */
define('NETWORK_ADMIN_LIMIT', 3);
/* Role Limits End */

/* Complete Start */
define('COMPLETE', true);
define('INCOMPLETE', false);
/* Complete End */

/* Network Security IP */
define('ALLOW_FROM_ALL_IP', '0.0.0.0');
/* Network Security IP */

/* Feeds */
define('Feeds_to_load', 10);
/* Feeds */

/* Likes */
define('Likes_to_load', 5);
define('Max_Likes_to_load',100);
/* Likes */


/* Task Module */
define('TASK_REMINDER_TIME', 600);
define('Task_To_Search', 10);
define('Task_To_Load', 20);
/* Task Module End */

/* User Module */
//define('TASK_REMINDER_TIME', 600);
define('User_To_Load', 5);
define('Network_User_Load', 10);
define('User_To_Search', 3);
define('User_ReInvite_Time', 4320);
define('USER_REVERIFICATION_TIME', 7);
define('Edit_User_Time', 10);
/* User Module End */


/* Doc Module */
//define('TASK_REMINDER_TIME', 600);
define('Docs_To_Load', 10);
/* Doc Module End */

/* Conversation Module */
define('Conversationstoload', 20);
define('Messagestoload', 20);
define('Conversation_mapping_single', 'single');
define('Conversation_mapping_multiple', 'multiple');
define('Conversation_mapping_group', 'group');
/* Conversation Module End */

/* Log Module */
define('Logstoload', 50);
define('Log_type_user', 'user');
define('Log_type_group', 'group');
define('Log_type_file', 'file');
define('Log_type_task', 'task');
define('Log_type_network', 'network');
define('Log_activity_create', 'create');
define('Log_activity_edit', 'edit');
define('Log_activity_edit_name', 'editname');
define('Log_activity_delete', 'delete');
define('Log_activity_member_add', 'addmember');
define('Log_activity_member_remove', 'removemember');
define('Log_activity_member_left', 'leftmember');
define('Log_activity_task_complete', 'taskcomplete');
define('Log_activity_task_uncomplete', 'taskincomplete');
define('Log_activity_network_assignrole', 'assignrole');
define('Log_activity_comment_add', 'addcomment');
/* Log Module End */

/* Notification Module */
define('SEEN', 1);
define('NOT_SEEN', 0);
define('MAX_LOAD_NOTIFICATION', 20);
/* Notification Module End */

/* Comments */
define('Comments_to_load', 10);
/* Comments */
/* S3 File Paths */
define('S3_File_Upload_Path', 'files/uploaded/');
define('S3_Picture_Profile_Path', 'pictures/profile/');
/* S3 File Paths */

/* JS Being Used */

define('JS_gridle','gridle_19012015');
define('JS_license','license_151014');
/* JS Being Used */

/* Expiratoin Times */
define('EMAIL_TOKEN_EXPIRATION', 604800);

App::uses('RedirectionException', 'Lib');
/*send verification mail after 3 days*/
define('VARIFICATION_EXPIRATION', 259200);

/* default value*/
define('SET', 1);
define('RESET', 0);
define('GRIDLE', "gridle");
define('GOOGLE', "google");
define('TASKADD', 'Add');
define('TASKEDIT', 'Edit');
define('LISTS', 'list');
define('COUNT', 'count');

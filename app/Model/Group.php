<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

App::uses('SluggableBehavior', 'Utils.Model/Behavior');
App::uses('CakeEvent', 'Event');

class Group extends AppModel {

    public $uses = array('group','UserInfo');
    public $primaryKey = 'id';
    public $defaultFields;
    public $defaultConditions = array('Group.isDeleted' => NOT_DELETED);
    
    
    public $hasMany = array(
        'UsersGroup' => array(
            'className' => 'UsersGroup'
        )
    );
    public $belongsTo = array(
        'Network' => array(
            'className' => 'Network',
            'counterCache' => TRUE
        )
    );
    public $validate = array(
        'name' => array(
            'rule1' => array(
                'rule' => 'notEmpty',
                'message' => 'Group Name required'
            ),
            'rule2' => array(
                'rule' => array('maxLength', 30),
                'message' => 'Group name cannnot have characters greater than 30.'
            ),
            'rule3' => array(
                'rule' => array('custom', '/^[a-z0-9 _\-\']*$/i'), //Rule for alphanumeric + spaces

                'message' => 'Only alphabets, numbers(0-9), spaces( ), hyphens(-) and underscores(_) allowed in the name of team.'
            )
        ),
        'description' => array(
            'rule' => array('maxLength', 100),
            'message' => 'Description cannot have characters greater than 300 characters!'
        ),
        'group_member_limit' => array(
            'rule1' => array(
                'rule' => 'notEmpty',
                'message' => 'Group Member limit can not be empty'
            ),
            'rule2' => array(
                'rule' => 'Numeric',
                'message' => 'group memeber limit can only be numeric'
            )
        ),
        'group_member_count' => array(
            'rule1' => array(
                'rule' => 'Numeric',
                'message' => 'Group Member limit can only be numeric'
            )
        ),
        'status' => array(
            'rule1' => array(
                'rule' => 'notEmpty',
                'message' => 'Status can not be null'
            ),
            'rule2' => array(
                'rule' => array('maxLength', '20'),
                'message' => 'status can not be more than 20'
            )
        ),
        'isDeleted' => array(
            'rule1' => array(
                'rule' => 'notEmpty',
                'message' => 'Status can not be null'
            )
        ),
        'created' => array(
            'rule1' => array(
                'rule' => 'notEmpty',
                'created time can not be empty'
            ),
            'rule2' => array(
                'rule' => 'datetime',
                'message' => 'Please give a valid date and time.'
            )
        ),
        'modified' => array(
            'rule1' => array(
                'rule' => 'notEmpty',
                'created time can not be empty'
            ),
            'rule2' => array(
                'rule' => 'datetime',
                'message' => 'Please give a valid date and time.'
            )
        ),
        'deleted' => array(
            'rule1' => array(
                'rule' => 'datetime',
                'message' => 'Please give a valid date and time.'
            )
        )
    );

    public function add($data) {
        $this->create();
        $data['UsersGroup'][0]['user_id'] = $data['Group']['admin'];
        $data['UsersGroup'][0]['network_id'] = $data['Group']['network_id'];
        $data['UsersGroup'][0]['added_by'] = $data['Group']['admin'];
        $data['UsersGroup'][0]['role'] = ROLE_ADMIN;
        $data['UsersGroup'][0]['status'] = STATUS_ACTIVE;

        //additional data
        $data['Group']['users_group_count'] = 1;
        if ($this->saveAssociated($data)) {
            $data['Group']['id'] = $this->getLastInsertID();
            $this->data = $data;

            $Event = new CakeEvent(
                    'Controller.Group.afterCreate', $this, array('user_id' => $data['Group']['admin'], 'network_id' => $data['Group']['network_id'], 'group_id' => $data['Group']['id'])
            );
            $this->getEventManager()->dispatch($Event);
            return TRUE;
        } else
            return FALSE;
    }

    public function getGroupField($group_id, $network_id, $field_name) {
        $field = 'Group.' . $field_name;
        $network = $this->findByIdAndNetworkId($group_id, $network_id, array($field));
        return isset($network['Group'][$field_name]) ? $network['Group'][$field_name] : null;
    }

    public function getRemainingUserCount($group_id
    ) {
        $group = $this->find('first', array(
            'conditions' => array('Group.id' => $group_id),
            'fields' => array('Group.groupmember_limit', 'Group.users_group_count')
        ));
        return $group['Group']['groupmember_limit'] - $group['Group']['users_group_count'];
    }

    public function getGroups($group_ids, $network_id) {
        return $this->find('all', array(
                    'conditions' => array('Group.id' => $group_ids),
                    'contain' => array('UsersGroup' => array(
                            'fields' => array('UsersGroup.user_id', 'UsersGroup.role', 'UsersGroup.status', 'UsersGroup.group_id'),
                            'conditions' => array('UsersGroup.isDeleted' => NOT_DELETED)
                        )),
                    'fields' => array('Group.id', 'Group.name', 'Group.status', 'Group.users_group_count', 'Group.description', 'Group.admin'),
                    'order' => array('Group.created')
        ));
    }
    
    public function getGroupUserInfo($result = null){
         $result = $result[0];
         $data = Hash::extract($result, 'UsersGroup');
         App::import('Model','UserInfo');
         $userInfo = new UserInfo();
         foreach ($data as $key => $value) {
              $info = $userInfo->view($value['user_id']);
              $result['UsersGroup'][$key]['UserInfo'] = $info[0]['UserInfo'];
         }
        return $result;
    }

    public function getGroupsWithDeletedUsers(
    $group_ids, $network_id) {
        return $this->find('all', array(
                    'conditions' => array('Group.id' => $group_ids, 'Group.network_id' => $network_id),
                    'contain' => array('UsersGroup' => array(
                            'fields' => array('UsersGroup.user_id', 'UsersGroup.role', 'UsersGroup.status', 'UsersGroup.group_id'),
                            'conditions' => array('UsersGroup.isDeleted' => 0)
                        )),
                    'fields' => array('Group.id', 'Group.name', 'Group.status', 'Group.users_group_count', 'Group.description', 'Group.user_id', 'Group.id'),
                    'order' => array('Group.created')
        ));
    }

    public function groupDelete($id, $network_id) {
        $dataSource = $this->getDataSource();
        $time = $dataSource->value(date('Y-m-d H:i:s'), 'string');
        if ($this->UsersGroup->updateAll(
                        array('Group.isDeleted' => DELETED, 'Group.deleted' => $time, 'UsersGroup.isDeleted' => DELETED, 'UsersGroup.deleted' => $time), array('Group.id' => $id))) {
            $this->updateCounterCache(array('network_id' => $network_id));
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function isNetworkGroups($network_id, $group_ids) {
        if (empty($group_ids))
            return true;
        $groups_count = $this->find('count', array('conditions' => array('Group.network_id' => $network_id, 'Group.id' => $group_ids)));

        if (count($group_ids) > 0 && count($group_ids) == $groups_count) {
            return true;
        }
        return false;
    }
    
    public function getGroupId($group_id = array(), $network_id = null, $page = null, $limit = null) {
//        $this->log('in get groupID');
        
        $result =  $this->find('all', array(
                    'conditions' => array(
                        'Group.id !=' => $group_id,
                        'Group.network_id' => $network_id,
                    ),
//            'group' => array('UsersGroup.group_id'),
            'fields' => array('Group.id'),
            'limit' => $limit/* Task_To_Load */,
            'page' => $page,
            'order'=>'Group.id desc'
        ));
//        $this->log($result);
        $result = Hash::extract($result, '{n}.Group.id');
        return $result;
    }
    
    public function getAllGroupId($group_id = array(), $network_id = null, $page = null, $limit = null, $id = null) {
//        $this->log('in get groupID');
        if($id == 0){
            $result =  $this->find('all', array(
                    'conditions' => array(
                        'Group.id !=' => $group_id,
                        'Group.network_id' => $network_id,
                    ),
//            'group' => array('UsersGroup.group_id'),
            'fields' => array('Group.id'),
            'limit' => $limit/* Task_To_Load */,
            'page' => $page,
            'order'=>'Group.id desc'
        ));
        }else{
            $result =  $this->find('all', array(
                    'conditions' => array(
                        'Group.network_id' => $network_id,
                        'And' => array(
                                'Group.id !=' => $group_id,
                                'Group.id <' => $id,
                                      )
                    ),
//            'group' => array('UsersGroup.group_id'),
            'fields' => array('Group.id'),
            'limit' => $limit/* Task_To_Load */,
            'page' => $page,
            'order'=>'Group.id desc'
        ));
        }
        
//        $this->log($result);
        $result = Hash::extract($result, '{n}.Group.id');
        return $result;
    }
    
    public function getGroup($group_ids = null) {
        return $this->find('all', array(
                    'conditions' => array('Group.id' => $group_ids
                                            ),
//                    'contain' => array('UsersGroup' => array(
//                            'fields' => array('UsersGroup.user_id', 'UsersGroup.role', 'UsersGroup.status', 'UsersGroup.group_id'),
//                            'conditions' => array('UsersGroup.isDeleted' => NOT_DELETED)
//                        )),
                    'fields' => array('Group.id', 'Group.name', 'Group.status', 'Group.users_group_count', 'Group.description', 'Group.admin'),
                    'order' => array('Group.created')
        ));
    }
    
    public function findLike($group_id = null, $name = NULL){
        $data = array();
        $result = $this->find('all', array(
            'conditions'=>array(
                'Group.id' => $group_id,
                'Group.name like' => $name.'%',
                ),
            'order'=>array('Group.id DESC')
            ));
         foreach ($result as $key => $value) {
            
            $result[$key]['Group']['type'] = 'group';
            $result[$key]['Group']['value'] = $value['Group']['id'];
            $data['Tags'][$key] = $result[$key]['Group'];
        }         
            return $data;
    }
    

}

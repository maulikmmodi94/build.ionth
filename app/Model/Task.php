<?php

App::uses('TasksAppModel', 'Model');

/**
 * Task Model
 *
 * @property User $User
 * @property TaskAssignment $TaskAssignment
 */
class Task extends TasksAppModel {

    /**
     * Display field
     *
     * @var string
     */
    public $displayField = 'title';


    //The Associations below have been created with all possible keys, those that are not needed can be removed

    /**
     * belongsTo associations
     *
     * @var array
     */
    public $belongsTo = array(
        'User' => array(
            'className' => 'User',
            'foreignKey' => 'created_by',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );

    public $uses = array('User', 'UserInfo', 'Task', 'Group');
    
    /**
     * hasMany associations
     *
     * @var array
     */
    public $hasMany = array(
        'TaskAssignment' => array(
            'className' => 'TaskAssignment',
            'foreignKey' => 'task_id',
            'dependent' => true,
            'conditions' => array(
                'TaskAssignment.isDeleted' => NOT_DELETED)
        )
    );

    /**
     * Validation
     * @var array
     */
    public $validate = array(
        'title' => array(
            'rule1' => array(
                'rule' => 'notEmpty',
                'required' => true,
                'message' => 'Title can not be empty',
                'on' => 'create'
            ),
            'rule2' => array(
                'rule' => array('maxLength', 500),
                'message' => 'Title cannot have characters greater than 500!'
            ),
        ),
        'description' => array(
            'rule' => array('maxLength', 255),
            'message' => 'Description cannot have characters greater than 255!'
        ),
        'end_date' => array(
            'rule' => 'checkDate',
            'message' => 'The start date cannot be after end date.'),
    );
    public $defaultConditions = array('Task.isDeleted' => NOT_DELETED);

    public function __construct($id = false, $table = null, $ds = null) {
        parent::__construct($id, $table, $ds);
        $time = $this->readTimeZone();
        if ($time) {
            $model = ($this->name);
            $this->virtualFields['start_date'] = 'DATE_SUB(' . $model . '.start_date,INTERVAL ' . $time . ' MINUTE)';
            $this->virtualFields['end_date'] = 'DATE_SUB(' . $model . '.end_date,INTERVAL ' . $time . ' MINUTE)';
            $this->virtualFields['completed'] = 'DATE_SUB(' . $model . '.completed,INTERVAL ' . $time . ' MINUTE)';
        }
    }
   
    public function beforeValidate($options = array()) {

        if ($this->hasField('completed') && !empty($this->data[$this->alias]['is_completed']) && $this->data[$this->alias]['is_completed'] === COMPLETE && !empty($this->data[$this->alias]['completed_by'])) {

            $db = $this->getDataSource();
            $default = array('formatter' => 'date');

            $colType = array_merge($default, $db->columns[$this->getColumnType('completed')]);

            $time = time();
            if (array_key_exists('format', $colType)) {
                $time = call_user_func($colType['formatter'], $colType['format']);
            }

            $this->data[$this->alias]['completed'] = $time;
        }
        return true;
    }

    /**
     * Custom validation method to ensure that start_date should less than end_date
     *

     * @return boolean Success
     */
    public function checkDate() {
        if (isset($this->data[$this->alias]['start_date']) && isset($this->data[$this->alias]['end_date']) && ($this->data[$this->alias]['end_date'] >= $this->data[$this->alias]['start_date'])) {
            return true;
        }
        return false;
    }

    public function getField($id, $field_name) {
        $this->id = $id;
        $field = $this->field($field_name);
        $this->id = null;
        return !is_null($field) ? $field : null;
    }
    
public function getTaskId($user_id, $network_id, $type = null, $id = null) {
    $options = array();
//return task id of user(which are associated on that task)    
    if($type == 'user'){
        $options = array(
            'conditions' => array(
                'TaskAssignment.user_id' => $user_id,
                'TaskAssignment.network_id' => $network_id,
            ),
            'fields' => array('TaskAssignment.task_id')
        );
        $this->TaskAssignment->Behaviors->load('Containable', array('autoFields' => false));

        $task_ids = $this->TaskAssignment->find('list', $options);
        $task_ids = array_unique($task_ids);
        $op = array(
            'conditions' => array(
                                'TaskAssignment.task_id'=> $task_ids,
                                'TaskAssignment.user_id' => $id,
                'TaskAssignment.network_id' => $network_id,
            ),
            'fields' => array('TaskAssignment.task_id')
        );

        $this->TaskAssignment->Behaviors->load('Containable', array('autoFields' => false));

        $task_ids = $this->TaskAssignment->find('list', $op);
    }
// return id of group(associated)
    elseif($type == 'group'){
        $options = array(
            'conditions' => array(
            'AND' => array( 
                                'TaskAssignment.user_id' => $user_id,
                                'TaskAssignment.group_id' => $id,
                            ),
                'TaskAssignment.network_id' => $network_id,
            ),
            'fields' => array('TaskAssignment.task_id')
        );
        $this->TaskAssignment->Behaviors->load('Containable', array('autoFields' => false));

        $task_ids = $this->TaskAssignment->find('list', $options);
    }
//search task by its name
    else{
        $options = array(
            'conditions' => array(
             'Task.network_id' => $network_id,
             'Task.title like' => '%'.$type.'%'
            ),
            'contain' => array('TaskAssignment' =>
                array('conditions' => array(
                        'TaskAssignment.user_id' => $user_id,
                        'TaskAssignment.isDeleted' => NOT_DELETED
                    )
                )),
            'fields' => array('Task.id'),
        );
        
        $task = $this->find('all', $options);

//        $this->log('$task');
//        $this->log($task);
        $task_ids = array();
            foreach ($task as $value) {
                      if(!empty($value['TaskAssignment'])){
                          array_push($task_ids,$value['Task']['id']);
                        }  
            }
        
//        $this->log('$task_ids');
//        $this->log($task_ids);
    }
        return $task_ids;
    }
//    public function getUserTask($user_id, $network_id, $page = 1, $id = null, $type = null){
//        $this->log("in getusertask");
//        $options = array(
//            'conditions' => array(
//                'TaskAssignment.user_id' => $user_id,
//                'TaskAssignment.network_id' => $network_id,
//                'TaskAssignment.role' => $role
//            ),
//            'fields' => array('TaskAssignment.task_id')
//        );
//
//        if (is_null($role)) {
//            unset($options['conditions']['TaskAssignment.role']);
//        }
//        $this->log($options);
//        $this->TaskAssignment->Behaviors->load('Containable', array('autoFields' => false));
//        $task_ids = $this->TaskAssignment->find('list', $options);
//        $this->log($task_ids);
//        return $this->gets($task_ids);
//
//    }

    public function gets($task_ids = null, $current_user_id = null) {
        $tasks = $this->find('all', array(
            'conditions' => array('Task.id' => $task_ids),
            'contain' => array('TaskAssignment' =>
                array('fields' => array('TaskAssignment.user_id', 'TaskAssignment.group_id', 'TaskAssignment.role'),
                    'conditions'=>array(
                    'TaskAssignment.user_id !=' => $current_user_id,
                         'TaskAssignment.isDeleted' => NOT_DELETED)
                ),
                'Comment'
            ))
        );
        
        $tasks = $this->attachTags($tasks);
        
        return $tasks;
        
    }
    
    public function getTaskCount($user_id, $network_id, $option, $id = null, $type = null){

        if(!empty($type)){
         $list_task_ids = $this->getTaskId($user_id, $network_id,$type,  $id);
         $list_task_ids = array_unique($list_task_ids);
        }
         $options = array(
            'conditions' => array('Task.id' => $list_task_ids),
            'fields' => array('id'),
            'order' => 'Task.end_date'
            );

switch ($option) {

            case "pending" :
                
                $options['conditions']['Task.end_date < '] = date('Y-m-d');

                $options['conditions']['Task.is_completed'] = 0;
                
                break;

            case "today" :

                $options['conditions']['DATE(Task.end_date) '] = date('Y-m-d');

                $options['conditions']['Task.is_completed'] = 0;
                
                break;
            
            case "tomorrow" :

                $options['conditions']['DATE(Task.end_date) '] = date('Y-m-d',strtotime('+1 days'));

                $options['conditions']['Task.is_completed'] = 0;
                
                break;
            
             case "other":

                $options['conditions']['DATE(Task.end_date) >'] = date('Y-m-d',strtotime('+7 days'));
                
                $options['conditions']['Task.is_completed'] = 0;
                
                break;

            
            //Removed - has to be removed from code
            case "this_week" :

                $options['conditions']['DATE(Task.end_date) >'] = date('Y-m-d',strtotime('+1 days'));
                $options['conditions']['DATE(Task.end_date) <'] = date('Y-m-d',strtotime('+8 days'));

                $options['conditions']['Task.is_completed'] = 0;
                
                break;
            //Removed - has to be removed from code
            case "next_week":
                
                $options['conditions']['DATE(Task.end_date) >'] = date('Y-m-d',strtotime('+7 days'));
                
                $options['conditions']['Task.is_completed'] = 0;

                
                break;

            case "upcoming" :

                $options['conditions']['Task.is_completed'] = 0;
                
                $options['conditions']['DATE(Task.end_date) >'] = date('Y-m-d');

                break;
            
            case "upto" :

                $options['conditions']['Task.is_completed'] = 0;
                
                $options['conditions']['DATE(Task.end_date) >='] = date('Y-m-d');

                break;
            
            case "all" :
                
//                $options['conditions']['end_date < '] = date('Y-m-d H:i:s');
//
                $options['conditions']['Task.is_completed'] = 0;
                
                break;

            case "completed" : 
                
                $options['conditions']['Task.is_completed'] = 1;
                
                break;
            
            default :

                break;
        }
        $tasks = $this->find('count', $options);

        return $tasks;
        
    }

    





    public function TaskDelete($task_id, $deleted_by) {

        return $this->delete($task_id);

//        $dataSource = $this->getDataSource();
//        $dataSource->begin();
//        $time = $dataSource->value(date('Y-m-d H:i:s'), 'string');
//        $return_status = true;
//
//        try {
//
//            $return_status = $return_status &&
//                    $this->updateAll(
//                            array(
//                        'Task.isDeleted' => DELETED,
//                        'Task.deleted' => $time,
//                        'Task.deleted_by' => $deleted_by
//                            ), array(
//                        'Task.id' => $task_id
//                            )
//            );
//
//            $return_status = $return_status && $this->TaskAssignment->updateAll(array('TaskAssignment.isDeleted' => DELETED, 'TaskAssignment.deleted' => $time), array('TaskAssignment.task_id' => $task_id));
//
//
//            if (!$return_status) {
//                throw new Exception();
//            }
//            $dataSource->commit();
//            return TRUE;
//        } catch (Exception $e) {
//            $dataSource->rollback();
//            return FALSE;
//        }
    }

    public function setComplete($task_id, $status, $completed_by) {
        $this->id = $task_id;
        $this->set('is_completed', $status);
        $this->set('completed_by', $completed_by);

        if ($this->save()) {
            return true;
        }
        return false;
    }

    public function isPartOfTask($data) {
        $groups = ClassRegistry::init('UsersGroup');
        $group_ids = $groups->getActiveGroups($data['user'], @$data['network_id']);
        //Check if user or his groups is a part of the doc
        $count = $this->TaskAssignment->hasAny(array(
            'TaskAssignment.task_id' => $data['id'],
            'Or' => array(
                array('TaskAssignment.user_id' => $data['user']),
                array('TaskAssignment.group_id' => $group_ids)
            )
        ));
        if ($count == sizeof($data['user']))
            return true;
        else
            return false;
    }

    public function taskCount($network_id = null) {

        return $this->find('all', array(
                    'conditions' => array(
                        'Task.network_id' => $network_id,
                    ),
                    'fields' => array('Task.id'),
                    'order' => 'Task.created desc'
        ));
    }

    public function taskCreatedByUser($user_id = null, $network_id = null) {

//        if(isset($network_id)){
        return $this->find('all', array(
                    'conditions' => array(
                        'Task.created_by' => $user_id,
                        'Task.network_id' => $network_id,
                        'Task.taskassignment_count >' => 1
                    ),
                    'order' => 'Task.created desc'
        ));
    }

    public function taskCountById($task_id = array()) {

        return $this->find('all', array(
                    'conditions' => array(
                        'Task.id' => $task_id
                    ),
                    'order' => 'Task.created desc'
        ));
    }
    
    /*     * ***********************************************************
     *              MOBILE APPLICATION SERVICES
     * ***********************************************************
     */

    /**
     * Returns all the tasks of a user of a given network 
     * Made for mobile application
     * @author anupama
     * @since 27 april,2015
     * @param type $user_id
     * @param type $network_id
     * @param type $role
     * @return type
     */
    public function getUserTasks($user_id, $network_id, $role = NULL) {

        $options = array(
            'conditions' => array(
                'TaskAssignment.user_id' => $user_id,
                'TaskAssignment.network_id' => $network_id,
                'TaskAssignment.role' => $role
            ),
            'fields' => array('TaskAssignment.task_id')
        );

        if (is_null($role)) {
            unset($options['conditions']['TaskAssignment.role']);
        }

        $this->TaskAssignment->Behaviors->load('Containable', array('autoFields' => false));

        $task_ids = $this->TaskAssignment->find('list', $options);

        return $task_ids;
    }

    /**
     * copy of function gets() for the mobile application
     * can be used to get pending, today's tasks, this weeks tasks and future tasks
     * changed the fields to get from the function
     * @author anupama
     * @created 27th april, 2015
     * @param type $task_ids
     * @return array
     */
    public function getTasks($user_id, $network_id, $option, $page = null,$id = null, $type = null) {

        if(!empty($type)){
         $list_task_ids = $this->getTaskId($user_id, $network_id,$type, $id );
         $list_task_ids = array_unique($list_task_ids);
           
        }else{
            $list_task_ids = $this->getUserTasks($user_id, $network_id);
        }
         
        $options = array(
            'conditions' => array('Task.id' => $list_task_ids),
            'contain' => array(
                'Comment' => array('fields' => array('Comment.id', 'Comment.user_id', 'Comment.body', 'Comment.created')),
                'TaskAssignment' => array('conditions'=>array(
                    'TaskAssignment.user_id !=' => $user_id,
                    'TaskAssignment.isDeleted' => NOT_DELETED
                    )
                )
            ),
            'limit' => Task_To_Load,
            'page' => $page,
            'fields' => array('id', 'title', 'start_date', 'end_date', 'priority', 'is_completed', 'completed_by', 'comment_count', 'taskassignment_count', 'created_by', 'created', 'completed'),
//            'order' => 'Task.end_date'
            );


        switch ($option) {

            case "pending" :
                
                $options['conditions']['Task.end_date < '] = date('Y-m-d H:i:s');

                $options['conditions']['Task.is_completed'] = 0;
                
                $options['order'] = 'Task.end_date desc';
                
                break;

            case "today" :

                $options['conditions']['DATE(Task.end_date) '] = date('Y-m-d');

                $options['conditions']['Task.is_completed'] = 0;
                
                $options['order'] = 'Task.end_date asc';
                
                break;
            
            case "tomorrow" :

//                 $start_date = date('Y-m-d');
//                
//                $end_date = date('Y-m-d', strtotime('+1 days'));
                $options['conditions']['DATE(Task.end_date) '] = date('Y-m-d',strtotime('+1 days'));

                $options['conditions']['Task.is_completed'] = 0;
                
                $options['order'] = 'Task.end_date asc';
                
                break;
            
             case "other";

                $options['conditions']['DATE(Task.end_date) >'] = date('Y-m-d',strtotime('+7 days'));
                
                $options['conditions']['Task.is_completed'] = 0;
                
                $options['order'] = 'Task.end_date asc';
                
                break;

            
            case "this_week" :
                $date = 8-date('w');
                $options['conditions']['DATE(Task.end_date) >'] = date('Y-m-d',strtotime('+1 days'));
                $options['conditions']['DATE(Task.end_date) <'] = date('Y-m-d',strtotime('+'.$date.' days'));
                $options['conditions']['Task.is_completed'] = 0;
                
                $options['order'] = 'Task.end_date asc';
                
                break;
            
            case "next_week";
                $date = 7-date('w');
                 $options['conditions']['DATE(Task.end_date) >'] = date('Y-m-d',strtotime('+'.$date.' days'));
                
                $options['conditions']['Task.is_completed'] = 0;
                
                $options['order'] = 'Task.end_date asc';
                
                break;

            case "upcoming" :

                $options['conditions']['Task.is_completed'] = 0;
                
                $options['conditions']['Task.DATE(end_date) >'] = date('Y-m-d');
                
                $options['order'] = 'Task.end_date asc';

                break;
            
            case "upto" :

                $options['conditions']['Task.is_completed'] = 0;
                
                $options['conditions']['DATE(Task.end_date) >='] = date('Y-m-d');
                
                $options['order'] = 'Task.end_date asc';

                break;
            
            case "all" :
                
//                $options['conditions']['end_date < '] = date('Y-m-d H:i:s');
//
//                $options['conditions']['Task.is_completed'] = 0;
                
                $options['order'] = 'Task.end_date desc';
                
                break;

            case "completed" : 
                
                $options['conditions']['Task.is_completed'] = 1;
                
                
                $options['order'] = 'Task.end_date desc';
                
                break;
            
            default :

                break;
        }
        $tasks = $this->find('all', $options);
        $tasks_final = $this->attachTags($tasks);
        $tasks_final = Hash::remove($tasks_final, '{n}.TaskAssignment');
        return $tasks_final;
    }

    /**
     * Attaching tags that are associated with the tasks
     * 
     * Tags represents the uses and groups that have been assigned the task
     * 
     * @param type $tasks
     * @return array
     */
    public function attachTags($tasks) {
        App::import('Model','UserInfo');
        foreach ($tasks as $key => $value) {

            $tags = array();
            $group = array();
            $group_tags = array_unique(Hash::extract($value, 'TaskAssignment.{n}[group_id!=0].group_id'));

            $user_tags = Hash::extract($value, 'TaskAssignment.{n}[group_id=0]');
            $completed_id = Hash::extract($value, 'Task[completed_by!=0].completed_by');
            if(!empty($completed_id[0])){
                
                $complete = new UserInfo();
                $temp['info'] = $complete->view($completed_id[0]);
                $value['Completed_by']['completed_by'] = $temp['info'][0]['UserInfo']['user_id'];
                $value['Completed_by']['name'] = $temp['info'][0]['UserInfo']['name'];
                $value['Completed_by']['vga'] = $temp['info'][0]['UserInfo']['vga'];
                $tasks[$key]['Completed_by'] = $value['Completed_by'];
            }
            foreach ($user_tags as $user_key => $user_value) {
                $user_value['value'] = $user_value['user_id'];

                $user_value['type'] = 'user';
                $user = new UserInfo();
                $temp['info'] = $user->view($user_value['user_id']);
                $user_value['name'] = $temp['info'][0]['UserInfo']['name'];
                $user_value['vga'] = $temp['info'][0]['UserInfo']['vga'];
                array_push($tags, $user_value);
            }

            foreach ($group_tags as $group_value) {
                App::import('Model','Group');
                $grp = new Group();
                $data = array();
                $data = $grp->getGroup($group_value);
                $group = array(
                    'value' => $group_value,
                    'group_id'=> $group_value,
                    'type' => 'group',
                    'role' => 'user',
                    'name'=>$data[0]['Group']['name'],
                    'info' => $data[0]['Group']
                );
//                array_push($tags, array('value' => $group_value, 'group_id' => $group_value, 'type' => 'group', 'role' => 'user'));
                array_push($tags, $group);
            }

            $tasks[$key]['Tags'] = $tags;
        }

        return $tasks;
    }

}

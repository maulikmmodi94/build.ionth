<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Log extends AppModel {

    public $useTable = 'logs';
    public $primaryKey = 'id';
    public $hasMany = array(
        'LogParticipant' => array(
            'className' => 'LogParticipant',
            'foreignKey' => 'log_id'
        ),
        'Notification' => array(
            'className' => 'Notification',
            'foreignKey' => 'log_id'
        )
    );

    /**
     * Constructor
     *
     * @param bool|string $id ID
     * @param string $table Table
     * @param string $ds Datasource
     */
    public function __construct($id = false, $table = null, $ds = null) {
        parent::__construct($id, $table, $ds);
        $this->defaultConditions = array(
            $this->alias . '.isDeleted' => NOT_DELETED
        );
    }

//    public $belongsTo = array(
//        'Notification' => array(
//            'className' => 'Notification',
//            'foreignKey' => 'log_id'
//        )
//    );

    public function add($creator, $network_id, $type, $object_id, $activity, $data = NULL) {
        $this->create();
        $data['user_id'] = $creator;
        $data['network_id'] = $network_id;
        $data['type'] = $type;
        $data['object_id'] = $object_id;
        $data['activity'] = $activity;
        if ($this->save($data))
            return $this->getLastInsertID();
        else
            return FALSE;
    }

    public function getLogs($log_ids) {
        return $this->find('all', array(
                    'conditions' => array('Log.id' => $log_ids),
                    'contain' => array(
                        'LogParticipant' => array(
                            'fields' => array('LogParticipant.*')
                        )
                    ),
                    'order' => array('Log.id DESC')
        ));
    }

    public function deleteLog($id) {
        $conditions = array(
            $this->alias.'.'.$this->primaryKey => $id
        );
        return $this->soft_delete($conditions,array(),1);

    }

}

<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Message extends AppModel {

    public $uses = 'messages';
    public $belongsTo = array('Conversation' => array(
            'className' => 'Conversation',
            'counterCache' => true,
            'order' => array('Message.created DESC')
        )
    );
    public $validate = array(
        'body' => array(
            'rule1' => array(
                'rule' => 'notEmpty',
                'required' => true,
                'message' => 'Message cannot be empty'
            ),
            'rule2' => array(
                'rule' => array('maxLength', 1000),
                'message' => 'Message cannot have more than 1000 characters'
            )
        ),
        'conversation_id' => array(
            'rule1' => array(
                'rule' => 'notEmpty',
                'message' => 'Conversation Id can not be empty'
            ),
            'rule2' => array(
                'rule' => 'numeric',
                'message' => 'Conversation Id can only be numeric'
            )
        ),
        'user_id' => array(
            'rule1' => array(
                'rule' => 'notEmpty',
                'message' => 'User Id can not be empty'
            ),
            'rule2' => array(
                'rule' => 'numeric',
                'message' => 'User Id can only be numeric'
            )
        ),
        'isDeleted' => array(
            'rule1' => array(
                'rule' => 'notEmpty',
                'message' => 'Status can not be null'
            ),
            'rule2' => array(
                'rule' => array('inList', array(0, 1)),
                'message' => 'isDeleted can only be from (0,1)'
            )
        ),
        'created' => array(
            'rule1' => array(
                'rule' => 'notEmpty',
                'created time can not be empty'
            ),
            'rule2' => array(
                'rule' => 'datetime',
                'message' => 'Please give a valid date and time.'
            )
        ),
        'deleted' => array(
            'rule1' => array(
                'rule' => 'datetime',
                'message' => 'Please give a valid date and time.'
            )
        )
    );

    public function addmessage($conversation_id, $author, $body) {
        $data = array();
        $data['Conversation']['id'] = $conversation_id;
        $data['Message']['conversation_id'] = $conversation_id;
        $data['Message']['user_id'] = $author;
        $body = $this->detectAndTransformLinks($body);
        $data['Message']['body'] = $body;
        if ($this->saveAll($data))
            return TRUE;
        else
            return FALSE;
    }

    /**
     * Load messages of a conversation
     * @param type $conversation_id
     * @param type $reference
     * @param type $direction
     * @param type $limit
     * @return type
     * @author Naimish Sakhpara <naimish.sakhpara@blogtard.com>
     */
    public function loadmessage($conversation_id, $reference, $direction, $limit) {
        switch ($direction) {
            case 'old':
                $query = "< '" . $reference . "'";
                break;
            case 'new':
                $query = "> '" . $reference . "'";
                break;
        }
        $messages = $this->find('all', array(
            'conditions' => array('and' => array(
                    array('Message.conversation_id' => $conversation_id),
                    array('Message.id' . $query)
                )),
            'fields' => array('Message.id', 'Message.conversation_id', 'Message.user_id', 'Message.body', 'Message.created'),
            'limit' => $limit,
            'order' => array('Message.id')
        ));
        foreach ($messages as $key => $message) {
            $messages[$key] = $messages[$key]['Message'];
            unset($messages[$key]['Message']);
        }
        return $messages;
    }

    public function getlatestid() {
        $message = $this->find('first', array(
            'order' => array('Message.id DESC'),
            'fields' => array('Message.id')
        ));
        return ($message == NULL) ? 0 : $message['Message']['id'];
    }

    /**
     * Check for new messages after a given id
     * @param type $time
     * @return null
     * @author Naimish Sakhpara <naimish.sakhpara@blogtard.com>
     */
    public function checkfornewmessages($id = NULL) {
        $conversations = $this->find('all', array(
            'conditions' => array('Message.id >' . $id),
            'fields' => array('DISTINCT Message.conversation_id', 'Message.id'),
            'order' => array('Message.created DESC')
        ));
        if ($conversations == NULL)
            return NULL;
        else {
            $conversationsids = Set::classicExtract($conversations, '{n}.Message.conversation_id');
            return array($conversationsids, $conversations[0]['Message']['id']);
        }
    }

}

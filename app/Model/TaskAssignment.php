<?php

App::uses('TasksAppModel', 'Model');

/**
 * TaskAssignment Model
 *
 * @property Task $Task
 * @property User $User
 * @property Group $Group
 */
class TaskAssignment extends TasksAppModel {
    //The Associations below have been created with all possible keys, those that are not needed can be removed

    /**
     * belongsTo associations
     *
     * @var array
     */
    public $belongsTo = array(
        'Task' => array(
            'className' => 'Task',
            'foreignKey' => 'task_id',
            'counterCache' => array('taskassignment_count' => array(
                    array('TaskAssignment.status' => STATUS_ACTIVE, 'TaskAssignment.isDeleted' => NOT_DELETED)
                )),
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'User' => array(
            'className' => 'User',
            'foreignKey' => 'user_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'Group' => array(
            'className' => 'Group',
            'foreignKey' => 'group_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'UserNetwork' => array(
            'className' => 'UserNetwork',
            'foreignKey' => false,
            'type'=>'inner',
            'conditions' => array(
                'UserNetwork.user_id = TaskAssignment.user_id',
                'UserNetwork.network_id = TaskAssignment.network_id',
                'UserNetwork.isDeleted' => NOT_DELETED, 
                'UserNetwork.status' => STATUS_ACTIVE),
            'fields' => array('UserNetwork.id','UserNetwork.user_id'),
            'order' => ''
        )
    );
    public $defaultConditions = array('TaskAssignment.isDeleted' => NOT_DELETED);

    public function isCreator($task_id = NULL, $user_id = NULL) {

        return $this->find('count', array('conditions' => array('TaskAssignment.task_id' => $task_id, 'TaskAssignment.user_id' => $user_id, 'TaskAssignment.role' => ROLE_ADMIN)));
    }

    public function isAssignee($task_id = NULL, $user_id = NULL) {
        return $this->find('count', array('conditions' => array('TaskAssignment.task_id' => $task_id, 'TaskAssignment.user_id' => $user_id, 'TaskAssignment.role' => ROLE_USER)));
    }

    public function getField($task_id, $user_id, $field_name) {
        $field = 'TaskAssignment.' . $field_name;
        $task = $this->findByTaskIdAndUserId($task_id, $user_id, array($field));
        return isset($task['TaskAssignment'][$field_name]) ? $task['TaskAssignment'][$field_name] : NULL;
    }

    public function add($task_id = null, $assignee_data = array(), $added_by = null, $network_id = null) {

        try {


            $assignee = array();
            foreach ($assignee_data as $key => $value) {
                $assignee[$key]['TaskAssignment'] = $value;
            }

            $user_ids = Hash::extract($assignee, '{n}.TaskAssignment.user_id');
            $already_assignee = $this->find('all', array('fields' => array('TaskAssignment.user_id', 'TaskAssignment.group_id', 'TaskAssignment.task_id'), 'conditions' => array('TaskAssignment.task_id' => $task_id, 'TaskAssignment.user_id' => $user_ids, 'TaskAssignment.role' => ROLE_USER, 'isDeleted' => NOT_DELETED)));

            $new_assignee = $assignee;
            if (count($already_assignee) > 0) {

                foreach ($assignee as $key1 => $value1) {
                    foreach ($already_assignee as $key2 => $value2) {


                        if ($assignee[$key1]['TaskAssignment']['user_id'] == $already_assignee[$key2]['TaskAssignment']['user_id'] && $assignee[$key1]['TaskAssignment']['group_id'] == $already_assignee[$key2]['TaskAssignment']['group_id']) {
                            unset($assignee[$key1]);
                            break;
                        }
                    }
                }

                //$new_assignee = array_diff($assignee, $already_assignee);
            }
            $new_assignee = $assignee;

            // $already_assignee = array_diff($assignee, $new_assignee);
            // $already_assignee = Hash::insert($already_assignee, '{n}.TaskAssignment.status', 'already assignee');
            $new_assignee = Hash::insert($new_assignee, '{n}.TaskAssignment.task_id', $task_id);
            $new_assignee = Hash::insert($new_assignee, '{n}.TaskAssignment.added_by', $added_by);
            $new_assignee = Hash::insert($new_assignee, '{n}.TaskAssignment.network_id', $network_id);
            if (count($new_assignee) > 0 && !$this->saveMany($new_assignee)) {
                throw new Exception();
            }
            return TRUE;
        } catch (Exception $e) {
            return FALSE;
        }
    }

    /**
     * Remove Assignee from Task
     *
     * @author Hardik Sondagar
     * @abstract Remove Assignee from Task
     * @param integer $task_id Task Id.
     * @param integer $value Assignee Id ( UserID or GroupID depending on $type ) to be remove.
     * @param string $type Assignee Type ( User or Group ).
     * @param integer $removed_by User who removed Assignee.
     * @return boolean true on success , false on failure
     * @since 09/09/14
     */
    public function removeAssignee($task_id, $value, $type, $removed_by) {

        /* Convert data into SQL format */
        $dataSource = $this->getDataSource();
        $STATUS_REMOVED = $dataSource->value(STATUS_REMOVED, 'string');
        $time = $dataSource->value(date('Y-m-d H:i:s'), 'string');

        /* Things to update */
        $to_update = array(
            'isDeleted' => DELETED,
            'status' => $STATUS_REMOVED,
            'removed_by' => $removed_by,
            'deleted' => $time
        );

        /* On following condition */
        $common_conditions = array(
            'TaskAssignment.task_id' => $task_id,
            // 'TaskAssignment.group_id' => $value,
            'TaskAssignment.role' => ROLE_USER
        );

        switch ($type) {
            case 'group':
                /* If Group is removing then remove all the Task Assignee who part of group */
                $specific_conditions = array('TaskAssignment.group_id' => $value);
                break;

            case 'user':
                /* If User is removing then set group_id==0 to avoid removing Group Assignee */
                $specific_conditions = array(
                    'TaskAssignment.user_id' => $value,
                    'TaskAssignment.group_id' => 0
                );
                break;

            default:
                return false;
                break;
        }

        /* Final condition by merging $common_condition and $specific_conditions based on $type */
        $conditions = array_merge($common_conditions, $specific_conditions);

        /* Soft delete Assignee and update CounterCache is any row affected */
        if ($this->updateAll($to_update, $conditions) && $this->getAffectedRows() > 0) {
            $this->updateCounterCache(array('task_id' => $task_id));
            return true;
        }
        return false;
    }

    public function deleteAssignee($value, $type, $removed_by) {
        $dataSource = $this->getDataSource();
        $STATUS_REMOVED = $dataSource->value(STATUS_REMOVED, 'string');
        $time = $dataSource->value(date('Y-m-d H:i:s'), 'string');
        if ($type == "group") {
            $options = array(
                'conditions' => array(
                    'TaskAssignment.group_id' => $value,
                    'TaskAssignment.role' => ROLE_USER
                ),
                'fields' => array('TaskAssignment.task_id')
            );
            $task_ids = $this->find('list', $options);
            $return = $this->updateAll(array('isDeleted' => DELETED, 'status' => $STATUS_REMOVED, 'removed_by' => $removed_by, 'deleted' => $time), array('TaskAssignment.group_id' => $value, 'TaskAssignment.role' => ROLE_USER));
            if ($return) {

                $this->updateCounterCache(array('task_id' => $task_ids));
            }
            return $return;
        } else if ($type == "user") {
            $options = array(
                'conditions' => array(
                    'TaskAssignment.user_id' => $value,
                    'TaskAssignment.group_id' => 0,
                    'TaskAssignment.role' => ROLE_USER
                ),
                'fields' => array('TaskAssignment.task_id')
            );
            $task_ids = $this->find('list', $options);
            $return = $this->updateAll(array('isDeleted' => DELETED, 'status' => $STATUS_REMOVED, 'removed_by' => $removed_by), array('TaskAssignment.user_id' => $value, 'TaskAssignment.group_id' => 0, 'TaskAssignment.role' => ROLE_USER));
            if ($return) {

                $this->updateCounterCache(array('task_id' => $task_ids));
            }
            return $return;
        } else {
            return FALSE;
        }
    }

    public function taskAssigned($user_id = NULL, $network_id = null) {
//       if(isset($network_id)){
        return $this->find('all', array(
                    'conditions' => array(
                        'TaskAssignment.user_id' => $user_id,
                        'TaskAssignment.network_id' => $network_id,
                        'TaskAssignment.role' => 'user'
                    ),
                    'fields' => array('TaskAssignment.task_id'),
                    'order' => 'TaskAssignment.created desc'
        ));
//       }
//        
//        return $this->find('all',array(
//            'conditions' => array(
//                'TaskAssignment.user_id' =>$user_id,
//                'TaskAssignment.role' => 'user'
//            ),
//            'fields' => array('TaskAssignment.task_id'),
//            'order' => 'TaskAssignment.created desc'
//        ));
    }

    public function taskAssignedToGroup($group_id = NULL) {
        return $this->find('all', array(
                    'conditions' => array(
                        'TaskAssignment.group_id' => $group_id
//                'TaskAssignment.role' => 'user'
                    ),
                    'fields' => array('TaskAssignment.task_id'),
                    'order' => 'TaskAssignment.created desc'
        ));
    }

    public function getTaskUser($network_id = NULL, $user_id = null, $page = null, $limit = null) {

//        $this->log("in get taskuser");

        $task_created = $this->taskCreated($network_id, $user_id, $page, $limit);
//        $this->log($task_created);
        $task_assigny = $this->taskAssigne($network_id, $user_id, $page, $limit);
//        $this->log($task_assigny);
        $task_created = array_merge($task_created, $task_assigny);

//        rsort($task_created,"cmp");

        $task_created = Set::sort($task_created, '{n}.{n}.{ids}', 'desc');
//           usort($task_created, 'sortByOrder');

        $this->log($task_created);

        $user_ids = array_unique(Hash::extract($task_created, '{n}.TaskAssignment[list!=' . $user_id . '].list'));

//        $this->log($);
        return $user_ids;
////        $this->log($user_ids);
//        
//        $result = $this->find('all',array(
//            'conditions' => array(
//                'TaskAssignment.network_id' =>$network_id,
//                'Or'=> array(
//                             'TaskAssignment.added_by' =>$user_id,              
//                             'TaskAssignment.user_id' =>$user_id
//                          )
//                ),
//
//            'group' => array('TaskAssignment.user_id','TaskAssignment.added_by'),
//            'fields' => array('COUNT(TaskAssignment.user_id) as ids','TaskAssignment.user_id','COUNT(TaskAssignment.added_by) as cr_id','TaskAssignment.added_by'),
//            'limit' => $limit/*Task_To_Load*/,
//            'page' => $page,
//            'order' => 'ids desc'
//        ));
////        $already_assignee['group'] = Hash::extract($already_assignee, '{n}.TaskAssignment[group_id!=0].group_id');
//        
//        foreach ($result as $key => $value) {
//            if($result[$key]['TaskAssignment']['user_id'] == $user_id){
//                $result[$key]['TaskAssignment']['list'] = $result[$key]['TaskAssignment']['added_by'];
//            }  else {
//                $result[$key]['TaskAssignment']['list'] = $result[$key]['TaskAssignment']['user_id'];
//            }
////            $result[$key]['TaskAssignment']['list'] = $result[$key]['TaskAssignment']['user_id'];
////            $result[$key]['TaskAssignment']['list'] = $result[$key]['TaskAssignment']['added_by'];
//        }
//        $this->log($result);    
//        
//        $user_ids = Hash::extract($result, '{n}.TaskAssignment[list!='.$user_id.'].list');
////        $this->log($user_ids);
////        $added_by = Hash::extract($result, '{n}.TaskAssignment[added_by!='.$user_id.'].added_by');
////        $this->log($added_by);
////        $user_ids = array_merge($user_ids, $added_by);
//        $this->log($user_ids);
////        $user_ids = array_unique($user_ids);
//        
//         if(count(array_unique($user_ids))<count($user_ids))
//        {
//    // Array has duplicates
//    $this->log("Array has duplicates");
//            $user = $this->getTaskUser($network_id, $user_id, ++$page, $limit = 6/*Task_To_Load*/-count(array_unique($user_ids)));
////            $user_ids = array_unique($user_ids);
//            $user_ids = array_unique(array_merge($user_ids, $user));
//            $loadMore = TRUE;
//            $this->log($user_ids);
//        }
//        else{
//            return $user_ids;
//        }
//        if(count($user_ids) < (6/*Task_To_Load*/)){
//            $user = $this->getTaskUser($network_id, $user_id, ++$page, $limit = 6/*Task_To_Load*/-count(array_unique($user_ids)));
////            $user_ids = array_unique($user_ids);
//            $user_ids = array_unique(array_merge($user_ids, $user));
//            $loadMore = TRUE;
//            $this->log($user_ids);
//        }
//        return $user_ids;
    }

    public function getTaskId($network_id = NULL, $user_id = null) {

        return $this->find('list', array(
                    'conditions' => array(
                        'TaskAssignment.network_id' => $network_id,
                        'TaskAssignment.user_id =' => $user_id,
//                'AND'=> array(
//                             'TaskAssignment.added_by' =>$user_id,              
//                             'TaskAssignment.user_id !=' =>$user_id
//                          )
                    ),
//            'group' => array('TaskAssignment.user_id'/*,'TaskAssignment.added_by'*/),
                    'fields' => array('TaskAssignment.task_id'),
//            'limit' => $limit/*Task_To_Load*/,
//            'page' => $page,
                    'order' => 'TaskAssignment.user_id desc'
        ));
    }

    public function getTaskUserId($task_id = array(), $user_id = null, $page = null, $limit = null) {

        $result = $this->find('all', array(
            'conditions'=> array(
//                'TaskAssignment.network_id' => $network_id,
                'TaskAssignment.user_id != ' => $user_id,
                'TaskAssignment.task_id' => $task_id,
            ),
            'contain'=> array('UserNetwork'),
            'group' => array('TaskAssignment.user_id'),
            'fields' => array('COUNT(TaskAssignment.user_id) as ids', 'TaskAssignment.user_id','TaskAssignment.task_id'),
            'order' => 'ids desc',
            'limit'=> $limit,
            'page'=> $page
        ));
        $result = Hash::extract($result, '{n}.TaskAssignment.user_id');
        return $result;
    }
    
    public function getTaskGroupId($group_id = array(), $user_id = null, $page = null, $limit = null) {

        $result = $this->find('all', array(
            'conditions' => array(
                'TaskAssignment.group_id' => $group_id,
//                'TaskAssignment.user_id !=' => $user_id
//                'AND'=> array(
//                             'TaskAssignment.added_by' =>$user_id,              
//                             'TaskAssignment.user_id !=' =>$user_id
//                          )
            ),
            'group' => array('TaskAssignment.group_id'),
            'fields' => array('COUNT(TaskAssignment.group_id) as ids', 'TaskAssignment.group_id'),
            'limit' => $limit/* Task_To_Load */,
            'page' => $page,
            'order' => 'ids desc'
        ));
        $result = Hash::extract($result, '{n}.TaskAssignment.group_id');
        return $result;
    }
    
    public function getGroupId($network_id = null) {
        $result = $this->find('list', array(
                            'conditions' => array(
                            'TaskAssignment.network_id' => $network_id,
                                'TaskAssignment.group_id !=' => 0
                ),
            
                            'fields' => 'TaskAssignment.group_id'));
       return array_unique($result);
    }
    

    public function getUserId($task_ids = null) {
        $result = $this->find('list', array(
                            'conditions' => array(
                            'TaskAssignment.task_id' => $task_ids),
                            'fields' => 'TaskAssignment.user_id'));
                        return array_unique($result);
    }

    public function taskCreated($network_id = NULL, $user_id = null, $page = null, $limit = null) {

        $result = $this->find('all', array(
            'conditions' => array(
                'TaskAssignment.network_id' => $network_id,
                'AND' => array(
                    'TaskAssignment.added_by' => $user_id,
                    'TaskAssignment.user_id !=' => $user_id
                )
            ),
            'group' => array('TaskAssignment.user_id'/* ,'TaskAssignment.added_by' */),
            'fields' => array('COUNT(TaskAssignment.user_id) as ids', 'TaskAssignment.user_id'/* ,'COUNT(TaskAssignment.added_by) as cr_id' */, 'TaskAssignment.added_by'),
            'limit' => $limit/* Task_To_Load */,
            'page' => $page,
            'order' => 'ids desc'
        ));
        foreach ($result as $key => $value) {
            if ($result[$key]['TaskAssignment']['user_id'] == $user_id) {
                $result[$key]['TaskAssignment']['list'] = $result[$key]['TaskAssignment']['added_by'];
            } else {
                $result[$key]['TaskAssignment']['list'] = $result[$key]['TaskAssignment']['user_id'];
            }
//            $result[$key]['TaskAssignment']['list'] = $result[$key]['TaskAssignment']['user_id'];
//            $result[$key]['TaskAssignment']['list'] = $result[$key]['TaskAssignment']['added_by'];
        }
        return $result;
    }

    public function taskAssigne($network_id = NULL, $user_id = null, $page = null, $limit = null) {

        $result = $this->find('all', array(
            'conditions' => array(
                'TaskAssignment.network_id' => $network_id,
                'AND' => array(
                    'TaskAssignment.added_by !=' => $user_id,
                    'TaskAssignment.user_id' => $user_id
                )
            ),
            'group' => array(/* 'TaskAssignment.user_id', */'TaskAssignment.added_by'),
            'fields' => array(/* 'COUNT(TaskAssignment.user_id) as ids', */'TaskAssignment.user_id', 'COUNT(TaskAssignment.added_by) as ids', 'TaskAssignment.added_by'),
            'limit' => $limit/* Task_To_Load */,
            'page' => $page,
            'order' => 'ids desc'
        ));

        foreach ($result as $key => $value) {
            if ($result[$key]['TaskAssignment']['user_id'] == $user_id) {
                $result[$key]['TaskAssignment']['list'] = $result[$key]['TaskAssignment']['added_by'];
            } else {
                $result[$key]['TaskAssignment']['list'] = $result[$key]['TaskAssignment']['user_id'];
            }
//            $result[$key]['TaskAssignment']['list'] = $result[$key]['TaskAssignment']['user_id'];
//            $result[$key]['TaskAssignment']['list'] = $result[$key]['TaskAssignment']['added_by'];
        }
        return $result;
    }
    
    
    public function getAllTaskUser($task_id = array(), $user_id = null, $page = null, $limit = null){

        return $this->find('all', array(
            'conditions'=> array(
//                'TaskAssignment.network_id' => $network_id,
                'TaskAssignment.user_id != ' => $user_id,
                'TaskAssignment.task_id' => $task_id,
            ),
            'contain'=> array('UserNetwork'),
            'group' => array('TaskAssignment.user_id'),
            'fields' => array('COUNT(TaskAssignment.user_id) as ids', 'TaskAssignment.user_id','TaskAssignment.task_id'),
            'order' => 'ids desc',
            'limit'=> $limit,
            'page'=> $page
        ));
    }

}

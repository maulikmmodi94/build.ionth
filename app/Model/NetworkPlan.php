<?php

App::uses('AppModel', 'Model');

/**
 * NetworkPlan Model
 *
 * @property Network $Network
 * @property Plan $Plan
 */
class NetworkPlan extends AppModel {

    public $useTable = 'network_plans';

    //The Associations below have been created with all possible keys, those that are not needed can be removed

    /**
     * belongsTo associations
     *
     * @var array
     */
    public $belongsTo = array(
        'Network' => array(
            'className' => 'Network',
            'foreignKey' => 'network_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'Plan' => array(
            'className' => 'Plan',
            'foreignKey' => 'plan_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );

    /**
     * Constructor
     *
     * @param bool|string $id ID
     * @param string $table Table
     * @param string $ds Datasource
     */
    public function __construct($id = false, $table = null, $ds = null) {
        parent::__construct($id, $table, $ds);
        $this->defaultConditions = array(
            $this->alias . '.isDeleted' => NOT_DELETED
        );
    }
    
    public function init()
    {
        
    }

}

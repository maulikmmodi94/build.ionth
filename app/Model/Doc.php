<?php

App::import('Vendor', 'S3', array('file' => 'S3' . DS . 'S3.php'));
App::uses('DocsAppModel', 'Model');

class Doc extends DocsAppModel {

    private $_bucket_name = _bucket_name;
    public $components = array('RequestHandler');
    public $useTable = 'docs';
    public $hasMany = array(
        'DocAssignment' => array(
            'className' => 'DocAssignment',
            'foreignKey' => 'doc_id',
            'dependent' => true,
            'conditions' => array(
                'DocAssignment.isDeleted' => NOT_DELETED
            ),
        )
    );
    public $virtualFields = array(
        'full_name' => 'CONCAT(Doc.name, ".", Doc.file_extension)'
    );
    public $validate = array(
        'hash_name' => array(
//            'rule1' => array(
//                'rule' => 'notEmpty',
//                'message' => 'File Name can not be empty'
//            ),
//            'rule2' => array(
//                'rule' => array('maxLength', 255),
//                'message' => 'FIle Name can not be more than 255'
//            )
        ),
        'hash_id' => array(
            'rule1' => array(
                'rule' => 'notEmpty',
                'message' => 'Unique File ID can not be empty'
            ),
            'rule2' => array(
                'rule' => 'isUnique',
                'message' => 'Unique File ID already exist'
            )
        ),
        'file_size' => array(
            'rule1' => array(
                'rule' => 'notEmpty',
                'message' => 'FIle Size can not be empty'
            ),
            'rule2' => array(
                'rule' => 'Numeric',
                'message' => 'File Size can only be Numeric'
            )
        ),
        'file_extension' => array(
            'rule1' => array(
                'rule' => 'notEmpty',
                'message' => 'File Extension can not be empty'
            ),
            'rule2' => array(
                'rule' => array('maxLength', 15),
                'message' => 'File Extension can not be more than 15'
            )
        ),
        'file_mime' => array(
            'rule1' => array(
                'rule' => 'notEmpty',
                'message' => 'File Mime Type can not be empty'
            ),
            'rule2' => array(
                'rule' => array('maxLength', 128),
                'message' => 'File Mime can not be more than 128'
            )
        ),
        'user_id' => array(
            'rule1' => array(
                'rule' => 'notEmpty',
                'message' => 'Uploaded user id can not be empty'
            ),
            'rule2' => array(
                'rule' => 'numeric',
                'message' => 'User Id can only be Numeric'
            )
        ),
        'bucket_path' => array(
            'rule1' => array(
                'rule' => 'notEmpty',
                'message' => 'Bucket Path can not be empty'
            ),
            'rule2' => array(
                'rule' => array('maxLength', 150),
                'message' => 'Bucket Path can be maximum of 150 length'
            )
        ),
        'preview_url' => array(
            'rule1' => array(
                'rule' => 'url',
                'message' => 'Not a valid URL'
            )
        ),
        'isDeleted' => array(
            'rule1' => array(
                'rule' => 'notEmpty',
                'message' => 'Status can not be null'
            ),
            'rule2' => array(
                'rule' => array('inList', array(0, 1)),
                'message' => 'isDeleted can only be from (0,1)'
            )
        ),
        'created' => array(
            'rule1' => array(
                'rule' => 'notEmpty',
                'created time can not be empty'
            ),
            'rule2' => array(
                'rule' => 'datetime',
                'message' => 'Please give a valid date and time.'
            )
        ),
        'modified' => array(
            'rule1' => array(
                'rule' => 'notEmpty',
                'created time can not be empty'
            ),
            'rule2' => array(
                'rule' => 'datetime',
                'message' => 'Please give a valid date and time.'
            )
        ),
        'deleted' => array(
            'rule1' => array(
                'rule' => 'datetime',
                'message' => 'Please give a valid date and time.'
            )
        )
    );
    public $defaultConditions = array('Doc.isDeleted  ' => NOT_DELETED);

    function addDoc($userid, $network_id, $sharelist = NULL, $data) {
        $dataSource = $this->getDataSource();

        $this->create();
        $data['Doc'] ['user_id'] = $userid;
        $data['Doc'] ['network_id'] = $network_id;
        $docassignment = array();
        $values = array();
        $values['user_id'] = $userid;
        $values['network_id'] = $network_id;
        $values['shared_by'] = $userid;
        $values['status'] = STATUS_ACTIVE;
        array_push($docassignment, $values);
        if (!empty($sharelist['user']) || !empty($sharelist['group'])) {
            $values = array();
            foreach ($sharelist['user'] as $key => $value) {
                $values['user_id'] = $value;
                $values['network_id'] = $network_id;
                $values['shared_by'] = $userid;
                $values['status'] = STATUS_ACTIVE;
                array_push($docassignment, $values);
            }
            $values = array();
            foreach ($sharelist['group'] as $key => $value) {
                $values['group_id'] = $value;
                $values['network_id'] = $network_id;
                $values['shared_by'] = $userid;
                $values['status'] = STATUS_ACTIVE;
                array_push($docassignment, $values);
            }
        }
        $data['DocAssignment'] = $docassignment;
        if ($this->saveAssociated($data)) {
            return $this->getLastInsertID();
        } else {
            return false;
        }
    }

    /*
     * Function that return DocShare details for given Doc IDs in a 
     * network
     */

    public function getDocs($docs_ids) {
        return $this->find('all', array(
                    'conditions' => array('Doc.id' => $docs_ids),
                    'fields' => array('Doc.id', 'Doc.name', 'Doc.file_size', 'Doc.file_extension', 'Doc.created', 'Doc.user_id', 'Doc.file_mime', 'Doc.comment_count', 'Doc.preview_url','Doc.thumbnailLink', 'Doc.provider','Doc.full_name'),
                    'order' => array('Doc.modified DESC'),
                    'contain' => array('DocAssignment' => array(
//                            'conditions' => array(
//                                'DocAssignment.isDeleted' => NOT_DELETED
//                            ),
                            'fields' => array('DocAssignment.group_id', 'DocAssignment.user_id', 'DocAssignment.shared_by')
                        ),
                        'Comment'
                    )
        ));
    }

    public function getDocField($field_name, $doc_id, $network_id) {
        $field = 'Doc.' . $field_name;
        $doc = $this->find('all', array('conditions' => array('id' => $doc_id, 'network_id' => $network_id), 'fields' => $field_name));
        return isset($doc[0]['Doc'][$field_name]) ? $doc[0]['Doc'][$field_name] : NULL;
    }

    /*
     * Function that checks if Doc Exist and isActive with the given Doc_ID
     */

    public function getDocObject($doc_id, $network_id) {
        return $this->find('first', array(
                    'conditions' => array('Doc.id' => $doc_id),
                    'fields' => array('Doc.id', 'Doc.name', 'Doc.file_size', 'Doc.file_extension', 'Doc.created', 'Doc.user_id', 'Doc.file_mime', 'Doc.preview_url'),
        ));
    }

    /*
     * Function returns total file space used by the a 
     * user in a network
     */

    public function getFileSpaceUser($user_id, $network_id) {

        $count = $this->find('all', array(
            'conditions' => array(
                'Doc.user_id' => $user_id,
                'Doc.network_id' => $network_id
            ),
            'fields' => array(
                'sum(Doc.file_size) as file_size_count'
            )
                )
        );
        return $count[0][0]['file_size_count'];
    }

    /*
     * Function that returns total file space used by a 
     * network. 
     */

    public function getFileSpaceNetwork($network_id) {
        $count = $this->find('all', array(
            'conditions' => array(
                'Doc.network_id' => $network_id
            ),
            'fields' => array(
                'sum(Doc.file_size) as file_size_count'
            )
                )
        );
        return $count[0][0]['file_size_count'];
    }

    /*
     * Function to upload file to AWS S3 Service
     */

    public function uploadFile($bucket_path, $name, $network_id, $userid) {
        $this->s3_filepath = S3_File_Upload_Path . $network_id . "/" . $userid . "/";
        $s3 = new S3(_aws_access_key, _aws_secret_key);
        if (is_uploaded_file($name)) {
            if ($s3->putObjectFile($name, $this->_bucket_name, $bucket_path, S3::ACL_AUTHENTICATED_READ, array(), array(
                        "Content-Disposition" => "attachment",
                        "Content-Type" => "application/octet-stream"
                    ))) {
                return true;
            }
        }
        return false;
    }

    /*
     * Function to delete file from AWS S3 Service 
     */

    public function deleteFile($bucket_path) {
        $s3 = new S3(_aws_access_key, _aws_secret_key);
        if ($s3->deleteObject($this->_bucket_name, $bucket_path)) {
            return true;
        } else {
            return false;
        }
    }

    public function deleteDoc($id) {
        return $this->delete($id);
    }

    /*
     * getTemp path  of existing file.
     */

    public function getURL($bucket_path) {
       $s3 = new S3(_aws_access_key, _aws_secret_key);
       $url = $s3->getAuthenticatedURL(_bucket_name, $bucket_path, 3600, false);
        return $url;
    }

    public function isPartOfDoc($data) {
        $groups = ClassRegistry::init('UsersGroup');
        $group_ids = $groups->getActiveGroups($data['user'], $data['network_id']);
        //Check if user or his groups is a part of the doc
        $count = $this->DocAssignment->hasAny(array(
            'DocAssignment.doc_id' => $data['id'],
            'Or' => array(
                array('DocAssignment.user_id' => $data['user']),
                array('DocAssignment.group_id' => $group_ids)
            )
        ));
        if ($count == sizeof($data['user']))
            return true;
        else
            return false;
    }
    /**
     * search of docs by their names
     * @param type $doc_id
     * @param type $options
     * @return type
     */
    public function likeDocs($doc_id = null, $options = array(), $operation) {
        return $this->find($operation, array(
                    'conditions' => array(
                        'Doc.name like' => '%'.$options['type'].'%',
                        'Doc.id' => $doc_id
                    ),
                    'fields' => array('Doc.id'),
                    'order' => array('Doc.modified DESC'),
        ));
    }
}

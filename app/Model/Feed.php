<?php
 
class Feed extends AppModel {

    public $actsAs = array(
        'Tags.Taggable','Like.Likeable'
    );
    
//     public $hasAndBelongsToMany = array(
//        'Tag' => array(
//            'with' => 'Tagged'
//        )
//    );

    public $validate = array(
        'preview_link' => array(
            'rule1' => array(
                'rule' => 'url',
                'message' => 'Not a valid URL'
            )
        ),
        'message' => array(
            'rule1' => array(
                'rule' => 'notEmpty',
                'message' => 'Message can not be empty'
            ),
            'rule2' => array(
                'rule' => array('maxLength', 500),
                'message' => 'Message can be of maximum 500 characters'
            )
        ),
    );

    public $defaultConditions = array('Feed.is_Deleted' => NOT_DELETED);
    
    /**
     * 
     * decription : return the feeds which are exact match with the given feed.
     * @param int $tagName Description : it is the value of search tag
     */
    public function getFeedsSearch($tagName, $network_id = null, $page = 1, $limit = Feeds_to_load) {

        $result = $this->Tagged->find('tagged', array(
            'by' => $tagName,
            'model' => $this->alias,
            'recursive'=>0,
            'conditions'=>array($this->alias.'.network_id'=>$network_id),
            'order' => 'Feed.id desc',
            'limit' => $limit,
            'page' => $page
        ));
        
        $feeds=$this->gets(Hash::extract($result,'{n}.Feed.id'));
        return $feeds;
    }

    /**
     * description : return the last feed ID which is in the feed table.
     * @param type $network_id
     * @param type $user_id
     * @return type
     */
    public function getLastFeeds($network_id) {
        $lastFeed = $this->find('first', array(
                'conditions' => array('Feed.network_id' => $network_id),
//                                            'Feed.user_id' => $user_id),
                'fields' => 'Feed.id',
                'order' => array('Feed.id DESC')
                )
            );
        return isset($lastFeed['Feed']['id'])?$lastFeed['Feed']['id']:NULL;
    }

    /**
     * description : return the latest feeds which are not seen by user.
     * @param type $network_id
     * @param type $feedId
     * @return type
     */
    public function getLatestFeeds($network_id, $feedId = null) {
        $this->recursive = 1;
        return $this->find('all', array(
                    'conditions' => array('Feed.network_id' => $network_id,
                        'Feed.id >' => $feedId),
//                    'contain' => array('Comment'),
//                    'limit' => $limit,
//                    'page' => $page,
                    'order' => array('Feed.id DESC')
        ));
    }
    
    /**
     * Function to reterieve feeds and its associated items such as likes, comment
     * @param type $network_id
     * @param type $page
     * @param type $user_id
     * @param type $limit
     * @return type
     */
    public function getFeeds($network_id = null, $page = 1, $user_id = null, $limit = Feeds_to_load) {
        
        
//        SELECT TOP 10 * From (SELECT TOP 10 * FROM Customers where CustomerID = 90 
//            union
//        SELECT TOP 10 * FROM Customers where CustomerID <> 90 Order By CustomerID desc)
        $result = $this->find('all', array(
                    'conditions' => array('Feed.network_id' => $network_id),
                    'contain' => array(
                                    'Like' => array(
                                        'limit' => Likes_to_load,
                                           ),
                                    'Comment','Tag'
                                ),
                    'limit' => $limit,
                    'page' => $page,
                    'order'=>array('Feed.id DESC')
                        )
        );
        return $result;
                
    }

    public function addFeeds($data) {
        $this->create();
//        return $this->save($data, array(
//                    'fieldList' => array('user_id', 'network_id', 'message', 'preview_link')
//        ));
//        debug($data);
////        debug($this->saveAll($data));
////        return;
        return $this->saveAll($data);
    }

    public function editFeeds($id, $data) {
        $this->id = $id;
        return $this->save($data, array(
                    'fieldList' => array('message', 'preview_link')
        ));
    }
    
    /**
     * This function returns the user id of the feed's creator if it is not the current user. Otherwise it will return an empty list.
     * @param int $feed_id the id of the feed
     * @return list Returns the id of the user to whom the feed belongs
     */
    public function feedLike($feed_id) {
        return $this->find('list',array(
                        'conditions' => array('Feed.id' => $feed_id, 'Feed.user_id <>' => AuthComponent::user('id')),
                        'fields' => array('Feed.user_id')
        ));
    }
    
    /**
     * This function returns the user id of the feed's creator and all the people who commented on the feed other than the current user.
     * @param int $feed_id the id of the feed
     * @return list Returns the id of the user to whom the feed belongs and who have commented on the feed except the current user.
     */
    public function getfeedCommentUsers($feed_id) {
        $condition = array('Comment.model' => 'Feed','Comment.entity_id' => $feed_id);
        $result = $this->find('first',array(
                        'conditions' => array('Feed.id' => $feed_id),
                        'contain' => array('Comment' => array(
                            'conditions' => array('Comment.user_id <>' => AuthComponent::user('id')),
                            'limit' => $this->Comment->find('count',array('conditions' => $condition)),
                            'fields' => array('Comment.user_id')
                        )),
                        'fields' => array('Feed.user_id')
        ));
        $temp = hash::extract($result, 'Comment.{n}.user_id');
        array_push($temp, $result['Feed']['user_id']);
        return array_diff(array_unique($temp), array(AuthComponent::user('id')));
    }
    
    public function gets($ids,$options=array())
    {
        return $this->find('all',array(
           'conditions'=>array(
               $this->alias.'.id'=>$ids
           ),
            'contain'=>array('Like','Comment','Tag'),
            'order' => array('Feed.id DESC')
        ));
    }

    public function isPartOfFeed($feed_id)
    {   
        return $this->hasAny(array(
            'Feed.id' => $feed_id
        ));
    }
    
}
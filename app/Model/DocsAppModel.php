<?php

App::uses('AppModel', 'Model');

/**
 * Application model for Cake.
 *
 * Add your application-wide methods in the class below, your models
 * will inherit them.
 *
 * @package       app.Model
 */
class DocsAppModel extends AppModel {

    /**
     * Behaviors
     *
     * @var string $name
     */
    public $actsAs = array(
        'Utils.SoftDelete' => array('isDeleted' => 'deleted')
    );

}

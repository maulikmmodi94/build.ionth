<?php

/**
 * Created by PhpStorm.
 * User: Nikhil Maheshwari
 */
class Notification extends AppModel {

    public $useTable = 'notifications';
    public $primaryKey = 'id';
    public $belongsTo = array(
        'Log' => array(
            'className' => 'Log',
            'foreignKey' => 'log_id'
        )
    );
    public $validate = array(
        'user_id' => array(
            'rule1' => array(
                'rule' => 'notEmpty',
                'message' => 'user id can not be empty'
            ),
            'rule2' => array(
                'rule' => 'numeric',
                'message' => 'User Id can only be Numeric'
            )
        ),
        'network_id' => array(
            'rule1' => array(
                'rule' => 'notEmpty',
                'message' => 'Network id can not be empty'
            ),
            'rule2' => array(
                'rule' => 'numeric',
                'message' => 'Network Id can only be Numeric'
            )
        ),
        'isSeen' => array(
            'rule1' => array(
                'rule' => 'notEmpty',
                'message' => 'Notification status can not be empty'
            ),
            'rule2' => array(
                'rule' => array('inList', array(1, 0)),
                'message' => 'Notification status either be read or unread'
            )
        ),
        'log_id' => array(
            'rule1' => array(
                'rule' => 'notEmpty',
                'message' => 'Log id can not be empty'
            ),
            'rule2' => array(
                'rule' => 'numeric',
                'message' => 'Log Id can only be Numeric'
            )
        )
    );
 
    public $defaultConditions = array('Notification.isDeleted' => NOT_DELETED);
    /*
     * Function to add a new Notification
     */

    public function add($userlist, $network_id, $log_id) {
        $this->create();
        $data = [];
        foreach ($userlist as $key => $user_id) {
            $value = [];
            $value['Notification']['user_id'] = $user_id;
            $value['Notification']['network_id'] = $network_id;
            $value['Notification']['isSeen'] = NOT_SEEN;
            $value['Notification']['log_id'] = $log_id;
            array_push($data, $value);
        }

        if ($this->saveMany($data)) {
            //Increment UserNetwork unseenNotification Count
            $this->increaseUserNetworkNotification($network_id, $userlist);
            return true;
        } else {
            return false;
        }
    }

    function increaseUserNetworkNotification($network_id, $userlist, $value = NULL) {
        $UserNetwork = ClassRegistry::init('UserNetwork');
        $UserNetwork->updateAll(
                array('UserNetwork.unseen_notification' => '`UserNetwork`.`unseen_notification`+1'), array('UserNetwork.network_id' => $network_id, 'UserNetwork.user_id' => $userlist)
        );
    }
/**
 * this method is used to update notification count when some notification is read
 * @param type $network_id
 * @param type $userlist
 * @param type $value
 */
    public function updateUserNetworkNotification($network_id, $userlist, $value = NULL) {
        $UserNetwork = ClassRegistry::init('UserNetwork');
        $UserNetwork->updateAll(
                array('UserNetwork.unseen_notification' => $value), array('UserNetwork.network_id' => $network_id, 'UserNetwork.user_id' => $userlist)
        );
    }
    /**
     * this method is used for set notification read when it opens
     * @param type $network_id
     * @param type $user_id
     * @param type $notification_id
     * @return type
     */
    public function setNotificationSeen($network_id, $user_id, $notification_id= null) {

        $notification_read = $this->updateAll(array(
                                'Notification.isSeen' => SEEN
                                ), array(
                                'Notification.isSeen' => NOT_SEEN,
                                'Notification.network_id' => $network_id,
                                'Notification.user_id' => $user_id,
                                'Notification.id' => $notification_id,       
        ));
        
        if($notification_read){
            $unread_notification_count = $this->find('count', array(
                'conditions' => array(
                    'Notification.network_id' => $network_id,
                    'Notification.user_id' => $user_id,
                    'Notification.isSeen' => NOT_SEEN,
                )
            ));
            $this->updateUserNetworkNotification($network_id, $user_id, $unread_notification_count);
            return true;
        }
        return false;
    }
    /*
     * Function to change status of Notification from NotSeen to SEEN
     */

    public function setIsSeen($network_id, $user_id, $latest_notification_id) {
//        $value['Notification']['isSeen'] = SEEN;
        return $this->updateAll(array(
                    'Notification.isSeen' => SEEN
                        ), array(
                    'Notification.isSeen' => NOT_SEEN,
                    'Notification.network_id' => $network_id,
                    'Notification.user_id' => $user_id,
                    'Notification.id <=' => $latest_notification_id,       
        ));
    }

    /*
     * Function that returns the Notifications of a user in a network
     */

    public function getNotification($user_id, $network_id, $page, $notoload) {
        
        return $this->find('all', array(
                    'conditions' => array(
                        'Notification.user_id' => $user_id,
                        'Notification.network_id' => $network_id
                    ),
                    'order' => array('Notification.id Desc'),
                    'limit' => $notoload,
                     'page' => $page,
        ));
    }

    public function getUnseenNotifications($user_id, $network_id, $latest_notification_id) {
        return $this->find('all', array(
                    'conditions' => array(
                        'Notification.user_id' => $user_id,
//                        'Notification.isSeen' => NOT_SEEN,
                        'Notification.network_id' => $network_id,
                        'Notification.id >' => $latest_notification_id,
                    ),
                    'order' => array('Notification.id Desc')
        ));
    }
    
    public function countAndReadUnseenNotifications($network_id, $user_id, $latest_notification_id, $latest_notification_logid){
        
        $this->setIsSeen($network_id, $user_id, $latest_notification_id);
        
        $unread_notification_count = $this->find('count', array(
                                'conditions' => array(
//                                        'Notification.isSeen' => NOT_SEEN,
                                        'Notification.user_id' => $user_id,
                                        'Notification.network_id' => $network_id,
                                        'Notification.id >' => $latest_notification_id,
                                        'Notification.log_id !=' => $latest_notification_logid,
                                    ),
                                'order' => array('Notification.id Desc')
        ));
        
        return $unread_notification_count;
    }

        public function isNewNotification($user_id, $network_id, $latest_notification_id){
        
        $new_notification_count = $this->find('count', array(
                                'conditions' => array(
                                        'Notification.user_id' => $user_id,
                                        'Notification.network_id' => $network_id,
                                        'Notification.id >' => $latest_notification_id,
                                    ),
                                'order' => array('Notification.id Desc')
        ));
        if($new_notification_count > 0){
            return TRUE;
        }  else {
            return FALSE;
        }
        
    }

    /*
     * Function to remove a notification
     */

    public function deleteNotification($notification_id) {
        
        $conditions=array($this->alias.'.id'=>$notification_id);
        
        return $this->soft_delete($conditions);
        
    }

}

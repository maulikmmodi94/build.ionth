<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
App::uses('CakeEvent', 'Event');

class UsersGroup extends AppModel {

    public $useTable = 'user_groups';
    public $defaultConditions = array('UsersGroup.isDeleted' => NOT_DELETED);
    public $belongsTo = array(
        'User' => array(
            'className' => 'User'
        ),
        'Group' => array(
            'className' => 'Group',
            'counterCache' => TRUE,
            'counterScope' => array('UsersGroup.isDeleted' => NOT_DELETED),
        )
    );
    
     public $hasOne = array(
        'UsersGroupSelfJoin' => array(
            'className' => 'UsersGroup',
            'foreignKey' => false,
            'type' => 'inner',
        )
    );
    
    public $validate = array(
        'user_id' => array(
            'rule1' => array(
                'rule' => 'numeric',
                'message' => 'User-Id needs to be numeric.'
            ),
            'rule2' => array(
                'rule' => 'notEmpty',
                'message' => 'User-Id cannot be null'
            )
        ),
        'group_id' => array(
            'rule1' => array(
                'rule' => 'numeric',
                'message' => 'Group-Id needs to be numeric.'
            ),
            'rule2' => array(
                'rule' => 'notEmpty',
                'message' => 'Group-Id cannot be null'
            )
        ),
        'network_id' => array(
            'rule1' => array(
                'rule' => 'numeric',
                'message' => 'Network-Id needs to be numeric.'
            ),
            'rule2' => array(
                'rule' => 'notEmpty',
                'message' => 'Network-Id cannot be null'
            )
        ),
        'added_by' => array(
            'rule1' => array(
                'rule' => 'numeric',
                'message' => 'Added-by field needs to be numeric.'
            ),
            'rule2' => array(
                'rule' => 'notEmpty',
                'message' => 'Added-by field cannot be null'
            )
        ),
        'removed_id' => array(
            'rule1' => array(
                'rule' => 'numeric',
                'message' => 'Removed-by field needs to be numeric.'
            ),
            'rule2' => array(
                'rule' => 'notEmpty',
                'message' => 'Removed-by field cannot be null'
            )
        ),
        'role' => array(
            'rule1' => array(
                'rule' => 'notEmpty',
                'message' => 'Role cannot be null'
            )
        ),
        'status' => array(
            'rule1' => array(
                'rule' => 'notEmpty',
                'message' => 'Status cannot be null'
            ),
            'rule2' => array(
                'rule' => array('maxLength', '20'),
                'message' => 'status can not be more than 20'
            )
        ),
        'isDeleted' => array(
            'rule1' => array(
                'rule' => 'notEmpty',
                'message' => 'Status can not be null'
            )
        ),
        'created' => array(
            'rule1' => array(
                'rule' => 'notEmpty',
                'created time can not be empty'
            ),
            'rule2' => array(
                'rule' => 'datetime',
                'message' => 'Please give a valid date and time.'
            )
        ),
        'modified' => array(
            'rule1' => array(
                'rule' => 'notEmpty',
                'created time can not be empty'
            ),
            'rule2' => array(
                'rule' => 'datetime',
                'message' => 'Please give a valid date and time.'
            )
        ),
        'deleted' => array(
            'rule1' => array(
                'rule' => 'datetime',
                'message' => 'Please give a valid date and time.'
            )
        )
    );

    public function setUsersGroupField($user_id, $group_id, $network_id, $field_name, $value) {
        $field = 'UsersGroup.' . $field_name;
        return $this->updateAll(array($field => $value), array('UsersGroup.user_id' => $user_id, 'UsersGroup.group_id' => $group_id, 'UsersGroup.network_id' => $network_id));
    }

    public function getUsersGroupField($user_id, $group_id, $network_id, $field_name) {
        $field = 'UsersGroup.' . $field_name;
        $network = $this->findByUserIdAndGroupIdAndNetworkId($user_id, $group_id, $network_id, array($field));
        return isset($network['UsersGroup'][$field_name]) ? $network['UsersGroup'][$field_name] : NULL;
    }

    public function addmember($group_id, $network_id, $added_by, $userlist) {
        //Create new entries
        $this->create();
        $data = [];
        foreach ($userlist as $user) {
            $temp = array();
            $temp['UsersGroup']['user_id'] = $user;
            $temp['UsersGroup']['group_id'] = $group_id;
            $temp['UsersGroup']['network_id'] = $network_id;
            $temp['UsersGroup']['added_by'] = $added_by;
            $temp['UsersGroup']['role'] = ROLE_USER;
            $temp['UsersGroup']['status'] = STATUS_ACTIVE;
            array_push($data, $temp);
        }
        if ($this->saveMany($data)) {
            $Event = new CakeEvent(
                    'Controller.Group.afteraddmember', $this, array('user_id' => $added_by, 'network_id' => $network_id, 'group_id' => $group_id, 'list' => $userlist)
            );
            $this->getEventManager()->dispatch($Event);
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function removemember($group_id, $userlist,$curr_user) {
        $dataSource = $this->getDataSource();
        $time = $dataSource->value(date('Y-m-d H:i:s'), 'string');
        if ($this->updateAll(
                        array('UsersGroup.isDeleted' => DELETED,
                            'UsersGroup.deleted' => $time,
                            'UsersGroup.removed_by' => $curr_user,
                            'UsersGroup.status' => "'" . STATUS_REMOVED . "'"), 
                array('UsersGroup.group_id' => $group_id, 'UsersGroup.user_id' => $userlist)
                )) {
            $this->updateCounterCache(array('group_id' => $group_id));
            return true;
        } else {
            return false;
        }
    }

    public function getActiveGroups($user_id, $network_id, $id = null) {
        if(empty($id)){
            $id = $user_id;
        }
        return $this->find('list', [
                    'conditions' => [
                        'UsersGroup.network_id' => $network_id,
                        'UsersGroup.user_id' => $user_id,
                    ],
                    'contain'=> ['UsersGroupSelfJoin' => [
                        'conditions' => [
                            'UsersGroupSelfJoin.group_id = UsersGroup.group_id',
                            'UsersGroupSelfJoin.user_id' => $id,
                            'UsersGroupSelfJoin.isDeleted' => NOT_DELETED,
                            'UsersGroupSelfJoin.status' => STATUS_ACTIVE
                    ]]],
                    'fields' => ['UsersGroup.group_id'],
                    'order' => ['UsersGroup.group_id desc']
    ]);
        
        
    }
    
    public function getGroup($network_id, $group_id) {
        $returnObject = array();
        $group =  $this->find('all', array(
                    'conditions' => array(
//                        'UsersGroup.user_id' => $user_id,
                        'UsersGroup.network_id' => $network_id,
                        'UsersGroup.group_id' => $group_id
                    )
//                    'fields' => array('UsersGroup.group_id')
        ));
        App::import('Model', 'UserInfo');
        $user = new UserInfo();
        foreach ($group as $value) {
//            $this->log($value['UsersGroup']['user_id']);    
        $value['UsersGroup']['user_info'] = $user->view($value['UsersGroup']['user_id']); 
//        $this->log($value);
        $value['UsersGroup']['user_info'] = $value['UsersGroup']['user_info'][0]['UserInfo'];
        array_push($returnObject, $value);
        }
        
//        $this->log($returnObject);
        return $returnObject;
    }
    
    
    public function getGroupId($group_id = array(), $network_id = null, $page = null, $limit = null) {
//        $this->log('in get groupID');
        $result =  $this->find('all', array(
                    'conditions' => array(
                        'UsersGroup.group_id !=' => $group_id,
                        'UsersGroup.network_id' => $network_id,
                    ),
//            'group' => array('UsersGroup.group_id'),
            'fields' => array('UsersGroup.group_id'),
            'limit' => $limit/* Task_To_Load */,
            'page' => $page,
            'order'=>'UsersGroup.group_id desc'
        ));
//        $this->log($result);
        $result = Hash::extract($result, '{n}.UsersGroup.group_id');
        return $result;
    }
    
//    public function getUserGroupId($group_id = array(), $network_id = null,$user_id = null , $page = null, $limit = null) {
////        $this->log('in get groupID');
//        $result =  $this->find('all', array(
//                    'conditions' => array(
//                        'UsersGroup.group_id !=' => $group_id,
//                        'UsersGroup.network_id' => $network_id,
//                        'UsersGroup.user_id' => $user_id
//                    ),
////            'group' => array('UsersGroup.group_id'),
//            'fields' => array('UsersGroup.group_id'),
//            'limit' => $limit/* Task_To_Load */,
//            'page' => $page,
//            'order'=>'UsersGroup.id desc'
//        ));
////        $this->log($result);
//        $result = Hash::extract($result, '{n}.UsersGroup.group_id');
//        return $result;
//    }
    
    public function getAllGroupId($group_id = array(), $network_id = null,$user_id = null, $page = null, $limit = null, $id = null) {
//        $this->log('in get groupID');
        if($id == 0){
            $result =  $this->find('all', array(
                    'conditions' => array(
                        'UsersGroup.group_id !=' => $group_id,
                        'UsersGroup.network_id' => $network_id,
                        'UsersGroup.user_id' => $user_id
                    ),
//            'group' => array('UsersGroup.group_id'),
            'fields' => array('UsersGroup.group_id'),
            'limit' => $limit/* Task_To_Load */,
            'page' => $page,
            'order'=>'UsersGroup.id desc'
        ));
        }else{
            $result =  $this->find('all', array(
                    'conditions' => array(
                        'UsersGroup.network_id' => $network_id,
                        'UsersGroup.user_id' => $user_id,
                        'And' => array(
                                'UsersGroup.group_id !=' => $group_id,
                                'UsersGroup.id <' => $id,
//                                 'Group.id <' => $id,
                                      )
                    ),
//            'group' => array('UsersGroup.group_id'),
            'fields' => array('UsersGroup.group_id'),
            'limit' => $limit/* Task_To_Load */,
            'page' => $page,
            'order'=>'UsersGroup.id desc'
        ));
        }
        $result = Hash::extract($result, '{n}.UsersGroup.group_id');
        return $result;
        
    }

    /**
     * getGroups
     *
     * @author Hardik Sondagar
     * @abstract getGroups
     * @param integer $network_id 
     * @param integer $user_id 
     * @param array $group_ids if specify result will be from these $group_ids
     * @since 08/10/14
     */
    public function getGroups($network_id = NULL, $user_id = NULL, $group_ids = NULL) {

        $conditions = array(
            $this->alias . '.network_id' => $network_id,
            $this->alias . '.user_id' => $user_id
        );

        if (is_array($group_ids)) {
            $conditions = array_merge($conditions, array($this->alias . '.group_id' => $group_ids));
        }

        return $this->find('list', array(
                    'conditions' => $conditions,
                    'fields' => array(
                        $this->alias . '.group_id'
                    )
        ));
    }

    
    public function getNetworkGroups($network_id) {
        return $this->find('list', array(
                    'conditions' => array(
//                        'UsersGroup.user_id' => $user_id,
                        'UsersGroup.network_id' => $network_id
                    ),
                    'fields' => array('UsersGroup.group_id')
        ));
    }
    
    public function getUserGroupId($group_id, $network_id){
        $result =  $this->find('all', array(
                    'conditions' => array(
                        'UsersGroup.group_id' => $group_id,
                        'UsersGroup.network_id' => $network_id
                    ),
//            'group' => array('UsersGroup.group_id'),
            'fields' => array('UsersGroup.group_id','UsersGroup.id'),
        ));
        $result = Hash::extract($result, '{n}.UsersGroup.id');
        return $result;
    }
}

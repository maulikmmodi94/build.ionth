<?php

/**
 * 
 * User Controller
 * @author Hardik Sondagar <hardik.sondagar@blogtard.com>
 * @abstract User Controller Class
 * @copyright (c) 2013, Blogtard.com 
 * 
 */
App::uses('Model', 'Model');

class UserNetwork extends AppModel {

    public $primaryKey = 'id';
    public $uses = array('UserNetwork','Network','UserInfo');
    public $belongsTo = array(
        'Network' => array(
            'className' => 'Network',
            'counterCache' => array('user_count' => array(
                    array('UserNetwork.status' => STATUS_ACTIVE, 'UserNetwork.isDeleted' => NOT_DELETED)
                )),
        ),
        'User' => array(
            'className' => 'User'
        )
    );
    public $hasOne = array(
        'UserInfo' => array(
            'className' => 'UserInfo',
            'foreignKey' => false,
            'conditions' => array('UserInfo.user_id =`UserNetwork`.`user_id`')
        )
    );
     public $hasMany = array(
        'TaskAssignment' => array(
            'className' => 'TaskAssignment',
            'foreignKey' => 'user_id',
            'conditions' => array('TaskAssignment.user_id = UserNetwork.user_id')
        )
    );
    public $defaultConditions = array('UserNetwork.isDeleted' => NOT_DELETED, 'UserNetwork.status' => array(STATUS_ACTIVE));

    public function update($username, $network_id, $conditions = null) {


        $db = $this->getDataSource();
        $added_by = $db->value($conditions['UserNetwork']['added_by'], 'string');
        return $this->updateAll(array('UserNetwork.status' => $conditions['UserNetwork']['status'], 'UserNetwork.added_by' => $added_by), array('UserNetwork.username' => $username, 'UserNetwork.network_id' => $network_id));
    }

    public function add($data) {

        $this->create();
        return $this->save($data);
    }

    public function view($username, $onlyAdmin = FALSE) {

        if ($onlyAdmin) {
            return $this->findAllByUsernameAndRole($username, ROLE_ADMIN);
        } else {
            return $this->findAllByUsername($username);
        }
    }

    public function getUserNetworkField($user_id, $network_id, $field_name) {
        $field = 'UserNetwork.' . $field_name;
        $network = $this->findByUserIdAndNetworkId($user_id, $network_id, array($field));
        if (count($network) > 0) {
            return $network['UserNetwork'][$field_name];
        } else {
            return -1;
        }
    }

    public function setUserNetworkField($user_id, $network_id, $field_name, $field_value) {
        $field = 'UserNetwork.' . $field_name;
        return $this->updateAll(
                        array(
                    $field => $field_value,
                        ), array(
                    'UserNetwork.user_id' => $user_id,
                    'UserNetwork.network_id' => $network_id
                        )
        );
    }

    public function getremaininggroupcount($username, $network_id) {
        $user = $this->findByUsernameAndNetworkId($username, $network_id, array('UserNetwork.user_group_limit', 'UserNetwork.users_group_count'));
        return $user['UserNetwork']['user_group_limit'] - $user['UserNetwork']['users_group_count'];
    }

    public function isAdmin($user_id = NULL, $network_id = NULL) {
        $UserNetwork = $this->findByUserIdAndNetworkId($user_id, $network_id, array('UserNetwork.role'));
        $allowed_role = array('admin', 'owner');

        if (count($UserNetwork) > 0 && in_array($UserNetwork['UserNetwork']['role'], $allowed_role)) {
            if ($UserNetwork['UserNetwork']['role'] == $allowed_role[0]) {
                return 1;
            }
            return 2;
        }
        return false;
    }

    public function remove($user_id, $network_id, $removed_by = NULL, $allow_admin_remove = false, $allow_owner_remove = false) {

        $db = $this->getDataSource();
        $status = $db->value(STATUS_REMOVED, 'string');
        $deleted = $db->value(date('Y-m-d H:i:s'), 'string');

        /* Things to Update */
        $update = array(
            'UserNetwork.status' => $status,
            'UserNetwork.isDeleted' => DELETED,
            'UserNetwork.removed_by' => $removed_by,
            'UserNetwork.deleted' => $deleted,
        );

        /* Update on following Conditions */
        $conditions = array(
            'UserNetwork.user_id' => $user_id,
            'UserNetwork.network_id' => $network_id,
            'UserNetwork.status' => STATUS_ACTIVE,
            'UserNetwork.role' => array(
                ROLE_USER
            )
        );

        /* Allow to remove admin for owner only */
        if ($allow_admin_remove) {
            $conditions['UserNetwork.role'] = array(ROLE_USER, ROLE_ADMIN);
        }
        /* Allow to remove owner for superadmin only */
        if ($allow_owner_remove) {
            $conditions['UserNetwork.role'] = array(ROLE_USER, ROLE_ADMIN, ROLE_OWNER);
        }


        if ($this->updateAll($update, $conditions)) {
            $this->updateCounterCache(array('network_id' => $network_id));
            return true;
        }
        return false;
    }
    
    
    /**
     * Leave a network
     * @param type $network_id
     * @param type $user_id
     * @return boolean
     * @author Manish M. Demblani <manish_demblani@blogtard.com
     */
    public function leavenetwork($network_id, $user_id) {

        $db = $this->getDataSource();
        $status = $db->value(STATUS_LEFT, 'string');
        $deleted = $db->value(date('Y-m-d H:i:s'), 'string');
        
        /* Things to Update */
        $update = array(
            'UserNetwork.status' => $status,
            'UserNetwork.isDeleted' => DELETED,
            'UserNetwork.removed_by' => $user_id,
            'UserNetwork.deleted' => $deleted
        );
        /* Update on following Conditions */
        $conditions = array(
            'UserNetwork.user_id' => $user_id,
            'UserNetwork.network_id' => $network_id,
            'UserNetwork.status' => STATUS_ACTIVE,
            'UserNetwork.role !=' => ROLE_OWNER
        );
        
        if ($this->updateAll($update,$conditions))
        {
            $this->updateCounterCache(array('network_id' => $network_id));
            return true;
        } else {
            return false;
        }
    }

    public function assignRole($user_id, $network_id, $role) {

        $allowed_role = array(ROLE_ADMIN, ROLE_USER);
        if (!in_array($role, $allowed_role)) {
            return false;
        }

        $db = $this->getDataSource();
        $role = $db->value($role, 'string');
        /* Things to Update */
        $update = array('UserNetwork.role' => $role);



        /* Update on following Conditions */
        $conditions = array('UserNetwork.user_id' => $user_id, 'UserNetwork.network_id' => $network_id, 'UserNetwork.status' => STATUS_ACTIVE, 'UserNetwork.role !=' => ROLE_OWNER);

        return $this->updateAll($update, $conditions);
    }

    public function getNetworkUser($network_id) {
//        debug($network_id);
        return $this->find('all', array(
                    'contain' => array(
                        'UserInfo'=> array('fields' => array('UserInfo.email'))
                        ),
                    'fields' => array('UserNetwork.user_id'),
                    'conditions' => array('UserNetwork.network_id' => $network_id)
                )
            );
    }

    public function isNetworkUsers($network_id, $user_ids) {
        if (empty($user_ids))
            return true;
        $users_count = $this->find('count', array('conditions' => array('UserNetwork.network_id' => $network_id, 'UserNetwork.user_id' => $user_ids)));

        if (count($user_ids) > 0 && count($user_ids) == $users_count) {
            return true;
        }
        return false;
    }

    public function getAdminCount($network_id) {
        $conditions = array('UserNetwork.network_id' => $network_id, 'UserNetwork.role' => ROLE_ADMIN);
        return $this->getCount($conditions);
    }

    public function getUserNetworkCount($user_id = NULL) {
        if ($user_id) {
            $conditions = array('UserNetwork.user_id' => $user_id);
            return $this->find('count', array('conditions' => $conditions));
        } else {
            return -1;
        }
    }

    /**
     * getUsers
     *
     * @author Hardik Sondagar
     * @abstract get network users
     * @param integer $network_id 
     * @param array $user_ids if specify result will be from these user_ids
     * @since 08/10/14
     */
    public function getUsers($network_id = NULL, $user_ids = NULL) {
        $conditions = array(
            $this->alias . '.network_id' => $network_id
        );

        if (is_array($user_ids)) {
            $conditions = array_merge($conditions, array($this->alias . '.user_id' => $user_ids));
        }
        return $this->find('list', array(
                    'conditions' => $conditions,
                    'fields' => array(
                        $this->alias . '.user_id'
                    )
        ));
    }

    public function getUserRole($network_id = null, $user_id = null) {
        return $this->find('all', array(
                    'conditions' => array('UserNetwork.network_id' => $network_id,
                        'UserNetwork.user_id' => $user_id
                    ),
                    'fields' => array('UserNetwork.role')
        ));
    }

    public function findNetwork($User_id = null){
        $this->log('$User_id');
        $this->log($User_id);
        $var = $this->find('first', array(
              'conditions' => array(
                                    'UserNetwork.user_id' => $User_id
                                    ),
              'fields' => array('UserNetwork.network_id'),
             'order' => 'UserNetwork.network_id desc'
              ));
              return $var['UserNetwork']['network_id'];
    }

    /**
     * Function for getting all the networks of a user
     * Made for mobile application
     * @author Anupama <anupama@blogtard.com>
     */
    public function getUserNetworks($user_id) {

        if (!isset($user_id)) {
            return -1;
        }

        $UserNetworks = $this->find('all', array(''
            . 'conditions' => array('UserNetwork.user_id' => $user_id),
            'contain' => array('Network'),
            'fields' => array('network_id', 'role', 'status', 'Network.id', 'Network.name','Network.url', 'Network.user_count', 'Network.status')
        ));

        return $UserNetworks;
    }

    /**
     * Returns the default network of a user based on number of users in the network
     * @param type $user_id
     * @return int
     * @todo : check the network returned is not been expired
     */
    public function getDefaultNetwork($user_id = 0) {

        $defaultNetwork = 0;
        $user_count = 0;

        $UserNetworks = $this->getUserNetworks($user_id);

        //finding default network
        foreach ($UserNetworks as $UserNetwork) {
            
            if ($UserNetwork['Network']['user_count'] > $user_count) {
            
                $user_count = $UserNetwork['Network']['user_count'];
                
                $defaultNetwork = $UserNetwork['Network']['id'];
            }
        }
        
        $this->log("Default network",$defaultNetwork);
        return $defaultNetwork;
    }
    
    
    public function getUser($network_id = null, $user_ids = array(), $page = null, $limit = null){
       
        
         $result =  $this->find('all', array(
            'conditions' => array('UserNetwork.network_id' => $network_id,
                                  'UserNetwork.user_id !=' => $user_ids
                        ), 
            'fields' => array('UserNetwork.user_id'),
            'limit' => $limit,
            'page' => $page,
            'order' => 'UserNetwork.id desc'
        ));
         $result = Hash::extract($result, '{n}.UserNetwork.user_id');
        return $result;
       }
    public function getAllNetworkUsers($network_id = null){
       
        
         $result =  $this->find('all', array(
            'conditions' => array('UserNetwork.network_id' => $network_id
//                                  'UserNetwork.user_id ' => $user_ids
                        ), 
            'fields' => array('UserNetwork.user_id')
//            'limit' => $limit,
//            'page' => $page,
//            'order' => 'UserNetwork.id desc'
        ));
         $result = Hash::extract($result, '{n}.UserNetwork.user_id');
        return $result;
       }
       
       public function getAllUser($network_id = null, $user_ids = array(), $page = null, $limit = null,$id = null){
       if($id == 0){
           $result =  $this->find('all', array(
            'conditions' => array('UserNetwork.network_id' => $network_id,
                                  'UserNetwork.user_id !=' => $user_ids
                        ), 
            'fields' => array('UserNetwork.user_id'),
            'limit' => $limit,
            'page' => $page,
            'order' => 'UserNetwork.id desc'
        ));
       }else{
           $result =  $this->find('all', array(
            'conditions' => array('UserNetwork.network_id' => $network_id,
                            'And' => array(      'UserNetwork.user_id !=' => $user_ids,
                                                 'UserNetwork.id <' => $id
                                           )
                        ), 
             'fields' => array('UserNetwork.user_id'),
             'limit' => $limit,
            'page' => $page,
             'order' => 'UserNetwork.id desc'
        ));
       }
        
         
         $result = Hash::extract($result, '{n}.UserNetwork.user_id');
         
        return $result;
       }
       
       public function getUserNetworkId($user_ids, $network_id){
            $result =  $this->find('all', array(
            'conditions' => array('UserNetwork.network_id' => $network_id,
                                  'UserNetwork.user_id ' => $user_ids
                        ), 
            'fields' => array('UserNetwork.user_id','UserNetwork.id'),
        ));
         $result = Hash::extract($result, '{n}.UserNetwork.id');
        return $result;
       }
       
//       public function getAllUserTask($user_id, $network_id){
//           $result = $this->find('all', array(
//               'conditions' => array(
//                   'UserNetwork.network_id' => $network_id,
//                   'UserNetwork.user_id !=' => $user_id
//               ),
//               'contain' => array(
//                   'TaskAssignment'
//               ),
////               'fields' => array('UserNetwork.user_id','UserNetwork.id'),
//           ));
//           return $result;
//       }
    
/**
     * discription : save the last feedID in the table
     * @param type $user_id
     * @param type $network_id
     * @param type $field
     * @return type
     */
    public function setFeedField($user_id, $network_id, $field){
        
         return $this->updateAll(
                        array('UserNetwork.lastseen_feed' => $field), 
                array('and' => array(
                        array('UserNetwork.network_id' => $network_id),
                        array('UserNetwork.user_id' => $user_id)
                    ))
        );
        
    }
    /**
     * discription : return the last feedID which is seen by user.
     * @param type $user_id
     * @param type $network_id
     * @return type
     */
    public function getFeedId($user_id, $network_id){
        $feedId = $this->find('first', array(
                    'conditions' => array('UserNetwork.network_id' => $network_id,
                                            'UserNetwork.user_id' => $user_id),
                     'fields' => 'UserNetwork.lastseen_feed'
                    )
        );
        
        return $feedId['UserNetwork']['lastseen_feed'];
    }

}
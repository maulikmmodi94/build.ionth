<?php

/**
 * Copyright 2010 - 2013, Cake Development Corporation (http://cakedc.com)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright 2010 - 2013, Cake Development Corporation (http://cakedc.com)
 * @license MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
App::import('Vendor', 'S3', array('file' => 'S3' . DS . 'S3.php'));

/**
 * Users Plugin User Model
 *
 * @package User
 * @subpackage User.Model
 */
class UserInfo extends AppModel {

    /**
     * Name
     *
     * @var string
     */
    public $name = 'UserInfo';

    /**
     * Relations
     */
    public $belongsTo = array('User');

    /**
     * Virtual Field
     */
    public function __construct($id = false, $table = null, $ds = null) {
        parent::__construct($id, $table, $ds);
        $this->virtualFields = array(
            'name' => 'CONCAT(UserInfo.first_name, " ", UserInfo.last_name)',
            'rname' => 'CONCAT(UserInfo.last_name, " ", UserInfo.first_name)',
            'xvga' => 'CONCAT("' . _public_bucket_endpoint . S3_Picture_Profile_Path . '",UserInfo.photo_dir, "/xvga_", UserInfo.photo)',
            'vga' => 'CONCAT("' . _public_bucket_endpoint . S3_Picture_Profile_Path . '", UserInfo . photo_dir ,"/vga_", UserInfo.photo)',
            'thumb' => 'CONCAT("' . _public_bucket_endpoint . S3_Picture_Profile_Path . '", UserInfo . photo_dir ,"/thumb_", UserInfo.photo)'
        );
    }

    /**
     * Adding Photo Upload Plugin
     */
    var $uses = array('Upload.Upload');
    public $s3_bucket_name = _public_bucket_name;
    public $s3_file_permissions = S3::ACL_PUBLIC_READ;
    public $s3_filepath = null;
    public $actsAs = array(
        'Containable',
        'Upload.Upload' => array(
            'photo' => array(
                'rootDir' => '',
                'path' => 'pictures{DS}profile{DS}',
                'thumbnailPath' => 'pictures{DS}profile{DS}',
                'fields' => array(
                    'dir' => 'photo_dir',
                    'type' => 'photo_type',
                    'size' => 'photo_size',
                ),
                'thumbnails' => true,
                'thumbnailSizes' => array(
                    'xvga' => '160x160',
                    'vga' => '64x64',
                    'thumb' => '32x32'
                ),
                'mimetypes' => array('image/jpeg', 'image/jpg', 'image/png',),
                'mode' => 0755,
                'rename' => false
            )
        )
    );

    /**
     * Validation parameters
     *
     * @var array
     */
    public $validate = array(
        'first_name' => array(
            'rule1' => array(
                'rule' => array('between', 2, 36),
                'message' => 'Your first name is too long.'
            ),
            'rule2' => array(
                'rule' => array('custom', '/^[a-zA-Z. ]*$/'),
                'message' => 'Your name can have only alphabets, spaces and dots.'
            ),
            'rule3' => array(
                'rule' => 'notEmpty',
                'message' => "Come on, you've got to have a first name."
            )
        ),
        'last_name' => array(
            'rule1' => array(
                'rule' => array('between', 2, 36),
                'on' => 'update',
                'message' => 'Your last name is too long.'
            ),
            'rule2' => array(
                'rule' => array('custom', '/^[a-zA-Z. ]*$/'),
                'message' => 'Your name can have only alphabets, spaces and dots.',
                'on' => 'update'
            ),
            'rule3' => array(
                'rule' => 'notEmpty',
                'message' => "Come on, you've got to have a last name."
            )
        ),
        'gender' => array(
            'rule1' => array(
                'rule' => array('inList', array(0, 1)),
                'message' => 'Gender can only be male or female'
            )
        ),
        'designation' => array(
            'rule1' => array(
                'rule' => array('between', 0, 200),
                'message' => 'Seriously, thats how long your title is! Please make it shorter.',
                'allowEmpty' => TRUE
            ),
            'rule2' => array(
                'rule' => array('custom', '/^[a-z0-9 _-]*$/i'), //Rule for alphanumeric + spaces + underscore and hyphens
                'message' => 'Designation can have alphabets, numbers, hyphens, underscores and spaces only.'
            )
        ),
        'skype' => array(
            'rule1' => array(
                'rule' => array('between', 6 , 32),
                'message' => 'Invalid Skype Id.',
                'allowEmpty' => TRUE
            ),
            'rule2' => array(
                'rule' => array('custom','/^(live:|facebook:)*[a-z][a-z0-9\.,\-_]{5,31}$/i'),
                'message' => 'Your Skype Name must be between 6-32 characters, start with a letter and contain only letters and numbers (no spaces or special characters).'
            )
        ),
        'company' => array(
            'rule1' => array(
                'rule' => array('between', 0, 200),
                'message' => 'company name can not be more than 200',
                'allowEmpty' => TRUE
            )
        ),
        'photo' => array(
            'rule1' => array(
                'rule' => array('fileSize', '<=', '1MB'),
                'message' => 'Image must be less than 1MB'
            ),
            'rule2' => array(
                'rule' => array('extension', array('jpeg', 'png', 'jpg')),
                'message' => 'Please supply a valid image.',
                'allowEmpty' => TRUE
            )
        ),
        'dob' => array(
            'rule1' => array(
                'rule' => 'date',
                'message' => 'Date of Birth should in proper date format'
            ),
            'inRange' => array(
                'rule' => 'isAgeInAcceptedRange',
                'message' => 'Your age must be greater than 13.'
            )
        ),
        'location' => array(
            'rule1' => array(
                'rule' => array('between', 2, 100),
                'message' => 'Location can be between 3 and 100',
                'allowEmpty' => TRUE
            )
        ),
        'isDeleted' => array(
            'rule1' => array(
                'rule' => 'notEmpty',
                'message' => 'Status can not be null'
            )
        ),
        'created' => array(
            'rule1' => array(
                'rule' => 'notEmpty',
                'created time can not be empty'
            ),
            'rule2' => array(
                'rule' => 'datetime',
                'message' => 'Please give a valid date and time.'
            )
        ),
        'modified' => array(
            'rule1' => array(
                'rule' => 'notEmpty',
                'created time can not be empty'
            ),
            'rule2' => array(
                'rule' => 'datetime',
                'message' => 'Please give a valid date and time.'
            )
        ),
        'deleted' => array(
            'rule1' => array(
                'rule' => 'datetime',
                'message' => 'Please give a valid date and time.'
            )
        )
    );

    function isAgeInAcceptedRange($check) {

        list($Y, $m, $d) = explode("-", $check['dob']);

        $userAge = ( date("md") < $m . $d ? date("Y") - $Y - 1 : date("Y") - $Y );
        if (($userAge >= 13)):
            return true;
        else:
            return false;
        endif;
    }

    public function view($user_ids, $options = null) {

        /* default condition and fields */
        $fields = array('UserInfo.user_id', 'UserInfo.first_name', 'UserInfo.last_name', 'UserInfo.name', 'UserInfo.vga', 'UserInfo.thumb', 'UserInfo.xvga', 'UserInfo.location', 'UserInfo.designation','UserInfo.company', 'UserInfo.contact_no','UserInfo.email');
        $conditions = array('UserInfo.user_id' => $user_ids);

        if (!empty($options['fields']))
            $fields = array_merge($fields, $options['fields']);

        if (!empty($options['conditions']))
            $conditions = array_merge($conditions, $options['conditions']);

        $users = $this->find('all', array(
            'conditions' => $conditions,
            'fields' => $fields
        ));
        return $users;
    }
    
    public function findLike($user_id = null, $name = NULL){
        
        $data = array();
        $result = $this->find('all', array(
            'conditions'=>array(
                'UserInfo.user_id' => $user_id,
                'Or' => array(
                    'UserInfo.rname like' => $name.'%',
                    'UserInfo.name like' => $name.'%'
                    )
                ),
            'order'=>array('UserInfo.id DESC')
            ));
            foreach ($result as $key => $value) {
            $result[$key]['UserInfo']['type'] = 'user';
            $result[$key]['UserInfo']['value'] = $value['UserInfo']['user_id'];
            $data['Tags'][$key] = $result[$key]['UserInfo'];
        }        
            return $data;
    }

}

<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class Conversation extends AppModel {

    public $uses = 'conversations';
    public $primaryKey = 'id';
    public $hasMany = array(
        'Message' => array(
            'className' => 'Message',
            'order' => 'Message.created DESC'
        ),
        'UserConversation' => array(
            'className' => 'UserConversation',
            'conditions' => array('UserConversation.isDeleted' => NOT_DELETED)
        )
    );
    public $defaultConditions = array('Conversation.isDeleted' => NOT_DELETED);
    public $validate = array(
        'id' => array(
            'rule1' => array(
                'rule' => 'notEmpty',
                'message' => 'Id can not be empty'
            ),
            'rule2' => array(
                'rule' => 'numeric',
                'message' => 'Id can only be numeric'
            )
        ),
        'network_id' => array(
            'rule1' => array(
                'rule' => 'notEmpty',
                'message' => 'Network Id can not be empty'
            ),
            'rule2' => array(
                'rule' => 'numeric',
                'message' => 'Network Id can only be numeric'
            )
        ),
        'title' => array(
            'rule1' => array(
                'rule' => array('maxLength', 30),
                'message' => 'Title can not be more than 30'
            )
        ),
        'user_id' => array(
            'rule1' => array(
                'rule' => 'notEmpty',
                'message' => 'User Id can not be empty'
            ),
            'rule2' => array(
                'rule' => 'numeric',
                'message' => 'User Id can only be numeric'
            )
        ),
        'group_id' => array(
            'rule1' => array(
                'rule' => 'numeric',
                'message' => 'Group Id can only be numeric'
            )
        ),
        'mapping' => array(
            'rule1' => array(
                'rule' => 'notEmpty',
                'message' => 'Mapping can not be null'
            ),
            'rule2' => array(
                'rule' => array('maxLength', '20'),
                'message' => 'Mapping can not be more than 20'
            )
        ),
        'status' => array(
            'rule1' => array(
                'rule' => 'notEmpty',
                'message' => 'Status can not be null'
            ),
            'rule2' => array(
                'rule' => array('maxLength', '20'),
                'message' => 'status can not be more than 20'
            )
        ),
        'userconversation_count' => array(
            'rule1' => array(
                'rule' => 'notEmpty',
                'message' => 'User Conversation Count can not be empty'
            ),
            'rule2' => array(
                'rule' => 'numeric',
                'message' => 'User Conversation Count can only be numeric'
            )
        ),
        'message_count' => array(
            'rule1' => array(
                'rule' => 'notEmpty',
                'message' => 'Message Count can not be empty'
            ),
            'rule2' => array(
                'rule' => 'numeric',
                'message' => 'Message Count can only be numeric'
            )
        ),
        'isDeleted' => array(
            'rule1' => array(
                'rule' => 'notEmpty',
                'message' => 'isDeleted can not be null'
            ),
            'rule2' => array(
                'rule' => array('inList', array(0, 1)),
                'message' => 'isDeleted can only be from (0,1)'
            )
        ),
        'created' => array(
            'rule1' => array(
                'rule' => 'notEmpty',
                'created time can not be empty'
            ),
            'rule2' => array(
                'rule' => 'datetime',
                'message' => 'Please give a valid date and time.'
            )
        ),
        'modified' => array(
            'rule1' => array(
                'rule' => 'notEmpty',
                'created time can not be empty'
            ),
            'rule2' => array(
                'rule' => 'datetime',
                'message' => 'Please give a valid date and time.'
            )
        ),
        'deleted' => array(
            'rule1' => array(
                'rule' => 'datetime',
                'message' => 'Please give a valid date and time.'
            )
        )
    );

    /**
     * @abstract    Create new conversation and stores the related users along with the first message
     * @param integer $network_id
     * @param string $author
     * @param integer $authorid
     * @param array $users
     * @param integer $mapping
     * @param string $msgbody
     * @param integer $msgdata
     * @param string $title
     * @param string $description
     * @param integer $type
     * @return      conversationid if successful,false otherwise
     */
    public function createconversation($network_id, $author, $users, $mapping, $group_id = NULL, $msgbody) {
        $time = Date('Y-m-d H:i:s');
        $data = array();
        $userconversation = array();
        $message = array();
        //create conversation
        $data['Conversation']['network_id'] = $network_id;
        $data['Conversation']['user_id'] = $author;
        $data['Conversation']['mapping'] = $mapping;
        if (!empty($group_id)) {   //in case group chat, add group id
            $data['Conversation']['group_id'] = $group_id;
        }
        $data['Conversation']['created'] = $time;
        $data['Conversation']['modified'] = $time;
        //add author in conversation as admin  
        $value['network_id'] = $network_id;
        $value['user_id'] = $author;
        $value['role'] = ROLE_ADMIN;
        $value['status'] = STATUS_ACTIVE;
        $value['isUnread'] = SEEN;
        if (!empty($group_id)) {   //in case group chat, add group id
            $value['group_id'] = $group_id;
        }
        array_push($userconversation, $value);
        //add users in conversation as normal users
        if (sizeof($users) > 0) {
            foreach ($users as $user) {
                if ($user !== $author || ($user === $author && $mapping == Conversation_mapping_single)) {
                    $value = array();
                    $value['network_id'] = $network_id;
                    $value['user_id'] = $user;
                    $value['role'] = ROLE_USER;
                    $value['status'] = STATUS_ACTIVE;
                    $value['isUnread'] = NOT_SEEN;
                    if (!empty($group_id)) {   //in case group chat, add group id
                        $value['group_id'] = $group_id;
                    }                    
                    array_push($userconversation, $value);
                }
            }
        }
        //add all usersconversation data
        $data['UserConversation'] = $userconversation;
        $value = array();
        $value['created_by'] = $author;
        $value['body'] = $this->detectAndTransformLinks($msgbody);
        $value['created'] = $time;
        $value['user_id'] = $author;
        array_push($message, $value);
        $data['Message'] = $message;
        //create conversation settings for all these users
        if ($this->saveAll($data, array(
                    'atomic' => TRUE,
                    'deep' => TRUE
                        )
                ))
            return $this->getLastInsertID();
        else {
            return False;
        }
    }

    public function getConversationField($conversation_id, $network_id, $field_name) {
        $field = 'Conversation.' . $field_name;
        $conversation = $this->findByIdAndNetworkId($conversation_id, $network_id, array($field));
        return isset($conversation['Conversation'][$field_name]) ? $conversation['Conversation'][$field_name] : NULL;
    }

    public function conversationDelete($conversation_id) {
        $dataSource = $this->getDataSource();
        $time = $dataSource->value(date('Y-m-d H:i:s'), 'string');
        return $this->UserConversation->updateAll(
                array('UserConversation.isDeleted' => DELETED, 'UserConversation.deleted' => $time, 'Conversation.isDeleted' => DELETED, 'Conversation.deleted' => $time), array('Or' => array('UserConversation.conversation_id' => $conversation_id, 'Conversation.id' => $conversation_id)
                )
        );
    }

}

?>

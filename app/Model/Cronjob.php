<?php

App::uses('AppModel', 'Model');

/**
 * Cronjob Model
 *
 */
class Cronjob extends AppModel {

    /**
     * Display field
     *
     * @var string
     */
    public $displayField = 'name';


}

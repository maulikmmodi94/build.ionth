<?php

App::uses('AppModel', 'Model');

/**
 * Plan Model
 *
 */
class Plan extends AppModel {

    /**
     * Display field
     *
     * @var string
     */
    public $displayField = 'name';

    /**
     * Validation parameters
     *
     * @var array
     */
    public $validate = array(
        'name' => array(
            'rule1' => array(
                'rule' => array('between', 2, 36),
                'message' => 'Plan name must be greater than 2 and less than 36.'
            ),
            'rule2' => array(
                'rule' => array('custom', '/^[a-zA-Z ]*$/'),
                'message' => 'Your name can have only alphabets and spaces.'
            ),
            'rule3' => array(
                'rule' => 'notEmpty',
                'message' => "Come on, you've got to have a plan name."
            )
        ),
        'description' => array(
            'rule1' => array(
                'rule' => array('between', 0, 200),
                'message' => 'Seriously, thats how long your description is! Please make it shorter.',
                'allowEmpty' => TRUE
            ),
            'rule2' => array(
                'rule' => array('custom', '/^[a-z0-9 _-]*$/i'), //Rule for alphanumeric + spaces + underscore and hyphens
                'message' => 'description can have alphabets, numbers, hyphens, underscores and spaces only.'
            )
        ),
        'user_limit' => array(
            'rule1' => array(
                'rule' => array('between', 0, 999),
                'message' => 'user limit should between 0 and 999',
            ),
            'rule2' => array(
                'rule' => 'numeric',
                'message' => 'Only number allowed.'
            )
        ),
        'time_limit' => array(
            'rule1' => array(
                'rule' => 'numeric',
                'message' => 'Only number allowed.'
            )
        ),
        'storage_limit' => array(
            'rule1' => array(
                'rule' => 'numeric',
                'message' => 'Only number allowed.'
            )
        ),
       
    );


}

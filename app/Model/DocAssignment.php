<?php

App::uses('DocsAppModel', 'Model');

/**
 * 
 */
class DocAssignment extends DocsAppModel {

    public $useTable = 'docs_assignment';
    public $defaultConditions = array('DocAssignment.isDeleted ' => NOT_DELETED, 'DocAssignment.status' => STATUS_ACTIVE);
    public $belongsTo = array(
        'Doc' => array(
            'className' => 'Doc',
            'foreignKey' => 'doc_id',
            'counterCache' => TRUE
        )
    );
    
    public $hasOne = array(
   'DocAssignmentSelfJoin' => array(
       'className' => 'DocAssignment',
       'foreignKey' => false,
       'type' => 'inner',
       )
   );

    public $validate = array(
        'doc_id' => array(
            'rule1' => array(
                'rule' => 'notEmpty',
                'message' => 'Doc id can not be empty'
            ),
            'rule2' => array(
                'rule' => 'numeric',
                'message' => 'Doc ID can only be Numeric'
            )
        ),
        'group_id' => array(
            'rule1' => array(
                'rule' => 'notEmpty',
                'message' => 'Group id can not be empty'
            ),
            'rule2' => array(
                'rule' => 'numeric',
                'message' => 'Group can only be Numeric'
            )
        ),
        'user_id' => array(
            'rule1' => array(
                'rule' => 'notEmpty',
                'message' => 'User id can not be empty'
            ),
            'rule2' => array(
                'rule' => 'numeric',
                'message' => 'User can only be Numeric'
            )
        ),
        'shared_by' => array(
            'rule1' => array(
                'rule' => 'notEmpty',
                'message' => 'Shared By can not be empty'
            ),
            'rule2' => array(
                'rule' => 'numeric',
                'message' => 'Shared By only be Numeric'
            )
        ),
        'removed_by' => array(
            'rule1' => array(
                'rule' => 'notEmpty',
                'message' => 'Removed By can not be empty'
            ),
            'rule2' => array(
                'rule' => 'numeric',
                'message' => 'Removed By can only be Numeric'
            )
        ),
        'status' => array(
            'rule1' => array(
                'rule' => 'notEmpty',
                'message' => 'Status can not be null'
            ),
            'rule2' => array(
                'rule' => array('maxLength', '20'),
                'message' => 'status can not be more than 20'
            )
        ),
        'isDeleted' => array(
            'rule1' => array(
                'rule' => 'notEmpty',
                'message' => 'Status can not be null'
            ),
            'rule2' => array(
                'rule' => array('inList', array(0, 1)),
                'message' => 'isDeleted can only be from (0,1)'
            )
        ),
        'created' => array(
            'rule1' => array(
                'rule' => 'notEmpty',
                'created time can not be empty'
            ),
            'rule2' => array(
                'rule' => 'datetime',
                'message' => 'Please give a valid date and time.'
            )
        ),
        'modified' => array(
            'rule1' => array(
                'rule' => 'notEmpty',
                'created time can not be empty'
            ),
            'rule2' => array(
                'rule' => 'datetime',
                'message' => 'Please give a valid date and time.'
            )
        ),
        'deleted' => array(
            'rule1' => array(
                'rule' => 'datetime',
                'message' => 'Please give a valid date and time.'
            )
        )
    );

    /*
     * Return Values
     * false -> Failed to share
     * true -> Successfully shared
     */

    public function addSharedList($doc_id, $shared_by, $network_id, $sharelist = NULL) {
        $this->create();
        $data = array();
        $values = array();
        if (!empty($sharelist['user']) || !empty($sharelist['group'])) {
            if (!empty($sharelist['user'])) {
                foreach ($sharelist['user'] as $key => $value) {
                    $values['doc_id'] = $doc_id;
                    $values['user_id'] = $value;
                    $values['network_id'] = $network_id;
                    $values['shared_by'] = $shared_by;
                    $values['status'] = STATUS_ACTIVE;
                    array_push($data, $values);
                }
            }
            $values = array();
            if (!empty($sharelist['group'])) {
                foreach ($sharelist['group'] as $key => $value) {
                    $values['doc_id'] = $doc_id;
                    $values['group_id'] = $value;
                    $values['network_id'] = $network_id;
                    $values['shared_by'] = $shared_by;
                    $values['status'] = STATUS_ACTIVE;
                    array_push($data, $values);
                }
            }
            if ($this->saveAll($data)) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /*
     * true -> Failed to remove the user form sharelist
     * false -> Successfully removed
     */

    public function removeSharedList($doc_id, $removed_by, $network_id, $remove_id, $remove_type) {
        if ($remove_type == 'user') {
            if ($this->updateAll(
                            array(
                        'DocAssignment.isDeleted' => DELETED,
                        'DocAssignment.status' => '"' . STATUS_REMOVED . '"',
                        'DocAssignment.removed_by' => $removed_by
                            ), array(
                        'DocAssignment.doc_id = ' => $doc_id,
                        'DocAssignment.network_id' => $network_id,
                        'DocAssignment.user_id' => $remove_id
                            )
                    )
            ) {
                $this->updateCounterCache(array('doc_id' => $doc_id));
                return true;
            } else {
                return false;
            }
        } else if ($remove_type == 'group') {
            if ($this->updateAll(
                            array(
                        'DocAssignment.isDeleted' => DELETED,
                        'DocAssignment.status' => '"' . STATUS_REMOVED . '"',
                        'DocAssignment.removed_by' => $removed_by
                            ), array(
                        'DocAssignment.doc_id = ' => $doc_id,
                        'DocAssignment.network_id' => $network_id,
                        'DocAssignment.group_id' => $remove_id
                            )
                    )
            ) {
                $this->updateCounterCache(array('doc_id' => $doc_id));
                return true;
            } else {
                return false;
            }
        }
    }

    /*
     * Function returns the list of groups and users with whom doc is shared
     */

    public function checkSharedList($doc_id, $remove_id, $remove_type) {
        $sharedlist = $this->find('list', array(
            'conditions' => array(
                'DocAssignment.doc_id' => $doc_id,
                'Or' => array(
                    'DocAssignment.user_id' => $removelist['user'],
                    'DocAssignment.group_id' => $removelist['group'],
                )
            ),
            'fields' => array('DocAssignment.group_id', 'DocAssignment.user_id')
        ));
        $removelist['user'] = array_intersect($removelist['user'], $sharedlist);
        $removelist['group'] = array_intersect($removelist['group'], array_keys($sharedlist));
        return $removelist;
    }
    
    public function sortDocs($user_id, $network_id, $options){
        $option = $this->getSharedDocs($user_id, $network_id, $options);
        unset($option['order']);
        
        switch ($options['sort']) {

            case "size0" :
                
                $option['contain']['Doc']['order'] = array('Doc.file_size asc');
                
                break;

            case "size1" :

               $option['contain']['Doc']['order'] = array('Doc.file_size desc');
                
                break;

            case "create0" :

               $option['contain']['Doc']['order'] = array('Doc.created asc');
    
                break;
    
            case "create1" :

               $option['contain']['Doc']['order'] = array('Doc.created desc');
                
               break;
           
           case "name0" :

               $option['contain']['Doc']['order'] = array('Doc.name asc');
                
               break;
           
           case "name1" :

               $option['contain']['Doc']['order'] = array('Doc.name desc');
                
               break;
            default :
                
            break;
        }
        return $this->find('list', $option);
    }

    /*
     * Function returns list of Doc that are shared with the 
     * given user or group
     */

    public function getSharedDocs($user_id, $network_id, $options = array()) {
        if(empty($options['group_id'])){
            $options['group_id']=-1;
        }
        if($options['type'] == 'user'){
            $option = [
                'conditions' => [
                    'DocAssignment.network_id' => $network_id,
                    'OR'=> [
                        'DocAssignment.user_id' => $user_id,
                        'DocAssignment.group_id' => $options['group_id']
                    ]
                ],
                'contain' => ['DocAssignmentSelfJoin' => [
                    'conditions' => [
                        'DocAssignmentSelfJoin.doc_id = DocAssignment.doc_id',
                        'DocAssignmentSelfJoin.isDeleted' => NOT_DELETED,
                        'DocAssignmentSelfJoin.status' => STATUS_ACTIVE,
                        'OR'=> [
                                'DocAssignmentSelfJoin.user_id' => $options['id'],
                                'DocAssignmentSelfJoin.group_id' => $options['group_id']
                        ]
                     ]
                ]],
                'fields' => ['DocAssignment.doc_id'],
                'limit' => $options['limit'],
                'group' =>  ['DocAssignment.doc_id'],
                'page' => $options['page'],
                'order' => 'DocAssignment.id desc'
            ];
        }else if($options['type'] == 'group'){
            $option = array(
                'conditions' => array(
                        'DocAssignment.network_id' => $network_id,
                        'DocAssignment.group_id' => $options['group_id']
                ),
                'fields' => array('DocAssignment.doc_id'),
                'limit' => $options['limit'],
                'group' =>  array('DocAssignment.doc_id'),
                'page' => $options['page'],
                'order' => 'DocAssignment.id desc'
            );
        }else{
            $option = array(
            'conditions' => array(
                'DocAssignment.network_id' => $network_id,
                'OR' => array(
                    'DocAssignment.user_id' => $user_id,
                    'DocAssignment.group_id' => $options['group_id']
                )
            ),
            'fields' => array('DocAssignment.doc_id'),
            'group' =>  array('DocAssignment.doc_id'),
            'order' => 'DocAssignment.id desc'
            );  
        }
        return $option;
    }
    /**
     * main method where doc_id is computed based on user_id or group_id or search string
     * 
     * @param type $user_id
     * @param type $network_id
     * @param type $options
     * @return type
     */
    public function getDocId($user_id, $network_id, $options){
        $result = array();
        
        if(empty($options['sort'])){
            $option = $this->getSharedDocs($user_id, $network_id, $options);
            $doc_id = $this->find('list', $option);
            $doc_id_count = $this->getSharedDocsCount($user_id, $network_id, $options);
        }  else {
            $doc_id = $this->sortDocs($user_id, $network_id, $options);
            $doc_id_count = count($doc_id);
        }
         if($options['type'] == 'user' || $options['type'] == 'group'){
             
            $result[0] = $doc_id;
            $result['doc_count'] = $doc_id_count;
             
         }else{
            $this->Doc = ClassRegistry::init("Doc");
            $result[0] = $this->Doc->likeDocs($doc_id, $options, LISTS);
            $result['doc_count'] = $this->Doc->likeDocs($doc_id, $options, COUNT);
         }
        return $result;
    }

    



    public function getSharedDocsCount($user_id, $network_id, $options) {

        $option = $this->getSharedDocs($user_id, $network_id, $options);
        
        if(isset($option['limit']) && isset($option['page'])){
            unset($option['limit']);   
            unset($option['page']);
        }
        return $this->find('count', $option);
    }

    /*
     * Function to check if Doc is assignment to a user or group
     */

    public function checkAssignedDoc($doc_id, $share_list) {
        $docassigned = $this->find('all', array(
            'conditions' => array(
                'DocAssignment.doc_id' => $doc_id,
                'Or' => array(
                    'DocAssignment.user_id' => $share_list['user'],
                    'DocAssignment.group_id' => $share_list['group'],
                )
            ),
            'fields' => array('DocAssignment.group_id', 'DocAssignment.user_id')
        ));
        $share_list['user'] = array_diff($share_list['user'], Hash::extract($docassigned, '{n}.DocAssignment.user_id'));
        $share_list['group'] = array_diff($share_list['group'], Hash::extract($docassigned, '{n}.DocAssignment.group_id'));
        return $share_list;
    }

    public function getDocAssignmentField($doc_id, $network_id, $user_id, $field_name) {
        $field = 'DocAssignment.' . $field_name;
        $doc = $this->find('all', array('conditions' => array('DocAssignment.doc_id' => $doc_id, 'DocAssignment.user_id' => $user_id, 'DocAssignment.network_id' => $network_id), 'fields' => $field_name));
        return isset($doc[0]['DocAssignment'][$field_name]) ? $doc[0]['DocAssignment'][$field_name] : NULL;
    }

}

<?php

/**
 * 
 * User Controller
 * @author Hardik Sondagar <hardik.sondagar@blogtard.com>
 * @abstract User Controller Class
 * @copyright (c) 2013, Blogtard.com 
 * 
 */
App::uses('Model', 'Model');
App::uses('SluggableBehavior', 'Utils.Model/Behavior');

class Network extends AppModel {

    public $hasMany = array(
        'UserNetwork' => array(
            'className' => 'UserNetwork',
            'dependent' => true,
        ),
        'NetworkPlan' => array(
            'className' => 'NetworkPlan',
            'dependent' => true,

        ),
        'Group' => array(
            'className' => 'Group'
        ),
        'NetworkAccess' => array(
            'className' => 'NetworkAccess'
        )
    );

    /**
     * Virtual Field
     */
    public $virtualFields = array(
        'remaining_users' => 'Network.user_limit - Network.user_count');
    public $validate = array(
        'url' => array(
            'rule1' => array(
                'rule' => 'notEmpty',
                'message' => 'URL cannot be empty'
            ),
            'rule2' => array(
                'rule' => array('maxLength', 20),
                'message' => 'URL cannot contain characters greater then 30.'
            ),
            'rule3' => array(
                'rule' => 'isUnique',
                'message' => 'This url has already been taken.'
            ),
            'rule4' => array(
                'rule' => array('custom', '/^[a-z0-9 _\-\']*$/i'), //Rule for alphanumeric + spaces + underscore and hyphens
                'message' => 'Network URL can have alphabets, numbers, hyphens, underscores and spaces only'
            )
        ),
        'name' => array(
            'rule1' => array(
                'rule' => 'notEmpty',
                'message' => 'Name is required!'
            ),
            'rule2' => array(
                'rule' => array('maxLength', 30),
                'message' => 'Network name cannot contain characters greater then 30.'
            ),
            'rule3' => array(
                'rule' => array('custom', '/^[a-z0-9 _\-\']*$/i'), //Rule for alphanumeric + spaces + underscore and hyphens
                'message' => 'Network can have alphabets, numbers, hyphens, underscores and spaces only'
            ),
            'rule4' => array(
                'rule' => array('minLength', 2),
                'message' => 'Network name cannot contain characters less then 2.'
            )
//            'rule3' => array(
//                'rule' => 'alphaNumeric',
//                'message' => 'Name should be alphanumeric'
//            ),
        ),
        'user_count' => array(
            'rule1' => array(
                'rule' => 'notEmpty',
                'message' => 'User count can not be empty'
            ),
            'rule2' => array(
                'rule' => 'Numeric',
                'message' => 'user count can only be numeric'
            )
        ),
        'group_count' => array(
            'rule1' => array(
                'rule' => 'notEmpty',
                'message' => 'Group count can not be null'
            ),
            'rule2' => array(
                'rule' => 'Numeric',
                'message' => 'user count can only be numeric'
            )
        ),
        'file_count' => array(
            'rule1' => array(
                'rule' => 'notEmpty',
                'message' => 'File count can not be null'
            ),
            'rule2' => array(
                'rule' => 'Numeric',
                'message' => 'file count can only be numeric'
            )
        ),
        'user_limit' => array(
            'rule1' => array(
                'rule' => 'notEmpty',
                'message' => 'User limit can not be null'
            ),
            'rule2' => array(
                'rule' => 'Numeric',
                'message' => 'user limit can only be numeric'
            )
        ),
        'file limit' => array(
            'rule1' => array(
                'rule' => 'notEmpty',
                'message' => 'file limit can not be null'
            ),
            'rule2' => array(
                'rule' => 'Numeric',
                'message' => 'file limit can only be numeric'
            )
        ),
        'status' => array(
            'rule1' => array(
                'rule' => 'notEmpty',
                'message' => 'Status can not be null'
            ),
            'rule2' => array(
                'rule' => array('maxLength', '20'),
                'message' => 'status can not be more than 20'
            )
        ),
        'isDeleted' => array(
            'rule1' => array(
                'rule' => 'notEmpty',
                'message' => 'Status can not be null'
            )
        ),
        'expires' => array(
            'rule1' => array(
                'rule' => 'notEmpty',
                'expire time can not be empty'
            ),
            'rule2' => array(
                'rule' => 'datetime',
                'message' => 'Please give a valid date and time.'
            )
        ),
        'created' => array(
            'rule1' => array(
                'rule' => 'notEmpty',
                'created time can not be empty'
            ),
            'rule2' => array(
                'rule' => 'datetime',
                'message' => 'Please give a valid date and time.'
            )
        ),
        'modified' => array(
            'rule1' => array(
                'rule' => 'notEmpty',
                'created time can not be empty'
            ),
            'rule2' => array(
                'rule' => 'datetime',
                'message' => 'Please give a valid date and time.'
            )
        ),
        'deleted' => array(
            'rule1' => array(
                'rule' => 'datetime',
                'message' => 'Please give a valid date and time.'
            )
        )
    );
    public $defaultConditions = array('Network.isDeleted' => NOT_DELETED, 'Network.expires > NOW()');

    /**
     * Constructor
     *
     * @param bool|string $id ID
     * @param string $table Table
     * @param string $ds Datasource
     */
    public function __construct($id = false, $table = null, $ds = null) {
        $this->_setupBehaviors();
        parent::__construct($id, $table, $ds);
        $time = $this->readTimeZone();
        if ($time) {
            $model = ($this->name);
            $this->virtualFields['expires'] = 'DATE_SUB(' . $model . '.expires,INTERVAL ' . $time . ' MINUTE)';
        }
    }

    /**
     * Setup available plugins
     *
     * This checks for the existence of certain plugins, and if available, uses them.
     *
     * @return void
     * @link https://github.com/CakeDC/search
     * @link https://github.com/CakeDC/utils
     */
    protected function _setupBehaviors() {
        if (class_exists('SluggableBehavior') && Configure::read('Users.disableSlugs') !== true) {
            $this->actsAs['Utils.Sluggable'] = array(
                'slug' => 'url',
                'label' => 'name',
                'separator' => '',
                'method' => 'multibyteSlug');
        }
    }

    public function getNetworkField($network_id, $field_name) {
        $field = 'Network.' . $field_name;
        $network = $this->findById($network_id, array($field));
        return isset($network['Network'][$field_name]) ? $network['Network'][$field_name] : NULL;
    }

    public function setNetworkField($network_id, $field_name, $field_value) {
        $field = 'Network.' . $field_name;
        $done = $this->updateAll(
                array(
            $field => $field_value,
                ), array(
            'Network.id' => $network_id
                )
        );
        return $done;
    }

    public function getRemainingUsers($id) {
        $this->id = $id;
        return $this->field('remaining_users');
    }

    public function add($data) {
         $data['Network']['url'] = $this->makeUniqueSlug(preg_replace('/[^\da-z]/i', '', strtolower($data['Network']['name'])));
        $this->create();
        return $this->saveAll($data);
    }

    public function isUrlAvailable($url = NULL) {
        if (!empty($url)) {
            $conditions = array(
                'Network.url' => $url
            );
            $this->defaultConditions = NULL;
            return !$this->hasAny($conditions);
        } else {
            return FALSE;
        }
    }

    public function deleteNetwork($id) {
        $conditions = array(
            $this->alias . '.' . $this->primaryKey => $id
        );
        $addition_to_update = array(
            $this->alias . '.user_count' => 0,
            $this->alias . '.group_count' => 0,
            $this->alias . '.file_count' => 0
        );

        $this->unbindModel(
                array(
                    'hasMany' => array('Group')
                )
        );
        return $this->soft_delete($conditions, $addition_to_update, 1);
    }


}

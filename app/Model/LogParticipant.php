<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class LogParticipant extends AppModel {

    public $useTable = 'log_participants';
    public $belongsTo = array(
        'Log' => array(
            'className' => 'Log',
            'foreignKey' => 'log_id'
        )
    );

    public function add($log_id, $network_id, $list) {
        $this->create();
        $data = array();
        foreach ($list as $key => $value) {
            $value['LogParticipant']['type'] = $value['type'];
            $value['LogParticipant']['participant_id'] = $value['value'];
            $value['LogParticipant']['log_id'] = $log_id;
            $value['LogParticipant']['network_id'] = $network_id;
            array_push($data, $value);
        }
        $this->saveAll($data);
    }

    public function viewuser($user_id, $network_id, $page) {
        $log_ids = $this->find('list', array('conditions' => array(
                'LogParticipant.type' => Log_type_user,
                'LogParticipant.participant_id' => $user_id,
                'LogParticipant.network_id' => $network_id
            ),
            'fields' => array('LogParticipant.log_id'),
            'page' => $page,
            'limit' => Logstoload
        ));
        return $this->Log->find('all', array(
                    'conditions' => array('Log.id' => $log_ids),
                    'contain' => array(
                        'LogParticipant' => array(
                            'fields' => array('LogParticipant.*')
                        )
                    ),
                    'order' => array('Log.id DESC')
        ));
    }

    public function viewgroup($group_id, $network_id) {
        $log_ids = $this->find('list', array('conditions' => array(
                'LogParticipant.type' => Log_type_group,
                'LogParticipant.participant_id' => $group_id,
                'LogParticipant.network_id' => $network_id
            ),
            'fields' => array('LogParticipant.log_id')
        ));
        return $this->Log->find('all', array(
                    'conditions' => array('Log.id' => $log_ids),
                    'contain' => array(
                        'LogParticipant' => array(
                            'fields' => array('LogParticipant.*')
                        )
                    ),
                    'order' => array('Log.id DESC')
        ));
    }

    public function view($value, $network_id, $type, $page) {
        //User and group can be both an object and participant
        if ($type == Log_type_user || $type == Log_type_group) {
            $log_ids = $this->find('list', array('conditions' => array(
                    'LogParticipant.type' => $type,
                    'LogParticipant.participant_id' => $value,
                    'LogParticipant.network_id' => $network_id
                ),
                'fields' => array('LogParticipant.log_id')
            ));
            return $this->Log->find('all', array(
                        'conditions' => array(
                            'or' => array(
                                array('Log.id' => $log_ids),
                                array('and' =>
                                    array('Log.type' => $type),
                                    array('Log.object_id' => $value)
                                )
                            )
                        ),
                        'contain' => array(
                            'LogParticipant' => array(
                                'fields' => array('LogParticipant.*')
                            )
                        ),
                        'order' => array('Log.id DESC'),
                        'page' => $page,
                        'limit' => Logstoload
            ));
        } else {
            return $this->Log->find('all', array(
                        'conditions' => array('Log.type' => $type, 'Log.object_id' => $value),
                        'contain' => array(
                            'LogParticipant' => array(
                                'fields' => array('LogParticipant.*')
                            )
                        ),
                        'order' => array('Log.id DESC')
            ));
        }
    }

//
//    public function viewnetwork($network_id, $page) {
//        return $this->find('all', array(
//                    'contains' => array(
//                        'Log' => array(
//                            'fields' => array('Log.creator', 'Log.object_id', 'Log.code')
//                        )
//                    ),
//                    'conditions' => array(
//                        'LogParticipant.network_id' => $network_id
//                    ),
//                    'fields' => array('LogParticipant.log_id', 'Log.creator', 'Log.object_id', 'Log.code'),
//                    'page' => $page,
//                    'limit' => LogsToLoad,
//                    'order' => array('LogParticipant.id DESC')
//        ));
//    }
//
}

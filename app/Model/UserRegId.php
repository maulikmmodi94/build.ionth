<?php
App::uses('AppModel', 'Model');
/**
 * UserRegId Model
 *
 */
class UserRegId extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'user_regId';
        
        public function store_regId($data = array()){
            $this->create();
            $this->save($data, FALSE);
        }
        
        public function Check_regId($reg_id= null, $user_id = null){
            return $this->find('first', array(
                'conditions' => array('UserRegId.user_id' => $user_id,
                                        'UserRegId.registration_id' => $reg_id),
                'fields' => array('UserRegId.registration_id'),
                    'order' => 'UserRegId.id desc'
            ));
        }
        
        public function fetch_regId($user_id = null){
            return $this->find('all', array(
                'conditions' => array('UserRegId.user_id' => $user_id),
                'fields' => array('UserRegId.registration_id'),
                    'order' => 'UserRegId.id desc'
            ));
        }

}

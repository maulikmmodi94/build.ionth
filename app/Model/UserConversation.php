<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class UserConversation extends AppModel {

    public $belongsTo = array(
        'Conversation' => array(
            'className' => 'Conversation',
            'conditions' => array('UserConversation.isDeleted' => NOT_DELETED),
            'counterCache' => true
        ),
        'User' => array(
            'className' => 'User',
            'type' => 'left'
        )
    );
    public $defaultConditions = array('UserConversation.isDeleted' => 0);
    public $validate = array(
        'conversation_id' => array(
            'rule1' => array(
                'rule' => 'notEmpty',
                'message' => 'Conversation Id can not be empty'
            ),
            'rule2' => array(
                'rule' => 'numeric',
                'message' => 'Conversation Id can only be numeric'
            )
        ),
        'network_id' => array(
            'rule1' => array(
                'rule' => 'notEmpty',
                'message' => 'Network Id can not be empty'
            ),
            'rule2' => array(
                'rule' => 'numeric',
                'message' => 'Network Id can only be numeric'
            )
        ),
        'user_id' => array(
            'rule1' => array(
                'rule' => 'numeric',
                'message' => 'User Id can only be numeric'
            )
        ),
        'group_id' => array(
            'rule1' => array(
                'rule' => 'numeric',
                'message' => 'Group Id can only be numeric'
            )
        ),
        'role' => array(
            'rule1' => array(
                'rule' => 'notEmpty',
                'message' => 'Role can not be null'
            ),
            'rule2' => array(
                'rule' => array('maxLength', '20'),
                'message' => 'Role can not be more than 20'
            )
        ),
        'status' => array(
            'rule1' => array(
                'rule' => 'notEmpty',
                'message' => 'Status can not be null'
            ),
            'rule2' => array(
                'rule' => array('maxLength', '20'),
                'message' => 'status can not be more than 20'
            )
        ),
        'isDeleted' => array(
            'rule1' => array(
                'rule' => 'notEmpty',
                'message' => 'Status can not be null'
            ),
            'rule2' => array(
                'rule' => array('inList', array(0, 1)),
                'message' => 'isDeleted can only be from (0,1)'
            )
        ),
        'created' => array(
            'rule1' => array(
                'rule' => 'notEmpty',
                'created time can not be empty'
            ),
            'rule2' => array(
                'rule' => 'datetime',
                'message' => 'Please give a valid date and time.'
            )
        ),
        'modified' => array(
            'rule1' => array(
                'rule' => 'notEmpty',
                'created time can not be empty'
            ),
            'rule2' => array(
                'rule' => 'datetime',
                'message' => 'Please give a valid date and time.'
            )
        ),
        'deleted' => array(
            'rule1' => array(
                'rule' => 'datetime',
                'message' => 'Please give a valid date and time.'
            )
        )
    );

    /**
     * @abstract Check if a one-one conversation exists between the two users
     * @param type $author  User1
     * @param type $user    User2
     * @return type         Conversation_id if exists,else NULL
     * @author Naimish Sakhpara <naimish.sakhpara@blogtard.com>
     */
    public function getconversationid($author, $user, $network_id) {
        $author_list = $this->find('list', array(
            'conditions' => array(
                'UserConversation.user_id' => $author,
                'UserConversation.network_id' => $network_id
            ),
            'fields' => array('UserConversation.conversation_id')
                )
        );
        $user_list = $this->find('list', array(
            'conditions' => array(
                'UserConversation.user_id' => $user,
                'UserConversation.network_id' => $network_id
            ),
            'fields' => array('UserConversation.conversation_id')
                )
        );
        $common_conv = array_intersect($author_list, $user_list);
        $common_conv = $this->Conversation->find('first', array('conditions' => array(
                'Conversation.id' => $common_conv,
                'Conversation.mapping' => 'single'
            ),
            'fields' => array('Conversation.id')
                )
        );
        return isset($common_conv['Conversation']['id']) ? $common_conv['Conversation']['id'] : Null;
    }

    /**
     * Load conversations
     * @param type $user
     * @param type $limit
     * @param string $reference
     * @param type $direction
     * @return type
     * @author Naimish Sakhpara <naimish.sakhpara@blogtard.com>
     */
    public function loadconversations($user, $network_id, $page = 1) {
        $conversation_ids = $this->find('list', array('conditions' => array(
                'UserConversation.user_id' => $user,
                'UserConversation.network_id' => $network_id
            ), 'fields' => array('UserConversation.conversation_id')
        ));
        return $this->Conversation->find('all', array(
                    'conditions' => array(
                        'Conversation.id' => $conversation_ids
                    ),
                    'contain' => array('UserConversation' => array(
                            'fields' => array('UserConversation.user_id', 'UserConversation.status', 'UserConversation.role', 'UserConversation.group_id', 'UserConversation.isUnread'),
                            'conditions' => array('UserConversation.isDeleted' => NOT_DELETED)
                        ),
                        'Message' => array(
                            'fields' => array('Message.id', 'Message.body', 'Message.created', 'Message.user_id'),
                            'limit' => Messagestoload,
                            'order' => array('Message.id DESC')
                        )),
                    'fields' => array('Conversation.id', 'Conversation.user_id', 'Conversation.group_id', 'Conversation.user_conversation_count', 'Conversation.mapping', 'Conversation.status'),
                    'order' => array('Conversation.modified DESC'),
                    'page' => $page,
                    'limit' => Conversationstoload
        ));
    }

    public function getconversationtotalpages($user, $network_id) {
        $count = $this->find('count', array(
            'conditions' => array(
                'UserConversation.user_id' => $user,
                'UserConversation.network_id' => $network_id
            )
        ));
        if (($count % Conversationstoload) == 0)
            return $count / Conversationstoload;
        else
            return floor($count / Conversationstoload) + 1;
    }

    public function getUserConversationField($user_id, $conversation_id, $network_id, $field_name) {
        $field = 'UserConversation.' . $field_name;
        $conversation = $this->findByUserIdAndConversationIdAndNetworkId($user_id, $conversation_id, $network_id, array($field));
        return isset($conversation['UserConversation'][$field_name]) ? $conversation['UserConversation'][$field_name] : NULL;
    }

    /**
     * @abstract    Load particular conversations of a user
     * @param type $conversation_id
     * @param type $user_id
     * @return type
     * @author Naimish Sakhpara <naimish.sakhpara@blogtard.com>
     */
    public function loadconversationsfromid($conversation_ids, $user_id, $latestmessageid) {
        $conversation_ids = $this->find('list', array(
            'conditions' => array('UserConversation.conversation_id' => $conversation_ids, 'UserConversation.user_id' => $user_id),
            'fields' => array('UserConversation.conversation_id')
        ));
        return $this->Conversation->find('all', array(
                    'conditions' => array(
                        'Conversation.id' => $conversation_ids
                    ),
                    'contain' => array('UserConversation' => array(
                            'fields' => array('UserConversation.user_id', 'UserConversation.status', 'UserConversation.role', 'UserConversation.group_id', 'UserConversation.isUnread'),
                            'conditions' => array('UserConversation.isDeleted' => NOT_DELETED)
                        ),
                        'Message' => array(
                            'fields' => array('Message.id', 'Message.body', 'Message.created', 'Message.user_id'),
                            'conditions' => array('Message.id >' => $latestmessageid),
                            'limit' => Messagestoload,
                            'order' => array('Message.id DESC')
                        )),
                    'fields' => array('Conversation.id', 'Conversation.user_id', 'Conversation.group_id', 'Conversation.user_conversation_count', 'Conversation.mapping', 'Conversation.status'),
                    'order' => array('Conversation.modified DESC'),
                    'limit' => Conversationstoload
        ));
    }

    /**
     * @abstract Add participants to a conversation. If entry present changes the status.
     * @param type $conversation_id
     * @param type $userlist        user_ids in an array
     * @return boolean      True if successful,False Otherwise
     * @author Naimish Sakhpara <naimish.sakhpara@blogtard.com>
     */
    public function addparticipants($conversation_id, $network_id, $userlist, $group_id = NULL) {
        $this->create();
        $data = [];
        foreach ($userlist as $user) {
            $value = array();
            $value['conversation_id'] = $conversation_id;
            $value['user_id'] = $user;
            $value['role'] = ROLE_USER;
            $value['status'] = STATUS_ACTIVE;
            $value['network_id'] = $network_id;
            $value['isUnread'] = 0;
            if (!is_null($group_id)) {
                $value['group_id'] = $group_id;
            }
            array_push($data, $value);
        }
        if ($this->saveAll($data, array(
                    'atomic' => TRUE,
                    'deep' => TRUE
                )))
            return TRUE;
        else {
            return FALSE;
        }
    }

    /**
     * @abstract    Remove participants from a conversation.
     * @param type $conversation_id
     * @param type $author
     * @param type $userlist    
     * @return boolean
     * @author Naimish Sakhpara <naimish.sakhpara@blogtard.com>
     */
    public function removeparticipants($conversation_id, $network_id, $userlist) {
        $dataSource = $this->getDataSource();
        $time = $dataSource->value(date('Y-m-d H:i:s'), 'string');
        if ($this->updateAll(
                        array('UserConversation.status' => "'" . STATUS_REMOVED . "'", 'UserConversation.deleted' => $time, 'UserConversation.isDeleted' => DELETED), array('UserConversation.conversation_id' => $conversation_id, 'UserConversation.network_id' => $network_id, 'UserConversation.user_id' => $userlist))) {
            $this->updateCounterCache(array('conversation_id' => $conversation_id));
            return true;
        } else {
            return false;
        }
    }

    /**
     * Leave a conversation
     * @param type $conversation_id
     * @param type $author
     * @return boolean
     * @author Naimish Sakhpara <naimish.sakhpara@blogtard.com>
     */
    public function leaveconversation($conversation_id, $author) {
        $dataSource = $this->getDataSource();
        $time = $dataSource->value(date('Y-m-d H:i:s'), 'string');
        if ($this->updateAll(
                        array('UserConversation.status' => STATUS_LEFT, 'UserConversation.isDeleted' => ISDELETED_DELETED, 'UserConversation.deleted' => $time,), array('UserConversation.conversation_id' => $conversation_id, 'UserConversation.user_id' => $author)
                )) {
            $this->updateCounterCache(array('conversation_id' => $conversation_id));
            return true;
        } else {
            return false;
        }
    }

    public function getgroupconversationid($group_id = NULL) {
        $details = $this->Conversation->find('first', array(
            'conditions' => array('Conversation.group_id' => $group_id),
            'fields' => array('Conversation.id')
        ));
        return $details['Conversation']['id'];
    }

    public function setconversationunread($conversation_id, $user_id) {
        return $this->updateAll(array('UserConversation.isUnread' => NOT_SEEN), array('and' => array(
                        array('UserConversation.conversation_id' => $conversation_id),
                        array('UserConversation.user_id !=' => $user_id)
        )));
    }

    public function setread($user_id, $conversation_id) {
        return $this->updateAll(
                        array('UserConversation.isUnread' => SEEN), array('and' => array(
                        array('UserConversation.conversation_id' => $conversation_id),
                        array('UserConversation.user_id' => $user_id)
                    ))
        );
    }

    public function loadconversationpeople($conversation_id) {
        $conversations = $this->Conversation->find('first', array(
            'conditions' => array('Conversation.id' => $conversation_id),
            'contain' => array('UserConversation')
        ));
        return $conversations;
    }

}

?>

<?php

/**
 * Application model for CakePHP.
 *
 * This file is application-wide model file. You can put all
 * application-wide model-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Model
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
App::uses('Model', 'Model');

/**
 * Application model for Cake.
 *
 * Add your application-wide methods in the class below, your models
 * will inherit them.
 *
 * @package       app.Model
 */
class AppModel extends Model {

    public $recursive = -1;
    public $actsAs = array('Containable');

    public function splittaglist($data) {
        /* Valid Types */
        $types = array('user', 'group', 'email');
        $returnData = array();
        foreach ($types as $value) {
            $returnData[$value] = Hash::extract($data, '{n}[type=/' . $value . '/].value');
        }
        foreach ($returnData as $key => $value) {
            $returnData[$key] = array_unique($value);
        }
        return $returnData;
    }

    public function validateEmails($emails = array()) {
        $status = false;
        $return = array();
        $return['valid'] = array();
        $return['invalid'] = array();
        foreach ($emails as $value) {
            $status = filter_var($value, FILTER_VALIDATE_EMAIL);
            if ($status) {
                array_push($return['valid'], $value);
            } else {
                array_push($return['invalid'], $value);
            }
        }

        return $return;
    }

    /**
     * Generate token used by the user registration system
     *
     * @param int $length Token Length
     * @return string
     */
    public function generateToken($length = 10) {
        $possible = '0123456789abcdefghijklmnopqrstuvwxyz';
        $token = "";
        $i = 0;

        while ($i < $length) {
            $char = substr($possible, mt_rand(0, strlen($possible) - 1), 1);
            if (!stristr($token, $char)) {
                $token .= $char;
                $i++;
            }
        }
        return $token;
    }

    public function __construct($id = false, $table = null, $ds = null) {
        parent::__construct($id, $table, $ds);
        $time = $this->readTimeZone();
        if ($time) {
            $model = ($this->name);
            if ($this->hasField('created')) {
                $this->virtualFields['created'] = 'DATE_SUB(' . $model . '.created,INTERVAL ' . $time . ' MINUTE)';
            }
            if ($this->hasField('modified')) {
                $this->virtualFields['modified'] = 'DATE_SUB(' . $model . '.modified,INTERVAL ' . $time . ' MINUTE)';
            }
            if ($this->hasField('deleted')) {
                $this->virtualFields['deleted'] = 'DATE_SUB(' . $model . '.deleted,INTERVAL ' . $time . ' MINUTE)';
            }
        }
    }

    public function beforeFind($queryData) {

        if (isset($this->defaultFields) && !empty($this->defaultFields)) {
            $queryData['fields'] = array_merge((array) $this->defaultFields, (array) $queryData['fields']);
        }
        if (isset($this->defaultConditions) && !empty($this->defaultConditions)) {
            $queryData['conditions'] = array_merge((array) $this->defaultConditions, (array) $queryData['conditions']);
        }
        return $queryData;
    }

    public function getCount($conditions = array()) {
        return $this->find('count', array('conditions' => $conditions));
    }

    public function getUTCDate($date, $operation) {
        $date = new DateTime($date);
        $time = $this->readTimeZone();
        if ($time) {
            if ($operation == 'add') {
                $date->add(new DateInterval('PT' . -$time . 'M'));
            } else {
                $date->sub(new DateInterval('PT' . -$time . 'M'));
            }
        }

        return $date->format('Y:m:d H:i:s');
    }

    public function setTimeZone($timezone) {
        //Set the timezone of user
        if ((int) $timezone) {
            CakeSession::write('Auth.User.timezone', (int) $timezone);
        } else {
            CakeSession::write('Auth.User.timezone', 0);
        }
    }

    public function readTimeZone() {
        if (class_exists('CakeSession') && CakeSession::check('Auth.User.timezone')) {
            return (int) CakeSession::read('Auth.User.timezone');
        }
        return 0;
    }
    
    function detectAndTransformHashTags($str) {
        $strings = explode("\n", $str);
        $out = '';
        foreach ($strings as $string) {
            $this->log($string);
            $partofstring = explode(" ", $string);
            $output = '';
            foreach ($partofstring as $partstring) {
                //check for any links present
                if (substr($partstring, 0, 7) == "http://" || substr($partstring, 0, 8) == "https://") {
                    //if a string is found,will be converted into URLs 
                    $partstring = ' <a href="' . $partstring . '" target="_blank">' . $partstring . '</a>';
                } //check for any emailids present
                else if (substr($partstring, 0, 4) == "www.") {
                    //if a string is found,will be converted into URLs 
                    $partstring = ' <a href="http://' . $partstring . '" target="_blank">' . $partstring . '</a>';
                } else if (filter_var(filter_var($partstring, FILTER_SANITIZE_EMAIL), FILTER_VALIDATE_EMAIL)) {
                    //if emailid found,will be converted into link
                    $partstring = ' <a href="mailto:' . $partstring . '" target="_blank">' . $partstring . '</a>';
                } else if (substr($partstring, 0, 1) == "#") {
                    $var = substr($partstring, 1);
                    if (ctype_alnum($var) && (!ctype_digit($var))) {
                        $partstring = preg_replace('/(^|[^a-z0-9_])#([a-z0-9_]+)/i', '<a href="' . Router::url(array('controller' => 'feeds', 'action' => 'hashtag')) . '/' . $var . '">$0</a>', $partstring);
                    }
                }
                $output .= $partstring . " ";
            }
//        $output = trim($output);
            //return array containing data and resultant body
//        return $output;
            $out .= $output . " <br> ";
        }
//    $out = trim($out);
        return $out;
    }

    function detectAndTransformLinks($string) {
        $partofstring = explode(" ", $string);
        $output = '';
        foreach ($partofstring as $partstring) {
            //check for any links present
            if (substr($partstring, 0, 7) == "http://" || substr($partstring, 0, 8) == "https://") {
                //if a string is found,will be converted into URLs 
                $partstring = '<a href="' . $partstring . '" target="_blank">' . $partstring . '</a>';
            } //check for any emailids present
            else if (substr($partstring, 0, 4) == "www.") {
                //if a string is found,will be converted into URLs 
                $partstring = '<a href="http://' . $partstring . '" target="_blank">' . $partstring . '</a>';
            } else if (filter_var(filter_var($partstring, FILTER_SANITIZE_EMAIL), FILTER_VALIDATE_EMAIL)) {
                //if emailid found,will be turned blue
                $partstring = '<font color="blue">' . $partstring . "</font>";
            }
            $output .= " " . $partstring;
        }
        $output = trim($output);
        //return array containing data and resultant body
        return $output;
    }

    /**
     * Soft Delete
     *
     * @author Hardik Sondagar
     * @abstract Soft Delete, will set isDeleted to DELETED and deleted to current timestamp, 
     *           This function also perform same operations on associate models via recursion.
     * @param array $conditions conditions for update.
     * @param array $addition_to_update addition fields to update (optional).
     * @param integer $level Number of level in Association to perform update, default is 0.
     * @return integer $AffectedRows returns number of rows affected by updation
     * @since 12/09/14
     * @modified 12/09/14
     */
    function soft_delete($conditions, $addition_to_update = array(), $level = 0) {

        /* Convert update fields to SQL expressions. Literal values converted to quoted using DboSource::value() */
        $dataSource = $this->getDataSource();
        $time = $dataSource->value(date('Y-m-d H:i:s'), 'string');

        /* Default fields to update */
        $default_to_update = array(
            $this->alias . '.isDeleted' => DELETED,
            $this->alias . '.deleted' => $time,
        );

        /* merging default fields and additional fields into $to_update */
        $to_update = array_merge($default_to_update, $addition_to_update);

        /* Perform Update */
        $return = $this->updateAll($to_update, $conditions);

        /* Perform soft_delete on associated models ( only if current model's primary key is set in $conditions variable */
        if ($return && $level > 0 && !empty($this->__backAssociation) && isset($conditions[$this->alias . '.' . $this->primaryKey])) {
            $level = $level - 1;
            $id = $conditions[$this->alias . '.' . $this->primaryKey];
            foreach (array_merge($this->hasMany, $this->hasOne) as $key => $model) {
//                if ($model['dependent'] !== true) {
//                    continue;
//                }
                $conditions = array(
                    $model['className'] . '.' . $model['foreignKey'] => $id
                );
                $this->$model['className']->soft_delete($conditions, array(), $level);
            }
        }

        return $return;
        /* return number of rows affected by updation */
        return $this->getAffectedRows();
    }

    /*
     * SoftDelete Behavior
     */

    public function exists($id = null) {
        if ($this->Behaviors->attached('SoftDelete')) {
            return $this->existsAndNotDeleted($id);
        } else {
            return parent::exists($id);
        }
    }

    public function delete($id = null, $cascade = true) {
        $result = parent::delete($id, $cascade);
        if ($result === false && $this->Behaviors->enabled('SoftDelete')) {
            $this->defaultConditions = null;
            return $this->field('isDeleted', array('isDeleted' => 1));
        }
        return $result;
    }
    
    
    
   

}

<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
App::uses('Model', 'Model');

class NetworkAccess extends AppModel {

    public $primaryKey = 'id';
    public $useTable = 'network_access';
    public $belongsTo = array(
        'Network' => array(
            'className' => 'Network',
            'counterCache' => 'ip_check'
        )
    );
    public $validate = array(
        'name' => array(
//            'rule1' => array(
//                'rule' => array('minLength',3),
//                'message' => 'Name should have atleast 3 characters.'
//            ),
            'rule2' => array(
                'rule' => array('maxLength', 30),
                'message' => 'Name cannnot have characters greater than 30.'
            ),
            'rule3' => array(
                'rule' => array('custom', '/^[a-z0-9 _-]*$/i'), //Rule for alphanumeric + spaces
                'message' => 'Name should be alphanumeric'
            )
        ),
        'role' => array(
            'rule1' => array(
                'rule' => 'notEmpty',
                'message' => 'Role cannot be empty'
            ),
            'rule2' => array(
                'rule' => array('inList', array(ROLE_ADMIN,ROLE_USER, ROLE_SPECIAL)),
                'message' => 'Invalid role'
            ),
            'rule3' => array(
                'rule' => array('custom', '/^[a-zA-Z]*$/i'), //Rule for alphabets
                'message' => 'Role should contain only alphabets'
            ), 
//            'required' => true,
//            'on' => 'create',
        ),
        'network_id' => array(
            'rule1' => array(
                'rule' => 'notEmpty',
                'message' => 'Network Id can not be empty'
            ),
            'rule2' => array(
                'rule' => 'numeric',
                'message' => 'Network Id can only be numeric'
            ),
//            'required' => true,
//            'on' => 'create',
        ),
        'user_id' => array(
            'rule1' => array(
                'rule' => 'numeric',
                'message' => 'User Id can only be numeric'
            )
        ),
        'ip_address' => array(
            'rule1' => array(
                'rule' => array('ValidIP'), //Rule for ip address
                'message' => 'Please enter valid ip address'
            )
        ),
    );
    
    public function ValidIP()
    {
        return (bool)filter_var($this->data[$this->alias]['ip_address'], FILTER_VALIDATE_IP);
    }

    public function add($data) {
        $this->create();
        $this->save($data);
    }

    public function deleteIp($network_id, $ip) {
        $this->deleteAll(
                array(
                    'NetworkAccess.network_id' => $network_id,
                    'NetworkAccess.ip_address' => $ip
                )
        );
    }

    public function deleteUser($network_id, $user) {
        $this->deleteAll(
                array(
                    'NetworkAccess.network_id' => $network_id,
                    'NetworkAccess.ip_address' => $user
                )
        );
    }

}
